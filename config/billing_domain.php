<?php
return [
    'annually' => '1',
    'biennially' => '2',
    'triennially' => '3',
    'quadrennially' => '4',
    'quinquennially' => '5'
];
