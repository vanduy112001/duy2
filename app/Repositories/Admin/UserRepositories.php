<?php

namespace App\Repositories\Admin;

use App\Factories\AdminFactories;
use App\Model\User;
use App\Model\Credit;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\SocialAccount;
use App\Model\GroupUser;
use App\Model\GroupProduct;
use App\Model\LogPayment;
use App\Model\QuotationDetail;
use App\Model\SendMail;
use Mail;
use Illuminate\Support\Facades\Hash;
use App\Model\LogActivity;
use App\Model\HistoryPay;
use App\Model\Agency;
use App\Model\Quotation;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use http\Env\Request;
use App\Model\Product;
use App\Model\QuotationServer;

// use App\Model\UserMeta;


/**
 * Repositories
 */
class UserRepositories
{

  protected $model;
  protected $userMetaModel;
  protected $credit;
  protected $social;
  protected $verify;
  protected $dashboard;
  protected $group_user;
  protected $group_product;
  protected $email_user;
  protected $subject;
  protected $history_pay;
  protected $log_activity;
  protected $log_payment;
  protected $agency;
  protected $quotation;
  protected $quotation_detail;
  protected $link_send_mail;
  protected $token;
  protected $send_mail;
  protected $product;
  protected $quotation_server;

  function __construct()
  {
    $this->model = new User();
    $this->product = new Product;
    $this->userMetaModel = new UserMeta();
    $this->credit = new Credit();
    $this->verify = new Verify;
    $this->social = new SocialAccount;
    $this->group_user = new GroupUser;
    $this->group_product = new GroupProduct;
    $this->log_activity = new LogActivity();
    $this->history_pay = new HistoryPay;
    $this->log_payment = new LogPayment;
    $this->agency = new Agency;
    $this->quotation = new Quotation;
    $this->quotation_detail = new QuotationDetail;
    $this->send_mail = new SendMail;
    $this->quotation_server = new QuotationServer;
    // API
    $this->dashboard = AdminFactories::dashBoardRepositories();
    $this->link_send_mail = env('HOST_API');
    $this->tokent = env('HOST_API_TOKEN');
  }

  public function getGroupUser()
  {
    return $this->group_user->orderBy('id', 'desc')->get();
  }

  public function all()
  {
    $result = $this->model->orderBy('id', 'desc')->with('user_meta', 'credit', 'social_account')->get();
    return $result;
  }

  public function change_social($id)
  {
    try {
      $user = $this->model->find($id);
      $this->social->where('user_id', $id)->delete();
      return [ 'error' => 0 ];
    } catch (\Throwable $th) {
      report($th);
      return [ 'error' => 1 ];
    }
  }

  //Đếm người dùng để cung cấp cho datatable
  public function count_users($user_type_group, $group_user)
  {
    // dd($user_type_group);
    $query = $this->model;
    //kiểm tra nhóm khách hàng (doanh nghiệp hay cá nhân)
    if ($user_type_group == 'enterprise') {
      $query = $query->where('enterprise', 1);
    } elseif ($user_type_group == 'individual') {
      $query = $query->where('enterprise', 0);
    }

    //kiểm tra nhóm đại lý
    if ($group_user) {
        if ($group_user === 'normal') {
            $query = $query->where('group_user_id', 0);
        } else {
            $query = $query->where('group_user_id', $group_user);
        }
    }

    return $query->get()->count();
  }

  //Đếm người dùng để cung cấp cho datatable (với điều kiện search)
  public function count_users_with_search($user_type_group, $group_user, $search)
  {
    //Nhóm các object search lại với nhau (id, name, email, phone)
    $query = $this->model->where(function($query_group) use($search) {
                            $query_group->where('id','LIKE','%'. $search. '%')
                            ->orWhere('name', 'LIKE','%'. $search. '%')
                            ->orWhere('email', 'LIKE','%'. $search. '%')
                            ->orWhereHas('user_meta', function ($query) use ($search) {
                                $query->where('phone', 'like', '%'.$search.'%');
                            });
                          });

    //Lọc khách hàng (doanh nghiệp hay cá nhân) từ object đã search
    if ($user_type_group == 'enterprise') {
        $query = $query->where('enterprise', 1);
    } elseif ($user_type_group == 'individual') {
        $query = $query->where('enterprise', 0);
    }
    //kiểm tra nhóm đại lý
    if ($group_user) {
        if ($group_user === 'normal') {
            $query = $query->where('group_user_id', 0);
        } else {
            $query = $query->where('group_user_id', $group_user);
        }
    }
    return $query->get()->count();
  }

  //Lấy dữ liệu người dùng để datatable có thể render
  public function get_all_to_datatale($user_type_group, $group_user, $start, $limit, $order,$dir)
  {
    //Gộp cột (credit_value + credit_total ở bảng Credit) vào bảng User để datatable có thể sort, vì không sort được ở bảng relationship
    $query = $this->model->with('user_meta', 'social_account')
                          ->addSelect([
                              'credit_value' => Credit::select('value')->whereColumn('credits.user_id', 'users.id')->take(1),
                              'credit_total' => Credit::select('total')->whereColumn('credits.user_id', 'users.id')->take(1)
                          ]);

    //kiểm tra nhóm khách hàng (doanh nghiệp hay cá nhân)
    if ($user_type_group == 'enterprise') {
      $query = $query->where('enterprise', 1);
    } elseif ($user_type_group == 'individual') {
      $query = $query->where('enterprise', 0);
    }

    //kiểm tra nhóm đại lý
    if ($group_user) {
        if ($group_user === 'normal') {
            $query = $query->where('group_user_id', 0);
        } else {
            $query = $query->where('group_user_id', $group_user);
        }
    }
    //Cung cấp dữ liệu phân trang
    $query = $query->offset($start)->limit($limit);

    //credit_value và credit_total có thuộc tính varchar, để sort thì chuyển sang dạng SIGN(64-bit integer)
    if ($order == 'credit_value' || $order == 'credit_total') {
      $query = $query->orderByRaw('CONVERT('.$order.', SIGNED) '.$dir);
    } else {
       $query = $query->orderBy($order,$dir);
    }
    return $query->get();
  }

  //Lấy dữ liệu người dùng để datatable có thể render (với điều kiện search)
  public function get_all_search_to_datatable($user_type_group, $group_user, $search, $start, $limit, $order, $dir)
  {
    //Tạo cột (credit_value + credit_total ở bảng Credit) và gộp vào bảng User để datatable có thể sort, vì không sort được ở bảng relationship
    $query = $this->model->with('user_meta', 'social_account')
                          ->addSelect([
                            'credit_value' => Credit::select('value')->whereColumn('credits.user_id', 'users.id')->take(1),
                            'credit_total' => Credit::select('total')->whereColumn('credits.user_id', 'users.id')->take(1)
                          ]);

    //Nhóm các object search lại với nhau (id, name, email, phone)
    $query = $query->where(function($query_group) use($search) {
                            $query_group->where('id','LIKE','%'. $search. '%')
                            ->orWhere('name', 'LIKE','%'. $search. '%')
                            ->orWhere('email', 'LIKE','%'. $search. '%')
                            ->orWhereHas('user_meta', function ($query) use ($search) {
                                $query->where('phone', 'like', '%'.$search.'%');
                            });
                          });

    //Lọc khách hàng (doanh nghiệp hay cá nhân) từ object đã search
    if ($user_type_group == 'enterprise') {
      $query = $query->where('enterprise', 1);
    }
    if ($user_type_group == 'individual') {
      $query = $query->where('enterprise', 0);
    }

    //kiểm tra nhóm đại lý
    if ($group_user) {
        if ($group_user === 'normal') {
            $query = $query->where('group_user_id', 0);
        } else {
            $query = $query->where('group_user_id', $group_user);
        }
    }

    //Cung cấp dữ liệu phân trang
    $query = $query->offset($start)->limit($limit);

    //credit_value và credit_total có thuộc tính varchar, để sort thì chuyển sang dạng SIGN(64-bit integer)
    if ($order == 'credit_value' || $order == 'credit_total') {
      $query = $query->orderByRaw('CONVERT('.$order.', SIGNED) '.$dir);
    } else {
       $query = $query->orderBy($order,$dir);
    }

    return $query->get();
  }

  public function searchUser($q)
  {
    return $this->model->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%')->get();
  }

  public function list_user_personal()
  {
    $result = $this->model->where('enterprise', false)->orderBy('id', 'desc')->with('credit')->get();
    return $result;
  }

  public function list_user_enterprise()
  {
    $result = $this->model->where('enterprise', true)->orderBy('id', 'desc')->with('credit')->get();
    return $result;
  }

  public function get_all_user()
  {
    $result = $this->model->orderBy('id', 'desc')->with('credit', 'user_meta', 'group_user')->get();
    return $result;
  }

  public function update_credit_with_admin($id, $request)
  {
     $user = $this->model->find($id);
     $money = !empty($request['amount']) ? $request['amount'] : 0;
     $money = str_replace( '.', '', $money );
     $money = str_replace( ',', '', $money );
     if ( $request['method_pay'] == 'plus' ) {
        $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'ADPL' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Cộng tiền vào tài khoản',
          'type_gd' => '98',
          'method_gd' => 'credit',
          'date_gd' => date('Y-m-d'),
          'money'=> $money,
          'status' => 'Active',
        ];
        $create = $this->history_pay->create($data_history);

        $credit = $user->credit;
        if (isset($credit)) {
          // ghi log giao dịch
          $data_log_payment = [
            'history_pay_id' => $create->id,
            'before' => $credit->value,
            'after' => $credit->value + $money,
          ];
          $this->log_payment->create($data_log_payment);

          $credit->value += $money;
          $credit->total += $money;
          $credit->save();
        } else {
          // ghi log giao dịch
          $data_log_payment = [
            'history_pay_id' => $create->id,
            'before' => 0,
            'after' => $money,
          ];
          $this->log_payment->create($data_log_payment);
          $data_credit = [
            'user_id' => $user->id,
            'value' => $money,
            'total' => $money,
          ];
          $this->credit->create($data_credit);
        }
          // Tạo log
        $data_log = [
         'user_id' => $user->id,
         'action' => 'Cộng tiền vào tài khoản',
         'model' => 'User/UpdateCredit',
         'description_user' => ' cộng ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản',
       ];
       $this->log_activity->create($data_log);
          // Tạo log
       $data_log = [
         'user_id' => Auth::user()->id,
         'action' => 'Cộng tiền vào tài khoản',
         'model' => 'User/UpdateCredit',
         'description' => ' cộng ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a>' ,
       ];
       $this->log_activity->create($data_log);

       return $create;
     }
     else {
        $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'ADMI' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Trừ tiền tài khoản',
          'type_gd' => '99',
          'method_gd' => 'credit',
          'date_gd' => date('Y-m-d'),
          'money'=> $money,
          'status' => 'Active',
        ];
        $create = $this->history_pay->create($data_history);

        $credit = $user->credit;
        if (isset($credit)) {
          // ghi log giao dịch
          $data_log_payment = [
            'history_pay_id' => $create->id,
            'before' => $credit->value,
            'after' => $credit->value - $money,
          ];
          $this->log_payment->create($data_log_payment);

          $credit->value -= $money;
          $credit->save();
        }
              // Tạo log
        $data_log = [
         'user_id' => $user->id,
         'action' => 'Trừ tiền tài khoản',
         'model' => 'User/UpdateCredit',
         'description_user' => ' Trừ ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản',
       ];
       $this->log_activity->create($data_log);
              // Tạo log
       $data_log = [
         'user_id' => Auth::user()->id,
         'action' => 'Trừ tiền tài khoản',
         'model' => 'User/UpdateCredit',
         'description' => ' trừ ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a>' ,
       ];
       $this->log_activity->create($data_log);

       return $create;
     }
  }

  // Tạo data trong bảng users
  public function create($data_user)
  {
    $user = $this->model->create($data_user);
    $data_send_mail_user = [
        'name' => $user->name,
        'email' => $user->email,
        'password' => $data_user['password'],
        'subject' => 'Đăng ký thành công tài khoản Cloudzone Portal - Cloudzone.vn'
    ];
    $data_tb_send_mail = [
      'type' => 'mail_create_user',
      'user_id' => $user->id,
      'status' => false,
      'content' => serialize($data_send_mail_user),
    ];
    $this->send_mail->create($data_tb_send_mail);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'tạo',
      'model' => 'Admin/User',
      'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> ',
    ];
    $this->log_activity->create($data_log);
    $data_credit = [
        'user_id' => $user->id,
        'total' => '0',
        'value' => '0',
    ];
    $this->credit->create($data_credit);
    $this->dashboard->sendUserDashBoard($user);
    if ($user) {
      return $user;
    } else {
      return false;
    }
  }

  // Tạo data trong bảng user_metas
  public function create_user_meta($data_user_meta)
  {
      $data_user_meta['makh'] = 'MAKH' . $this->rand_string(6);
      $user_meta = $this->userMetaModel->create($data_user_meta);
      if ($user_meta) {
        return true;
      } else {
        return false;
      }
  }

  // lấy data từ id của người dùng
  public function detail($id)
  {
    $userInfo = $this->model->find($id);
    return $userInfo;
  }

  public function detail_user_meta($id)
  {
    $userInfo = $this->userMetaModel->where('user_id', $id)->first();
    return $userInfo;
  }

  public function update_user($id, $data_user)
  {
    $user = $this->model->find($id);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'chỉnh sửa',
      'model' => 'Admin/User',
      'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> ',
    ];
    $this->log_activity->create($data_log);
    $user = $user->update($data_user);
    if ($user) {
      return true;
    } else {
      return false;
    }
  }

  public function update_user_meta($id, $data_user_meta)
  {
    $userMeta = $this->userMetaModel->where('user_id', $id)->update($data_user_meta);
    if ($userMeta) {
      return true;
    } else {
      return false;
    }
  }


  public function delete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];
      // Xóa tất cả các table có liên quan trên user
      // ghi log
      $user =$this->model->find($id);
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'xóa',
        'model' => 'Admin/User',
        'description' => ' khách hàng ' . $user->name . ' - ' . $user->email ,
      ];
      $this->log_activity->create($data_log);

      $this->userMetaModel->where('user_id', $id)->delete();
      $delete = $this->model->where('id', $id)->delete();
      $this->credit->where('user_id', $id)->delete();
      $this->social->where('user_id', $id)->delete();
      $this->verify->where('user_id', $id)->delete();

      if ($delete) {
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }

  public function multidelete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];
      // Xóa tất cả các table có liên quan trên user
      // ghi log
      $users =$this->model->whereIn('id', $id)->get();
      foreach ($users as $key => $user) {
         $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/User',
          'description' => ' khách hàng ' . $user->name . ' - ' . $user->email ,
        ];
        $this->log_activity->create($data_log);
      }

      $this->userMetaModel->whereIn('user_id', $id)->delete();
      $delete = $this->model->whereIn('id', $id)->delete();

      if ($delete) {
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }
    function rand_string($n) {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $n; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    // Nhóm người dùng
    public function loadGroupUser()
    {
        return $this->group_user->orderBy('id', 'desc')->with('users', 'group_products')->get();
    }
    // Tạo group nhóm đại lý
    public function create_group_user($data_create)
    {
        $create = $this->group_user->create($data_create);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/GroupUser',
          'description' => ' nhóm khách hàng <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $create->id . '">' . $create->name . '</a> ',
        ];
        $this->log_activity->create($data_log);

        if ($create) {
           return 'Tạo nhóm đại lý thành công';
        } else {
           return 'Tạo nhóm đại lý thất bại';
        }
    }
    // Chỉnh sửa nhóm đại lý
    public function update_group_user($data_update, $id)
    {
        $update = $this->group_user->find($id)->update($data_update);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/GroupUser',
          'description' => ' nhóm khách hàng <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $id . '">' . $data_update['name'] . '</a> ',
        ];
        $this->log_activity->create($data_log);
        if ($update) {
            return 'Chỉnh sửa nhóm đại lý thành công';
        } else {
            return 'Chỉnh sửa nhóm đại lý thất bại';
        }
    }
    // Xóa nhóm đại lý
    public function delete_group_user($id)
    {
        $group_user = $this->group_user->find($id);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/GroupUser',
          'description' => ' nhóm khách hàng ' . $group_user->name,
        ];
        $this->log_activity->create($data_log);

        foreach ($group_user->users as $key => $user) {
            $user->group_user_id = 0;
            $user->save();
        }
        foreach ($group_user->group_products as $key => $group_product) {
            $group_product->group_user_id = 0;
            $group_product->save();
        }
        $group_user->delete();
        return true;
    }
    // chi tiết nhóm đại lý
    public function detail_group_user($id)
    {
        $group_user = $this->group_user->find($id);
        return $group_user;
    }
    // Lấy những user không có trong group
    public function load_users($group_user_id)
    {
        return $this->model->where('group_user_id', '!=', $group_user_id)->get();
    }
    // update user vào group user
    public function update_user_with_group_user($data)
    {
        $user_ids = $data['user_id'];
        $data = ['group_user_id' => $data['id']];
        $group_user_id = $data['group_user_id'];
        foreach ($user_ids as $key => $user_id) {
            // ghi log
            $user = $this->model->find($user_id);
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'thêm',
              'model' => 'Admin/GroupUser',
              'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> vào nhóm khách hàng <a href="/admin/dai-ly/chi-tiet-dai-ly/' . $group_user_id . '">#' . $group_user_id . '</a> ',
            ];
            $this->log_activity->create($data_log);

            $update = $this->model->find($user_id)->update($data);
            if (!$update) {
                return false;
            }
        }
        return true;
    }
    // Xóa user ra nhóm đại lý
    public function delete_user_with_group_user($user_id)
    {
        $data = ['group_user_id' => 0];
        // ghi log
        $user = $this->model->find($user_id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/GroupUser',
          'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> ra nhóm khách hàng <a href="/admin/dai-ly/chi-tiet-dai-ly/' . $user->group_user->id . '">#' . $user->group_user->name . '</a> ',
        ];
        $this->log_activity->create($data_log);

        $update = $this->model->find($user_id)->update($data);
        return $update;
    }
    // Load nhóm sản phẩm không có trong nhóm đại lý
    public function load_group_products($group_user_id)
    {
        return $this->group_product->where('group_user_id' , '!=' , $group_user_id)->get();
    }
    // update group_product vào group user
    public function update_group_product_with_group_user($data)
    {
        $group_product_ids = $data['group_product_id'];
        $data = ['group_user_id' => $data['id']];
        $group_user_id = $data['group_user_id'];
        foreach ($group_product_ids as $key => $group_product_id) {
            // ghi log
            $group_product = $this->group_product->find($group_product_id);
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'thêm',
              'model' => 'Admin/GroupUser',
              'description' => ' nhóm sản phẩm ' . $group_product->name . ' vào nhóm khách hàng <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $group_user_id . '">#' . $group_user_id . '</a> ',
            ];
            $this->log_activity->create($data_log);
            $update = $this->group_product->find($group_product_id)->update($data);
            if (!$update) {
                return false;
            }
        }
        return true;
    }
    // xáo nhóm sản phẩm ra nhóm đại lý
    public function delete_group_product_with_group_user($group_product_id)
    {
        $data = ['group_user_id' => 0];
        // ghi log
        $group_product = $this->group_product->find($group_product_id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/GroupUser',
          'description' => ' nhóm sản phẩm ' . $group_product->name . ' ra nhóm khách hàng <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $group_product->group_user->id . '">#' . $group_product->group_user->name . '</a> ',
        ];
        $this->log_activity->create($data_log);

        $update = $this->group_product->find($group_product_id)->update($data);
        return $update;
    }
    // Lấy tất cả group user
    public function get_group_users()
    {
        return $this->group_user->get();
    }
    // lấy tất cả group user và cơ bản
    public function get_group_user_with_product()
    {
      $data = [ 'id' => 0, 'name' => 'Cơ bản'];
      $list_group_users = $this->group_user->get();
      foreach ($list_group_users as $key => $group_user) {
        $data[] = [
          'id' => $group_user->id,
          'name' => $group_user->name,
        ];
      }
      return $data;
    }
    // cập nhật số dư tài khoản của user
    public function updateCredit($data)
    {
        $user_credit = $this->credit->where('user_id', $data['user_id'])->first();
        $user_credit->value = $data['user_credit'];
        // ghi log
        $user = $this->model->find($data['user_id']);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cập nhật',
          'model' => 'Admin/GroupUser',
          'description' =>  ' số dư tài khoản ' . number_format($data['user_credit'],0,",",".") . ' VNĐ cho khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> ',
        ];
        $this->log_activity->create($data_log);
        $update = $user_credit->save();
        return $update;
    }
    // thêm cloudzone point cho user
    public function updateCloudzonePoint($data)
    {
        // ghi log
        $user = $this->model->find($data['user_id']);
        $user_meta = $user->user_meta;
        $user_meta->point += $data['cloudzone_point'];
        $user_meta->save();
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'thêm',
          'model' => 'Admin/GroupUser',
          'description' =>  number_format($data['cloudzone_point'],0,",",".") . ' điểm Cloudzone Point cho khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> ',
        ];
        $this->log_activity->create($data_log);
        $update = $user_meta->point;
        return $update;
    }
    // cập nhật chỉnh sửa tổng tiền đã nạp cho user
    public function updateCreditTotal($data)
    {
        $user_credit = $this->credit->where('user_id', $data['user_id'])->first();
        $user_credit->total = $data['user_credit'];
        $update = $user_credit->save();
        // ghi log
        $user = $this->model->find($data['user_id']);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cập nhật',
          'model' => 'Admin/GroupUser',
          'description' =>  'tổng tiền đã nạp ' . number_format($data['user_credit'],0,",",".") . ' VNĐ cho khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> ',
        ];
        $this->log_activity->create($data_log);

        return $update;
    }
    // Active hoặc InActive User
    public function actionUser($data) {
        if ($data['type'] == 'active') {
            $user = $this->model->find($data['id']);
            $user->email_verified_at = date('Y-m-d h:m:s');
            $update = $user->save();
        } else {
            $user = $this->model->find($data['id']);
            $user->email_verified_at = null;
            $update = $user->save();
        }
    }

    public function send_mail_with_user($data)
    {
        $user = $this->detail($data['user_id']);
        $data['name'] = $user->name;
        try {
            $data_tb_send_mail = [
              'type' => 'admin_send_mail',
              'user_id' => $user->id,
              'status' => false,
              'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
            report($e);
            return true;
        }

        return true;

    }

    public function get_all_users()
    {
      return $this->model->get();
    }

    public function create_payment($data)
    {
        $method_pay = $data['method_pay'];
        $user = $this->model->find($data['user_id']);
        if ($method_pay == 'pay_in_office') {
            $ma_gd = 'GDOF' . strtoupper(substr(sha1(time()), 34, 39));
        }
        elseif ($method_pay == 'bank_transfer_vietcombank') {
            $ma_gd = 'GDVC' . strtoupper(substr(sha1(time()), 34, 39));
        }
        elseif ($method_pay == 'bank_transfer_techcombank') {
            $ma_gd = 'GDTC' . strtoupper(substr(sha1(time()), 34, 39));
        }
        else if ($method_pay == 'momo') {
            $ma_gd = 'GDMM' . strtoupper(substr(sha1(time()), 34, 39));
        }
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => $ma_gd,
            'discription' => !empty($data['discription']) ? $data['discription']:'',
            'type_gd' => '1',
            'method_gd' => $data['method_pay'],
            'date_gd' => date('Y-m-d'),
            'money'=> $data['amount'],
            'status' => 'Active',
        ];
        $create = $this->history_pay->create($data_history);

        $credit = $this->credit->where('user_id', $data['user_id'])->first();
        if (isset($credit)) {
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $create->id,
              'before' => $credit->value,
              'after' => $credit->value + $data['amount'],
            ];
            $this->log_payment->create($data_log_payment);
            $credit->value += $data['amount'];
            $credit->total += $data['amount'];
            $credit->save();
        } else {
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $create->id,
              'before' => 0,
              'after' => $data['amount'],
            ];
            $this->log_payment->create($data_log_payment);
            $data_credit = [
                'user_id' => $data['user_id'],
                'value' => $data['amount'],
                'total' => $data['amount'],
            ];
            $this->credit->create($data_credit);
        }
        // Tạo log
        if ($method_pay == 'pay_in_office') {
            $text_ma_gd = 'trực tiếp tại văn phòng';
        }
        elseif ($method_pay == 'bank_transfer_vietcombank') {
            $text_ma_gd = 'Vietcombank';
        }
        elseif ($method_pay == 'bank_transfer_techcombank') {
            $text_ma_gd = 'Techcombank';
        }
        else if ($method_pay == 'momo') {
            $text_ma_gd = 'MoMo';
        }
        $data_log = [
           'user_id' => $user->id,
           'action' => 'Tạo',
           'model' => 'Admin/addPaymentForm',
           'description' => ' nạp tiền vào tài khoản <a target="_blank" href="/admin/payment/detail/' . $create->id . '">#' . $create->ma_gd . '</a> bằng hình thức ' . $text_ma_gd,
        ];
        $this->log_activity->create($data_log);

        try {
          $data = [
              'name' => $user->name,
              'amount' => $create->money,
              'ma_gd' => $create->ma_gd,
              'created_at' => $create->created_at,
              'subject' => 'Nạp tiền vào tài khoản tại CLOUDZONE'
          ];

          $data_tb_send_mail = [
            'type' => 'admin_create_payment',
            'user_id' => $user->id,
            'status' => false,
            'content' => serialize($data),
          ];
          $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
          report($e);
          return $create;
        }
        return $create;
    }

    public function list_agency_api()
    {
        $list_agencies  = $this->agency->with('user')->paginate(30);
        $data = [
          'data' => [],
        ];
        foreach ($list_agencies as $key => $agency) {
            $agency->user_name = !empty($agency->user->name) ? $agency->user->name : '<span class="text-danger">Đã xóa</span>';
            $data['data'][] = $agency;
        }
        $data['total'] = $list_agencies->total();
        $data['perPage'] = $list_agencies->perPage();
        $data['current_page'] = $list_agencies->currentPage();
        return $data;
    }

    public function create_agency($data)
    {
        $data_agency = [
          'user_id' => $data['user_id'],
          'user_api' => $data['user_api'],
          'password_api' => $data['password'],
        ];
        $create = $this->agency->create($data_agency);
        if ($create) {
          return 'Tạo tài khoản API thành công';
        } else {
          return false;
        }
    }

    public function update_agency($data)
    {
        $data_agency = [
          'user_id' => $data['user_id'],
          'user_api' => $data['user_api'],
          'password_api' => $data['password'],
        ];
        $update = $this->agency->where('id', $data['id'])->update($data_agency);
        if ($update) {
          return 'Sửa tài khoản API thành công';
        } else {
          return false;
        }
    }

    public function detail_agency_api($id)
    {
        $agency = $this->agency->find($id);
        $users = $this->model->get();
        $agency->users = $users;
        return $agency;
    }

    public function delete_agency($id)
    {
        $agency = $this->agency->find($id)->delete();
        $delete = '';
        if ($agency) {
          $delete = 'Xóa tài khoản API thành công';
        } else {
          $delete = 'Xóa tài khoản API thất bại';
        }
        return $delete;
    }

    /**
     * Báo giá
     */
    public function list_quotation($q)
    {
        if (!empty($q)) {
          $list_quotation = $this->quotation->where('title' , '%'.$q.'%')->orderBy('id', 'desc')->paginate(20);
        } else {
          $list_quotation = $this->quotation->orderBy('id', 'desc')->paginate(20);
        }
        $data = [
          'data' => []
        ];
        foreach ($list_quotation as $key => $quotation) {
          $quotation->user_name = !empty($quotation->user->name) ? $quotation->user->name : '<span class="text-danger">Đã xóa</span>';
          $quotation->create_date = date( 'd-m-Y H:i:s', strtotime($quotation->created_at) );
          $data['data'][] = $quotation;
        }
        $data['total'] = $list_quotation->total();
        $data['perPage'] = $list_quotation->perPage();
        $data['current_page'] = $list_quotation->currentPage();
        return $data;
    }

    public function list_quotation_by_user($id)
    {
      $list_quotation = $this->quotation->where('user_id', $id)->orderBy('id', 'desc')->paginate(20);
      return $list_quotation;
    }

    public function quotation_detail($id)
    {
        return $this->quotation->find($id);
    }

    public function store_quotation($data)
    {
    
        $billings = [
            'monthly' => 1,
            'twomonthly' => 2,
            'quarterly' => 3,
            'semi_annually' => 6,
            'annually' => 12,
            'biennially' => 24,
            'triennially' => 36,
        ];
        $data_quotation = [
          'title' => $data['title'],
          'user_id' => $data['user_id'],
          'total' => 0,
          'payment_cycle' => !empty($data['payment_cycle']) ? $data['payment_cycle'] : '',
          'name_replace' => !empty($data['name_replace']) ? $data['name_replace'] : '',
          'vat' => !empty($data['vat']) ? true : false,
          'date_create' => date('Y-m-d'),
        ];
        
        $quotation = $this->quotation->create($data_quotation);
        $total = 0;
        foreach ($data['type_product'] as $key => $type_product) {
         
          $os = '';
          if ( !empty($data['os'][$key]) ) {
            if ( $data['os'][$key] != 'other' ) {
              $os = $data['os'][$key];
            } else {
              $os = $data['os_replace'][$key];
            }
          }
          if ( !empty($data['product_id'][$key]) ) {
            $product = $this->product->find($data['product_id'][$key]);
            if ( $product->type_product == 'Server' || $product->type_product == 'Colocation' ) {
              $data_meta = [];
              $data_meta = [
                'quotation_id' => $quotation->id,
                'type_product' => $type_product,
                'product_id' => !empty($data['product_id'][$key]) ? $data['product_id'][$key] : 0,
                'addon_cpu' => !empty($data['addon_cpu'][$key]) ? $data['addon_cpu'][$key] : 0,
                'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
                'addon_disk' => !empty($data['addon_disk'][$key]) ? $data['addon_disk'][$key] : 0,
                'qtt_ip' => !empty($data['qtt_ip'][$key]) ? $data['qtt_ip'][$key] : 0,
                'qtt' => !empty($data['qtt'][$key]) ? $data['qtt'][$key] : 0,
                'os' => $os,
                'billing_cycle' => !empty($data['billing_cycle'][$key]) ? $data['billing_cycle'][$key] : 'monthly',
                'price_billing_cycle' => !empty($data['price_billing_cycle'][$key]) ? $data['price_billing_cycle'][$key] : 'monthly',
                'domain' => !empty($data['domain'][$key]) ? $data['domain'][$key] : '',
                'amount' => !empty($data['amount'][$key]) ? $data['amount'][$key] : 0,
                'start_date' => !empty($data['start_date'][$key]) ? date('Y-m-d', strtotime($data['start_date'][$key])) : '',
                'end_date' => !empty($data['end_date'][$key]) ? date('Y-m-d', strtotime($data['end_date'][$key])) : '',
                'datacenter' => !empty($data['datacenter'][$key]) ? $data['datacenter'][$key] : 1,
              ];
              $quotation_detail = $this->quotation_detail->create($data_meta);
              $biling_meta = !empty($billings[$data_meta['billing_cycle']]) ? $billings[$data_meta['billing_cycle']] : 1;
              $price_billing_cycle_meta = !empty($billings[$data_meta['price_billing_cycle']]) ? $billings[$data_meta['price_billing_cycle']] : 1;
              $total += $data['amount'][$key];
            
              $data_addon_server = [
                'quotation_detail_id' => $quotation_detail->id,
                'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
                'addon_ip' => !empty($data['addon_ip'][$key]) ? $data['addon_ip'][$key] : 0,
                'addon_disk2' => !empty($data['addon_disk2'][$key]) ? $data['addon_disk2'][$key] : 0,
                'addon_disk3' => !empty($data['addon_disk3'][$key]) ? $data['addon_disk3'][$key] : 0,
                'addon_disk4' => !empty($data['addon_disk4'][$key]) ? $data['addon_disk4'][$key] : 0,
                'addon_disk5' => !empty($data['addon_disk5'][$key]) ? $data['addon_disk5'][$key] : 0,
                'addon_disk6' => !empty($data['addon_disk6'][$key]) ? $data['addon_disk6'][$key] : 0,
                'addon_disk7' => !empty($data['addon_disk7'][$key]) ? $data['addon_disk7'][$key] : 0,
                'addon_disk8' => !empty($data['addon_disk8'][$key]) ? $data['addon_disk8'][$key] : 0,
              ];
              $this->quotation_server->create($data_addon_server);
            
            }
             else {
              $data_meta = [];
              $data_meta = [
                'quotation_id' => $quotation->id,
                'type_product' => $type_product,
                'product_id' => !empty($data['product_id'][$key]) ? $data['product_id'][$key] : 0,
                'addon_cpu' => !empty($data['addon_cpu'][$key]) ? $data['addon_cpu'][$key] : 0,
                'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
                'addon_disk' => !empty($data['addon_disk'][$key]) ? $data['addon_disk'][$key] : 0,
                'qtt_ip' => !empty($data['qtt_ip'][$key]) ? $data['qtt_ip'][$key] : 0,
                'qtt' => !empty($data['qtt'][$key]) ? $data['qtt'][$key] : 0,
                'os' => $os,
                'billing_cycle' => !empty($data['billing_cycle'][$key]) ? $data['billing_cycle'][$key] : 'monthly',
                'price_billing_cycle' => !empty($data['price_billing_cycle'][$key]) ? $data['price_billing_cycle'][$key] : 'monthly',
                'domain' => !empty($data['domain'][$key]) ? $data['domain'][$key] : '',
                'amount' => !empty($data['amount'][$key]) ? $data['amount'][$key] : 0,
                'start_date' => !empty($data['start_date'][$key]) ? date('Y-m-d', strtotime($data['start_date'][$key])) : '',
                'end_date' => !empty($data['end_date'][$key]) ? date('Y-m-d', strtotime($data['end_date'][$key])) : '',
              ];
              $this->quotation_detail->create($data_meta);
              $biling_meta = !empty($billings[$data_meta['billing_cycle']]) ? $billings[$data_meta['billing_cycle']] : 1;
              $price_billing_cycle_meta = !empty($billings[$data_meta['price_billing_cycle']]) ? $billings[$data_meta['price_billing_cycle']] : 1;
              $total += $data['amount'][$key];
            }
          } else {
            $data_meta = [];
            $data_meta = [
              'quotation_id' => $quotation->id,
              'type_product' => $type_product,
              'product_id' => !empty($data['product_id'][$key]) ? $data['product_id'][$key] : 0,
              'addon_cpu' => !empty($data['addon_cpu'][$key]) ? $data['addon_cpu'][$key] : 0,
              'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
              'addon_disk' => !empty($data['addon_disk'][$key]) ? $data['addon_disk'][$key] : 0,
              'qtt_ip' => !empty($data['qtt_ip'][$key]) ? $data['qtt_ip'][$key] : 0,
              'qtt' => !empty($data['qtt'][$key]) ? $data['qtt'][$key] : 0,
              'os' => $os,
              'billing_cycle' => !empty($data['billing_cycle'][$key]) ? $data['billing_cycle'][$key] : 'monthly',
              'price_billing_cycle' => !empty($data['price_billing_cycle'][$key]) ? $data['price_billing_cycle'][$key] : 'monthly',
              'domain' => !empty($data['domain'][$key]) ? $data['domain'][$key] : '',
              'amount' => !empty($data['amount'][$key]) ? $data['amount'][$key] : 0,
              'start_date' => !empty($data['start_date'][$key]) ? date('Y-m-d', strtotime($data['start_date'][$key])) : '',
              'end_date' => !empty($data['end_date'][$key]) ? date('Y-m-d', strtotime($data['end_date'][$key])) : '',
            ];
            $this->quotation_detail->create($data_meta);
            $biling_meta = !empty($billings[$data_meta['billing_cycle']]) ? $billings[$data_meta['billing_cycle']] : 1;
            $price_billing_cycle_meta = !empty($billings[$data_meta['price_billing_cycle']]) ? $billings[$data_meta['price_billing_cycle']] : 1;
            $total += $data['amount'][$key];
          }
        }
        if ( !empty($data['vat']) ) {
          $total += $total/10;
        }
        $text_total = $this->convert_number_to_words($total);
        $quotation->total = $total;
        $quotation->text_total = $text_total;
        // dd($total, $text_total);
        return $quotation->save();
    }

    public function update_quotation($data)
    {
        // dd($data);
        $billings = [
            'monthly' => 1,
            'twomonthly' => 2,
            'quarterly' => 3,
            'semi_annually' => 6,
            'annually' => 12,
            'biennially' => 24,
            'triennially' => 36,
        ];
        $quotation = $this->quotation->find($data['quotation_id']);
        $data_quotation = [
          'title' => $data['title'],
          'user_id' => $data['user_id'],
          'total' => 0,
          'name_replace' => !empty($data['name_replace']) ? $data['name_replace'] : '',
          'payment_cycle' => !empty($data['payment_cycle']) ? $data['payment_cycle'] : '',
          'vat' => !empty($data['vat']) ? true : false,
        ];
      
        $quotation->update($data_quotation);
        foreach ($quotation->quotation_details as $key => $quotation_detail) {
          $this->quotation_server->where('quotation_detail_id', $quotation_detail->id)->delete();
          $quotation_detail->delete();
        }
        $total = 0;
        foreach ($data['type_product'] as $key => $type_product) {
          $os = '';
          $product = $this->product->find($data['product_id'][$key]);
          if ( !empty($data['os'][$key]) ) {
            if ( $data['os'][$key] != 'other' ) {
              $os = $data['os'][$key];
            } else {
              $os = $data['os_replace'][$key];
            }
          }
          $data_meta = [];
          $data_meta = [
            'quotation_id' => $quotation->id,
            'type_product' => $type_product,
            'product_id' => !empty($data['product_id'][$key]) ? $data['product_id'][$key] : 0,
            'addon_cpu' => !empty($data['addon_cpu'][$key]) ? $data['addon_cpu'][$key] : 0,
            'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
            'addon_disk' => !empty($data['addon_disk'][$key]) ? $data['addon_disk'][$key] : 0,
            'qtt_ip' => !empty($data['qtt_ip'][$key]) ? $data['qtt_ip'][$key] : 0,
            'qtt' => !empty($data['qtt'][$key]) ? $data['qtt'][$key] : 0,
            'os' => $os,
            'billing_cycle' => !empty($data['billing_cycle'][$key]) ? $data['billing_cycle'][$key] : 'monthly',
            'price_billing_cycle' => !empty($data['price_billing_cycle'][$key]) ? $data['price_billing_cycle'][$key] : 'monthly',
            'domain' => !empty($data['domain'][$key]) ? $data['domain'][$key] : '',
            'amount' => !empty($data['amount'][$key]) ? $data['amount'][$key] : 0,
            'start_date' => !empty($data['start_date'][$key]) ? date('Y-m-d', strtotime($data['start_date'][$key])) : '',
            'end_date' => !empty($data['end_date'][$key]) ? date('Y-m-d', strtotime($data['end_date'][$key])) : '',
          ];
          $biling_meta = !empty($billings[$data_meta['billing_cycle']]) ? $billings[$data_meta['billing_cycle']] : 1;
          $price_billing_cycle_meta = !empty($billings[$data_meta['price_billing_cycle']]) ? $billings[$data_meta['price_billing_cycle']] : 1;
          $quotation_detail = $this->quotation_detail->create($data_meta);
          $total += $data['amount'][$key] * $data['qtt'][$key] * $biling_meta / $price_billing_cycle_meta;
          if ( $product->type_product == 'Server' ) {
            $data_addon_server = [
              'quotation_detail_id' => $quotation_detail->id,
              'addon_ram' => !empty($data['addon_ram'][$key]) ? $data['addon_ram'][$key] : 0,
              'addon_ip' => !empty($data['addon_ip'][$key]) ? $data['addon_ip'][$key] : 0,
              'addon_disk2' => !empty($data['addon_disk2'][$key]) ? $data['addon_disk2'][$key] : 0,
              'addon_disk3' => !empty($data['addon_disk3'][$key]) ? $data['addon_disk3'][$key] : 0,
              'addon_disk4' => !empty($data['addon_disk4'][$key]) ? $data['addon_disk4'][$key] : 0,
              'addon_disk5' => !empty($data['addon_disk5'][$key]) ? $data['addon_disk5'][$key] : 0,
              'addon_disk6' => !empty($data['addon_disk6'][$key]) ? $data['addon_disk6'][$key] : 0,
              'addon_disk7' => !empty($data['addon_disk7'][$key]) ? $data['addon_disk7'][$key] : 0,
              'addon_disk8' => !empty($data['addon_disk8'][$key]) ? $data['addon_disk8'][$key] : 0,
            ];
            $this->quotation_server->create($data_addon_server);
          }
        }
        if ( !empty($data['vat']) ) {
          $total += $total/10;
        }
        $text_total = $this->convert_number_to_words($total);  
        $quotation->total = $total;
        $quotation->text_total = $text_total;
        return $quotation->save();
    }

    public function delete_quotation($id)
    {
        return $this->quotation->find($id)->delete();
    }

    public function get_product_with_quotation($quotation_detail)
    {
      // $data = [];
      $billingDashBoard = config('billingDashBoard');
      $product = $this->product->find( $quotation_detail->product_id );
      $billing_cycle = $quotation_detail->billing_cycle;
      $product->amount = $product->pricing[$billing_cycle];
      $product->ip = $product->meta_product->ip;

      $total = !empty( $product->pricing[$billing_cycle] ) ? $product->pricing[$billing_cycle] : 0;
      if ( !empty($quotation_detail->quotation_server) ) {
        $quotation_server = $quotation_detail->quotation_server;
        if ( !empty( $quotation_server->addon_ram ) ) {
            $productAddonRam = $this->product->find($quotation_server->addon_ram);
            $total += !empty($productAddonRam->pricing[$billing_cycle]) ? $productAddonRam->pricing[$billing_cycle] : 0;
        }
        $product->productAddonRam = $quotation_server->addon_ram;
        if ( !empty( $quotation_server->addon_disk2 ) ) {
            $productDisk2 = $this->product->find($quotation_server->addon_disk2);
            $total += !empty($productDisk2->pricing[$billing_cycle]) ? $productDisk2->pricing[$billing_cycle] : 0;
        }
        $product->productDisk2 = $quotation_server->addon_disk2;
        if ( !empty( $quotation_server->addon_disk3 ) ) {
            $productDisk3 = $this->product->find($quotation_server->addon_disk3);
            $total += !empty($productDisk3->pricing[$billing_cycle]) ? $productDisk3->pricing[$billing_cycle] : 0;
        }
        $product->productDisk3 = $quotation_server->addon_disk3;
        if ( !empty( $quotation_server->addon_disk4 ) ) {
            $productDisk4 = $this->product->find($quotation_server->addon_disk4);
            $total += !empty($productDisk4->pricing[$billing_cycle]) ? $productDisk4->pricing[$billing_cycle] : 0;
        }
        $product->productDisk4 = $quotation_server->addon_disk4;
        if ( !empty( $quotation_server->addon_disk5 ) ) {
            $productDisk5 = $this->product->find($quotation_server->addon_disk5);
            $total += !empty($productDisk5->pricing[$billing_cycle]) ? $productDisk5->pricing[$billing_cycle] : 0;
        }
        $product->productDisk5 = $quotation_server->addon_disk5;
        if ( !empty( $quotation_server->addon_disk6 ) ) {
            $productDisk6 = $this->product->find($quotation_server->addon_disk6);
            $total += !empty($productDisk6->pricing[$billing_cycle]) ? $productDisk6->pricing[$billing_cycle] : 0;
        }
        $product->productDisk6 = $quotation_server->addon_disk6;
        if ( !empty( $quotation_server->addon_disk7 ) ) {
            $productDisk7 = $this->product->find($quotation_server->addon_disk7);
            $total += !empty($productDisk7->pricing[$billing_cycle]) ? $productDisk7->pricing[$billing_cycle] : 0;
        }
        $product->productDisk7 = $quotation_server->addon_disk7;
        if ( !empty( $quotation_server->addon_disk8 ) ) {
            $productDisk8 = $this->product->find($quotation_server->addon_disk8);
            $total += !empty($productDisk8->pricing[$billing_cycle]) ? $productDisk8->pricing[$billing_cycle] : 0;
        }
        $product->productDisk8 = $quotation_server->addon_disk8;
        if ( !empty( $quotation_server->addon_ip ) ) {
            $productAddonIp = $this->product->find($quotation_server->addon_ip);
            $total += $productAddonIp->pricing[$billing_cycle];
        }
        $product->productAddonIp = $quotation_server->addon_ip;
      } else {
        $product->productAddonRam = 0;
        $product->productDisk2 = 0;
        $product->productDisk3 = 0;
        $product->productDisk4 = 0;
        $product->productDisk5 = 0;
        $product->productDisk6 = 0;
        $product->productDisk7 = 0;
        $product->productDisk8 = 0;
        $product->productAddonIp = 0;
      }
      $product->amount = $total;
      // cấu hình
      $config = '';
      $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
      $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
      $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
      $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
      $config = 'CPU: ' . $cpu . ' (' . $cores . ')<br>RAM: ' . $ram . '<br>DISK: ' . $disk;
      $product->config = $config;
      $product->second = !empty($product->product_drive->second) ? 1 : 0;
      // foreach ($quotation->quotation_details as $key => $quotation_detail) {
      //   $product = $this->product->find( $quotation_detail->product_id );
      //   $billing_cycle = $quotation_detail->billing_cycle;
      //   $product->amount = $product->pricing[$billing_cycle];
      //   $product->ip = $product->meta_product->ip;

      //   $total = !empty( $product->pricing[$billing_cycle] ) ? $product->pricing[$billing_cycle] : 0;
      //   if ( !empty($quotation_detail->quotation_server) ) {
      //     $quotation_server = $quotation_detail->quotation_server;
      //     if ( !empty( $quotation_server->addon_ram ) ) {
      //         $productAddonRam = $this->product->find($quotation_server->addon_ram);
      //         $total += !empty($productAddonRam->pricing[$billing_cycle]) ? $productAddonRam->pricing[$billing_cycle] : 0;
      //         $product->productAddonRam = $productAddonRam;
      //     }
      //     if ( !empty( $quotation_server->addon_disk2 ) ) {
      //         $productDisk2 = $this->product->find($quotation_server->addon_disk2);
      //         $total += !empty($productDisk2->pricing[$billing_cycle]) ? $productDisk2->pricing[$billing_cycle] : 0;
      //         $product->productDisk2 = $productDisk2;
      //     }
      //     if ( !empty( $quotation_server->addon_disk3 ) ) {
      //         $productDisk3 = $this->product->find($quotation_server->addon_disk3);
      //         $total += !empty($productDisk3->pricing[$billing_cycle]) ? $productDisk3->pricing[$billing_cycle] : 0;
      //         $product->productDisk3 = $productDisk3;
      //     }
      //     if ( !empty( $quotation_server->addon_disk4 ) ) {
      //         $productDisk4 = $this->product->find($quotation_server->addon_disk4);
      //         $total += !empty($productDisk4->pricing[$billing_cycle]) ? $productDisk4->pricing[$billing_cycle] : 0;
      //         $product->productDisk4 = $productDisk4;
      //     }
      //     if ( !empty( $quotation_server->addon_disk5 ) ) {
      //         $productDisk5 = $this->product->find($quotation_server->addon_disk5);
      //         $total += !empty($productDisk5->pricing[$billing_cycle]) ? $productDisk5->pricing[$billing_cycle] : 0;
      //         $product->productDisk5 = $productDisk5;
      //     }
      //     if ( !empty( $quotation_server->addon_disk6 ) ) {
      //         $productDisk6 = $this->product->find($quotation_server->addon_disk6);
      //         $total += !empty($productDisk6->pricing[$billing_cycle]) ? $productDisk6->pricing[$billing_cycle] : 0;
      //         $product->productDisk6 = $productDisk6;
      //     }
      //     if ( !empty( $quotation_server->addon_disk7 ) ) {
      //         $productDisk7 = $this->product->find($quotation_server->addon_disk7);
      //         $total += !empty($productDisk7->pricing[$billing_cycle]) ? $productDisk7->pricing[$billing_cycle] : 0;
      //         $product->productDisk7 = $productDisk7;
      //     }
      //     if ( !empty( $quotation_server->addon_disk8 ) ) {
      //         $productDisk8 = $this->product->find($quotation_server->addon_disk8);
      //         $total += !empty($productDisk8->pricing[$billing_cycle]) ? $productDisk8->pricing[$billing_cycle] : 0;
      //         $product->productDisk8 = $productDisk8;
      //     }
      //     if ( !empty( $quotation_server->addon_ip ) ) {
      //         $productAddonIp = $this->product->find($quotation_server->addon_ip);
      //         $total += $productAddonIp->pricing[$billing_cycle];
      //         $product->productAddonIp = $productAddonIp;
      //     }
      //   }
      //   $product->amount = $total;
      //   // cấu hình
      //   $config = '';
      //   $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
      //   $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
      //   $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
      //   $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
      //   $config = 'CPU: ' . $cpu . ' (' . $cores . ')<br>RAM: ' . $ram . '<br>DISK: ' . $disk;
      //   $product->config = $config;
      //   $product->second = !empty($product->product_drive->second) ? 1 : 0;
      //   $data[$key] = $product;
      // }
      return $product;
    }

    function convert_number_to_words($number) {
      // dd($number);
      $convert = [
          0 => 'không',
          1 => 'một',
          2 => 'hai',
          3 => 'ba',
          4 => 'bốn',
          5 => 'năm',
          6 => 'sáu',
          7 => 'bảy',
          8 => 'tám',
          9 => 'chín',
          10 => 'mười',
          11 => 'mười một',
          12 => 'mười hai',
          13 => 'mười ba',
          14 => 'mười bốn',
          15 => 'mười lăm',
          16 => 'mười sáu',
          17 => 'mười bảy',
          18 => 'mười tám',
          19 => 'mười chín',
      ];
      $term = $number;
      $string = '';
          // number lớn hơn 1 tỷ
      if ( $number > 999999999 ) {
          $sub_string = '';
          $sub_number = (int) floor( $number / 1000000000 );
          $number %= 1000000000;
            // sub_number lớn hơn 100
          if ( $sub_number > 100 ) {
              $number_h = $sub_number;
              $sub_number_h = (int) floor( $sub_number / 100 );
              $number_h %= 100;
              $sub_string = $convert[$sub_number_h] . ' trăm ';
              // sub_number_h lớn hơn 10
              if ( $number_h > 20 ) {
                  $number_0 = $number_h;
                  $sub_number_t = (int) floor( $number_h / 10 );
                  $number_0 %= 10;
                  $sub_string .= $convert[$sub_number_t];
                  $sub_string .= ' mươi ';
                  if ( $number_0 > 0 ) {
                      $sub_string .= $convert[$number_0];
                      $sub_string .= ' ';
                  }
              } else {
                  if ( $sub_number_t > 0 && $sub_number_t < 10 ) {
                      $sub_string .= 'lẻ ';
                      $sub_string .= $convert[$sub_number_t];
                      $sub_string .= ' ';
                  } else {
                      $sub_string .= $convert[$sub_number_t];
                      $sub_string .= ' ';
                  }
              }
              $string = $sub_string . 'tỷ ';
          }
          elseif ( $sub_number > 10 ) {
              $number_0 = $sub_number;
              $sub_number_t = (int) floor( $sub_number / 10 );
              $number_0 %= 10;
              $sub_string .= $convert[$sub_number_t];
              $sub_string .= ' mươi ';
              if ( $number_0 ) {
                  $sub_string .= $convert[$number_0];
                  $sub_string .= ' ';
              }
              $string = $sub_string . 'tỷ ';
          }
          else {
              $sub_string .= $convert[$sub_number];
              $sub_string .= ' ';
              $string = $sub_string . 'tỷ ';
          }
      }
          // number lớn hơn 1 triệu
      if ( $number > 999999 ) {
          $sub_string = '';
          $sub_number = (int) floor( $number / 1000000 );
          $number %= 1000000;
            // sub_number lớn hơn 100
          if ( $sub_number >= 100 ) {
              $number_h = $sub_number;
              $sub_number_h = (int) floor( $sub_number / 100 );
              $number_h %= 100;
              $sub_string = $convert[$sub_number_h] . ' trăm ';
              // sub_number_h lớn hơn 10
              if ($number_h) {
                if ( $number_h > 10 ) {
                  $number_0 = $number_h;
                  $sub_number_t = (int) floor( $number_h / 10 );
                  $number_0 %= 10;
                  $sub_string .= $convert[$sub_number_t];
                  $sub_string .= ' mươi ';
                  if ( $number_0 > 0 ) {
                      $sub_string .= $convert[$number_0];
                      $sub_string .= ' ';
                  }
                } else {
                    if ($sub_number_t > 0 && $sub_number_t < 10 ) {
                        $sub_string .= 'lẻ ';
                        $sub_string .= $convert[$sub_number_t];
                        $sub_string .= ' ';
                    } else {
                        $sub_string .= $convert[$sub_number_t];
                        $sub_string .= ' ';
                    }
                }
              }
              $string .= $sub_string . 'triệu ';
          }
          elseif ( $sub_number > 20 ) {
              $number_0 = $sub_number;
              $sub_number_t = (int) floor( $sub_number / 10 );
              $number_0 %= 10;
              $sub_string .= $convert[$sub_number_t];
              $sub_string .= ' mươi ';
              if ( $number_0 > 0 ) {
                  $sub_string .= $convert[$number_0];
                  $sub_string .= ' ';
              }
              $string .= $sub_string . 'triệu ';
          }
          else {
              if ( $term < 10000000 ) {
                  $sub_string .= $convert[$sub_number];
                  $sub_string .= ' ';
                  $string .= $sub_string . 'triệu ';
              } else {
                  if ( $sub_number > 0 ) {
                      $sub_string .= $convert[$sub_number];
                      $sub_string .= ' ';
                      $string .= $sub_string . 'triệu ';
                  }
              }
              
          }
      }
          // number lớn hơn 1 ngàn
      if ( $number > 999 ) {
          $sub_string = '';
          $sub_number = (int) floor( $number / 1000 );
          $number %= 1000;
            // sub_number lớn hơn 100
          if ( $sub_number >= 100 ) {
              $number_h = $sub_number;
              $sub_number_h = (int) floor( $sub_number / 100 );
              $number_h %= 100;
              $sub_string = $convert[$sub_number_h] . ' trăm ';
              // sub_number_h lớn hơn 10
              if ($number_h) {
                if ( $number_h > 19 ) {
                    $number_0 = $number_h;
                    $sub_number_t = (int) floor( $number_h / 10 );
                    $number_0 %= 10;
                    $sub_string .= $convert[$sub_number_t];
                    $sub_string .= ' mươi ';
                    if ( $number_0 > 0 ) {
                        $sub_string .= $convert[$number_0];
                        $sub_string .= ' ';
                    }
                } else {
                    if ($number_h) {
                        $sub_string .= ' lẻ ';
                        $sub_string .= $convert[$number_h];
                        $sub_string .= ' ';
                    }
                }
              }
              $string .= $sub_string . 'ngàn ';
          }
          elseif ( $sub_number > 20 ) {
              $number_0 = $sub_number;
              $sub_number_t = (int) floor( $sub_number / 10 );
              $number_0 %= 10;
              $sub_string .= $convert[$sub_number_t];
              $sub_string .= ' mươi ';
              if ( $number_0 > 0 ) {
                  $sub_string .= $convert[$number_0];
                  $sub_string .= ' ';
              }
              $string .= $sub_string . 'ngàn ';
          }
          else {
              $sub_string .= $convert[$sub_number];
              $sub_string .= ' ';
              $string .= $sub_string . 'ngàn ';
          }
      }
          // dd($number);
          // number lớn hơn bé hơn 1000
      if ( $number < 1000 &&  $number > 0) {
          $sub_string = '';
          $sub_number = $number;
            // sub_number lớn hơn 100
          if ( $sub_number >= 100 ) {
              $number_h = $sub_number;
              $sub_number_h = (int) floor( $sub_number / 100 );
              $number_h %= 100;
              $sub_string = $convert[$sub_number_h] . ' trăm ';
              // sub_number_h lớn hơn 10
              if ($number_h) {
                if ( $number_h > 10 ) {
                    $number_0 = $sub_number;
                    $sub_number_t = (int) floor( $number_h / 10 );
                    $number_0 %= 10;
                    $sub_string .= $convert[$sub_number_t];
                    $sub_string .= ' mươi ';
                    if ( $number_0 ) {
                        $sub_string .= $convert[$number_0];
                        $sub_string .= ' ';
                    }
                } else {
                    if ( $number_h ) {
                        $sub_string .= ' lẻ ';
                        $sub_string .= $convert[$number_h];
                        $sub_string .= ' ';
                    }
                }
              }
              $string .= $sub_string;
          }
          elseif ( $sub_number > 20 ) {
              $number_0 = $sub_number;
              $sub_number_t = (int) floor( $sub_number / 10 );
              $number_0 %= 10;
              $sub_string .= $convert[$sub_number_t];
              $sub_string .= ' mươi ';
              if ( $number_0 ) {
                  $sub_string .= $convert[$number_0];
                  $sub_string .= ' ';
              }
              $string .= $sub_string;
          }
          else {
              if ( $sub_number ) {
                  $sub_string .= $convert[$sub_number];
                  $sub_string .= ' ';
                  $string .= $sub_string;
              }
          }
      }
      
      return ucfirst($string)  . 'đồng';
    }

}
