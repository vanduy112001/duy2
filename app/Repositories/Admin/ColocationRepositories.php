<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\LogActivity;
use App\Model\Colocation;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\HistoryPay;
use App\Model\ColocationConfig;
use App\Model\ColocationIp;
use App\Model\GroupProduct;
use App\Model\ColocationConfigIp;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ColocationRepositories {

    protected $user;
    protected $colo;
    protected $log;
    protected $order;
    protected $detail_order;
    protected $history_pay;
    protected $colocation_config;
    protected $colocation_config_id;
    protected $colocation_ip;
    protected $group_product;

    public function __construct()
    {
        $this->user = new User;
        $this->colo = new Colocation;
        $this->log = new LogActivity;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->history_pay = new HistoryPay;
        $this->colocation_config = new ColocationConfig;
        $this->colocation_ip = new ColocationIp;
        $this->colocation_config_id = new ColocationConfigIp;
        $this->group_product = new GroupProduct;
    }

    public function list_colocations()
    {
        $list_colos = $this->colo->orderBy('id', 'desc')->get();
        return $list_colos;
    }

    public function detail($id)
    {
        $colocation = $this->colo->find($id);
        return $colocation;
    }

    public function delete($id)
    {
        $colo = $this->colo->find($id);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'xóa',
            'model' => 'Admin/Colocation',
            'description' => ' Colocation ' . $colo->ip,
            'service' =>  $colo->ip,
        ];
        $this->log->create($data_log);
        if ( $colo->status == 'Active' ) {
            $colo->status_colo = 'delete_colo';
            // $colo->colo_id = -1;
            $delete = $colo->save();
        } else {
            $delete = $this->colo->where('id', $id)->delete();
        }
        return $delete;
    }

    public function update($data)
    {
        try {
            $user = $this->user->find($data['user_id']);
            $colocation = $this->colo->find($data['id']);
            // dd($data , $data['next_due_date'] , date('Y-m-d', strtotime($data['next_due_date'])));
            $data_update = [
              'user_id' => $data['user_id'],
              'type_colo' => $data['type_colo'],
              'ip' => $data['ip'],
              'status_colo' => $data['status_colo'],
              'billing_cycle' => $data['billing_cycle'],
              'next_due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
              'date_create' => date('Y-m-d H:i:s', strtotime($data['created_at'])),
              'location' => $data['location'],
              'bandwidth' => $data['bandwidth'],
              'power' => $data['power'],
              'rack' => $data['rack'],
              'product_id' => $data['product_id'],
            ];
            if ( !empty($data['amount']) ) {
                // ghi log
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'chỉnh sửa',
                    'model' => 'Admin/Colocation',
                    'description' => ' giá áp tay Colocation ' . $colocation->ip,
                    'service' =>  $colocation->ip,
                ];
                $this->log->create($data_log);
    
                $data_update['amount'] = $data['amount'];
            }
            $update = $colocation->update($data_update);
            if ( !empty($data['addIp']) ) {
                if ( count($data['addIp']) ) {
                    $n = 0;
                    $this->colocation_ip->where('colocation_id', $colocation->id)->delete();
                    foreach ($data['addIp'] as $key => $addIp) {
                        if ( !empty($addIp) ) {
                            $n++;
                            $data_add_ip = [
                                'colocation_id' => $colocation->id,
                                'ip' => $addIp
                            ];
                            $this->colocation_ip->create($data_add_ip);
                        }
                    }
                    if ( empty($detail->colocation_config_ips) ) {
                        if ( $n ) {
                            $colocation_config = $colocation->colocation_config;
                            if ( !empty($colocation_config) ) {
                                $colocation_config->ip = $n;
                                $colocation_config->save();
                            } else {
                                $data_config = [
                                    'colocation_id' => $colocation->id,
                                    'ip' => $n
                                ];
                                $this->colocation_config->create($data_config);
                            }
                        }
                    }
                }
                // ghi log
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'chỉnh sửa',
                    'model' => 'Admin/Colocation',
                    'description' => ' ip addon Colocation ' . $colocation->ip,
                    'service' =>  $colocation->ip,
                ];
                $this->log->create($data_log);
            }
            if ( !empty( $data['product_addon_ip'] ) ) {
                foreach ($data['product_addon_ip'] as $key => $product_addon_ip) {
                    $colocation_config_ip = $this->colocation_ip->find($data['colocation_config_ip'][$key]);
                    if ( $product_addon_ip == 0 ) {
                        if ( !empty($colocation->colocation_config) ) {
                            $colocation_config = $colocation->colocation_config;
                            $colocation_config->ip -= !empty($colocation_config_ip->product->meta_product->ip) ? 
                                $colocation_config_ip->product->meta_product->ip : 0;
                            $colocation_config->save();
                        }
                        $colocation_config_ip->delete();
                        // ghi log
                        $data_log = [
                            'user_id' => Auth::user()->id,
                            'action' => 'xóa',
                            'model' => 'Admin/Colocation',
                            'description' => ' gói addon ip của Colocation ' . $colocation->ip,
                            'service' =>  $colocation->ip,
                        ];
                        $this->log->create($data_log);
                    }
                    else {
                        if ( $product_addon_ip ==  $data['colocation_config_ip'][$key]) {
                            $product = $this->product->find($product_addon_ip);
                            if ( !empty($colocation->colocation_config) ) {
                                $colocation_config = $colocation->colocation_config;
                                $colocation_config->ip -= !empty($product->meta_product->ip) ? 
                                    $product->meta_product->ip : 0;
                                $colocation_config->save();
                            }
                            $colocation_config_ip->product_id = $product_addon_ip;
                            $colocation_config_ip->save();
                            // ghi log
                            $data_log = [
                                'user_id' => Auth::user()->id,
                                'action' => 'chỉnh sửa',
                                'model' => 'Admin/Colocation',
                                'description' => ' gói addon ip của Colocation ' . $colocation->ip,
                                'service' =>  $colocation->ip,
                            ];
                            $this->log->create($data_log);
                        }
                    }
                }
            }
            if ( !empty( $data['add_product_addon_ip'] ) ) {
                foreach ($data['add_product_addon_ip'] as $key => $add_product_addon_ip) {
                    if ( $add_product_addon_ip ) {
                        $product = $this->product->find($add_product_addon_ip);
                        $data_colocation_config_ip = [
                            'colocation_id' => $colocation->id,
                            'product_id' => $add_product_addon_ip
                        ];
                        $this->colocation_config_id->create($data_colocation_config_ip);
                        if ( !empty($colocation->colocation_config) ) {
                            $colocation_config = $colocation->colocation_config;
                            $colocation_config->ip += !empty($product->meta_product->ip) ? $product->meta_product->ip : 0;
                            $colocation_config->save();
                        } else {
                            $data_config = [
                                'colocation_id' => $colocation->id,
                                'ip' => !empty($product->meta_product->ip) ? $product->meta_product->ip : 0,
                            ];
                            $this->colocation_config->create($data_config);
                        }
                        // ghi log
                        $data_log = [
                            'user_id' => Auth::user()->id,
                            'action' => 'thêm',
                            'model' => 'Admin/Colocation',
                            'description' => ' gói addon ip cho Colocation ' . $colocation->ip,
                            'service' =>  $colocation->ip,
                        ];
                        $this->log->create($data_log);
                    }
                }
            }
            return $update;
        } catch (\Throwable $th) {
            report($data);
            Log::info( json_encode($data) );
            return false;
        }
    }

    public function store($data)
    {
        $due_date = $data['next_due_date'];
        // dd($due_date);
        $user = $this->user->find($data['user_id']);
        $data_order = [
            'user_id' => $data['user_id'],
            'total' => $data['amount'],
            'status' => 'Finish',
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $create_order = $this->order->create($data_order);
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $date = date('Y-m-d');

        $data_order_detail = [
            'order_id' => $create_order->id,
            'type' => 'Colocation',
            'due_date' => date('Y-m-d', strtotime($due_date)),
            'description' => !empty($data['description'])? $data['description'] : '',
            'payment_method' => 1,
            'status' => 'paid',
            'sub_total' => $data['amount'],
            'quantity' => 1,
            'user_id' => $data['user_id'],
            'paid_date' => date('Y-m-d'),
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        // history_pay
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODC' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $data['amount'],
            'status' => 'Active',
            'detail_order_id' => $detail_order->id,
            'method_gd_invoice' => 'credit',
        ];
        $this->history_pay->create($data_history);

        $data_colocation = [
            'detail_order_id' => $detail_order->id,
            'user_id' => $data['user_id'],
            'ip' => !empty($data['ip'])? $data['ip'] : '',
            'next_due_date' => date('Y-m-d', strtotime($due_date)),
            'location' => $data['location'],
            'amount' => $data['amount'],
            'billing_cycle' => $data['billing_cycle'],
            'status' => 'Active',
            'status_colo' => $data['status_colo'],
            'paid' => 'paid',
            'date_create' => date('Y-m-d h:i:s', strtotime($data['created_at'])),
            'type_colo' => $data['type_colo'],
            'bandwidth' => $data['bandwidth'],
            'power' => $data['power'],
            'rack' => $data['rack'],
        ];
        $create_colocation = $this->colo->create($data_colocation);
        if ( !empty($data['addIp']) ) {
            if ( count($data['addIp']) ) {
                $n = 0;
                foreach ($data['addIp'] as $key => $addIp) {
                    if ( !empty($addIp) ) {
                        $n++;
                        $data_add_ip = [
                            'colocation_id' => $create_colocation->id,
                            'ip' => $addIp
                        ];
                        $this->colocation_ip->create($data_add_ip);
                    }
                }
                if ( $n ) {
                    $data_config = [
                        'colocation_id' => $create_colocation->id,
                        'ip' => $n
                    ];
                    $this->colocation_config->create($data_config);
                }
            }
        }
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'tạo',
            'model' => 'Admin/Colocation',
            'description' => ' Colocation ' . $create_colocation->ip,
        ];
        $this->log->create($data_log);

        if($create_colocation) {
            return true;
        } else {
            return false;
        }
    }

    public function listColocation($request)
    {
        $data = [];
        $data['data'] = [];
        $status_vps = config('status_vps');
        $billings = config('billing');
        $qtt = !empty($request['qtt']) ? $request['qtt'] : 30;
        if ( !empty($request['status']) ) {
            $listColocation = $this->colo->where('status_colo', $request['status'])->where('ip', 'like', '%'.$request['q'].'%')
                    ->orderBy('id', 'desc')->paginate($qtt);
        } else {
            $listColocation = $this->colo->where('status_colo', '!=', null)->where('ip', 'like', '%'.$request['q'].'%')
                    ->orderBy('id', 'desc')->paginate($qtt);
        }

        foreach ($listColocation as $key => $colo) {
            // khách hàng
            $colo->text_user_name = !empty($colo->user->name) ? $colo->user->name : '<span class="text-danger">Đã xóa</span>';
            // sản phẩm
            $product = $colo->product;
            if (!empty($colo->product)) {
                if ( !empty($colo->product->duplicate) ) {
                    $colo->text_product = '<a href="'. route('admin.product.edit' , $colo->product->id) .'">'. $colo->product->name .'</a>';
                } else {
                    if(!empty($colo->product->group_product->private)) {
                    $colo->text_product = '<a href="'. route('admin.product_private.editPrivate' , [ $colo->product->group_product->id, $colo->product->id ]) .'">'. $proxy->product->name .'</a>';
                    } else {
                    $colo->text_product = '<a href="'. route('admin.product.edit' , $colo->product->id) .'">'. $colo->product->name .'</a>';
                    }
                }
            } else {
                $colo->text_product = '<span class="text-danger">Đã xóa</span>';
            }
            // sl ip
            $colo->count_ip = !empty( $colo->colocation_config->ip ) ? $colo->colocation_config->ip : 0;
            $text_addon_ip = '';
            foreach ($colo->colocation_ips as $colocation_ip) {
                $text_addon_ip .= $colocation_ip->ip . '<br>';
            }
            $colo->text_addon_ip = $text_addon_ip;
            // ngày tạo
            $colo->date_create = date('H:i:m d-m-Y', strtotime($colo->created_at));
            // ngày kết thúc
            $colo->text_next_due_date = date('d-m-Y', strtotime($colo->next_due_date));
            // tổng thời gian thuê 
            $total_time = '';
            $create_date = new Carbon($colo->created_at);
            $next_due_date = new Carbon($colo->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                if ( $create_date != $next_due_date ) {
                    $total_time .= ($month + 1) . ' Tháng';
                }
            } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                if ( $create_date != $next_due_date ) {
                    $total_time = ($diff_month + 1) . ' Tháng';
                }
                else {
                    $total_time = $diff_month . ' Tháng';
                }
            }
            $colo->total_time = $total_time;
            // chu kỳ thanh toán
            $colo->text_billing_cycle = !empty($billings[$colo->billing_cycle]) ? $billings[$colo->billing_cycle] : '1 Tháng';
            // chi phí
            if ( !empty($colo->amount) ) {
                $amount = $colo->amount;
            } else {
                $amount = !empty( $product->pricing[$colo->billing_cycle] ) ? $product->pricing[$colo->billing_cycle] : 0;
                if ( !empty($colo->colocation_config_ips) ) {
                    foreach ($colo->colocation_config_ips as $key => $colocation_config_ip) {
                      $amount += !empty( $colocation_config_ip->product->pricing[$colo->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colo->billing_cycle] : 0;
                    }
                }
            }
            $colo->amount = number_format($amount,0,",",".");
            // trạng thái
            if ( $colo->status != 'Pending' ) {
                if ( $colo->status_colo == 'on' ) {
                    $colo->text_status = '<span class="text-success">Đang bật</span>';
                }
                elseif ( $colo->status_colo == 'off' ) {
                    $colo->text_status = '<span class="text-danger">Đã tắt</span>';
                }
                elseif ( $colo->status_colo == 'progressing' ) {
                    $colo->text_status = '<span class="text-info">Đang tạo</span>';
                }
                elseif ( $colo->status_colo == 'expire' ) {
                    $colo->text_status = '<span class="text-danger">Hết hạn</span>';
                }
                elseif ( $colo->status_colo == 'reset_password' ) {
                    $colo->text_status = '<span class="text-primary">Đang đổi mật khâu</span>';
                }
                elseif ( $colo->status_colo == 'cancel' ) {
                    $colo->text_status = '<span class="text-danger">Đã hủy</span>';
                }
                elseif ( $colo->status_colo == 'delete' ) {
                    $colo->text_status = '<span class="text-danger">Đã xóa</span>';
                }
                elseif ( $colo->status_colo == 'change_user' ) {
                    $colo->text_status = '<span class="text-danger">Đã chuyển</span>';
                }
                else {
                    $colo->text_status = '<span class="text-danger">Trạng thái lỗi!</span>';
                }
            } else {
                $colo->text_status = '<span class="text-danger">Chưa thanh toán</span>';
            }
            $data['data'][] = $colo;
        }
        $data['total'] = $listColocation->total();
        $data['perPage'] = $listColocation->perPage();
        $data['current_page'] = $listColocation->currentPage();
        return $data;
    }

    public function listProductByUser($userId)
    {
        $user = $this->user->find($userId);
        $list_group_product = $this->group_product->where('group_user_id', $user->group_user_id)
                                ->where('type', 'colocation')->get();
        $products = [];
        foreach ($list_group_product as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                $products[] = $product;
            }
        }
        return $products;
    }

}

?>
