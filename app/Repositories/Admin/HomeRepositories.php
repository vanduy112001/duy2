<?php

namespace App\Repositories\Admin;

// model
use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Product;
use App\Model\Pricing;
use App\Model\Ticket;
use App\Model\TicketMessage;
use App\Model\OrderExpired;
use App\Model\HistoryPay;
use App\Model\Momo;
use App\Model\Colocation;
use App\Model\EmailHosting;
// dich vu
use Carbon\Carbon;


class HomeRepositories {

    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $product;
    protected $pricing;
    protected $ticket;
    protected $order_expired;
    protected $history_pay;
    protected $momo;
    protected $colocation;
    protected $email_hosting;

    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->pricing = new Pricing;
        $this->product = new Product;
        $this->product = new Product;
        $this->ticket = new Ticket;
        $this->order_expired = new OrderExpired;
        $this->history_pay = new HistoryPay;
        $this->momo = new Momo();
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
    }

    // tổng số Vps
    public function total_vps()
    {
        return $this->vps->where('status', 'Active')->where('location', 'cloudzone')->count();
    }
    // tổng số Vps US
    public function total_vps_us()
    {
        return $this->vps->where('status', 'Active')->where('location', 'us')->count();
    }
    // tổng số hosting
    public function total_hosting()
    {
        return $this->hosting->where('status', 'Active')->count();
    }
    // tổng số hosting
    public function total_email_hosting()
    {
        return $this->email_hosting->where('status', 'Active')->count();
    }
    // tổng số server
    public function total_server()
    {
        return $this->server->where('status', 'Active')->count();
    }
    // tổng số server
    public function total_colocation()
    {
        return $this->colocation->where('status', 'Active')->count();
    }
    // Số colocation hết hạn
    public function total_colo_termination()
    {
        $list_vps = $this->colocation->get();
        $total = 0;
        if ( $list_vps->count() > 0 ) {
            foreach ($list_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Số vps hết hạn
    public function total_vps_termination()
    {
        $data_vps = $this->vps->where('status', 'Active')->where('location', 'cloudzone')->get();
        $total = 0;
        if ( $data_vps->count() > 0 ) {
            foreach ($data_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Số vps us hết hạn
    public function total_vps_us_termination()
    {
        $data_vps = $this->vps->where('status', 'Active')->where('location', 'us')->get();
        $total = 0;
        if ( $data_vps->count() > 0 ) {
            foreach ($data_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Số luongj ticket mới
    public function total_ticket_pending()
    {
        $tickets = $this->ticket->where('status', 'pending')->count();
        return $tickets;
    }
    // Số hosting hết hạn
    public function total_hosting_termination()
    {
        $data_hosting = $this->hosting->get();
        $total = 0;
        if ( $data_hosting->count() > 0 ) {
            foreach ($data_hosting as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Số email hosting hết hạn
    public function total_email_hosting_termination()
    {
        $data_hosting = $this->email_hosting->get();
        $total = 0;
        if ( $data_hosting->count() > 0 ) {
            foreach ($data_hosting as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Số server hết hạn
    public function total_server_termination()
    {
        $data_server = $this->server->get();
        $total = 0;
        if ( $data_server->count() > 0 ) {
            foreach ($data_server as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }

    public function list_momo()
    {
      $list_momo = $this->momo->orderByRaw('created_at desc')->paginate(30);
      // dd($list_momo);
      return $list_momo;
    }

    public function login_momo($data)
    {
        $login_momo = config('login_admin_momo');
        // dd($data, $login_momo); 
        if ( $data['user_login_momo'] == $login_momo['user'] && $data['passwd_login_momo'] == $login_momo['password'] ) {
          return true;
        }
        return false;
    }

    public function search_momo($request)
    {
      if ( $request['type'] == 'number_phone' ) {
        $data = [];
        $data['data'] = [];
        $list_momo = $this->momo->where('number_phone', 'like', '%'.$request['q'].'%')->orderByRaw('created_at desc')->paginate(30);
        if ($list_momo->count() > 0) {
          foreach ($list_momo as $key => $momo) {
              $momo->date = date('h:m:s d-m-Y', strtotime($momo->created_at));
              $momo->amount = number_format( $momo->amount ,0,",",".");
              $data['data'][] = $momo;
          }
        }
        $data['total'] = $list_momo->total();
        $data['perPage'] = $list_momo->perPage();
        $data['current_page'] = $list_momo->currentPage();
        return $data;
       }
       elseif ( $request['type'] == 'content' ) {
          $data = [];
          $data['data'] = [];
          $list_momo = $this->momo->where('content', 'like', '%'.$request['q'].'%')->orderByRaw('created_at desc')->paginate(30);
          if ($list_momo->count() > 0) {
            foreach ($list_momo as $key => $momo) {
              $momo->date = date('h:m:s d-m-Y', strtotime($momo->created_at));
              $momo->amount = number_format( $momo->amount ,0,",",".");
              $data['data'][] = $momo;
            }
          }
          $data['total'] = $list_momo->total();
          $data['perPage'] = $list_momo->perPage();
          $data['current_page'] = $list_momo->currentPage();
          return $data;
       }
       elseif ( $request['type'] == 'id_momo' ) {
          $data = [];
          $data['data'] = [];
          $list_momo = $this->momo->where('id_momo', 'like', '%'.$request['q'].'%')->orderByRaw('created_at desc')->paginate(30);
          if ($list_momo->count() > 0) {
            foreach ($list_momo as $key => $momo) {
              $momo->date = date('h:m:s d-m-Y', strtotime($momo->created_at));
              $momo->amount = number_format( $momo->amount ,0,",",".");
              $data['data'][] = $momo;
            }
          }
          $data['total'] = $list_momo->total();
          $data['perPage'] = $list_momo->perPage();
          $data['current_page'] = $list_momo->currentPage();
          return $data;
       }
    }

    public function detail_momo($id_momo)
    {
        $momo = $this->momo->where('id_momo', $id_momo)->first();
        return $momo;
    }

    public function update_momo($request)
    {
        $momo = $this->momo->where('id_momo', $request['id_momo'])->first();
        if ( isset($momo) ) {
          $data = [
            'number_phone' => $request['number_phone'],
            'amount' => $request['amount'],
            'content' => $request['content'],
            'id_momo' => $request['id_momo'],
            'date_trade' => date('Y-m-d'),
          ];
          $this->momo->where('id_momo', $request['id_momo'])->delete();
          $create_momo = $this->momo->create($data);
          return $create_momo;
        } else {
          return false;
        }
    }

    public function data_sevices()
    {
        $data = [
           'VPS' => [
              'total' => $this->vps->where('location', 'cloudzone')->count(),
              'used' => $this->vps->where('status', 'Active')->where('location', 'cloudzone')->count(),
              'cancel' => $this->vps->where('status', 'Cancel')->where('location', 'cloudzone')->count(),
              'termination' => $this->total_vps_termination(),
           ],
           'VPS US' => [
              'total' => $this->vps->where('location', 'us')->count(),
              'used' => $this->vps->where('status', 'Active')->where('location', 'us')->count(),
              'cancel' => $this->vps->where('status', 'Cancel')->where('location', 'us')->count(),
              'termination' => $this->total_vps_us_termination(),
           ],
           'Hosting' => [
              'total' => $this->hosting->count(),
              'used' => $this->hosting->where('status', 'Active')->count(),
              'cancel' => $this->hosting->where('status', 'Cancel')->count(),
              'termination' => $this->total_hosting_termination(),
           ],
           'Hosting' => [
              'total' => $this->email_hosting->count(),
              'used' => $this->email_hosting->where('status', 'Active')->count(),
              'cancel' => $this->email_hosting->where('status', 'Cancel')->count(),
              'termination' => $this->total_email_hosting_termination(),
           ],
           'Server' => [
              'total' => $this->server->count(),
              'used' => $this->server->where('status', 'Active')->count(),
              'cancel' => $this->server->where('status', 'Cancel')->count(),
              'termination' => $this->total_server_termination(),
           ],
           'Colocation' => [
              'total' => $this->colocation->count(),
              'used' => $this->colocation->where('status', 'Active')->count(),
              'cancel' => $this->colocation->where('status', 'Cancel')->count(),
              'termination' => $this->total_colo_termination(),
           ],
        ];
        return $data;
    }
    // Tông tiền vps
    public function total_price_vps()
    {
        $data_vps = $this->vps->where('status', 'Active')->where('location', 'cloudzone')->get();
        $total = 0;
        if ($data_vps->count() > 0 ) {
            foreach ($data_vps as $key => $vps) {
                $pricing = $this->pricing->where('product_id', $vps->product_id)->first();
                if (isset($pricing)) {
                    $total += $pricing[$vps->billing_cycle];
                }
            }
        }
        return $total;
    }
    // Tông tiền vps
    public function total_price_vps_us()
    {
        $data_vps = $this->vps->where('status', 'Active')->where('location', 'us')->get();
        $total = 0;
        if ($data_vps->count() > 0 ) {
            foreach ($data_vps as $key => $vps) {
                $pricing = $this->pricing->where('product_id', $vps->product_id)->first();
                if (isset($pricing)) {
                    $total += $pricing[$vps->billing_cycle];
                }
            }
        }
        return $total;
    }
    // Tông tiền hosting
    public function total_price_hosting()
    {
        $data_hosting = $this->hosting->where('status', 'Active')->get();
        $total = 0;
        if ($data_hosting->count() > 0 ) {
            foreach ($data_hosting as $key => $hosting) {
                $pricing = $this->pricing->where('product_id', $hosting->product_id)->first();
                if (isset($pricing)) {
                    $total += $pricing[$hosting->billing_cycle];
                }
            }
        }
        return $total;
    }
    // Tông tiền hosting
    public function total_price_email_hosting()
    {
        $data_hosting = $this->email_hosting->where('status', 'Active')->get();
        $total = 0;
        if ($data_hosting->count() > 0 ) {
            foreach ($data_hosting as $key => $hosting) {
                $pricing = $this->pricing->where('product_id', $hosting->product_id)->first();
                if (isset($pricing)) {
                    $total += $pricing[$hosting->billing_cycle];
                }
            }
        }
        return $total;
    }
    // Tông tiền server
    public function total_price_server()
    {
        $data_server = $this->server->where('status', 'Active')->get();
        $total = 0;
        if ($data_server->count() > 0 ) {
            foreach ($data_server as $key => $server) {
                $total += $server->amount;
            }
        }
        return $total;
    }
    // Tông tiền colocation
    public function total_price_colocation()
    {
        $data_server = $this->colocation->where('status', 'Active')->get();
        $total = 0;
        if ($data_server->count() > 0 ) {
            foreach ($data_server as $key => $server) {
                $total += $server->amount;
            }
        }
        return $total;
    }
    // Tổng thu tất cả
    public function data_total_price()
    {
        $data = [
            'VPS' => $this->total_price_vps(),
            'VPS US' => $this->total_price_vps_us(),
            'Hosting' => $this->total_price_hosting(),
            'Email Hosting' => $this->total_price_email_hosting(),
            'Server' => $this->total_price_server(),
            'Colocation' => $this->total_price_colocation(),
            'Total' => $this->total_price_vps() + $this->total_price_hosting()
                          + $this->total_price_server() + $this->total_price_colocation() + $this->total_price_email_hosting(),
        ];
        return $data;
    }

    public function chart_user($type, $sort1, $sort2 , $sort3)
    {
        $time = [];
        $data = [];
        $users = $this->user->get();
        if ($type == 'sort_select') {
            if (empty($sort1) && empty($sort2)) {
                for ($i=0; $i < 7; $i++) {
                    $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                    $date = Carbon::parse('this week')->addDays($i);
                    $date = $date->year . '-' . $date->month . '-' . $date->day;
                    $date = new Carbon($date);
                    $n = 0;
                    $default = 0;
                    $google = 0;
                    $fb = 0;
                    $total = 0;
                    foreach ($users as $key => $user) {
                       if (!empty($user->created_at)) {
                           $create_at = date('Y-m-d', strtotime($user->created_at));
                           $create_at = new Carbon($create_at);
                           if ($date->diffInDays($create_at) == 0) {
                              $n++;
                              $total++;
                              if (empty($user->social_account)) {
                                 $default++;
                              } else {
                                 if ($user->social_account->provider == 'google') {
                                    $google++;
                                 }
                                 elseif ($user->social_account->provider == 'facebook') {
                                    $fb++;
                                 }
                                 else {
                                    $default++;
                                 }
                              }
                           }
                       }
                    }
                    $data_percents[] = [
                      'total' => $total,
                      'google' => $google,
                      'fb' => $fb,
                      'df' => $default
                    ];
                    $data[] = $n;
                }

                $default = 0;
                $google = 0;
                $fb = 0;
                $total = 0;
                foreach ($data_percents as $key => $data_percent) {
                    $default += $data_percent['df'];
                    $google += $data_percent['google'];
                    $fb += $data_percent['fb'];
                    $total += $data_percent['total'];
                }

                $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);
                $result = [
                  'percent' => '(Tuần này: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                  'time' => $time,
                  'data' => $data,
                ];
                return $result;
            }
        } elseif ($type == 'last_week') {
            for ($i=0; $i < 7; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('last week')->addDays($i)));
                $date = Carbon::parse('last week')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date = new Carbon($date);
                $n = 0;
                $default = 0;
                $google = 0;
                $fb = 0;
                $total = 0;
                foreach ($users as $key => $user) {
                   if (!empty($user->created_at)) {
                       $create_at = date('Y-m-d', strtotime($user->created_at));
                       $create_at = new Carbon($create_at);
                       if ($date->diffInDays($create_at) == 0) {
                          $n++;
                          $total++;
                          if (empty($user->social_account)) {
                             $default++;
                          } else {
                             if ($user->social_account->provider == 'google') {
                                $google++;
                             }
                             elseif ($user->social_account->provider == 'facebook') {
                                $fb++;
                             }
                             else {
                                $default++;
                             }
                          }
                       }
                   }
                }
                $data[] = $n;
                $data_percents[] = [
                  'total' => $total,
                  'google' => $google,
                  'fb' => $fb,
                  'df' => $default
                ];
            }

            $default = 0;
            $google = 0;
            $fb = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
                $default += $data_percent['df'];
                $google += $data_percent['google'];
                $fb += $data_percent['fb'];
                $total += $data_percent['total'];
            }

            $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
            $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
            $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);
            $result = [
              'percent' => '(Tuần trước: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
              'time' => $time,
              'data' => $data,
            ];
            return $result;
        }
        elseif ($type == 'last_month') {
            $first_day_last_month = Carbon::parse('first day of last month');
            $last_day_last_month = Carbon::parse('last day of last month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of last month')->addDays($i)));
                $date = Carbon::parse('first day of last month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date = new Carbon($date);
                $n = 0;
                $default = 0;
                $google = 0;
                $fb = 0;
                $total = 0;

                foreach ($users as $key => $user) {
                   if (!empty($user->created_at)) {
                       $create_at = date('Y-m-d', strtotime($user->created_at));
                       $create_at = new Carbon($create_at);
                       if ($date->diffInDays($create_at) == 0) {
                          $n++;
                          $total++;
                          if (empty($user->social_account)) {
                             $default++;
                          } else {
                             if ($user->social_account->provider == 'google') {
                                $google++;
                             }
                             elseif ($user->social_account->provider == 'facebook') {
                                $fb++;
                             }
                             else {
                                $default++;
                             }
                          }
                       }
                   }
                }
                $data_percents[] = [
                  'total' => $total,
                  'google' => $google,
                  'fb' => $fb,
                  'df' => $default
                ];
                $data[] = $n;
            }

            $default = 0;
            $google = 0;
            $fb = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
                $default += $data_percent['df'];
                $google += $data_percent['google'];
                $fb += $data_percent['fb'];
                $total += $data_percent['total'];
            }

            $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
            $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
            $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

            $result = [
              'percent' => '(Tháng trước: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
              'time' => $time,
              'data' => $data,
            ];
            return $result;
        }
        elseif ($type == 'this_month') {
            $first_day_last_month = Carbon::parse('first day of this month');
            $last_day_last_month = Carbon::parse('last day of this month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                $date = Carbon::parse('first day of this month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date = new Carbon($date);
                $n = 0;
                $default = 0;
                $google = 0;
                $fb = 0;
                $total = 0;

                foreach ($users as $key => $user) {
                   if (!empty($user->created_at)) {
                       $create_at = date('Y-m-d', strtotime($user->created_at));
                       $create_at = new Carbon($create_at);
                       if ($date->diffInDays($create_at) == 0) {
                          $n++;
                          $total++;
                          if (empty($user->social_account)) {
                             $default++;
                          } else {
                             if ($user->social_account->provider == 'google') {
                                $google++;
                             }
                             elseif ($user->social_account->provider == 'facebook') {
                                $fb++;
                             }
                             else {
                                $default++;
                             }
                          }
                       }
                   }
                }
                $data[] = $n;
                $data_percents[] = [
                  'total' => $total,
                  'google' => $google,
                  'fb' => $fb,
                  'df' => $default
                ];
            }

            $default = 0;
            $google = 0;
            $fb = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
                $default += $data_percent['df'];
                $google += $data_percent['google'];
                $fb += $data_percent['fb'];
                $total += $data_percent['total'];
            }

            $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
            $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
            $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

            $result = [
              'percent' => '(Tháng này: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
              'time' => $time,
              'data' => $data,
            ];
            return $result;
        }
        elseif ($type == 'sort') {
            switch ( $sort1 ) {
              case 'day':
                  switch ($sort2) {

                    case 'week':
                        for ($i=0; $i < 7; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                            $date = Carbon::parse('this week')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date = new Carbon($date);
                            $n = 0;
                            $default = 0;
                            $google = 0;
                            $fb = 0;
                            $total = 0;

                            foreach ($users as $key => $user) {
                               if (!empty($user->created_at)) {
                                   $create_at = date('Y-m-d', strtotime($user->created_at));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if (empty($user->social_account)) {
                                         $default++;
                                      } else {
                                         if ($user->social_account->provider == 'google') {
                                            $google++;
                                         }
                                         elseif ($user->social_account->provider == 'facebook') {
                                            $fb++;
                                         }
                                         else {
                                            $default++;
                                         }
                                      }
                                   }
                               }
                            }
                            $data[] = $n;
                            $data_percents[] = [
                              'total' => $total,
                              'google' => $google,
                              'fb' => $fb,
                              'df' => $default
                            ];
                        }

                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                            $default += $data_percent['df'];
                            $google += $data_percent['google'];
                            $fb += $data_percent['fb'];
                            $total += $data_percent['total'];
                        }

                        $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                        $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                        $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);


                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tuần: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        return $result;
                      break;

                    case 'month':
                        $first_day_last_month = Carbon::parse('first day of this month');
                        $last_day_last_month = Carbon::parse('last day of this month');
                        $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                            $date = Carbon::parse('first day of this month')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date = new Carbon($date);
                            $n = 0;
                            $default = 0;
                            $google = 0;
                            $fb = 0;
                            $total = 0;

                            foreach ($users as $key => $user) {
                               if (!empty($user->created_at)) {
                                   $create_at = date('Y-m-d', strtotime($user->created_at));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if (empty($user->social_account)) {
                                         $default++;
                                      } else {
                                         if ($user->social_account->provider == 'google') {
                                            $google++;
                                         }
                                         elseif ($user->social_account->provider == 'facebook') {
                                            $fb++;
                                         }
                                         else {
                                            $default++;
                                         }
                                      }
                                   }
                               }
                            }
                            $data[] = $n;
                            $data_percents[] = [
                              'total' => $total,
                              'google' => $google,
                              'fb' => $fb,
                              'df' => $default
                            ];
                        }

                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                            $default += $data_percent['df'];
                            $google += $data_percent['google'];
                            $fb += $data_percent['fb'];
                            $total += $data_percent['total'];
                        }
                        $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                        $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                        $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tháng: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        return $result;
                      break;

                    case 'year':
                        $first_day_this_year = '1-1-' . Carbon::parse()->year;
                        $first_day_this_year = new Carbon($first_day_this_year);
                        $last_day_this_year = Carbon::parse('last day of this year');
                        $last_day_this_year = '31-12-' . Carbon::parse()->year;
                        $last_day_this_year = new Carbon($last_day_this_year);
                        $c = 0;
                        $t = $last_day_this_year->diffInDays($first_day_this_year) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $first_day_this_year = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year2 = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year = new Carbon($first_day_this_year);
                            $first_day_this_year2 = new Carbon($first_day_this_year2);
                            $time[] = date('d-m-Y', strtotime($first_day_this_year2->addDays($i)));
                            $date = $first_day_this_year->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date = new Carbon($date);
                            $n = 0;
                            $default = 0;
                            $google = 0;
                            $fb = 0;
                            $total = 0;
                            foreach ($users as $key => $user) {
                               if (!empty($user->created_at)) {
                                   $create_at = date('Y-m-d', strtotime($user->created_at));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if (empty($user->social_account)) {
                                         $default++;
                                      } else {
                                         if ($user->social_account->provider == 'google') {
                                            $google++;
                                         }
                                         elseif ($user->social_account->provider == 'facebook') {
                                            $fb++;
                                         }
                                         else {
                                            $default++;
                                         }
                                      }
                                   }
                               }
                            }
                            $data[] = $n;
                            $data_percents[] = [
                              'total' => $total,
                              'google' => $google,
                              'fb' => $fb,
                              'df' => $default
                            ];
                        }
                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                            $default += $data_percent['df'];
                            $google += $data_percent['google'];
                            $fb += $data_percent['fb'];
                            $total += $data_percent['total'];
                        }
                        $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                        $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                        $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong năm: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        // dd($result);
                        return $result;
                      break;

                  }
                break;

              case 'week':
                  switch ($sort2) {
                    case 'month':
                        // dd( Carbon::now()->month);
                        for ($i=0; $i < 4; $i++) {
                            $n = 0;
                            $t = $i;
                            $first_sunday_of_month = date('d', strtotime(Carbon::parse('first sunday of this month')));
                            // lấy ngày bắt đầu và ngày kết thúc
                            if ($i == 0) {
                               $dayStar = 1;
                               $dayEnd = $first_sunday_of_month;
                            } elseif ($i == 3) {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               if ( Carbon::now()->month == 1 || Carbon::now()->month == 3 || Carbon::now()->month == 5
                                 || Carbon::now()->month == 7 || Carbon::now()->month == 8 || Carbon::now()->month == 10 || Carbon::now()->month == 12 ) {
                                 $dayEnd = 31;
                               }
                               else if ( Carbon::now()->month == 2 ) {
                                   $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                               } else {
                                 $dayEnd =  30 ;
                               }
                            } else {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               $dayEnd =   $first_sunday_of_month + ($i - 1) * 7 + 7;
                            }
                            $time[] = 'Tuần ' . ($t + 1) . ' (' . $dayStar . ' - ' . $dayEnd . ' / ' . Carbon::now()->month . ')';
                            $default = 0;
                            $google = 0;
                            $fb = 0;
                            $total = 0;

                            for ($j=$dayStar; $j <= $dayEnd; $j++) {
                                $date = Carbon::now()->year . '-' . Carbon::now()->month . '-' . $j;
                                $date = new Carbon($date);
                                foreach ($users as $key => $user) {
                                   if (!empty($user->created_at)) {
                                       $create_at = date('Y-m-d', strtotime($user->created_at));
                                       $create_at = new Carbon($create_at);
                                       if ($date->diffInDays($create_at) == 0) {
                                          $n++;
                                          $total++;
                                          if (empty($user->social_account)) {
                                             $default++;
                                          } else {
                                             if ($user->social_account->provider == 'google') {
                                                $google++;
                                             }
                                             elseif ($user->social_account->provider == 'facebook') {
                                                $fb++;
                                             }
                                             else {
                                                $default++;
                                             }
                                          }
                                       }
                                   }
                                }
                            }
                            $data[] = $n;
                            $data_percents[] = [
                              'total' => $total,
                              'google' => $google,
                              'fb' => $fb,
                              'df' => $default
                            ];
                        }

                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                            $default += $data_percent['df'];
                            $google += $data_percent['google'];
                            $fb += $data_percent['fb'];
                            $total += $data_percent['total'];
                        }
                        $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                        $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                        $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong tháng: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        // dd($result);
                        return $result;
                      break;

                    case 'year':
                        $current_week = Carbon::now()->weeksInYear;
                        $first_monday = date('d', strtotime(Carbon::parse('first Monday of January')));
                        $first_sunday = date('d', strtotime(Carbon::parse('first Sunday of January')));
                        if ($first_sunday < 7) {
                          $first_sunday += 7;
                        }
                        for ($i=0; $i < $current_week; $i++) {
                             $first_day = '1-1-'.Carbon::now()->year;
                             $t =  $i;
                             $n = 0;
                             $default = 0;
                             $google = 0;
                             $fb = 0;
                             $total = 0;
                             $time[] = 'Tuần ' . ($t + 1);
                             if ($i == 0) {
                                $dayStar = 1;
                                if ($first_sunday < 7) {
                                   $dayEnd = $first_sunday + 6;
                                } else {
                                   $dayEnd = $first_sunday;
                                }
                             }
                             elseif ( $i == $current_week -1 ) {
                                 $dayStar = $i * 7 + 6;
                                 $dayEnd = Carbon::now()->isLeapYear() ? 366 : 365;
                             } else {
                                 $dayStar = $first_monday + $i * 7;
                                 $dayEnd = $first_monday + $i * 7 + 6;
                             }

                             for ($j = $dayStar; $j <= $dayEnd; $j++) {
                                 $k = $j;
                                 $k = ($k-1) * 60 * 60 * 24;
                                 $day = $first_day;
                                 $day = date('d-m-Y', strtotime($day) + $k);
                                 $date = new Carbon($day);
                                 foreach ($users as $key => $user) {
                                    if (!empty($user->created_at)) {
                                        $create_at = date('Y-m-d', strtotime($user->created_at));
                                        $create_at = new Carbon($create_at);
                                        if ($date->diffInDays($create_at) == 0) {
                                           $n++;
                                           $total++;
                                            if (empty($user->social_account)) {
                                               $default++;
                                            } else {
                                               if ($user->social_account->provider == 'google') {
                                                  $google++;
                                               }
                                               elseif ($user->social_account->provider == 'facebook') {
                                                  $fb++;
                                               }
                                               else {
                                                  $default++;
                                               }
                                            }
                                        }
                                    }
                                 }

                             }
                             $data[] = $n;
                             $data_percents[] = [
                                'total' => $total,
                                'google' => $google,
                                'fb' => $fb,
                                'df' => $default
                              ];
                        }

                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                            $default += $data_percent['df'];
                            $google += $data_percent['google'];
                            $fb += $data_percent['fb'];
                            $total += $data_percent['total'];
                        }


                        $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                        $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                        $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong năm: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];

                        return $result;
                      break;

                  }
                break;

              case 'month':
                  for ($i=1; $i <= 12; $i++) {
                      $dayStar = 1;
                      $n = 0;
                      $default = 0;
                      $google = 0;
                      $fb = 0;
                      $total = 0;
                      $time[] = 'Tháng ' . $i;
                      if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 8 || $i == 10 || $i == 12 ) {
                        $dayEnd = 31;
                      }
                      else if ( $i == 2 ) {
                          $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                      } else {
                          $dayEnd =  30 ;
                      }

                      for ($j=$dayStar; $j <= $dayEnd; $j++) {
                          $date =  Carbon::now()->year . '-' . $i . '-' . $j;
                          $date = new Carbon($date);
                          foreach ($users as $key => $user) {
                             if (!empty($user->created_at)) {
                                 $create_at = date('Y-m-d', strtotime($user->created_at));
                                 $create_at = new Carbon($create_at);
                                 if ($date->diffInDays($create_at) == 0) {
                                    $n++;
                                    $total++;
                                    if (empty($user->social_account)) {
                                       $default++;
                                    } else {
                                       if ($user->social_account->provider == 'google') {
                                          $google++;
                                       }
                                       elseif ($user->social_account->provider == 'facebook') {
                                          $fb++;
                                       }
                                       else {
                                          $default++;
                                       }
                                    }
                                 }
                             }
                          }
                      }
                      $data[] = $n;
                      $data_percents[] = [
                        'total' => $total,
                        'google' => $google,
                        'fb' => $fb,
                        'df' => $default
                      ];
                  }
                  $default = 0;
                  $google = 0;
                  $fb = 0;
                  $total = 0;
                  foreach ($data_percents as $key => $data_percent) {
                      $default += $data_percent['df'];
                      $google += $data_percent['google'];
                      $fb += $data_percent['fb'];
                      $total += $data_percent['total'];
                  }
                  $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                  $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                  $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                  $result = [
                    'percent' => '(Sắp xếp theo tháng trong năm: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                    'time' => $time,
                    'data' => $data,
                  ];
                  // dd($result);
                  return $result;

                break;

            }
        }

        elseif ($type == 'custom') {
            switch ($sort1) {
                case 'day':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $diff_day = $dayEnd;
                    $diff_day = $diff_day->diffInDays($dayStar);
                    if ($diff_day < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($diff_day); $i++) {
                        $day_start =  new Carbon($sort2);
                        $date = $day_start;
                        $date = $date->addDays($i);
                        $time[] = date( 'd-m-Y', strtotime($date) );
                        $date = $date->year . '-' . $date->month . '-' . $date->day;
                        $date = new Carbon($date);
                        $n = 0;
                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;

                        foreach ($users as $key => $user) {
                           if (!empty($user->created_at)) {
                               $create_at = date('Y-m-d', strtotime($user->created_at));
                               $create_at = new Carbon($create_at);
                               if ($date->diffInDays($create_at) == 0) {
                                  $n++;
                                  $total++;
                                  if (empty($user->social_account)) {
                                     $default++;
                                  } else {
                                     if ($user->social_account->provider == 'google') {
                                        $google++;
                                     }
                                     elseif ($user->social_account->provider == 'facebook') {
                                        $fb++;
                                     }
                                     else {
                                        $default++;
                                     }
                                  }
                               }
                           }
                        }
                        $data[] = $n;
                        $data_percents[] = [
                          'total' => $total,
                          'google' => $google,
                          'fb' => $fb,
                          'df' => $default
                        ];
                    }

                    $default = 0;
                    $google = 0;
                    $fb = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                        $default += $data_percent['df'];
                        $google += $data_percent['google'];
                        $fb += $data_percent['fb'];
                        $total += $data_percent['total'];
                    }
                    $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                    $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                    $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo ngày: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];
                    // dd($result);
                    return $result;
                  break;
                case 'week':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->weekOfYear - $dayStar->weekOfYear;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= $current ; $i++) {
                       $n = 0;
                       $default = 0;
                       $google = 0;
                       $fb = 0;
                       $total = 0;
                       $a = $i;
                       $day_star =  new Carbon($sort2);
                       $m = 7 - $day_star->dayOfWeek;
                       if ($i == 0) {
                          $day1 = $day_star;
                          $t = $day_star->year . '-' . $day_star->month . '-' . $day_star->day;
                          $t = new Carbon($t);
                          $day2 = $t->addDays($m);
                       } elseif ($i == $n ) {
                          $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i-1) * 7 ) + 1 );
                          $day2 =  $dayEnd;
                       } else {
                         $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i - 1) * 7 ) + 1 );
                         $t = $day1->year . '-' . $day1->month . '-' . $day1->day;
                         $t = new Carbon($t);
                         $day2 = $t->addDays(6);
                       }
                       $time[] = 'Tuần ' . ($a + 1) . ' (' . date('d', strtotime($day1)) . ' - ' . date('d', strtotime($day2)) . ' / '. $day_star->month .' )' ;
                       $k = $day2->diffInDays($day1);
                       for ($j=0; $j <= $k; $j++) {
                          $date =  $day1->year . '-' . $day1->month . '-' . $day1->day;
                          $date = new Carbon($date);
                          $date = $date->addDays($j);
                          foreach ($users as $key => $user) {
                             if (!empty($user->created_at)) {
                                 $create_at = date('Y-m-d', strtotime($user->created_at));
                                 $create_at = new Carbon($create_at);
                                 if ($date->diffInDays($create_at) == 0) {
                                    $n++;
                                    $total++;
                                    if (empty($user->social_account)) {
                                       $default++;
                                    } else {
                                       if ($user->social_account->provider == 'google') {
                                          $google++;
                                       }
                                       elseif ($user->social_account->provider == 'facebook') {
                                          $fb++;
                                       }
                                       else {
                                          $default++;
                                       }
                                    }
                                 }
                             }
                          }
                       }
                       $data[] = $n;
                       $data_percents[] = [
                        'total' => $total,
                        'google' => $google,
                        'fb' => $fb,
                        'df' => $default
                      ];
                    }

                    $default = 0;
                    $google = 0;
                    $fb = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                        $default += $data_percent['df'];
                        $google += $data_percent['google'];
                        $fb += $data_percent['fb'];
                        $total += $data_percent['total'];
                    }
                    $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                    $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                    $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tuần: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];
                    // dd($result);
                    return $result;

                  break;

                case 'month':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->month - $dayStar->month;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($current) ; $i++) {
                        $day_time = new Carbon($dayStar);
                        $n = 0;
                        $default = 0;
                        $google = 0;
                        $fb = 0;
                        $total = 0;
                        $time[] = 'Tháng ' . $day_time->addMonths($i)->month;
                        if ($i == 0) {
                            $day_start =  new Carbon($dayStar);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . $day_start->day;
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        } elseif ($i == $current) {
                            $day_end = new Carbon($dayEnd);
                            $day1 = $day_end->year . '-' . $day_end->month . '-' . '1';
                            $day2 = $day_end->year . '-' . $day_end->month . '-' . $day_end->day;
                        } else {
                            $day_start =  new Carbon($sort2);
                            $day_start = $day_start->addMonths($i);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . '1';
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        }

                        $day1 = new Carbon($day1);
                        $day2 = new Carbon($day2);
                        $diff_day = $day2->diffInDays($day1);

                        for ($j=0; $j <= $diff_day ; $j++) {
                           $date = $day1->year . '-' . $day1->month . '-' . '1';
                           $date = new Carbon($date);
                           $date = $date->addDays($j);
                           foreach ($users as $key => $user) {
                              if (!empty($user->created_at)) {
                                  $create_at = date('Y-m-d', strtotime($user->created_at));
                                  $create_at = new Carbon($create_at);
                                  if ($date->diffInDays($create_at) == 0) {
                                     $n++;
                                     $total++;
                                      if (empty($user->social_account)) {
                                         $default++;
                                      } else {
                                         if ($user->social_account->provider == 'google') {
                                            $google++;
                                         }
                                         elseif ($user->social_account->provider == 'facebook') {
                                            $fb++;
                                         }
                                         else {
                                            $default++;
                                         }
                                      }
                                  }
                              }
                           }
                        }
                        $data[] = $n;
                        $data_percents[] = [
                          'total' => $total,
                          'google' => $google,
                          'fb' => $fb,
                          'df' => $default
                        ];
                    }
                    $default = 0;
                    $google = 0;
                    $fb = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                        $default += $data_percent['df'];
                        $google += $data_percent['google'];
                        $fb += $data_percent['fb'];
                        $total += $data_percent['total'];
                    }
                    $google = $google == 0 ? 0 : round($google * 100 / $total , 2);
                    $default = $default == 0 ? 0 : round($default * 100 / $total , 2);
                    $fb = $fb == 0? 0 : round($fb * 100 / $total , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tháng: ' . $total . ', Google: ' . $google . '%, Facebook: ' . $fb . '%, Email: ' . $default . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];

                    return $result;

                  break;

            }
        }

    }


    public function chart_vps($type, $sort1, $sort2,$sort3)
    {
        try {
          $time = [];
          $data = [];
          $list_vps = $this->vps->get();
          $orders = $this->order->with('detail_orders', 'order_expireds')->get();
          if ($type == 'sort_select') {
              if (empty($sort1) && empty($sort2)) {
                  for ($i=0; $i < 7; $i++) {
                      $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                      $date = Carbon::parse('this week')->addDays($i);
                      $date = $date->year . '-' . $date->month . '-' . $date->day;
                      $date_expired = $date;
                      $date_expired = new Carbon($date_expired);
                      $date = new Carbon($date);
                      $n = 0;
                      $e = 0;
                      $total_vps = 0;
                      $total_nat = 0;
                      $total = 0;
                      $total_expired = 0;
                      $total_expired_vps = 0;
                      $total_expired_nat = 0;
                      foreach ($list_vps as $key => $vps) {
                         if (!empty($vps->date_create)) {
                             $create_at = date('Y-m-d', strtotime($vps->date_create));
                             $create_at = new Carbon($create_at);
                             if ($date->diffInDays($create_at) == 0) {
                                $n++;
                                $total++;
                                if ($vps->type_vps == 'vps') {
                                   $total_vps++;
                                }
                                else {
                                   $total_nat++;
                                }
                             }
                         }
                      }
                      foreach ($orders as $key => $order) {
                         if ($order->type == 'expired') {
                            if ( $order->order_expireds->count() > 0 ) {
                                foreach ($order->order_expireds as $key => $order_expired) {
                                  if ($order_expired->type == 'vps') {
                                      $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                      $create_at = new Carbon($create_at);
                                      if ($date_expired->diffInDays($create_at) == 0) {
                                          $e++;
                                          $total_expired++;
                                          if (!empty($order_expired->vps->type_vps)) {
                                            if ($order_expired->vps->type_vps == 'vps') {
                                               $total_expired_vps++;
                                            }
                                            else {
                                               $total_expired_nat++;
                                            }
                                          }
                                      }
                                   }
                                }
                            }
                         }
                      }
                      $data_percents[] = [
                        'total' => $total,
                        'vps' => $total_vps,
                        'nat' => $total_nat,
                        'total_expired' => $total_expired,
                        'total_expired_vps' => $total_expired_vps,
                        'total_expired_nat' => $total_expired_nat,
                      ];
                      $data[] = $n;
                      $data_expired[] = $e;
                  }

                  $total_vps = 0;
                  $total_nat = 0;
                  $total = 0;
                  $total_expired = 0;
                  $total_expired_vps = 0;
                  $total_expired_nat = 0;
                  foreach ($data_percents as $key => $data_percent) {
                      $total_vps += $data_percent['vps'];
                      $total_nat += $data_percent['nat'];
                      $total += $data_percent['total'];
                      $total_expired += $data_percent['total_expired'];
                      $total_expired_vps += $data_percent['total_expired_vps'];
                      $total_expired_nat += $data_percent['total_expired_nat'];
                  }

                  $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                  $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                  $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                  $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                  $result = [
                    'percent' => '(Tuần này: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                    'percent_expired' => '(Tuần này: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                    'time' => $time,
                    'data' => $data,
                    'data_expired' => $data_expired,
                  ];
                  return $result;
              }
          }

          elseif ($type == 'last_week')
          {
              for ($i=0; $i < 7; $i++) {
                  $time[] = date('d-m-Y', strtotime(Carbon::parse('last week')->addDays($i)));
                  $date = Carbon::parse('last week')->addDays($i);
                  $date = $date->year . '-' . $date->month . '-' . $date->day;
                  $date_expired = $date;
                  $date_expired = new Carbon($date_expired);
                  $date = new Carbon($date);
                  $e = 0;
                  $n = 0;
                  $total_vps = 0;
                  $total_nat = 0;
                  $total = 0;
                  $total_expired = 0;
                  $total_expired_vps = 0;
                  $total_expired_nat = 0;
                  foreach ($list_vps as $key => $vps) {
                     if (!empty($vps->date_create)) {
                         $create_at = date('Y-m-d', strtotime($vps->date_create));
                         $create_at = new Carbon($create_at);
                         if ($date->diffInDays($create_at) == 0) {
                            $n++;
                            $total++;
                            if ($vps->type_vps == 'vps') {
                               $total_vps++;
                            }
                            else {
                               $total_nat++;
                            }
                         }
                     }
                  }

                  foreach ($orders as $key => $order) {
                    if ($order->type == 'expired') {
                      if ( $order->order_expireds->count() > 0 ) {
                        foreach ($order->order_expireds as $key => $order_expired) {
                          if ($order_expired->type == 'vps') {
                            $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date_expired->diffInDays($create_at) == 0) {
                              $e++;
                              $total_expired++;
                              if (!empty($order_expired->vps->type_vps)) {
                                if ($order_expired->vps->type_vps == 'vps') {
                                  $total_expired_vps++;
                                }
                                else {
                                  $total_expired_nat++;
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                  $data_percents[] = [
                    'total' => $total,
                    'vps' => $total_vps,
                    'nat' => $total_nat,
                    'total_expired' => $total_expired,
                    'total_expired_vps' => $total_expired_vps,
                    'total_expired_nat' => $total_expired_nat,
                  ];
                  $data[] = $n;
                  $data_expired[] = $e;

              }

              $total_vps = 0;
              $total_nat = 0;
              $total = 0;
              $total_expired = 0;
              $total_expired_vps = 0;
              $total_expired_nat = 0;
              foreach ($data_percents as $key => $data_percent) {
                $total_vps += $data_percent['vps'];
                $total_nat += $data_percent['nat'];
                $total += $data_percent['total'];
                $total_expired += $data_percent['total_expired'];
                $total_expired_vps += $data_percent['total_expired_vps'];
                $total_expired_nat += $data_percent['total_expired_nat'];
              }

              $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
              $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
              $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
              $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

              $result = [
                'percent' => '(Tuần trước: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                'percent_expired' => '(Tuần trước: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                'time' => $time,
                'data' => $data,
                'data_expired' => $data_expired,
              ];

              return $result;
          }

          elseif ($type == 'last_month')
          {
              $first_day_last_month = Carbon::parse('first day of last month');
              $last_day_last_month = Carbon::parse('last day of last month');
              $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
              for ($i=0; $i < $t; $i++) {
                  $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of last month')->addDays($i)));
                  $date = Carbon::parse('first day of last month')->addDays($i);
                  $date = $date->year . '-' . $date->month . '-' . $date->day;
                  $date_expired = $date;
                  $date_expired = new Carbon($date_expired);
                  $e = 0;
                  $date = new Carbon($date);
                  $n = 0;
                  $total_vps = 0;
                  $total_nat = 0;
                  $total = 0;
                  $total_expired = 0;
                  $total_expired_vps = 0;
                  $total_expired_nat = 0;

                  foreach ($list_vps as $key => $vps) {
                     if (!empty($vps->date_create)) {
                         $create_at = date('Y-m-d', strtotime($vps->date_create));
                         $create_at = new Carbon($create_at);
                         if ($date->diffInDays($create_at) == 0) {
                            $n++;
                            $total++;
                            if ($vps->type_vps == 'vps') {
                               $total_vps++;
                            }
                            else {
                               $total_nat++;
                            }
                         }
                     }
                  }

                  foreach ($orders as $key => $order) {
                    if ($order->type == 'expired') {
                      if ( $order->order_expireds->count() > 0 ) {
                        foreach ($order->order_expireds as $key => $order_expired) {
                          if ($order_expired->type == 'vps') {
                              $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                              $create_at = new Carbon($create_at);
                              if ($date_expired->diffInDays($create_at) == 0) {
                                  $e++;
                                  $total_expired++;
                                  if (!empty($order_expired->vps->type_vps)) {
                                    if ($order_expired->vps->type_vps == 'vps') {
                                      $total_expired_vps++;
                                    }
                                    else {
                                      $total_expired_nat++;
                                    }
                                  }
                              }
                           }
                        }
                      }
                    }
                  }
                  $data_percents[] = [
                    'total' => $total,
                    'vps' => $total_vps,
                    'nat' => $total_nat,
                    'total_expired' => $total_expired,
                    'total_expired_vps' => $total_expired_vps,
                    'total_expired_nat' => $total_expired_nat,
                  ];
                  $data[] = $n;
                  $data_expired[] = $e;
              }

              $total_vps = 0;
              $total_nat = 0;
              $total = 0;
              $total_expired = 0;
              $total_expired_vps = 0;
              $total_expired_nat = 0;
              foreach ($data_percents as $key => $data_percent) {
                $total_vps += $data_percent['vps'];
                $total_nat += $data_percent['nat'];
                $total += $data_percent['total'];
                $total_expired += $data_percent['total_expired'];
                $total_expired_vps += $data_percent['total_expired_vps'];
                $total_expired_nat += $data_percent['total_expired_nat'];
              }

              $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
              $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
              $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
              $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

              $result = [
                'percent' => '(Tháng trước: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                'percent_expired' => '(Tháng trước: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                'time' => $time,
                'data' => $data,
                'data_expired' => $data_expired,
              ];
              return $result;
          }

          elseif ($type == 'this_month')
          {
              $first_day_last_month = Carbon::parse('first day of this month');
              $last_day_last_month = Carbon::parse('last day of this month');
              $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
              for ($i=0; $i < $t; $i++) {
                  $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                  $date = Carbon::parse('first day of this month')->addDays($i);
                  $date = $date->year . '-' . $date->month . '-' . $date->day;
                  $date_expired = $date;
                  $date_expired = new Carbon($date_expired);
                  $e = 0;
                  $date = new Carbon($date);
                  $n = 0;
                  $total_vps = 0;
                  $total_nat = 0;
                  $total = 0;
                  $total_expired = 0;
                  $total_expired_vps = 0;
                  $total_expired_nat = 0;

                  foreach ($list_vps as $key => $vps) {
                     if (!empty($vps->date_create)) {
                         $create_at = date('Y-m-d', strtotime($vps->date_create));
                         $create_at = new Carbon($create_at);
                         if ($date->diffInDays($create_at) == 0) {
                            $n++;
                            $total++;
                            if ($vps->type_vps == 'vps') {
                               $total_vps++;
                            }
                            else {
                               $total_nat++;
                            }
                         }
                     }
                  }

                  foreach ($orders as $key => $order) {
                    if ($order->type == 'expired') {
                      if ( $order->order_expireds->count() > 0 ) {
                        foreach ($order->order_expireds as $key => $order_expired) {
                          if ($order_expired->type == 'vps') {
                              $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                              $create_at = new Carbon($create_at);
                              if ($date_expired->diffInDays($create_at) == 0) {
                                  $e++;
                                  $total_expired++;
                                  if (!empty($order_expired->vps->type_vps)) {
                                    if ($order_expired->vps->type_vps == 'vps') {
                                      $total_expired_vps++;
                                    }
                                    else {
                                      $total_expired_nat++;
                                    }
                                  }
                              }
                           }
                        }
                      }
                    }
                  }
                  $data_percents[] = [
                    'total' => $total,
                    'vps' => $total_vps,
                    'nat' => $total_nat,
                    'total_expired' => $total_expired,
                    'total_expired_vps' => $total_expired_vps,
                    'total_expired_nat' => $total_expired_nat,
                  ];
                  $data[] = $n;
                  $data_expired[] = $e;
              }

              $total_vps = 0;
              $total_nat = 0;
              $total = 0;
              $total_expired = 0;
              $total_expired_vps = 0;
              $total_expired_nat = 0;
              foreach ($data_percents as $key => $data_percent) {
                $total_vps += $data_percent['vps'];
                $total_nat += $data_percent['nat'];
                $total += $data_percent['total'];
                $total_expired += $data_percent['total_expired'];
                $total_expired_vps += $data_percent['total_expired_vps'];
                $total_expired_nat += $data_percent['total_expired_nat'];
              }

              $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
              $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
              $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
              $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

              $result = [
                'percent' => '(Tháng này: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                'percent_expired' => '(Tháng này: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                'time' => $time,
                'data' => $data,
                'data_expired' => $data_expired,
              ];
              return $result;
          }

          elseif ($type == 'sort')
          {
              switch ( $sort1 ) {
                case 'day':
                    switch ($sort2) {

                      case 'week':
                          for ($i=0; $i < 7; $i++) {
                              $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                              $date = Carbon::parse('this week')->addDays($i);
                              $date = $date->year . '-' . $date->month . '-' . $date->day;
                              $date_expired = $date;
                              $date_expired = new Carbon($date_expired);
                              $e = 0;
                              $date = new Carbon($date);
                              $n = 0;
                              $total_vps = 0;
                              $total_nat = 0;
                              $total = 0;
                              $total_expired = 0;
                              $total_expired_vps = 0;
                              $total_expired_nat = 0;

                              foreach ($list_vps as $key => $vps) {
                                 if (!empty($vps->date_create)) {
                                     $create_at = date('Y-m-d', strtotime($vps->date_create));
                                     $create_at = new Carbon($create_at);
                                     if ($date->diffInDays($create_at) == 0) {
                                        $n++;
                                        $total++;
                                        if ($vps->type_vps == 'vps') {
                                           $total_vps++;
                                        }
                                        else {
                                           $total_nat++;
                                        }
                                     }
                                 }
                              }
                              foreach ($orders as $key => $order) {
                                if ($order->type == 'expired') {
                                  if ( $order->order_expireds->count() > 0 ) {
                                    foreach ($order->order_expireds as $key => $order_expired) {
                                      if ($order_expired->type == 'vps') {
                                          $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                          $create_at = new Carbon($create_at);
                                          if ($date_expired->diffInDays($create_at) == 0) {
                                              $e++;
                                              $total_expired++;
                                              if (!empty($order_expired->vps->type_vps)) {
                                                if ($order_expired->vps->type_vps == 'vps') {
                                                  $total_expired_vps++;
                                                }
                                                else {
                                                  $total_expired_nat++;
                                                }
                                              }
                                          }
                                       }
                                    }
                                  }
                                }
                              }
                              $data_percents[] = [
                                'total' => $total,
                                'vps' => $total_vps,
                                'nat' => $total_nat,
                                'total_expired' => $total_expired,
                                'total_expired_vps' => $total_expired_vps,
                                'total_expired_nat' => $total_expired_nat,
                              ];
                              $data[] = $n;
                              $data_expired[] = $e;
                          }

                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;
                          foreach ($data_percents as $key => $data_percent) {
                            $total_vps += $data_percent['vps'];
                            $total_nat += $data_percent['nat'];
                            $total += $data_percent['total'];
                            $total_expired += $data_percent['total_expired'];
                            $total_expired_vps += $data_percent['total_expired_vps'];
                            $total_expired_nat += $data_percent['total_expired_nat'];
                          }

                          $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                          $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                          $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                          $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                          $result = [
                            'percent' => '(Sắp xếp theo ngày trong tuần: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                            'percent_expired' => '(Sắp xếp theo ngày trong tuần: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                            'time' => $time,
                            'data' => $data,
                            'data_expired' => $data_expired,
                          ];
                          return $result;
                        break;

                      case 'month':
                          $first_day_last_month = Carbon::parse('first day of this month');
                          $last_day_last_month = Carbon::parse('last day of this month');
                          $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
                          for ($i=0; $i < $t; $i++) {
                              $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                              $date = Carbon::parse('first day of this month')->addDays($i);
                              $date = $date->year . '-' . $date->month . '-' . $date->day;
                              $date_expired = $date;
                              $date_expired = new Carbon($date_expired);
                              $e = 0;
                              $date = new Carbon($date);
                              $n = 0;
                              $total_vps = 0;
                              $total_nat = 0;
                              $total = 0;
                              $total_expired = 0;
                              $total_expired_vps = 0;
                              $total_expired_nat = 0;

                              foreach ($list_vps as $key => $vps) {
                                 if (!empty($vps->date_create)) {
                                     $create_at = date('Y-m-d', strtotime($vps->date_create));
                                     $create_at = new Carbon($create_at);
                                     if ($date->diffInDays($create_at) == 0) {
                                        $n++;
                                        $total++;
                                        if ($vps->type_vps == 'vps') {
                                           $total_vps++;
                                        }
                                        else {
                                           $total_nat++;
                                        }
                                     }
                                 }
                              }

                              foreach ($orders as $key => $order) {
                                if ($order->type == 'expired') {
                                  if ( $order->order_expireds->count() > 0 ) {
                                    foreach ($order->order_expireds as $key => $order_expired) {
                                      if ($order_expired->type == 'vps') {
                                          $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                          $create_at = new Carbon($create_at);
                                          if ($date_expired->diffInDays($create_at) == 0) {
                                              $e++;
                                              $total_expired++;
                                              if (!empty($order_expired->vps->type_vps)) {
                                                if ($order_expired->vps->type_vps == 'vps') {
                                                  $total_expired_vps++;
                                                }
                                                else {
                                                  $total_expired_nat++;
                                                }
                                              }
                                          }
                                       }
                                    }
                                  }
                                }
                              }
                              $data_percents[] = [
                                'total' => $total,
                                'vps' => $total_vps,
                                'nat' => $total_nat,
                                'total_expired' => $total_expired,
                                'total_expired_vps' => $total_expired_vps,
                                'total_expired_nat' => $total_expired_nat,
                              ];
                              $data[] = $n;
                              $data_expired[] = $e;
                          }

                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;
                          foreach ($data_percents as $key => $data_percent) {
                            $total_vps += $data_percent['vps'];
                            $total_nat += $data_percent['nat'];
                            $total += $data_percent['total'];
                            $total_expired += $data_percent['total_expired'];
                            $total_expired_vps += $data_percent['total_expired_vps'];
                            $total_expired_nat += $data_percent['total_expired_nat'];
                          }

                          $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                          $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                          $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                          $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                          $result = [
                            'percent' => '(Sắp xếp theo ngày trong tháng: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                            'percent_expired' => '(Sắp xếp theo ngày trong tháng: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                            'time' => $time,
                            'data' => $data,
                            'data_expired' => $data_expired,
                          ];
                          return $result;
                        break;

                      case 'year':
                          $first_day_this_year = '1-1-' . Carbon::parse()->year;
                          $first_day_this_year = new Carbon($first_day_this_year);
                          $last_day_this_year = Carbon::parse('last day of this year');
                          $last_day_this_year = '31-12-' . Carbon::parse()->year;
                          $last_day_this_year = new Carbon($last_day_this_year);
                          $c = 0;
                          $t = $last_day_this_year->diffInDays($first_day_this_year) + 1;
                          for ($i=0; $i < $t; $i++) {
                              $first_day_this_year = '1-1-' . Carbon::parse()->year;
                              $first_day_this_year2 = '1-1-' . Carbon::parse()->year;
                              $first_day_this_year = new Carbon($first_day_this_year);
                              $first_day_this_year2 = new Carbon($first_day_this_year2);
                              $time[] = date('d-m-Y', strtotime($first_day_this_year2->addDays($i)));
                              $date = $first_day_this_year->addDays($i);
                              $date = $date->year . '-' . $date->month . '-' . $date->day;
                              $date_expired = $date;
                              $date_expired = new Carbon($date_expired);
                              $e = 0;
                              $date = new Carbon($date);
                              $n = 0;
                              $total_vps = 0;
                              $total_nat = 0;
                              $total = 0;
                              $total_expired = 0;
                              $total_expired_vps = 0;
                              $total_expired_nat = 0;

                              foreach ($list_vps as $key => $vps) {
                                 if (!empty($vps->date_create)) {
                                     $create_at = date('Y-m-d', strtotime($vps->date_create));
                                     $create_at = new Carbon($create_at);
                                     if ($date->diffInDays($create_at) == 0) {
                                        $n++;
                                        $total++;
                                        if ($vps->type_vps == 'vps') {
                                           $total_vps++;
                                        }
                                        else {
                                           $total_nat++;
                                        }
                                     }
                                 }
                              }

                              foreach ($orders as $key => $order) {
                                if ($order->type == 'expired') {
                                  if ( $order->order_expireds->count() > 0 ) {
                                    foreach ($order->order_expireds as $key => $order_expired) {
                                      if ($order_expired->type == 'vps') {
                                          $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                          $create_at = new Carbon($create_at);
                                          if ($date_expired->diffInDays($create_at) == 0) {
                                              $e++;
                                              $total_expired++;
                                              if (!empty($order_expired->vps->type_vps)) {
                                                if ($order_expired->vps->type_vps == 'vps') {
                                                  $total_expired_vps++;
                                                }
                                                else {
                                                  $total_expired_nat++;
                                                }
                                              }
                                          }
                                       }
                                    }
                                  }
                                }
                              }
                              $data_percents[] = [
                                'total' => $total,
                                'vps' => $total_vps,
                                'nat' => $total_nat,
                                'total_expired' => $total_expired,
                                'total_expired_vps' => $total_expired_vps,
                                'total_expired_nat' => $total_expired_nat,
                              ];
                              $data[] = $n;
                              $data_expired[] = $e;
                          }
                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;
                          foreach ($data_percents as $key => $data_percent) {
                            $total_vps += $data_percent['vps'];
                            $total_nat += $data_percent['nat'];
                            $total += $data_percent['total'];
                            $total_expired += $data_percent['total_expired'];
                            $total_expired_vps += $data_percent['total_expired_vps'];
                            $total_expired_nat += $data_percent['total_expired_nat'];
                          }

                          $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                          $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                          $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                          $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                          $result = [
                            'percent' => '(Sắp xếp theo ngày trong năm: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                            'percent_expired' => '(Sắp xếp theo ngày trong năm: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                            'time' => $time,
                            'data' => $data,
                            'data_expired' => $data_expired,
                          ];
                          // dd($result);
                          return $result;
                        break;

                    }
                  break;

                case 'week':
                    switch ($sort2) {
                      case 'month':
                          // dd( Carbon::now()->month);
                          for ($i=0; $i < 4; $i++) {
                              $n = 0;
                              $e = 0;
                              $t = $i;
                              $first_sunday_of_month = date('d', strtotime(Carbon::parse('first sunday of this month')));
                              // lấy ngày bắt đầu và ngày kết thúc
                              if ($i == 0) {
                                 $dayStar = 1;
                                 $dayEnd = $first_sunday_of_month;
                              } elseif ($i == 3) {
                                 $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                                 if ( Carbon::now()->month == 1 || Carbon::now()->month == 3 || Carbon::now()->month == 5
                                   || Carbon::now()->month == 7 || Carbon::now()->month == 8 || Carbon::now()->month == 10 || Carbon::now()->month == 12 ) {
                                   $dayEnd = 31;
                                 }
                                 else if ( Carbon::now()->month == 2 ) {
                                     $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                                 } else {
                                   $dayEnd =  30 ;
                                 }
                              } else {
                                 $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                                 $dayEnd =   $first_sunday_of_month + ($i - 1) * 7 + 7;
                              }
                              $time[] = 'Tuần ' . ($t + 1) . ' (' . $dayStar . ' - ' . $dayEnd . ' / ' . Carbon::now()->month . ')';
                              $total_vps = 0;
                              $total_nat = 0;
                              $total = 0;
                              $total_expired = 0;
                              $total_expired_vps = 0;
                              $total_expired_nat = 0;

                              for ($j=$dayStar; $j <= $dayEnd; $j++) {
                                  $date = Carbon::now()->year . '-' . Carbon::now()->month . '-' . $j;
                                  $date_expired = $date;
                                  $date_expired = new Carbon($date_expired);
                                  $date = new Carbon($date);
                                  foreach ($list_vps as $key => $vps) {
                                     if (!empty($vps->date_create)) {
                                         $create_at = date('Y-m-d', strtotime($vps->date_create));
                                         $create_at = new Carbon($create_at);
                                         if ($date->diffInDays($create_at) == 0) {
                                            $n++;
                                            $total++;
                                            if ($vps->type_vps == 'vps') {
                                               $total_vps++;
                                            }
                                            else {
                                               $total_nat++;
                                            }
                                         }
                                     }
                                  }
                                  foreach ($orders as $key => $order) {
                                    if ($order->type == 'expired') {
                                      if ( $order->order_expireds->count() > 0 ) {
                                        foreach ($order->order_expireds as $key => $order_expired) {
                                          if ($order_expired->type == 'vps') {
                                              $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                              $create_at = new Carbon($create_at);
                                              if ($date_expired->diffInDays($create_at) == 0) {
                                                  $e++;
                                                  $total_expired++;
                                                  if (!empty($order_expired->vps->type_vps)) {
                                                    if ($order_expired->vps->type_vps == 'vps') {
                                                      $total_expired_vps++;
                                                    }
                                                    else {
                                                      $total_expired_nat++;
                                                    }
                                                  }
                                              }
                                           }
                                        }
                                      }
                                    }
                                  }
                              }
                              $data_percents[] = [
                                'total' => $total,
                                'vps' => $total_vps,
                                'nat' => $total_nat,
                                'total_expired' => $total_expired,
                                'total_expired_vps' => $total_expired_vps,
                                'total_expired_nat' => $total_expired_nat,
                              ];
                              $data[] = $n;
                              $data_expired[] = $e;
                          }

                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;
                          foreach ($data_percents as $key => $data_percent) {
                            $total_vps += $data_percent['vps'];
                            $total_nat += $data_percent['nat'];
                            $total += $data_percent['total'];
                            $total_expired += $data_percent['total_expired'];
                            $total_expired_vps += $data_percent['total_expired_vps'];
                            $total_expired_nat += $data_percent['total_expired_nat'];
                          }

                          $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                          $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                          $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                          $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                          $result = [
                            'percent' => '(Sắp xếp theo tuần trong tháng: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                            'percent_expired' => '(Sắp xếp theo tuần trong tháng: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                            'time' => $time,
                            'data' => $data,
                            'data_expired' => $data_expired,
                          ];
                          // dd($result);
                          return $result;
                        break;

                      case 'year':
                          $current_week = Carbon::now()->weeksInYear;
                          $first_monday = date('d', strtotime(Carbon::parse('first Monday of January')));
                          $first_sunday = date('d', strtotime(Carbon::parse('first Sunday of January')));
                          if ($first_sunday < 7) {
                            $first_sunday += 7;
                          }
                          for ($i=0; $i < $current_week; $i++) {
                               $first_day = '1-1-'.Carbon::now()->year;
                               $t =  $i;
                               $n = 0;
                               $e = 0;
                               $total_vps = 0;
                               $total_nat = 0;
                               $total = 0;
                               $total_expired = 0;
                               $total_expired_vps = 0;
                               $total_expired_nat = 0;
                               $time[] = 'Tuần ' . ($t + 1);
                               if ($i == 0) {
                                  $dayStar = 1;
                                  if ($first_sunday < 7) {
                                     $dayEnd = $first_sunday + 6;
                                  } else {
                                     $dayEnd = $first_sunday;
                                  }
                               }
                               elseif ( $i == $current_week -1 ) {
                                   $dayStar = $i * 7 + 6;
                                   $dayEnd = Carbon::now()->isLeapYear() ? 366 : 365;
                               } else {
                                   $dayStar = $first_monday + $i * 7;
                                   $dayEnd = $first_monday + $i * 7 + 6;
                               }

                               for ($j = $dayStar; $j <= $dayEnd; $j++) {
                                   $k = $j;
                                   $k = ($k-1) * 60 * 60 * 24;
                                   $day = $first_day;
                                   $day = date('d-m-Y', strtotime($day) + $k);
                                   $date_expired = $day;
                                   $date_expired = new Carbon($date_expired);
                                   $date = new Carbon($day);
                                   foreach ($list_vps as $key => $vps) {
                                      if (!empty($vps->date_create)) {
                                          $create_at = date('Y-m-d', strtotime($vps->date_create));
                                          $create_at = new Carbon($create_at);
                                          if ($date->diffInDays($create_at) == 0) {
                                             $n++;
                                             $total++;
                                             if ($vps->type_vps == 'vps') {
                                                $total_vps++;
                                             }
                                             else {
                                                $total_nat++;
                                             }
                                          }
                                      }
                                   }

                                   foreach ($orders as $key => $order) {
                                     if ($order->type == 'expired') {
                                       if ( $order->order_expireds->count() > 0 ) {
                                         foreach ($order->order_expireds as $key => $order_expired) {
                                           if ($order_expired->type == 'vps') {
                                               $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                               $create_at = new Carbon($create_at);
                                               if ($date_expired->diffInDays($create_at) == 0) {
                                                   $e++;
                                                   $total_expired++;
                                                   if (!empty($order_expired->vps->type_vps)) {
                                                     if ($order_expired->vps->type_vps == 'vps') {
                                                       $total_expired_vps++;
                                                     }
                                                     else {
                                                       $total_expired_nat++;
                                                     }
                                                   }
                                               }
                                            }
                                         }
                                       }
                                     }
                                   }


                               }
                               $data_percents[] = [
                                 'total' => $total,
                                 'vps' => $total_vps,
                                 'nat' => $total_nat,
                                 'total_expired' => $total_expired,
                                 'total_expired_vps' => $total_expired_vps,
                                 'total_expired_nat' => $total_expired_nat,
                               ];
                               $data[] = $n;
                               $data_expired[] = $e;
                          }

                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;
                          foreach ($data_percents as $key => $data_percent) {
                            $total_vps += $data_percent['vps'];
                            $total_nat += $data_percent['nat'];
                            $total += $data_percent['total'];
                            $total_expired += $data_percent['total_expired'];
                            $total_expired_vps += $data_percent['total_expired_vps'];
                            $total_expired_nat += $data_percent['total_expired_nat'];
                          }

                          $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                          $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                          $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                          $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                          $result = [
                            'percent' => '(Sắp xếp theo tuần trong năm: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                            'percent_expired' => '(Sắp xếp theo tuần trong năm: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                            'time' => $time,
                            'data' => $data,
                            'data_expired' => $data_expired,
                          ];

                          return $result;
                        break;

                    }
                  break;

                case 'month':
                    for ($i=1; $i <= 12; $i++) {
                        $dayStar = 1;
                        $n = 0;
                        $total_vps = 0;
                        $total_nat = 0;
                        $total = 0;
                        $e = 0;

                        $total_expired = 0;
                        $total_expired_vps = 0;
                        $total_expired_nat = 0;
                        $time[] = 'Tháng ' . $i;
                        if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 8 || $i == 10 || $i == 12 ) {
                          $dayEnd = 31;
                        }
                        else if ( $i == 2 ) {
                            $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                        } else {
                            $dayEnd =  30 ;
                        }

                        for ($j=$dayStar; $j <= $dayEnd; $j++) {
                            $date =  Carbon::now()->year . '-' . $i . '-' . $j;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $date = new Carbon($date);
                            foreach ($list_vps as $key => $vps) {
                               if (!empty($vps->date_create)) {
                                   $create_at = date('Y-m-d', strtotime($vps->date_create));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if ($vps->type_vps == 'vps') {
                                          $total_vps++;
                                      }
                                      else {
                                          $total_nat++;
                                      }
                                   }
                               }
                            }

                            foreach ($orders as $key => $order) {
                              if ($order->type == 'expired') {
                                if ( $order->order_expireds->count() > 0 ) {
                                  foreach ($order->order_expireds as $key => $order_expired) {
                                    if ($order_expired->type == 'vps') {
                                        $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                        $create_at = new Carbon($create_at);
                                        if ($date_expired->diffInDays($create_at) == 0) {
                                            $e++;
                                            $total_expired++;
                                            if (!empty($order_expired->vps->type_vps)) {
                                              if ($order_expired->vps->type_vps == 'vps') {
                                                $total_expired_vps++;
                                              }
                                              else {
                                                $total_expired_nat++;
                                              }
                                            }
                                        }
                                     }
                                  }
                                }
                              }
                            }
                        }
                        $data_percents[] = [
                          'total' => $total,
                          'vps' => $total_vps,
                          'nat' => $total_nat,
                          'total_expired' => $total_expired,
                          'total_expired_vps' => $total_expired_vps,
                          'total_expired_nat' => $total_expired_nat,
                        ];
                        $data[] = $n;
                        $data_expired[] = $e;
                    }
                    $total_vps = 0;
                    $total_nat = 0;
                    $total = 0;
                    $total_expired = 0;
                    $total_expired_vps = 0;
                    $total_expired_nat = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total_vps += $data_percent['vps'];
                      $total_nat += $data_percent['nat'];
                      $total += $data_percent['total'];
                      $total_expired += $data_percent['total_expired'];
                      $total_expired_vps += $data_percent['total_expired_vps'];
                      $total_expired_nat += $data_percent['total_expired_nat'];
                    }

                    $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                    $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                    $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                    $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                    $result = [
                      'percent' => '(Sắp xếp theo tháng trong năm: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                      'percent_expired' => '(Sắp xếp theo tháng trong năm: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                      'time' => $time,
                      'data' => $data,
                      'data_expired' => $data_expired,
                    ];
                    // dd($result);
                    return $result;

                  break;

              }
          }

          elseif ($type == 'custom')
          {
              switch ($sort1) {
                  case 'day':
                      $time = [];
                      $dayStar =  new Carbon($sort2);
                      $dayEnd = new Carbon($sort3);
                      $diff_day = $dayEnd;
                      $diff_day = $diff_day->diffInDays($dayStar);
                      if ($diff_day < 0) {
                        $term = $dayStar;
                        $dayStar = $dayEnd;
                        $dayEnd = $term;
                      }
                      for ($i=0; $i <= abs($diff_day); $i++) {
                          $day_start =  new Carbon($sort2);
                          $date = $day_start;
                          $date = $date->addDays($i);
                          $time[] = date( 'd-m-Y', strtotime($date) );
                          $date = $date->year . '-' . $date->month . '-' . $date->day;
                          $date_expired = $date;
                          $date_expired = new Carbon($date_expired);
                          $e = 0;
                          $date = new Carbon($date);
                          $n = 0;
                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;

                          foreach ($list_vps as $key => $vps) {
                             if (!empty($vps->date_create)) {
                                 $create_at = date('Y-m-d', strtotime($vps->date_create));
                                 $create_at = new Carbon($create_at);
                                 if ($date->diffInDays($create_at) == 0) {
                                    $n++;
                                    $total++;
                                    if ($vps->type_vps == 'vps') {
                                        $total_vps++;
                                    }
                                    else {
                                        $total_nat++;
                                    }
                                 }
                             }
                          }
                          foreach ($orders as $key => $order) {
                            if ($order->type == 'expired') {
                              if ( $order->order_expireds->count() > 0 ) {
                                foreach ($order->order_expireds as $key => $order_expired) {
                                  if ($order_expired->type == 'vps') {
                                      $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                      $create_at = new Carbon($create_at);
                                      if ($date_expired->diffInDays($create_at) == 0) {
                                          $e++;
                                          $total_expired++;
                                          if (!empty($order_expired->vps->type_vps)) {
                                            if ($order_expired->vps->type_vps == 'vps') {
                                              $total_expired_vps++;
                                            }
                                            else {
                                              $total_expired_nat++;
                                            }
                                          }
                                      }
                                   }
                                }
                              }
                            }
                          }
                          $data_percents[] = [
                            'total' => $total,
                            'vps' => $total_vps,
                            'nat' => $total_nat,
                            'total_expired' => $total_expired,
                            'total_expired_vps' => $total_expired_vps,
                            'total_expired_nat' => $total_expired_nat,
                          ];
                          $data[] = $n;
                          $data_expired[] = $e;
                      }

                      $total_vps = 0;
                      $total_nat = 0;
                      $total = 0;
                      $total_expired = 0;
                      $total_expired_vps = 0;
                      $total_expired_nat = 0;
                      foreach ($data_percents as $key => $data_percent) {
                        $total_vps += $data_percent['vps'];
                        $total_nat += $data_percent['nat'];
                        $total += $data_percent['total'];
                        $total_expired += $data_percent['total_expired'];
                        $total_expired_vps += $data_percent['total_expired_vps'];
                        $total_expired_nat += $data_percent['total_expired_nat'];
                      }

                      $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                      $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                      $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                      $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                      $result = [
                        'percent' => '(Sắp xếp tùy chọn theo ngày: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                        'percent_expired' => '(Sắp xếp tùy chọn theo ngày: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                        'time' => $time,
                        'data' => $data,
                        'data_expired' => $data_expired,
                      ];
                      // dd($result);
                      return $result;
                    break;

                  case 'week':
                      $time = [];
                      $dayStar =  new Carbon($sort2);
                      $dayEnd = new Carbon($sort3);
                      $current = $dayEnd->weekOfYear - $dayStar->weekOfYear;
                      if ($current < 0) {
                        $term = $dayStar;
                        $dayStar = $dayEnd;
                        $dayEnd = $term;
                      }
                      for ($i=0; $i <= $current ; $i++) {
                         $n = 0;
                         $total_vps = 0;
                         $total_nat = 0;
                         $total = 0;
                         $e = 0;
                         $total_expired = 0;
                         $total_expired_vps = 0;
                         $total_expired_nat = 0;
                         $a = $i;
                         $day_star =  new Carbon($sort2);
                         $m = 7 - $day_star->dayOfWeek;
                         if ($i == 0) {
                            $day1 = $day_star;
                            $t = $day_star->year . '-' . $day_star->month . '-' . $day_star->day;
                            $t = new Carbon($t);
                            $day2 = $t->addDays($m);
                         } elseif ($i == $n ) {
                            $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i-1) * 7 ) + 1 );
                            $day2 =  $dayEnd;
                         } else {
                           $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i - 1) * 7 ) + 1 );
                           $t = $day1->year . '-' . $day1->month . '-' . $day1->day;
                           $t = new Carbon($t);
                           $day2 = $t->addDays(6);
                         }
                         $time[] = 'Tuần ' . ($a + 1) . ' (' . date('d', strtotime($day1)) . ' - ' . date('d', strtotime($day2)) . ' / '. $day_star->month .' )' ;
                         $k = $day2->diffInDays($day1);
                         for ($j=0; $j <= $k; $j++) {
                            $date =  $day1->year . '-' . $day1->month . '-' . $day1->day;
                            $date = new Carbon($date);
                            $date = $date->addDays($j);
                            $date_expired =  $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = new Carbon($date_expired);
                            foreach ($list_vps as $key => $vps) {
                               if (!empty($vps->date_create)) {
                                   $create_at = date('Y-m-d', strtotime($vps->date_create));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if ($vps->type_vps == 'vps') {
                                          $total_vps++;
                                      }
                                      else {
                                          $total_nat++;
                                      }
                                   }
                               }
                            }
                            foreach ($orders as $key => $order) {
                              if ($order->type == 'expired') {
                                if ( $order->order_expireds->count() > 0 ) {
                                  foreach ($order->order_expireds as $key => $order_expired) {
                                    if ($order_expired->type == 'vps') {
                                        $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                        $create_at = new Carbon($create_at);
                                        if ($date_expired->diffInDays($create_at) == 0) {
                                            $e++;
                                            $total_expired++;
                                            if (!empty($order_expired->vps->type_vps)) {
                                              if ($order_expired->vps->type_vps == 'vps') {
                                                $total_expired_vps++;
                                              }
                                              else {
                                                $total_expired_nat++;
                                              }
                                            }
                                        }
                                     }
                                  }
                                }
                              }
                            }
                         }
                         $data_percents[] = [
                           'total' => $total,
                           'vps' => $total_vps,
                           'nat' => $total_nat,
                           'total_expired' => $total_expired,
                           'total_expired_vps' => $total_expired_vps,
                           'total_expired_nat' => $total_expired_nat,
                         ];
                         $data[] = $n;
                         $data_expired[] = $e;
                      }

                      $total_vps = 0;
                      $total_nat = 0;
                      $total = 0;
                      $total_expired = 0;
                      $total_expired_vps = 0;
                      $total_expired_nat = 0;
                      foreach ($data_percents as $key => $data_percent) {
                        $total_vps += $data_percent['vps'];
                        $total_nat += $data_percent['nat'];
                        $total += $data_percent['total'];
                        $total_expired += $data_percent['total_expired'];
                        $total_expired_vps += $data_percent['total_expired_vps'];
                        $total_expired_nat += $data_percent['total_expired_nat'];
                      }

                      $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                      $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                      $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                      $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                      $result = [
                        'percent' => '(Sắp xếp tùy chọn theo tuần: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                        'percent_expired' => '(Sắp xếp tùy chọn theo tuần: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                        'time' => $time,
                        'data' => $data,
                        'data_expired' => $data_expired,
                      ];
                      // dd($result);
                      return $result;

                    break;

                  case 'month':
                      $time = [];
                      $dayStar =  new Carbon($sort2);
                      $dayEnd = new Carbon($sort3);
                      $current = $dayEnd->month - $dayStar->month;
                      if ($current < 0) {
                        $term = $dayStar;
                        $dayStar = $dayEnd;
                        $dayEnd = $term;
                      }
                      for ($i=0; $i <= abs($current) ; $i++) {
                          $day_time = new Carbon($dayStar);
                          $n = 0;
                          $total_vps = 0;
                          $total_nat = 0;
                          $total = 0;
                          $e = 0;
                          $total_expired = 0;
                          $total_expired_vps = 0;
                          $total_expired_nat = 0;

                          $time[] = 'Tháng ' . $day_time->addMonths($i)->month;
                          if ($i == 0) {
                              $day_start =  new Carbon($dayStar);
                              $day1 = $day_start->year . '-' . $day_start->month . '-' . $day_start->day;
                              if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                                || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                                $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                              }
                              else if ( $day_start->month == 2 ) {
                                  $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                              } else {
                                $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                              }
                          } elseif ($i == $current) {
                              $day_end = new Carbon($dayEnd);
                              $day1 = $day_end->year . '-' . $day_end->month . '-' . '1';
                              $day2 = $day_end->year . '-' . $day_end->month . '-' . $day_end->day;
                          } else {
                              $day_start =  new Carbon($sort2);
                              $day_start = $day_start->addMonths($i);
                              $day1 = $day_start->year . '-' . $day_start->month . '-' . '1';
                              if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                                || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                                $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                              }
                              else if ( $day_start->month == 2 ) {
                                  $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                              } else {
                                $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                              }
                          }

                          $day1 = new Carbon($day1);
                          $day2 = new Carbon($day2);
                          $diff_day = $day2->diffInDays($day1);

                          for ($j=0; $j <= $diff_day ; $j++) {
                             $date = $day1->year . '-' . $day1->month . '-' . '1';
                             $date = new Carbon($date);
                             $date = $date->addDays($j);
                             $date_expired = $date->year . '-' . $date->month . '-' . $date->day;
                             $date_expired = new Carbon($date_expired);
                             foreach ($list_vps as $key => $vps) {
                                if (!empty($vps->date_create)) {
                                    $create_at = date('Y-m-d', strtotime($vps->date_create));
                                    $create_at = new Carbon($create_at);
                                    if ($date->diffInDays($create_at) == 0) {
                                       $n++;
                                       $total++;
                                       if ($vps->type_vps == 'vps') {
                                           $total_vps++;
                                       }
                                       else {
                                           $total_nat++;
                                       }
                                    }
                                }
                             }
                             foreach ($orders as $key => $order) {
                               if ($order->type == 'expired') {
                                 if ( $order->order_expireds->count() > 0 ) {
                                   foreach ($order->order_expireds as $key => $order_expired) {
                                     if ($order_expired->type == 'vps') {
                                         $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                         $create_at = new Carbon($create_at);
                                         if ($date_expired->diffInDays($create_at) == 0) {
                                             $e++;
                                             $total_expired++;
                                             if (!empty($order_expired->vps->type_vps)) {
                                               if ($order_expired->vps->type_vps == 'vps') {
                                                 $total_expired_vps++;
                                               }
                                               else {
                                                 $total_expired_nat++;
                                               }
                                             }
                                         }
                                      }
                                   }
                                 }
                               }
                             }
                          }
                          $data_percents[] = [
                            'total' => $total,
                            'vps' => $total_vps,
                            'nat' => $total_nat,
                            'total_expired' => $total_expired,
                            'total_expired_vps' => $total_expired_vps,
                            'total_expired_nat' => $total_expired_nat,
                          ];
                          $data[] = $n;
                          $data_expired[] = $e;
                      }
                      $total_vps = 0;
                      $total_nat = 0;
                      $total = 0;
                      $total_expired = 0;
                      $total_expired_vps = 0;
                      $total_expired_nat = 0;
                      foreach ($data_percents as $key => $data_percent) {
                        $total_vps += $data_percent['vps'];
                        $total_nat += $data_percent['nat'];
                        $total += $data_percent['total'];
                        $total_expired += $data_percent['total_expired'];
                        $total_expired_vps += $data_percent['total_expired_vps'];
                        $total_expired_nat += $data_percent['total_expired_nat'];
                      }

                      $total_vps = $total_vps == 0 ? 0 : round($total_vps * 100 / $total , 2);
                      $total_nat = $total_nat == 0? 0 : round($total_nat * 100 / $total , 2);
                      $total_expired_vps = $total_expired_vps == 0? 0 : round($total_expired_vps * 100 / $total_expired , 2);
                      $total_expired_nat = $total_expired_nat == 0? 0 : round($total_expired_nat * 100 / $total_expired , 2);

                      $result = [
                        'percent' => '(Sắp xếp tùy chọn theo tháng: ' . $total . ', VPS: ' . $total_vps . '%, NAT VPS: ' . $total_nat . '% )',
                        'percent_expired' => '(Sắp xếp tùy chọn theo tháng: ' . $total_expired . ', VPS: ' . $total_expired_vps . '%, NAT VPS: ' . $total_expired_nat . '% )',
                        'time' => $time,
                        'data' => $data,
                        'data_expired' => $data_expired,
                      ];

                      return $result;

                    break;

              }
          }

        }
        catch (\Exception $e) {
          report($e);
          return false;
        }

    }

    public function chart_hosting($type, $sort1, $sort2,$sort3)
    {
        $time = [];
        $data = [];
        $list_vps = $this->vps->get();
        $hostings = $this->hosting->get();
        $orders = $this->order->with('detail_orders', 'order_expireds')->get();
        if ($type == 'sort_select') {
            if (empty($sort1) && empty($sort2)) {
                for ($i=0; $i < 7; $i++) {
                    $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                    $date = Carbon::parse('this week')->addDays($i);
                    $date = $date->year . '-' . $date->month . '-' . $date->day;
                    $date_expired = $date;
                    $date_expired = new Carbon($date_expired);
                    $date = new Carbon($date);
                    $n = 0;
                    $e = 0;
                    $total_vn = 0;
                    $total_si = 0;
                    $total = 0;
                    $total_expired = 0;
                    $total_expired_vn = 0;
                    $total_expired_si = 0;
                    foreach ($hostings as $key => $hosting) {
                       if (!empty($hosting->date_create)) {
                           $create_at = date('Y-m-d', strtotime($hosting->date_create));
                           $create_at = new Carbon($create_at);
                           if ($date->diffInDays($create_at) == 0) {
                              $n++;
                              $total++;
                              if ($hosting->location == 'vn') {
                                 $total_vn++;
                              }
                              elseif ($hosting->location == 'si') {
                                 $total_si++;
                              }
                           }
                       }
                    }
                    foreach ($orders as $key => $order) {
                       if ($order->type == 'expired') {
                          if ( $order->order_expireds->count() > 0 ) {
                              foreach ($order->order_expireds as $key => $order_expired) {
                                if ($order_expired->type == 'hosting') {
                                  $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                  $create_at = new Carbon($create_at);
                                  if ($date_expired->diffInDays($create_at) == 0) {
                                    $e++;
                                    $total_expired++;
                                    if ($order_expired->hosting->location == 'vn') {
                                      $total_expired_vn++;
                                    }
                                    elseif ($order_expired->hosting->location == 'si') {
                                      $total_expired_si++;
                                    }
                                  }
                                }
                              }
                          }
                       }
                    }
                    $data_percents[] = [
                      'total' => $total,
                      'vn' => $total_vn,
                      'si' => $total_si,
                      'total_expired' => $total_expired,
                      'total_expired_vn' => $total_expired_vn,
                      'total_expired_si' => $total_expired_si,
                    ];
                    $data[] = $n;
                    $data_expired[] = $e;
                }

                $total_vn = 0;
                $total_si = 0;
                $total = 0;
                $total_expired = 0;
                $total_expired_vn = 0;
                $total_expired_si = 0;
                foreach ($data_percents as $key => $data_percent) {
                    $total_vn += $data_percent['vn'];
                    $total_si += $data_percent['si'];
                    $total += $data_percent['total'];
                    $total_expired += $data_percent['total_expired'];
                    $total_expired_vn += $data_percent['total_expired_vn'];
                    $total_expired_si += $data_percent['total_expired_si'];
                }

                $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                $result = [
                  'percent' => '(Tuần này: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                  'percent_expired' => '(Tuần này: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                  'time' => $time,
                  'data' => $data,
                  'data_expired' => $data_expired,
                ];
                return $result;
            }
        }
        elseif ($type == 'last_week')
        {
            for ($i=0; $i < 7; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('last week')->addDays($i)));
                $date = Carbon::parse('last week')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $date = new Carbon($date);
                $e = 0;
                $n = 0;
                $total_vn = 0;
                $total_si = 0;
                $total = 0;
                $total_expired = 0;
                $total_expired_vn = 0;
                $total_expired_si = 0;
                foreach ($hostings as $key => $hosting) {
                   if (!empty($hosting->date_create)) {
                       $create_at = date('Y-m-d', strtotime($hosting->date_create));
                       $create_at = new Carbon($create_at);
                       if ($date->diffInDays($create_at) == 0) {
                          $n++;
                          $total++;
                          if ($hosting->location == 'vn') {
                             $total_vn++;
                          }
                          elseif ($hosting->location == 'si') {
                             $total_si++;
                          }
                       }
                   }
                }

                foreach ($orders as $key => $order) {
                  if ($order->type == 'expired') {
                    if ( $order->order_expireds->count() > 0 ) {
                      foreach ($order->order_expireds as $key => $order_expired) {
                          if ($order_expired->type == 'hosting') {
                            $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date_expired->diffInDays($create_at) == 0) {
                              $e++;
                              $total_expired++;
                              if ($order_expired->hosting->location == 'vn') {
                                $total_expired_vn++;
                              }
                              elseif ($order_expired->hosting->location == 'si') {
                                $total_expired_si++;
                              }
                            }
                          }
                      }
                    }
                  }
                }
                $data_percents[] = [
                  'total' => $total,
                  'vn' => $total_vn,
                  'si' => $total_si,
                  'total_expired' => $total_expired,
                  'total_expired_vn' => $total_expired_vn,
                  'total_expired_si' => $total_expired_si,
                ];
                $data[] = $n;
                $data_expired[] = $e;

            }

            $total_vn = 0;
            $total_si = 0;
            $total = 0;
            $total_expired = 0;
            $total_expired_vn = 0;
            $total_expired_si = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total_vn += $data_percent['vn'];
              $total_si += $data_percent['si'];
              $total += $data_percent['total'];
              $total_expired += $data_percent['total_expired'];
              $total_expired_vn += $data_percent['total_expired_vn'];
              $total_expired_si += $data_percent['total_expired_si'];
            }

            $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
            $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
            $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
            $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

            $result = [
              'percent' => '(Tuần trước: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
              'percent_expired' => '(Tuần trước: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
              'time' => $time,
              'data' => $data,
              'data_expired' => $data_expired,
            ];

            return $result;
        }

        elseif ($type == 'last_month')
        {
            $first_day_last_month = Carbon::parse('first day of last month');
            $last_day_last_month = Carbon::parse('last day of last month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of last month')->addDays($i)));
                $date = Carbon::parse('first day of last month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $e = 0;
                $date = new Carbon($date);
                $n = 0;
                $total_vn = 0;
                $total_si = 0;
                $total = 0;
                $total_expired = 0;
                $total_expired_vn = 0;
                $total_expired_si = 0;

                foreach ($hostings as $key => $hosting) {
                   if (!empty($hosting->date_create)) {
                       $create_at = date('Y-m-d', strtotime($hosting->date_create));
                       $create_at = new Carbon($create_at);
                       if ($date->diffInDays($create_at) == 0) {
                          $n++;
                          $total++;
                          if ($hosting->location == 'vn') {
                             $total_vn++;
                          }
                          elseif ($hosting->location == 'si') {
                             $total_si++;
                          }
                       }
                   }
                }

                foreach ($orders as $key => $order) {
                  if ($order->type == 'expired') {
                    if ( $order->order_expireds->count() > 0 ) {
                      foreach ($order->order_expireds as $key => $order_expired) {
                        if ($order_expired->type == 'hosting') {
                            $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date_expired->diffInDays($create_at) == 0) {
                              $e++;
                              $total_expired++;
                              if ($order_expired->hosting->location == 'vn') {
                                $total_expired_vn++;
                              }
                              elseif ($order_expired->hosting->location == 'si') {
                                $total_expired_si++;
                              }
                            }
                          }
                      }
                    }
                  }
                }
                $data_percents[] = [
                  'total' => $total,
                  'vn' => $total_vn,
                  'si' => $total_si,
                  'total_expired' => $total_expired,
                  'total_expired_vn' => $total_expired_vn,
                  'total_expired_si' => $total_expired_si,
                ];
                $data[] = $n;
                $data_expired[] = $e;
            }

            $total_vn = 0;
            $total_si = 0;
            $total = 0;
            $total_expired = 0;
            $total_expired_vn = 0;
            $total_expired_si = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total_vn += $data_percent['vn'];
              $total_si += $data_percent['si'];
              $total += $data_percent['total'];
              $total_expired += $data_percent['total_expired'];
              $total_expired_vn += $data_percent['total_expired_vn'];
              $total_expired_si += $data_percent['total_expired_si'];
            }

            $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
            $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
            $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
            $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

            $result = [
              'percent' => '(Tháng trước: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
              'percent_expired' => '(Tháng trước: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
              'time' => $time,
              'data' => $data,
              'data_expired' => $data_expired,
            ];
            return $result;
        }

        elseif ($type == 'this_month')
        {
            $first_day_last_month = Carbon::parse('first day of this month');
            $last_day_last_month = Carbon::parse('last day of this month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                $date = Carbon::parse('first day of this month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $e = 0;
                $date = new Carbon($date);
                $n = 0;
                $total_vn = 0;
                $total_si = 0;
                $total = 0;
                $total_expired = 0;
                $total_expired_vn = 0;
                $total_expired_si = 0;

                foreach ($hostings as $key => $hosting) {
                  if (!empty($hosting->date_create)) {
                    $create_at = date('Y-m-d', strtotime($hosting->date_create));
                    $create_at = new Carbon($create_at);
                    if ($date->diffInDays($create_at) == 0) {
                      $n++;
                      $total++;
                      if ($hosting->location == 'vn') {
                        $total_vn++;
                      }
                      elseif ($hosting->location == 'si') {
                        $total_si++;
                      }
                    }
                  }
                }

                foreach ($orders as $key => $order) {
                  if ($order->type == 'expired') {
                    if ( $order->order_expireds->count() > 0 ) {
                      foreach ($order->order_expireds as $key => $order_expired) {
                        if ($order_expired->type == 'hosting') {
                            $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date_expired->diffInDays($create_at) == 0) {
                              $e++;
                              $total_expired++;
                              if ($order_expired->hosting->location == 'vn') {
                                $total_expired_vn++;
                              }
                              elseif ($order_expired->hosting->location == 'si') {
                                $total_expired_si++;
                              }
                            }
                          }
                      }
                    }
                  }
                }
                $data_percents[] = [
                  'total' => $total,
                  'vn' => $total_vn,
                  'si' => $total_si,
                  'total_expired' => $total_expired,
                  'total_expired_vn' => $total_expired_vn,
                  'total_expired_si' => $total_expired_si,
                ];
                $data[] = $n;
                $data_expired[] = $e;
            }

            $total_vn = 0;
            $total_si = 0;
            $total = 0;
            $total_expired = 0;
            $total_expired_vn = 0;
            $total_expired_si = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total_vn += $data_percent['vn'];
              $total_si += $data_percent['si'];
              $total += $data_percent['total'];
              $total_expired += $data_percent['total_expired'];
              $total_expired_vn += $data_percent['total_expired_vn'];
              $total_expired_si += $data_percent['total_expired_si'];
            }

            $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
            $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
            $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
            $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

            $result = [
              'percent' => '(Tháng này: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
              'percent_expired' => '(Tháng này: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
              'time' => $time,
              'data' => $data,
              'data_expired' => $data_expired,
            ];
            return $result;
        }

        elseif ($type == 'sort')
        {
            switch ( $sort1 ) {
              case 'day':
                  switch ($sort2) {

                    case 'week':
                        for ($i=0; $i < 7; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                            $date = Carbon::parse('this week')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $e = 0;
                            $date = new Carbon($date);
                            $n = 0;
                            $total_vn = 0;
                            $total_si = 0;
                            $total = 0;
                            $total_expired = 0;
                            $total_expired_vn = 0;
                            $total_expired_si = 0;

                            foreach ($hostings as $key => $hosting) {
                              if (!empty($hosting->date_create)) {
                                $create_at = date('Y-m-d', strtotime($hosting->date_create));
                                $create_at = new Carbon($create_at);
                                if ($date->diffInDays($create_at) == 0) {
                                  $n++;
                                  $total++;
                                  if ($hosting->location == 'vn') {
                                    $total_vn++;
                                  }
                                  elseif ($hosting->location == 'si') {
                                    $total_si++;
                                  }
                                }
                              }
                            }
                            foreach ($orders as $key => $order) {
                              if ($order->type == 'expired') {
                                if ( $order->order_expireds->count() > 0 ) {
                                  foreach ($order->order_expireds as $key => $order_expired) {
                                    if ($order_expired->type == 'hosting') {
                                      $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                      $create_at = new Carbon($create_at);
                                      if ($date_expired->diffInDays($create_at) == 0) {
                                        $e++;
                                        $total_expired++;
                                        if ($order_expired->hosting->location == 'vn') {
                                          $total_expired_vn++;
                                        }
                                        elseif ($order_expired->hosting->location == 'si') {
                                          $total_expired_si++;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'vn' => $total_vn,
                              'si' => $total_si,
                              'total_expired' => $total_expired,
                              'total_expired_vn' => $total_expired_vn,
                              'total_expired_si' => $total_expired_si,
                            ];
                            $data[] = $n;
                            $data_expired[] = $e;
                        }

                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total_vn += $data_percent['vn'];
                          $total_si += $data_percent['si'];
                          $total += $data_percent['total'];
                          $total_expired += $data_percent['total_expired'];
                          $total_expired_vn += $data_percent['total_expired_vn'];
                          $total_expired_si += $data_percent['total_expired_si'];
                        }

                        $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                        $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                        $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                        $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tuần: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                          'percent_expired' => '(Sắp xếp theo ngày trong tuần: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                          'time' => $time,
                          'data' => $data,
                          'data_expired' => $data_expired,
                        ];
                        return $result;
                      break;

                    case 'month':
                        $first_day_last_month = Carbon::parse('first day of this month');
                        $last_day_last_month = Carbon::parse('last day of this month');
                        $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                            $date = Carbon::parse('first day of this month')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $e = 0;
                            $date = new Carbon($date);
                            $n = 0;
                            $total_vn = 0;
                            $total_si = 0;
                            $total = 0;
                            $total_expired = 0;
                            $total_expired_vn = 0;
                            $total_expired_si = 0;

                            foreach ($hostings as $key => $hosting) {
                              if (!empty($hosting->date_create)) {
                                $create_at = date('Y-m-d', strtotime($hosting->date_create));
                                $create_at = new Carbon($create_at);
                                if ($date->diffInDays($create_at) == 0) {
                                  $n++;
                                  $total++;
                                  if ($hosting->location == 'vn') {
                                    $total_vn++;
                                  }
                                  elseif ($hosting->location == 'si') {
                                    $total_si++;
                                  }
                                }
                              }
                            }

                            foreach ($orders as $key => $order) {
                              if ($order->type == 'expired') {
                                if ( $order->order_expireds->count() > 0 ) {
                                  foreach ($order->order_expireds as $key => $order_expired) {
                                    if ($order_expired->type == 'hosting') {
                                      $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                      $create_at = new Carbon($create_at);
                                      if ($date_expired->diffInDays($create_at) == 0) {
                                        $e++;
                                        $total_expired++;
                                        if ($order_expired->hosting->location == 'vn') {
                                          $total_expired_vn++;
                                        }
                                        elseif ($order_expired->hosting->location == 'si') {
                                          $total_expired_si++;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }

                            $data_percents[] = [
                              'total' => $total,
                              'vn' => $total_vn,
                              'si' => $total_si,
                              'total_expired' => $total_expired,
                              'total_expired_vn' => $total_expired_vn,
                              'total_expired_si' => $total_expired_si,
                            ];
                            $data[] = $n;
                            $data_expired[] = $e;
                        }

                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total_vn += $data_percent['vn'];
                          $total_si += $data_percent['si'];
                          $total += $data_percent['total'];
                          $total_expired += $data_percent['total_expired'];
                          $total_expired_vn += $data_percent['total_expired_vn'];
                          $total_expired_si += $data_percent['total_expired_si'];
                        }

                        $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                        $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                        $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                        $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tháng: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                          'percent_expired' => '(Sắp xếp theo ngày trong tháng: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                          'time' => $time,
                          'data' => $data,
                          'data_expired' => $data_expired,
                        ];
                        return $result;
                      break;

                    case 'year':
                        $first_day_this_year = '1-1-' . Carbon::parse()->year;
                        $first_day_this_year = new Carbon($first_day_this_year);
                        $last_day_this_year = Carbon::parse('last day of this year');
                        $last_day_this_year = '31-12-' . Carbon::parse()->year;
                        $last_day_this_year = new Carbon($last_day_this_year);
                        $c = 0;
                        $t = $last_day_this_year->diffInDays($first_day_this_year) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $first_day_this_year = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year2 = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year = new Carbon($first_day_this_year);
                            $first_day_this_year2 = new Carbon($first_day_this_year2);
                            $time[] = date('d-m-Y', strtotime($first_day_this_year2->addDays($i)));
                            $date = $first_day_this_year->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $e = 0;
                            $date = new Carbon($date);
                            $n = 0;
                            $total_vn = 0;
                            $total_si = 0;
                            $total = 0;
                            $total_expired = 0;
                            $total_expired_vn = 0;
                            $total_expired_si = 0;

                            foreach ($hostings as $key => $hosting) {
                              if (!empty($hosting->date_create)) {
                                $create_at = date('Y-m-d', strtotime($hosting->date_create));
                                $create_at = new Carbon($create_at);
                                if ($date->diffInDays($create_at) == 0) {
                                  $n++;
                                  $total++;
                                  if ($hosting->location == 'vn') {
                                    $total_vn++;
                                  }
                                  elseif ($hosting->location == 'si') {
                                    $total_si++;
                                  }
                                }
                              }
                            }

                            foreach ($orders as $key => $order) {
                              if ($order->type == 'expired') {
                                if ( $order->order_expireds->count() > 0 ) {
                                  foreach ($order->order_expireds as $key => $order_expired) {
                                    if ($order_expired->type == 'hosting') {
                                      $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                      $create_at = new Carbon($create_at);
                                      if ($date_expired->diffInDays($create_at) == 0) {
                                        $e++;
                                        $total_expired++;
                                        if ($order_expired->hosting->location == 'vn') {
                                          $total_expired_vn++;
                                        }
                                        elseif ($order_expired->hosting->location == 'si') {
                                          $total_expired_si++;
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'vn' => $total_vn,
                              'si' => $total_si,
                              'total_expired' => $total_expired,
                              'total_expired_vn' => $total_expired_vn,
                              'total_expired_si' => $total_expired_si,
                            ];
                            $data[] = $n;
                            $data_expired[] = $e;
                        }
                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total_vn += $data_percent['vn'];
                          $total_si += $data_percent['si'];
                          $total += $data_percent['total'];
                          $total_expired += $data_percent['total_expired'];
                          $total_expired_vn += $data_percent['total_expired_vn'];
                          $total_expired_si += $data_percent['total_expired_si'];
                        }

                        $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                        $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                        $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                        $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong năm: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                          'percent_expired' => '(Sắp xếp theo ngày trong năm: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                          'time' => $time,
                          'data' => $data,
                          'data_expired' => $data_expired,
                        ];
                        // dd($result);
                        return $result;
                      break;

                  }
                break;

              case 'week':
                  switch ($sort2) {
                    case 'month':
                        // dd( Carbon::now()->month);
                        for ($i=0; $i < 4; $i++) {
                            $n = 0;
                            $e = 0;
                            $t = $i;
                            $first_sunday_of_month = date('d', strtotime(Carbon::parse('first sunday of this month')));
                            // lấy ngày bắt đầu và ngày kết thúc
                            if ($i == 0) {
                               $dayStar = 1;
                               $dayEnd = $first_sunday_of_month;
                            } elseif ($i == 3) {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               if ( Carbon::now()->month == 1 || Carbon::now()->month == 3 || Carbon::now()->month == 5
                                 || Carbon::now()->month == 7 || Carbon::now()->month == 8 || Carbon::now()->month == 10 || Carbon::now()->month == 12 ) {
                                 $dayEnd = 31;
                               }
                               else if ( Carbon::now()->month == 2 ) {
                                   $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                               } else {
                                 $dayEnd =  30 ;
                               }
                            } else {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               $dayEnd =   $first_sunday_of_month + ($i - 1) * 7 + 7;
                            }
                            $time[] = 'Tuần ' . ($t + 1) . ' (' . $dayStar . ' - ' . $dayEnd . ' / ' . Carbon::now()->month . ')';
                            $total_vn = 0;
                            $total_si = 0;
                            $total = 0;
                            $total_expired = 0;
                            $total_expired_vn = 0;
                            $total_expired_si = 0;

                            for ($j=$dayStar; $j <= $dayEnd; $j++) {
                                $date = Carbon::now()->year . '-' . Carbon::now()->month . '-' . $j;
                                $date_expired = $date;
                                $date_expired = new Carbon($date_expired);
                                $date = new Carbon($date);
                                foreach ($hostings as $key => $hosting) {
                                  if (!empty($hosting->date_create)) {
                                    $create_at = date('Y-m-d', strtotime($hosting->date_create));
                                    $create_at = new Carbon($create_at);
                                    if ($date->diffInDays($create_at) == 0) {
                                      $n++;
                                      $total++;
                                      if ($hosting->location == 'vn') {
                                        $total_vn++;
                                      }
                                      elseif ($hosting->location == 'si') {
                                        $total_si++;
                                      }
                                    }
                                  }
                                }
                                foreach ($orders as $key => $order) {
                                  if ($order->type == 'expired') {
                                    if ( $order->order_expireds->count() > 0 ) {
                                      foreach ($order->order_expireds as $key => $order_expired) {
                                        if ($order_expired->type == 'hosting') {
                                          $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                          $create_at = new Carbon($create_at);
                                          if ($date_expired->diffInDays($create_at) == 0) {
                                            $e++;
                                            $total_expired++;
                                            if ($order_expired->hosting->location == 'vn') {
                                              $total_expired_vn++;
                                            }
                                            elseif ($order_expired->hosting->location == 'si') {
                                              $total_expired_si++;
                                            }
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'vn' => $total_vn,
                              'si' => $total_si,
                              'total_expired' => $total_expired,
                              'total_expired_vn' => $total_expired_vn,
                              'total_expired_si' => $total_expired_si,
                            ];
                            $data[] = $n;
                            $data_expired[] = $e;
                        }

                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total_vn += $data_percent['vn'];
                          $total_si += $data_percent['si'];
                          $total += $data_percent['total'];
                          $total_expired += $data_percent['total_expired'];
                          $total_expired_vn += $data_percent['total_expired_vn'];
                          $total_expired_si += $data_percent['total_expired_si'];
                        }

                        $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                        $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                        $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                        $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong tháng: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                          'percent_expired' => '(Sắp xếp theo tuần trong tháng: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                          'time' => $time,
                          'data' => $data,
                          'data_expired' => $data_expired,
                        ];
                        // dd($result);
                        return $result;
                      break;

                    case 'year':
                        $current_week = Carbon::now()->weeksInYear;
                        $first_monday = date('d', strtotime(Carbon::parse('first Monday of January')));
                        $first_sunday = date('d', strtotime(Carbon::parse('first Sunday of January')));
                        if ($first_sunday < 7) {
                          $first_sunday += 7;
                        }
                        for ($i=0; $i < $current_week; $i++) {
                             $first_day = '1-1-'.Carbon::now()->year;
                             $t =  $i;
                             $n = 0;
                             $e = 0;
                             $total_vn = 0;
                             $total_si = 0;
                             $total = 0;
                             $total_expired = 0;
                             $total_expired_vn = 0;
                             $total_expired_si = 0;
                             $time[] = 'Tuần ' . ($t + 1);
                             if ($i == 0) {
                                $dayStar = 1;
                                if ($first_sunday < 7) {
                                   $dayEnd = $first_sunday + 6;
                                } else {
                                   $dayEnd = $first_sunday;
                                }
                             }
                             elseif ( $i == $current_week -1 ) {
                                 $dayStar = $i * 7 + 6;
                                 $dayEnd = Carbon::now()->isLeapYear() ? 366 : 365;
                             } else {
                                 $dayStar = $first_monday + $i * 7;
                                 $dayEnd = $first_monday + $i * 7 + 6;
                             }

                             for ($j = $dayStar; $j <= $dayEnd; $j++) {
                                 $k = $j;
                                 $k = ($k-1) * 60 * 60 * 24;
                                 $day = $first_day;
                                 $day = date('d-m-Y', strtotime($day) + $k);
                                 $date_expired = $day;
                                 $date_expired = new Carbon($date_expired);
                                 $date = new Carbon($day);
                                 foreach ($hostings as $key => $hosting) {
                                   if (!empty($hosting->date_create)) {
                                     $create_at = date('Y-m-d', strtotime($hosting->date_create));
                                     $create_at = new Carbon($create_at);
                                     if ($date->diffInDays($create_at) == 0) {
                                       $n++;
                                       $total++;
                                       if ($hosting->location == 'vn') {
                                         $total_vn++;
                                       }
                                       elseif ($hosting->location == 'si') {
                                         $total_si++;
                                       }
                                     }
                                   }
                                 }

                                 foreach ($orders as $key => $order) {
                                   if ($order->type == 'expired') {
                                     if ( $order->order_expireds->count() > 0 ) {
                                       foreach ($order->order_expireds as $key => $order_expired) {
                                         if ($order_expired->type == 'hosting') {
                                           $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                           $create_at = new Carbon($create_at);
                                           if ($date_expired->diffInDays($create_at) == 0) {
                                             $e++;
                                             $total_expired++;
                                             if ($order_expired->hosting->location == 'vn') {
                                               $total_expired_vn++;
                                             }
                                             elseif ($order_expired->hosting->location == 'si') {
                                               $total_expired_si++;
                                             }
                                           }
                                         }
                                       }
                                     }
                                   }
                                 }


                             }
                             $data_percents[] = [
                               'total' => $total,
                               'vn' => $total_vn,
                               'si' => $total_si,
                               'total_expired' => $total_expired,
                               'total_expired_vn' => $total_expired_vn,
                               'total_expired_si' => $total_expired_si,
                             ];

                             $data[] = $n;
                             $data_expired[] = $e;
                        }

                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total_vn += $data_percent['vn'];
                          $total_si += $data_percent['si'];
                          $total += $data_percent['total'];
                          $total_expired += $data_percent['total_expired'];
                          $total_expired_vn += $data_percent['total_expired_vn'];
                          $total_expired_si += $data_percent['total_expired_si'];
                        }

                        $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                        $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                        $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                        $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong năm: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                          'percent_expired' => '(Sắp xếp theo tuần trong năm: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                          'time' => $time,
                          'data' => $data,
                          'data_expired' => $data_expired,
                        ];

                        return $result;
                      break;

                  }
                break;

              case 'month':
                  for ($i=1; $i <= 12; $i++) {
                      $dayStar = 1;
                      $n = 0;
                      $e = 0;
                      $total_vn = 0;
                      $total_si = 0;
                      $total = 0;
                      $total_expired = 0;
                      $total_expired_vn = 0;
                      $total_expired_si = 0;
                      $time[] = 'Tháng ' . $i;
                      if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 8 || $i == 10 || $i == 12 ) {
                        $dayEnd = 31;
                      }
                      else if ( $i == 2 ) {
                          $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                      } else {
                          $dayEnd =  30 ;
                      }

                      for ($j=$dayStar; $j <= $dayEnd; $j++) {
                          $date =  Carbon::now()->year . '-' . $i . '-' . $j;
                          $date_expired = $date;
                          $date_expired = new Carbon($date_expired);
                          $date = new Carbon($date);
                          foreach ($hostings as $key => $hosting) {
                            if (!empty($hosting->date_create)) {
                              $create_at = date('Y-m-d', strtotime($hosting->date_create));
                              $create_at = new Carbon($create_at);
                              if ($date->diffInDays($create_at) == 0) {
                                $n++;
                                $total++;
                                if ($hosting->location == 'vn') {
                                  $total_vn++;
                                }
                                elseif ($hosting->location == 'si') {
                                  $total_si++;
                                }
                              }
                            }
                          }


                          foreach ($orders as $key => $order) {
                            if ($order->type == 'expired') {
                              if ( $order->order_expireds->count() > 0 ) {
                                foreach ($order->order_expireds as $key => $order_expired) {
                                  if ($order_expired->type == 'hosting') {
                                    $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                    $create_at = new Carbon($create_at);
                                    if ($date_expired->diffInDays($create_at) == 0) {
                                      $e++;
                                      $total_expired++;
                                      if ($order_expired->hosting->location == 'vn') {
                                        $total_expired_vn++;
                                      }
                                      elseif ($order_expired->hosting->location == 'si') {
                                        $total_expired_si++;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                      }
                      $data_percents[] = [
                        'total' => $total,
                        'vn' => $total_vn,
                        'si' => $total_si,
                        'total_expired' => $total_expired,
                        'total_expired_vn' => $total_expired_vn,
                        'total_expired_si' => $total_expired_si,
                      ];
                      $data[] = $n;
                      $data_expired[] = $e;
                  }
                  $total_vn = 0;
                  $total_si = 0;
                  $total = 0;
                  $total_expired = 0;
                  $total_expired_vn = 0;
                  $total_expired_si = 0;
                  foreach ($data_percents as $key => $data_percent) {
                    $total_vn += $data_percent['vn'];
                    $total_si += $data_percent['si'];
                    $total += $data_percent['total'];
                    $total_expired += $data_percent['total_expired'];
                    $total_expired_vn += $data_percent['total_expired_vn'];
                    $total_expired_si += $data_percent['total_expired_si'];
                  }

                  $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                  $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                  $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                  $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                  $result = [
                    'percent' => '(Sắp xếp theo tháng trong năm: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                    'percent_expired' => '(Sắp xếp theo tháng trong năm: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                    'time' => $time,
                    'data' => $data,
                    'data_expired' => $data_expired,
                  ];
                  // dd($result);
                  return $result;

                break;

            }
        }

        elseif ($type == 'custom')
        {
            switch ($sort1) {
                case 'day':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $diff_day = $dayEnd;
                    $diff_day = $diff_day->diffInDays($dayStar);
                    if ($diff_day < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($diff_day); $i++) {
                        $day_start =  new Carbon($sort2);
                        $date = $day_start;
                        $date = $date->addDays($i);
                        $time[] = date( 'd-m-Y', strtotime($date) );
                        $date = $date->year . '-' . $date->month . '-' . $date->day;
                        $date_expired = $date;
                        $date_expired = new Carbon($date_expired);
                        $e = 0;
                        $date = new Carbon($date);
                        $n = 0;
                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;

                        foreach ($hostings as $key => $hosting) {
                          if (!empty($hosting->date_create)) {
                            $create_at = date('Y-m-d', strtotime($hosting->date_create));
                            $create_at = new Carbon($create_at);
                            if ($date->diffInDays($create_at) == 0) {
                              $n++;
                              $total++;
                              if ($hosting->location == 'vn') {
                                $total_vn++;
                              }
                              elseif ($hosting->location == 'si') {
                                $total_si++;
                              }
                            }
                          }
                        }
                        foreach ($orders as $key => $order) {
                          if ($order->type == 'expired') {
                            if ( $order->order_expireds->count() > 0 ) {
                              foreach ($order->order_expireds as $key => $order_expired) {
                                if ($order_expired->type == 'hosting') {
                                  $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                  $create_at = new Carbon($create_at);
                                  if ($date_expired->diffInDays($create_at) == 0) {
                                    $e++;
                                    $total_expired++;
                                    if ($order_expired->hosting->location == 'vn') {
                                      $total_expired_vn++;
                                    }
                                    elseif ($order_expired->hosting->location == 'si') {
                                      $total_expired_si++;
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                        $data_percents[] = [
                          'total' => $total,
                          'vn' => $total_vn,
                          'si' => $total_si,
                          'total_expired' => $total_expired,
                          'total_expired_vn' => $total_expired_vn,
                          'total_expired_si' => $total_expired_si,
                        ];
                        $data[] = $n;
                        $data_expired[] = $e;
                    }

                    $total_vn = 0;
                    $total_si = 0;
                    $total = 0;
                    $total_expired = 0;
                    $total_expired_vn = 0;
                    $total_expired_si = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total_vn += $data_percent['vn'];
                      $total_si += $data_percent['si'];
                      $total += $data_percent['total'];
                      $total_expired += $data_percent['total_expired'];
                      $total_expired_vn += $data_percent['total_expired_vn'];
                      $total_expired_si += $data_percent['total_expired_si'];
                    }

                    $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                    $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                    $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                    $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo ngày: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                      'percent_expired' => '(Sắp xếp tùy chọn theo ngày: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                      'time' => $time,
                      'data' => $data,
                      'data_expired' => $data_expired,
                    ];
                    // dd($result);
                    return $result;
                  break;

                case 'week':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->weekOfYear - $dayStar->weekOfYear;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= $current ; $i++) {
                       $n = 0;
                       $e = 0;
                       $total_vn = 0;
                       $total_si = 0;
                       $total = 0;
                       $total_expired = 0;
                       $total_expired_vn = 0;
                       $total_expired_si = 0;;
                       $a = $i;
                       $day_star =  new Carbon($sort2);
                       $m = 7 - $day_star->dayOfWeek;
                       if ($i == 0) {
                          $day1 = $day_star;
                          $t = $day_star->year . '-' . $day_star->month . '-' . $day_star->day;
                          $t = new Carbon($t);
                          $day2 = $t->addDays($m);
                       } elseif ($i == $n ) {
                          $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i-1) * 7 ) + 1 );
                          $day2 =  $dayEnd;
                       } else {
                         $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i - 1) * 7 ) + 1 );
                         $t = $day1->year . '-' . $day1->month . '-' . $day1->day;
                         $t = new Carbon($t);
                         $day2 = $t->addDays(6);
                       }
                       $time[] = 'Tuần ' . ($a + 1) . ' (' . date('d', strtotime($day1)) . ' - ' . date('d', strtotime($day2)) . ' / '. $day_star->month .' )' ;
                       $k = $day2->diffInDays($day1);
                       for ($j=0; $j <= $k; $j++) {
                          $date =  $day1->year . '-' . $day1->month . '-' . $day1->day;
                          $date = new Carbon($date);
                          $date = $date->addDays($j);
                          $date_expired =  $date->year . '-' . $date->month . '-' . $date->day;
                          $date_expired = new Carbon($date_expired);
                          foreach ($hostings as $key => $hosting) {
                            if (!empty($hosting->date_create)) {
                              $create_at = date('Y-m-d', strtotime($hosting->date_create));
                              $create_at = new Carbon($create_at);
                              if ($date->diffInDays($create_at) == 0) {
                                $n++;
                                $total++;
                                if ($hosting->location == 'vn') {
                                  $total_vn++;
                                }
                                elseif ($hosting->location == 'si') {
                                  $total_si++;
                                }
                              }
                            }
                          }
                          foreach ($orders as $key => $order) {
                            if ($order->type == 'expired') {
                              if ( $order->order_expireds->count() > 0 ) {
                                foreach ($order->order_expireds as $key => $order_expired) {
                                  if ($order_expired->type == 'hosting') {
                                    $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                    $create_at = new Carbon($create_at);
                                    if ($date_expired->diffInDays($create_at) == 0) {
                                      $e++;
                                      $total_expired++;
                                      if ($order_expired->hosting->location == 'vn') {
                                        $total_expired_vn++;
                                      }
                                      elseif ($order_expired->hosting->location == 'si') {
                                        $total_expired_si++;
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                       }
                       $data_percents[] = [
                         'total' => $total,
                         'vn' => $total_vn,
                         'si' => $total_si,
                         'total_expired' => $total_expired,
                         'total_expired_vn' => $total_expired_vn,
                         'total_expired_si' => $total_expired_si,
                       ];
                       $data[] = $n;
                       $data_expired[] = $e;
                    }

                    $total_vn = 0;
                    $total_si = 0;
                    $total = 0;
                    $total_expired = 0;
                    $total_expired_vn = 0;
                    $total_expired_si = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total_vn += $data_percent['vn'];
                      $total_si += $data_percent['si'];
                      $total += $data_percent['total'];
                      $total_expired += $data_percent['total_expired'];
                      $total_expired_vn += $data_percent['total_expired_vn'];
                      $total_expired_si += $data_percent['total_expired_si'];
                    }

                    $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                    $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                    $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                    $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tuần: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                      'percent_expired' => '(Sắp xếp tùy chọn theo tuần: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                      'time' => $time,
                      'data' => $data,
                      'data_expired' => $data_expired,
                    ];
                    // dd($result);
                    return $result;

                  break;

                case 'month':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->month - $dayStar->month;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($current) ; $i++) {
                        $day_time = new Carbon($dayStar);
                        $n = 0;
                        $e = 0;
                        $total_vn = 0;
                        $total_si = 0;
                        $total = 0;
                        $total_expired = 0;
                        $total_expired_vn = 0;
                        $total_expired_si = 0;

                        $time[] = 'Tháng ' . $day_time->addMonths($i)->month;
                        if ($i == 0) {
                            $day_start =  new Carbon($dayStar);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . $day_start->day;
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        } elseif ($i == $current) {
                            $day_end = new Carbon($dayEnd);
                            $day1 = $day_end->year . '-' . $day_end->month . '-' . '1';
                            $day2 = $day_end->year . '-' . $day_end->month . '-' . $day_end->day;
                        } else {
                            $day_start =  new Carbon($sort2);
                            $day_start = $day_start->addMonths($i);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . '1';
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        }

                        $day1 = new Carbon($day1);
                        $day2 = new Carbon($day2);
                        $diff_day = $day2->diffInDays($day1);

                        for ($j=0; $j <= $diff_day ; $j++) {
                           $date = $day1->year . '-' . $day1->month . '-' . '1';
                           $date = new Carbon($date);
                           $date = $date->addDays($j);
                           $date_expired = $date->year . '-' . $date->month . '-' . $date->day;
                           $date_expired = new Carbon($date_expired);
                           foreach ($hostings as $key => $hosting) {
                             if (!empty($hosting->date_create)) {
                               $create_at = date('Y-m-d', strtotime($hosting->date_create));
                               $create_at = new Carbon($create_at);
                               if ($date->diffInDays($create_at) == 0) {
                                 $n++;
                                 $total++;
                                 if ($hosting->location == 'vn') {
                                   $total_vn++;
                                 }
                                 elseif ($hosting->location == 'si') {
                                   $total_si++;
                                 }
                               }
                             }
                           }

                           foreach ($orders as $key => $order) {
                             if ($order->type == 'expired') {
                               if ( $order->order_expireds->count() > 0 ) {
                                 foreach ($order->order_expireds as $key => $order_expired) {
                                   if ($order_expired->type == 'hosting') {
                                     $create_at = date('Y-m-d', strtotime($order_expired->created_at));
                                     $create_at = new Carbon($create_at);
                                     if ($date_expired->diffInDays($create_at) == 0) {
                                       $e++;
                                       $total_expired++;
                                       if ($order_expired->hosting->location == 'vn') {
                                         $total_expired_vn++;
                                       }
                                       elseif ($order_expired->hosting->location == 'si') {
                                         $total_expired_si++;
                                       }
                                     }
                                   }
                                 }
                               }
                             }
                           }
                        }
                        $data_percents[] = [
                          'total' => $total,
                          'vn' => $total_vn,
                          'si' => $total_si,
                          'total_expired' => $total_expired,
                          'total_expired_vn' => $total_expired_vn,
                          'total_expired_si' => $total_expired_si,
                        ];
                        $data[] = $n;
                        $data_expired[] = $e;
                    }
                    $total_vn = 0;
                    $total_si = 0;
                    $total = 0;
                    $total_expired = 0;
                    $total_expired_vn = 0;
                    $total_expired_si = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total_vn += $data_percent['vn'];
                      $total_si += $data_percent['si'];
                      $total += $data_percent['total'];
                      $total_expired += $data_percent['total_expired'];
                      $total_expired_vn += $data_percent['total_expired_vn'];
                      $total_expired_si += $data_percent['total_expired_si'];
                    }

                    $total_vn = $total_vn == 0 ? 0 : round($total_vn * 100 / $total , 2);
                    $total_si = $total_si == 0? 0 : round($total_si * 100 / $total , 2);
                    $total_expired_vn = $total_expired_vn == 0? 0 : round($total_expired_vn * 100 / $total_expired , 2);
                    $total_expired_si = $total_expired_si == 0? 0 : round($total_expired_si * 100 / $total_expired , 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tháng: ' . $total . ', VN: ' . $total_vn . '%, SI: ' . $total_si . '% )',
                      'percent_expired' => '(Sắp xếp tùy chọn theo tháng: ' . $total_expired . ', VN: ' . $total_expired_vn . '%, SI: ' . $total_expired_si . '% )',
                      'time' => $time,
                      'data' => $data,
                      'data_expired' => $data_expired,
                    ];

                    return $result;

                  break;

            }
        }

    }

    public function chart_payment($type, $sort1, $sort2,$sort3)
    {
        $time = [];
        $data = [];
        $payments = $this->history_pay->where('type_gd' , 1)->where('status', 'Active')->get();
        if ($type == 'sort_select') {
            if (empty($sort1) && empty($sort2)) {
                for ($i=0; $i < 7; $i++) {
                    $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                    $date = Carbon::parse('this week')->addDays($i);
                    $date = $date->year . '-' . $date->month . '-' . $date->day;
                    $date_expired = $date;
                    $date_expired = new Carbon($date_expired);
                    $date = new Carbon($date);
                    $total = 0;
                    $total_momo = 0;
                    $total_pay_in_office = 0;
                    $total_bank_transfer_techcombank = 0;
                    $total_bank_transfer_vietcombank = 0;
                    foreach ($payments as $key => $payment) {
                      $create_at = date('Y-m-d', strtotime($payment->created_at));
                      $create_at = new Carbon($create_at);
                      if ($date->diffInDays($create_at) == 0) {
                        $total += $payment->money;
                        if ($payment->method_gd == 'momo') {
                          $total_momo += $payment->money;
                        }
                        elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                          $total_bank_transfer_vietcombank += $payment->money;
                        }
                        elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                          $total_bank_transfer_techcombank += $payment->money;
                        } else {
                          $total_pay_in_office += $payment->money;
                        }
                      }
                    }
                    $data_percents[] = [
                      'total' => $total,
                      'total_off' => $total_pay_in_office,
                      'total_momo' => $total_momo,
                      'total_vcb' => $total_bank_transfer_vietcombank,
                      'total_tcb' => $total_bank_transfer_techcombank,
                    ];
                    $data[] = $total;
                }

                $total_momo = 0;
                $total_pay_in_office = 0;
                $total_bank_transfer_techcombank = 0;
                $total_bank_transfer_vietcombank = 0;
                $total = 0;
                foreach ($data_percents as $key => $data_percent) {
                  $total += $data_percent['total'];
                  $total_pay_in_office += $data_percent['total_off'];
                  $total_momo += $data_percent['total_momo'];
                  $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                  $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                }

                $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total , 2);
                $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total , 2);

                $result = [
                  'percent' => '(Tuần này: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                  'time' => $time,
                  'data' => $data,
                ];
                return $result;
            }
        }

        elseif ($type == 'last_week')
        {
            for ($i=0; $i < 7; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('last week')->addDays($i)));
                $date = Carbon::parse('last week')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $date = new Carbon($date);
                $total = 0;
                $total_momo = 0;
                $total_pay_in_office = 0;
                $total_bank_transfer_techcombank = 0;
                $total_bank_transfer_vietcombank = 0;
                foreach ($payments as $key => $payment) {
                    $create_at = date('Y-m-d', strtotime($payment->created_at));
                    $create_at = new Carbon($create_at);
                    if ($date->diffInDays($create_at) == 0) {
                       $total += $payment->money;
                       if ($payment->method_gd == 'momo') {
                          $total_momo += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                          $total_bank_transfer_vietcombank += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                          $total_bank_transfer_techcombank += $payment->money;
                       } else {
                         $total_pay_in_office += $payment->money;
                       }
                    }
                }
                $data_percents[] = [
                  'total' => $total,
                  'total_off' => $total_pay_in_office,
                  'total_momo' => $total_momo,
                  'total_vcb' => $total_bank_transfer_vietcombank,
                  'total_tcb' => $total_bank_transfer_techcombank,
                ];
                $data[] = $total;
            }

            $total_momo = 0;
            $total_pay_in_office = 0;
            $total_bank_transfer_techcombank = 0;
            $total_bank_transfer_vietcombank = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total += $data_percent['total'];
              $total_pay_in_office += $data_percent['total_off'];
              $total_momo += $data_percent['total_momo'];
              $total_bank_transfer_techcombank += $data_percent['total_vcb'];
              $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
            }

            $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
            $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
            $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total , 2);
            $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total , 2);

            $result = [
              'percent' => '(Tuần trước: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
              'time' => $time,
              'data' => $data,
            ];

            return $result;
        }

        elseif ($type == 'last_month')
        {
            $first_day_last_month = Carbon::parse('first day of last month');
            $last_day_last_month = Carbon::parse('last day of last month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of last month')->addDays($i)));
                $date = Carbon::parse('first day of last month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $date = new Carbon($date);
                $total = 0;
                $total_momo = 0;
                $total_pay_in_office = 0;
                $total_bank_transfer_techcombank = 0;
                $total_bank_transfer_vietcombank = 0;

                foreach ($payments as $key => $payment) {
                    $create_at = date('Y-m-d', strtotime($payment->created_at));
                    $create_at = new Carbon($create_at);
                    if ($date->diffInDays($create_at) == 0) {
                       $total += $payment->money;
                       if ($payment->method_gd == 'momo') {
                          $total_momo += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                          $total_bank_transfer_vietcombank += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                          $total_bank_transfer_techcombank += $payment->money;
                       } else {
                         $total_pay_in_office += $payment->money;
                       }
                    }
                }
                $data_percents[] = [
                  'total' => $total,
                  'total_off' => $total_pay_in_office,
                  'total_momo' => $total_momo,
                  'total_vcb' => $total_bank_transfer_vietcombank,
                  'total_tcb' => $total_bank_transfer_techcombank,
                ];
                $data[] = $total;
            }

            $total_momo = 0;
            $total_pay_in_office = 0;
            $total_bank_transfer_techcombank = 0;
            $total_bank_transfer_vietcombank = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total += $data_percent['total'];
              $total_pay_in_office += $data_percent['total_off'];
              $total_momo += $data_percent['total_momo'];
              $total_bank_transfer_techcombank += $data_percent['total_vcb'];
              $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
            }

            $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
            $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
            $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total , 2);
            $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total , 2);

            $result = [
              'percent' => '(Tháng trước: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
              'time' => $time,
              'data' => $data,
            ];
            return $result;
        }

        elseif ($type == 'this_month')
        {
            $first_day_last_month = Carbon::parse('first day of this month');
            $last_day_last_month = Carbon::parse('last day of this month');
            $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
            for ($i=0; $i < $t; $i++) {
                $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                $date = Carbon::parse('first day of this month')->addDays($i);
                $date = $date->year . '-' . $date->month . '-' . $date->day;
                $date_expired = $date;
                $date_expired = new Carbon($date_expired);
                $date = new Carbon($date);
                $total = 0;
                $total_momo = 0;
                $total_pay_in_office = 0;
                $total_bank_transfer_techcombank = 0;
                $total_bank_transfer_vietcombank = 0;

                foreach ($payments as $key => $payment) {
                    $create_at = date('Y-m-d', strtotime($payment->created_at));
                    $create_at = new Carbon($create_at);
                    if ($date->diffInDays($create_at) == 0) {
                       $total += $payment->money;
                       if ($payment->method_gd == 'momo') {
                          $total_momo += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                          $total_bank_transfer_vietcombank += $payment->money;
                       }
                       elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                          $total_bank_transfer_techcombank += $payment->money;
                       } else {
                         $total_pay_in_office += $payment->money;
                       }
                    }
                }
                $data_percents[] = [
                  'total' => $total,
                  'total_off' => $total_pay_in_office,
                  'total_momo' => $total_momo,
                  'total_vcb' => $total_bank_transfer_vietcombank,
                  'total_tcb' => $total_bank_transfer_techcombank,
                ];
                $data[] = $total;
            }

            $total_momo = 0;
            $total_pay_in_office = 0;
            $total_bank_transfer_techcombank = 0;
            $total_bank_transfer_vietcombank = 0;
            $total = 0;
            foreach ($data_percents as $key => $data_percent) {
              $total += $data_percent['total'];
              $total_pay_in_office += $data_percent['total_off'];
              $total_momo += $data_percent['total_momo'];
              $total_bank_transfer_techcombank += $data_percent['total_vcb'];
              $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
            }

            $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
            $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
            $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
            $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

            $result = [
              'percent' => '(Tháng này: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
              'time' => $time,
              'data' => $data,
            ];
            return $result;
        }

        elseif ($type == 'sort')
        {
            switch ( $sort1 ) {
              case 'day':
                  switch ($sort2) {

                    case 'week':
                        for ($i=0; $i < 7; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('this week')->addDays($i)));
                            $date = Carbon::parse('this week')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $date = new Carbon($date);
                            $total = 0;
                            $total_momo = 0;
                            $total_pay_in_office = 0;
                            $total_bank_transfer_techcombank = 0;
                            $total_bank_transfer_vietcombank = 0;

                            foreach ($payments as $key => $payment) {
                              $create_at = date('Y-m-d', strtotime($payment->created_at));
                              $create_at = new Carbon($create_at);
                              if ($date->diffInDays($create_at) == 0) {
                                $total += $payment->money;
                                if ($payment->method_gd == 'momo') {
                                  $total_momo += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                  $total_bank_transfer_vietcombank += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                  $total_bank_transfer_techcombank += $payment->money;
                                } else {
                                  $total_pay_in_office += $payment->money;
                                }
                              }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'total_off' => $total_pay_in_office,
                              'total_momo' => $total_momo,
                              'total_vcb' => $total_bank_transfer_vietcombank,
                              'total_tcb' => $total_bank_transfer_techcombank,
                            ];
                            $data[] = $total;

                        }

                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total += $data_percent['total'];
                          $total_pay_in_office += $data_percent['total_off'];
                          $total_momo += $data_percent['total_momo'];
                          $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                          $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                        }

                        $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                        $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                        $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                        $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tuần: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        return $result;
                      break;

                    case 'month':
                        $first_day_last_month = Carbon::parse('first day of this month');
                        $last_day_last_month = Carbon::parse('last day of this month');
                        $t = $last_day_last_month->diffInDays($first_day_last_month) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $time[] = date('d-m-Y', strtotime(Carbon::parse('first day of this month')->addDays($i)));
                            $date = Carbon::parse('first day of this month')->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $date = new Carbon($date);
                            $total = 0;
                            $total_momo = 0;
                            $total_pay_in_office = 0;
                            $total_bank_transfer_techcombank = 0;
                            $total_bank_transfer_vietcombank = 0;

                            foreach ($payments as $key => $payment) {
                              $create_at = date('Y-m-d', strtotime($payment->created_at));
                              $create_at = new Carbon($create_at);
                              if ($date->diffInDays($create_at) == 0) {
                                $total += $payment->money;
                                if ($payment->method_gd == 'momo') {
                                  $total_momo += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                  $total_bank_transfer_vietcombank += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                  $total_bank_transfer_techcombank += $payment->money;
                                } else {
                                  $total_pay_in_office += $payment->money;
                                }
                              }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'total_off' => $total_pay_in_office,
                              'total_momo' => $total_momo,
                              'total_vcb' => $total_bank_transfer_vietcombank,
                              'total_tcb' => $total_bank_transfer_techcombank,
                            ];
                            $data[] = $total;
                        }

                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total += $data_percent['total'];
                          $total_pay_in_office += $data_percent['total_off'];
                          $total_momo += $data_percent['total_momo'];
                          $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                          $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                        }

                        $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                        $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                        $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                        $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong tháng: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        return $result;
                      break;

                    case 'year':
                        $first_day_this_year = '1-1-' . Carbon::parse()->year;
                        $first_day_this_year = new Carbon($first_day_this_year);
                        $last_day_this_year = Carbon::parse('last day of this year');
                        $last_day_this_year = '31-12-' . Carbon::parse()->year;
                        $last_day_this_year = new Carbon($last_day_this_year);
                        $c = 0;
                        $t = $last_day_this_year->diffInDays($first_day_this_year) + 1;
                        for ($i=0; $i < $t; $i++) {
                            $first_day_this_year = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year2 = '1-1-' . Carbon::parse()->year;
                            $first_day_this_year = new Carbon($first_day_this_year);
                            $first_day_this_year2 = new Carbon($first_day_this_year2);
                            $time[] = date('d-m-Y', strtotime($first_day_this_year2->addDays($i)));
                            $date = $first_day_this_year->addDays($i);
                            $date = $date->year . '-' . $date->month . '-' . $date->day;
                            $date_expired = $date;
                            $date_expired = new Carbon($date_expired);
                            $date = new Carbon($date);
                            $total = 0;
                            $total_momo = 0;
                            $total_pay_in_office = 0;
                            $total_bank_transfer_techcombank = 0;
                            $total_bank_transfer_vietcombank = 0;

                            foreach ($payments as $key => $payment) {
                              $create_at = date('Y-m-d', strtotime($payment->created_at));
                              $create_at = new Carbon($create_at);
                              if ($date->diffInDays($create_at) == 0) {
                                $total += $payment->money;
                                if ($payment->method_gd == 'momo') {
                                  $total_momo += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                  $total_bank_transfer_vietcombank += $payment->money;
                                }
                                elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                  $total_bank_transfer_techcombank += $payment->money;
                                } else {
                                  $total_pay_in_office += $payment->money;
                                }
                              }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'total_off' => $total_pay_in_office,
                              'total_momo' => $total_momo,
                              'total_vcb' => $total_bank_transfer_vietcombank,
                              'total_tcb' => $total_bank_transfer_techcombank,
                            ];
                            $data[] = $total;

                        }
                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total += $data_percent['total'];
                          $total_pay_in_office += $data_percent['total_off'];
                          $total_momo += $data_percent['total_momo'];
                          $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                          $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                        }

                        $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                        $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                        $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                        $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                        $result = [
                          'percent' => '(Sắp xếp theo ngày trong năm: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        // dd($result);
                        return $result;
                      break;

                  }
                break;

              case 'week':
                  switch ($sort2) {
                    case 'month':
                        // dd( Carbon::now()->month);
                        for ($i=0; $i < 4; $i++) {
                            $n = 0;
                            $e = 0;
                            $t = $i;
                            $first_sunday_of_month = date('d', strtotime(Carbon::parse('first sunday of this month')));
                            // lấy ngày bắt đầu và ngày kết thúc
                            if ($i == 0) {
                               $dayStar = 1;
                               $dayEnd = $first_sunday_of_month;
                            } elseif ($i == 3) {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               if ( Carbon::now()->month == 1 || Carbon::now()->month == 3 || Carbon::now()->month == 5
                                 || Carbon::now()->month == 7 || Carbon::now()->month == 8 || Carbon::now()->month == 10 || Carbon::now()->month == 12 ) {
                                 $dayEnd = 31;
                               }
                               else if ( Carbon::now()->month == 2 ) {
                                   $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                               } else {
                                 $dayEnd =  30 ;
                               }
                            } else {
                               $dayStar = ($first_sunday_of_month + 1) +  ($i - 1) * 7;
                               $dayEnd =   $first_sunday_of_month + ($i - 1) * 7 + 7;
                            }
                            $time[] = 'Tuần ' . ($t + 1) . ' (' . $dayStar . ' - ' . $dayEnd . ' / ' . Carbon::now()->month . ')';
                            $total = 0;
                            $total_momo = 0;
                            $total_pay_in_office = 0;
                            $total_bank_transfer_techcombank = 0;
                            $total_bank_transfer_vietcombank = 0;

                            for ($j=$dayStar; $j <= $dayEnd; $j++) {
                                $date = Carbon::now()->year . '-' . Carbon::now()->month . '-' . $j;
                                $date_expired = $date;
                                $date_expired = new Carbon($date_expired);
                                $date = new Carbon($date);
                                foreach ($payments as $key => $payment) {
                                  $create_at = date('Y-m-d', strtotime($payment->created_at));
                                  $create_at = new Carbon($create_at);
                                  if ($date->diffInDays($create_at) == 0) {
                                    $total += $payment->money;
                                    if ($payment->method_gd == 'momo') {
                                      $total_momo += $payment->money;
                                    }
                                    elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                      $total_bank_transfer_vietcombank += $payment->money;
                                    }
                                    elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                      $total_bank_transfer_techcombank += $payment->money;
                                    } else {
                                      $total_pay_in_office += $payment->money;
                                    }
                                  }
                                }
                            }
                            $data_percents[] = [
                              'total' => $total,
                              'total_off' => $total_pay_in_office,
                              'total_momo' => $total_momo,
                              'total_vcb' => $total_bank_transfer_vietcombank,
                              'total_tcb' => $total_bank_transfer_techcombank,
                            ];
                            $data[] = $total;
                        }

                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total += $data_percent['total'];
                          $total_pay_in_office += $data_percent['total_off'];
                          $total_momo += $data_percent['total_momo'];
                          $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                          $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                        }

                        $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                        $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                        $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                        $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong tháng: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];
                        // dd($result);
                        return $result;
                      break;

                    case 'year':
                        $current_week = Carbon::now()->weeksInYear;
                        $first_monday = date('d', strtotime(Carbon::parse('first Monday of January')));
                        $first_sunday = date('d', strtotime(Carbon::parse('first Sunday of January')));
                        if ($first_sunday < 7) {
                          $first_sunday += 7;
                        }
                        for ($i=0; $i < $current_week; $i++) {
                             $first_day = '1-1-'.Carbon::now()->year;
                             $t =  $i;
                             $total = 0;
                             $total_momo = 0;
                             $total_pay_in_office = 0;
                             $total_bank_transfer_techcombank = 0;
                             $total_bank_transfer_vietcombank = 0;
                             $time[] = 'Tuần ' . ($t + 1);
                             if ($i == 0) {
                                $dayStar = 1;
                                if ($first_sunday < 7) {
                                   $dayEnd = $first_sunday + 6;
                                } else {
                                   $dayEnd = $first_sunday;
                                }
                             }
                             elseif ( $i == $current_week -1 ) {
                                 $dayStar = $i * 7 + 6;
                                 $dayEnd = Carbon::now()->isLeapYear() ? 366 : 365;
                             } else {
                                 $dayStar = $first_monday + $i * 7;
                                 $dayEnd = $first_monday + $i * 7 + 6;
                             }

                             for ($j = $dayStar; $j <= $dayEnd; $j++) {
                                 $k = $j;
                                 $k = ($k-1) * 60 * 60 * 24;
                                 $day = $first_day;
                                 $day = date('d-m-Y', strtotime($day) + $k);
                                 $date_expired = $day;
                                 $date_expired = new Carbon($date_expired);
                                 $date = new Carbon($day);
                                 foreach ($payments as $key => $payment) {
                                   $create_at = date('Y-m-d', strtotime($payment->created_at));
                                   $create_at = new Carbon($create_at);
                                   if ($date->diffInDays($create_at) == 0) {
                                     $total += $payment->money;
                                     if ($payment->method_gd == 'momo') {
                                       $total_momo += $payment->money;
                                     }
                                     elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                       $total_bank_transfer_vietcombank += $payment->money;
                                     }
                                     elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                       $total_bank_transfer_techcombank += $payment->money;
                                     } else {
                                       $total_pay_in_office += $payment->money;
                                     }
                                   }
                                 }


                             }

                             $data_percents[] = [
                               'total' => $total,
                               'total_off' => $total_pay_in_office,
                               'total_momo' => $total_momo,
                               'total_vcb' => $total_bank_transfer_vietcombank,
                               'total_tcb' => $total_bank_transfer_techcombank,
                             ];
                             $data[] = $total;

                        }

                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;
                        $total = 0;
                        foreach ($data_percents as $key => $data_percent) {
                          $total += $data_percent['total'];
                          $total_pay_in_office += $data_percent['total_off'];
                          $total_momo += $data_percent['total_momo'];
                          $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                          $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                        }

                        $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                        $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                        $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                        $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                        $result = [
                          'percent' => '(Sắp xếp theo tuần trong năm: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                          'time' => $time,
                          'data' => $data,
                        ];

                        return $result;
                      break;

                  }
                break;

              case 'month':
                  for ($i=1; $i <= 12; $i++) {
                      $dayStar = 1;
                      $total = 0;
                      $total_momo = 0;
                      $total_pay_in_office = 0;
                      $total_bank_transfer_techcombank = 0;
                      $total_bank_transfer_vietcombank = 0;
                      $time[] = 'Tháng ' . $i;
                      if ( $i == 1 || $i == 3 || $i == 5 || $i == 7 || $i == 8 || $i == 10 || $i == 12 ) {
                        $dayEnd = 31;
                      }
                      else if ( $i == 2 ) {
                          $dayEnd = Carbon::now()->isLeapYear() ? 29 : 28;
                      } else {
                          $dayEnd =  30 ;
                      }

                      for ($j=$dayStar; $j <= $dayEnd; $j++) {
                          $date =  Carbon::now()->year . '-' . $i . '-' . $j;
                          $date_expired = $date;
                          $date_expired = new Carbon($date_expired);
                          $date = new Carbon($date);
                          foreach ($payments as $key => $payment) {
                            $create_at = date('Y-m-d', strtotime($payment->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date->diffInDays($create_at) == 0) {
                              $total += $payment->money;
                              if ($payment->method_gd == 'momo') {
                                $total_momo += $payment->money;
                              }
                              elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                $total_bank_transfer_vietcombank += $payment->money;
                              }
                              elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                $total_bank_transfer_techcombank += $payment->money;
                              } else {
                                $total_pay_in_office += $payment->money;
                              }
                            }
                          }

                      }
                      $data_percents[] = [
                        'total' => $total,
                        'total_off' => $total_pay_in_office,
                        'total_momo' => $total_momo,
                        'total_vcb' => $total_bank_transfer_vietcombank,
                        'total_tcb' => $total_bank_transfer_techcombank,
                      ];
                      $data[] = $total;
                  }
                  $total_momo = 0;
                  $total_pay_in_office = 0;
                  $total_bank_transfer_techcombank = 0;
                  $total_bank_transfer_vietcombank = 0;
                  $total = 0;
                  foreach ($data_percents as $key => $data_percent) {
                    $total += $data_percent['total'];
                    $total_pay_in_office += $data_percent['total_off'];
                    $total_momo += $data_percent['total_momo'];
                    $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                    $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                  }

                  $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                  $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                  $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                  $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                  $result = [
                    'percent' => '(Sắp xếp theo tháng trong năm: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                    'time' => $time,
                    'data' => $data,
                  ];
                  // dd($result);
                  return $result;

                break;

            }
        }

        elseif ($type == 'custom')
        {
            switch ($sort1) {
                case 'day':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $diff_day = $dayEnd;
                    $diff_day = $diff_day->diffInDays($dayStar);
                    if ($diff_day < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($diff_day); $i++) {
                        $day_start =  new Carbon($sort2);
                        $date = $day_start;
                        $date = $date->addDays($i);
                        $time[] = date( 'd-m-Y', strtotime($date) );
                        $date = $date->year . '-' . $date->month . '-' . $date->day;
                        $date_expired = $date;
                        $date_expired = new Carbon($date_expired);
                        $date = new Carbon($date);
                        $total = 0;
                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;


                        foreach ($payments as $key => $payment) {
                          $create_at = date('Y-m-d', strtotime($payment->created_at));
                          $create_at = new Carbon($create_at);
                          if ($date->diffInDays($create_at) == 0) {
                            $total += $payment->money;
                            if ($payment->method_gd == 'momo') {
                              $total_momo += $payment->money;
                            }
                            elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                              $total_bank_transfer_vietcombank += $payment->money;
                            }
                            elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                              $total_bank_transfer_techcombank += $payment->money;
                            } else {
                              $total_pay_in_office += $payment->money;
                            }
                          }
                        }
                        $data_percents[] = [
                          'total' => $total,
                          'total_off' => $total_pay_in_office,
                          'total_momo' => $total_momo,
                          'total_vcb' => $total_bank_transfer_vietcombank,
                          'total_tcb' => $total_bank_transfer_techcombank,
                        ];
                        $data[] = $total;
                    }

                    $total_momo = 0;
                    $total_pay_in_office = 0;
                    $total_bank_transfer_techcombank = 0;
                    $total_bank_transfer_vietcombank = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total += $data_percent['total'];
                      $total_pay_in_office += $data_percent['total_off'];
                      $total_momo += $data_percent['total_momo'];
                      $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                      $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                    }

                    $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                    $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                    $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                    $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo ngày: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];
                    // dd($result);
                    return $result;
                  break;

                case 'week':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->weekOfYear - $dayStar->weekOfYear;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= $current ; $i++) {
                       $n = 0;
                       $e = 0;
                       $total = 0;
                       $total_momo = 0;
                       $total_pay_in_office = 0;
                       $total_bank_transfer_techcombank = 0;
                       $total_bank_transfer_vietcombank = 0;
                       $a = $i;
                       $day_star =  new Carbon($sort2);
                       $m = 7 - $day_star->dayOfWeek;
                       if ($i == 0) {
                          $day1 = $day_star;
                          $t = $day_star->year . '-' . $day_star->month . '-' . $day_star->day;
                          $t = new Carbon($t);
                          $day2 = $t->addDays($m);
                       } elseif ($i == $n ) {
                          $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i-1) * 7 ) + 1 );
                          $day2 =  $dayEnd;
                       } else {
                         $day1 = $day_star->addDays(7 - $day_star->dayOfWeek)->addDays( ( ($i - 1) * 7 ) + 1 );
                         $t = $day1->year . '-' . $day1->month . '-' . $day1->day;
                         $t = new Carbon($t);
                         $day2 = $t->addDays(6);
                       }
                       $time[] = 'Tuần ' . ($a + 1) . ' (' . date('d', strtotime($day1)) . ' - ' . date('d', strtotime($day2)) . ' / '. $day_star->month .' )' ;
                       $k = $day2->diffInDays($day1);
                       for ($j=0; $j <= $k; $j++) {
                          $date =  $day1->year . '-' . $day1->month . '-' . $day1->day;
                          $date = new Carbon($date);
                          $date = $date->addDays($j);
                          $date_expired =  $date->year . '-' . $date->month . '-' . $date->day;
                          $date_expired = new Carbon($date_expired);
                          foreach ($payments as $key => $payment) {
                            $create_at = date('Y-m-d', strtotime($payment->created_at));
                            $create_at = new Carbon($create_at);
                            if ($date->diffInDays($create_at) == 0) {
                              $total += $payment->money;
                              if ($payment->method_gd == 'momo') {
                                $total_momo += $payment->money;
                              }
                              elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                $total_bank_transfer_vietcombank += $payment->money;
                              }
                              elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                $total_bank_transfer_techcombank += $payment->money;
                              } else {
                                $total_pay_in_office += $payment->money;
                              }
                            }
                          }
                       }
                       $data_percents[] = [
                         'total' => $total,
                         'total_off' => $total_pay_in_office,
                         'total_momo' => $total_momo,
                         'total_vcb' => $total_bank_transfer_vietcombank,
                         'total_tcb' => $total_bank_transfer_techcombank,
                       ];
                       $data[] = $total;
                    }

                    $total_momo = 0;
                    $total_pay_in_office = 0;
                    $total_bank_transfer_techcombank = 0;
                    $total_bank_transfer_vietcombank = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total += $data_percent['total'];
                      $total_pay_in_office += $data_percent['total_off'];
                      $total_momo += $data_percent['total_momo'];
                      $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                      $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                    }

                    $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                    $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                    $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                    $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tuần: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];
                    // dd($result);
                    return $result;

                  break;

                case 'month':
                    $time = [];
                    $dayStar =  new Carbon($sort2);
                    $dayEnd = new Carbon($sort3);
                    $current = $dayEnd->month - $dayStar->month;
                    if ($current < 0) {
                      $term = $dayStar;
                      $dayStar = $dayEnd;
                      $dayEnd = $term;
                    }
                    for ($i=0; $i <= abs($current) ; $i++) {
                        $day_time = new Carbon($dayStar);
                        $total = 0;
                        $total_momo = 0;
                        $total_pay_in_office = 0;
                        $total_bank_transfer_techcombank = 0;
                        $total_bank_transfer_vietcombank = 0;

                        $time[] = 'Tháng ' . $day_time->addMonths($i)->month;
                        if ($i == 0) {
                            $day_start =  new Carbon($dayStar);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . $day_start->day;
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        } elseif ($i == $current) {
                            $day_end = new Carbon($dayEnd);
                            $day1 = $day_end->year . '-' . $day_end->month . '-' . '1';
                            $day2 = $day_end->year . '-' . $day_end->month . '-' . $day_end->day;
                        } else {
                            $day_start =  new Carbon($sort2);
                            $day_start = $day_start->addMonths($i);
                            $day1 = $day_start->year . '-' . $day_start->month . '-' . '1';
                            if ( $day_start->month == 1 || $day_start->month == 3 || $day_start->month == 5 || $day_start->month == 7
                              || $day_start->month == 8 || $day_start->month == 10 || $day_start->month == 12 ) {
                              $day2 = $day_start->year . '-' . $day_start->month . '-' . 31;
                            }
                            else if ( $day_start->month == 2 ) {
                                $day2 = $day_start->isLeapYear() ? $day_start->year . '-' . $day_start->month . '-' . 29 : $day_start->year . '-' . $day_start->month . '-' . 28;
                            } else {
                              $day2 =  $day_start->year . '-' . $day_start->month . '-' . 30 ;
                            }
                        }

                        $day1 = new Carbon($day1);
                        $day2 = new Carbon($day2);
                        $diff_day = $day2->diffInDays($day1);

                        for ($j=0; $j <= $diff_day ; $j++) {
                           $date = $day1->year . '-' . $day1->month . '-' . '1';
                           $date = new Carbon($date);
                           $date = $date->addDays($j);
                           $date_expired = $date->year . '-' . $date->month . '-' . $date->day;
                           $date_expired = new Carbon($date_expired);
                           foreach ($payments as $key => $payment) {
                             $create_at = date('Y-m-d', strtotime($payment->created_at));
                             $create_at = new Carbon($create_at);
                             if ($date->diffInDays($create_at) == 0) {
                               $total += $payment->money;
                               if ($payment->method_gd == 'momo') {
                                 $total_momo += $payment->money;
                               }
                               elseif ($payment->method_gd == 'bank_transfer_vietcombank') {
                                 $total_bank_transfer_vietcombank += $payment->money;
                               }
                               elseif ($payment->method_gd == 'bank_transfer_techcombank') {
                                 $total_bank_transfer_techcombank += $payment->money;
                               } else {
                                 $total_pay_in_office += $payment->money;
                               }
                             }
                           }
                        }
                        $data_percents[] = [
                          'total' => $total,
                          'total_off' => $total_pay_in_office,
                          'total_momo' => $total_momo,
                          'total_vcb' => $total_bank_transfer_vietcombank,
                          'total_tcb' => $total_bank_transfer_techcombank,
                        ];
                        $data[] = $total;
                    }
                    $total_momo = 0;
                    $total_pay_in_office = 0;
                    $total_bank_transfer_techcombank = 0;
                    $total_bank_transfer_vietcombank = 0;
                    $total = 0;
                    foreach ($data_percents as $key => $data_percent) {
                      $total += $data_percent['total'];
                      $total_pay_in_office += $data_percent['total_off'];
                      $total_momo += $data_percent['total_momo'];
                      $total_bank_transfer_techcombank += $data_percent['total_vcb'];
                      $total_bank_transfer_vietcombank += $data_percent['total_tcb'];
                    }

                    $total_pay_in_office = $total_pay_in_office == 0 ? 0 : round($total_pay_in_office * 100 / $total , 2);
                    $total_momo = $total_momo == 0? 0 : round($total_momo * 100 / $total , 2);
                    $total_bank_transfer_techcombank = $total_bank_transfer_techcombank == 0? 0 : round($total_bank_transfer_techcombank * 100 / $total, 2);
                    $total_bank_transfer_vietcombank = $total_bank_transfer_vietcombank == 0? 0 : round($total_bank_transfer_vietcombank * 100 / $total, 2);

                    $result = [
                      'percent' => '(Sắp xếp tùy chọn theo tháng: ' . number_format($total,0,",",".") . ' VNĐ, MoMo: ' . $total_momo . '%, TCB: ' . $total_bank_transfer_techcombank . '%, VCB: ' . $total_bank_transfer_vietcombank . '%, Office: ' . $total_pay_in_office . '% )',
                      'time' => $time,
                      'data' => $data,
                    ];

                    return $result;

                  break;

            }
        }

    }


}
