<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\LogActivity;

class AuthRepositories {

    protected $user;
    protected $log;

    public function __construct()
    {
        $this->user = new User;
        $this->log = new LogActivity;
    }

    public function detail($id)
    {
        return $this->user->with('user_meta')->find($id);
    }

    public function list_log()
    {
        $logs = $this->log->orderBy('id', 'desc')->paginate(30);
        $data = [ 'data' => [] ];
        foreach ($logs as $key => $log) {
            if ($log->user_id == 999999) {
                $log->user_name = 'Hệ thống Portal';
            }
            elseif ($log->user_id == 999998) {
                $log->user_name = 'Hệ thống Dashboard';
            }
            elseif ($log->user_id == 999997) {
                $log->user_name = 'Hệ thống DirectAdmin Việt Nam';
            }
            else {
                $log->user_name = $log->user->name;
            }
            $log->text_created_at = date('H:i:s d-m-Y', strtotime($log->created_at));
            $data['data'][] = $log;
        }
        $data['total'] = $logs->total();
        $data['perPage'] = $logs->perPage();
        $data['current_page'] = $logs->currentPage();
        return $data;
    }

    public function list_log_with_action($qtt, $user, $action ,$service)
    {
        if (empty($qtt)) {
           $qtt = 30;
        }
        if ( !empty($user) && !empty($action) && !empty($service) ) {
          $logs = $this->log->where('user_id', $user)->where('action', $action)->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($user) && !empty($action) ) {
          $logs = $this->log->where('action', $action)->where('user_id', $user)->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($user) && !empty($service) ) {
          $logs = $this->log->where('user_id', $user)->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($action) && !empty($service) ) {
          $logs = $this->log->where('action', $action)->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($user) ) {
          $logs = $this->log->where('user_id', $user)->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($service) ) {
          // code...service
          $logs = $this->log->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($action) ) {
          $logs = $this->log->where('action', $action)->orderBy('id', 'desc')->paginate($qtt);
        }
        else {
           $logs = $this->log->orderBy('id', 'desc')->paginate($qtt);
        }
        $data = ['data' => []];
        foreach ($logs as $key => $log) {
            if ($log->user_id == 999999) {
                $log->user_name = 'Hệ thống Portal';
            }
            elseif ($log->user_id == 999998) {
                $log->user_name = 'Hệ thống Dashboard';
            }
            elseif ($log->user_id == 999997) {
                $log->user_name = 'Hệ thống DirectAdmin Việt Nam';
            }
            else {
                $log->user_name = $log->user->name;
            }
            $log->text_created_at = date('H:i:s d-m-Y', strtotime($log->created_at));
            $data['data'][] = $log;
        }
        $data['total'] = $logs->total();
        $data['perPage'] = $logs->perPage();
        $data['current_page'] = $logs->currentPage();
        return $data;
    }

}

?>
