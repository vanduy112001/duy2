<?php
namespace App\Repositories\Admin;

use App\Model\Contract;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\Pricing;
use App\Model\ContractDetail;
use App\Model\Vps;
use App\Model\Domain;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\EmailHosting;
use App\Model\Colocation;
use App\Model\Product;
use App\Model\GroupProduct;
use App\Factories\AdminFactories;
use Carbon\Carbon;
use Auth;

class ContractRepositories {

    protected $user;
    protected $contract;
    protected $contract_detail;
    protected $user_meta;
    protected $product;
    protected $group_product;
    protected $pricing;
    protected $vps;
    protected $domain;
    protected $server;
    protected $hosting;
    protected $colocation;
    protected $email_hosting;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->contract = new Contract;
        // $this->product = AdminFactories::productRepositories();
        $this->product = new Product;
        $this->pricing = new Pricing;
        $this->contract_detail = new ContractDetail;
        $this->vps = new Vps;
        $this->domain = new Domain;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
        $this->group_product = new GroupProduct;
    }

    public function get_all() 
    {
        return $this->contract->orderBy('id', 'DESC')->get();
    }

    public function get_contract_with_id($id)
    {
        return $this->contract->find($id);
    }

    public function get_users() 
    {
        return $this->user->get();
    }
    
    public function add_data_contract_form($id) 
    {
        $data_user = $this->user->where('id', $id)->with('user_meta')->first();
        // $group_products = $this->product->get_group_product_private($id);
        return $data = [
            'data_user' => $data_user,
            // 'data_group_product' => $group_products
        ];
    }

    public function get_amount_contract_form($id, $billing)
    {
        $pricing = $this->pricing->where('product_id', $id)->first();
        return $pricing[$billing];
    }

    // public function get_product_pricing($id)
    // {
    //     return $this->pricing->where('product_id', $id)->first();
    // }
    
    public function get_product($id)
    {
        return $this->product->detail_product($id);
    }

    public function create_data_contract($data)
    {
        $result = [
            'status' => false,
            'message' => 'Tạo hợp đồng thất bại, vui lòng thử lại'
        ];
        $create_data = [
            'title' => $data['title'] ? $data['title'] : '', 
            'user_id' => $data['user_id'] ? $data['user_id'] : '', 
            'contract_number' => $data['contract_number'] ? $data['contract_number'] : '', 
            'company' => $data['company'] ? $data['company'] : '', 
            'abbreviation_name' => $data['abbreviation_name'] ? $data['abbreviation_name'] : null, 
            'deputy' => $data['deputy'] ? $data['deputy'] : '', 
            'deputy_position' => $data['deputy_position'] ? $data['deputy_position'] : '', 
            'address' => $data['address'] ? $data['address'] : '', 
            'phone' => $data['phone'] ? $data['phone'] : '', 
            'email' => $data['email'] ? $data['email'] : '', 
            'mst' => $data['mst'] ? $data['mst'] : '', 
            'date_create' => $data['date_create'] ? Carbon::createFromFormat('d-m-Y', $data['date_create'])->format('Y-m-d') : null, 
            'date_end' => $data['date_end'] ? Carbon::createFromFormat('d-m-Y', $data['date_end'])->format('Y-m-d') : null,
            'status' => $data['status'] ? $data['status'] : '',
            'vat' => isset($data['vat']) ? 1 : 0,
            'contract_type' => $data['contract_type'] ? $data['contract_type'] : null,
            'domain_website' => $data['domain_website'] ? $data['domain_website'] : null,
        ];
        $create = $this->contract->create($create_data);
        if ($create) {
            $result = [
                'status' => true,
                'message' => 'Tạo hợp đồng thành công',
                'id' => $create->id
            ];
        }
        return $result;
    }

    public function delete($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa hợp đồng thất bại, vui lòng thử lại'
        ];
        $delete = $this->contract->find($id)->delete();
        $delete_contract_details = $this->contract_detail->where('contract_id', $id)->delete();
        if ($delete) {
            $result = [
              'status' => true,
              'message' => 'Xóa thành công.',
            ];
        }
        return $result;
    }

    public function multidelete($ids)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa hợp đồng thất bại, vui lòng thử lại'
        ];
        $delete = $this->contract->whereIn('id', $ids)->delete();
        $delete_contract_details = $this->contract_detail->whereIn('contract_id', $ids)->delete();
        if ($delete) {
            $result = [
              'status' => true,
              'message' => 'Xóa thành công.'
            ];
        }
        return $result;
    }

    public function update($data, $id)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhập đồng thất bại, vui lòng thử lại'
        ];
        $update_data = [
            'title' => $data['title'] ? $data['title'] : '', 
            'user_id' => $data['user_id'] ? $data['user_id'] : '', 
            'contract_number' => $data['contract_number'] ? $data['contract_number'] : '', 
            'company' => $data['company'] ? $data['company'] : '',
            'abbreviation_name' => $data['abbreviation_name'] ? $data['abbreviation_name'] : null,
            'deputy' => $data['deputy'] ? $data['deputy'] : '', 
            'deputy_position' => $data['deputy_position'] ? $data['deputy_position'] : '', 
            'address' => $data['address'] ? $data['address'] : '', 
            'phone' => $data['phone'] ? $data['phone'] : '', 
            'email' => $data['email'] ? $data['email'] : '', 
            'mst' => $data['mst'] ? $data['mst'] : '', 
            'date_create' => $data['date_create'] ? Carbon::createFromFormat('d-m-Y', $data['date_create'])->format('Y-m-d') : null, 
            'date_end' => $data['date_end'] ? Carbon::createFromFormat('d-m-Y', $data['date_end'])->format('Y-m-d') : null,
            'status' => $data['status'] ? $data['status'] : '',
            'vat' => isset($data['vat']) ? 1 : 0,
            'contract_type' => $data['contract_type'] ? $data['contract_type'] : null,
            'domain_website' => $data['domain_website'] ? $data['domain_website'] : null,
        ];
        $update = $this->contract->find($id)->update($update_data);
        if ($update) {
            $result = [
              'status' => true,
              'message' => 'Cập nhập thành công.'
            ];
        }
        return $result;
    }

    public function get_contract_details($id)
    {
        $contract_details = $this->contract_detail->where('contract_id', $id)->orderBy('date_end', 'DESC')->get();
        return $contract_details;
    }

    public function get_contract_detail_with_id($id)
    {
        $contract_detail = $this->contract_detail->find($id);
        return $contract_detail;
    }

    public function create_contract_detail($data_contract_detail)
    {
        $result = [
            'status' => false,
            'message' => 'Tạo sản phẩm cho hợp đồng thất bại, vui lòng thử lại'
        ];
        if ($data_contract_detail['form_type'] == 'create') {
            $create_data = [
                'contract_id' => $data_contract_detail['add_contract_id'] ? $data_contract_detail['add_contract_id'] : '', 
                'billing_cycle' => $data_contract_detail['add_billing_cycle'] ? $data_contract_detail['add_billing_cycle'] : '', 
                'amount' => $data_contract_detail['add_amount'] ? $data_contract_detail['add_amount'] : '', 
                'qtt' => $data_contract_detail['add_qtt'] ? $data_contract_detail['add_qtt'] : '', 
                'date_create' => $data_contract_detail['add_date_create'] ? Carbon::createFromFormat('d-m-Y', $data_contract_detail['add_date_create'])->format('Y-m-d') : null, 
                'date_end' => $data_contract_detail['add_date_end'] ? Carbon::createFromFormat('d-m-Y', $data_contract_detail['add_date_end'])->format('Y-m-d') : null,
                'note' => $data_contract_detail['add_note'] ? $data_contract_detail['add_note'] : '', 
                'product_type' => $data_contract_detail['add_product_type'] ? $data_contract_detail['add_product_type'] : '',
                'target_id' => $data_contract_detail['add_target_id'] ? $data_contract_detail['add_target_id'] : '',
            ];
        } elseif ($data_contract_detail['form_type'] == 'add new') {
            $create_data = [
                'contract_id' => $data_contract_detail['add_contract_id_new'] ? $data_contract_detail['add_contract_id_new'] : '', 
                'billing_cycle' => $data_contract_detail['add_billing_cycle_new'] ? $data_contract_detail['add_billing_cycle_new'] : '', 
                'amount' => $data_contract_detail['add_amount_new'] ? $data_contract_detail['add_amount_new'] : '', 
                'qtt' => $data_contract_detail['add_qtt_new'] ? $data_contract_detail['add_qtt_new'] : '', 
                'date_create' => $data_contract_detail['add_date_create_new'] ? Carbon::createFromFormat('d-m-Y', $data_contract_detail['add_date_create_new'])->format('Y-m-d') : null, 
                'date_end' => $data_contract_detail['add_date_end_new'] ? Carbon::createFromFormat('d-m-Y', $data_contract_detail['add_date_end_new'])->format('Y-m-d') : null,
                'note' => $data_contract_detail['add_note_new'] ? $data_contract_detail['add_note_new'] : '', 
                'product_type' => $data_contract_detail['add_product_type_new'] ? $data_contract_detail['add_product_type_new'] : '',
                'product_option' => $data_contract_detail['add_product_option'] ? $data_contract_detail['add_product_option'] : '',
                'target_id' => isset($data_contract_detail['add_target_id_new']) ? $data_contract_detail['add_target_id_new'] : null,
            ];
            // dd($create_data);
        }
        $create = $this->contract_detail->create($create_data);
        if ($create) {
            $result = [
                'status' => true,
                'message' => 'Tạo sản phẩm cho hợp đồng thành công',
                // 'contract_detail' => $create->id,
            ];
        }
        return $result;
    }

    public function delete_contract_detail($id)
    {
        $result = [
            'status' => false,
            'message' => 'Xóa sản phẩm thất bại, vui lòng thử lại'
        ];
        $delete = $this->contract_detail->find($id)->delete();
        if ($delete) {
            $result = [
                'status' => true,
                'message' => 'Xóa sản phẩm thành công'
            ];
        }
        return $result;
    }

    public function get_contract_detail($data) 
    {
        $contract_detail = $this->contract_detail->find($data['id']);
        // $group_products = $this->product->get_group_product_private($data['user_id']);
        $billings = config('billing');
        return $data = [
            'contract_detail' => $contract_detail,
            // 'data_group_product' => $group_products,
            'billings' => $billings,
        ];
    }
    
    public function update_contract_detail($id, $data)
    {
        // dd( $data);
        $result = [
            'status' => false,
            'message' => 'Cập nhập sản phẩm thất bại, vui lòng thử lại'
        ];
        $update_contract = $this->contract_detail->find($id);
        $data_update = [
            // 'product_id' => $data['product_id'] ? $data['product_id'] : '',
            'qtt' => $data['update_qtt'] ? $data['update_qtt'] : '',
            'amount' => $data['update_amount'] ? $data['update_amount'] : '',
            'billing_cycle' => $data['update_billing_cycle'] ? $data['update_billing_cycle'] : '',
            'date_create' => $data['update_date_create'] ? Carbon::createFromFormat('d-m-Y', $data['update_date_create'])->format('Y-m-d') : null,
            'date_end' => $data['update_date_end'] ? Carbon::createFromFormat('d-m-Y', $data['update_date_end'])->format('Y-m-d') : null,
            'note' => $data['update_note'] ? $data['update_note'] : '',
            'product_option' => $data['update_product_option'] ? $data['update_product_option'] : '',
            // 'product_type' => $data['update_product_type'] ? $data['update_product_type'] : '',
            // 'target_id' => $data['update_target_id'] ? $data['update_target_id'] : '',
        ];
        $update = $update_contract->update($data_update);
        if ($update) {
            return true;
        }
        return $result;
    }

    public function get_target_from_user($data)
    {
        $targets = [];
        $product_type = $data['product_type'];
        $user_id = $data['user_id'];
        if ($product_type === 'vps') {
            $targets = $this->vps->where('user_id', $user_id)->where('location', 'cloudzone')->orderBy('id', 'DESC')->get();
        } 
        if ($product_type === 'vps_us') {
            $targets = $this->vps->where('user_id', $user_id)->where('location', 'us')->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'hosting') {
            $targets = $this->hosting->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'email_hosting') {
            $targets = $this->email_hosting->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'domain') {
            $targets = $this->domain->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'server') {
            $targets = $this->server->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'colocation') {
            $targets = $this->colocation->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }

        return $targets;
    }

    public function get_target_detail($data)
    {
        $target_detail = [];
        $product_type = $data['product_type'];
        $target_id = $data['target_id'];
        if ($product_type == 'vps' || $product_type == 'vps_us') {
            $target_detail = $this->vps->with('detail_order')->find($target_id);
        } 
        elseif ($product_type === 'hosting') {
            $target_detail = $this->hosting->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'email_hosting') {
            $target_detail = $this->email_hosting->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'server') {
            $target_detail = $this->server->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'domain') {
            $target_detail = $this->domain->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'colocation') {
            $target_detail = $this->colocation->with('detail_order')->find($target_id);
        }

        return $target_detail;
    }

    public function get_contract_detail_vps($id, $product_type)
    {
        if ($product_type == 'vps') {
            return $this->vps->find($id);
        } elseif ($product_type == 'product_vps') {
            return $this->product->find($id);
        } else {
            return null;
        } 
    }

    public function get_contract_detail_vps_us($id, $product_type)
    {
        if ($product_type == 'vps_us') {
            return $this->vps->find($id);
        } elseif ($product_type == 'product_vps_us') {
            return $this->product->find($id);
        } else {
            return null;
        } 
    }

    public function get_contract_detail_hosting($id, $product_type)
    {
        if ($product_type == 'hosting') {
            return $this->hosting->find($id);
        } elseif ($product_type == 'product_hosting') {
            return $this->product->find($id);
        } else {
            return null;
        } 
    }

    public function get_contract_detail_email_hosting($id, $product_type)
    {
        if ($product_type == 'email_hosting') {
            return $this->email_hosting->find($id);
        } elseif ($product_type == 'product_email_hosting') {
            return $this->product->find($id);
        } else {
            return null;
        } 
    }

    public function get_contract_detail_server($id)
    {
        return $this->server->find($id);
    }

    public function get_contract_detail_colocation($id)
    {
        return $this->colocation->find($id);
    }

    public function get_contract_detail_domain($id)
    {
        return $this->domain->find($id);
    }

    public function contract_detail($id)
    {
        $contract_detail = $this->contract_detail->find($id);
        return $contract_detail;
    }

    public function get_contract_pending()
    {
        $contract_pendings = $this->contract->where('status', 'pending')->count();
        return $contract_pendings ;
    }

    public function update_customer_info($data)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhập thất bại'
        ];
        $user_id = $data['user_id'];
        $data_update = [
            'company' => $data['company'] ? $data['company'] : '',
            'abbreviation_name' => $data['abbreviation_name'] ? $data['abbreviation_name'] : null,
            'phone' => $data['phone'] ? $data['phone'] : '',
            'address' => $data['address'] ? $data['address'] : '',
            'mst' => $data['mst'] ? $data['mst'] : ''
        ];
        $update = $this->user_meta->where('user_id', $user_id)->update($data_update);
        if ($update) {
            $result = [
                'status' => true,
                'message' => 'Cập nhập thành công'
            ];
            return $result;
        } else {
            return $result;
        }
    }

    public function get_target_product_from_user($data)
    {
        $targets = [];
        $product_type = $data['product_type'];
        $user_id = $data['user_id'];
        $user = $this->user->find($user_id);

        // Lọc nhóm người dùng trong nhóm sản phẩm
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $this->group_product->where('group_user_id', $group_user->id);
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0);
        }

        //Xuất sản phẩm trong nhóm sản phẩm
        if ($product_type == 'product_vps') {
            $targets = $group_products->where('type', 'vps')->with('products')->orderBy('id', 'asc')->get();
        } 
        elseif ($product_type == 'product_vps_us') {
            $targets = $group_products->where('type', 'vps_us')->with('products')->orderBy('id', 'asc')->get();
        } 
        elseif ($product_type == 'product_hosting') {
            $targets = $group_products->where('type', 'hosting')->with('products')->orderBy('id', 'asc')->get();
        }
        elseif ($product_type == 'product_email_hosting') {
            $targets = $group_products->where('type', 'email_hosting')->with('products')->orderBy('id', 'asc')->get();
        }
        // elseif ($product_type === 'product_server') {
        //     $targets = $group_products->where('type', $product_type)->get();
        // }
        return $targets;
    }

    public function get_target_detail_product($data)
    {
        $product_type = $data['product_type'];
        $product_id = $data['target_id'];
        if($product_type == 'product_vps' || $product_type == 'product_vps_us' || $product_type == 'product_hosting' || $product_type == 'product_email_hosting' ) {
            $product = $this->product->with('pricing')->find($product_id);
            return $product;
        } else {
            return null;
        }
    }

    public function create_new_contract_number($contract_number)
    {
        $all_contract_numbers = $this->contract->get();
        if ($all_contract_numbers->contains('contract_number', $contract_number)) {
            for ($i = 2; $i <= 20; $i++) {
                $new_contract_number = $contract_number . '/0' . $i;
                if (!$all_contract_numbers->contains('contract_number', $new_contract_number)) {
                    return $new_contract_number;
                }
            }
        } else {
            return $contract_number;
        }
        throw new \Exception('Không thể tạo số hợp đồng');
    }

    public function get_owner($id, $product_type) 
    {
        if($product_type == 'vps' || $product_type == 'vps_us') {
            $user_id = $this->vps->find($id)->user_id;
        } elseif( $product_type == 'hosting' ) {
            $user_id = $this->hosting->find($id)->user_id;
        }
        $owner = $this->user->where('id', $user_id)->with('user_meta')->first();
        return $owner;
    }

    public function get_products($ids, $product_type)
    {
        if($product_type == 'vps' || $product_type == 'vps_us') {
            $products = $this->vps->whereIn('id', $ids)->get();
        } elseif( $product_type == 'hosting' ) {
            $products = $this->hosting->whereIn('id', $ids)->get();
        }
        return $products;
    }

    public function save_product_to_contract($data)
    {
        $result = [
            'status' => false,
            'message' => 'Tạo hợp đồng thất bại, vui lòng thử lại'
        ];

        $contract_data = [
            'title' => $data['title'] ? $data['title'] : '', 
            'user_id' => $data['user_id'] ? $data['user_id'] : '', 
            'contract_number' => $data['contract_number'] ? $data['contract_number'] : '', 
            'company' => $data['company'] ? $data['company'] : '', 
            'abbreviation_name' => $data['abbreviation_name'] ? $data['abbreviation_name'] : null, 
            'deputy' => $data['deputy'] ? $data['deputy'] : '', 
            'deputy_position' => $data['deputy_position'] ? $data['deputy_position'] : '', 
            'address' => $data['address'] ? $data['address'] : '', 
            'phone' => $data['phone'] ? $data['phone'] : '', 
            'email' => $data['email'] ? $data['email'] : '', 
            'mst' => $data['mst'] ? $data['mst'] : '', 
            'date_create' => $data['date_create'] ? Carbon::createFromFormat('d-m-Y', $data['date_create'])->format('Y-m-d') : null, 
            'date_end' => $data['date_end'] ? Carbon::createFromFormat('d-m-Y', $data['date_end'])->format('Y-m-d') : null,
            'status' => $data['status'] ? $data['status'] : '',
            'vat' => isset($data['vat']) ? 1 : 0,
            'contract_type' => $data['contract_type'] ? $data['contract_type'] : null,
            'domain_website' => $data['domain_website'] ? $data['domain_website'] : null,
        ];

        $create = $this->contract->create($contract_data);

        foreach ($data['target_ids'] as $key => $target_id) {
            $contract_detail_data = [
                'contract_id' => $create->id, 
                'billing_cycle' => $data['billing_cycles'][$key] ? $data['billing_cycles'][$key] : '', 
                'amount' => $data['amounts'][$key] ? $data['amounts'][$key] : '', 
                'qtt' => 1, 
                'date_create' => $data['date_creates'][$key] ? Carbon::createFromFormat('d-m-Y', $data['date_creates'][$key])->format('Y-m-d') : null, 
                'date_end' => $data['date_ends'][$key] ? Carbon::createFromFormat('d-m-Y', $data['date_ends'][$key])->format('Y-m-d') : null,
                'note' => $data['add_notes'][$key] ? $data['add_notes'][$key] : '', 
                'product_type' => $data['product_types'][$key] ? $data['product_types'][$key] : '',
                'target_id' => $target_id,
            ];
            $this->contract_detail->create($contract_detail_data);
        }

        if ($create) {
            $result = [
                'status' => true,
                'message' => 'Tạo hợp đồng thành công',
                'id' => $create->id
            ];
        }
        return $result;
    }

    public function userContract($id) 
    {
        $user_contracts = $this->contract->where('user_id', $id)->orderBy('id', 'DESC')->get();
        return $user_contracts;
    }

    public function getUser($id)
    {
        return $this->user->find($id);
    }

}