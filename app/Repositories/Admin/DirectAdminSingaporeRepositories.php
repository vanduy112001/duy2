<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Services\DirectAdmin;
use App\Model\Product;
use App\Model\Hosting;
use App\Model\ServerHosting;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\EventPromotion;
use App\Model\Zalo;
use App\Jobs\TestSendMail;
use GuzzleHttp\Client;
use http\Env\Request;

class DirectAdminSingaporeRepositories {

    protected $user;
    protected $da;
    protected $da_host;
    protected $da_port;
    protected $da_user;
    protected $da_password;
    protected $product;
    protected $hosting;
    protected $ip;
    protected $server_hosting;
    protected $type_server_hosting;
    protected $readnotification;
    protected $notification;
    protected $zalo;
    protected $event_promotion;

    public function __construct()
    {
        $this->user = new User;
        $this->server_hosting = new ServerHosting;
        $this->product = new Product;
        $this->hosting = new Hosting;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->event_promotion = new EventPromotion();
        $this->zalo = new Zalo();
        // kiểm tra server hosting
        $server_hosting_active = $this->server_hosting->where('location', 'si')->where('active', true)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
    }

    public function login_zalo()
    {
        $token_zalo = $this->zalo->first();
        if ( ! isset($token_zalo) ) {
          $app_uri = '425677306795358073';
          $redirect_uri = 'https://portal.cloudzone.vn/zalo/callback';
          $token = '5BYs9N4bn0qIlhSbUo313nExioDjVu1u49o84IW3-Ln8__5oTGRwAHB_-sCL6Oep8F-jT3OokISUrPDE9pI1V1ZvdZ0m4v1P9egW24Dqls8YdRTDE4os5WpGn5eGEB0CBUMlVJWCepfVyQHhIHdLCqY3sbfzJyeyPgZiILH1dmnAi81zRt2WBbYHhqib0zubEF-lKoiEcoOPvkz9UN_UCb-UzLH3PFHwNe74AJvjjbjMlweNRssCULBra14m5EHOM8xxBcHWzKj5XDqSG6Z1Ub2hHHztFSS8';
          $state = '7ID8Yq6RWS8SeGVnNVVL';
          // $url = "https://oauth.zaloapp.com/v3/auth?app_id=". $app_uri ."&redirect_uri=". $redirect_uri ."&state=". $state ."\" ";
          $url = 'https://graph.zalo.me/v2.0/me/friends?access_token='+$token+'&from=0&limit=5&fields=id,name,gender,picture';
          $client = new Client([
             'headers' => [
                'Content-Type' => 'application/json',
              ],
          ]);
          $response = $client->get(
                $url
          );
          dd(json_decode($response->getBody()->getContents()));
          $token_zalo2 = $this->zalo->first();
          return $token_zalo2;
        }
        return false;
    }

    public function zalo_callback($data)
    {
        $token_zalo = $this->zalo->first();
        if ( isset($token_zalo) ) {
          $token_zalo->token = $data['token'];
          $token_zalo->save();
        } else {
          $data_token = [
            'token' => $data['token']
          ];
          $this->zalo->create($data_token);
        }
    }

    public function test_mail()
    {
        $data = [
            'name' => 'Lê Tấn Hiển',
            'email' => 'uthienth92@gmail.com',
        ];
        dispatch( new TestSendMail($data) )->delay(now()->addSeconds(10));
    }

    public function url_control_panel()
    {
       $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
       $url = '<a href="' . $host_connect .'" target="_blank">' . $host_connect . '</a>';
       return $url;
    }

    public function orderCreateAccount($hostings, $due_date)
    {
        foreach ($hostings as $key => $hosting) {
            $user = $this->user->find($hosting->user_id);
            $product = $this->product->find($hosting->product_id);
            $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản.</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
            // $da['error'] = 0;
            if ($da['error'] == 0) {
                if ($due_date == 'one_time_pay') {
                    $due_date = '2099-12-31';
                }
                $data_hosting = [
                    'status' => 'Active',
                    'paid' => 'paid',
                    'status_hosting' => 'on',
                    'date_create' => date('Y-m-d'),
                    'next_due_date' => $due_date,
                    'server_hosting_id' => $this->type_server_hosting,
                ];
                $hosting->update($data_hosting);
            } else {
                return false;
            }
        }
        return true;
    }

    public function adminCreateHosting($hosting, $due_date)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
        if ( isset($event) ) {
           $meta_user = $user->user_meta;
           $meta_user->point += $event->point;
           $meta_user->save();
           // Tạo thông báo cho user
           $content = '';
           $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản.</p>';

           $data_notification = [
               'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
               'content' => $content,
               'status' => 'Active',
           ];
           $infoNotification = $this->notification->create($data_notification);
           $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
        }

        $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
        // $da['error'] = 0;
        if ($da['error'] == 0) {
            if ($due_date == 'one_time_pay') {
                $due_date = '2099-12-31';
            }
            $data_hosting = [
                'status' => 'Active',
                'paid' => 'paid',
                'status_hosting' => 'on',
                'date_create' => date('Y-m-d'),
                'next_due_date' => $due_date,
                'server_hosting_id' => $this->type_server_hosting,
            ];
            $hosting->update($data_hosting);
        } else {
            return false;
        }
        return true;
    }


    public function orderCreateAccountWithPoint($hostings, $due_date)
    {
        foreach ($hostings as $key => $hosting) {
            $user = $this->user->find($hosting->user_id);
            $product = $this->product->find($hosting->product_id);

            $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
            if ($da['error'] == 0) {
                if ($due_date == 'one_time_pay') {
                    $due_date = '2099-12-31';
                }
                $data_hosting = [
                    'status' => 'Active',
                    'paid' => 'paid',
                    'status_hosting' => 'on',
                    'date_create' => date('Y-m-d'),
                    'next_due_date' => $due_date,
                    'server_hosting_id' => $this->type_server_hosting,
                ];
                $hosting->update($data_hosting);
            } else {
                return false;
            }
        }
        return true;
    }


    // tạo 1 hosting
    public function createHosting($hosting, $due_date)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
        if ($da['error'] == 0) {
            $data_hosting = [
                'status' => 'Active',
                'paid' => 'paid',
                'status_hosting' => 'on',
                'date_create' => date('Y-m-d'),
                'next_due_date' => $due_date,
                'server_hosting_id' => $this->type_server_hosting,
            ];
            $hosting->update($data_hosting);
        } else {
            return false;
        }
        return true;
    }
    // User yêu cầu xóa hosting
    public function suspendHosting($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'cancel';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    // User yêu cầu tat hosting
    public function off($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'off';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    // User yêu cầu bật hosting bị suppen
    public function unSuspend($hosting)
    {
        if ($hosting->status_hosting == 'admin_off') {
            $data = [
              'error' => 2223
            ];
            return $data;
        }
        if ($hosting->status_hosting == 'cancel') {
            $data = [
              'error' => 2224
            ];
            return $data;
        }
        $da = $this->unSuspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'on';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }

    public function admin_unSuspend($hosting)
    {
        $da = $this->unSuspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'on';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    /*
    * Admin
    */
    public function adminSuspend($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'admin_off';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    // Module directAdmin
    public function showPackage()
    {
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_PACKAGES_USER");
        return $result;
    }

    public function createAccount($domain, $username, $passwd, $passwd2, $email, $package = 'basic')
    {
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_ACCOUNT_USER", [
            'action' => 'create',
            'add' => 'Submit',
            'domain' => $domain, //uthientest.cloudzone.vn
            'username' => $username,
            'passwd' => $passwd,
            'passwd2' => $passwd2,
            'email' => $email,
            'package' => !empty($package)? $package : 'basic',
            'ip' => $this->ip,
        ], "POST");
        return $result;
    }

    public function deleteAccount($hosting)
    {
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect, $this->da_user, $this->da_password);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SELECT_USERS", [
            'confirmed' => 'Confirm',
            'delete' => 'yes',
            'select0' => $hosting->user,
        ], "POST");
        return $result;
    }

    public function showIP()
    {
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect, $this->da_user, $this->da_password);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SHOW_RESELLER_IPS");
        return $result;
    }

    public function suspendAccount($hosting)
    {
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SELECT_USERS", [
            'location' => 'CMD_SELECT_USERS',
            'suspend' => 'Suspend',
            'select0' => $hosting->user,
        ], "POST");
        return $result;
    }

    public function unSuspendAccount($hosting)
    {
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SELECT_USERS", [
            'location' => 'CMD_SELECT_USERS',
            'suspend' => 'Unsuspend',
            'select0' => $hosting->user,
        ], "POST");
        return $result;
    }

    public function login_as($username, $passwd)
    {
    // dd($username);
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // $da =  new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        // dd($username);
        // $result = $da->query("CMD_API_LOGIN_KEYS", [
        //     'keyname' => $username,
        //     'key' => '3xKsz45iz6ts65G0X666HjaD4PU5U227Q99427bwvk8O4861PN1RBYY7uO95e609',
        //     'key2' => '3xKsz45iz6ts65G0X666HjaD4PU5U227Q99427bwvk8O4861PN1RBYY7uO95e609',
        //     'never_expires' => 'no',
        //     'expiry_timestamp' => 100000,
        //     'max_uses' => 1,
        //     'clear_key' => 'yes',
        //     'allow_html' => 'no',
        //     'passwd' => $passwd,
        // ], 'POST');
        // $result = $da->login_as('letanhi');
        // $key = $this->rand_string(64);
        // $result2 = $da->query("CMD_API_LOGIN_KEYS", [
        //   'action' =>'create',
        //     'type' => 'one_time_url',
        //     'keyname' => 'letanhi',
        //     'key' => $key,
        //     'key2' => $key,
        //     'type' => 'url',
        //     'never_expires' => 'no',
        //     'clear_key' => 'yes',
        //     'allow_html' => 'yes',
        //     'max_uses' => 1,
        //     'passwd' => $this->da_password,
        // ], 'POST');
        // $result2 = $da->query("CMD_API_LOGIN_KEYS", [
        //     'action' => 'show_modify',
        //     'keyname' => 'letanhi',
        // ]);
        // dd($result,$result2,$key);
        // if ($result2['error'] == 0) {
        //   $da->logout();
        //     $da->logout();
        //     return $key;
        // }
        return false;
    }

    public function getHostingDaPortal($id)
    {
        $data  = [];
        $data_search = [];
        // kiểm tra server hosting
        $server_hosting_active = $this->server_hosting->where('id', $id)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $results = $da->query("CMD_API_SHOW_USERS");
        foreach ($results as $key => $result) {
            $data[] = $da->query("CMD_API_SHOW_USER_CONFIG" , [
                'user' => $result,
            ]);
        }
        foreach ($data as $key => $hosting) {
            $check = $this->hosting->where('domain', $hosting['domain'])->first();
            if (isset($check)) {
                $hosting['user_pt'] = $check->user_hosting;
            } else {
                $hosting['user_pt'] = 0;
            }
            $data_search[] = $hosting;
        }
        return $data_search;
    }

    public function getUserHostingDaPortal($username, $id)
    {
        $server_hosting_active = $this->server_hosting->where('id', $id)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SHOW_USER_CONFIG" , [
            'user' => $username,
        ]);
        return $result;
    }

    function rand_string($n) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $n; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function upgrade($hosting, $package)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        // $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }

        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_MODIFY_USER", [
            'action' => 'package',
            'user' => $hosting->user,
            'package' => $package, //uthientest.cloudzone.vn
        ], "POST");
        return $result;
    }

}
