<?php

namespace App\Repositories\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Factories\UserFactories;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Product;
use App\Model\User;
use App\Model\Email;
use App\Model\Order;
use App\Model\VpsConfig;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\EventPromotion;
use App\Model\LogActivity;
use App\Model\ProxyState;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
// API
use App\Services\DirectAdmin;
use App\Services\DashBoard;
use Mail;

class DashBoardRepositories {

    protected $user;
    protected $dashboard;
    protected $product;
    protected $email;
    protected $order;
    protected $vps;
    protected $detail_order;
    protected $home;
    protected $vps_config;
    protected $readnotification;
    protected $notification;
    protected $event_promotion;
    protected $log_activity;
    protected $state_proxy;
    // goi email
    protected $user_send_email;
    protected $subject;


    public function __construct()
    {
        $this->dashboard = new DashBoard();
        $this->user = new User;
        $this->product = new Product;
        $this->email = new Email;
        $this->order = new Order;
        $this->vps = new Vps;
        $this->detail_order = new DetailOrder;
        $this->vps_config = new VpsConfig;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->event_promotion = new EventPromotion();
        $this->log_activity = new LogActivity();
        $this->state_proxy = new ProxyState();
    }
    // Tạo VPS bình thường bang tien
    public function createVPS($list_vps, $due_date)
    {
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);
            $event = $this->event_promotion->where('product_id', $vps->product_id)->where('billing_cycle', $vps->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi đăng ký gói dịch vụ ' . $vps->product->name . '</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (float) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (float) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (float) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            $os = !empty($vps->os) ? $vps->os : 2;
            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                elseif ($os == 2) {
                    $os = 12;
                }
                elseif ($os == 3) {
                    $os = 13;
                }
                elseif ($os == 30) {
                    $os = 30;
                }
                elseif ($os == 31) {
                    $os = 31;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else {
                    $os = 14;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (float)$cpu : 1,
                'ram' => !empty($ram) ? (float)$ram : 1,
                'disk' => $disk > 20 ? (float)$disk : 20,
                'type' => 0,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data, $vps);
            if (!empty($result)) {

                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  $result->id,
                    'ip' => $result->ip,
                    'user' => $result->username,
                    'password' => $result->password,
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    // ghi log admin
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }

            }
            else {
              return false;
            }
        }
        return true;

    }
    // Tạo VPS bình thường bang tien
    public function createVpsUs($list_vps)
    {
        $response = [ 'error' => 0];
        $states = config('states');
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);

            $add_on = [];
            $cpu = !empty($product->meta_product->cpu) ? (float)$product->meta_product->cpu : 1;
            $ram = !empty($product->meta_product->memory) ? (float)$product->meta_product->memory : 1;
            $disk = !empty($product->meta_product->disk) ? (float)$product->meta_product->disk : 12;

            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $idState = 13;
            foreach ($states as $key => $state) {
                if ( $state ==  $vps->state) {
                     $idState = $key;
                }
            }
            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'state' => $idState, // Ngay het han
                'saleprice' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'resources' => $cpu . '-' . $ram . '-' . $disk, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                // 'resources' => '2-2-12', //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVpsUs($data, $vps);
            if ( $result['error'] == 0 ) {
                $response['content'][$vps->id] = 0;
                try {
                    $state = !empty( $result['content']->state[0] ) ? $result['content']->state[0] : 'Unknow';
                } catch (Exception $e) {
                    report($e);
                    $state = 'Unknow';
                }
                $state = str_replace("\"", "", $state);
                $state = str_replace("[", "", $state);
                $state = str_replace("]", "", $state);
                $data_vps_return = [
                    'vm_id' => !empty($result['content']->vm_id) ? $result['content']->vm_id :  $result['content']->id,
                    'ip' => $result['content']->ip,
                    'user' => 'Administrator',
                    'password' => !empty($result['content']->password) ? $result['content']->password : '',
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $result['content']->end_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'on',
                    'state' => $state,
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    // ghi log admin
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS US thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => !empty($vps->user_id) ? $vps->user_id : 999998,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' VPS US thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);

                } catch (\Exception $e) {
                    report($e);
                    continue;
                }

            }
            elseif ( $result['error'] == 1 ) {
                $response['error'] = 1;
                $response['content'][$vps->id] = 1;
                $data_vps_return = [
                    'vm_id' => !empty($result['content']->vm_id) ? $result['content']->vm_id :  $result['content']->id,
                    'user' => 'Administrator',
                    'password' => !empty($result['content']->password) ? $result['content']->password : '',
                    'paid' => 'paid',
                    'status' => 'Active',
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    // ghi log admin
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS US thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => !empty($vps->user_id) ? $vps->user_id : 999998,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' VPS US thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    report($e);
                    continue;
                }
            }
            else {
              $response['error'] = 2;
              $response['contentFail'][$vps->id] = 2;
            }
        }
        return $response;

    }
    // Tạo proxy
    public function createProxy($list_proxy)
    {
        $response = [ 'error' => 0];
        $data = [];
        $state = [];
        $config_billing_DashBoard = config('billingDashBoard');
        foreach ($list_proxy as $key => $proxy) {
            // Thông tin vps
            $user = $this->user->find($proxy->user_id);
            $product = Product::find($proxy->product_id);

            $customer = !empty($user->customer_id) ? $user->customer_id : 91;
            $leased_time = $config_billing_DashBoard[$proxy->billing_cycle];
            $price = $proxy->detail_order->sub_total;
            $username = !empty($proxy->username) ? $proxy->username : '';
            $password = !empty($proxy->password) ? $proxy->password : '';
            $created_at = date('Y-m-d', strtotime($proxy->created_at));
            $state = $proxy->state;
            // if ( !isset( $state ) ) {
            //     $state[$proxy->state] = 1;
            // } else {
            //     $check = false;
            //     foreach ($state as $key => $value) {
            //         if ( $key ==  $proxy->state ) {
            //             $check = $key;
            //         }
            //     }
            //     if ( $check ) {
            //         $state[$proxy->state] = $state[$key] + 1;
            //     } else {
            //         $state[$proxy->state] = 1;
            //     }
            // }
        }
        // foreach ($state as $key => $value) {
        //     $data[] = [
        //         'customer' => $customer, // customer id dashboard
        //         'port' => $value,
        //         'leased_time' => $leased_time, // Thoi gian thue theo thang
        //         'price' => $price, //Tong tien thanh toan
        //         'username' => $username,
        //         'password' => $password,
        //         'created_date' => $created_at, // ngay tao
        //         'state' => $key, // Ngay het han
        //     ];
        // }
        $data = [
            'customer' => $customer, // customer id dashboard
            'port' => $list_proxy->count(),
            'leased_time' => $leased_time, // Thoi gian thue theo thang
            'price' => $price, //Tong tien thanh toan
            'username' => $username,
            'password' => $password,
            'created_date' => $created_at, // ngay tao
            'state' => $state, // Ngay het han
        ];
        // dd($data);
        // Gui
        $result = $this->dashboard->createProxy($data);
        if ( $result ) {
            foreach ($list_proxy as $key => $proxy) {
                $data_proxy_return = [
                    'proxy_id' => !empty($result[$key]->proxy_id) ? $result[$key]->proxy_id : '',
                    'ip' => !empty($result[$key]->ip) ? $result[$key]->ip : '',
                    'port' => !empty($result[$key]->port) ? $result[$key]->port : '',
                    'username' => !empty($result[$key]->username) ? $result[$key]->username : '',
                    'password' => !empty($result[$key]->password) ? $result[$key]->password : '',
                    'state' => !empty($result[$key]->state) ? $result[$key]->state : '',
                    'next_due_date' => !empty($result[$key]->end_date) ? $result[$key]->end_date : '',
                    'date_create' => date('Y-m-d'),
                    'status' => 'progressing',
                ];
                $proxy->update($data_proxy_return);
                try {
                    // ghi log
                    $text_vps = '#' . $proxy->id;
                    if (!empty($proxy->ip)) {
                       $text_vps .= ' IP: <a target="_blank" href="/admin/proxy/detail/' . $proxy->id  . '">' . $proxy->ip . '</a>';
                    }
                    // ghi log admin
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' Proxy thành công ' . $text_vps,
                      'service' =>  $proxy->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => !empty($proxy->user_id) ? $proxy->user_id : 999998,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' Proxy thành công <a target="_blank" href="/service/detail/' . $proxy->id  . '?type=proxy">' . $proxy->ip . '</a>',
                      'service' =>  $proxy->ip,
                    ];
                    $this->log_activity->create($data_log_user);
    
                } catch (\Exception $e) {
                    report($e);
                }
            }
        }
        else {
          $response['error'] = 2;
        }
        return $response;
    }
    // Tạo VPS bình thường bang diem
    public function createVPSWithPoint($list_vps, $due_date)
    {
        // dd('da den createVPS');
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (float) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (float) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (float) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            $os = !empty($vps->os) ? $vps->os : 2;
            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                else if ($os == 2) {
                    $os = 12;
                }
                else if ($os == 3) {
                    $os = 13;
                }
                else if ($os == 31) {
                    $os = 31;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else {
                    $os = 14;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (float)$cpu : 1,
                'ram' => !empty($ram) ? (float)$ram : 1,
                'disk' => $disk > 20 ? (float)$disk : 20,
                'type' => 0,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data, $vps);
            if (!empty($result)) {
                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  $result->id,
                    'ip' => $result->ip,
                    'user' => $result->username,
                    'password' => $result->password,
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                    'promotion' => true,
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS thành công ' . $text_vps . ' bằng Cloudzone Point',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }
            }

        }
        return true;

    }
    // Tạo Nat VPS bang tien
    public function createNatVPS($list_vps, $due_date)
    {
        // dd('da den createVPS');
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);
            $event = $this->event_promotion->where('product_id', $vps->product_id)->where('billing_cycle', $vps->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi đăng ký gói dịch vụ ' . $vps->product->name . '</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            if (!empty($vps->os)) {
                if ($vps->os == 4) {
                  $os =  2;
                } else {
                   $os = $vps->os;
                }
            } else {
               $os =  2;
            }

            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                else if ($os == 2) {
                    $os = 12;
                }
                else if ($os == 3) {
                    $os = 13;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else if ($os == 31) {
                    $os = 31;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (int)$cpu : 1,
                'ram' => !empty($ram) ? (int)$ram : 1,
                'disk' => $disk > 20 ? (int)$disk : 20,
                'type' => 5,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data, $vps);
            if (!empty($result)) {
                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  null,//!empty($result->vm_id) ? $result->vm_id :  $result->id
                    'ip' => !empty($result->ip) ? $result->ip : '', //$result->ip
                    'user' => !empty($result->username) ? $result->username : 'root', //$result->username
                    'password' => !empty($result->password) ? $result->password : null, //$result->password
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS NAT thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' NAT VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }
            }
            else {
              return false;
            }

        }
        return true;

    }
    // Tạo Nat VPS bang diem
    public function createNatVPSWithPoint($list_vps, $due_date)
    {
        // dd('da den createVPS');
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            if (!empty($vps->os)) {
                if ($vps->os == 4) {
                  $os =  2;
                } else {
                   $os = $vps->os;
                }
            } else {
               $os =  2;
            }

            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                else if ($os == 2) {
                    $os = 12;
                }
                else if ($os == 3) {
                    $os = 13;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else if ($os == 31) {
                    $os = 31;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (int)$cpu : 1,
                'ram' => !empty($ram) ? (int)$ram : 1,
                'disk' => $disk > 20 ? (int)$disk : 20,
                'type' => 5,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data, $vps);
            $result = true;
            if (!empty($result)) {
                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  null,//!empty($result->vm_id) ? $result->vm_id :  $result->id
                    'ip' => !empty($result->ip) ? $result->ip : '', //$result->ip
                    'user' => !empty($result->username) ? $result->username : 'root', //$result->username
                    'password' => !empty($result->password) ? $result->password : null, //$result->password
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                    'promotion' => true,
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS NAT thành công ' . $text_vps . ' bằng Cloudzone Point.',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' NAT VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }
            }

        }
        return true;

    }
    // Gữi user qua bên dashboard và lấy vm_id lưu vào user
    public function sendUserDashBoard($user)
    {
        $data_create = [
            'name' => $user->name . ' ' . $user->id,
            'email' => $user->email,
        ];
        $dashboard_id = $this->dashboard->sendUserDashBoard($data_create);
        if ($dashboard_id) {
            $user->customer_id = $dashboard_id->id;
            $user->save();
            return true;
        } else {
            return false;
        }
    }


    // Bật VPS
    public function onVPSUS($vps)
    {
        // return true;
        // Gui
        if ( $vps->vm_id > 1000000000 ) {
          $result = $this->dashboard->onVPSUS($vps);
        } else {
          $result = $this->dashboard->onVPS($vps);
        }

        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'on';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'bật',
                      'model' => 'Admin/DashBoard',
                      'description' => 'dịch vụ VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }

        } else {
            return false;
        }
    }
    // Bật VPS
    public function restartVPSUS($vps)
    {
        // return true;
        // Gui
          // Log::info('da den ' . $vps->vm_id . ' \n' );
        if ( $vps->vm_id > 1000000000 ) {
          $result = $this->dashboard->onVPSUS($vps);
        } else {
          $result = $this->dashboard->restartVPS($vps);
        }

        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'on';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'bật',
                      'model' => 'Admin/DashBoard',
                      'description' => 'dịch vụ VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }

        } else {
            return false;
        }
    }
    // Bật VPS US
    public function onVPS($vps)
    {
        // Gui
        $result = $this->dashboard->onVPS($vps);
        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'on';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'bật',
                      'model' => 'Admin/DashBoard',
                      'description' => 'dịch vụ VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }

        } else {
            return false;
        }
    }
    // Bật VPS
    public function offVPS($vps)
    {
        // Gui
        $result = $this->dashboard->offVPS($vps);
        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'off';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tắt',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }
        } else {
            return false;
        }
        return true;
    }
    // Bật VPS
    public function offVPSUS($vps)
    {
        // return true;
        // Gui
        if ( $vps->vm_id > 1000000000 ) {
          $result = $this->dashboard->offVPSUS($vps);
        } else {
          $result = $this->dashboard->offVPS($vps);
        }

        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'off';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tắt',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }
        } else {
            return false;
        }
    }
    // Bật VPS
    public function restartVPS($vps)
    {
        // Gui
        $result = $this->dashboard->restartVPS($vps);
        if ($result) {
            if (!empty($result['error'])) {
                return $result;
            } else {
                $vps->status_vps = 'on';
                $vps->save();
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'khởi động lại',
                      'model' => 'Admin/DashBoard',
                      'description' => ' dịch vụ VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    $data['success'] = true;
                    return  $data;
                }
                $data['success'] = true;
                return  $data;
            }
        } else {
            return false;
        }
    }
    // Bật VPS
    public function terminatedVps($vps)
    {
        // Gui
        try {
             $result = $this->dashboard->offVPS($vps);
        } catch (Exception $e) {
            report($e);
        }
        // $result['success'] = true;
        $vps->status_vps = 'cancel';
        $vps->save();
        try {
            // ghi log
            $text_vps = '#' . $vps->id;
            if (!empty($vps->ip)) {
                 $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
            }
            $data_log = [
              'user_id' => 999998,
              'action' => 'hủy',
              'model' => 'Admin/DashBoard',
              'description' => ' VPS thành công ' . $text_vps,
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
        } catch (\Exception $e) {
            $data['success'] = true;
            return  $data;
        }
        $data['success'] = true;
        return  $data;
    }
    // Hủy VPS US
    public function terminatedVpsUS($vps)
    {
        // return true;
        // Gui
        try {
          if ( $vps->vm_id > 1000000000 ) {
            $result = $this->dashboard->offVPSUS($vps);
          } else {
            $result = $this->dashboard->offVPS($vps);
          }
        } catch (Exception $e) {
            report($e);
        }
        // $result['success'] = true;
        $vps->status_vps = 'cancel';
        $vps->save();
        try {
                    // ghi log
            $text_vps = '#' . $vps->id;
            if (!empty($vps->ip)) {
                $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
             }
             $data_log = [
              'user_id' => 999998,
              'action' => 'hủy',
              'model' => 'Admin/DashBoard',
              'description' => ' VPS thành công ' . $text_vps,
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
        } catch (\Exception $e) {
            $data['success'] = true;
            return  $data;
        }
        $data['success'] = true;
        return  $data;
    }
    // Gia han vps
    public function expired_vps($data_expired, $vm_id)
    {
        $vps = $this->vps->where('vm_id', $vm_id)->first();
        $result = $this->dashboard->expired_vps($data_expired, $vm_id, $vps);
        if ($result) {
          return $result;
        } else {
          return false;
        }

    }

    // Gia han vps us
    public function expired_vps_us($data_expired)
    {
        $result = $this->dashboard->expired_vps_us($data_expired);
        // $result = '{"error": 0}';
        // $result = json_decode($result);
        if ($result) {
          return $result;
        } else {
          return false;
        }

    }

    // Gia han vps
    public function expired_proxy($data_expired)
    {
        $result = $this->dashboard->expired_proxy($data_expired);
        if ($result) {
          return $result;
        } else {
          return false;
        }
    }

    public function upgrade_vps($data_addon, $vm_id, $vps)
    {
        $result = $this->dashboard->upgrade_vps($data_addon, $vm_id, $vps);
        // $result = true;
        if ($result) {
          return true;
        } else {
          return false;
        }
    }

    public function rebuild_vps($data_rebuild, $vm_id, $vps)
    {
        $result = $this->dashboard->rebuild_vps($data_rebuild, $vm_id);
        // $result = true;
        if ($result) {
          try {
              // ghi log
              $data_log = [
                'user_id' => 999998,
                'action' => 'cài đặt lại',
                'model' => 'User/Services',
                'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
          } catch (\Exception $e) {
              return true;
          }
          return true;
        } else {
          return false;
        }
    }

    public function rebuild_vps_us($vps)
    {
        if ( $vps->vm_id < 1000000000 ) {
          $data_rebuild = [
            "mob" => "rebuild",
            "name" => $vps->vm_id,
            "template" => 12,
            'type' => 0,
          ];
          $result = $this->dashboard->rebuild_vps($data_rebuild, $vps->vm_id);
        }
        else {
          $result = $this->dashboard->rebuild_vps_us($vps);
        }
        // $result = true;
        if ($result) {
          try {
              // ghi log
              $data_log = [
                'user_id' => 999998,
                'action' => 'cài đặt lại',
                'model' => 'User/Services',
                'description' => ' VPS US <a target="_blank" href="/admin/vps_us/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
          } catch (\Exception $e) {
              return true;
          }
          return true;
        } else {
          return false;
        }
    }

    public function reset_password($vps)
    {
        $result = $this->dashboard->reset_password($vps);
        // $result = true;
        if ($result) {
          try {
            //   if ( $result->reset_pass_done == '0' ) {
            //       $vps->status_vps = 'reset_password';
            //       $vps->save();
            //   }
              $vps->status_vps = 'reset_password';
              $vps->save();
              // ghi log
              $data_log = [
                'user_id' => 999998,
                'action' => 'cài đặt mật khẩu',
                'model' => 'User/Services',
                'description' => ' VPS US <a target="_blank" href="/admin/vps_us/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);

          } catch (\Exception $e) {
              report($e);
              return true;
          }
          return true;
        } else {
          return false;
        }
    }

    public function change_ip_vps_us($vps)
    {
        $result = $this->dashboard->change_ip_vps_us($vps);
        // $result = true;
        if ($result) {
          return true;
        } else {
          return false;
        }
    }

    public function change_ip($vps, $sub_total, $order_change_vps)
    {
        // return true;
        $data_change_vps = [
            "mob" => "change_ip",
            "name" => $vps->vm_id,
            "package" => $sub_total,
            "template" => $vps->os,
            'type' => $vps->type_vps == 'vps' ? 0 : 5,
        ];
        $result = $this->dashboard->change_ip($data_change_vps, $vps->vm_id);
        // return true;
        // $vps->ip = '192.168.1.10';
        // $vps->status_vps = 'change_ip';
        // $vps->save();
        // $order_change_vps->changed_ip = '192.168.1.10';
        // $order_change_vps->save();
        // return true;
        if ($result) {
          $vps->ip = $result->new_ip;
          $vps->user = $result->username;
          $vps->password = $result->password;
          $vps->status_vps = 'change_ip';
          $vps->save();
          $order_change_vps->changed_ip = $result->new_ip;
          $order_change_vps->save();
          try {
              // ghi log
              $text_vps = '#' . $vps->id;
              if (!empty($vps->ip)) {
                 $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
              }
              $data_log = [
                'user_id' => 999998,
                'action' => 'đổi IP',
                'model' => 'Admin/DashBoard',
                'description' => ' VPS thành công ' . $text_vps,
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
              // ghi log user
              $data_log_user = [
                'user_id' => $vps->user_id,
                'action' => 'cài đặt lại',
                'model' => 'User/Services',
                'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log_user);
          } catch (\Exception $e) {
              $data['success'] = true;
              return  $data;
          }
          return true;
        } else {
          return false;
        }
    }

    public function admin_change_ip($vps)
    {
        // return true;
        $data_change_vps = [
            "mob" => "changeip",
            "name" => $vps->vm_id,
            "package" => 0,
            "template" => $vps->os,
            'type' => $vps->type_vps == 'vps' ? 0 : 5,
        ];
        $ip_before = $vps->ip;
        $result = $this->dashboard->change_ip($data_change_vps, $vps->vm_id);

        if ($result) {
          if ( !empty($result->new_ip) ) {
            try {
                $vps->ip = $result->new_ip;
                $vps->user = $result->username;
                $vps->password = $result->password;
                $vps->status_vps = 'change_ip';
                $vps->save();
                // ghi log
                $text_vps = '#' . $vps->id;
                if (!empty($vps->ip)) {
                   $text_vps .= ' từ Ip (' . $ip_before . ') sang <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                }
                $data_log = [
                  'user_id' => 999998,
                  'action' => 'đổi IP',
                  'model' => 'Admin/DashBoard',
                  'description' => ' VPS thành công ' . $text_vps,
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                // ghi log user
                $data_log_user = [
                  'user_id' => $vps->user_id,
                  'action' => 'đổi IP',
                  'model' => 'User/Services',
                  'description_user' => ' VPS ' . $text_vps,
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log_user);
            } catch (\Exception $e) {
                  report($e);
                  return  true;
            }
          }
          return true;
        } else {
          return false;
        }
    }

    public function update_sevice_vps($vps, $user)
    {
        $data_update_sevice_vps = [
            "mob" => 'update_vps',
            "name" => $vps->vm_id,
            'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
            'type' => $vps->type_vps == 'vps' ? 0 : 5,
        ];
        $result = $this->dashboard->update_sevice_vps($data_update_sevice_vps, $vps->vm_id);
        try {
            // ghi log
            $text_vps = '#' . $vps->id;
            if (!empty($vps->ip)) {
               $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
            }
            $data_log = [
              'user_id' => 999998,
              'action' => 'đổi khách hàng',
              'model' => 'Admin/DashBoard',
              'description' => 'cho VPS thành công ' . $text_vps,
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
        } catch (\Exception $e) {
        }
    }

    public function vps_console($vm_id)
    {
       try {
         $result = $this->dashboard->vps_console($vm_id);
         // $result = true;
         if ( !empty($result->vm_host) && !empty($result->vm_ticket) ) {
              $data = [
                 'error' => 0,
                 'vm_host' => $result->vm_host, //$result->vm_host
                 'vm_ticket' => $result->vm_ticket, //$result->vm_ticket
              ];
         } else {
             $data = [
                'error' => 2,
                'vm_host' => '',
                'vm_ticket' => '',
             ];
         }
         return $data;
       } catch (\Exception $e) {
         report($e);
         return false;
       }

    }

    public function update_leased_time($vm_id, $data_update_leased_time, $ip)
    {
        $result = $this->dashboard->update_leased_time($vm_id, $data_update_leased_time, $ip);
        return $result;
    }

    // Tạo VPS bằng API
    public function createVpsWithApi($list_vps, $due_date)
    {
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);
            $event = $this->event_promotion->where('product_id', $vps->product_id)->where('billing_cycle', $vps->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi đăng ký gói dịch vụ ' . $vps->product->name . '</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (float) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (float) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (float) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            $os = !empty($vps->os) ? $vps->os : 2;
            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                else if ($os == 2) {
                    $os = 12;
                }
                else if ($os == 3) {
                    $os = 13;
                }
                else if ($os == 31) {
                    $os = 31;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else {
                    $os = 14;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (float)$cpu : 1,
                'ram' => !empty($ram) ? (float)$ram : 1,
                'disk' => $disk > 20 ? (float)$disk : 20,
                'type' => 0,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data, $vps);
            if (!empty($result)) {
                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  $result->id,
                    'ip' => $result->ip,
                    'user' => $result->username,
                    'password' => $result->password,
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    // ghi log admin
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }

            }

        }
        return $list_vps;

    }
    // Tạo Nat VPS bang API
    public function createNatVPSWithApi($list_vps, $due_date)
    {
        // dd('da den createVPS');
        foreach ($list_vps as $key => $vps) {
            // Thông tin vps
            $user = $this->user->find($vps->user_id);
            $config_billing_DashBoard = config('billingDashBoard');
            $product = Product::find($vps->product_id);
            $event = $this->event_promotion->where('product_id', $vps->product_id)->where('billing_cycle', $vps->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi đăng ký gói dịch vụ ' . $vps->product->name . '</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $add_on = [];
            $cpu = $product->meta_product->cpu;
            $ram = $product->meta_product->memory;
            $disk = $product->meta_product->disk;
            // Addon

            if (!empty($vps->addon_id)) {
                $vps_config = $this->vps_config->where('vps_id', $vps->id)->first();
                if (!empty($vps_config)) {

                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $created_at = date('Y-m-d', strtotime($vps->created_at));
            $leased_time = $config_billing_DashBoard[$vps->billing_cycle];

            if (!empty($vps->os)) {
                if ($vps->os == 4) {
                  $os =  2;
                } else {
                   $os = $vps->os;
                }
            } else {
               $os =  2;
            }

            if (empty($vps->security)) {
                if ($os == 1) {
                    $os = 11;
                }
                else if ($os == 2) {
                    $os = 12;
                }
                else if ($os == 3) {
                    $os = 13;
                }
                elseif ($os == 15) {
                    $os = 15;
                }
                else if ($os == 31) {
                    $os = 31;
                }
            }

            $data = [
                'customer' => !empty($user->customer_id) ? $user->customer_id : 91, // customer id dashboard
                'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
                'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
                'end_date' => $due_date, // Ngay het han
                'mob' => $vps->detail_order->sub_total, //Tong tien thanh toan
                'template' => $os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
                'cpu' => !empty($cpu) ? (int)$cpu : 1,
                'ram' => !empty($ram) ? (int)$ram : 1,
                'disk' => $disk > 20 ? (int)$disk : 20,
                'type' => 5,
            ];
            // dd($data);
            // Gui
            $result = $this->dashboard->createVPS($data,$vps);
            if (!empty($result)) {
                $data_vps_return = [
                    'vm_id' => !empty($result->vm_id) ? $result->vm_id :  null,//!empty($result->vm_id) ? $result->vm_id :  $result->id
                    'ip' => !empty($result->ip) ? $result->ip : '', //$result->ip
                    'user' => !empty($result->username) ? $result->username : 'root', //$result->username
                    'password' => !empty($result->password) ? $result->password : null, //$result->password
                    'paid' => 'paid',
                    'status' => 'Active',
                    'next_due_date' => $due_date,
                    'date_create' => date('Y-m-d'),
                    'status_vps' => 'progressing',
                ];
                $vps->update($data_vps_return);
                // dd($vps);
                try {
                    // ghi log
                    $text_vps = '#' . $vps->id;
                    if (!empty($vps->ip)) {
                       $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999998,
                      'action' => 'tạo',
                      'model' => 'Admin/DashBoard',
                      'description' => ' VPS NAT thành công ' . $text_vps,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'tạo',
                      'model' => 'User/Services',
                      'description_user' => ' NAT VPS thành công IP: <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);
                } catch (\Exception $e) {
                    continue;
                }
            }

        }
        return $list_vps;

    }

    public function change_user($vps, $customer_id)
    {
        $result = $this->dashboard->change_user($vps, $customer_id);
        // $result = true;
        return $result;
    }

    public function check_qtt_proxy($data_check)
    {
        // dd($data_check);
        $result = $this->dashboard->check_qtt_proxy($data_check);
        // $result = true;
        return $result;
    }

}
