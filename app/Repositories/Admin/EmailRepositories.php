<?php
namespace App\Repositories\Admin;

use App\Model\Email;
use App\Model\User;
use App\Model\GroupEmail;
use App\Model\MetaEmail;
use App\Model\LogActivity;
use App\Model\SendMail;
use App\Model\Vps;
use App\Model\EmailGroupUser;
use Illuminate\Support\Facades\Auth;

/**
 * Repositories
 */
class EmailRepositories
{

  protected $email;
  protected $group_email;
  protected $meta_email;
  protected $log_activity;
  protected $send_mail;
  protected $vps;
  protected $user;
  protected $emailGroupUser;

  function __construct()
  {
    $this->email = new Email;
    $this->group_emails = new GroupEmail;
    $this->meta_email = new MetaEmail;
    $this->log_activity = new LogActivity();
    $this->send_mail = new SendMail();
    $this->vps = new Vps;
    $this->user = new User;
    $this->emailGroupUser = new EmailGroupUser;
  }

  public function list_user()
  {
    return $this->user->where('id', '!=', 1)->get();
  }

  public function getEmail()
  {
      return $this->email->orderBy('id', 'desc')->get();
  }
  // list danh sách group email
  public function getGroupEmail()
  {
      return $this->group_emails->orderBy('id', 'desc')->with('emails')->get();
  }
  // list email template
  public function getTemplateEmail()
  {
    return $this->email->where('type', 1)->orderBy('id', 'desc')->get();
  }
  // Tạo group email
  public function storeGroupEmail($name)
  {
      $data = [ 'name' => $name, ];
      $store = $this->group_emails->create($data);
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'tạo',
        'model' => 'Admin/GroupEmail',
        'description' => ' nhóm email ' . $name,
      ];
      $this->log_activity->create($data_log);
      if($store) {
        return $store;
      } else {
        return 'Tạo group email thất bại!';
      }
  }
  // Sửa group email
  public function editGroupEmail($id, $name)
  {
      $group_email = $this->group_emails->find($id);
      $group_email->name = $name;
      $group_email->save();
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'chỉnh sửa',
        'model' => 'Admin/GroupEmail',
        'description' => ' nhóm email ' . $name,
      ];
      $this->log_activity->create($data_log);
      return $group_email;
  }
  // Tạo email
  public function createEmail($name, $idGroupEmail)
  {
      $data = [
          'name' => $name,
          'group_email_id' => $idGroupEmail,
      ];
      $create = $this->email->create($data);
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'tạo',
        'model' => 'Admin/Email',
        'description' => ' email <a target="_blank" href="/admin/emails/editEmail?id='. $create->id .'">'. $create->name .'</a>',
      ];
      $this->log_activity->create($data_log);
      return $create;
  }
  // detail email
  public function detailEmail($id)
  {
      return $this->email->find($id);
  }
  // Chinh sua email template
  public function updateEmail($id, $name, $data_email_meta)
  {
      $email = $this->email->find($id);
      $email->name = $name;
      $email->save();

      $email_meta = $this->meta_email->where('email_id', $id)->first();
      if(isset($email_meta)) {
        $meta = $email_meta->update($data_email_meta);
      } else {
        $meta = $this->meta_email->create($data_email_meta);
      }
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'chỉnh sửa',
        'model' => 'Admin/Email',
        'description' => ' email <a target="_blank" href="/admin/emails/editEmail?id='. $email->id .'">'. $email->name .'</a>',
      ];
      $this->log_activity->create($data_log);
      if ($email && $meta) {
        return true;
      } else {
        return false;
      }
  }
  //Xóa email template
  public function deleteEmail($id)
  {
    // dd('da den');
      $delete_meta = $this->meta_email->where('email_id', $id)->delete();
      // dd('da den 111');
      $delete = $this->email->find($id);
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'xóa',
        'model' => 'Admin/Email',
        'description' => ' email ' . $delete->name,
      ];
      $this->log_activity->create($data_log);
      if($delete) {
        $delete->delete();
        return 'da xoa';
      } else {
        return 'khong co email';
      }

  }
  // list email
  public function list_emails()
  {
    $emails = $this->email->get();
    return $emails;
  }
/* Email marketing */
  // Tạo email
  public function create_email_marketing($data)
  {
    $data_email = [
      'name' => $data['name'],
      'status' => !empty($data['status']) ? true : false,
      'group_email_id' => 0,
      'type' => 2,
    ];
    // dd($data_email);
    $create_email = $this->email->create($data_email);
    if ( $create_email ) {
      // Kiểm tra xem có group hay không
      $check_group = true;
      if ( !empty( $data['group_user_id'] ) ) {
        foreach ($data['group_user_id'] as $key => $group_user_id) {
          if ( $group_user_id == 99 ) {
            $check_group = false;
          }
        }
      } else {
        $check_group = false;
      }
      $data_meta = [
        'subject' => $data['subject'],
        'content' => $data['content'],
        'discription' => '',
        'email_id' => $create_email->id,
        'type_group' => $check_group,
        'time' => $data['time'],
        'date' => $data['date'],
      ];
      $meta = $this->meta_email->create($data_meta);
      if ( $check_group ) {
        foreach ($data['group_user_id'] as $key => $group_user_id) {
          $data_email_group_user = [
            'email_id' => $create_email->id,
            'group_user_id' => $group_user_id,
          ];
          $this->emailGroupUser->create($data_email_group_user);
        }
      }
      return true;
    } else {
      return false;
    }
  }
  // Tạo email
  public function update_email_marketing($data)
  {
    $email = $this->email->find($data['id']);
    if ( isset($email) ) {
      $data_email = [
        'name' => $data['name'],
        'status' => !empty($data['status']) ? true : false,
      ];
      $email->update($data_email);
      // Kiểm tra xem có group hay không
      $check_group = true;
      if ( !empty( $data['group_user_id'] ) ) {
        foreach ($data['group_user_id'] as $key => $group_user_id) {
          if ( $group_user_id == 99 ) {
            $check_group = false;
          }
        }
      } else {
        $check_group = false;
      }
      $data_meta = [
        'subject' => $data['subject'],
        'content' => $data['content'],
        'discription' => '',
        'type_group' => $check_group,
        'time' => $data['time'],
        'date' => $data['date'],
      ];
      $email->meta_email->update($data_meta);
      if ( $check_group ) {
        $this->emailGroupUser->where('email_id', $data['id'])->delete();
        foreach ($data['group_user_id'] as $key => $group_user_id) {
          $data_email_group_user = [
            'email_id' => $email->id,
            'group_user_id' => $group_user_id,
          ];
          $this->emailGroupUser->create($data_email_group_user);
        }
      }
      return true;
    } else {
      return false;
    }
  }
  // list email
  public function get_list_email_marketing()
  {
    $list_email = $this->email->where('type', 2)->orderBy('id', 'desc')->paginate(30);
    $data = ['data' => []];
    foreach ($list_email as $key => $email) {
      // Các nhóm đại lý
      $email->text_group = '';
      if ( !empty($email->meta_email->type_group) ) {
        foreach ($email->email_group_users as $key => $email_group_user) {
          if ( $email_group_user->group_user_id == 0 ) {
            $email->text_group .= 'Cơ bản, ';
          }
          elseif ( !empty($email_group_user->group_user->name) ) {
            $email->text_group .= '<a href="/admin/dai-ly/chi-tiet-dai-ly/'. $email_group_user->group_user->id .'">'. $email_group_user->group_user->name .'</a>, ';
          }
        }
      } else {
        $email->text_group = '<span class="text-success">Tất cả</span>';
      }
      // Thời gian
      $email->text_time = '';
      if ( !empty( $email->meta_email->time ) ) {
        $email->text_time = $email->meta_email->time . ' ';
      }
      if ( !empty( $email->meta_email->date ) ) {
        $email->text_time .= $email->meta_email->date . ' ';
      }
      // thời gian bắt đầu gửi
      if ( !empty($email->mail_marketing->created_at) ) {
        $email->time_before_send = date('H:i:s d-m-Y', strtotime($email->mail_marketing->created_at));
      } else {
        $email->time_before_send = ''; 
      }
      $data['data'][] = $email;
    }
    $data['total'] = $list_email->total();
    $data['perPage'] = $list_email->perPage();
    $data['current_page'] = $list_email->currentPage();
    return $data;
  }
  // delete
  public function delete_email_marketing($id)
  {
    try {
      $email = $this->email->find($id);
      if ( isset($email) ) {
        $this->emailGroupUser->where('email_id', $id)->delete();
        $email->meta_email->delete();
        $email->delete();
        return [
          'error' => 0
        ];
      }
      return [
        'error' => 1
      ];
    } catch (Exception $e) {
      report($e);
      return [
        'error' => 1
      ];
    }
  }
  // on
  public function on_email_marketing($id)
  {
    try {
      $email = $this->email->find($id);
      if ( isset($email) ) {
        $email->status = true;
        $email->save();
        return [
          'error' => 0
        ];
      }
      return [
        'error' => 1
      ];
    } catch (Exception $e) {
      report($e);
      return [
        'error' => 1
      ];
    }
  }
  // off
  public function off_email_marketing($id)
  {
    try {
      $email = $this->email->find($id);
      if ( isset($email) ) {
        $email->status = false;
        $email->save();
        return [
          'error' => 0
        ];
      }
      return [
        'error' => 1
      ];
    } catch (Exception $e) {
      report($e);
      return [
        'error' => 1
      ];
    }
  }
  // send mail
  public function send_email_marketing($data)
  {
    // dd($data);
    try {
      $user = $this->user->find($data['user_id']);
      $url = url('');
      $url = str_replace(['http://','https://'], ['', ''], $url);

      $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$url}'];
      $replace = [ $user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $url ];
      $data['content'] = str_replace($search, $replace, $data['content']);
      // dd($data);
      $data_tb_send_mail = [
        'type' => 'test_mail_template_marketing',
        'type_service' => '',
        'user_id' => $data['user_id'],
        'status' => false,
        'content' => serialize($data),
      ];
      $this->send_mail->create($data_tb_send_mail);
      return true;
    } catch (Exception $e) {
      report($e);
      return false;
    }
  }
/* Email service */
  public function create_email_service($data)
  {
    $data_email = [
      'name' => $data['name'],
      'status' => !empty($data['status']) ? true : false,
      'group_email_id' => 0,
      'type' => 3,
      'type_service' => $data['type_service']
    ];
    // dd($data_email);
    $create_email = $this->email->create($data_email);
    if ( $create_email ) {
      $data_meta = [
        'subject' => $data['subject'],
        'content' => $data['content'],
        'discription' => '',
        'email_id' => $create_email->id,
      ];
      $meta = $this->meta_email->create($data_meta);
      return true;
    } else {
      return false;
    }
  }
  // update email
  public function update_email_service($data)
  {
    $email = $this->email->find($data['id']);
    if ( isset($email) ) {
      $data_email = [
        'name' => $data['name'],
        'type_service' => $data['type_service']
      ];
      $email->update( $data_email );
      $meta_email = $email->meta_email;
      $data_meta = [
        'subject' => $data['subject'],
        'content' => $data['content'],
        'discription' => '',
      ];
      $meta_email->update( $data_meta );
      return true;
    } else {
      return false;
    }
  }
  // list email
  public function get_list_email_service()
  {
    $list_email = $this->email->where('type', 3)->orderBy('id', 'desc')->with('meta_email')->paginate(30);
    return $list_email;
  }
/* Send Email */
  // list send mail
  public function load_send_mail()
  {
      $list_mail = $this->send_mail->orderBy('id', 'desc')->paginate(30);
      $type_send_mail = config('type_send_mail');
      $data = [ 'data' => [] ];
      if ($list_mail->count()) {
        foreach ($list_mail as $key => $mail) {
          $mail->user_name = !empty($mail->user->name) ? $mail->user->name : '<span class="text-danger">Đã xóa</span>';
          $mail->user_mail = !empty($mail->user->email) ? $mail->user->email : '<span class="text-danger">Đã xóa</span>';
          $mail->date_create_mail = date('H:i:s d-m-Y', strtotime($mail->created_at));
          $mail->type_send_mail = !empty($type_send_mail[$mail->type]) ? $type_send_mail[$mail->type] : 'Không có trong thư viện';
          if ( $mail->status ) {
            $mail->send_time = !empty(date('H:i:s d-m-Y', strtotime($mail->send_time))) ? date('H:i:s d-m-Y', strtotime($mail->send_time)) : '';
          }
          $data['data'][] = $mail;
        }
      }
      $data['total_mail'] = $this->send_mail->count();
      $data['total_send_mail'] = $this->send_mail->whereStatus(true)->count();
      $data['total'] = $list_mail->total();
      $data['perPage'] = $list_mail->perPage();
      $data['current_page'] = $list_mail->currentPage();
      return $data;
  }

  public function load_detail_send_mail($id)
  {
    $send_mail = $this->send_mail->find($id);
    $type_send_mail = config('type_send_mail');
    $subject = '';
    // dd($send_mail);
    try {
      if ( isset($send_mail) ) {
        if ( $send_mail->type == 'mail_cron_nearly' ) {
          if ( $send_mail->type_service == 'vps' ) {
            $subject = 'Thông báo VPS hết hạn';
          }
          elseif( $send_mail->type_service == 'vps_us' ) {
            $subject = 'Thông báo VPS US hết hạn';
          }
          elseif ( $send_mail->type_service == 'hosting' ) {
            $subject = 'Thông báo Hosting hết hạn';
          }
          elseif ( $send_mail->type_service == 'server' ) {
            $subject = 'Thông báo Server hết hạn';
          }
          elseif ( $send_mail->type_service == 'proxy' ) {
            $subject = 'Thông báo Proxy hết hạn';
          }
          elseif ( $send_mail->type_service == 'colocation' ) {
            $subject = 'Thông báo Colocation hết hạn';
          }
          try {
            $content = unserialize($send_mail->content);
          } catch (\Throwable $th) {
            $content = '';
          }
        }
        elseif ( $send_mail->type == 'mail_order_vps' || $send_mail->type == 'mail_order_server' || $send_mail->type == 'mail_order_vps_us' || $send_mail->type == 'mail_order_hosting' ||
        $send_mail->type == 'mail_order_expire_vps' || $send_mail->type == 'mail_order_expire_vps_us' || $send_mail->type == 'mail_order_expire_hosting' ) {
          $data = unserialize($send_mail->content);
          $content = $data['content'];
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'mail_finish_order_vps' || $send_mail->type == 'mail_finish_order_server' ||
          $send_mail->type == 'mail_finish_order_hosting' || $send_mail->type == 'mail_finish_order_email_hosting' ||
          $send_mail->type == 'mail_finish_order_expire_vps' || $send_mail->type == 'mail_finish_order_expire_hosting' ||
          $send_mail->type == 'mail_finish_order_colo' || $send_mail->type == 'mail_order_colo' ||
          $send_mail->type == 'mail_finish_order_expire_server' || $send_mail->type == 'mail_finish_payment_colo' 
          || $send_mail->type == 'mail_finish_order_expire_proxy' || $send_mail->type == 'mail_finish_order_proxy'
          ||   $send_mail->type == 'mail_finish_config_server'
        ) {
          $data = unserialize($send_mail->content);
          $content = $data['content'];
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'mail_cron_auto_refurn_finish' ) {
          if ( $send_mail->type_service == 'vps' ) {
            try {
              $data = unserialize($send_mail->content);
              $content = $data['content'];
            } catch (\Throwable $th) {
              $content = '';
            }
            $subject = 'Tự động gia hạn VPS thành công';
          } else {
            try {
              $data = unserialize($send_mail->content);
              $content = $data['content'];
            } catch (\Throwable $th) {
              $content = '';
            }
            $subject = 'Tự động gia hạn VPS US thành công';
          }

        }
        elseif ( $send_mail->type == 'order_upgrade_hosting' || $send_mail->type == 'order_addon_vps'
          || $send_mail->type == 'finish_order_upgarde_hosting' || $send_mail->type == 'finish_order_addon_vps'
        ) {
          $data = unserialize($send_mail->content);
          $content = $data;
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'request_payment' ) {
          $data = unserialize($send_mail->content);
          $content = $data;
          $subject = 'Yêu cầu nạp tiền vào tài khoản tại CLOUDZONE';
        }
        elseif ( $send_mail->type == 'mail_create_user' || $send_mail->type == 'admin_send_mail' || $send_mail->type == 'admin_create_payment' ||
            $send_mail->type == 'finish_payment' || $send_mail->type == 'mail_finish_rebuild_vps'
            || $send_mail->type == 'mail_upgrade_error_vps' || $send_mail->type == 'mail_rebuild_error_vps' || $send_mail->type == 'mail_verify_user'
            || $send_mail->type == 'mail_tutorial_user' || $send_mail->type == 'mail_marketing_user'
            || $send_mail->type == 'mail_marketing_tiny' || $send_mail->type == 'mail_marketing_vps_uk'
            || $send_mail->type == 'mail_update_feature' || $send_mail->type == 'test_mail_template_marketing'
            || $send_mail->type == 'mail_template_marketing'
        ) {
          $data = unserialize($send_mail->content);
          // dd($data);
          $content = $data;
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'mail_order_change_ip' || $send_mail->type == 'mail_order_finish_change_ip' ) {
          $data = unserialize($send_mail->content);
          // dd($data);
          $content = $data;
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'mail_create_error_vps' || $send_mail->type == 'mail_create_error_vps_us' ||
          $send_mail->type == 'mail_expire_error_vps' || $send_mail->type == 'mail_expire_error_vps_us'
        ) {
          $data = unserialize($send_mail->content);
          $content = $data;
          $content['user_name'] = !empty($content->vps->user_vps->name) ?  $content->vps->user_vps->name : $content->vps->user_id;
          $subject = $data['subject'];
        }
        elseif ( $send_mail->type == 'mail_cron_auto_refurn_error' ) {
          $data = unserialize($send_mail->content);
          $content = $data;
          $content['user_name'] = !empty($send_mail->user->name) ? $send_mail->user->name : 'Không tìm thấy';
          $subject = 'Lỗi tự động gia hạn VPS (hết SDTK)';
          $content['next_due_date'] = date('d-m-Y', strtotime( $content['next_due_date'] ));
        }
        elseif ( $send_mail->type == 'admin_mail_cron_auto_refurn_error' ) {
          $subject = 'Tự động gia hạn VPS thất bại';
          $content['name'] = !empty($send_mail->user->name) ? $send_mail->user->name : 'Không tìm thấy';
          // dd($send_mail->service_id);
          $vps = $this->vps->find( $send_mail->service_id );
          // dd($vps);
          $content['ip'] = $vps->ip;
        }
        elseif ( $send_mail->type == 'mail_forgot_password' ) {
          $subject = 'Đổi mật khẩu tài khoản '. !empty($send_mail->user->email) ? $send_mail->user->email : 'Không tìm thấy';
          $content['name'] = !empty($send_mail->user->name) ? $send_mail->user->name : 'Không tìm thấy';
          $content['email'] = !empty($send_mail->user->email) ? $send_mail->user->email : 'Không tìm thấy';
          $content['token'] = !empty($send_mail->user->verify->token) ? $send_mail->user->verify->token : '';
        }
        $data = [
          'error' => 0,
          'subject' => $subject,
          'date_create_mail' => date('H:i:s d-m-Y', strtotime($send_mail->created_at)),
          'send_time' => date('H:i:s d-m-Y', strtotime($send_mail->send_time)),
          'type_send_mail' => !empty($type_send_mail[$send_mail->type]) ? $type_send_mail[$send_mail->type] : 'Không có trong thư viện',
          'content' => $content,
          'type' => $send_mail->type,
          'type_service' => $send_mail->type_service,
          'billing' => config('billing'),
          'status' => $send_mail->status
        ];
      } else {
        $data = [
          'error' => 1,
        ];
      }
    } catch (\Throwable $th) {
      report($th);
      $data = [
        'error' => 2,
      ];
    }
    return $data;
  }

  public function loadFilterSendMail( $qtt, $user, $action, $date )
  {
    $type_send_mail = config('type_send_mail');
    $data = [ 'data' => [] ];
    if (empty($qtt)) {
      $qtt = 30;
    }
    if ( empty($user) && empty($action) && empty($date) ) {
      $list_mail = $this->send_mail->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $qtt;
    }
    elseif ( !empty($user) && !empty($action) && !empty($date) ) {
      $list_mail = $this->send_mail->where('user_id', $user)->whereType($action)->whereDate('created_at', date('Y-m-d', strtotime($date)))->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->where('user_id', $user)->whereType($action)->whereDate('created_at', date('Y-m-d', strtotime($date)))->count();
    }
    elseif ( !empty($user) && !empty($action) ) {
      $list_mail = $this->send_mail->where('user_id', $user)->whereType($action)->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->where('user_id', $user)->whereType($action)->count();
    }
    elseif ( !empty($user) && !empty($date) ) {
      $list_mail = $this->send_mail->where('user_id', $user)->whereDate('created_at', date('Y-m-d', strtotime($date)))->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->where('user_id', $user)->whereDate('created_at', date('Y-m-d', strtotime($date)))->count();
    }
    elseif ( !empty($action) && !empty($date) ) {
      $list_mail = $this->send_mail->whereType($action)->whereDate('created_at', date('Y-m-d', strtotime($date)))->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->whereType($action)->whereDate('created_at', date('Y-m-d', strtotime($date)))->count();
    }
    elseif ( !empty($user) ) {
      $list_mail = $this->send_mail->where('user_id', $user)->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->whereType($action)->count();
    }
    elseif ( !empty($action) ) {
      $list_mail = $this->send_mail->whereType($action)->orderBy('id', 'desc')->paginate($qtt);
    }
    elseif ( !empty($date) ) {
      $list_mail = $this->send_mail->whereDate('created_at', date('Y-m-d', strtotime($date)))->orderBy('id', 'desc')->paginate($qtt);
      $data['total_mail'] = $this->send_mail->whereDate('created_at', date('Y-m-d', strtotime($date)))->count();
    }

    if ($list_mail->count()) {
      foreach ($list_mail as $key => $mail) {
        $mail->user_name = !empty($mail->user->name) ? $mail->user->name : '<span class="text-danger">Đã xóa</span>';
        $mail->user_mail = !empty($mail->user->email) ? $mail->user->email : '<span class="text-danger">Đã xóa</span>';
        $mail->date_create_mail = date('d-m-Y', strtotime($mail->created_at));
        $mail->type_send_mail = !empty($type_send_mail[$mail->type]) ? $type_send_mail[$mail->type] : 'Không có trong thư viện';
        if ( $mail->status ) {
          $mail->send_time = !empty(date('H:i:s d-m-Y', strtotime($mail->send_time))) ? date('H:i:s d-m-Y', strtotime($mail->send_time)) : '';
        }
        $data['data'][] = $mail;
      }
    }

    $data['total'] = $list_mail->total();
    $data['perPage'] = $list_mail->perPage();
    $data['current_page'] = $list_mail->currentPage();
    $data['total_send_mail'] = $list_mail->where('status', true)->count();
    return $data;

  }

}
