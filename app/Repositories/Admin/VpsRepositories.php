<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\VpsConfig;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Product;
use App\Model\GroupProduct;
use App\Model\Email;
use App\Events\VpsEvent;
use App\Model\HistoryPay;
use App\Model\TotalPrice;
use App\Model\OrderChangeVps;
use App\Model\SendMail;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\Proxy;
use Mail;
use App\Factories\AdminFactories;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class VpsRepositories {
//product - email - detail_order -user - order - user_send_email - subject
    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $vps_config;
    protected $server;
    protected $hosting;
    protected $product;
    protected $group_product;
    protected $email;
    protected $user_send_email;
    protected $subject;
    protected $total_price;
    protected $history_pay;
    protected $order_change_ip;
    protected $log_activity;
    protected $send_mail;
    protected $readnotification;
    protected $notification;
    protected $proxy;
    // API
    protected $da;
    protected $dashboard;

    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->vps_config = new VpsConfig;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->product = new Product;
        $this->email = new Email;
        $this->order_change_ip = new OrderChangeVps;
        $this->group_product = new GroupProduct;
        $this->total_price = new TotalPrice;
        $this->history_pay = new HistoryPay;
        $this->log_activity = new LogActivity();
        $this->send_mail = new SendMail;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->proxy = new Proxy();
        $this->dashboard = AdminFactories::dashBoardRepositories();
    }

    public function get_vps()
    {
        return $this->vps->with('user_vps','detail_order')->where('location', 'cloudzone')->orderBy('id', 'desc')->with('vps_config', 'order_addon_vps')->paginate(30);
    }

    public function get_vps_us()
    {
        return $this->vps->with('user_vps','detail_order')->where('location', 'us')->orderBy('id', 'desc')->with('vps_config', 'order_addon_vps')->paginate(30);
    }

    public function api_get_vps()
    {
        $list_vps = $this->vps->where('ip', '!=', "")->where('vm_id' , '<', 2000000000)->where('status_vps', '!=', 'delete_vps')->where('status_vps', '!=', 'cancel')->where('status_vps', '!=', 'change_user')->where('location', 'cloudzone')->with('user_vps')->orderBy('id', 'desc')->get();
        $data = [];
        // ->select('id', 'ip', 'next_due_date', 'vm_id')
        foreach ($list_vps as $key => $vps) {
            $data[$key]['id'] = $vps->id;
            $data[$key]['ip'] = $vps->ip;
            $data[$key]['next_due_date'] = $vps->next_due_date;
            $data[$key]['vm_id'] = $vps->vm_id;
            $data[$key]['user_id'] = $vps->user_id;
            $data[$key]['status_vps'] = $vps->status_vps;
            $data[$key]['customer_id'] = !empty($vps->user_vps->customer_id) ? $vps->user_vps->customer_id : 'Đã xóa';

            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            if (!empty($vps->addon_id)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {

                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $data[$key]['config'] = "$cpu-$ram-$disk";
        }
        return $data;
    }

    public function get_vps_by_vmid($list_vmid)
    {
        $list_vps = [];
        if ( is_array($list_vmid['vm_id']) ) {
            foreach ($list_vmid['vm_id'] as $key => $vm_id) {
                $list_vps_by_vmid = $this->vps->where('vm_id' , $vm_id)->where('location', 'cloudzone')->with('user_vps')->get();
                if ( $list_vps_by_vmid->count() > 0 ) {
                    foreach ($list_vps_by_vmid as $key => $vps_by_vmid) {
                        $list_vps[] = $vps_by_vmid;
                    }
                }
            }
        } else {
            $list_vps_by_vmid = $this->vps->where('vm_id' , $list_vmid['vm_id'])->where('location', 'cloudzone')->with('user_vps')->get();
            if ( $list_vps_by_vmid->count() > 0 ) {
                foreach ($list_vps_by_vmid as $key => $vps_by_vmid) {
                    $list_vps[] = $vps_by_vmid;
                }
            }
        }
        $data = [];
        // ->select('id', 'ip', 'next_due_date', 'vm_id')
        if ( count( $list_vps ) ) {
            $data['error'] = 0;
            foreach ($list_vps as $key => $vps) {
                $data[$key]['id'] = $vps->id;
                $data[$key]['ip'] = $vps->ip;
                $data[$key]['next_due_date'] = $vps->next_due_date;
                $data[$key]['vm_id'] = $vps->vm_id;
                $data[$key]['user_id'] = $vps->user_id;
                $data[$key]['status_vps'] = $vps->status_vps;
                $data[$key]['customer_id'] = !empty($vps->user_vps->customer_id) ? $vps->user_vps->customer_id : 'Đã xóa';$product = $vps->product;

                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                if (!empty($vps->addon_id)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {

                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $data[$key]['config'] = "$cpu-$ram-$disk";
            }
        } else {
            $data['error'] = 0;
            $data['note'] = "Không có VPS nào trùng với các vm_id";
        }
        return $data;
    }

    public function get_vps_us_by_vmid($list_vmid)
    {
        $list_vps = [];
        if ( is_array($list_vmid['vm_id']) ) {
            foreach ($list_vmid['vm_id'] as $key => $vm_id) {
                $list_vps_by_vmid = $this->vps->where('vm_id' , $vm_id)->where('location', 'us')->with('user_vps')->get();
                if ( $list_vps_by_vmid->count() > 0 ) {
                    foreach ($list_vps_by_vmid as $key => $vps_by_vmid) {
                        $list_vps[] = $vps_by_vmid;
                    }
                }
            }
        } else {
            $list_vps_by_vmid = $this->vps->where('vm_id' , $list_vmid['vm_id'])->where('location', 'us')->with('user_vps')->get();
            if ( $list_vps_by_vmid->count() > 0 ) {
                foreach ($list_vps_by_vmid as $key => $vps_by_vmid) {
                    $list_vps[] = $vps_by_vmid;
                }
            }
        }
        $data = [];
        // ->select('id', 'ip', 'next_due_date', 'vm_id')
        if ( count( $list_vps ) ) {
            $data['error'] = 0;
            foreach ($list_vps as $key => $vps) {
                $data[$key]['id'] = $vps->id;
                $data[$key]['ip'] = $vps->ip;
                $data[$key]['next_due_date'] = $vps->next_due_date;
                $data[$key]['vm_id'] = $vps->vm_id;
                $data[$key]['user_id'] = $vps->user_id;
                $data[$key]['status_vps'] = $vps->status_vps;
                $data[$key]['customer_id'] = !empty($vps->user_vps->customer_id) ? $vps->user_vps->customer_id : 'Đã xóa';$product = $vps->product;

                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                $data[$key]['config'] = "$cpu-$ram-$disk";
            }
        } else {
            $data['error'] = 0;
            $data['note'] = "Không có VPS nào trùng với các vm_id";
        }
        return $data;
    }

    public function get_all_vps_us()
    {
        $list_vps = $this->vps->where('ip', '!=', "")->where('status_vps', '!=', 'delete_vps')->where('status_vps', '!=', 'change_user')->where('status_vps', '!=', 'cancel')->where('location', 'us')->with('user_vps')->orderBy('id', 'desc')->get();
        $data = [];
        // ->select('id', 'ip', 'next_due_date', 'vm_id')
        foreach ($list_vps as $key => $vps) {
            $data[$key]['id'] = $vps->id;
            $data[$key]['ip'] = $vps->ip;
            $data[$key]['next_due_date'] = $vps->next_due_date;
            $data[$key]['vm_id'] = $vps->vm_id;
            $data[$key]['user_id'] = $vps->user_id;
            $data[$key]['status_vps'] = $vps->status_vps;
            $data[$key]['customer_id'] = !empty($vps->user_vps->customer_id) ? $vps->user_vps->customer_id : 'Đã xóa';

            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            $data[$key]['config'] = "$cpu-$ram-$disk";
        }
        return $data;
    }

    public function detail_vps($id)
    {
        return $this->vps->find($id);
    }

    public function change_ip($data)
    {
        // validate data
        if ( empty($data['ip']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi kết nối đổi IP cho VPS không có IP',
            ];
            return $restore;
        }
        elseif ( empty($data['user']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi kết nối đổi IP cho VPS không có user',
            ];
            return $restore;
        }
        elseif ( empty($data['password']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi kết nối đổi IP cho VPS không có password',
            ];
            return $restore;
        }
        elseif ( empty($data['vm_id']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi kết nối đổi IP cho VPS không có vm_id',
            ];
            return $restore;
        }
        elseif ( empty($data['status_vps']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi kết nối đổi IP cho VPS không có status_vps',
            ];
            return $restore;
        }

        if ( !empty( $data['create_order'] ) ) {
            $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
            $total = !empty( $data['total']) ? $data['total'] : 30000;
            if ( !empty($vps) ) {
                $user = $this->user->find($vps->user_id);
                $data_order = [
                    'user_id' => $user->id,
                    'total' => $total,
                    'status' => 'Finish',
                    'description' => 'change vps',
                    'makh' => '',
                ];
                $order = $this->order->create($data_order);
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'change_ip',
                    'due_date' => $date,
                    'description' => 'change vps',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '5',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'method_gd_invoice' => 'credit',
                    'money'=> $total,
                    'status' => 'Active',
                    'detail_order_id' => $detail_order->id,
                ];
                $this->history_pay->create($data_history);
                $data_change_ip = [
                    'detail_order_id' => $detail_order->id,
                    'vps_id' => $vps->id,
                    'ip' => $vps->ip,
                    'changed_ip' => $data['ip'],
                ];
                $order_change_vps = $this->order_change_ip->create($data_change_ip);
                $ip = $vps->ip;
                $vps->ip = $data['ip'];
                $vps->user = $data['user'];
                $vps->password = $data['password'];
                $vps->status_vps = $data['status_vps'];
                $update = $vps->save();
                if ($update) {
                    $restore = [
                      'error' => 0,
                      'status' => 'Cập nhật đổi IP cho VPS thành công',
                    ];
                    try {
                        // ghi log user
                        $data_log = [
                          'user_id' => 999998,
                          'action' => 'api đổi IP',
                          'model' => 'Admin/API_Dashboard',
                          'description' => ' đổi IP VPS ' . $ip . ' thành <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                        $data_log_user = [
                          'user_id' => $vps->user_id,
                          'action' => 'đổi IP',
                          'model' => 'Admin/VPSs',
                          'description_user' => ' VPS ' . $ip . ' <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log_user);
                    } catch (\Exception $e) {
                        return $restore;
                    }
                } else {
                    $restore = [
                      'error' => 1,
                      'status' => 'Cập nhật đổi IP cho VPS thất bại',
                    ];
                }
                return $restore;
            } else {
                $restore = [
                  'error' => 1,
                  'status' => 'Lỗi kết nối đổi IP cho VPS không có VPS này',
                ];
                return $restore;
            }
        } else {
            $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
            if ( !empty($vps) ) {
                $ip = $vps->ip;
                $vps->ip = $data['ip'];
                $vps->user = $data['user'];
                $vps->password = $data['password'];
                $vps->status_vps = $data['status_vps'];
                $update = $vps->save();
                if ($update) {
                    $restore = [
                      'error' => 0,
                      'status' => 'Cập nhật đổi IP cho VPS thành công',
                    ];
                    try {
                        // ghi log user
                        $data_log = [
                          'user_id' => 999998,
                          'action' => 'api đổi IP',
                          'model' => 'Admin/API_Dashboard',
                          'description' => ' đổi IP VPS ' . $ip . ' thành <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                        $data_log_user = [
                          'user_id' => $vps->user_id,
                          'action' => 'đổi IP',
                          'model' => 'Admin/VPSs',
                          'description_user' => ' VPS ' . $ip . ' <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log_user);
                    } catch (\Exception $e) {
                        return $restore;
                    }
                } else {
                    $restore = [
                      'error' => 1,
                      'status' => 'Cập nhật đổi IP cho VPS thất bại',
                    ];
                }
                return $restore;
            } else {
                $restore = [
                  'error' => 1,
                  'status' => 'Lỗi kết nối đổi IP cho VPS không có VPS này',
                ];
                return $restore;
            }
        }

    }

    public function check_vps_with_change_user($id)
    {
      $vps = $this->vps->find($id);
      if ( isset($vps) ) {
        // dd($vps);
        if ( $vps->status == 'Active' ) {
          if ( $vps->status_vps != 'delete_vps' && $vps->status_vps != 'change_user' && $vps->status_vps != 'cancel' ) {
            return [
              "error" => 0,
              "content" => [ $vps ],
            ];
          } else {
            return [
              "error" => 3,
              "content" => ''
            ];
          }
        } else {
          return [
            "error" => 2,
            "content" => ''
          ];
        }
      } else {
        return [
          "error" => 1,
          "content" => ''
        ];
      }
    }

    public function check_mutil_vps_with_change_user($list_id_vps)
    {
      $data = [ 'error' => 0 , 'content' => [], 'text_error' => ''];
      foreach ($list_id_vps as $key => $id) {
        $vps = $this->vps->find($id);if ( isset($vps) ) {
          // dd($vps);
          if ( $vps->status == 'Active' ) {
            if ( $vps->status_vps != 'delete_vps' && $vps->status_vps != 'change_user' && $vps->status_vps != 'cancel' ) {
              $data['content'][] = $vps;
            } else {
              $data['error'] = 3;
              $data['text_error'] = 'VPS ' . $vps->ip . 'được chọn không được ở trạng thái "đã xóa" - "đã chuyển" - "đã hủy". Vui lòng kiểm tra lại.';
            }
          } else {
            $data['error'] = 2;
            $data['text_error'] = 'VPS (#' . $vps->id . ') ' . $vps->ip . ' chưa được tạo. Vui lòng kiểm tra lại.';
          }
        } else {
          $data['error'] = 1;
          $data['text_error'] = 'VPS (#' . $vps->id . ') không có VPS được chọn trong dữ liệu. Vui lòng kiểm tra lại.';
        }
      }
      return $data;
    }

    public function check_mutil_vps_with_export_excel($list_id_vps)
    {
      $data = [ 'error' => 0 , 'content' => [], 'text_error' => ''];
      $user_id = 0;
      foreach ($list_id_vps as $key => $id) {
        $vps = $this->vps->find($id);if ( isset($vps) ) {
          if ( $key == 0 ) {
            $user_id = $vps->user_id;
          } else {
            if ( $user_id != $vps->user_id ) {
              $data['error'] = 4;
              $data['text_error'] = 'Các VPS được chọn không cùng một khách hàng.';
            }
          }
          // dd($vps);
          if ( $vps->status != 'Active' ) {
            $data['error'] = 2;
            $data['text_error'] = 'VPS (#' . $vps->id . ') ' . $vps->ip . ' chưa được tạo. Vui lòng kiểm tra lại.';
          } else {
            $data['content'][] = $vps;
          }
        } else {
          $data['error'] = 1;
          $data['text_error'] = 'VPS (#' . $vps->id . ') không có VPS được chọn trong dữ liệu. Vui lòng kiểm tra lại.';
        }
      }
      return $data;
    }

    public function change_user_by_vps($data)
    {
      $user = $this->user->find($data['user_id']);
      foreach ($data['id'] as $key => $id) {
        $vps = $this->vps->find($id);
        $check_dashboard = $this->dashboard->change_user($vps, $user->customer_id);
        // $check_dashboard = true;
        if ( $check_dashboard ) {
          $check_order = $this->order_change_vps_user($vps, $user);

          $data_vps = [
            'detail_order_id' => $check_order['detail_order']->id,
            'user_id' => $user->id,
            'product_id' => $check_order['product']->id,
            'next_due_date' => $vps->next_due_date,
            'ip' => !empty($vps->ip)? $vps->ip : '',
            'os' => $vps->os,
            'billing_cycle' => $vps->billing_cycle,
            'status' => 'Active',
            'date_create' => $vps->date_create,
            'user' => !empty($vps->user)? $vps->user : '',
            'password' => !empty($vps->password)? $vps->password : '',
            'paid' => 'paid',
            'addon_id' => $vps->addon_id,
            'security' => $vps->security,
            'type_vps' => $vps->type_vps,
            'vm_id' => $vps->vm_id,
            'status_vps' => $vps->status_vps,
            'location' => 'cloudzone'
          ];
          $create_vps = $this->vps->create($data_vps);

          if (!empty( $vps->vps_config )) {
            $addon_vps = $vps->vps_config;
            if ($addon_vps->cpu > 0 || $addon_vps->ram > 0 || $addon_vps->disk > 0  ) {
              $data_vps_config = [
                'vps_id' => $create_vps->id,
                'ip' => '0',
                'cpu' => $addon_vps->cpu,
                'ram' => $addon_vps->ram,
                'disk' => $addon_vps->disk,
              ];
              $this->vps_config->create($data_vps_config);
            }
          }
          # xoa vps của khách hàng yêu cầu chuyển
          $vps->status_vps = "change_user";
          $vps->vm_id = -1;
          $vps->save();
          // Tạo log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'chuyển VPS',
            'model' => 'Admin/VPS',
            'description' => $vps->ip . ' cho khách hàng <a href="/admin/users/detail/'. $user->id .'" target="_blank">'. $user->name . ' (' . $user->email . ')</a>' ,
            'description_user' => $vps->ip . ' cho khách hàng ' . $user->name . ' (' . $user->email . ')',
          ];
          $this->log_activity->create($data_log);

        } else {
          return [
            "error" => 1
          ];
        }
      }
      return [
        "error" => 0,
        "user" => $user
      ];
    }

    public function change_user_by_vps_us($data)
    {
      $user = $this->user->find($data['user_id']);
      foreach ($data['id'] as $key => $id) {
        $vps = $this->vps->find($id);
        $check_dashboard = $this->dashboard->change_user($vps, $user->customer_id);
        // $check_dashboard = true;
        if ( $check_dashboard ) {
          $check_order = $this->order_change_vps_us_user($vps, $user);
          if ( $check_order ) {
            $data_vps = [
              'detail_order_id' => $check_order['detail_order']->id,
              'user_id' => $user->id,
              'product_id' => $check_order['product']->id,
              'next_due_date' => $vps->next_due_date,
              'ip' => !empty($vps->ip)? $vps->ip : '',
              'os' => $vps->os,
              'billing_cycle' => $vps->billing_cycle,
              'status' => 'Active',
              'date_create' => $vps->date_create,
              'user' => !empty($vps->user)? $vps->user : '',
              'password' => !empty($vps->password)? $vps->password : '',
              'paid' => 'paid',
              'addon_id' => $vps->addon_id,
              'security' => $vps->security,
              'type_vps' => $vps->type_vps,
              'vm_id' => $vps->vm_id,
              'status_vps' => $vps->status_vps,
              'location' => 'us',
              'state' => $vps->state,
            ];
            $create_vps = $this->vps->create($data_vps);

            # xoa vps của khách hàng yêu cầu chuyển
            $vps->status_vps = "change_user";
            $vps->vm_id = -1;
            $vps->save();
            // Tạo log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'chuyển VPS US',
              'model' => 'Admin/VPS',
              'description' => $vps->ip . ' cho khách hàng <a href="/admin/users/detail/'. $user->id .'" target="_blank">'. $user->name . ' (' . $user->email . ')</a>' ,
              'description_user' => $vps->ip . ' cho khách hàng ' . $user->name . ' (' . $user->email . ')',
            ];
            $this->log_activity->create($data_log);
          } else {
            return [
              "error" => 2,
              "user" => $user
            ];
          }

        } else {
          return [
            "error" => 1,
            "user" => $user
          ];
        }
      }
      return [
        "error" => 0,
        "user" => $user
      ];
    }

    public function order_change_vps_user($vps, $user)
    {
        $product = $this->check_product_by_user($vps, $user);
        if ( $product ) {
          $billing_cycle = $vps->billing_cycle;
          $total = $product->pricing[$billing_cycle];
          // dd($vps->vps_config);
          if ( !empty($vps->vps_config) ) {
            $add_on_products = $this->get_addon_product_private($user->id);
            $addon_vps = $vps->vps_config;
            $pricing_addon = 0;
            foreach ($add_on_products as $key => $add_on_product) {
              if (!empty($add_on_product->meta_product->type_addon)) {
                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                }
                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                }
                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                }
              }
            }
            $total += $pricing_addon;
          }
          // dd($total);
          $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
          ];
          // tao order
          $data_order = [
              'user_id' => $user->id,
              'promotion_id' => null,
              'total' => $total,
              'status' => 'Finish',
              'description' => 'Chuyển khách hàng',
              'verify' => true,
          ];
          $create_order = $this->order->create($data_order);
          // detail order
          $data_order_detail = [
            'order_id' => $create_order->id,
            'type' => 'VPS',
            'due_date' => $vps->next_due_date,
            'description' => 'Chuyển khách hàng',
            'payment_method' => 1,
            'status' => 'paid',
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => $user->id,
            'paid_date' => $vps->paid_date,
            'addon_id' => !empty($vps->vps_config)? '1' : '0',
          ];
          $detail_order = $this->detail_order->create($data_order_detail);
          $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => $vps->date_create,
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
          ];
          $this->history_pay->create($data_history);
          return [
            'detail_order' => $detail_order,
            'product' => $product,
          ];
        } else {
          return null;
        }
    }

    public function order_change_vps_us_user($vps, $user)
    {
        $product = $this->check_product_vps_us_by_user($vps, $user);
        if ( $product ) {
          $billing_cycle = $vps->billing_cycle;
          $total = $product->pricing[$billing_cycle];
          // dd($vps->vps_config);
          // dd($total);
          $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
          ];
          // tao order
          $data_order = [
              'user_id' => $user->id,
              'promotion_id' => null,
              'total' => $total,
              'status' => 'Finish',
              'description' => 'Chuyển khách hàng',
              'verify' => true,
          ];
          $create_order = $this->order->create($data_order);
          // detail order
          $data_order_detail = [
            'order_id' => $create_order->id,
            'type' => 'VPS-US',
            'due_date' => $vps->next_due_date,
            'description' => 'Chuyển khách hàng',
            'payment_method' => 1,
            'status' => 'paid',
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => $user->id,
            'paid_date' => $vps->paid_date,
            'addon_id' => !empty($vps->vps_config)? '1' : '0',
          ];
          $detail_order = $this->detail_order->create($data_order_detail);
          $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODVU' . strtoupper(substr(sha1(time()), 35, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => $vps->date_create,
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
          ];
          $this->history_pay->create($data_history);
          return [
            'detail_order' => $detail_order,
            'product' => $product,
          ];
        } else {
          return null;
        }
    }

    public function check_product_by_user($vps, $user)
    {
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('type', 'vps')->where('hidden', false)->where('group_user_id', $user->group_user_id)->get();
        } else {
            $group_products = $this->group_product->where('type', 'vps')->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
        }
        $product_before = $vps->product;
        foreach ($group_products as $key => $group_product) {
          foreach ($group_product->products as $key => $product) {
            if ( $product_before->meta_product->cpu == $product->meta_product->cpu && $product_before->meta_product->memory == $product->meta_product->memory && $product_before->meta_product->disk == $product->meta_product->disk ) {
              return $product;
            }
          }
        }
        return null;
    }

    public function check_product_vps_us_by_user($vps, $user)
    {
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('type', 'vps_us')->where('hidden', false)->where('group_user_id', $user->group_user_id)->get();
        } else {
            $group_products = $this->group_product->where('type', 'vps_us')->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
        }
        $product_before = $vps->product;
        foreach ($group_products as $key => $group_product) {
          foreach ($group_product->products as $key => $product) {
            if ( $product_before->meta_product->cpu == $product->meta_product->cpu && $product_before->meta_product->memory == $product->meta_product->memory && $product_before->meta_product->disk == $product->meta_product->disk ) {
              return $product;
            }
          }
        }
        return null;
    }

    public function list_all_user()
    {
      return $this->user->get();
    }

    public function change_next_due_date($data_dashboard)
    {
        // validate data
        if ( empty($data_dashboard) ) {
            $restore = [
              'error' => 4,
              'status' => 'Lỗi kết dữ liệu null',
            ];
            return $restore;
        }
        if ( count($data_dashboard) == 0 ) {
            $restore = [
              'error' => 5,
              'status' => 'Lỗi kết không có dữ liệu',
            ];
            return $restore;
        }
        $save = false;

        foreach ($data_dashboard['vps'] as $key => $data) {
            // validate data
            if ( empty($data['vm_id']) ) {
                $restore = [
                  'error' => 1,
                  'status' => 'Lỗi kết nối đổi ngày kết thúc cho VPS không có VM_ID',
                ];
                return $restore;
            }
            elseif ( empty($data['next_due_date']) ) {
                $restore = [
                  'error' => 1,
                  'status' => 'Lỗi kết nối đổi ngày kết thúc cho VPS không có ngày kết thúc',
                ];
                return $restore;
            }
            $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
            if ( isset($vps) ) {
                $list_vps = $this->vps->where('vm_id', $data['vm_id'])->get();
                foreach ($list_vps as $key => $vps) {
                    if ( !empty($data['next_due_date']) ) {
                        $vps->next_due_date = $data['next_due_date'];
                    }
                    if ( !empty($data['customer_id']) ) {
                        $user = $this->user->where("customer_id", $data['customer_id'])->first();
                        if ( isset($vps) ) {
                          $vps->user_id = $user->id;
                        }
                    }
                    if ( !empty($data['status_vps']) ) {
                        $vps->status_vps = $data['status_vps'];
                    }
                    if ( !empty($data['ip']) ) {
                      $vps->ip = $data['ip'];
                    }
                    if ( !empty($data['state']) ) {
                        $states = config('states');
                        $vps->state = !empty($states[$data['state']]) ? $states[$data['state']] : 'Unknow';
                    }
                    if ( !empty($data['password']) ) {
                        $states = config('password');
                        $vps->password = !empty($data['password']) ? $data['password'] : '';
                    }
                    $save = $vps->save();
                    // ghi log admin
                    try {
                        $text_vps = '#' . $vps->id;
                        if (!empty($vps->ip)) {
                            $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                        }
                        $data_log = [
                              'user_id' => 999998,
                              'action' => 'chỉnh sửa',
                              'model' => 'Admin/DashBoard',
                              'description' => ' VPS thành công ' . $text_vps,
                              'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                    } catch (Exception $e) {
                        report($e);
                    }
                }

            } else {
                $restore = [
                  'error' => 1,
                  'status' => 'VPS không có trên Portal',
                ];
                return $restore;
            }
        }

        if ($save) {
            $restore = [
              'error' => 0,
              'status' => 'Cập nhật VPS thành công',
          ];
          return $restore;
        } else {
            $restore = [
              'error' => 1,
              'status' => 'Cập nhật VPS thất bại',
            ];
            return $restore;
        }

    }

    public function change_user($data)
    {
        if ( empty($data['vm_id']) ) {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có vm_id',
          ];
          return $restore;
        }
        if ( empty($data['customer_id']) ) {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có customer id',
          ];
          return $restore;
        }
        if ( empty($data['status_vps']) ) {
            $restore = [
              'error' => 1,
              'status' => 'Lỗi không có status_vps',
            ];
            return $restore;
        }
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if ( isset($vps) ) {
            $user = $this->user->where('customer_id', $data['customer_id'])->first();
            if ( isset($user) ) {
                $vps->user_id = $user->id;
                $vps->status_vps = $data['status_vps'];
                $vps->save();
                $restore = [
                  'error' => 0,
                  'status' => 'Đổi user thành công',
                ];
                return $restore;
            } else {
                $restore = [
                  'error' => 1,
                  'status' => 'Không có User trên Portal',
                ];
                return $restore;
            }
        } else {
            $restore = [
              'error' => 1,
              'status' => 'Không có VPS trên Portal',
            ];
            return $restore;
        }
    }

    public function api_delete_vps($data)
    {
        if ( empty($data['vm_id']) ) {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có vm_id',
          ];
          return $restore;
        }
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if ( isset($vps) ) {
          $vps->status_vps = "delete_vps";
          $vps->vm_id = -1;
          $vps->save();
          try {
                $text_vps = '#' . $vps->id;
                if (!empty($vps->ip)) {
                    $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                }
                $data_log = [
                  'user_id' => 999998,
                  'action' => 'xóa',
                  'model' => 'Admin/DashBoard',
                  'description' => ' VPS ' . $text_vps,
                  'service' =>  $vps->ip,
                ];
                 $this->log_activity->create($data_log);
          } catch (Exception $e) {
                report($e);
          }
          $restore = [
            'error' => 0,
            'status' => 'Cập nhật xóa vps thành công',
          ];
          return $restore;
        } else {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có VPS trong Portal',
          ];
          return $restore;
        }
    }

    public function api_change_user($data)
    {
        if ( empty($data['vm_id']) ) {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có vm_id',
          ];
          return $restore;
        }
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if ( isset($vps) ) {
          $vps->status_vps = "change_user";
          $vps->vm_id = -1;
          $vps->save();
          try {
                $text_vps = '#' . $vps->id;
                if (!empty($vps->ip)) {
                    $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                }
                $data_log = [
                  'user_id' => 999998,
                  'action' => 'chuyển khách hàng',
                  'model' => 'Admin/DashBoard',
                  'description' => ' VPS ' . $text_vps,
                  'service' =>  $vps->ip,
                ];
                 $this->log_activity->create($data_log);
          } catch (Exception $e) {
                report($e);
          }
          $restore = [
            'error' => 0,
            'status' => 'Cập nhật xóa vps thành công',
          ];
          return $restore;
        } else {
          $restore = [
            'error' => 1,
            'status' => 'Lỗi không có VPS trong Portal',
          ];
          return $restore;
        }
    }

    public function delete_vps($id)
    {
        $vps = $this->vps->find($id);
        // ghi log
        $text_vps = '#' . $vps->id;
        if (!empty($vps->ip)) {
           $text_vps .= ' Ip: ' . $vps->ip;
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        if (!empty($vps)) {
            // Sửa detail order
            if ( $vps->status == 'Active' ) {
              $vps->vm_id = -1;
              $vps->status_vps = 'delete_vps';
              $delete_vps = $vps->save();
            } else {
              $delete_vps = $vps->delete();
            }
            return $delete_vps;
        }
        return false;
    }

    public function create_action($vps)
    {
        $data = [
            'create' => 'Tạo VPS thành công',
            'off' => '',
            'on' => '',
            'delete' => '',
            'cancel' => '',
        ];
        return $data;
    }

    public function change_ip_action($list_id)
    {
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->admin_change_ip($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'đổi IP',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function on_action($list_id)
    {
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->onVPS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'bật',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function us_on_action($vps)
    {
        foreach ($vps as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->onVPSUS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'bật',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function off_action($list_id)
    {
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->offVPS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'tắt',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function us_off_action($vps)
    {
        foreach ($vps as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->offVPSUS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'tắt',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function restart_action($list_id)
    {
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->restartVPS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'khởi động lại',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function us_restart_action($vps)
    {
        foreach ($vps as $key => $id) {
            $vps = $this->vps->find($id);
            if ( isset($vps) ) {
                $off = $this->dashboard->onVPSUS($vps);
                if ( $off ) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'khởi động lại',
                      'model' => 'Admin/VPS',
                      'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                }
            }
        }
        return true;
    }

    public function off_vps($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->offVPS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'tắt',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function off_vps_us($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->offVPSUS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'tắt',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function on_vps($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->onVPS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'bật',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function on_vps_us($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->onVPSUS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'bật',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function restart_vps($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->restartVPS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'khởi động lại',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function restart_vps_us($id)
    {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
            $off = $this->dashboard->onVPSUS($vps);
            if ( $off ) {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'khởi động lại',
                  'model' => 'Admin/VPS',
                  'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                return true;
            } else {
                return false;
            }

        }
        return false;
    }

    public function delete_action($list_id)
    {
        foreach ($list_id as $key => $id) {
            // ghi log
            $vps_choose = $this->vps->find($id);
            if ( isset($vps_choose) ) {
              $text_vps = '#' . $vps_choose->id;
              if (!empty($vps_choose->ip)) {
                $text_vps .= ' Ip: ' . $vps_choose->ip;
              }
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'xóa',
                'model' => 'Admin/VPS',
                'description' => ' VPS ' . $text_vps,
                'service' =>  $vps_choose->ip,
              ];
              $this->log_activity->create($data_log);
              if ( $vps_choose->status == 'Active' ) {
                $vps_choose->vm_id = -1;
                $vps_choose->status_vps = 'delete_vps';
                $delete = $vps_choose->save();
              } else {
                $delete = $vps_choose->delete();
              }
              if (!$delete) {
                  $data = [
                      'create' => '',
                      'off' => '',
                      'on' => '',
                      'delete' => 'Xóa VPS ID: ' + $id + 'thất bại.',
                      'cancel' => '',
                  ];
                  return $data;
              }
            }
        }
        $data = [
            'create' => '',
            'off' => '',
            'on' => '',
            'delete' => 'Xóa VPS thành công ',
            'cancel' => '',
        ];
        return $data;
    }

    public function cancel_action($vps)
    {
        foreach ($list_id as $key => $id) {
            $cancel_vps = $this->vps->find($id);
            $cancel_vps->status = 'Cancel';
            $action = $cancel_vps->save();
            // ghi log
            $text_vps = '#' . $cancel_vps->id;
            if (!empty($cancel_vps->ip)) {
               $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $cancel_vps->id  . '">' . $cancel_vps->ip . '</a>';
            }
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'hủy',
              'model' => 'Admin/VPS',
              'description' => ' VPS ' . $text_vps,
              'service' =>  $cancel_vps->ip,
            ];
            $this->log_activity->create($data_log);


            if ( $vps_choose->status == 'Active' ) {
              $vps_choose->vm_id = -1;
              $vps_choose->status_vps = 'delete_vps';
              $delete = $vps_choose->save();
            } else {
              $delete = $vps_choose->delete();
            }
            if (!$action) {
                $data = [
                    'create' => '',
                    'off' => '',
                    'on' => '',
                    'delete' => '',
                    'cancel' => 'Hủy VPS ID: '.$id.' thất bại',
                ];
                return $data;
            }
        }
        $data = [
            'create' => '',
            'off' => '',
            'on' => '',
            'delete' => '',
            'cancel' => 'Hủy VPS thành công',
        ];
        return $data;
    }

    public function detail_create_action($vps_id)
    {
        $data = [
            'create' => 'Tạo VPS thành công',
            'off' => '',
            'on' => '',
            'delete' => '',
            'cancel' => '',
            'paid' => '',
            'unpaid' => '',
        ];
        return $data;
    }

    public function detail_on_action($vps_id)
    {
        $vps = $this->vps->find($vps_id);
        $vps->status_vps = 'on';
        $action = $vps->save();
        if ($action) {
            $data = [
                'create' => '',
                'off' => '',
                'on' => 'Bật VPS thành công',
                'delete' => '',
                'cancel' => '',
                'paid' => '',
                'unpaid' => '',
            ];
        } else {
            $data = [
                'create' => '',
                'off' => '',
                'on' => 'Bật VPS thất bại',
                'delete' => '',
                'cancel' => '',
                'paid' => '',
                'unpaid' => '',
            ];
        }
        return $data;
    }

    public function detail_paid_action($vps_id)
    {
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];

        $vps = $this->vps->find($vps_id);
        $vps->next_due_date = date('Y-m-d', strtotime( $vps->created_at . $billing[$vps->billing_cycle] ));
        $vps->paid = 'paid';
        $vps->save();
        $detail_order_vps = $this->vps->where('detail_order_id', $vps->detail_order_id)->count();
        $vps_paid = $this->vps->where('paid', 'paid')->count();
        if ($detail_order_vps == $vps_paid) {
            $detail_order = $this->detail_order->find($vps->detail_order_id);
            $detail_order->status = 'paid';
            $detail_order->paid_date = date('Y-m-d');
            $detail_order->due_date = date('Y-m-d', strtotime( $vps->created_at . $billing[$vps->billing_cycle] ));
            $detail_order->save();
        }
        $data = [
            'create' => '',
            'off' => '',
            'on' => '',
            'delete' => '',
            'cancel' => '',
            'paid' => 'Gán thanh toán cho VPS thành công',
            'unpaid' => '',
        ];
        return $data;
    }

    public function detail_unpaid_action($vps_id)
    {
        $vps = $this->vps->find($vps_id);
        $vps->paid = 'unpaid';
        $vps->save();
        $data = [
            'create' => '',
            'off' => '',
            'on' => '',
            'delete' => '',
            'cancel' => '',
            'paid' => '',
            'unpaid' => 'Gán chưa thanh toán cho VPS thành công',
        ];
        return $data;
    }

    public function detail_off_action($vps_id)
    {
        $data = [
            'create' => '',
            'off' => 'Tắt VPS thành công',
            'on' => '',
            'delete' => '',
            'cancel' => '',
            'paid' => '',
            'unpaid' => '',
        ];
        return $data;
    }

    public function detail_delete_action($vps_id)
    {
        // ghi log
        $vps = $this->vps->find($vps_id);
        $text_vps = '#' . $vps->id;
        if (!empty($vps->ip)) {
           $text_vps .= ' Ip: ' . $vps->ip;
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        if ( $vps->status == 'Active' ) {
          $vps->vm_id = -1;
          $vps->status_vps = 'delete_vps';
          $delete = $vps->save();
        } else {
          $delete = $vps->delete();
        }
        if ($delete) {
            $data = [
                'create' => '',
                'off' => '',
                'on' => '',
                'delete' => 'Xóa VPS thành công',
                'cancel' => '',
                'paid' => '',
                'unpaid' => '',
            ];
            return $data;
        } else {
            $data = [
                'create' => '',
                'off' => '',
                'on' => '',
                'delete' => 'Xóa VPS thất bại',
                'cancel' => '',
                'paid' => '',
                'unpaid' => '',
            ];
            return $data;
        }
    }

    public function detail_cancel_action($vps_id)
    {
        $cancel = $this->vps->find($vps_id);
        $cancel->status = 'Cancel';
        $action = $cancel->save();
        // ghi log
        $text_vps = '#' . $cancel->id;
        if (!empty($cancel->ip)) {
           $text_vps .= ' Ip: ' . $cancel->ip;
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  ' dịch vụ ' . $cancel->ip,
        ];
        $this->log_activity->create($data_log);
        if ($action) {
            $data = [
                'create' => '',
                'off' => '',
                'on' => '',
                'delete' => '',
                'cancel' => 'Hủy VPS thành công',
                'paid' => '',
                'unpaid' => '',
            ];
            return $data;
        } else {
            $data = [
                'create' => '',
                'off' => '',
                'on' => '',
                'delete' => '',
                'cancel' => 'Hủy VPS thất bại',
                'paid' => '',
                'unpaid' => '',
            ];
            return $data;
        }
    }

    public function update_sevice_vps($data)
    {
        $vps = $this->vps->find($data['id']);
        if ( $vps->user_id != $data['user_id'] ) {
            $user = $this->user->find($data['user_id']);
            $this->dashboard->update_sevice_vps($vps, $user);
        }
        $data_vps = [
            'user_id' => $data['user_id'],
            'product_id' => $data['product_id'],
            'ip' => !empty($data['ip']) ? $data['ip'] : '',
            'user' => !empty($data['sevices_username']) ? $data['sevices_username'] : '',
            'password' => !empty($data['sevices_password']) ? $data['sevices_password'] : '',
            'status' => !empty($data['status']) ? $data['status'] : '',
            'price_override' => !empty($data['price_override']) ? $data['sub_total_hidden'] : 0,
            'expire_billing_cycle' => !empty($data['expire_billing_cycle']) ? $data['expire_billing_cycle'] : 0,
            'status_vps' => !empty($data['status_vps']) ? $data['status_vps'] : '',
            'vm_id' => !empty($data['vm_id']) ? $data['vm_id'] : '',
        ];
        if ( !empty($data['date_create']) && !empty($data['billing_cycle']) ) {
            // Tính ngày kết thúc cho VPS
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_create = $data['date_create'];
            $billing_cycle = $data['billing_cycle'];
            $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));
            $data_vps['date_create'] = date('Y-m-d', strtotime($data['date_create']));
            $data_vps['next_due_date'] = $next_due_date;
            $data_vps['billing_cycle'] = $data['billing_cycle'];
            $config_billing_DashBoard = config('billingDashBoard');
            $data_update_leased_time = [
              "mob" => "update_leased_time",
              "name" => $vps->vm_id,
              "created_date" => date('Y-m-d', strtotime($data['date_create'])),
              "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
            ];
            $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);
        }
        elseif (!empty($data['date_create'])) {
          // Tính ngày kết thúc cho VPS
          $billing = [
              'monthly' => '1 Month',
              'twomonthly' => '2 Month',
              'quarterly' => '3 Month',
              'semi_annually' => '6 Month',
              'annually' => '1 Year',
              'biennially' => '2 Year',
              'triennially' => '3 Year'
          ];
          $date_create = $data['date_create'];
          $billing_cycle = $vps->billing_cycle;
          $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));

          $data_vps['date_create'] = date('Y-m-d', strtotime($data['date_create']));
          $data_vps['next_due_date'] = $next_due_date;
          $config_billing_DashBoard = config('billingDashBoard');
          $data_update_leased_time = [
            "mob" => "update_leased_time",
            "name" => $vps->vm_id,
            "created_date" => date('Y-m-d', strtotime($data['date_create'])),
            "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
          ];
          $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);

        }
        elseif (!empty($data['billing_cycle'])) {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_create = $vps->date_create;
            $billing_cycle = $data['billing_cycle'];
            $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));

            $data_vps['next_due_date'] = $next_due_date;
            $data_vps['billing_cycle'] = $data['billing_cycle'];

            $config_billing_DashBoard = config('billingDashBoard');
            $data_update_leased_time = [
              "mob" => "update_leased_time",
              "name" => $vps->vm_id,
              "created_date" => $date_create,
              "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
            ];
            $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);
        }

        if (!empty($data['next_due_date'])) {
            $data_vps['next_due_date'] = date('Y-m-d', strtotime($data['next_due_date']));
            // dd(strtotime($data['next_due_date']));
        }

        if (!empty($data['paid_date'])) {
          $invoice = $this->detail_order->find($data['invoice_id']);
          $invoice->paid_date = date('Y-m-d', strtotime( $data['paid_date'] ));
          $invoice->save();
        }
        // ghi log
        $text_vps = '#' . $vps->id;
        if (!empty($vps->ip)) {
           $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        if ( !empty($data['addon_vps']) ) {
            if ( !empty($vps->vps_config) ) {
              $vps_config = $vps->vps_config;
              $vps_config->cpu = $data["addon_cpu"];
              $vps_config->ram = $data["addon_ram"];
              $vps_config->disk = $data["addon_disk"];
              $vps_config->save();
            } else {
                $data_config = [
                    'vps_id' => $vps->id,
                    'ip' => 0,
                    'cpu' => $data["addon_cpu"],
                    'ram' => $data["addon_ram"],
                    'disk' => $data["addon_disk"],
                ];
                $this->vps_config->create($data_config);
            }
        }

        $update = $this->vps->find($data['id'])->update($data_vps);

        return $update;
    }

    public function update_sevice_vps_us($data)
    {
        $vps = $this->vps->find($data['id']);
        $data_vps = [
            'user_id' => $data['user_id'],
            'product_id' => $data['product_id'],
            'ip' => !empty($data['ip']) ? $data['ip'] : '',
            'user' => !empty($data['sevices_username']) ? $data['sevices_username'] : '',
            'password' => !empty($data['sevices_password']) ? $data['sevices_password'] : '',
            'status' => !empty($data['status']) ? $data['status'] : '',
            'price_override' => !empty($data['price_override']) ? $data['sub_total_hidden'] : 0,
            'expire_billing_cycle' => !empty($data['expire_billing_cycle']) ? $data['expire_billing_cycle'] : 0,
            'status_vps' => !empty($data['status_vps']) ? $data['status_vps'] : '',
            'vm_id' => !empty($data['vm_id']) ? $data['vm_id'] : '',
            'state' => !empty($data['state']) ? $data['state'] : 'US',
        ];
        if ( !empty($data['date_create']) && !empty($data['billing_cycle']) ) {
            // Tính ngày kết thúc cho VPS
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_create = $data['date_create'];
            $billing_cycle = $data['billing_cycle'];
            $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));
            $data_vps['date_create'] = date('Y-m-d', strtotime($data['date_create']));
            $data_vps['next_due_date'] = $next_due_date;
            $data_vps['billing_cycle'] = $data['billing_cycle'];
            $config_billing_DashBoard = config('billingDashBoard');
            $data_update_leased_time = [
              "mob" => "update_leased_time",
              "name" => $vps->vm_id,
              "created_date" => date('Y-m-d', strtotime($data['date_create'])),
              "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
            ];
            $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);
        }
        elseif (!empty($data['date_create'])) {
          // Tính ngày kết thúc cho VPS
          $billing = [
              'monthly' => '1 Month',
              'twomonthly' => '2 Month',
              'quarterly' => '3 Month',
              'semi_annually' => '6 Month',
              'annually' => '1 Year',
              'biennially' => '2 Year',
              'triennially' => '3 Year'
          ];
          $date_create = $data['date_create'];
          $billing_cycle = $vps->billing_cycle;
          $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));

          $data_vps['date_create'] = date('Y-m-d', strtotime($data['date_create']));
          $data_vps['next_due_date'] = $next_due_date;
          $config_billing_DashBoard = config('billingDashBoard');
          $data_update_leased_time = [
            "mob" => "update_leased_time",
            "name" => $vps->vm_id,
            "created_date" => date('Y-m-d', strtotime($data['date_create'])),
            "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
          ];
          $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);

        }
        elseif (!empty($data['billing_cycle'])) {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_create = $vps->date_create;
            $billing_cycle = $data['billing_cycle'];
            $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));

            $data_vps['next_due_date'] = $next_due_date;
            $data_vps['billing_cycle'] = $data['billing_cycle'];

            $config_billing_DashBoard = config('billingDashBoard');
            $data_update_leased_time = [
              "mob" => "update_leased_time",
              "name" => $vps->vm_id,
              "created_date" => $date_create,
              "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
            ];
            $reques_vcenter = $this->dashboard->update_leased_time($vps->vm_id, $data_update_leased_time, $vps->ip);
        }

        if (!empty($data['next_due_date'])) {
            $data_vps['next_due_date'] = date('Y-m-d', strtotime($data['next_due_date']));
            // dd(strtotime($data['next_due_date']));
        }

        if (!empty($data['paid_date'])) {
          $invoice = $this->detail_order->find($data['invoice_id']);
          $invoice->paid_date = date('Y-m-d', strtotime( $data['paid_date'] ));
          $invoice->save();
        }
        // ghi log
        $text_vps = '#' . $vps->id;
        if (!empty($vps->ip)) {
           $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        if ( !empty($data['addon_vps']) ) {
          if ( !empty($vps->vps_config) ) {
              $vps_config = $vps->vps_config;
              $vps_config->cpu = $data["addon_cpu"];
              $vps_config->ram = $data["addon_ram"];
              $vps_config->disk = $data["addon_disk"];
              $vps_config->save();
          }
        }

        $update = $this->vps->find($data['id'])->update($data_vps);

        return $update;
    }

    public function loadIPVcenter($dropship, $data_center)
    {
        return '';
    }

    public function listVpsByUser($userId)
    {
        return $this->vps->where('user_id',$userId)->orderBy('id', 'desc')->paginate(30);
    }

    public function list_vps_by_user($userId, $status_vps)
    {
        if ( !empty($status_vps) ) {
          $list_vps = $this->vps->where('user_id',$userId)->where('status', 'Active')->where('location', 'cloudzone')
          ->where('status_vps', $status_vps)->with('product')->orderBy('id', 'desc')->paginate(30);
        } else {
          $list_vps = $this->vps->where('user_id',$userId)->where('status', 'Active')->where('location', 'cloudzone')->with('product')->orderBy('id', 'desc')->paginate(30);
        }


        $data = [];
        $data['data'] = [];
        $config_os = config('os');
        $billing = config('billing');
        foreach ($list_vps as $key => $vps) {
            // tên sản phẩm
            $vps->product_name = !empty($vps->product->name) ? $vps->product->name : 'Đã xóa';
            // ngày tạo
            $vps->start_date = date('h:i:s d-m-Y' , strtotime($vps->created_at));
            // giá
            $total = 0;
            if ( !empty($vps->price_override) ) {
              $total = $vps->price_override;
            } else {
               $product = $vps->product;
               $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
               if (!empty($vps->vps_config)) {
                 $addon_vps = $vps->vps_config;
                 $add_on_products = $this->get_addon_product_private($vps->user_id);
                 $pricing_addon = 0;
                 foreach ($add_on_products as $key => $add_on_product) {
                   if (!empty($add_on_product->meta_product->type_addon)) {
                     if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                       $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                     }
                     if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                       $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                     }
                     if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                       $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                     }
                   }
                 }
                 $total += $pricing_addon;
               }
            }
            $vps->amount = number_format( $total ,0,",",".");
            // ngày kết thúc
            $vps->end_date = date('h:i:s d-m-Y' , strtotime($vps->next_due_date));
            // chu kỳ
            $vps->billing_cycle = !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
            // cấu hình
            $product = $vps->product;
            if (!empty($product->meta_product)) {
                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 1;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 1;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 20;
                if ($vps->vps_config) {
                   $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                   $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                   $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                }
                $vps->config_vps = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }
            // loại
            if ( $vps->location == 'us' ) {
              $type = 'VPS US - ';
              $type .= $vps->state;
              $vps->text_type_vps = $type;
            } else {
              if ( $vps->type_vps == 'vps'  ) {
                $vps->text_type_vps = 'VPS';
              } else {
                $vps->text_type_vps = 'NAT-VPS';
              }
            }
            $data['data'][] = $vps;
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;
    }

    public function list_vps_us_by_user($userId, $status_vps)
    {
        if (!empty($status_vps)) {
          $list_vps = $this->vps->where('user_id',$userId)->where('status', 'Active')->where('location', 'us')
          ->where('status_vps', $status_vps)->with('product')->orderBy('id', 'desc')->paginate(30);
        } else {
          $list_vps = $this->vps->where('user_id',$userId)->where('status', 'Active')->where('location', 'us')->with('product')->orderBy('id', 'desc')->paginate(30);
        }

        $data = [];
        $data['data'] = [];
        $config_os = config('os');
        $billing = config('billing');
        foreach ($list_vps as $key => $vps) {
            // tên sản phẩm
            $vps->product_name = !empty($vps->product->name) ? $vps->product->name : 'Đã xóa';
            // ngày tạo
            $vps->start_date = date('h:i:s d-m-Y' , strtotime($vps->created_at));
            // giá
            $total = 0;
            if ( !empty($vps->price_override) ) {
              $total = $vps->price_override;
            } else {
               $product = $vps->product;
               $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
               if (!empty($vps->vps_config)) {
                 $addon_vps = $vps->vps_config;
                 $add_on_products = $this->get_addon_product_private($vps->user_id);
                 $pricing_addon = 0;
                 foreach ($add_on_products as $key => $add_on_product) {
                   if (!empty($add_on_product->meta_product->type_addon)) {
                     if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                       $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                     }
                     if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                       $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                     }
                     if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                       $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                     }
                   }
                 }
                 $total += $pricing_addon;
               }
            }
            $vps->amount = number_format( $total ,0,",",".");
            // ngày kết thúc
            $vps->end_date = date('h:i:s d-m-Y' , strtotime($vps->next_due_date));
            // chu kỳ
            $vps->billing_cycle = !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
            // cấu hình
            $product = $vps->product;
            if (!empty($product->meta_product)) {
                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 1;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 1;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 20;
                if ($vps->vps_config) {
                   $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                   $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                   $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                }
                $vps->config_vps = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }
            // loại
            if ( $vps->location == 'us' ) {
              $type = 'VPS US - ';
              $type .= $vps->state;
              $vps->text_type_vps = $type;
            } else {
              if ( $vps->type_vps == 'vps'  ) {
                $vps->text_type_vps = 'VPS';
              } else {
                $vps->text_type_vps = 'NAT-VPS';
              }
            }
            $data['data'][] = $vps;
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;
    }

    public function totalVPSByUser($userId){
        return $this->vps->where('user_id',$userId)->where('status','Active')->count();
    }

    public function countUsed($userId)
    {
        return $this->vps->where('user_id',$userId)->where('status','Active')
            ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild' ])
            ->count();
    }

    public function countDelete($userId)
    {
        return $this->vps->where('user_id',$userId)->where('status','Active')
            ->whereIn('status_vps', [ 'delete_vps', 'change_user', 'cancel' ])
            ->count();
    }

    public function countExpire($userId)
    {
        return $this->vps->where('user_id',$userId)->where('status','Active')
            ->where('status_vps', 'expire')
            ->count();
    }

    // Api VPS form DashBoard
    public function update_status_vps_form_dashboard($data)
    {
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if (isset($vps)) {
            if ($vps->status_vps == 'progressing') {
              $vps->status_vps = $data['status'];
              $vps->save();
              // $event = event(new VpsEvent($vps->user_id, $vps->ip));
              $this->send_mail_finish($vps);
              return true;
            }
            elseif ($vps->status_vps == 'rebuild') {
                $vps->status_vps = $data['status'];
                $vps->save();
                // $event = event(new VpsEvent($vps->user_id, $vps->ip));
                $this->send_mail_rebuild($vps);
                return true;
            }
            elseif ($vps->status_vps == 'change_ip') {
                $vps->status_vps = $data['status'];
                $vps->save();
                // $event = event(new VpsEvent($vps->user_id, $vps->ip));
                $this->send_mail_change_ip($vps);
                return true;
            }
            else {
                $vps->status_vps = $data['status'];
                $vps->save();
                return true;
            }
            try {
                        $text_vps = '#' . $vps->id;
                        if (!empty($vps->ip)) {
                            $text_vps .= ' IP: <a target="_blank" href="/admin/vps/detail/' . $vps->id  . '">' . $vps->ip . '</a>';
                        }
                        $data_log = [
                              'user_id' => 999998,
                              'action' => 'chỉnh sửa',
                              'model' => 'Admin/DashBoard',
                              'description' => ' trạng thái VPS ' . $text_vps,
                              'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
            } catch (Exception $e) {
                        report($e);
            }
        }
        return true;
    }

    public function update_rebuild_vps($data)
    {
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if (isset($vps)) {
          $user = $vps->user_vps;
          $content = '<p>Xin chào <b>' . $user->name . ',</b> </p>' ;
          $content .= '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu cài đặt lại hệ điều hành cho VPS</p>';
          $content .= '<p><b>Thông tin VPS:</b><br />';
          $content .= '- IP: ' . $vps->ip . '<br />';
          $content .= '- Tài khoản: '. $vps->user .'<br />';
          $content .= '- Mật khẩu: ' . $data['password'] . '<br />';
          $content .= '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
          $data_notification = [
              'name' => "Hoàn thành cài đặt lại hệ điều hành cho VPS " . $vps->ip,
              'content' => $content,
              'status' => 'Active',
          ];
          $infoNotification = $this->notification->create($data_notification);
          $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
          return true;
        }
        else {
            return false;
        }
    }

    public function update_reset_password($data)
    {
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if (isset($vps)) {
          $user = $vps->user_vps;
          $content = '<p>Xin chào <b>' . $user->name . ',</b> </p>' ;
          $content .= '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu đặt lại mật khẩu cho VPS</p>';
          $content .= '<p><b>Thông tin VPS:</b><br />';
          $content .= '- IP: ' . $vps->ip . '<br />';
          $content .= '- Tài khoản: '. $vps->user .'<br />';
          $content .= '- Mật khẩu: ' . $data['password'] . '<br />';
          $content .= '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
          $data_notification = [
              'name' => "Hoàn thành đặt lại mật khẩu cho VPS " . $vps->ip,
              'content' => $content,
              'status' => 'Active',
          ];
          $infoNotification = $this->notification->create($data_notification);
          $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
          return true;
        }
        else {
            return false;
        }
    }

    public function change_ip_vps($data)
    {
        $vps = $this->vps->where('vm_id', $data['vm_id'])->first();
        if (isset($vps)) {

            $user = $vps->user_vps;
            if ( $vps->location == 'cloudzone' ) {
                $iped = $data['old_ip'];
                $content = '<p>Xin chào <b>' . $vps->user_vps->name . ',</b> </p>' ;
                $content .= '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu thay đổi IP cho VPS VN</p>';
                $content .= '<p><b>Thông tin VPS:</b><br />';
                $content .= '- IP cũ: ' . $iped . '<br />';
                $content .= '- IP mới: ' . $data['ip'] . '<br />';
                $content .= '- Tài khoản: '. $vps->user .'<br />';
                $content .= '- Mật khẩu: ' . $data['password'] . '<br />';
                $content .= '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
                $data_notification = [
                    'name' => "Hoàn thành thay đổi IP VPS " . $iped . " sang " . $data['ip'],
                    'content' => $content,
                    'status' => 'Active',
                ];
                $infoNotification = $this->notification->create($data_notification);
                $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);

                $vps->status_vps = $data['status'];
                $vps->ip = $data['ip'];
                $vps->password = $data['password'];
                $vps->save();

                $this->send_mail_change_ip($iped, $vps);
                return true;
            }
            else {
                // Tạo thông báo cho user
                if ( $vps->vm_id < 1000000000 ) {
                    $iped = $data['old_ip'];

                    $content = '<p>Xin chào <b>' . $vps->user_vps->name . ',</b> </p>' ;
                    $content .= '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu thay đổi IP cho VPS US</p>';
                    $content .= '<p><b>Thông tin VPS:</b><br />';
                    $content .= '- IP cũ: ' . $iped . '<br />';
                    $content .= '- IP mới: ' . $data['ip'] . '<br />';
                    $content .= '- Tài khoản: '. $vps->user .'<br />';
                    $content .= '- Mật khẩu: ' . $data['password'] . '<br />';
                    $content .= '- Bang: ' . $data['state'] . '</p>';
                    $content .= '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
                    $data_notification = [
                        'name' => "Hoàn thành thay đổi IP VPS " . $iped . " sang " . $data['ip'],
                        'content' => $content,
                        'status' => 'Active',
                    ];
                    $infoNotification = $this->notification->create($data_notification);
                    $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);

                    $vps->status_vps = $data['status'];
                    $vps->ip = $data['ip'];
                    $vps->password = $data['password'];
                    $vps->state = $data['state'];
                    $vps->save();

                    $this->send_mail_change_ip($iped, $vps);
                    return true;
                } else {
                    $iped = $data['old_ip'];

                    $content = '<p>Xin chào <b>' . $vps->user_vps->name . ',</b> </p>' ;
                    $content .= '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu thay đổi IP cho VPS US</p>';
                    $content .= '<p><b>Thông tin VPS:</b><br />';
                    $content .= '- IP cũ: ' . $iped . '<br />';
                    $content .= '- IP mới: ' . $data['ip'] . '<br />';
                    $content .= '- Tài khoản: '. $vps->user .'<br />';
                    $content .= '- Mật khẩu: ' . $data['password'] . '<br />';
                    $content .= '- Bang: ' . $data['state'] . '</p>';
                    $content .= '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
                    $data_notification = [
                        'name' => "Hoàn thành thay đổi IP VPS " . $iped . " sang " . $data['ip'],
                        'content' => $content,
                        'status' => 'Active',
                    ];
                    $infoNotification = $this->notification->create($data_notification);
                    $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);

                    $vps->status_vps = $data['status'];
                    $vps->ip = $data['ip'];
                    $vps->password = $data['password'];
                    $vps->state = $data['state'];
                    $vps->save();

                    $this->send_mail_change_ip($iped, $vps);
                    return true;
                }
            }
        }
        else {
            return false;
        }
    }
//product - email - detail_order -user - order - user_send_email - subject
    public function send_mail_finish($vps)
    {
        $billings = config('billing');
        $detail_order = $this->detail_order->find($vps->detail_order_id);
        $order = $this->order->find($detail_order->order_id);
        $product = $this->product->find($vps->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $configOs = config('os');
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($vps->addon_id);
        }
        $user = $this->user->find($vps->user_id);
        $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
        $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$vps->billing_cycle], '', $order->created_at, $vps->user, $vps->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, !empty($configOs[$vps->os]) ? $configOs[$vps->os] : 'Windows Server 2012 R2' , '', $url];
        $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

        $data = [
            'content' => $content,
            'user_name' => $user->name,
            'subject' => $email->meta_email->subject,
        ];

        $data_tb_send_mail = [
          'type' => 'mail_finish_order_vps',
          'type_service' => 'vps',
          'user_id' => $user->id,
          'status' => false,
          'content' => serialize($data),
        ];
        $this->send_mail->create($data_tb_send_mail);

    }

    public function send_mail_rebuild($vps)
    {
        // $this->user_send_email = $vps->user_vps->email;
        $configOs = config('listOs');
        $data = [
          'ip' => $vps->ip,
          'os' => $vps->os,
          'security' => $vps->security,
          'subject' => 'Yêu cầu cài đặt lại VPS '. $vps->ip .' thành công'
        ];

        $data_tb_send_mail = [
          'type' => 'mail_finish_rebuild_vps',
          'type_service' => 'vps',
          'user_id' => $vps->user_id,
          'status' => false,
          'content' => serialize($data),
        ];
        $this->send_mail->create($data_tb_send_mail);

        // $mail = Mail::send('users.mails.rebuild_vps', $data, function($message){
        //     $message->from('support@cloudzone.vn', 'Cloudzone');
        //     $message->to($this->user_send_email)->subject('Yêu cầu cài đặt lại VPS thành công');
        // });
    }

    public function send_mail_change_ip($iped, $vps)
    {
        try {
            $user = $vps->user_vps;
            if ( $vps->location == 'cloudzone' ) {
                $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_change_ip_vn')->first();
                if ( isset($mail_system) ) {
                    $search_subject = [ '{$ip_old}' , '{$ip_new}' ];
                    $replace_subject = [ $iped, $vps->ip ];
                    $subject = str_replace($search_subject, $replace_subject, !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : '');

                    $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}',
                        '{$services_username}', '{$services_password}', '{$total}', '{$state}', '{$ip}', '{$ip_old}', '{$ip_new}' ];
                    $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0,
                        url(''), '', $vps->user, $vps->password, '20.000 VNĐ', '',  $vps->ip, $iped, $vps->ip];
                    $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                    $data = [
                      'content' => $content,
                      'subject' => $subject,
                    ];
                } else {
                    $data = [
                        'name' => $vps->user_vps->name,
                        'ip' => $vps->ip,
                        'iped' => $iped,
                        'user' => $vps->user,
                        'password' => $vps->password,
                        'subject' => 'Hoàn thành thay đổi IP cho VPS VN từ ' . $iped . ' sang ' . $vps->ip,
                    ];
                }
                $data_tb_send_mail = [
                    'type' => 'mail_order_finish_change_ip',
                    'type_service' => 'vps_vn',
                    'user_id' => $vps->user_vps->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } else {
                $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_change_ip')->first();
                if ( isset($mail_system) ) {
                  $search_subject = [ '{$ip_old}' , '{$ip_new}' ];
                  $replace_subject = [ $iped, $vps->ip ];
                  $subject = str_replace($search_subject, $replace_subject, !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : '');

                  $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}',
                    '{$services_username}', '{$services_password}', '{$total}', '{$state}', '{$ip}', '{$ip_old}', '{$ip_new}' ];
                  $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0,
                    url(''), '', $vps->user, $vps->password, '20.000 VNĐ', $vps->state,  $vps->ip, $iped, $vps->ip];
                  $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                  $data = [
                    'content' => $content,
                    'subject' => $subject,
                  ];
                } else {
                  $data = [
                    'name' => $vps->user_vps->name,
                    'ip' => $vps->ip,
                    'iped' => $iped,
                    'user' => $vps->user,
                    'password' => $vps->password,
                    'state' => $vps->state,
                    'subject' => 'Hoàn thành thay đổi IP cho VPS US từ ' . $iped . ' sang ' . $vps->ip,
                  ];
                }
                $data_tb_send_mail = [
                    'type' => 'mail_order_finish_change_ip',
                    'type_service' => 'vps_us',
                    'user_id' => $vps->user_vps->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            }
        } catch (\Exception $e) {
            report($e);
        }

    }


    public function addVPS($data)
    {
        $os = $data['os'];
        if (empty($data['security'])) {
            if ($os == 1) {
                $os = 11;
            }
            elseif ($os == 2) {
                $os = 12;
            }
            elseif ($os == 3) {
                $os = 13;
            }
            elseif ($os == 4) {
                $os = 14;
            }
        }
        $product = $this->product->find($data['product_id']);
        if (!empty($data['price_override'])) {
            $total = $data['total'];
        } else {
            $total = $product->pricing[$data['billing_cycle']];
        }
        if ( !empty($data['addon_cpu']) || !empty($data['addon_ram']) || !empty($data['addon_disk']) ) {
            $add_on_products = $this->get_addon_product_private($data['user_id']);
            $pricing_addon = 0;
            foreach ($add_on_products as $key => $add_on_product) {
                if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $data['addon_cpu'] * $add_on_product->pricing[$data['billing_cycle']];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $data['addon_ram'] * $add_on_product->pricing[$data['billing_cycle']];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $data['addon_disk'] * $add_on_product->pricing[$data['billing_cycle']] / 10;
                    }
                }
            }
            $total += $pricing_addon;
        }

        // Tính ngày kết thúc cho VPS
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $date_create = $data['date_create'];
        $billing_cycle = $data['billing_cycle'];
        $next_due_date = date('Y-m-d', strtotime( $date_create . $billing[$billing_cycle] ));

        $data_order = [
            'user_id' => $data['user_id'],
            'total' => $total,
            'status' => 'Finish',
        ];
        $order = $this->order->create($data_order);

        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'VPS',
            'due_date' => $next_due_date,
            'status' => 'paid',
            'paid_date' => date('Y-m-d'),
            'paid_date' => date('Y-m-d'),
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => $data['user_id'],
            'addon_id' => '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find($data['user_id']);

        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Active',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);

        $data_vps = [
            'detail_order_id' => $detail_order->id,
            'user_id' =>  $data['user_id'],
            'product_id' => $data['product_id'],
            'addon_id' => '0',
            'ip' => $data['ip'],
            'os' => $os,
            'billing_cycle' => $data['billing_cycle'],
            'next_due_date' => $next_due_date,
            'date_create' => date('Y-m-d', strtotime($data['created_at'])),
            'vm_id' => $data['vm_id'],
            'status_vps' => 'on',
            'paid' => 'paid',
            'status' => 'Active',
            'user' => !empty($data['sevices_username'])? $data['sevices_username'] : '',
            'password' => !empty($data['sevices_password'])? $data['sevices_password'] : '',
            'addon_qtt' => '0',
            'security' => !empty($data['security']) ? true : false,
            'price_override' => !empty($data['price_override']) ? $data['total'] : '0',
            'expire_billing_cycle' => !empty($data['expire_billing_cycle']) ? $data['expire_billing_cycle'] : 0,
        ];
        $create_vps = $this->vps->create($data_vps);
        $config_billing_DashBoard = config('billingDashBoard');
        $data_update_leased_time = [
          "mob" => "update_leased_time",
          "name" => $create_vps->vm_id,
          "created_date" => date('Y-m-d', strtotime($data['date_create'])),
          "leased_time" => !empty($config_billing_DashBoard[$billing_cycle]) ? $config_billing_DashBoard[$billing_cycle] : 1,
        ];
        $reques_vcenter = $this->dashboard->update_leased_time($create_vps->vm_id, $data_update_leased_time, $create_vps->ip);

        // ghi log
        $text_vps = '#' . $create_vps->id;
        if (!empty($create_vps->ip)) {
           $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $create_vps->id  . '">' . $create_vps->ip . '</a>';
        }
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'thêm',
          'model' => 'Admin/VPS',
          'description' => ' VPS ' . $text_vps,
          'service' =>  $create_vps->ip,
        ];
        $this->log_activity->create($data_log);

        if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
            $data_vps_config = [
                'vps_id' => $create_vps->id,
                'ip' => '0',
                'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
            ];
            $this->vps_config->create($data_vps_config);
        }

        if (!empty($data['sub_value'])) {
            $user_credit = $user->credit;
            $user_credit->value = $user_credit->value - $total;
            $user_credit->save();
        }

        if (!empty($this->total_price->first())) {
            $total_price = $this->total_price->first();
            $total_price->vps += $total;
            $total_price->save();
        } else {
            $data_total_price = [
                'vps' => $total,
                'hosting' => '0',
                'server' => '0',
            ];
            $this->total_price->create($data_total_price);
        }

        return $create_vps;

    }

    public function get_product_vps_with_user($customer_id)
    {
        $user = $this->user->where('customer_id', $customer_id)->first();
        if ( !empty($user) ) {
            $group_products = $this->get_group_product_private($user);
            $products = [];
            foreach ($group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                   if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS'  || $product->type_product == 'VPS-US') {
                     $data = [
                       'id' => $product->id,
                       'name' => $product->name,
                       'cpu' => !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0,
                       'ram' => !empty($product->meta_product->memory) ? $product->meta_product->memory : 0,
                       'disk' => !empty($product->meta_product->disk) ? $product->meta_product->disk : 0,
                       'monthly' => !empty($product->pricing->monthly) ? $product->pricing->monthly : 0,
                     ];
                     $products[] = $data;
                   }
                }
            }
            return $products;
        } else {
          return false;
        }
    }

    // get meta group product
    public function get_group_product_private($user)
    {
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
        }

        return $group_products;

    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    public function search($q, $status, $sort, $qtt)
    {
        $data = [];
        $data['data'] = [];
        $billings = config('billing');
        $qtt = !empty($qtt) ? $qtt : 30;
        if ( empty($status) ) {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('location', 'cloudzone')
                ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
                ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('location', 'cloudzone')
                ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
                ->where('next_due_date' , '!=', null)
                ->orderByRaw('next_due_date ' . $sort)
                ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                ->paginate($qtt);
          }
            // dd('da den 1');
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->where('o.location', 'cloudzone')
              ->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        else {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->where('next_due_date' , '!=', null)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])->paginate($qtt);
          }
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->select(['o.*'])->where('o.status_vps', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        return $data;
    }

    public function search_multi_vps_vn($ips)
    {
        $data = [];
        $data['data'] = [];
        $list_vps = [];
        $billings = config('billing');
        foreach ($ips as $key => $ip) {
            $search_vps = $this->vps
                  ->where('ip', trim($ip))
                  ->where('location', 'cloudzone')
                  ->orderBy('id', 'desc')
                  ->with('product', 'user_vps')->get();
            if ( $search_vps->count() > 0 ) {
                foreach ($search_vps as $key => $f_vps) {
                    $list_vps[] = $f_vps;
                }
            }
        }

          if (count($list_vps) > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
        return $data;
    }

    public function search_multi_vps_us($ips)
    {
        $data = [];
        $data['data'] = [];
        $billings = config('billing');
        $list_vps = [];
        foreach ($ips as $key => $ip) {
            $search_vps = $this->vps
                  ->where('ip', trim($ip))
                  ->where('location', 'us')
                  ->orderBy('id', 'desc')
                  ->with('product', 'user_vps')->get();
            if ( $search_vps->count() > 0 ) {
                foreach ($search_vps as $key => $f_vps) {
                    $list_vps[] = $f_vps;
                }
            }
        }
        if (count($list_vps) > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
        return $data;
    }

    public function search_vps_us($q, $status, $qtt)
    {
        $data = [];
        $data['data'] = [];
        $qtt = !empty($qtt) ? $qtt : 30;
        $billings = config('billing');
        if ( empty($status) ) {
          $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('location', 'us')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('id', 'desc')->paginate($qtt);
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->where('o.location', 'us')->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        else {
          $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('id', 'desc')->paginate($qtt);
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->select(['o.*'])->where('o.status_vps', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        return $data;
    }

    public function select_status_vps($q, $status, $sort, $qtt)
    {
        $data = [];
        $data['data'] = [];
        $billings = config('billing');
        $qtt = !empty($qtt) ? $qtt : 30;
        if ( empty( $q ) ) {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('status_vps', $status)->where('location', 'cloudzone')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('status_vps', $status)->where('location', 'cloudzone')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->where('next_due_date' , '!=', null)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
              ->paginate($qtt);
          }
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        else {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('status_vps', $status)->where('location', 'cloudzone')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)->where('location', 'cloudzone')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->where('next_due_date' , '!=', null)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])->paginate($qtt);
          }
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->select(['o.*'])->where('o.status_vps', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        return $data;
    }

    public function select_status_vps_us($q, $status, $sort, $qtt)
    {
        $data = [];
        $data['data'] = [];
        $qtt = !empty($qtt) ? $qtt : 30;
        $billings = config('billing');
        if ( empty( $q ) ) {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('status_vps', $status)->where('location', 'us')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('status_vps', $status)->where('location', 'us')
              ->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])->paginate($qtt);
          }
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        else {
          if ( empty($sort) ) {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)
              ->where('location', 'us')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->orderBy('id', 'desc')->paginate($qtt);
          }
          else {
              $list_vps = $this->vps->where('ip', 'like', '%'.$q.'%')->where('status_vps', $status)
              ->where('location', 'us')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')
              ->where('next_due_date' , '!=', null)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])->paginate($qtt);
          }
          if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                  $product = $vps->product;
                  if (!empty($product->meta_product)) {
                      $cpu = $product->meta_product->cpu;
                      $ram = $product->meta_product->memory;
                      $disk = $product->meta_product->disk;
                      if ($vps->vps_config) {
                         $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                         $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                         $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                      }
                      $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  }
                  if (!empty($vps->price_override)) {
                      $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->total_vps = $sub_total;
                  // tổng thời gian
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    if ( $month ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    $total_time = $diff_month . ' Tháng';
                  }
                  $vps->total_time = $total_time;
                  // thời gian thuê
                  $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                  $data['data'][] = $vps;
              }
          }
          else {
            $list_vps =  $this->vps->from('vps as o')->select(['o.*'])->where('o.status_vps', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_vps', 'product', 'detail_order', 'order_addon_vps')->orderBy('o.id', 'desc')->paginate($qtt);
            if ($list_vps->count() > 0) {
              foreach ($list_vps as $key => $vps) {
                $product = $vps->product;
                if (!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                  if ($vps->vps_config) {
                    $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                    $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                    $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                  }
                  $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                }
                if (!empty($vps->price_override)) {
                  $sub_total = $vps->price_override;
                } else {
                  $product = $vps->product;
                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $sub_total += $pricing_addon;
                  }
                }
                $vps->total_vps = $sub_total;
                // tổng thời gian
                $total_time = '';
                $create_date = new Carbon($vps->date_create);
                $next_due_date = new Carbon($vps->next_due_date);
                if ( $next_due_date->diffInYears($create_date) ) {
                  $year = $next_due_date->diffInYears($create_date);
                  $total_time = $year . ' Năm ';
                  $create_date = $create_date->addYears($year);
                  $month = $next_due_date->diffInMonths($create_date);
                  if ( $month ) {
                    $total_time .= $month . ' Tháng';
                  }
                } else {
                  $diff_month = $next_due_date->diffInMonths($create_date);
                  $total_time = $diff_month . ' Tháng';
                }
                $vps->total_time = $total_time;
                // thời gian thuê
                $vps->text_billing_cycle = $billings[$vps->billing_cycle];
                $data['data'][] = $vps;
              }
            }
          }
          $data['total'] = $list_vps->total();
          $data['perPage'] = $list_vps->perPage();
          $data['current_page'] = $list_vps->currentPage();
          $data['firstItem'] = $list_vps->firstItem();
          $data['lastItem'] = $list_vps->lastItem();
        }
        return $data;
    }

    public function search_user($q)
    {
        $list_users = $this->user->where('name', 'like', '%'.$q.'%')->with('vps')->get();
        $list_vps =  [];
        $sub_total = 0;
        $billings = config('billing');
        foreach ($list_users as $key => $user) {
          if (!empty($user->vps)) {
            foreach ($user->vps as $key => $vps) {

              $product = $vps->product;
              if (!empty($product->meta_product)) {
                $cpu = $product->meta_product->cpu;
                $ram = $product->meta_product->memory;
                $disk = $product->meta_product->disk;
                if ($vps->vps_config) {
                  $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                  $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                  $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
                }
                $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
              }

              if (!empty($vps->price_override)) {
                $sub_total = $vps->price_override;
                $order_addon_vps = $vps->order_addon_vps;
                if (!empty($order_addon_vps)) {
                  foreach ($order_addon_vps as $key => $addon_vps) {
                    $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                  }
                }
                $vps->total_vps = $sub_total;
              } else {
                $sub_total = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
                $order_addon_vps = $vps->order_addon_vps;
                if (!empty($order_addon_vps)) {
                  foreach ($order_addon_vps as $key => $addon_vps) {
                    $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                  }
                }
                $vps->total_vps = $sub_total;
              }

              $list_vps[$key] = $vps;
              $list_vps[$key]['user_vps']['id'] = $user->id;
            }
          }
        }
        return $list_vps;

    }

    public function search_user_form($q)
    {
        $list_vps = $this->vps->from('vps as o')->where('o.location', 'cloudzone')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.name', 'like' , '%'.$q.'%')->with('user_vps', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        return $list_vps;
    }

    public function list_vps_of_personal()
    {
        $list_vps = $this->vps->from('vps as o')->where('o.location', 'cloudzone')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user_vps', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        $billings = config('billing');
        if ( $list_vps->count() > 0 ) {
          foreach ($list_vps as $key => $vps) {

            $product = $vps->product;
            if (!empty($product->meta_product)) {
              $cpu = $product->meta_product->cpu;
              $ram = $product->meta_product->memory;
              $disk = $product->meta_product->disk;
              if ($vps->vps_config) {
                $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
              }
              $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }

            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
            } else {
              $sub_total = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
              // tổng thời gian
              $total_time = '';
              $create_date = new Carbon($vps->date_create);
              $next_due_date = new Carbon($vps->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                if ( $month ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                $total_time = $diff_month . ' Tháng';
              }
              $vps->total_time = $total_time;
              // thời gian thuê
              $vps->text_billing_cycle = $billings[$vps->billing_cycle];
            }
            $data['data'][] = $vps;
          }
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;

    }

    public function list_vps_of_enterprise()
    {
        $list_vps = $this->vps->from('vps as o')->where('o.location', 'cloudzone')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user_vps', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        $billings = config('billing');
        if ( $list_vps->count() > 0 ) {
          foreach ($list_vps as $key => $vps) {

            $product = $vps->product;
            if (!empty($product->meta_product)) {
              $cpu = $product->meta_product->cpu;
              $ram = $product->meta_product->memory;
              $disk = $product->meta_product->disk;
              if ($vps->vps_config) {
                $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
              }
              $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }

            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
            } else {
              $sub_total = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
              // tổng thời gian
              $total_time = '';
              $create_date = new Carbon($vps->date_create);
              $next_due_date = new Carbon($vps->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                if ( $month ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                $total_time = $diff_month . ' Tháng';
              }
              $vps->total_time = $total_time;
              // thời gian thuê
              $vps->text_billing_cycle = $billings[$vps->billing_cycle];
            }
            $data['data'][] = $vps;
          }
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;

    }

    public function list_vps_us_of_personal()
    {
        $list_vps = $this->vps->from('vps as o')->where('o.location', 'us')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user_vps', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        $billings = config('billing');
        if ( $list_vps->count() > 0 ) {
          foreach ($list_vps as $key => $vps) {

            $product = $vps->product;
            if (!empty($product->meta_product)) {
              $cpu = $product->meta_product->cpu;
              $ram = $product->meta_product->memory;
              $disk = $product->meta_product->disk;
              if ($vps->vps_config) {
                $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
              }
              $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }

            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
            } else {
              $sub_total = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
              // tổng thời gian
              $total_time = '';
              $create_date = new Carbon($vps->date_create);
              $next_due_date = new Carbon($vps->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                if ( $month ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                $total_time = $diff_month . ' Tháng';
              }
              $vps->total_time = $total_time;
              // thời gian thuê
              $vps->text_billing_cycle = $billings[$vps->billing_cycle];
            }
            $data['data'][] = $vps;
          }
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;

    }

    public function list_vps_us_of_enterprise()
    {
        $list_vps = $this->vps->from('vps as o')->where('o.location', 'us')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user_vps', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        $billings = config('billing');
        if ( $list_vps->count() > 0 ) {
          foreach ($list_vps as $key => $vps) {

            $product = $vps->product;
            if (!empty($product->meta_product)) {
              $cpu = $product->meta_product->cpu;
              $ram = $product->meta_product->memory;
              $disk = $product->meta_product->disk;
              if ($vps->vps_config) {
                $cpu += !empty($vps->vps_config->cpu) ? $vps->vps_config->cpu : 0;
                $ram += !empty($vps->vps_config->ram) ? $vps->vps_config->ram : 0;
                $disk += !empty($vps->vps_config->disk) ? $vps->vps_config->disk : 0;
              }
              $vps->config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            }

            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
            } else {
              $sub_total = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                  $sub_total += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
              $vps->total_vps = $sub_total;
              // tổng thời gian
              $total_time = '';
              $create_date = new Carbon($vps->date_create);
              $next_due_date = new Carbon($vps->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                if ( $month ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                $total_time = $diff_month . ' Tháng';
              }
              $vps->total_time = $total_time;
              // thời gian thuê
              $vps->text_billing_cycle = $billings[$vps->billing_cycle];
            }
            $data['data'][] = $vps;
          }
        }
        $data['total'] = $list_vps->total();
        $data['perPage'] = $list_vps->perPage();
        $data['current_page'] = $list_vps->currentPage();
        return $data;

    }

    public function log_payment_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $log_payments = $this->log_activity->where('service', $vps->ip)->where('user_id', $vps->user_id)
                        ->where('action', 'thanh toán')
                        ->orderBy('id', 'desc')->get();
        return $log_payments;
    }

    public function vps_console($id)
    {
        $vps =  $this->vps->where('id', $id)->first();
        if ( isset($vps) ) {
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'truy cập console',
              'model' => 'Admin/VPSs_VPS',
              'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
              'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
            $data = $this->dashboard->vps_console($vps->vm_id);
            $data['ip'] = $vps->ip;
            return $data;
        } else {
           return false;
        }
    }

    public function export_excel($list_id_vps, $dataRequest)
    {
      $dataResponse = [];
      $head = [];
      if ( !empty($dataRequest['ip']) ) {
        $head[] = 'IP';
      }
      if ( !empty($dataRequest['state']) ) {
        $head[] = 'Bang';
      }
      if ( !empty($dataRequest['config']) ) {
        $head[] = 'Cấu hình';
      }
      if ( !empty($dataRequest['due_date']) ) {
        $head[] = 'Ngày kết thúc';
      }
      if ( !empty($dataRequest['password']) ) {
        $head[] = 'Mật khẩu';
      }
      $dataResponse[] = $head;
      foreach ($list_id_vps as $key => $id) {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
          $body = [];
          if ( !empty($dataRequest['ip']) ) {
            $body[] = $vps->ip;
          }
          if ( !empty($dataRequest['state']) ) {
            $body[] = $vps->state;
          }
          if ( !empty($dataRequest['config']) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $body[] = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          }
          if ( !empty($dataRequest['due_date']) ) {
            $body[] = date('d-m-Y', strtotime($vps->next_due_date));
          }
          if ( !empty($dataRequest['password']) ) {
            $body[] = $vps->password;
          }
          $dataResponse[] = $body;
        }
      }
      return $dataResponse;
    }

    public function log_activities_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $log_activities = $this->log_activity->where('service', $vps->ip)
                        ->whereIn( 'action', [ 'tạo', 'tắt', 'bật dịch vụ', 'khởi động lại', 'truy cập console',
                            'gia hạn', 'cấu hình thêm', 'đổi IP', 'hủy dịch vụ', 'đặt lại mật khẩu', 'yêu cầu' ] )
                        ->get();
        return $log_activities;
    }

    public function add_vps_with_dashboard($data_dashboard)
    {
         if ( is_array($data_dashboard) ) {
           foreach ($data_dashboard as $key => $data) {
               if ( $data['type_vps'] == 'vps' || $data['type_vps'] == 'nat' ) {
                    // validete
                    if ( empty($data['customer_id']) ) {
                         $data = [
                           "error" => 5,
                           "status" => is_array($data) . ' - ' . is_string($data) . ' - ' . $data,
                         ];
                         return $data;
                    }
                    try {
                         $customer_id = (int) $data['customer_id'];


                         $user = $this->user->where("customer_id", $customer_id)->first();
                    }
                    catch (\Exception $e) {
                         $data = [
                           "error" => 5,
                           "status" => "Không có customer_id"
                         ];
                         return $data;
                    }
                    // kiểm tra vps
                    // try {
                    //     $list_check_vps = $this->vps->where( 'vm_id' , $data['vm_id'])->where( 'ip' , $data['ip'])
                    //         ->where('location', 'cloudzone') ->get();
                    //     $check_vps = true;
                    //     foreach ($list_check_vps as $key => $lc_vps) {
                    //         if ( $lc_vps->status_vps != 'delete_vps' && $lc_vps->status_vps != 'change_user' ) {
                    //                 $data = [
                    //                    "error" => 5,
                    //                    "status" => "VMID và IP đã có trên Portal"
                    //                 ];
                    //                 return $data;
                    //         }
                    //     }
                    // } catch (Exception $e) {
                    //     report($e);
                    //     $data = [
                    //         "error" => 5,
                    //         "status" => "VMID và IP đã có trên Portal"
                    //     ];
                    //     return $data;
                    // }
                   // lay san phảm
                   $product = $this->product->find($data['product_id']);
                   if ( !empty($data['override']) ) {
                      $total = $data['total'];
                   } else {
                      $total = $product->pricing[$data['billing_cycle']];
                   }
                   $add_on = $data['add_on'];
                   if ( !empty($add_on) ) {
                       $cpu = !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0;
                       $ram = !empty($data['addon_ram']) ? $data['addon_ram'] : 0;
                       $disk =  !empty($data['addon_disk']) ? $data['addon_disk'] : 0;
                       $add_on_products = $this->get_addon_product_private($user->id);
                       $pricing_addon = 0;
                       foreach ($add_on_products as $key => $add_on_product) {
                         if (!empty($add_on_product->meta_product->type_addon)) {
                           if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                             $pricing_addon += $cpu * $add_on_product->pricing['monthly'];
                           }
                           if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                             $pricing_addon += $ram * $add_on_product->pricing['monthly'];
                           }
                           if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                             $pricing_addon += $disk * $add_on_product->pricing['monthly'] / 10;
                           }
                         }
                       }
                       $total += $pricing_addon;
                   }
                   // tao order
                   $data_order = [
                       'user_id' => $user->id,
                       'total' => $total,
                       'status' => 'Finish',
                       'description' => 'Add VPS form DashBoard',
                       'verify' => true,
                   ];
                   $create_order = $this->order->create($data_order);
                   // ket thuc tao order
                   // tao order detail
                   $data_order_detail = [
                       'order_id' => $create_order->id,
                       'type' => 'VPS',
                       'due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                       'description' => 'Add VPS form DashBoard',
                       'payment_method' => 1,
                       'status' => 'paid',
                       'sub_total' => $total,
                       'quantity' => 1,
                       'user_id' => $user->id,
                       'paid_date' => date('Y-m-d', strtotime($data['date_create'])),
                       'addon_id' => '0',
                   ];
                   $detail_order = $this->detail_order->create($data_order_detail);
                   // Tạo history pay
                   $data_history = [
                       'user_id' => $user->id,
                       'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
                       'discription' => 'Hóa đơn số ' . $detail_order->id,
                       'type_gd' => '2',
                       'method_gd' => 'invoice',
                       'date_gd' => date('Y-m-d', strtotime($data['date_create'])),
                       'money'=> $total,
                       'status' => 'Active',
                       'method_gd_invoice' => 'credit',
                       'detail_order_id' => $detail_order->id,
                   ];
                   $this->history_pay->create($data_history);
                   $data_vps = [
                       'detail_order_id' => $detail_order->id,
                       'user_id' => $user->id,
                       'product_id' => $data['product_id'],
                       'next_due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                       'ip' => !empty($data['ip'])? $data['ip'] : '',
                       'os' => $data['os'],
                       'billing_cycle' => !empty($data['billing_cycle']) ? $data['billing_cycle'] : 'monthly',
                       'status' => 'Active',
                       'user' => !empty($data['user_vps'])? $data['user_vps'] : '',
                       'password' => !empty($data['password_vps'])? $data['password_vps'] : '',
                       'paid' => 'paid',
                       'addon_id' => '0',
                       'date_create' => date('Y-m-d', strtotime($data['date_create'])),
                       'status_vps' => !empty($data['status_vps']) ? $data['status_vps'] : 'on',
                       'description_ip' => !empty($data['description']) ? $data['description'] : null,
                       'vm_id' => $data['vm_id'],
                       'security' => !empty($data['security']) ? true : false,
                       'price_override' => !empty($data['override']) ? $total : '',
                       'type_vps' => ($data['type_vps'] == 'nat') ? 'nat_vps' : 'vps',
                   ];
                   $create_vps = $this->vps->create($data_vps);
                   if ( !empty($add_on) ) {
                       $data_vps_config = [
                           'vps_id' => $create_vps->id,
                           'ip' => '0',
                           'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                           'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                           'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                       ];
                       $this->vps_config->create($data_vps_config);
                   }
                   // ghi log
                   $text_vps = '#' . $create_vps->id;
                   if (!empty($create_vps->ip)) {
                      $text_vps .= ' Ip: <a target="_blank" href="/admin/vps/detail/' . $create_vps->id  . '">' . $create_vps->ip . '</a>';
                   }
                   $data_log = [
                     'user_id' => 999998,
                     'action' => 'thêm',
                     'model' => 'Admin/VPS',
                     'description' => ' VPS ' . $text_vps . ' từ dashboard',
                     'service' =>  $create_vps->ip,
                   ];
                   $this->log_activity->create($data_log);
               }
               else {
                 if ( empty($data['customer_id']) ) {
                   $data = [
                     "error" => 5,
                     "status" => is_array($data) . ' - ' . is_string($data) . ' - ' . $data,
                   ];
                   return $data;
                 }
                 try {
                   $customer_id = (int) $data['customer_id'];
                   $user = $this->user->where("customer_id", $customer_id)->first();
                 }
                 catch (\Exception $e) {
                   $data = [
                     "error" => 5,
                     "status" => "Không có customer_id"
                   ];
                 }

                 $product = $this->product->find($data['product_id']);
                 if ( !empty($data['override']) ) {
                    $total = $data['total'];
                 } else {
                    $total = $product->pricing[$data['billing_cycle']];
                 }
                 // tao order
                 $data_order = [
                     'user_id' => $user->id,
                     'total' => $total,
                     'status' => 'Finish',
                     'description' => 'Add VPS US form DashBoard',
                     'verify' => true,
                 ];
                 $create_order = $this->order->create($data_order);
                 // ket thuc tao order
                 // tao order detail
                 $data_order_detail = [
                     'order_id' => $create_order->id,
                     'type' => 'VPS-US',
                     'due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                     'description' => 'Add VPS US form DashBoard',
                     'payment_method' => 1,
                     'status' => 'paid',
                     'sub_total' => $total,
                     'quantity' => 1,
                     'user_id' => $user->id,
                     'paid_date' => date('Y-m-d', strtotime($data['date_create'])),
                     'addon_id' => '0',
                 ];
                 $detail_order = $this->detail_order->create($data_order_detail);
                 // Tạo history pay
                 $data_history = [
                     'user_id' => $user->id,
                     'ma_gd' => 'GDOVU' . strtoupper(substr(sha1(time()), 34, 39)),
                     'discription' => 'Hóa đơn số ' . $detail_order->id,
                     'type_gd' => '2',
                     'method_gd' => 'invoice',
                     'date_gd' => date('Y-m-d', strtotime($data['date_create'])),
                     'money'=> $total,
                     'status' => 'Active',
                     'method_gd_invoice' => 'credit',
                     'detail_order_id' => $detail_order->id,
                 ];
                 $this->history_pay->create($data_history);
                 $data_vps = [
                     'detail_order_id' => $detail_order->id,
                     'user_id' => $user->id,
                     'product_id' => $data['product_id'],
                     'next_due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                     'ip' => !empty($data['ip'])? $data['ip'] : '',
                     'os' => $data['os'],
                     'billing_cycle' => !empty($data['billing_cycle']) ? $data['billing_cycle'] : 'monthly',
                     'status' => 'Active',
                     'user' => !empty($data['user_vps'])? $data['user_vps'] : '',
                     'password' => !empty($data['password_vps'])? $data['password_vps'] : '',
                     'paid' => 'paid',
                     'addon_id' => '0',
                     'date_create' => date('Y-m-d', strtotime($data['date_create'])),
                     'status_vps' => !empty($data['status_vps']) ? $data['status_vps'] : 'on',
                     'vm_id' => $data['vm_id'],
                     'security' => !empty($data['security']) ? true : false,
                     'price_override' => !empty($data['override']) ? $total : '',
                     'type_vps' => ($data['type_vps'] == 'nat') ? 'nat_vps' : 'vps',
                     'location' => 'us',
                     'state' => !empty($data['state']) ? $data['state'] : 'VPS US',
                 ];
                 $create_vps = $this->vps->create($data_vps);
                 if ( !empty($add_on) ) {
                     $data_vps_config = [
                         'vps_id' => $create_vps->id,
                         'ip' => '0',
                         'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                         'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                         'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                     ];
                     $this->vps_config->create($data_vps_config);
                 }
                 // ghi log
                 $text_vps = '#' . $create_vps->id;
                 if (!empty($create_vps->ip)) {
                    $text_vps .= ' Ip: <a target="_blank" href="/admin/vps_us/detail/' . $create_vps->id  . '">' . $create_vps->ip . '</a>';
                 }
                 $data_log = [
                   'user_id' => 999998,
                   'action' => 'thêm',
                   'model' => 'Admin/VPS_US',
                   'description' => ' VPS US ' . $text_vps . ' từ dashboard',
                   'service' =>  $create_vps->ip,
                 ];
                 $this->log_activity->create($data_log);
               }
           }

           return true;
         }
         else {
           $data = [
             "error" => 6,
             "status" => 'Lỗi dữ liệu không phải là mảng',
           ];
           return $data;
         }
    }
    // API ADD VPS (POST)
    // https://portal.cloudzone.vn/api/add_vps
    // [
    //   {
    //     "customer_id": 1606, // customer id
    //     "product_id": 1, // ID của sản phẩm
    //     "override": false, // Giá áp tay (true hoặc false
    //     "total": 0,  // Nếu có giá ap tay thì để giá
    //     "date_create": "2020/01/20", // Ngày tạo VPS (định dạng: năm/tháng/ngày)
    //     "next_due_date": "2020/02/20", // Ngày kết thúc VPS (định dạng: năm/tháng/ngày)
    //     "ip": "192.168.1.6", // IP của VPS
    //     "os": 1, // Mặc định là 1 ( Windows 7 64bit )
    //     "billing_cycle": "monthly", // Thời gian thuê VPS
    //     "user_vps": "admin", // User VPS
    //     "password_vps": "123123", // Password VPS
    //     "security": 1, // Bảo mật true hoặc false
    //     "type_vps": "vps", // Loại VPS ( vps hoặc nat  hoặc vps us)
    //     "vm_id": 1606, // VMID
    //     "status_vps": "on" // Trạng thái VPS (mặc định là on)
    //      "add_on": false // có addon hay không
    //      "addon_cpu": 0 // addon cpu
    //      "addon_ram": 0 // addon ram
    //      "addon_disk": 0 // addon disk
    //      "state": "new york" // bang (mặc định sẽ về VPS US)
    //   }
    // ]
    public function add_proxy_with_dashboard($data_dashboard)
    {
         if ( is_array($data_dashboard) ) {
           foreach ($data_dashboard as $key => $data) {
               if ( empty($data['customer_id']) ) {
                    $data = [
                      "error" => 5,
                      "status" => is_array($data) . ' - ' . is_string($data) . ' - ' . $data,
                    ];
                    return $data;
               }
               try {
                    $customer_id = (int) $data['customer_id'];
                    $user = $this->user->where("customer_id", $customer_id)->first();
               }
               catch (\Exception $e) {
                    $data = [
                      "error" => 5,
                      "status" => "Không có customer_id"
                    ];
                    return $data;
               }


              $product = $this->get_group_product_proxy($user->id);
              if ( !isset($product) ) {
                $data = [
                  "error" => 6,
                  "status" => "Không có sản phẩm trong user này"
                ];
              }
              if ( !empty($data['override']) ) {
                 $total = $data['total'];
              } else {
                 $total = $product->pricing[$data['billing_cycle']];
              }
              // tao order
              $data_order = [
                  'user_id' => $user->id,
                  'total' => $total,
                  'status' => 'Finish',
                  'description' => 'Add Proxy form DashBoard',
                  'verify' => true,
              ];
              $create_order = $this->order->create($data_order);
              // ket thuc tao order
              // tao order detail
              $data_order_detail = [
                  'order_id' => $create_order->id,
                  'type' => 'Proxy',
                  'due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                  'description' => 'Add Proxy form DashBoard',
                  'payment_method' => 1,
                  'status' => 'paid',
                  'sub_total' => $total,
                  'quantity' => 1,
                  'user_id' => $user->id,
                  'paid_date' => date('Y-m-d', strtotime($data['date_create'])),
                  'addon_id' => '0',
              ];
              $detail_order = $this->detail_order->create($data_order_detail);
              // Tạo history pay
              $data_history = [
                  'user_id' => $user->id,
                  'ma_gd' => 'GDODP' . strtoupper(substr(sha1(time()), 34, 39)),
                  'discription' => 'Hóa đơn số ' . $detail_order->id,
                  'type_gd' => '2',
                  'method_gd' => 'invoice',
                  'date_gd' => date('Y-m-d', strtotime($data['date_create'])),
                  'money'=> $total,
                  'status' => 'Active',
                  'method_gd_invoice' => 'credit',
                  'detail_order_id' => $detail_order->id,
              ];
              $this->history_pay->create($data_history);
              $data_proxy = [
                  'detail_order_id' => $detail_order->id,
                  'user_id' => $user->id,
                  'product_id' => $product->id,
                  'next_due_date' => date('Y-m-d', strtotime($data['next_due_date'])),
                  'ip' => !empty($data['ip'])? $data['ip'] : '',
                  'port' => $data['port'],
                  'billing_cycle' => !empty($data['billing_cycle']) ? $data['billing_cycle'] : 'monthly',
                  'status' => $data['status'],
                  'username' => !empty($data['user_proxy'])? $data['user_proxy'] : '',
                  'password' => !empty($data['password_proxy'])? $data['password_proxy'] : '',
                  'date_create' => date('Y-m-d', strtotime($data['date_create'])),
                  'proxy_id' => $data['proxy_id'],
                  'location' => 'us',
                  'price_override' => !empty($data['override']) ? $total : 0,
                  'state' => !empty( $data['state'] ) ? $data['state'] : 'Random',
              ];
              $create_proxy = $this->proxy->create($data_proxy);
              // ghi log
              $text_vps = '#' . $create_proxy->id;
              if (!empty($create_proxy->ip)) {
                 $text_vps .= ' Ip: <a target="_blank" href="/admin/proxy/detail/' . $create_proxy->id  . '">' . $create_proxy->ip . '</a>';
              }
              $data_log = [
                'user_id' => 999998,
                'action' => 'thêm',
                'model' => 'Admin/Proxy',
                'description' => ' Proxy ' . $text_vps . ' từ dashboard',
                'service' =>  $create_proxy->ip,
              ];
              $this->log_activity->create($data_log);
           }

           return true;
         }
         else {
           $data = [
             "error" => 6,
             "status" => 'Lỗi dữ liệu không phải là mảng',
           ];
           return $data;
         }
    }
    // get meta group product server
    public function get_group_product_proxy($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_product = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'proxy')->where('hidden', false)->first();
        } else {
            $group_product = $this->group_product->where('group_user_id', 0)->where('type', 'proxy')->where('hidden', false)->where('private', 0)->first();
            // dd($group_products);
        }
        $response = [];
        foreach ($group_product->products as $key => $product) {
          $response = $product;
        }
        return $response;
    }

}
