<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\UserMeta;

class LoginRepositories {

    protected $user;
    protected $user_meta;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
    }

    public function detail($id)
    {
        return $this->user->with('user_meta')->find($id);
    }

    public function check_login_at_admin($admin_id , $admin_email, $admin_passwd)
    {
        $user = $this->user->find($admin_id);
        $check = false;
        // dd($user->email, $admin_email ,$user->password ,$admin_passwd);
        if (!empty($user)) {
            if ( $user->email == $admin_email && $user->password == $admin_passwd ) {
                $check = true;
            }
        }
        return $check;
    }

}
