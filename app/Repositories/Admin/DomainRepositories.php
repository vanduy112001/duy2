<?php

namespace App\Repositories\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Factories\UserFactories;
use App\Model\DetailOrder;
use App\Model\Domain;
use App\Model\DomainProduct;
use App\Model\User;
use App\Model\Email;
use App\Model\Order;
use Carbon\Carbon;
use App\Model\HistoryPay;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use App\Model\DomainExpired;
// API
use App\Services\DomainPA;
use phpDocumentor\Reflection\Types\Null_;

class DomainRepositories {

    protected $user;
    protected $email;
    protected $order;
    protected $domain;
    protected $detail_order;
    protected $home;
    protected $pavietnam;
    protected $domain_product;
    // goi email
    protected $user_send_email;
    protected $subject;
    protected $history_pay;
    protected $domain_exp;


    public function __construct()
    {
        $this->user = new User;
        $this->pavietnam = new DomainPA();
        $this->domain = new Domain;
        $this->domain_product = new DomainProduct;
        $this->email = new Email;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->history_pay = new HistoryPay;
        $this->domain_exp = new DomainExpired;
    }

    public function get_all()
    {
        return $this->domain->with('user', 'domain_product', 'detail_order')->orderBy('id', 'desc')->get();
    }

    public function delete($id)
    {
        $delete = $this->domain->find($id)->delete();
        if($delete) {
            $data = [
                'status' => true,
                'message' => 'Xóa thành công tên miền id: ',
            ];
        } else {
            $data = [
                'status' => false,
                'message' => 'Truy vấn lỗi',
            ];
        }
        return $data;
    }

    public function registerDomain($domain, $due_date)
    {
        return $this->pavietnam->registerDomainPA($domain, $due_date);
    }

    public function get_info_domain($domain)
    {
        return $this->pavietnam->get_info_domain($domain);
    }

    public function renewDomain($domain, $year)
    {
        return $this->pavietnam->renewDomain($domain, $year);
    }

    public function detail($id)
    {
       return $this->domain->find($id);
    }

    public function multidelete($id)
    {
      try {
        $result = [
          'status' => false,
          'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
        ];
        // Xóa tất cả các table có liên quan trên user
        $delete = $this->domain->whereIn('id', $id)->delete();

        if ($delete) {
          $result = [
            'status' => true,
            'message' => 'Xóa thành công.',
            'id' => $id
          ];
        }
        return $result;
      } catch (\Exception $exception) {
        return [
          'status' => false,
          'message' => $exception->getMessage(),
        ];
      }
    }

    public function info($id)
    {
        $info = $this->domain->find($id);
        return json_encode($info);
    }

    public function get_domain_by_detail_id($id)
    {
      return $this->domain->where('detail_order_id', $id)->get();
    }

    public function change_pass_domain($id, $pass)
    {
      $get_domain = $this->domain->find($id);
      $old_pass =  $get_domain->password_domain;
      $update_pass = $get_domain->update(['password_domain' => $pass]);
      if($update_pass) {
        $domain = $get_domain->domain;
        $request_panhanhoa = $this->pavietnam->change_pass_domain($domain, $pass);
        if ($request_panhanhoa->{'ReturnCode'} == 200) {
        // if ($request_panhanhoa['ReturnCode'] == 200) {
          $data = [
            'status' => true,
            'message' => 'Đổi mật khẩu thành công',
            'password' => $get_domain->password_domain,
          ];
        } else { //Nếu cập nhập qua PA lỗi thì trả lại pass domain cũ cho portal
          $get_domain->update(['password_domain' => $old_pass]);
          $data = [
            'status' => false,
            'message' => $request_panhanhoa->{'ReturnText'},
            'password' => $get_domain->password_domain,
          ];
        }

      } else {
        $data = [
          'status' => false,
          'message' => 'Đổi mật khẩu thất bại',
          'password' => $get_domain->password_domain,
        ];
      }
      return json_encode($data);
    }

    public function check_account_still()
    {
        $request_panhanhoa = $this->pavietnam->check_account_still();
        if (!empty($request_panhanhoa->{'Username'})) {
        // if (!empty($request_panhanhoa['user_name'])) {
          $data = [
            'status' => true,
            'user_name' => $request_panhanhoa->{'Username'},
            'money' => $request_panhanhoa->{'Money'},
          ];
        } else {
          $data = [
            'status' => false,
            'message' => $request_panhanhoa->{'ReturnText'},
          ];
        }
        return json_encode($data);
    }

    public function update_domain($id, $data)
    {
      $domain = $this->domain->find($id);
      if(isset($domain)) {
        if (!empty($data['cmnd_after'])) {
          $cmnd_after = preg_replace('/\s+/', '', $data['cmnd_after']->getClientOriginalName());
          $file_name_after = date('d-m-Y').'-'.'scan-after-'. '-' . $cmnd_after;
          $path1 = $data['cmnd_after']->storeAs('public/cmnd', $file_name_after);
        } else {
            $cmnd_after  = $domain->cmnd_after;
        }
        if (!empty($data['cmnd_before'])) {
          $cmnd_before = preg_replace('/\s+/', '', $data['cmnd_before']->getClientOriginalName());
          $file_name_before = date('d-m-Y').'-'.'scan-before-'. '-' . $cmnd_before;
          $path2 = $data['cmnd_before']->storeAs('public/cmnd', $file_name_before);
        } else {
            $cmnd_before  = $domain->cmnd_before;
        }
        $path = 'storage/cmnd/';
        $data_domain = [
          'detail_order_id' => !empty($data['detail_order_id']) ? $data['detail_order_id'] : $domain->detail_order_id,
          'user_id' => !empty($data['user_id']) ? $data['user_id'] : $domain->user_id,
          'status' => !empty($data['status']) ? $data['status'] : $domain->status,
          'product_domain_id' => !empty($data['product_domain_id']) ? $data['product_domain_id'] : $domain->product_domain_id,
          'billing_cycle' => !empty($data['billing_cycle']) ? $data['billing_cycle'] : $domain->billing_cycle,
          'customer_domain' => !empty($data['customer_domain']) ? $data['customer_domain'] : $domain->customer_domain,
          'owner_name' => !empty($data['owner-name']) ? $data['owner-name'] : $domain->owner_name,
          'ownerid_number' => !empty($data['ownerid-number']) ? $data['ownerid-number'] : $domain->ownerid_number,
          'owner_taxcode' => !empty($data['owner-taxcode']) ? $data['owner-taxcode'] : $domain->owner_taxcode,
          'owner_address' => !empty($data['owner-address']) ? $data['owner-address'] : $domain->owner_address,
          'owner_email' => !empty($data['owner-email']) ? $data['owner-email'] : $domain->owner_email,
          'owner_phone' => !empty($data['owner-phone']) ? $data['owner-phone'] : $domain->owner_phone,
          'ui_name' => !empty($data['ui_name']) ? $data['ui_name'] : $domain->ui_name,
          'uiid_number' => !empty($data['uiid_number']) ? $data['uiid_number'] : $domain->uiid_number,
          'ui_gender' => !empty($data['ui_gender']) ? $data['ui_gender'] : $domain->ui_gender,
          'ui_birthdate' => !empty($data['ui_birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['ui_birthdate'])->format('Y-m-d') : $domain->ui_birthdate,
          'ui_address' => !empty($data['ui_address']) ? $data['ui_address'] : $domain->ui_address,
          'ui_province' => !empty($data['ui_province']) ? $data['ui_province'] : $domain->ui_province,
          'ui_email' => !empty($data['ui_email']) ? $data['ui_email'] : $domain->ui_email,
          'ui_phone' => !empty($data['ui_phone']) ? $data['ui_phone'] : $domain->ui_phone,
          'admin_name' => !empty($data['admin-name']) ? $data['admin-name'] : $domain->admin_name,
          'adminid_number' => !empty($data['adminid-number']) ? $data['adminid-number'] : $domain->adminid_number,
          'admin_gender' => !empty($data['admin-gender']) ? $data['admin-gender'] : $domain->admin_gender,
          'admin_birthdate' => !empty($data['admin-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['admin-birthdate'])->format('Y-m-d') : $domain->admin_birthdate,
          'admin_address' => !empty($data['admin-address']) ? $data['admin-address'] : $domain->admin_address,
          'admin_province' => !empty($data['admin-province']) ? $data['admin-province'] : $domain->admin_province,
          'admin_email' => !empty($data['admin-email']) ? $data['admin-email'] : $domain->admin_email,
          'admin_phone' => !empty($data['admin-phone']) ? $data['admin-phone'] : $domain->admin_phone,
          'domain' => !empty($data['domain']) ? $data['domain'] : $domain->domain,
          'domain_ext' => !empty($data['domain_ext']) ? $data['domain_ext'] : $domain->domain_ext,
          'domain_name' => !empty($data['domain_name']) ? $data['domain_name'] : $domain->domain_name,
          'domain_year' => !empty($data['domain_year']) ? $data['domain_year'] : $domain->domain_year,
          'password_domain' => !empty($data['password_domain']) ? $data['password_domain'] : $domain->password_domain,
          'next_due_date' => !empty($data['next_due_date']) ? $data['next_due_date'] : $domain->next_due_date,
          'promotion' => !empty($data['promotion']) ? $data['promotion'] : $domain->promotion,
          'cmnd_after' => !empty($path1) ? $path.$file_name_after : $domain->cmnd_after,
          'cmnd_before' => !empty($path2) ? $path.$file_name_before : $domain->cmnd_before,
        ];
        // dd($data_domain);
        $update_domain = $this->domain->where('id', $id)->update($data_domain);
      }
      if($update_domain) {
        return true;
      } else {
        return false;
      }
    }

    public function create_domain($data, $product)
    {
      $time = config('billing_domain');
      $total = $product[$data['billing_cycle']];
      $user = $this->user->find($data['user_id']);
      $path = 'storage/cmnd/'. $data['user_id']. '/';
      $date_paid = Carbon::createFromFormat('d/m/Y', $data['date_paid'])->format('Y-m-d');
      if (!empty($data['cmnd_after'])) {
        $cmnd_after = preg_replace('/\s+/', '', $data['cmnd_after']->getClientOriginalName());
        $file_name_after = date('d-m-Y').'-'.'scan-after-' . $cmnd_after;
        $path1 = $data['cmnd_after']->storeAs('public/cmnd/' . $data['user_id'], $file_name_after);
        $cmnd_after_storage_link = $path . $file_name_after;
      } elseif (!empty($data['cmnd_after_in_profile']) && empty($data['cmnd_after'])) {
        $cmnd_after_storage_link = $user->user_meta->cmnd_after;
      }
      if (!empty($data['cmnd_before'])) {
        $cmnd_before = preg_replace('/\s+/', '', $data['cmnd_before']->getClientOriginalName());
        $file_name_before = date('d-m-Y').'-'.'scan-before-' . $cmnd_before;
        $path2 = $data['cmnd_before']->storeAs('public/cmnd/' . $data['user_id'], $file_name_before);
        $cmnd_before_storage_link = $path . $file_name_before;
      } elseif (!empty($data['cmnd_before_in_profile']) && empty($data['cmnd_before'])) {
        $cmnd_before_storage_link = $user->user_meta->cmnd_after;
      } 
      
      //create order detail
      $data_order = [
        'user_id' => $data['user_id'],
        'total' => $total,
        'status' => 'Finish',
        'description' => !empty($data['description']) ? $data['description'] : '',
      ];
      $order = $this->order->create($data_order);

      //create order detail
      $date = date('Y-m-d');
      $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Domain',
          'due_date' => date('Y-m-d', strtotime( $date . '+3 Day' )),
          'paid_date' => date('Y-m-d'),
          'description' => !empty($data['description'])? $data['description'] : '',
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => '1',
          'user_id' => $data['user_id'],
      ];
      $detail_order = $this->detail_order->create($data_order_detail);

      //create order history
      $data_history = [
        'user_id' => $data['user_id'],
        'ma_gd' => 'GDODD' . strtoupper(substr(sha1(time()), 34, 39)),
        'discription' => 'Hóa đơn số ' . $detail_order->id,
        'type_gd' => '6',
        'method_gd' => 'domain',
        'date_gd' => date('Y-m-d'),
        'money'=> $total,
        'status' => 'Active',
        'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      $next_due_date = date('Y-m-d', strtotime( $date_paid . '+'.$time[$data['billing_cycle']].' Year' ));
      // if ($data['status'] == 'Active') {
      //   $next_due_date = date('Y-m-d', strtotime( $date_paid . '+'.$time[$data['billing_cycle']].' Year' ));
      // } else {
      //   $next_due_date = NULL;
      // }

      if($detail_order && $data_order) {
        $data_domain = [
          'user_id' => $data['user_id'],
          'detail_order_id' => $detail_order->id,
          'product_domain_id' => $product->id,
          'domain' => $data['domain'],
          'status' => $data['status'],
          'password_domain' => $data['password_domain'],
          'customer_domain' => $data['customer_domain'],
          'billing_cycle' => $data['billing_cycle'],
          'domain_ext' => $product->type,
          'domain_name' => strstr( $data['domain'], ".", true),
          'domain_year' => $time[$data['billing_cycle']],
          'next_due_date' => $next_due_date,
          'owner_name' => $data['owner-name'],
          'ownerid_number' => $data['ownerid-number'],
          'owner_taxcode' => $data['owner-taxcode'],
          'owner_address' => $data['owner-address'],
          'owner_email' => $data['owner-email'],
          'owner_phone' => $data['owner-phone'],
          'ui_name' => $data['ui_name'],
          'uiid_number' => $data['uiid_number'],
          'ui_phone' => $data['ui_phone'],
          'ui_email' => $data['ui_email'],
          'ui_address' => $data['ui_address'],
          'ui_province' => $data['ui_province'],
          'admin_name' => !empty($data['admin-name']) ? $data['admin-name'] : '',
          'adminid_number' => !empty($data['adminid-number']) ? $data['adminid-number'] : '',
          'admin_gender' => !empty($data['admin-gender']) ? $data['admin-gender'] : '',
          'admin_birthdate' => !empty($data['admin-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['admin-birthdate'])->format('Y-m-d') : NULL,
          'admin_address' => !empty($data['admin-address']) ? $data['admin-address'] : '',
          'admin_province' => !empty($data['admin-province']) ? $data['admin-province'] : '',
          'admin_email' => !empty($data['admin-email']) ? $data['admin-email'] : '',
          'admin_phone' => !empty($data['admin-phone']) ? $data['admin-phone'] : '',
          'cmnd_before' => !empty($cmnd_before_storage_link) ? $cmnd_before_storage_link : '',
          'cmnd_after' => !empty($cmnd_after_storage_link) ? $cmnd_after_storage_link : '',
        ];
        // dd($data_domain);
        $create_domain = $this->domain->create($data_domain);
        if(!$create_domain) {
          return $result = [
            'status' => false,
            'message' => "Truy vấn lỗi",
          ];
        }
        //register domain via PA Việt Nam (if Status = Active)
        if ($data['status'] == 'Active') {
          $request_panhanhoa = $this->registerDomain($create_domain, Carbon::now()->addYear()); //Request API PAVietNam
          if($request_panhanhoa->{'ReturnCode'} == 200) {
            $data_reg = [
                'user_id' => $data['user_id'],
                'domain' => $create_domain->domain,
                'due_date' => $create_domain->next_due_date,
                'total' => $total,
            ];
            // $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
            //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            //     $message->to($user->email)->subject('Hoàn thành đăng ký domain');
            // });
            $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain');
            });
            return $result = [
              'status' => true,
              'message' => 'Tạo tên miền trên Portal và đăng ký trên PA Việt Nam thành công'
            ];
          } else {
            $data_reg = [
                'user_id' => $data['user_id'],
                'domain' => $create_domain->domain,
                'error_code' => $request_panhanhoa->{'ReturnCode'},
                'error' => $request_panhanhoa->{'ReturnText'},
            ];
            // dd($data_error);
            $mail = Mail::send('users.mails.error_reg_domain', $data_reg, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
            });
            $create_domain->update(['status' => 'Pending']);
            // $create_domain->update(['status' => 'Pending', 'next_due_date' => NULL]);
            return $result = [
              'status' => false,
              'message' => "Tạo tên miền trên Portal thành công, tuy nhiên đăng ký trên PA Việt Nam bị lỗi ( Mã lỗi: " . $data_reg['error_code'] . " - " .  $data_reg['error'] . " )",
            ];
         }
        }
        return $result = [
          'status' => true,
          'message' => 'Tạo tên miền trên Portal thành công'
        ];
      } else {
        return $result = [
          'status' => false,
          'message' => 'Tạo tên miền trên Portal thất bại'
        ];
      }
    }

    public function get_domain($id)
    {
      return $this->domain->find($id);
    }

    public function reOrderDomain($domain, $due_date)
    {
      $request_panhanhoa = $this->pavietnam->registerDomainPA($domain, $due_date);
      $date = ('Y-m-d');
      $time = config('billing_domain');
      $next_due_date = date('Y-m-d', strtotime( $date . '+'.$time[$domain['billing_cycle']].' Year' ));
      $user = $this->user->find($domain->user_id);
      if($request_panhanhoa->{'ReturnCode'} == 200) {
        $data_reg = [
            'user_id' => $domain->user_id,
            'domain' => $domain->domain,
            'due_date' => $next_due_date,
            'total' => $total,
        ];
        // $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
        //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
        //     $message->to(User::findOrFail($domain->user_id)->email)->subject('Hoàn thành đăng ký domain');
        // });
        $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain');
        });
        $domain->update(['status' => 'Active', 'next_due_date' => $next_due_date]);
        return $result = [
          'status' => true,
          'message' => 'Đăng ký tên miền trên PA Việt Nam thành công'
        ];
      } else {
        $data_reg = [
            'user_id' => $domain->user_id,
            'domain' => $domain->domain,
            'error_code' => $request_panhanhoa->{'ReturnCode'},
            'error' => $request_panhanhoa->{'ReturnText'},
        ];
        // dd($data_error);
        $mail = Mail::send('users.mails.error_reg_domain', $data_reg, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
        });
        $domain->update(['status' => 'Pending']);
        return $result = [
          'status' => false,
          'message' => "Đăng ký trên PA Việt Nam bị lỗi ( Mã lỗi: " . $data_reg['error_code'] . " - " .  $data_reg['error'] . " )",
        ];
     }

    }

    public function update_next_due_date($id, $next_due_date)
    {
      $result = [
        'status' => false,
        'message' => 'Truy vấn lỗi, vui lòng thử lại',
      ];
      if(!empty($next_due_date)) {
        $next_due_date = Carbon::createFromFormat('d-m-Y', $next_due_date)->format('Y-m-d');
      } else {
        $next_due_date = NULL;
      }
      $domain = $this->domain->find($id);
      if($domain) {
        $update = $domain->update(['next_due_date' => $next_due_date]);
        if($update) {
          if($domain->next_due_date !== NULL) {
            $result = [
              'status' => true,
              'next_due_date' => Carbon::createFromFormat('Y-m-d', $domain->next_due_date)->format('d-m-Y'),
            ];
          } else {
            $result = [
              'status' => true,
              'next_due_date' => 'Chưa tạo',
            ];
          }
          return $result;
        } else {
          return $result;
        }
      } else {
        return $result;
      }
    }

    public function extend_domain_expired($data, $product)
    {
      $time = config('billing_domain_exp');
      $domain = $this->domain->where('domain', $data['domain'])->first();
      $next_due_date = $domain->next_due_date;
      if(!$next_due_date) {
        return $result = [
          'status' => false,
          'message' => "Không có dữ liệu next_due_date trong data",
        ];
      }
      $total = $product[$data['billing']];
      $user = $this->user->find($domain->user_id);
      $date_paid = date('Y-m-d');

      //create order exp
      $data_order = [
        'user_id' => $user->id,
        'total' => $total,
        'status' => 'Finish',
        'description' => 'Gia hạn domain',
        'type' => 'expired',
      ];
      $order = $this->order->create($data_order);
      //create order exp detail
      $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Domain_exp',
          'due_date' => date('Y-m-d', strtotime( $next_due_date . '+'.$time[$data['billing']].' Year' )),
          'paid_date' => date('Y-m-d'),
          'description' => 'Gia hạn domain',
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => '1',
          'user_id' => $user->id,
      ];
      $detail_order = $this->detail_order->create($data_order_detail);

      //create order history
      $data_history = [
        'user_id' => $user->id,
        'ma_gd' => 'GDEXD' . strtoupper(substr(sha1(time()), 34, 39)),
        'discription' => 'Hóa đơn số ' . $detail_order->id,
        'type_gd' => '7',
        'method_gd' => 'domain_exp',
        'date_gd' => date('Y-m-d'),
        'money'=> $total,
        'status' => 'Active',
        'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);

      // $next_due_date = date('Y-m-d', strtotime( $date_paid . '+'.$time[$data['billing']].' Year' ));

      if($detail_order && $data_order) {
        $data_domain_exp = [
          'billing_cycle' => $data['billing'],
          'detail_order_id' => $detail_order->id,
          'domain_id' => $domain->id,
        ];
        $create_domain_exp = $this->domain_exp->create($data_domain_exp);
        if (!$create_domain_exp) {
          return $result = [
            'status' => false,
            'message' => "Truy vấn lỗi",
          ];
        }
        //Gia hạn domain qua PA Việt Nam
        $request_panhanhoa = $this->renewDomain($domain, $time[$data['billing']]); //Request API PAVietNam
        if($request_panhanhoa->{'ReturnCode'} == 200) {
          $next_due_date = date('Y-m-d', strtotime( $next_due_date . '+'.$time[$data['billing']].' Year' ));
          $domain->update([
            'next_due_date' => $next_due_date,
          ]);
          $data_renew = [
            'user_id' => $user->id,
            'domain' => $domain->domain,
            'due_date' => $domain->next_due_date,
            'total' => $total,
          ];
          $mail_to_admin = Mail::send('users.mails.finish_renew_domain_to_admin', $data_renew, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành gia hạn domain');
          });
          return $result = [
            'status' => true,
            'message' => 'Gia hạn tên miền trên PA Việt Nam thành công'
          ];
        } else {
            $data_error = [
                'user_id' => $user->id,
                'domain' => $domain->domain,
                'error_code' => $request_panhanhoa->{'ReturnCode'},
                'error' => $request_panhanhoa->{'ReturnText'},
            ];
            // dd($data_error);
            $mail = Mail::send('users.mails.error_renew_domain', $data_error, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi gia hạn domain');
            });
            return $result = [
              'status' => false,
              'message' => "Gia hạn tên miền trên PA Việt Nam bị lỗi ( Mã lỗi: " . $data_error['error_code'] . " - " .  $data_error['error'] . " )",
            ];
         }
      } else {
        return $result = [
          'status' => false,
          'message' => "Gia hạn tên miền thất bại, hãy thử lại",
        ];
      }
    }

    public function create_domain_with_PA($data)
    {
        $domain_product = $this->domain_product->find($data['product_id']);
        $billing_cycle = $data['billing_cycle'];
        $user = $this->user->find($data['user_id']);
        $product = $this->domain_product->find($data['product_id']);
        if ($data['price_override']) {
            $total = $data['price_override'];
        } else {
            $total = $domain_product[$billing_cycle];
        }

        //create order
       $data_order = [
           'user_id' => $user->id,
           'total' => $total,
           'status' => 'Active',
           'description' => '',
       ];
       $order = $this->order->create($data_order);

       //create order detail
       $due_date = $data['next_due_date'];
       $data_order_detail = [
           'order_id' => $order->id,
           'type' => 'Domain',
           'due_date' => date('Y-m-d', strtotime($due_date)),
           'description' => !empty($data['description'])? $data['description'] : '',
           'payment_method' => 1,
           'status' => 'paid',
           'sub_total' => $total,
           'quantity' => 1,
           'user_id' => $data['user_id'],
           'paid_date' => date('Y-m-d'),
           'addon_id' => !empty($data['addon'])? '1' : '0',
       ];
       $detail_order = $this->detail_order->create($data_order_detail);
       //create order history
       $data_history = [
           'user_id' => $user->id,
           'ma_gd' => 'GDODD' . strtoupper(substr(sha1(time()), 34, 39)),
           'discription' => 'Hóa đơn số ' . $detail_order->id,
           'type_gd' => '6',
           'method_gd' => 'domain',
           'date_gd' => date('Y-m-d'),
           'money'=> $total,
           'status' => 'Active',
           'method_gd_invoice' => 'credit',
           'detail_order_id' => $detail_order->id,
       ];
       $this->history_pay->create($data_history);
       // create domain
       $billing_domain = [
         'annually' => 1,
         'biennially' => 2,
         'triennially' => 3,
       ];
       $unicode = array(
         'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
         'd'=>'đ|Đ',
         'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
         'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
         'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
         'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
         'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
       );
       $str_name = strtolower($data['ui_name']);
       $path = 'storage/cmnd/';
       $cmnd_before = '';
       $cmnd_after = '';
       $file_name_after = '';
       $file_name_before = '';
       if ( !empty($data['cmnd_before']) ) {
         $cmnd_before = preg_replace('/\s+/', '', $data['cmnd_before']->getClientOriginalName());
         $file_name_before = date('d-m-Y').'-'.'scan-before-' . $cmnd_before;
         $path3 = $data['cmnd_before']->storeAs('public/cmnd', $file_name_before);
       }
       if ( !empty($data['cmnd_after']) ) {
         $cmnd_after = preg_replace('/\s+/', '', $data['cmnd_after']->getClientOriginalName());
         $file_name_after = date('d-m-Y').'-'.'scan-after-' . $cmnd_after;
         $path2 = $data['cmnd_after']->storeAs('public/cmnd', $file_name_after);
       }

       $data_domain = [
           'detail_order_id' => $detail_order->id,
           'user_id' => $user->id,
           'next_due_date' => date('Y-m-d', strtotime($due_date)),
           'product_domain_id' => $data['product_id'],
           'status' => 'Active',
           'billing_cycle' => $data['billing_cycle'],
           'customer_domain' => $data['customer_domain'],
           'owner_name' => $data['owner-name'],
           'owner_taxcode' => !empty($data['owner-taxcode']) ? $data['owner-taxcode'] : '',
           'ownerid_number' => !empty($data['ownerid-number']) ? $data['ownerid-number'] : '',
           'owner_address' => $data['owner-address'],
           'owner_email' => $data['owner-email'],
           'owner_phone' => $data['owner-phone'],
           'ui_name' => $data['ui_name'],
           'uiid_number' => !empty($data['uiid_number']) ? $data['uiid_number'] : '',
           'uiid_taxcode' => !empty($data['ui_taxcode']) ? $data['ui_taxcode'] : '',
           'ui_gender' => !empty($data['ui_gender']) ? $data['ui_gender'] : '',
           'ui_birthdate' => !empty($data['ui_birthdate']) ? date('Y/m/d', strtotime($data['ui_birthdate'])) : NULL,
           'ui_address' => $data['ui_address'],
           'ui_province' => $data['ui_province'],
           'ui_email' => $data['ui_email'],
           'ui_phone' => $data['ui_phone'],
           'admin_name' => !empty($data['admin-name']) ? $data['admin-name'] : '',
           'adminid_number' => !empty($data['adminid-number']) ? $data['adminid-number'] : '',
           'admin_gender' => !empty($data['admin-gender']) ? $data['admin-gender'] : '',
           'admin_birthdate' => !empty($data['admin-birthdate']) ? date('Y/m/d', strtotime($data['admin-birthdate'])) : NULL,
           'admin_address' => !empty($data['admin-address']) ? $data['admin-address'] : '',
           'admin_province' => !empty($data['admin-province']) ? $data['admin-province'] : '',
           'admin_email' => !empty($data['admin-email']) ? $data['admin-email'] : '',
           'admin_phone' => !empty($data['admin-phone']) ? $data['admin-phone'] : '',
           'domain' => $data['domain'],
           'domain_ext' => $product->type,
           'domain_name' => strstr($data['domain'],'.',true),
           'domain_year' => $billing_domain[$data['billing_cycle']],
           'password_domain' => $data['password_domain'],
           'cmnd_after' => $path . $file_name_after,
           'cmnd_before' => $path . $file_name_before,
       ];
       $create_domain = $this->domain->create($data_domain);
       return $create_domain;
    }

    public function get_data_user($user_id) {
      return $this->user->where('id', $user_id)->with('user_meta')->first();
    }

}
