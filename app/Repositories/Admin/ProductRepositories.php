<?php

namespace App\Repositories\Admin;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\MetaGroupProduct;
use App\Model\User;
use App\Model\VpsOs;
use App\Model\ProductUpgrate;
use App\Model\LogActivity;
use App\Model\State;
use App\Model\ProductDrive;
use App\Model\ProductDatacenter;
use App\Model\ProductRaid;
use App\Model\ServerManagement;
use App\Model\GroupUser;
use App\Model\ProxyState;
use Illuminate\Support\Facades\Auth;

class ProductRepositories
{

    protected $group_product;
    protected $product;
    protected $product_upgrate;
    protected $meta_product;
    protected $meta_group_product;
    protected $pricing;
    protected $user;
    protected $vps_os;
    protected $log_activity;
    protected $state;
    protected $product_drive;
    protected $product_datacenter;
    protected $product_raid;
    protected $server_management;
    protected $group_user;
    protected $state_proxy;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->product_upgrate = new ProductUpgrate;
        $this->meta_product = new MetaProduct;
        $this->meta_group_product = new MetaGroupProduct;
        $this->pricing = new Pricing;
        $this->user = new User;
        $this->vps_os = new VpsOs;
        $this->log_activity = new LogActivity();
        $this->state = new State();
        $this->product_drive = new ProductDrive();
        $this->product_datacenter = new ProductDatacenter();
        $this->product_raid = new ProductRaid();
        $this->server_management = new ServerManagement();
        $this->group_user = new GroupUser;
        $this->state_proxy = new ProxyState;
    }

    public function get_all_group_product()
    {
        return $this->group_product->with('products')->orderBy('id', 'asc')->get();
    }

    public function list_vps_os($id)
    {
        return $this->vps_os->where('product_id', $id)->get();
    }

    public function get_products_in_event()
    {
        $products = $this->product->orderBy('id', 'desc')->with('group_product')->get();
        return $products;
    }

    public function get_product()
    {
        $products = $this->product->where('type_product', 'Hosting')->orderBy('id', 'desc')->get();
        return $products;
    }

    public function list_product_hosting_default()
    {
        $group_products = $this->group_product->where('private', false)->orderBy('id', 'desc')->with('products')->get();
        $data  = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->type_product == 'Hosting') {
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    public function list_product_duplicate($id)
    {
        return $this->product->where('duplicate', $id)->get();
    }

    public function list_product_hosting_private_default()
    {
        $group_products = $this->group_product->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        $data  = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->type_product == 'Hosting') {
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    public function list_product_hosting_singapore_default()
    {
        $group_products = $this->group_product->where('private', false)->orderBy('id', 'desc')->with('products')->get();
        $data  = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->type_product == 'Hosting-Singapore') {
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    public function list_product_hosting_singapore_private_default()
    {
        $group_products = $this->group_product->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        $data  = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->type_product == 'Hosting-Singapore') {
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    public function get_product_vps()
    {
        $products = $this->product->where('type_product', 'VPS')->orderBy('id', 'desc')->get();
        return $products;
    }

    public function create_group_product($data, $user_id, $group_product_duplicate)
    {
        $group_product = $this->group_product->create($data);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'tạo',
            'model' => 'Admin/Product',
            'description' => ' nhóm sản phẩm ' . $group_product->name,
        ];
        $this->log_activity->create($data_log);

        if ($group_product) {
            if (!empty($user_id)) {
                foreach ($user_id as $key => $id) {
                    $data_meta_group_product  = [
                        'user_id' => $id,
                        'group_product_id' => $group_product->id,
                        'group_product_duplicate' => $group_product_duplicate,
                    ];
                    $update_meta = $this->meta_group_product->create($data_meta_group_product);
                    if (!$update_meta) {
                        return 'Tạo nhóm sản phẩm riêng thất bại';
                    }
                }
            }
            return 'Tạo nhóm sản phẩm thành công';
        } else {
            return 'Tạo nhóm sản phẩm thất bại';
        }
    }

    // get user
    public function get_user()
    {
        $users = $this->user->get();
        return $users;
    }
    // get group product
    public function get_group_product()
    {
        $group_products = $this->group_product->where('private', false)->orderBy('id', 'desc')->with('products')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                $info = '';
                if ($product->type_product == 'VPS' || $product->type_product == 'VPS-US' || $product->type_product == 'NAT-VPS') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->cpu)) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if (!empty($product->meta_product->memory)) {
                        $info .= '-' . $product->meta_product->memory . ' ram';
                    }
                    if (!empty($product->meta_product->disk)) {
                        $info .= '-' . $product->meta_product->disk . ' disk';
                    }
                    if (!empty($product->meta_product->ip)) {
                        $info .= '-' . $product->meta_product->ip . ' ip';
                    }
                    $info .= '<br>HĐH: ';
                    if (!empty($product->meta_product->os)) {
                        $info .= $product->meta_product->os;
                    }
                    $info .= '<br>Băng thông: ';
                    if (!empty($product->meta_product->bandwidth)) {
                        $info .= $product->meta_product->bandwidth;
                    }
                } elseif ($product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore' || $product->type_product == 'Email Hosting') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->storage)) {
                        $info .= $product->meta_product->storage . 'MB';
                    }
                    if (!empty($product->meta_product->domain)) {
                        $info .= '-' . $product->meta_product->domain . ' domain ';
                    }
                    if (!empty($product->meta_product->database)) {
                        $info .= '-' . $product->meta_product->database . ' db ';
                    }
                    if (!empty($product->meta_product->ftp)) {
                        $info .= '-' . $product->meta_product->ftp . ' ftp ';
                    }
                    $info .= '<br>Control panel: ';
                    if (!empty($product->meta_product->panel)) {
                        $info .= $product->meta_product->panel;
                    }
                } elseif ($product->type_product == 'Addon-VPS') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->cpu)) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if (!empty($product->meta_product->memory)) {
                        $info .= $product->meta_product->memory . ' ram';
                    }
                    if (!empty($product->meta_product->disk)) {
                        $info .= $product->meta_product->disk . ' disk';
                    }
                    if (!empty($product->meta_product->ip)) {
                        $info .= $product->meta_product->ip . ' ip';
                    }
                }
                $product->info = $info;
                $duplicate = '';
                $list_product_duplicate = $this->product->where('duplicate', $product->id)->get();
                foreach ($list_product_duplicate as $key => $product_duplicate) {
                    $duplicate .= 'Đại lý ';
                    if (!empty($product_duplicate->group_product->group_user->name)) {
                        $duplicate .= $product_duplicate->group_product->group_user->name;
                    }
                    $duplicate .= ' - ';
                    if ($product_duplicate->pricing->type == 'one_time') {
                        $duplicate .= number_format($product_duplicate->pricing->one_time_pay, 0, ",", ".") . '/Vĩnh viễn';
                    } else {
                        if (!empty($product_duplicate->pricing->monthly)) {
                            $duplicate .= number_format($product_duplicate->pricing->monthly, 0, ",", ".") . '/1 Tháng';
                        } elseif (!empty($product_duplicate->pricing->twomonthly)) {
                            $duplicate .= number_format($product_duplicate->pricing->twomonthly, 0, ",", ".") . '/2 Tháng';
                        } elseif (!empty($product_duplicate->pricing->quarterly)) {
                            $duplicate .= number_format($product_duplicate->pricing->quarterly, 0, ",", ".") . '/3 Tháng';
                        } elseif (!empty($product_duplicate->pricing->semi_annually)) {
                            $duplicate .= number_format($product_duplicate->pricing->semi_annually, 0, ",", ".") . '/6 Tháng';
                        } elseif (!empty($product_duplicate->pricing->annually)) {
                            $duplicate .= number_format($product_duplicate->pricing->monthly, 0, ",", ".") . '/1 Năm';
                        } elseif (!empty($product_duplicate->pricing->biennially)) {
                            $duplicate .= number_format($product_duplicate->pricing->biennially, 0, ",", ".") . '/2 Năm';
                        } elseif (!empty($product_duplicate->pricing->triennially)) {
                            $duplicate .= number_format($product_duplicate->pricing->triennially, 0, ",", ".") . '/3 Năm';
                        }
                    }
                    $duplicate .= '<br />';
                }
                $product->duplicate = $duplicate;
            }
            $data[] = $group_product;
        }
        return $data;
    }
    // get group product private
    public function list_private_product($id)
    {
        if (!empty($id)) {
            $group_products = $this->group_product->where('group_user_id', $id)->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        } else {
            $group_products = $this->group_product->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        }
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                $info = '';
                if ($product->type_product == 'VPS' || $product->type_product == 'VPS-US' || $product->type_product == 'NAT-VPS') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->cpu)) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if (!empty($product->meta_product->memory)) {
                        $info .= '-' . $product->meta_product->memory . ' ram';
                    }
                    if (!empty($product->meta_product->disk)) {
                        $info .= '-' . $product->meta_product->disk . ' disk';
                    }
                    if (!empty($product->meta_product->ip)) {
                        $info .= '-' . $product->meta_product->ip . ' ip';
                    }
                    $info .= '<br>HĐH: ';
                    if (!empty($product->meta_product->os)) {
                        $info .= $product->meta_product->os;
                    }
                    $info .= '<br>Băng thông: ';
                    if (!empty($product->meta_product->bandwidth)) {
                        $info .= $product->meta_product->bandwidth;
                    }
                } elseif ($product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore' || $product->type_product == 'Email Hosting') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->storage)) {
                        $info .= $product->meta_product->storage . 'MB';
                    }
                    if (!empty($product->meta_product->domain)) {
                        $info .= '-' . $product->meta_product->domain . ' domain ';
                    }
                    if (!empty($product->meta_product->database)) {
                        $info .= '-' . $product->meta_product->database . ' db ';
                    }
                    if (!empty($product->meta_product->ftp)) {
                        $info .= '-' . $product->meta_product->ftp . ' ftp ';
                    }
                    $info .= '<br>Control panel: ';
                    if (!empty($product->meta_product->panel)) {
                        $info .= $product->meta_product->panel;
                    }
                } elseif ($product->type_product == 'Addon-VPS') {
                    $info .= 'Cấu hình: ';
                    if (!empty($product->meta_product->cpu)) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if (!empty($product->meta_product->memory)) {
                        $info .= $product->meta_product->memory . ' ram';
                    }
                    if (!empty($product->meta_product->disk)) {
                        $info .= $product->meta_product->disk . ' disk';
                    }
                    if (!empty($product->meta_product->ip)) {
                        $info .= $product->meta_product->ip . ' ip';
                    }
                }
                $product->info = $info;
            }
            $data[] = $group_product;
        }
        return $data;
    }
    // get group product private with group_user
    public function list_private_group_user($id)
    {
        if (!empty($id)) {
            $group_products = $this->group_product->where('group_user_id', $id)->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        } else {
            $group_products = $this->group_product->where('private', true)->orderBy('id', 'desc')->with('products')->get();
        }
        return $group_products;
    }
    // get all pricing
    public function get_pricings()
    {
        return $this->pricing->get();
    }
    // detail group product
    public function detail_group_product($id)
    {
        $group_product = $this->group_product->find($id);
        return $group_product;
    }
    // update group product
    public function update_group_product($id, $data, $user_id, $group_product_duplicate, $list_vps_os)
    {
        $group_product = $this->group_product->find($id);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'chỉnh sửa',
            'model' => 'Admin/Product',
            'description' => ' nhóm sản phẩm ' . $group_product->name,
        ];
        $this->log_activity->create($data_log);
        $update = $group_product->update($data);
        if ($update) {
            $this->meta_group_product->where('group_product_id', $id)->delete();
            if (!empty($user_id)) {
                foreach ($user_id as $key => $id) {
                    $data_meta_group_product  = [
                        'user_id' => $id,
                        'group_product_id' => $group_product->id,
                        'group_product_duplicate' => $group_product_duplicate,
                    ];
                    $update_meta = $this->meta_group_product->create($data_meta_group_product);
                    if (!$update_meta) {
                        return 'Tạo nhóm sản phẩm riêng thất bại';
                    }
                }
            }
            return 'Sửa nhóm sản phẩm thành công';
        } else {
            return 'Sửa nhóm sản phẩm thất bại';
        }
    }

    // get group product
    public function get_group_products()
    {
        $group_products = $this->group_product->where('private', false)->orderBy('id', 'desc')->get();
        return $group_products;
    }
    // Tạo sản phẩm
    public function store($data)
    {
        $create = $this->product->create($data);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'tạo',
            'model' => 'Admin/Product',
            'description' => ' sản phẩm ' . $create->name,
        ];
        $this->log_activity->create($data_log);
        if ($create) {
            return true;
        } else {
            return false;
        }
    }
    // Lấy detail sản phẩm
    public function detail_product($id)
    {
        $product = $this->product->with('meta_product', 'pricing', 'product_upgrates')->find($id);
        return $product;
    }
    // update sản phẩm
    public function update(
        $id,
        $data_product,
        $data_meta_product,
        $data_pricing,
        $list_vps_os,
        $list_upgrate,
        $data_chassis,
        $datacenterSelect,
        $raidSelect,
        $serverManagementSelect
    ) {
        // san pham
        $product = $this->product->find($id);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'chỉnh sửa',
            'model' => 'Admin/Product',
            'description' => ' sản phẩm ' . $product->name,
        ];
        $this->log_activity->create($data_log);
        $product->update($data_product);
        // meta product
        $meta_product = $this->meta_product->where('product_id', $id)->first();
        if (isset($meta_product)) {
            $meta_product = $meta_product->update($data_meta_product);
        } else {
            $meta_product = $this->meta_product->create($data_meta_product);
        }
        // pricing
        $pricing = $this->pricing->where('product_id', $id)->first();
        if (isset($pricing)) {
            $pricing = $pricing->update($data_pricing);
        } else {
            $pricing = $this->pricing->create($data_pricing);
        }
        // Os
        // dd($list_vps_os);
        if (isset($list_vps_os)) {
            $this->vps_os->where('product_id', $id)->delete();
            foreach ($list_vps_os as $key => $vps_os) {
                $data_vps_on = [
                    'product_id' => $id,
                    'os' => $vps_os,
                ];
                $this->vps_os->create($data_vps_on);
            }
        }
        // upgrate
        if (isset($list_upgrate)) {
            $this->product_upgrate->where('product_default_id', $id)->delete();
            foreach ($list_upgrate as $key => $upgrate) {
                $data_upgrate = [
                    'product_default_id' => $id,
                    'product_upgrate_id' => $upgrate,
                ];
                $this->product_upgrate->create($data_upgrate);
            }
        }
        // chassis
        if ($product->type_product == 'Server') {
            if (!empty($product->product_drive)) {
                $product_drive = $product->product_drive;
                $product_drive->update($data_chassis);
                $product_drive->four = $data_chassis['four'];
                $product_drive->save();
            } else {
                $product_drive = $this->product_drive->create($data_chassis);
                $product_drive->four = $data_chassis['four'];
                $product_drive->save();
            }
        }
        // Datacenter
        if ($product->type_product == 'Server' || $product->type_product == 'Colocation') {
            $this->product_datacenter->where('product_id', $id)->delete();
            foreach ($datacenterSelect as $key => $datacenter) {
                if ($datacenter != 'All') {
                    $data_datacenter = [
                        'product_id' => $id,
                        'datacenter' => $datacenter,
                    ];
                    $this->product_datacenter->create($data_datacenter);
                }
            }
        }
        // raid
        if ($product->type_product == 'Server') {
            $this->product_raid->where('product_id', $id)->delete();
            foreach ($raidSelect as $key => $raid) {
                if ($raid != 'All') {
                    $data_raid = [
                        'product_id' => $id,
                        'raid' => $raid,
                    ];
                    $this->product_raid->create($data_raid);
                }
            }
        }
        // server_management
        if ($product->type_product == 'Server') {
            $this->server_management->where('product_id', $id)->delete();
            foreach ($serverManagementSelect as $key => $server_management) {
                $data_server_management = [
                    'product_id' => $id,
                    'server_management' => $server_management,
                ];
                $this->server_management->create($data_server_management);
            }
        }
        if ($product && $meta_product && $pricing) {
            return true;
        } else {
            return false;
        }
    }
    // update duplicate sản phẩm
    public function update_duplicate($data)
    {
        $group_users = $this->group_user->get();
        foreach ($group_users as $key => $group_user) {
            if (!empty($data['duplicate' . $group_user->id])) {
                // dd($data);
                // chi tiết product
                $data_product = [
                    'name' => $data['name'],
                    'group_product_id' => $data['group_product' . $group_user->id],
                    'type_product' => $data['type_product'],
                    'module' => !empty($data['module']) ? $data['module'] : '',
                    'hidden' => !empty($data['hidden']) ? $data['hidden'] : 0,
                    'stt' => !empty($data['stt']) ? $data['stt'] : 10,
                    'package' => !empty($data['package']) ? $data['package'] : '',
                    'description' => !empty($data['description']) ? $data['description'] : '',
                    'duplicate' => $data['id']
                ];
                if (!empty($data['id' . $group_user->id])) {
                    $product = $this->product->find($data['id' . $group_user->id]);
                    $product->update($data_product);
                } else {
                    $product = $this->product->create($data_product);
                }
                // chi tiết meta_product
                $data_meta_product = [
                    'product_id' => $product->id,
                    'cpu' => !empty($data['cpu']) ? $data['cpu'] : '',
                    'memory' => !empty($data['ram']) ? $data['ram'] : '',
                    'disk' => !empty($data['disk']) ? $data['disk'] : '',
                    'bandwidth' => !empty($data['bandwidth']) ? $data['bandwidth'] : '',
                    'ip' => !empty($data['ip']) ? $data['ip'] : '',
                    'os' => !empty($data['os']) ? $data['os'] : '',
                    'email_id' => !empty($data['email_id']) ? $data['email_id'] : '',
                    'email_create' => !empty($data['email_create']) ? $data['email_create'] : '',
                    'storage' => !empty($data['storage']) ? $data['storage'] : '',
                    'domain' => !empty($data['domain']) ? $data['domain'] : '',
                    'sub_domain' => !empty($data['sub_domain']) ? $data['sub_domain'] : '',
                    'alias_domain' => !empty($data['alias_domain']) ? $data['alias_domain'] : '',
                    'database' => !empty($data['database']) ? $data['database'] : '',
                    'ftp' => !empty($data['ftp']) ? $data['ftp'] : '',
                    'panel' => !empty($data['panel']) ? $data['panel'] : '',
                    'name_server' => !empty($data['name_server']) ? $data['name_server'] : '',
                    'hidden' => '',
                    'email_expired' => !empty($data['email_expired']) ? $data['email_expired'] : 0,
                    'email_expired_finish' => $data['email_expired_finish'],
                    'type_addon' => !empty($data['type_addon']) ? $data['type_addon'] : '',
                    'product_special' => !empty($data['product_special']) ? $data['product_special'] : 0,
                    'girf' => !empty($data['girf']) ? $data['girf'] : null,
                    'promotion' => !empty($data['promotion']) ? 1 : 0,
                    'qtt_email' => !empty($data['qtt_email']) ? $data['qtt_email'] : '',
                    'chassis' => !empty($data['chassis']) ? $data['chassis'] : '',
                    'raid' => !empty($data['raid']) ? $data['raid'] : '',
                    'datacenter' => !empty($data['datacenter']) ? $data['datacenter'] : '',
                    'server_management' => !empty($data['server_management']) ? $data['server_management'] : '',
                    'backup' => !empty($data['backup']) ? $data['backup'] : '',
                    'cores' => !empty($data['cores']) ? $data['cores'] : '',
                    'port_network' => !empty($data['port_network']) ? $data['port_network'] : '',
                    'email_config_finish' => !empty($data['email_config_finish']) ? $data['email_config_finish'] : 0,
                ];
                $meta_product = $this->meta_product->where('product_id', $product->id)->first();
                if (isset($meta_product)) {
                    $meta_product = $meta_product->update($data_meta_product);
                } else {
                    $meta_product = $this->meta_product->create($data_meta_product);
                }
                // chi tiết pricing
                // 1 tháng
                $monthly = !empty($data['monthly' . $group_user->id]) ? $data['monthly' . $group_user->id] : 0;
                $monthly = str_replace('.', '', $monthly);
                $monthly = str_replace(',', '', $monthly);
                // 2 tháng
                $twomonthly = !empty($data['twomonthly' . $group_user->id]) ? $data['twomonthly' . $group_user->id] : 0;
                $twomonthly = str_replace('.', '', $twomonthly);
                $twomonthly = str_replace(',', '', $twomonthly);
                // 3 tháng
                $quarterly = !empty($data['quarterly' . $group_user->id]) ? $data['quarterly' . $group_user->id] : 0;
                $quarterly = str_replace('.', '', $quarterly);
                $quarterly = str_replace(',', '', $quarterly);
                // 6 tháng
                $semi_annually = !empty($data['semi_annually' . $group_user->id]) ? $data['semi_annually' . $group_user->id] : 0;
                $semi_annually = str_replace('.', '', $semi_annually);
                $semi_annually = str_replace(',', '', $semi_annually);
                // 1 năm
                $annually = !empty($data['annually' . $group_user->id]) ? $data['annually' . $group_user->id] : 0;
                $annually = str_replace('.', '', $annually);
                $annually = str_replace(',', '', $annually);
                // 2 năm
                $biennially = !empty($data['biennially' . $group_user->id]) ? $data['biennially' . $group_user->id] : 0;
                $biennially = str_replace('.', '', $biennially);
                $biennially = str_replace(',', '', $biennially);
                // 3 năm
                $triennially = !empty($data['triennially' . $group_user->id]) ? $data['triennially' . $group_user->id] : 0;
                $triennially = str_replace('.', '', $triennially);
                $triennially = str_replace(',', '', $triennially);
                // vĩnh viễn
                $one_time_pay = !empty($data['one_time_pay' . $group_user->id]) ? $data['one_time_pay' . $group_user->id] : 0;
                $one_time_pay = str_replace('.', '', $one_time_pay);
                $one_time_pay = str_replace(',', '', $one_time_pay);
                // data
                $data_pricing = [
                    'product_id' => $product->id,
                    'type' => !empty($data['type']) ? $data['type'] : 0,
                    'monthly' => $monthly,
                    'twomonthly' => $twomonthly,
                    'quarterly' => $quarterly,
                    'semi_annually' => $semi_annually,
                    'annually' => $annually,
                    'biennially' => $biennially,
                    'triennially' => $triennially,
                    'one_time_pay' => $one_time_pay,
                ];
                // pricing
                $pricing = $this->pricing->where('product_id', $product->id)->first();
                if (isset($pricing)) {
                    $pricing = $pricing->update($data_pricing);
                } else {
                    $pricing = $this->pricing->create($data_pricing);
                }
                // Os
                // dd($list_vps_os);
                $list_vps_os = !empty($data['os_vps']) ? $data['os_vps'] : [];
                if (isset($list_vps_os)) {
                    $this->vps_os->where('product_id', $product->id)->delete();
                    foreach ($list_vps_os as $key => $vps_os) {
                        $data_vps_on = [
                            'product_id' => $product->id,
                            'os' => $vps_os,
                        ];
                        $this->vps_os->create($data_vps_on);
                    }
                }
                // upgrate
                $list_upgrate = !empty($data['upgrate']) ? $data['upgrate'] : [];
                if (isset($list_upgrate)) {
                    $this->product_upgrate->where('product_default_id', $product->id)->delete();
                    foreach ($list_upgrate as $key => $upgrate) {
                        $data_upgrate = [
                            'product_default_id' => $product->id,
                            'product_upgrate_id' => $upgrate,
                        ];
                        $this->product_upgrate->create($data_upgrate);
                    }
                }
                // chassis
                $data_chassis = [
                    'product_id' => $product->id,
                    'first' => !empty($data['drive1']) ? $data['drive1'] : 0,
                    'second' => !empty($data['drive2']) ? $data['drive2'] : 0,
                    'third' => !empty($data['drive3']) ? $data['drive3'] : 0,
                    'four' => !empty($data['drive4']) ? $data['drive4'] : 0,
                    'five' => !empty($data['drive5']) ? $data['drive5'] : 0,
                    'six' => !empty($data['drive6']) ? $data['drive6'] : 0,
                    'seven' => !empty($data['drive7']) ? $data['drive7'] : 0,
                    'eight' => !empty($data['drive8']) ? $data['drive8'] : 0,
                ];
                if ($product->type_product == 'Server') {
                    if (!empty($product->product_drive)) {
                        $product_drive = $product->product_drive;
                        $product_drive->update($data_chassis);
                        $product_drive->four = $data_chassis['four'];
                        $product_drive->save();
                    } else {
                        $product_drive = $this->product_drive->create($data_chassis);
                        $product_drive->four = $data_chassis['four'];
                        $product_drive->save();
                    }
                }
                // Datacenter
                $datacenterSelect = !empty($data['datacenterSelect']) ? $data['datacenterSelect'] : [];
                if ($product->type_product == 'Server' || $product->type_product == 'Colocation') {
                    $this->product_datacenter->where('product_id', $product->id)->delete();
                    foreach ($datacenterSelect as $key => $datacenter) {
                        if ($datacenter != 'All') {
                            $data_datacenter = [
                                'product_id' => $product->id,
                                'datacenter' => $datacenter,
                            ];
                            $this->product_datacenter->create($data_datacenter);
                        }
                    }
                }
                // raid
                $raidSelect = !empty($data['raidSelect']) ? $data['raidSelect'] : [];
                if ($product->type_product == 'Server') {
                    $this->product_raid->where('product_id', $product->id)->delete();
                    foreach ($raidSelect as $key => $raid) {
                        if ($raid != 'All') {
                            $data_raid = [
                                'product_id' => $product->id,
                                'raid' => $raid,
                            ];
                            $this->product_raid->create($data_raid);
                        }
                    }
                }
                // server_management
                $serverManagementSelect = !empty($data['serverManagementSelect']) ? $data['serverManagementSelect'] : [];
                if ($product->type_product == 'Server') {
                    $this->server_management->where('product_id', $product->id)->delete();
                    foreach ($serverManagementSelect as $key => $server_management) {
                        $data_server_management = [
                            'product_id' => $product->id,
                            'server_management' => $server_management,
                        ];
                        $this->server_management->create($data_server_management);
                    }
                }
            }
        }
    }
    // Xoa san pham
    public function deleteProduct($id)
    {
        $delete_pricing = $this->pricing->where('product_id', $id)->delete();
        $delete_meta = $this->meta_product->where('product_id', $id)->delete();
        $this->product_upgrate->where('product_default_id', $id)->delete();
        $this->vps_os->where('product_id', $id)->delete();
        $this->product_drive->where('product_id', $id)->delete();
        // ghi log
        $product = $this->product->find($id);
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'xóa',
            'model' => 'Admin/Product',
            'description' => ' sản phẩm ' . !empty($product->name) ? $product->name : 'đã xóa',
        ];
        $this->log_activity->create($data_log);
        $delete_product = $this->product->where('id', $id)->delete();

        if ($delete_pricing && $delete_meta && $delete_product) {
            return true;
        } else {
            return false;
        }
    }

    // Xoa nhom san pham
    public function deleteGroupProduct($id)
    {
        $group_product = $this->group_product->find($id);
        // ghi log
        $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'xóa',
            'model' => 'Admin/Product',
            'description' => ' nhóm sản phẩm ' . $group_product->name,
        ];
        $this->log_activity->create($data_log);
        $meta_group_product = $this->meta_group_product->where('group_product_id', $id)->get();
        if (isset($meta_group_product)) {
            $this->meta_group_product->where('group_product_id', $id)->delete();
        }
        if (isset($group_product->products)) {
            foreach ($group_product->products as  $product) {
                $delete_pricing = $this->pricing->where('product_id', $product->id)->delete();
                $delete_meta = $this->meta_product->where('product_id', $product->id)->delete();
                $delete_product = $this->product->where('id', $product->id)->delete();
            }
        }
        // dd($group_product);
        $delete = $group_product->delete();
        if ($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function get_product_with_action($action, $user_id)
    {
        $group_products = $this->get_group_product_private($user_id);
        $products = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->type_product == $action) {
                    $products[] = $product;
                }
            }
        }
        return $products;
    }

    // get meta group product
    public function get_group_product_private($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $this->group_product->where('group_user_id', $group_user->id)->with('products')->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->with('products')->get();
        }
        return $group_products;
    }

    // list sản phẩm addon theo loại
    public function get_product_addon_by_type($typeGroup, $typeAddon, $user_id)
    {
        $user = $this->user->find($user_id);
        $data = [];
        $group_products = [];
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', $typeGroup)->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', $typeGroup)->get();
        }
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->meta_product->type_addon ==  $typeAddon) {
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    // get meta group product with type
    public function get_group_product_private_with_type($user_id, $type)
    {

        $user = $this->user->find($user_id);
        $data = [
            'error' => 0,
            'content' => null,
            'user' => $user,
        ];
        if ($type == 'vps' || $type == 'vps_us') {

            $product_addon = [];
            if (!empty($user->group_user_id)) {

                $group_user = $user->group_user;
                $group_products = $this->group_product->where('group_user_id', $group_user->id)->where('type', $type)->with('products')->get();
                foreach ($group_products as $key => $group_product) {
                    if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                        $product_addon = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                    }
                }
            } else {
                $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->where('type', $type)->with('products')->get();
                foreach ($group_products as $key => $group_product) {
                    if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                        $product_addon = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                    }
                }
            }
            $data['content'] = $group_products;
            $data['addon'] = $product_addon;
        } elseif ($type == 'server') {
            $product_addon_disk = [];
            $product_addon_ram = [];
            $product_addon_ip = [];
            if (!empty($user->group_user_id)) {
                $group_user = $user->group_user;
                $group_products = $this->group_product->where('group_user_id', $group_user->id)->where('type', $type)->with('products')->get();
                $group_product_addons = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
                foreach ($group_product_addons as $key => $group_product) {
                    foreach ($group_product->products as $key => $product) {
                        if ($product->meta_product->type_addon == 'addon_disk') {
                            $product->pricing = $product->pricing;
                            $product_addon_disk[] = $product;
                        }
                        if ($product->meta_product->type_addon == 'addon_ram') {
                            $product->pricing = $product->pricing;
                            $product_addon_ram[] = $product;
                        }
                        if ($product->meta_product->type_addon == 'addon_ip') {
                            $product->pricing = $product->pricing;
                            $product_addon_ip[] = $product;
                        }
                    }
                }
            } else {
                $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->where('type', $type)->with('products')->get();
                $group_product_addons = $this->group_product->where('group_user_id', 0)->where('hidden', false)->where('type', 'addon_server')->get();
                foreach ($group_product_addons as $key => $group_product) {
                    foreach ($group_product->products as $key => $product) {
                        if ($product->meta_product->type_addon == 'addon_disk') {
                            $product->pricing = $product->pricing;
                            $product_addon_disk[] = $product;
                        }
                        if ($product->meta_product->type_addon == 'addon_ram') {
                            $product->pricing = $product->pricing;
                            $product_addon_ram[] = $product;
                        }
                        if ($product->meta_product->type_addon == 'addon_ip') {
                            $product->pricing = $product->pricing;
                            $product_addon_ip[] = $product;
                        }
                    }
                }
            }
            $data['content'] = $group_products;
            $data['addon_disk'] = $product_addon_disk;
            $data['addon_ram'] = $product_addon_ram;
            $data['addon_ip'] = $product_addon_ip;
        } elseif ($type == 'hosting') {
            $product_addon = [];
            if (!empty($user->group_user_id)) {
                $group_user = $user->group_user;
                $group_products = $this->group_product->where('group_user_id', $group_user->id)->where('type', $type)->with('products')->get();
                // foreach ($group_products as $key => $group_product) {
                //     if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                //         $product_addon = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                //     }
                // }
            } else {
                $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->where('type', $type)->with('products')->get();
                // foreach ($group_products as $key => $group_product) {
                //     if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                //         $product_addon = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                //     }
                // }
            }
            $data['content'] = $group_products;
            $data['addon'] = $product_addon;
        } elseif ($type == 'colocation' || $type == 'Addon-Colocation') {

            $product_addon = [];
            if (!empty($user->group_user_id)) {
                $group_user = $user->group_user;
                $group_products = $this->group_product->where('group_user_id', $group_user->id)->where('type', $type)->with('products')->get();
            } else {
                $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->where('type', $type)->with('products')->get();
            }
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'colocation')->where('group_product_id', $group_product->id)->count() > 0) {
                    $product_addon = $this->product->where('type_product', 'colocation')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }

            $data['content'] = $group_products;
            $data['addon'] = $product_addon;
        } else {
            $data['error'] = 1;
        }
        return $data;
    }

    public function get_product_json($id)
    {
        $product = $this->product->find($id);
        return $product;
    }

    // Tạo meta group produc
    public function detail_meta_group_product($id)
    {
        $meta_group_product = $this->meta_group_product->where('group_product_id', $id)->get();
        return $meta_group_product;
    }

    public function get_group_private_products($id)
    {
        return $this->group_product->where('group_user_id', $id)->where('private', true)->orderBy('id', 'desc')->get();
    }

    public function get_addon_product($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    public function choose_change_ip_vps($user_id)
    {
        $user = $this->user->find($user_id);
        $pricing = 0;
        if ($user->group_user_id == 0) {
            $group_products = $this->group_product->where('private', false)->get();
            foreach ($group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                    if ($product->type_product == 'Change-IP') {
                        $pricing = $product->pricing->one_time_pay;
                    }
                }
            }
        } else {
            $group_user = $user->group_user();
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                    if ($product->type_product == 'Change-IP') {
                        $pricing = $product->pricing->one_time_pay;
                    }
                }
            }
        }
        return $pricing;
    }

    public function get_product_with_action_add_hosting($action)
    {
        return $this->product->where('type_product', $action)->orderBy('id', 'desc')->get();
    }

    public function get_product_with_quotation($data)
    {

        $billingDashBoard = config('billingDashBoard');
        $product = $this->product->find($data['id']);
        $product->amount = $product->pricing[$data['billing_cycle']];
        $product->ip = $product->meta_product->ip;
        if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS') {
            $billing = !empty($billingDashBoard[$data['billing_cycle']]) ? $billingDashBoard[$data['billing_cycle']] : 1;
            $productAddonCpu = $this->product->find($data['id_addon_cpu']);
            $product->amount += $productAddonCpu->pricing['monthly'] * $billing * $data['addon_cpu'];
            $productAddonRam = $this->product->find($data['id_addon_ram']);
            $product->amount += $productAddonRam->pricing['monthly'] * $billing * $data['addon_ram'];
            $productAddonDisk = $this->product->find($data['id_addon_disk']);
            $product->amount += $productAddonDisk->pricing['monthly'] * $billing * $data['addon_disk'] / 10;
            return $product;
        } elseif ($product->type_product == 'Server') {
            $group_product_addons = $this->group_product->where('group_user_id', $product->group_product->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
            // foreach ($group_product_addons as $key => $group_product) {
            //     foreach ($group_product->products as $key => $product) {
            //         if ( $product->meta_product->type_addon == 'addon_disk' ) {
            //             $product->pricing = $product->pricing;
            //             $product_addon_disk[] = $product;
            //         }
            //         if ( $product->meta_product->type_addon == 'addon_ram' ) {
            //             $product->pricing = $product->pricing;
            //             $product_addon_ram[] = $product;
            //         }
            //         if ( $product->meta_product->type_addon == 'addon_ip' ) {
            //             $product->pricing = $product->pricing;
            //             $product_addon_ip[] = $product;
            //         }
            //     }
            // }
            // tính tiền
            $billing_cycle = $data['billing_cycle'];

            $total = !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : 0;
            if (!empty($data['addon_ram_server'])) {
                $productAddonRam = $this->product->find($data['addon_ram_server']);
                $total += !empty($productAddonRam->pricing[$billing_cycle]) ? $productAddonRam->pricing[$billing_cycle] : 0;
            }
            if (!empty($data['addon_disk2'])) {
                $productDisk2 = $this->product->find($data['addon_disk2']);
                $total += !empty($productDisk2->pricing[$billing_cycle]) ? $productDisk2->pricing[$billing_cycle] : 0;
            }
            if (!empty($data['addon_disk3'])) {
                $productDisk3 = $this->product->find($data['addon_disk3']);
                $total += !empty($productDisk3->pricing[$billing_cycle]) ? $productDisk3->pricing[$billing_cycle] : 0;
            }
            if (!empty($data['addon_disk4'])) {
                $productDisk4 = $this->product->find($data['addon_disk4']);
                $total += $productDisk4->pricing[$billing_cycle];
            }
            if (!empty($data['addon_disk5'])) {
                $productDisk5 = $this->product->find($data['addon_disk5']);
                $total += $productDisk5->pricing[$billing_cycle];
            }
            if (!empty($data['addon_disk6'])) {
                $productDisk6 = $this->product->find($data['addon_disk6']);
                $total += $productDisk6->pricing[$billing_cycle];
            }
            if (!empty($data['addon_disk7'])) {
                $productDisk7 = $this->product->find($data['addon_disk7']);
                $total += $productDisk7->pricing[$billing_cycle];
            }
            if (!empty($data['addon_disk8'])) {
                $productDisk8 = $this->product->find($data['addon_disk8']);
                $total += $productDisk8->pricing[$billing_cycle];
            }
            if (!empty($data['addon_ip'])) {
                $productAddonIp = $this->product->find($data['addon_ip']);
                $total += $productAddonIp->pricing[$billing_cycle];
            }
            $product->amount = $total;
            // cấu hình
            $config = '';
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            $config = 'CPU: ' . $cpu . ' (' . $cores . ')<br>RAM: ' . $ram . '<br>DISK: ' . $disk;
            $product->config = $config;
            $product->second = !empty($product->product_drive->second) ? 1 : 0;
            $dataResponse['type_product'] = 'Server';
            $dataResponse['product'] = $product;
            // $data['addon_disk'] = $product_addon_disk;
            // $data['addon_ram'] = $product_addon_ram;
            // $data['addon_ip'] = $product_addon_ip;
            return $dataResponse;
        } elseif (
            $product->type_product == 'Colocation'  || $product->type_product == 'Hosting'

        ) {
         
           
            $billing_cycle = $data['billing_cycle'];
            $total = !empty($billingDashBoard[$data['billing_cycle']]) ? $billingDashBoard[$data['billing_cycle']] : 0;
            if (!empty($data['addon_ip'])) {
                $productAddonIp = $this->product->find($data['addon_ip']);
                $total += $productAddonIp->pricing[$billing_cycle];
            }
            $product->amount = $total;
            return $product;
        }
    }

    public function list_state()
    {
        return $this->state->where('location', 'us')->get();
    }

    public function create_state($data)
    {
        $data_create = [
            'name' => $data['name'],
            'id_state' => $data['id_state'],
            'hidden' => !empty($data['hidden']) ? true : false,
            'location' => !empty($data['location']) ? $data['location'] : 'us',
        ];
        $create = $this->state->create($data_create);
        if ($create) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/State',
                'description' => ' Bang ' . $create->name,
            ];
            $this->log_activity->create($data_log);
            return true;
        } else {
            return false;
        }
    }

    public function update_state($data)
    {
        $state = $this->state->find($data['id']);
        $data_create = [
            'name' => $data['name'],
            'id_state' => $data['id_state'],
            'hidden' => !empty($data['hidden']) ? true : false,
        ];
        $update = $state->update($data_create);
        if ($update) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'chỉnh sửa',
                'model' => 'Admin/State',
                'description' => ' Bang ' . $state->name,
            ];
            $this->log_activity->create($data_log);
            return true;
        } else {
            return false;
        }
    }

    public function delete_state($data)
    {
        $state = $this->state->find($data['id']);
        if (isset($state)) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'xóa',
                'model' => 'Admin/State',
                'description' => ' Bang ' . $state->name,
            ];
            $this->log_activity->create($data_log);
            return $state->delete();
        } else {
            return false;
        }
    }

    public function on_state($data)
    {
        $state = $this->state->find($data['id']);
        try {
            if (isset($state)) {
                $state->hidden = false;
                $state->save();
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'bật',
                    'model' => 'Admin/State',
                    'description' => ' Bang ' . $state->name,
                ];
                $this->log_activity->create($data_log);
                return ['error' => 0];
            } else {
                return ['error' => 1];
            }
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            return ['error' => 1];
        }
    }

    public function off_state($data)
    {
        $state = $this->state->find($data['id']);
        try {
            if (isset($state)) {
                $state->hidden = true;
                $state->save();
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'tắt',
                    'model' => 'Admin/State',
                    'description' => ' Bang ' . $state->name,
                ];
                $this->log_activity->create($data_log);
                return ['error' => 0];
            } else {
                return ['error' => 1];
            }
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            return ['error' => 1];
        }
    }

    public function detail_state($id)
    {
        return $this->state->find($id);
    }

    public function list_add_on_disk_server($userId)
    {
        $user = $this->user->find($userId);
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->meta_product->type_addon == 'addon_disk') {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    // ADDON RAM
    public function list_add_on_ram_server($userId)
    {
        $user = $this->user->find($userId);
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->meta_product->type_addon == 'addon_ram') {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }
    // ADDON IP
    public function list_add_on_ip_server($userId)
    {
        $user = $this->user->find($userId);
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ($product->meta_product->type_addon == 'addon_ip') {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    public function list_state_cloudzone()
    {
        return $this->state->where('location', 'cloudzone')->get();
    }

    public function list_state_proxy()
    {
        return $this->state_proxy->get();
    }

    public function create_state_proxy($data)
    {
        $data_create = [
            'name' => $data['name'],
            'id_state' => $data['id_state'],
            'hidden' => !empty($data['hidden']) ? true : false,
        ];
        $create = $this->state_proxy->create($data_create);
        if ($create) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/StateProxy',
                'description' => ' Bang ' . $create->name,
            ];
            $this->log_activity->create($data_log);
            return true;
        } else {
            return false;
        }
    }
    public function update_state_proxy($data)
    {
        $state = $this->state_proxy->find($data['id']);
        $data_create = [
            'name' => $data['name'],
            'id_state' => $data['id_state'],
            'hidden' => !empty($data['hidden']) ? true : false,
        ];
        $update = $state->update($data_create);
        if ($update) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'chỉnh sửa',
                'model' => 'Admin/StateProxy',
                'description' => ' Bang ' . $state->name,
            ];
            $this->log_activity->create($data_log);
            return true;
        } else {
            return false;
        }
    }

    public function delete_state_proxy($data)
    {
        $state = $this->state_proxy->find($data['id']);
        if (isset($state)) {
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'xóa',
                'model' => 'Admin/StateProxy',
                'description' => ' Bang ' . $state->name,
            ];
            $this->log_activity->create($data_log);
            return $state->delete();
        } else {
            return false;
        }
    }

    public function on_state_proxy($data)
    {
        $state = $this->state_proxy->find($data['id']);
        try {
            if (isset($state)) {
                $state->hidden = false;
                $state->save();
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'bật',
                    'model' => 'Admin/StateProxy',
                    'description' => ' Bang ' . $state->name,
                ];
                $this->log_activity->create($data_log);
                return ['error' => 0];
            } else {
                return ['error' => 1];
            }
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            return ['error' => 1];
        }
    }

    public function off_state_proxy($data)
    {
        $state = $this->state_proxy->find($data['id']);
        try {
            if (isset($state)) {
                $state->hidden = true;
                $state->save();
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'tắt',
                    'model' => 'Admin/StateProxy',
                    'description' => ' Bang ' . $state->name,
                ];
                $this->log_activity->create($data_log);
                return ['error' => 0];
            } else {
                return ['error' => 1];
            }
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            return ['error' => 1];
        }
    }

    public function detail_state_proxy($id)
    {
        return $this->state_proxy->find($id);
    }
}
