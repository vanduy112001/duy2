<?php
namespace App\Repositories\Api\Admin;

use Hash;
use App\Model\User;
use App\Model\GroupUser;
use App\Model\Credit;
use App\Model\UserMeta;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;
use http\Env\Request;
use App\Model\LogActivity;
use App\Model\SendMail;
use App\Model\HistoryPay;
use App\Model\LogPayment;
use App\Model\Order;
use App\Model\GroupProduct;
use App\Http\Requests\AdminPayMentValidate;

use App\Factories\AdminFactories;

class ApiUserRepository {

    protected $user;
    protected $groupUser;
    protected $group_product;
    protected $userMeta;
    protected $credit;
    protected $log_activity;
    protected $send_mail;
    protected $history_pay;
    protected $log_payment;
    protected $hostingRepository;
    protected $vpsRepository;
    protected $payment;
    protected $order;

    public function __construct()
    {
        $this->user = new User;
        $this->groupUser = new GroupUser;
        $this->group_product = new GroupProduct;
        $this->userMeta = new UserMeta;
        $this->credit = new Credit;
        $this->log_activity = new LogActivity();
        $this->send_mail = new SendMail;
        $this->history_pay = new HistoryPay;
        $this->log_payment = new LogPayment;
        $this->order = new Order;

        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->hostingRepository = AdminFactories::hostingRepositories();
        $this->vpsRepository = AdminFactories::vpsRepositories();
        $this->payment = AdminFactories::paymentRepositories();
    }
/**USER*/
    public function listUser($q)
    {
        // ->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%')
        $listUser = $this->user->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%')->orderBy('id', 'desc')->paginate(20);
        $data = [ 'data' => [] ];
        foreach ($listUser as $key => $user) {
            $user->text_credit = number_format( $user->credit->value ,0,",",".");
            $user->text_total = number_format( $user->credit->total ,0,",",".");
            $user->phone = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';
            $user->avatar = !empty($user->user_meta->avatar) ?  $user->user_meta->avatar : url('images/avatar5.png');
            if ( $user->group_user_id ) {
                $user->text_group_user = $user->group_user->name;
            } else {
                $user->text_group_user = 'Cơ bản';
            }
            $data['data'][] = $user;
        }
        $data['total'] = $listUser->total();
        $data['perPage'] = $listUser->perPage();
        $data['current_page'] = $listUser->currentPage();
        return $data;
    }

    public function createUser($data)
    {
        try {
            $data_user = [
                'email' => $data['email'],
                'name' => $data['name'],
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => $data['password'],
                'group_user_id' => $data['group_user_id'],
                'enterprise' => !empty( $data['enterprise'] ) ? true : false,
            ];
            $user = $this->user->create($data_user);

            $data_user_meta = [
                'user_id' => $user->id,
                'role' => 'user',
                'phone' => $data['phone'],
                'affiliate' => sha1(time()),
                'gender' => $data['gender'],
                'user_create' => Auth::user()->id,
                'address' => $data['address'],
                'company' => $data['company'],
                'mst' => !empty($data['mst']) ? $data['mst'] : null,
            ];
            $this->userMeta->create($data_user_meta);

            $data_credit = [
                'user_id' => $user->id,
                'total' => '0',
                'value' => '0',
            ];
            $this->credit->create($data_credit);

            $this->dashboard->sendUserDashBoard($user);

            $data_send_mail_user = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $data_user['password'],
                'subject' => 'Đăng ký thành công tài khoản Cloudzone Portal - Cloudzone.vn'
            ];
            $data_tb_send_mail = [
              'type' => 'mail_create_user',
              'user_id' => $user->id,
              'status' => false,
              'content' => serialize($data_send_mail_user),
            ];
            $this->send_mail->create($data_tb_send_mail);
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'tạo',
              'model' => 'Admin/Mobile/User',
              'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> ',
            ];
            $this->log_activity->create($data_log);

            return [
                'error' => 0,
                'message' => 'Tạo khách hàng thành công',
            ];
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Tạo khách hàng thất bại',
            ];
        }

    }

    public function detailUser($userId)
    {
        try {
            $user =  $this->user->find($userId);
            if ( isset($user) ) {
                $user->phone = $user->user_meta->phone;
                $user->gender = $user->user_meta->gender;
                $user->address = $user->user_meta->address;
                $user->phone = $user->user_meta->phone;
                $user->company = $user->user_meta->company;
                $user->mst = $user->user_meta->mst;
                $user->cmnd = $user->user_meta->cmnd;
                $user->avatar = !empty($user->user_meta->avatar) ? $user->user_meta->avatar : 'https://portal.cloudzone.vn/images/avatar5.png';
                $user->text_created_at = date( 'd-m-Y H:i:s', strtotime($user->created_at) );
                $user->text_credit = number_format( $user->credit->value ,0,",",".");
                $user->text_total = number_format( $user->credit->total ,0,",",".");
                $user->credit_value = $user->credit->value;
                $user->credit_total = $user->credit->total;
                if ( $user->group_user_id == 0 ) {
                    $user->text_group_user = 'Cơ bản';
                } else {
                    $user->text_group_user = !empty($user->group_user->name) ? $user->group_user->name : 'Cơ bản';
                }
                // Tổng
                $total_vps = $this->vpsRepository->totalVPSByUser($user->id);
                $total_hosting = $this->hostingRepository->totalByUser($user->id);
                // sử dụng
                $hostingsUsed = $this->hostingRepository->countUsed($user->id);
                $vpsListUsed = $this->vpsRepository->countUsed($user->id);
                // hết hạn
                $hostingsExpire = $this->hostingRepository->countExpire($user->id);
                $vpsExpire = $this->vpsRepository->countExpire($user->id);
                // xóa/hủy/chuyển
                $hostingDelete = $this->hostingRepository->countDelete($user->id);
                $vpsDelete = $this->vpsRepository->countDelete($user->id);

                $user->totalService = $total_vps + $total_hosting;
                $user->totalServiceUsed = $hostingsUsed + $vpsListUsed;
                $user->totalServiceExpire = $hostingsExpire + $vpsExpire;
                $user->totalServiceDelete = $hostingDelete + $vpsDelete;

                return [
                    'error' => 0,
                    'message' => 'Lấy thông tin khách hàng thành công',
                    'user' => $user
                ];
            } else {
                return [
                    'error' => 1,
                    'message' => 'Khách hàng không tồn tại',
                ];
            }
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Lấy thông tin khách hàng thất bại',
            ];
        }
    }

    public function detail($userId)
    {
        return $this->user->find($userId);
    }

    public function editUser($data)
    {
        try {
            $user = $this->detail($data['userId']);
            if (isset($user)) {
                $data_user = [
                    // 'email' => $data['email'],
                    'name' => $data['name'],
                    // 'password' => $data['password'],
                    'group_user_id' => $data['group_user_id'],
                    'enterprise' => !empty( $data['enterprise'] ) ? true : false,
                ];
                if ( $data['changePassword'] ) {
                    $data_user['password'] = $data['password'];
                }
                // ghi log
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'chỉnh sửa',
                  'model' => 'Admin/Mobile/User',
                  'description' => ' khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> ',
                ];
                $this->log_activity->create($data_log);

                $user->update($data_user);
                if ( !empty($data['password']) ) {
                    $user->password = $data['password'];
                    $user->save();
                }

                $data_user_meta = [
                    'phone' => $data['phone'],
                    'gender' => $data['gender'],
                    'address' => $data['address'],
                    'company' => $data['company'],
                    'mst' => !empty($data['mst']) ? $data['mst'] : null,
                ];
                $user->user_meta->update($data_user_meta);

                return [
                    'error' => 0,
                    'message' => 'Chỉnh sửa khách hàng thành công',
                ];
            } else {
                return [
                    'error' => 2,
                    'message' => 'Khách hàng không tồn tại',
                ];
            }
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Chỉnh sửa khách hàng thất bại',
            ];
        }

    }

    public function updateCredit($data)
    {
        try {
            $user = $this->detail($data['userId']);
            if ( isset($user) ) {
                $money = !empty($data['amount']) ? $data['amount'] : 0;
                $money = str_replace( '.', '', $money );
                $money = str_replace( ',', '', $money );

                if ( $data['action'] == 'plus' ) {
                    $data_history = [
                      'user_id' => $user->id,
                      'ma_gd' => 'ADPL' . strtoupper(substr(sha1(time()), 34, 39)),
                      'discription' => 'Cộng tiền vào tài khoản',
                      'type_gd' => '98',
                      'method_gd' => 'credit',
                      'date_gd' => date('Y-m-d'),
                      'money'=> $money,
                      'status' => 'Active',
                    ];
                    $create = $this->history_pay->create($data_history);

                    $credit = $user->credit;
                    $data_log_payment = [
                        'history_pay_id' => $create->id,
                        'before' => $credit->value,
                        'after' => $credit->value + $money,
                      ];
                      $this->log_payment->create($data_log_payment);

                      $credit->value += $money;
                      $credit->total += $money;
                      $credit->save();

                       // Tạo log
                        $data_log = [
                         'user_id' => $user->id,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/Mobile/UpdateCredit',
                         'description' => ' cộng ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a>' ,
                         'description_user' => ' cộng ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản',
                       ];
                       $this->log_activity->create($data_log);
                       return [
                            'error' => 0,
                            'message' => 'Cộng tiền vào tài khoản khách hàng thành công',
                        ];
                } else {
                    $data_history = [
                      'user_id' => $user->id,
                      'ma_gd' => 'ADMI' . strtoupper(substr(sha1(time()), 34, 39)),
                      'discription' => 'Trừ tiền vào tài khoản',
                      'type_gd' => '98',
                      'method_gd' => 'credit',
                      'date_gd' => date('Y-m-d'),
                      'money'=> $money,
                      'status' => 'Active',
                    ];
                    $create = $this->history_pay->create($data_history);

                    $credit = $user->credit;
                    $data_log_payment = [
                        'history_pay_id' => $create->id,
                        'before' => $credit->value,
                        'after' => $credit->value - $money,
                      ];
                      $this->log_payment->create($data_log_payment);

                      $credit->value -= $money;
                      $credit->total -= $money;
                      $credit->save();
                      // Tạo log
                        $data_log = [
                         'user_id' => $user->id,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/Mobile/UpdateCredit',
                         'description' => ' trừ ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a>' ,
                         'description_user' => ' trừ ' . number_format($money,0,",",".") . ' VNĐ vào số dư tài khoản',
                       ];
                       $this->log_activity->create($data_log);
                       return [
                            'error' => 0,
                            'message' => 'Trừ tiền trong tài khoản khách hàng thành công',
                        ];
                }
            } else {
                return [
                    'error' => 2,
                    'message' => 'Khách hàng không tồn tại',
                ];
            }
        } catch (Exception $e) {
            report($e, $data);
            return [
                'error' => 1,
                'message' => 'Chỉnh sửa khách hàng thất bại',
            ];
        }
    }

    public function historyPayment($userId)
    {
        $payments = $this->payment->history_pay_with_user($userId);
        $data = ['data' => []];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
            if ( $payment->method_gd == 'invoice' ) {
                $payment->text_method_gd = 'SDTK';
            } else {
                $payment->text_method_gd = 'Vietcombank';
            }
            if ($payment->type_gd == 2) {
                $payment->text_type_gd = 'Thanh toán ' . !empty($payment->detail_order->type) ? $payment->detail_order->type : "";
            } elseif ( $payment->type_gd == 3 ) {
                $payment->text_type_gd = 'Gia hạn ' . !empty($payment->detail_order->type) ? $payment->detail_order->type : "";
            } else {
                $payment->text_type_gd = !empty($type_payin[$payment->type_gd]) ? $type_payin[$payment->type_gd] : "Lỗi";
            }
            $payment->text_money = !empty($payment->money) ? number_format($payment->money,0,",",".") : 0;
            if ( !empty($payment->log_payment->before) ) {
                $payment->text_credit_before =  number_format($payment->log_payment->before,0,",",".");
            } else {
                $payment->text_credit_before = 0;
            }
            if ( !empty($payment->log_payment->after) ) {
                $payment->text_credit_after =  number_format($payment->log_payment->after,0,",",".");
            } else {
                $payment->text_credit_after = 0;
            }
            // text_date_gd
            if ( $payment->status == 'Active' ) {
              if (!empty( $payment->log_payment->created_at )) {
                $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
              } else {
                $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
              }
            } else {
              $payment->text_date_gd = '';
            }

            $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        return $data;
    }

    public function historyOrder($userId)
    {
        $orders = $this->order->where('user_id', $userId)->with('detail_orders', 'user')->orderBy('id', 'desc')->paginate(30);
        $data = ['data' => []];
        foreach ($orders as $key => $order) {
            $order->date_create = date('H:i:s d-m-Y', strtotime($order->created_at));
            $order->text_total = number_format($order->total,0,",",".");
            $order->detail_service = [];
            if ( $order->type == 'expired' ) {
                if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                        if ( $detail_order->type == 'VPS' ) {
                            $order->text_type_order = 'Gia hạn VPS';
                            $detail_service = [];
                            if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                    if ( !empty( $order_expired->vps->ip ) ) {
                                        $detail_service[] = $order_expired->vps->ip;
                                    } else {
                                        $detail_service[] = 'Đã xóa';
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'VPS-US' || $detail_order->type == 'VPS US' ) {
                            $order->text_type_order = 'Gia hạn VPS US';
                            $detail_service = [];
                            if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                    if ( !empty( $order_expired->vps->ip ) ) {
                                        $detail_service[] = $order_expired->vps->ip;
                                    } else {
                                        $detail_service[] = 'Đã xóa';
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'hosting' ) {
                            $order->text_type_order = 'Gia hạn Hosting';
                            $detail_service = [];
                            if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                    if ( !empty( $order_expired->hosting->domain ) ) {
                                        $detail_service[] = $order_expired->hosting->domain;
                                    } else {
                                        $detail_service[] = 'Đã xóa';
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'Domain' ) {
                            $order->text_type_order = 'Gia hạn Domain';
                            if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                    if (!empty($order_expired->domain->domain)) {
                                        $detail_service[] = $order_expired->domain->domain;
                                    } else {
                                        $detail_service[]= 'đã xóa';
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                    }
                }
            }
            else {
                if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                        if ( $detail_order->type == 'addon_vps' ) {
                            $order->text_type_order = 'Cấu hình thêm VPS';
                            if ( !empty($detail_order->order_addon_vps) ) {
                                $detail_service = [];
                                if ( !empty($detail_order->order_addon_vps) ) {
                                    foreach ( $detail_order->order_addon_vps as $order_addon_vps ) {
                                        $vps = $order_addon_vps->vps;
                                        if (!empty($vps->ip)) {
                                            $detail_service[] = $vps->ip;
                                        } else {
                                            $detail_service[] = 'Đã xóa';
                                        }
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'change_ip' ) {
                            $order->text_type_order = 'Đổi IP VPS';
                            $detail_service = [];
                            if ( !empty($detail_order->order_change_vps) ) {
                                foreach ( $detail_order->order_change_vps as $order_change_vps ) {
                                    if ( !empty($order_change_vps->vps_id) ) {
                                        $vps = $order_change_vps->vps;
                                        if (!empty($vps->ip)) {
                                            $detail_service[] = $vps->ip;
                                        } else {
                                            $detail_service[] = 'Đã xóa';
                                        }
                                    }
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'VPS' ) {
                            $order->text_type_order = 'Tạo VPS';
                            $detail_service = [];
                            if ( !empty($detail_order->vps) ) {
                                foreach ( $detail_order->vps as $vps ) {
                                    $detail_service[] = $vps->ip;
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'VPS-US' || $detail_order->type == 'VPS US' ) {
                            $order->text_type_order = 'Tạo VPS US';
                            $detail_service = [];
                            if ( !empty($detail_order->vps) ) {
                                foreach ( $detail_order->vps as $vps ) {
                                    $detail_service[] = $vps->ip;
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'NAT-VPS' ) {
                            $order->text_type_order = 'Tạo NAT VPS';
                            $detail_service = [];
                            if ( !empty($detail_order->vps) ) {
                                foreach ( $detail_order->vps as $vps ) {
                                    $detail_service[] = $vps->ip;
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'Hosting' ) {
                            $order->text_type_order = 'Tạo Hosting';
                            $detail_service = [];
                            if ( !empty($detail_order->hostings) ) {
                                foreach ( $detail_order->hostings as $hosting ) {
                                    $detail_service[] = $hosting->domain;
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'Domain' ) {
                            $order->text_type_order = 'Tạo Domain';
                            $detail_service = [];
                            if ( !empty($detail_order->domain) ) {
                                foreach ( $detail_order->domains as $domain ) {
                                    $detail_service[] = $domain->domain;
                                }
                            }
                            $order->detail_service = $detail_service;
                        }
                        elseif ( $detail_order->type == 'upgrade_hosting' ) {
                            $order->text_type_order = 'Nâng cấp Hosting';
                            $detail_service = [];
                            if ( !empty($detail_order->order_upgrade_hosting) ) {
                                $detail_service[] = $detail_order->order_upgrade_hosting->hosting;
                            }
                            $order->detail_service = $detail_service;
                        }
                    }
                }
            }
            $data['data'][] = $order;
        }
        $data['total'] = $orders->total();
        $data['perPage'] = $orders->perPage();
        $data['current_page'] = $orders->currentPage();
        return $data;
    }

    public function deleteUser($userId)
    {
        try {
            $user = $this->detail($userId);
            if ( !empty($user->user_meta) ) {
                $user->user_meta->delete();
            }
            if ( !empty($user->agency) ) {
                $user->agency->delete();
            }
            if ( !empty($user->social_account) ) {
                $user->social_account->delete();
            }
            if ( !empty($user->verify) ) {
                $user->verify->delete();
            }
            if ( !empty($user->credit) ) {
                $user->credit->delete();
            }

            $quotations = $user->quotations;
            foreach ($quotations as $key => $quotation) {
                $quotation->delete();
            }
            $history_pays = $user->history_pays;
            foreach ($history_pays as $key => $history_pay) {
                $history_pay->delete();
            }
            $orders = $user->orders;
            foreach ($orders as $key => $order) {
                $order->delete();
            }
            $list_vps = $user->vps;
            foreach ($list_vps as $key => $vps) {
                $vps->delete();
            }
            $hostings = $user->hostings;
            foreach ($hostings as $key => $hosting) {
                $hosting->delete();
            }
            $detail_orders = $user->detail_orders;
            foreach ($detail_orders as $key => $detail_order) {
                $detail_order->delete();
            }
            $log_activities = $user->log_activities;
            foreach ($log_activities as $key => $log_activity) {
                $log_activity->delete();
            }
            $send_mails = $user->send_mails;
            foreach ($send_mails as $key => $send_mail) {
                $send_mail->delete();
            }
            // Tạo log
            $data_log = [
               'user_id' => Auth::id(),
               'action' => 'Xóa',
               'model' => 'Admin/Mobile/User',
               'description' => ' tài khoản ' . $user->name . ' - ' . $user->email,
            ];
            $this->log_activity->create($data_log);

            $user->delete();
            return [
                'error' => 0,
                'message' => 'Xóa khách hàng thành công',
            ];
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Xóa khách hàng thất bại',
            ];
        }
    }
/**GROUP USER*/    
    public function listGroupUser()
    {
        $groupUser = $this->groupUser->orderBy('id', 'desc')->get();
        return $groupUser;
    }

    public function getlistGroupUser()
    {
        $groupUsers = $this->groupUser->orderBy('id', 'desc')->get();
        $data = [];
        $data[] = [
            'id' => 0,
            'name' => 'Cơ bản',
            'countUser' => $this->user->where('group_user_id', 0)->count(),
            'countGroupProduct' => $this->group_product->where('group_user_id', 0)->count(),
        ];
        foreach ($groupUsers as $key => $groupUser) {
            $groupUser->countUser = $this->user->where('group_user_id', $groupUser->id)->count();
            $groupUser->countGroupProduct = $this->group_product->where('group_user_id', $groupUser->id)->count();
            $data[] = $groupUser;
        }
        return $data;
    }

    public function createGroupUser($request)
    {
        try {
            $create = $this->groupUser->create([ 'name' => $request['name'] ]);
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'tạo',
              'model' => 'Admin/Mobile/GroupUser',
              'description' => ' đại lý <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $create->id . '">' . $create->name . '</a> ',
            ];
            $this->log_activity->create($data_log);

            return [
                'error' => 0,
                'message' => 'Tạo đại lý thành công',
            ];

        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Tạo đại lý thất bại',
            ];
        }
    }

    public function editGroupUser($request)
    {
        try {
            $groupUser = $this->groupUser->find($request['groupUserId']);
            if ( isset($groupUser) ) {
                $groupUser->name = $request['name'];
                $groupUser->save();
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'chỉnh sửa',
                  'model' => 'Admin/Mobile/GroupUser',
                  'description' => ' đại lý <a target="_blank" href="/admin/dai-ly/chi-tiet-dai-ly/' . $groupUser->id . '">' . $groupUser->name . '</a> ',
                ];
                $this->log_activity->create($data_log);

                return [
                    'error' => 0,
                    'message' => 'Chỉnh sửa đại lý thành công',
                ];
            } else {
                return [
                    'error' => 1,
                    'message' => 'Đại lý không tồn tại',
                ];
            }

        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Chỉnh sửa đại lý thất bại',
            ];
        }
    }

    public function detailGroupUser($groupUserId)
    {
        $groupUser = $this->groupUser->find($groupUserId);
        if ( isset($groupUser) ) {
            return [
                'error' => 0,
                'message' => 'Lấy chi tiết đại lý thành công',
                'groupUser' => $groupUser
            ];
        } else {
            return [
                'error' => 1,
                'message' => 'Đại lý không tồn tại',
            ];
        }
    }

    public function listUserByGroupUser($q, $groupUserId)
    {
        try {
            $listUser = $this->user->where('group_user_id', $groupUserId)->where('name', 'like', '%'.$q.'%')->orderBy('id', 'desc')->paginate(25);
            $data = [ 'data' => [] ];
            foreach ($listUser as $key => $user) {
                $user->text_credit = number_format( $user->credit->value ,0,",",".");
                $user->text_total = number_format( $user->credit->total ,0,",",".");
                $user->phone = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';
                $user->avatar = !empty($user->user_meta->avatar) ?  $user->user_meta->avatar : url('images/avatar5.png');
                if ( $user->group_user_id ) {
                    $user->text_group_user = $user->group_user->name;
                } else {
                    $user->text_group_user = 'Cơ bản';
                }
                $data['data'][] = $user;
            }
            $data['total'] = $listUser->total();
            $data['perPage'] = $listUser->perPage();
            $data['current_page'] = $listUser->currentPage();
            return $data;
        } catch (Exception $e) {
            report($e);
            return [];
        }
    }

    public function listGroupProductByGroupUser($value='')
    {
        try {
            $group_product = $this->group_product->where('group_user_id', $groupUserId)->paginate(25);
            return [
                'error' => 0,
                'message' => 'Hành động thành công',
                'listGroupProduct' => $group_product
            ];
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Truy vấn lỗi',
            ];
        }
    }

}