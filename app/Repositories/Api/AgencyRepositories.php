<?php
namespace App\Repositories\Api;

use Hash;
use App\Model\User;
use App\Model\Agency;
use App\Model\GroupProduct;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\VpsOs;

class AgencyRepositories {

  protected $user;
  protected $agency;
  protected $group_product;
  protected $meta_product;
  protected $pricing;
  protected $vps_os;

  public function __construct()
  {
    $this->user = new User;
    $this->agency = new Agency;
    $this->group_product = new GroupProduct;
    $this->meta_product = new MetaProduct;
    $this->pricing = new Pricing;
    $this->vps_os = new VpsOs;
  }


  public function login($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $data_restore = [];
      try {
        if ( isset($agency) ) {
            $token = $this->rand_string();
            $agency->token_api = Hash::make($token);
            $agency->save();
            $data_restore = [
              'error' => 0,
              'token' => $token,
              'status' => 'Kết nối thành công',
            ];
        }
        else {
          $data_restore = [
            'error' => 1,
            'status' => 'Kết nối thất bại',
          ];
        }
      } catch (\Exception $e) {
        report($e);
        $data_restore = [
          'error' => 2,
          'status' => 'Kết nối thất bại',
        ];
      }
      return $data_restore;

  }

  public function test_login($request, $data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $data_restore = [];
      try {
        if ( isset($agency) ) {
          $token = $request->header('Authorization');
          if (Hash::check($token , $agency->token_api))
          {
              $data_restore = [
                'error' => 0,
                'status' => 'Kết nối thành công',
              ];
          }
          else {
              $data_restore = [
                'error' => 2,
                'status' => 'Lỗi token không đúng',
              ];
          }
        }
        else {
            $data_restore = [
              'error' => 1,
              'status' => 'Lỗi tài khoản hoặc mật khẩu không đúng',
            ];
        }
      } catch (\Exception $e) {
        report($e);
        $data_restore = [
          'error' => 2,
          'status' => 'Lỗi kết nối thất bại',
        ];
      }
      return $data_restore;
  }

  public function check_credit($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $credit = $agency->user->credit;
      $data_restore = [
        'error' => 0,
        'credit' => !empty($credit->value) ? $credit->value : 0,
      ];
      return $data_restore;
  }

  // get meta group product
  public function get_group_product_private($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $user = $this->user->find($agency->user_id);
      $data_group_product = [];
      // dd($user);
      if ($user->group_user_id != 0) {
          $group_user = $user->group_user;
          $group_products = $group_user->group_products;
          foreach ($group_products as $key => $group_product) {
            if (empty($group_product->hidden)) {
              if ($group_product->type == 'vps' || $group_product->type == 'hosting') {
                  $data_product = [];
                  if ( !empty($group_product->products) ) {
                      $products = $group_product->products;
                      foreach ($products as $key => $product) {
                        $meta_product = $this->meta_product->where('product_id', $product->id )->first();
                        $config = [];
                        if ( $group_product->type == 'vps') {
                            $config = [
                              'ip' => $meta_product->ip,
                              'cpu' => $meta_product->cpu,
                              'ram' => $meta_product->memory,
                              'disk' => $meta_product->disk,
                              'os' => $meta_product->os,
                            ];
                        } else {
                            $config = [
                              'domain' => $meta_product->domain,
                              'storage' => $meta_product->storage,
                              'sub_domain' => $meta_product->sub_domain,
                              'database' => $meta_product->database,
                            ];
                        }
                        $product->config = $config;
                        $data_pricing = [];
                        $pricing = $this->pricing->where('product_id', $product->id )->first();
                        if ( isset($pricing) ) {
                            if ($pricing->type == 'recurring') {
                                $data_pricing = [
                                  'type' => 'recurring',
                                ];
                                if ( $pricing->monthly > 0 ) {
                                  $data_pricing['monthly'] = $pricing->monthly;
                                }
                                if ( $pricing->twomonthly > 0 ) {
                                  $data_pricing['twomonthly'] = $pricing->twomonthly;
                                }
                                if ( $pricing->quarterly > 0 ) {
                                  $data_pricing['quarterly'] = $pricing->quarterly;
                                }
                                if ( $pricing->semi_annually > 0 ) {
                                  $data_pricing['semi_annually'] = $pricing->semi_annually;
                                }
                                if ( $pricing->annually > 0 ) {
                                  $data_pricing['annually'] = $pricing->annually;
                                }
                                if ( $pricing->biennially > 0 ) {
                                  $data_pricing['biennially'] = $pricing->biennially;
                                }
                                if ( $pricing->triennially > 0 ) {
                                  $data_pricing['triennially'] = $pricing->triennially;
                                }
                            }
                            elseif ( $pricing->type == 'one_time' ) {
                                $data_pricing = [
                                  'type' => 'one_time',
                                  '1 Lan' => $pricing->one_time_pay,
                                ];
                            }
                            else {
                                $data_pricing = [
                                  'type' => 'free',
                                  'pricing' => 0,
                                ];
                            }
                        }
                        $product->pricings = $data_pricing;
                        $data_product[] = $product;
                      }
                      $group_product->products = $data_product;
                  }
                  $data_group_product[] = $group_product;
              }
            }
          }
      } else {
          $group_products = $this->group_product->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
          foreach ($group_products as $key => $group_product) {
            if (empty($group_product->hidden)) {
              if ($group_product->type == 'vps' || $group_product->type == 'hosting') {
                  $data_product = [];
                  if ( !empty($group_product->products) ) {
                      $products = $group_product->products;
                      foreach ($products as $key => $product) {
                        $meta_product = $this->meta_product->where('product_id', $product->id )->first();
                        $config = [];
                        if ( $group_product->type == 'vps') {
                            $config = [
                              'ip' => $meta_product->ip,
                              'cpu' => $meta_product->cpu,
                              'ram' => $meta_product->memory,
                              'disk' => $meta_product->disk,
                              'os' => $meta_product->os,
                            ];
                        } else {
                            $config = [
                              'domain' => $meta_product->domain,
                              'storage' => $meta_product->storage,
                              'sub_domain' => $meta_product->sub_domain,
                              'database' => $meta_product->database,
                            ];
                        }
                        $product->config = $config;
                        $data_pricing = [];
                        $pricing = $this->pricing->where('product_id', $product->id )->first();
                        if ( isset($pricing) ) {
                            if ($pricing->type == 'recurring') {
                                $data_pricing = [
                                  'type' => 'recurring',
                                ];
                                if ( $pricing->monthly > 0 ) {
                                  $data_pricing['monthly'] = $pricing->monthly;
                                }
                                if ( $pricing->twomonthly > 0 ) {
                                  $data_pricing['twomonthly'] = $pricing->twomonthly;
                                }
                                if ( $pricing->quarterly > 0 ) {
                                  $data_pricing['quarterly'] = $pricing->quarterly;
                                }
                                if ( $pricing->semi_annually > 0 ) {
                                  $data_pricing['semi_annually'] = $pricing->semi_annually;
                                }
                                if ( $pricing->annually > 0 ) {
                                  $data_pricing['annually'] = $pricing->annually;
                                }
                                if ( $pricing->biennially > 0 ) {
                                  $data_pricing['biennially'] = $pricing->biennially;
                                }
                                if ( $pricing->triennially > 0 ) {
                                  $data_pricing['triennially'] = $pricing->triennially;
                                }
                            }
                            elseif ( $pricing->type == 'one_time' ) {
                                $data_pricing = [
                                  'type' => 'one_time',
                                  '1 Lan' => $pricing->one_time_pay,
                                ];
                            }
                            else {
                                $data_pricing = [
                                  'type' => 'free',
                                  'pricing' => 0,
                                ];
                            }
                        }
                        $product->pricings = $data_pricing;
                        $data_product[] = $product;
                      }
                      $group_product->products = $data_product;
                  }
                  $data_group_product[] = $group_product;
              }
            }
          }
      }
      $data_restore = [
        'error' => 0,
        'group_product' => $data_group_product,
      ];
      return $data_restore;
  }

  function rand_string() {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
      $size = strlen( $chars );
      $str = '';
      for( $i = 0; $i < 32; $i++ ) {
          $str .= $chars[ rand( 0, $size - 1 ) ];
      }
      return $str;
  }

}

?>
