<?php
namespace App\Repositories\User;

use App\Model\Contract;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\Pricing;
use App\Model\ContractDetail;
use App\Model\Vps;
use App\Model\Domain;
use App\Model\Server;
use App\Model\Hosting;
// use App\Factories\AdminFactories;
use Carbon\Carbon;
use Auth;

class ContractRepositories {

    protected $user;
    protected $contract;
    protected $contract_detail;
    protected $user_meta;
    // protected $product;
    protected $pricing;
    protected $vps;
    protected $domain;
    protected $server;
    protected $hosting;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->contract = new Contract;
        // $this->product = AdminFactories::productRepositories();
        $this->pricing = new Pricing;
        $this->contract_detail = new ContractDetail;
        $this->vps = new Vps;
        $this->domain = new Domain;
        $this->server = new Server;
        $this->hosting = new Hosting;
    }

    public function get_all() 
    {
        $user_id = Auth::id();
        $contracts = $this->contract->where('user_id', $user_id)->orderBy('id', 'DESC')->paginate(20);
        return $contracts;
    }

    public function create_data_contract($user_detail, $data)
    {
        $data_contract = [
            'title' => '', 
            'user_id' => $user_detail->id,
            'contract_number' => '',
            // 'contract_number' => $this->create_contract_number($user_detail->name) . '-' . $this->random_char(5),
            'company' => $data['company'] ? $data['company'] : '', 
            'deputy' => $data['deputy'] ? $data['deputy'] : '', 
            'deputy_position' => $data['deputy_position'] ? $data['deputy_position'] : '', 
            'address' => $data['address'] ? $data['address'] : '', 
            'phone' => $data['phone'] ? $data['phone'] : '', 
            'email' => $data['email'] ? $data['email'] : '', 
            'mst' => $data['mst'] ? $data['mst'] : '',
            'date_create' => date('Y-m-d'),
            'date_end' => null,
            // 'status' => 'pending'
        ];
        $create_contract = $this->contract->create($data_contract);
        return $create_contract;
    }

    public function create_contract_number($fullname)
    {
        // $fullname = trim($fullname);
        // $lastname = substr($fullname, strpos($fullname, ' '), strlen($fullname));
        // return $lastname;
        $parts = explode(' ', trim($fullname));
        $num = count($parts);
        if($num > 1)
        {
            return $lastname = array_pop($parts);
        }
        else
        {
            $lastname = '';
        }
    }

    public function random_char($length) {
        $result           = '';
        $characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $charactersLength = strlen($characters);
        for ($i = 0; $i < $length; $i++)
            $result .= $characters[mt_rand(0, $charactersLength)];
        return $result;
    }

    public function get_contract($id)
    {
        $user_id = Auth::user()->id;
        $contract = $this->contract->where('user_id', $user_id)->with('contract_details')->find($id);
        return $contract;
    }

    public function get_target_from_user($data)
    {
        $targets = [];
        $product_type = $data['product_type'];
        $user_id = Auth::user()->id;
        if ($product_type === 'vps') {
            $targets = $this->vps->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        } 
        elseif ($product_type === 'domain') {
            $targets = $this->domain->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'server') {
            $targets = $this->server->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        elseif ($product_type === 'hosting') {
            $targets = $this->hosting->where('user_id', $user_id)->orderBy('id', 'DESC')->get();
        }
        return $targets;
    }

    public function get_target_detail($data)
    {
        $target_detail = [];
        $product_type = $data['product_type'];
        $target_id = $data['target_id'];
        $user_id = Auth::user()->id;
        if ($product_type === 'vps') {
            $target_detail = $this->vps->where('user_id', $user_id)->with('detail_order')->find($target_id);
        } 
        elseif ($product_type === 'domain') {
            $target_detail = $this->domain->where('user_id', $user_id)->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'server') {
            $target_detail = $this->server->where('user_id', $user_id)->with('detail_order')->find($target_id);
        }
        elseif ($product_type === 'hosting') {
            $target_detail = $this->hosting->where('user_id', $user_id)->with('detail_order')->find($target_id);
        }
        return $target_detail;
    }

    public function create_contract_detail($data_contract_detail)
    {
        $result = [
            'status' => false,
            'message' => 'Tạo sản phẩm cho hợp đồng thất bại, vui lòng thử lại'
        ];
        $data = [
            'product_type' => $data_contract_detail['add_product_type'],
            'target_id' => $data_contract_detail['add_target_id'],
        ];
        $target_detail = $this->get_target_detail($data);

        $create_data = [
            'contract_id' => $data_contract_detail['add_contract_id'] ? $data_contract_detail['add_contract_id'] : '',
            'billing_cycle' => $target_detail->billing_cycle, 
            'amount' => $target_detail->detail_order->sub_total,
            'qtt' => 1, 
            'date_create' => null, 
            'date_end' => $target_detail->next_due_date,
            'note' => '', 
            'product_type' => $data_contract_detail['add_product_type'] ? $data_contract_detail['add_product_type'] : '',
            'target_id' => $data_contract_detail['add_target_id'] ? $data_contract_detail['add_target_id'] : '',
        ];
        $create = $this->contract_detail->create($create_data);
        if ($create) {
            $result = [
                'status' => true,
                'message' => 'Tạo sản phẩm cho hợp đồng thành công'
            ];
        }
        return $result;
    }


    public function update($id, $data)
    {
        $result = [
            'status' => false,
            'message' => 'Cập nhập đồng thất bại, vui lòng thử lại'
        ];
        $update_data = [
            'company' => $data['company'] ? $data['company'] : '', 
            'deputy' => $data['deputy'] ? $data['deputy'] : '', 
            'deputy_position' => $data['deputy_position'] ? $data['deputy_position'] : '', 
            'address' => $data['address'] ? $data['address'] : '', 
            'phone' => $data['phone'] ? $data['phone'] : '', 
            'email' => $data['email'] ? $data['email'] : '', 
            'mst' => $data['mst'] ? $data['mst'] : '', 
        ];
        $update = $this->contract->find($id)->update($update_data);
        if ($update) {
            $result = [
              'status' => true,
              'message' => 'Cập nhập thành công.'
            ];
        }
        return $result;
    }

    public function check_latest_contract_status($user_id)
    {
        $contract = $this->contract->where('user_id', $user_id)->orderBy('id', 'desc')->first();
        if ($contract && $contract->status == 'pending') {
            return false;
        } else {
            return true;
        }
    }

    public function get_contract_with_id($id)
    {
        $user_id = Auth::user()->id;
        $contract = $this->contract->where('user_id', $user_id)->find($id);
        return $contract;
    }

    public function get_contract_details($id)
    {
        $contract_details = $this->contract_detail->where('contract_id', $id)->orderBy('date_end', 'DESC')->get();
        return $contract_details;
    }

}