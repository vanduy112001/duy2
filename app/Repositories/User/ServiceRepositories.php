<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\EmailHosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use App\Model\HistoryPay;
use App\Model\OrderAddonVps;
use App\Model\OrderExpired;
use App\Model\GroupUser;
use App\Model\LogActivity;
use App\Model\Domain;
use App\Model\VpsConfig;
use App\Model\SendMail;
use Mail;
use App\Factories\AdminFactories;
use Carbon\Carbon;
use App\Model\Customer;
use App\Model\OrderChangeVps;
use App\Model\ProductUpgrate;
use App\Model\OrderUpgradeHosting;
use App\Model\Colocation;
use App\Model\LogPayment;
use App\Model\Proxy;
use Illuminate\Support\Facades\Log;

class ServiceRepositories {

    protected $user;
    protected $user_meta;
    protected $verify;
    protected $email;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $order_addon_vps;
    protected $server;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $order_vetify;
    protected $credit;
    protected $history_pay;
    protected $order_expired;
    protected $group_user;
    protected $customer;
    protected $order_change_ip;
    protected $product_upgrate;
    protected $order_upgrade_hosting;
    protected $log_activity;
    protected $domain;
    protected $colocation;
    protected $email_hosting;
    protected $vps_config;
    protected $send_mail;
    protected $log_payment;
    protected $proxy;
    // API
    protected $da;
    protected $da_si;
    protected $dashboard;

    public function __construct()
    {
        $this->user = new User;
        $this->group_user = new GroupUser;
        $this->email = new Email;
        $this->user_meta = new UserMeta;
        $this->verify = new Verify;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->order_addon_vps = new OrderAddonVps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->order_vetify = new OrderVetify;
        $this->credit = new Credit;
        $this->history_pay = new HistoryPay;
        $this->order_expired = new OrderExpired;
        $this->customer = new Customer;
        $this->order_change_ip = new OrderChangeVps;
        $this->product_upgrate = new ProductUpgrate;
        $this->order_upgrade_hosting = new OrderUpgradeHosting;
        $this->log_activity = new LogActivity();
        $this->domain = new Domain;
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
        $this->vps_config = new VpsConfig;
        $this->send_mail = new SendMail;
        $this->log_payment = new LogPayment;
        $this->proxy = new Proxy;
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
    }
    // list tất cả
    public function list_vps()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('ip', '!=', '')->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function get_config_server($serverId)
    {
        $server = $this->server->find($serverId);
        if ( !empty($server->server_config) ) {
          $text_config = '';
          $server_config = $server->server_config;
          $data_config = [];
          if ( !empty($server_config->disk2) ) {
            $data_config[] = [
              'id' => $server_config->disk2,
              'name' => $server_config->product_disk2->name,
              'qtt' => 1
            ];
          }
          if ( !empty($server_config->disk3) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk3) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk3,
                'name' => $server_config->product_disk3->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk4) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk4) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk4,
                'name' => $server_config->product_disk4->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk5) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk5) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk5,
                'name' => $server_config->product_disk5->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk6) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk6) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk6,
                'name' => $server_config->product_disk6->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk7) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk7) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk7,
                'name' => $server_config->product_disk7->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk8) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk8) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk8,
                'name' => $server_config->product_disk8->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->ram) ) {
            $add_ram = 0;
            foreach ($server->server_config_rams as $server_config_ram) {
              $add_ram += !empty($server_config_ram->product->meta_product->memory) ? $server_config_ram->product->meta_product->memory : 0;
            }
            $text_config .= 'Addon RAM: '. $add_ram .' GB RAM <br>';
          }
          if ( count($data_config) ) {
            $text_config .= 'Addon Disk: ';
            foreach ($data_config as $key => $data) {
              $text_config .= ( $data['qtt'] . ' &#215; ' . $data['name']  );
              if ( count( $data_config ) - 1 > $key ) {
                $text_config .= ' - ';
              }
            }
            $text_config .= '';
          }
          if ( !empty($server_config->ip) ) {
            $text_config .= '<br>';
            $add_ram = 0;
            foreach ($server->server_config_ips as $server_config_ip) {
              $add_ram += !empty($server_config_ip->product->meta_product->ip) ? $server_config_ip->product->meta_product->ip : 0;
            }
            $text_config .= 'Addon IP: '. $add_ram .' IP Public';
          }
          return $text_config;
        } else {
          return '';
        }
    }

    public function change_user_by_vps($data)
    {
      $user = $this->user->where('email', $data['email'])->first();
      foreach ($data['list_vps'] as $key => $id) {
        $vps = $this->vps->find($id);
        $check_dashboard = $this->dashboard->change_user($vps, $user->customer_id);
        // $check_dashboard = true;
        if ( $check_dashboard ) {
          $check_order = $this->order_change_vps_user($vps, $user);

          $data_vps = [
            'detail_order_id' => $check_order['detail_order']->id,
            'user_id' => $user->id,
            'product_id' => $check_order['product']->id,
            'next_due_date' => $vps->next_due_date,
            'ip' => !empty($vps->ip)? $vps->ip : '',
            'os' => $vps->os,
            'billing_cycle' => $vps->billing_cycle,
            'status' => 'Active',
            'date_create' => $vps->date_create,
            'user' => !empty($vps->user)? $vps->user : '',
            'password' => !empty($vps->password)? $vps->password : '',
            'paid' => 'paid',
            'addon_id' => $vps->addon_id,
            'security' => $vps->security,
            'type_vps' => $vps->type_vps,
            'vm_id' => $vps->vm_id,
            'status_vps' => $vps->status_vps,
            'location' => 'cloudzone'
          ];
          $create_vps = $this->vps->create($data_vps);

          if (!empty( $vps->vps_config )) {
            $addon_vps = $vps->vps_config;
            if ($addon_vps->cpu > 0 || $addon_vps->ram > 0 || $addon_vps->disk > 0  ) {
              $data_vps_config = [
                'vps_id' => $create_vps->id,
                'ip' => '0',
                'cpu' => $addon_vps->cpu,
                'ram' => $addon_vps->ram,
                'disk' => $addon_vps->disk,
              ];
              $this->vps_config->create($data_vps_config);
            }
          }
          # xoa vps của khách hàng yêu cầu chuyển
          $vps->status_vps = "change_user";
          $vps->vm_id = -1;
          $vps->save();
          // Tạo log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'chuyển VPS',
            'model' => 'User/Service',
            'description' => $vps->ip . ' cho khách hàng <a href="/admin/users/detail/'. $user->id .'" target="_blank">'. $user->name . ' (' . $user->email . ')</a>' ,
            'description_user' => $vps->ip . ' cho khách hàng ' . $user->name . ' (' . $user->email . ')',
            'service' =>  $vps->ip,
          ];
          $this->log_activity->create($data_log);

        } else {
          return [
            "error" => 1
          ];
        }
      }
      return [
        "error" => 0,
        "user" => $user
      ];
    }

    public function change_user_by_vps_us($data)
    {
      $user = $this->user->where('email', $data['email'])->first();
      foreach ($data['list_vps'] as $key => $id) {
        $vps = $this->vps->find($id);
        $check_dashboard = $this->dashboard->change_user($vps, $user->customer_id);
        // $check_dashboard = true;
        if ( $check_dashboard ) {
          $check_order = $this->order_change_vps_us_user($vps, $user);
          if ( $check_order ) {
            $data_vps = [
              'detail_order_id' => $check_order['detail_order']->id,
              'user_id' => $user->id,
              'product_id' => $check_order['product']->id,
              'next_due_date' => $vps->next_due_date,
              'ip' => !empty($vps->ip)? $vps->ip : '',
              'os' => $vps->os,
              'billing_cycle' => $vps->billing_cycle,
              'status' => 'Active',
              'date_create' => $vps->date_create,
              'user' => !empty($vps->user)? $vps->user : '',
              'password' => !empty($vps->password)? $vps->password : '',
              'paid' => 'paid',
              'addon_id' => $vps->addon_id,
              'security' => $vps->security,
              'type_vps' => $vps->type_vps,
              'vm_id' => $vps->vm_id,
              'status_vps' => $vps->status_vps,
              'location' => 'us',
              'state' => $vps->state,
            ];
            $create_vps = $this->vps->create($data_vps);

            # xoa vps của khách hàng yêu cầu chuyển
            $vps->status_vps = "change_user";
            $vps->vm_id = -1;
            $vps->save();
            // Tạo log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'chuyển VPS US',
                'model' => 'User/Service',
                'description' => $vps->ip . ' cho khách hàng <a href="/admin/users/detail/'. $user->id .'" target="_blank">'. $user->name . ' (' . $user->email . ')</a>' ,
                'description_user' => $vps->ip . ' cho khách hàng ' . $user->name . ' (' . $user->email . ')',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (Exception $e) {
              report($e);
            }
          } else {
            return [
              "error" => 2,
              "user" => $user
            ];
          }

        } else {
          return [
            "error" => 1,
            "user" => $user
          ];
        }
      }
      return [
        "error" => 0,
        "user" => $user
      ];
    }

    public function order_change_vps_user($vps, $user)
    {
        $product = $this->check_product_by_user($vps, $user);
        if ( $product ) {
          $billing_cycle = $vps->billing_cycle;
          $total = $product->pricing[$billing_cycle];
          // dd($vps->vps_config);
          if ( !empty($vps->vps_config) ) {
            $add_on_products = $this->get_addon_product_private($user->id);
            $addon_vps = $vps->vps_config;
            $pricing_addon = 0;
            foreach ($add_on_products as $key => $add_on_product) {
              if (!empty($add_on_product->meta_product->type_addon)) {
                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                }
                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                }
                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                }
              }
            }
            $total += $pricing_addon;
          }
          // dd($total);
          $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
          ];
          // tao order
          $data_order = [
              'user_id' => $user->id,
              'promotion_id' => null,
              'total' => $total,
              'status' => 'Finish',
              'description' => 'Chuyển khách hàng',
              'verify' => true,
          ];
          $create_order = $this->order->create($data_order);
          // detail order
          $data_order_detail = [
            'order_id' => $create_order->id,
            'type' => 'VPS',
            'due_date' => $vps->next_due_date,
            'description' => 'Chuyển khách hàng',
            'payment_method' => 1,
            'status' => 'paid',
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => $user->id,
            'paid_date' => !empty($vps->date_create) ? $vps->date_create : '2021-1-1',
            'addon_id' => !empty($vps->vps_config)? '1' : '0',
          ];
          $detail_order = $this->detail_order->create($data_order_detail);
          $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => $vps->date_create,
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
          ];
          $this->history_pay->create($data_history);
          return [
            'detail_order' => $detail_order,
            'product' => $product,
          ];
        } else {
          return null;
        }
    }

    public function order_change_vps_us_user($vps, $user)
    {
        $product = $this->check_product_vps_us_by_user($vps, $user);
        if ( $product ) {
          $billing_cycle = $vps->billing_cycle;
          $total = $product->pricing[$billing_cycle];
          // dd($vps->vps_config);
          // dd($total);
          $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
          ];
          // tao order
          $data_order = [
              'user_id' => $user->id,
              'promotion_id' => null,
              'total' => $total,
              'status' => 'Finish',
              'description' => 'Chuyển khách hàng',
              'verify' => true,
          ];
          $create_order = $this->order->create($data_order);
          // detail order
          $data_order_detail = [
            'order_id' => $create_order->id,
            'type' => 'VPS-US',
            'due_date' => $vps->next_due_date,
            'description' => 'Chuyển khách hàng',
            'payment_method' => 1,
            'status' => 'paid',
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => $user->id,
            'paid_date' => !empty($vps->date_create) ? $vps->date_create : '2021-1-1',
            'addon_id' => !empty($vps->vps_config)? '1' : '0',
          ];
          $detail_order = $this->detail_order->create($data_order_detail);
          $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODVU' . strtoupper(substr(sha1(time()), 35, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => $vps->date_create,
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
          ];
          $this->history_pay->create($data_history);
          return [
            'detail_order' => $detail_order,
            'product' => $product,
          ];
        } else {
          return null;
        }
    }

    public function check_product_by_user($vps, $user)
    {
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('type', 'vps')->where('hidden', false)->where('group_user_id', $user->group_user_id)->get();
        } else {
            $group_products = $this->group_product->where('type', 'vps')->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
        }
        $product_before = $vps->product;
        foreach ($group_products as $key => $group_product) {
          foreach ($group_product->products as $key => $product) {
            if ( $product_before->meta_product->cpu == $product->meta_product->cpu && $product_before->meta_product->memory == $product->meta_product->memory && $product_before->meta_product->disk == $product->meta_product->disk ) {
              return $product;
            }
          }
        }
        return null;
    }

    public function check_product_vps_us_by_user($vps, $user)
    {
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('type', 'vps_us')->where('hidden', false)->where('group_user_id', $user->group_user_id)->get();
        } else {
            $group_products = $this->group_product->where('type', 'vps_us')->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
        }
        $product_before = $vps->product;
        foreach ($group_products as $key => $group_product) {
          foreach ($group_product->products as $key => $product) {
            if ( $product_before->meta_product->cpu == $product->meta_product->cpu && $product_before->meta_product->memory == $product->meta_product->memory && $product_before->meta_product->disk == $product->meta_product->disk ) {
              return $product;
            }
          }
        }
        return null;
    }

    public function list_server()
    {
        return $this->server->where('user_id', Auth::user()->id)->where('ip', '!=', '')->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_hosting()
    {
        return $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ đang on
    public function list_vps_on()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                  ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ còn hạn
    public function list_vps_use()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                  ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // qtt qttVpsPros()
    public function qttVpsPros()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('status_vps', 'progressing')->count();
    }
    // list dịch vụ đang vps us on
    public function list_vps_us_on()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ đang vps us on
    public function list_vps_us_use()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_nearly_and_expire()
    {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        // dd($list_vps);
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( count($list_vps) > 0 ) {
            foreach ($list_vps as $key_list => $data_vps) {
              foreach ($data_vps as $key => $vps) {
                  //  cấu hình
                  $product = $vps->product;
                  $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                  $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                  $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                  // Addon
                  if (!empty($vps->vps_config)) {
                      $vps_config = $vps->vps_config;
                      if (!empty($vps_config)) {
                          $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                          $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                          $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                      }
                  }
                  $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  // khách hàng
                  if (!empty($vps->ma_kh)) {
                      if(!empty($vps->customer))
                          if(!empty($vps->customer->customer_name)) {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                          }
                          else {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                          }
                      else {
                        $vps->link_customer = 'Chính tôi';
                      }
                  } else {
                      $customer = $this->get_customer_with_vps($vps->id);
                      if(!empty($customer)) {
                          if(!empty($customer->customer_name)) {
                            $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                          } else {
                            $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                          }
                      } else {
                          $vps->link_customer = 'Chính tôi';
                      }
                  }
                  // ngày tạo
                  $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
                  // ngày kết thúc
                  $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
                  // thời gian thuê
                  $vps->text_billing_cycle = $billing[$vps->billing_cycle];
                  if ( $key_list == 'sub3' ) {
                    $vps->text_next_due_date = 'Hết hạn 3 ngày';
                  }
                  elseif ( $key_list == 'sub2' ) {
                    $vps->text_next_due_date = 'Hết hạn 2 ngày';
                  }
                  elseif ( $key_list == 'sub1' ) {
                    $vps->text_next_due_date = 'Hết hạn 1 ngày';
                  }
                  elseif ( $key_list == 'sub0' ) {
                    $vps->text_next_due_date = 'Hết hạn 0 ngày';
                  }
                  else {
                    $vps->text_next_due_date = 'Còn hạn ' . $key_list . ' ngày';
                  }
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($vps->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $vps->isExpire = $isExpire;
                  $vps->expired = $expired;
                  // tổng thời gian thuê
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $create_date != $next_due_date ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    if ( $create_date != $next_due_date ) {
                      $total_time = $diff_month . ' Tháng';
                    }
                    else {
                      $total_time = $diff_month . ' Tháng';
                    }
                  }
                  $vps->total_time = $total_time;
                  // trạng thái vps
                  if (!empty( $vps->status_vps )) {
                    if ($vps->status_vps == 'on') {
                      $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
                    }
                    elseif ($vps->status_vps == 'progressing') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
                    }
                    elseif ($vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
                    }
                    elseif ($vps->status_vps == 'change_ip') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
                    }
                    elseif ($vps->status_vps == 'reset_password') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
                    }
                    elseif ($vps->status_vps == 'expire') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
                    }
                    elseif ($vps->status_vps == 'suspend') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
                    }
                    elseif ($vps->status_vps == 'admin_off') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
                    }
                    elseif ($vps->status_vps == 'cancel') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
                    }
                    elseif ($vps->status_vps == 'delete_vps') {
                        $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
                    }
                    elseif ($vps->status_vps == 'change_user') {
                        $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
                    }
                    else {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
                    }
                  } else {
                    $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
                  }
                  // gia của vps
                  $sub_total = 0;
                  if (!empty($vps->price_override)) {
                    $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->price_vps = $sub_total;
                  $data['data'][] = $vps;
              }

            }
        }
        return $data;
    }

    public function list_vps_us_nearly_and_expire()
    {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }

        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( count($list_vps) > 0 ) {
            foreach ($list_vps as $key_list => $data_vps) {
              foreach ($data_vps as $key => $vps) {
                  //  cấu hình
                  $product = $vps->product;
                  $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                  $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                  $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                  // Addon
                  if (!empty($vps->vps_config)) {
                      $vps_config = $vps->vps_config;
                      if (!empty($vps_config)) {
                          $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                          $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                          $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                      }
                  }
                  $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  // khách hàng
                  if (!empty($vps->ma_kh)) {
                      if(!empty($vps->customer))
                          if(!empty($vps->customer->customer_name)) {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                          }
                          else {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                          }
                      else {
                        $vps->link_customer = 'Chính tôi';
                      }
                  } else {
                      $customer = $this->get_customer_with_vps($vps->id);
                      if(!empty($customer)) {
                          if(!empty($customer->customer_name)) {
                            $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                          } else {
                            $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                          }
                      } else {
                          $vps->link_customer = 'Chính tôi';
                      }
                  }
                  // ngày tạo
                  $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
                  // ngày kết thúc
                  $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
                  // thời gian thuê
                  $vps->text_billing_cycle = $billing[$vps->billing_cycle];
                  if ( $key_list == 'sub3' ) {
                    $vps->text_next_due_date = 'Hết hạn 3 ngày';
                  }
                  elseif ( $key_list == 'sub2' ) {
                    $vps->text_next_due_date = 'Hết hạn 2 ngày';
                  }
                  elseif ( $key_list == 'sub1' ) {
                    $vps->text_next_due_date = 'Hết hạn 1 ngày';
                  }
                  elseif ( $key_list == 'sub0' ) {
                    $vps->text_next_due_date = 'Hết hạn 0 ngày';
                  }
                  else {
                    $vps->text_next_due_date = 'Còn hạn ' . $key_list . ' ngày';
                  }
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($vps->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $vps->isExpire = $isExpire;
                  $vps->expired = $expired;
                  // tổng thời gian thuê
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $create_date != $next_due_date ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    if ( $create_date != $next_due_date ) {
                      $total_time = $diff_month . ' Tháng';
                    }
                    else {
                      $total_time = $diff_month . ' Tháng';
                    }
                  }
                  $vps->total_time = $total_time;
                  // trạng thái vps
                  if (!empty( $vps->status_vps )) {
                    if ($vps->status_vps == 'on') {
                      $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
                    }
                    elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
                    }
                    elseif ($vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
                    }
                    elseif ($vps->status_vps == 'change_ip') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
                    }
                    elseif ($vps->status_vps == 'reset_password') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
                    }
                    elseif ($vps->status_vps == 'expire') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
                    }
                    elseif ($vps->status_vps == 'suspend') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
                    }
                    elseif ($vps->status_vps == 'admin_off') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
                    }
                    elseif ($vps->status_vps == 'delete_vps') {
                        $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
                    }
                    elseif ($vps->status_vps == 'change_user') {
                        $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
                    }
                    elseif ($vps->status_vps == 'cancel') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
                    }
                    else {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
                    }
                  } else {
                    $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
                  }
                  // gia của vps
                  $price_vps = 0;
                  if ( !empty($vps->price_override) ) {
                     $price_vps = $vps->price_override;
                     $order_addon_vps = $vps->order_addon_vps;
                     if (!empty($order_addon_vps)) {
                       foreach ($order_addon_vps as $key => $addon_vps) {
                           $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                      $price_vps = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
                      $order_addon_vps = $vps->order_addon_vps;
                      if (!empty($order_addon_vps)) {
                        foreach ($order_addon_vps as $key => $addon_vps) {
                            $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                        }
                      }
                  }
                  $vps->price_vps = $price_vps;
                  $data['data'][] = $vps;
              }

            }
        }
        return $data;
    }
    // list dịch vụ đang on
    public function vps_all()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'cloudzone')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ đang on
    public function vps_us_all()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ đang on
    public function server_all()
    {
        return $this->server->where('user_id', Auth::user()->id)->where('status', 'Active')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_server_nearly_and_expire()
    {
        $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_server', 'on')
                      ->orWhere('status_server', 'off')
                      ->orWhere('status_server', 'expire');
                })->orderBy('id', 'desc')->get();
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_server->count() > 0 ) {
            foreach ($list_server as $key => $server) {
              $check = false;
              if (!empty($server->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($server->next_due_date);
                if ($next_due_date->isPast()) {
                  $check = true;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $check = true;
                }
              }

              if ( $check ) {
                  //  cấu hình
                  $ip  = !empty ( $server->ip ) ? 1 : 0;
                  $ram = $server->ram;
                  $disk = $server->disk;
                  $add_on_ip = 0;
                  $add_on_ram = 0;
                  $add_on_disk = 0;
                  if ( !empty($server->server_config) ) {
                    $add_on_ip = !empty($server->server_config->ip) ? $server->server_config->ip : 0;
                    $add_on_ram = !empty($server->server_config->ram) ? $server->server_config->ram : 0;
                    $add_on_disk = !empty($server->server_config->disk) ? $server->server_config->disk : 0;
                  }
                  $server->text_vps_config = $ip . ' IP - '  . $ram . ' RAM - ' . $disk . ' DISK <br>';
                  if ( $add_on_ip || $add_on_ram || $add_on_disk ) {
                    $server->text_vps_config .= $add_on_ip . ' IP - '  . $add_on_ram . ' RAM - ' . $add_on_disk . ' DISK <br>';
                  }

                  // ngày tạo
                  $server->date_create = date('d-m-Y', strtotime($server->date_create));
                  // ngày kết thúc
                  $server->next_due_date = date('d-m-Y', strtotime($server->next_due_date));
                  // thời gian thuê
                  $server->text_billing_cycle = $billing[$server->billing_cycle];
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($server->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($server->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $server->isExpire = $isExpire;
                  $server->expired = $expired;
                  // trạng thái vps
                  if (!empty( $server->status_server )) {
                    if ($server->status_server == 'on') {
                      $server->text_status_vps = '<span class="text-success" data-id="' . $server->id . '">Đang bật</span>';
                    }
                    elseif ($server->status_server == 'expire') {
                      $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Đã hết hạn</span>';
                    }
                    elseif ($server->status_server == 'suspend') {
                      $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Đang bị khoá</span>';
                    }
                    elseif ($server->status_server == 'admin_off') {
                      $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Admin tắt</span>';
                    }
                    elseif ($server->status_server == 'cancel') {
                      $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Đã hủy</span>';
                    }
                    else {
                      $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
                    }
                  } else {
                    $server->text_status_vps = '<span class="text-danger" data-id="' . $server->id . '">Chưa được tạo</span>';
                  }
                  $server->amount = number_format( $server->amount ,0,",",".");
                  $data['data'][] = $server;
              }

            }
        }
        return $data;
    }

    public function list_colocation_nearly_and_expire()
    {
        $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_colo', 'on')
                      ->orWhere('status_colo', 'off')
                      ->orWhere('status_colo', 'expire');
                })->orderBy('id', 'desc')->get();
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_colocation->count() > 0 ) {
            foreach ($list_colocation as $key => $colocation) {
              $check = false;
              if (!empty($colocation->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($colocation->next_due_date);
                if ($next_due_date->isPast()) {
                  $check = true;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $check = true;
                }
              }

              if ( $check ) {
                  //  cấu hình
                  // ngày tạo
                  $colocation->date_create = date('d-m-Y', strtotime($colocation->date_create));
                  // ngày kết thúc
                  $colocation->next_due_date = date('d-m-Y', strtotime($colocation->next_due_date));
                  // thời gian thuê
                  $colocation->text_billing_cycle = $billing[$colocation->billing_cycle];
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($colocation->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($colocation->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $colocation->isExpire = $isExpire;
                  $colocation->expired = $expired;
                  // trạng thái vps
                  if (!empty( $colocation->status_colo )) {
                    if ($colocation->status_colo == 'on') {
                      $colocation->text_status_vps = '<span class="text-success" data-id="' . $colocation->id . '">Đang bật</span>';
                    }
                    elseif ($colocation->status_colo == 'expire') {
                      $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Đã hết hạn</span>';
                    }
                    elseif ($colocation->status_colo == 'suspend') {
                      $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Đang bị khoá</span>';
                    }
                    elseif ($colocation->status_colo == 'admin_off') {
                      $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Admin tắt</span>';
                    }
                    elseif ($colocation->status_colo == 'cancel') {
                      $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Đã hủy</span>';
                    }
                    else {
                      $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Đã tắt</span>';
                    }
                  } else {
                    $colocation->text_status_vps = '<span class="text-danger" data-id="' . $colocation->id . '">Chưa được tạo</span>';
                  }
                  $amount = 0;
                  if ( !empty($colocation->amount) ) {
                    $amount = $colocation->amount;
                  } else {
                    $product = $colocation->product;
                    $amount = !empty( $product->pricing[$colocation->billing_cycle] ) ? $product->pricing[$colocation->billing_cycle] : 0;
                    if ( !empty($colocation->colocation_config_ips) ) {
                      foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                        $amount += !empty( $colocation_config_ip->product->pricing[$colocation->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colocation->billing_cycle] : 0;
                      }
                    }
                  }
                  $colocation->amount = number_format( $amount ,0,",",".");
                  $data['data'][] = $colocation;
              }
            }
        }
        return $data;
    }
    // list dịch vụ đang on
    public function colocation_all()
    {
        return $this->colocation->where('user_id', Auth::user()->id)->where('status', 'Active')
                ->orderBy('id', 'desc')->paginate(20);
    }
    // list dịch vụ đang on
    public function hosting_all()
    {
        return $this->hosting->where('user_id', Auth::user()->id)->where('status', 'Active')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    // list dịch vụ đang on
    public function list_email_hosting()
    {
        return $this->email_hosting->where('user_id', Auth::user()->id)->where('status', 'Active')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    // list dịch vụ đang on
    public function get_qtt_vps_on($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', '
                    ' ]);
              })
              ->count();
    }
    // list dịch vụ đang on
    public function get_qtt_vps_us_on($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->count();
    }
    // list dịch vụ đang on
    public function list_vps_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // list dịch vụ đang on
    public function list_vps_use_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // list dịch vụ đang on
    public function list_vps_us_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // list dịch vụ đang on
    public function list_vps_us_use_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // xoa cac ky tự không can thiet
    public function mysql_real_escape_string($string)
    {
        $data = [ '\\', "\0", "\n", "\r", "'", '"', "\x1a" ];
        foreach ($data as $key => $s) {
          $string = str_replace($s, '' , $string);
        }
        return $string;
    }
    // search vps đang on
    public function search_vps($q, $sort, $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'cloudzone')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // search vps còn hạn
    public function search_vps_use($q, $sort, $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // search vps multi đang on
    public function search_multi_vps_on($ips)
    {
       $list_vps = [];
       foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->first();
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          if ( isset($vps) ) {
            //  cấu hình
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer)) {
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
                }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                  $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($vps->status_vps == 'change_user') {
                  $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    // search vps multi còn hạn
    public function search_multi_vps_use($ips)
    {
       $list_vps = [];
       foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->first();
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          if ( isset($vps) ) {
            //  cấu hình
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer)) {
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
                }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                  $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($vps->status_vps == 'change_user') {
                  $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    // search vps multi đang on
    public function search_multi_vps_us_on($ips)
    {
       $list_vps = [];
       foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('location', 'us')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->first();
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          if ( isset($vps) ) {
            //  cấu hình
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer)) {
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
                }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                  $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($vps->status_vps == 'change_user') {
                  $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    // search vps multi đang còn hạn
    public function search_multi_vps_us_use($ips)
    {
       $list_vps = [];
       foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('location', 'us')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->first();
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          if ( isset($vps) ) {
            //  cấu hình
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer)) {
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
                }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                  $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($vps->status_vps == 'change_user') {
                  $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    // search vps đang on
    public function search_vps_us($q, $sort , $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    // search vps đang sử dụng
    public function search_vps_us_use($q, $sort , $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'us')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    //check search server
    public function check_search_server($listServer, $serverQ)
    {
      if ( isset($listServer) ) {
        foreach ($listServer as $key => $server) {
          if ( !empty($server) ) {
            if ( $server->id == $serverQ->id ) {
              return false;
            }
          }
        }
      }
      return true;
    }
    // search server đang on
    public function select_server($request)
    {
        $data = [];
        $data['data'] = [];
        $status_vps = config('status_vps');
        $billing = config('billing');
        $qtt = !empty($request['qtt']) ? $request['qtt'] : 25;
        $action = !empty($request['action']) ? $request['action'] : 'use';
        $list_server = [];
        if ( $action == 'use' ) {
            if ( !empty($request['multi_q']) ) {
              $list_ip = explode(',', $request['multi_q']);
              foreach ($list_ip as $key => $ip) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip', trim($ip))
                    ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                    ->orderBy('id', 'desc')->first();
                  if ( !isset($serverQ) ) {
                    $serverQ = $this->server->where('user_id', Auth::user()->id)
                      ->where('status', 'Active')
                      ->where('ip2', trim($ip))
                      ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                      ->orderBy('id', 'desc')->first();
                  }
                  if ( !empty($serverQ->id) ) {
                    $checkArr = $this->check_search_server($list_server, $serverQ);
                    if ($checkArr) {
                      $list_server[] = $serverQ;
                    }
                  } else {
                    $list_server[] = $serverQ;
                  }
              }
              $data['total'] = 0;
              $data['perPage'] = 0;
              $data['current_page'] = 0;
            } else {
              if (!empty($request['sort'])) {
                $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
                if ( $sort == 'DESC' || $sort == 'ASC' ) {
                  $sort = $this->mysql_real_escape_string($sort);
                } else {
                  $sort = $this->mysql_real_escape_string('DESC');
                }
                $list_server = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                  ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
              } else {
                $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                ->orderBy('id', 'desc')->paginate($qtt);
              }
              $data['total'] = $list_server->total();
              $data['perPage'] = $list_server->perPage();
              $data['current_page'] = $list_server->currentPage();
            }
        }
        elseif ( $action == 'on' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        elseif ( $action == 'nearly' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->where('status_server', 'expire')
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->where('status_server', 'expire')
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->where('status_server', 'expire')
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->where('status_server', 'expire')
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        elseif ( $action == 'cancel' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        else {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        foreach ($list_server as $key => $server) {
            if ( !empty($server) ) {
              //  cấu hình
              $server->ip = !empty($server->ip) ? $server->ip : '';
              $server->ip2 = !empty($server->ip2) ? $server->ip2 : '';
              // $product = $vps->product;
              if ( !empty($server->config_text) ) {
                $server->text_server_config = $server->config_text . '<br>';
              } else {
                $product = $server->product;
                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                $server->text_server_config = $cpu . ' (' . $cores . '), ' . $ram . ', ' . $disk . ' (' . $cores . ')<br>';
              }
              $addonConfig = '';
              $addonConfig = $this->get_config_server($server->id);
              $server->text_server_config .= $addonConfig;
              // ngày tạo
              $server->date_create = date('d-m-Y', strtotime($server->date_create));
              // ngày kết thúc
              $server->next_due_date = date('d-m-Y', strtotime($server->next_due_date));
              // thời gian thuê
              $server->text_billing_cycle = $billing[$server->billing_cycle];
              // kiểm tra vps gần hết hạn hay đã hết hạn
              $isExpire = false;
              $expired = false;
              if(!empty($server->next_due_date)){
                  $next_due_date = strtotime(date('Y-m-d', strtotime($server->next_due_date)));
                  $date = date('Y-m-d');
                  $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                  if($next_due_date <= $date) {
                    $isExpire = true;
                  }
                  $date_now = strtotime(date('Y-m-d'));
                  if ($next_due_date < $date_now) {
                      $expired = true;
                  }
              }
              $server->isExpire = $isExpire;
              $server->expired = $expired;
              $text_day = '';
              $now = Carbon::now();
              $next_due_date = new Carbon($server->next_due_date);
              $diff_date = $next_due_date->diffInDays($now);
              if ( $next_due_date->isPast() ) {
                $text_day = 'Hết hạn ' . $diff_date . ' ngày';
              } else {
                $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
              }
              $server->text_day = $text_day;
              // tổng thời gian thuê
              $total_time = '';
              $create_date = new Carbon($server->date_create);
              $next_due_date = new Carbon($server->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                if ( $create_date != $next_due_date ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                if ( $create_date != $next_due_date ) {
                  $total_time = $diff_month . ' Tháng';
                }
                else {
                  $total_time = $diff_month . ' Tháng';
                }
              }
              $server->total_time = $total_time;
              // trạng thái vps
              if (!empty( $server->status_server )) {
                if ($server->status_server == 'on') {
                  $server->text_status_server = '<span class="text-success" data-id="' . $server->id . '">Đang bật</span>';
                }
                elseif ($server->status_server == 'off') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
                }
                elseif ($server->status_server == 'progressing') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang tạo ...</span>';
                }
                elseif ($server->status_server == 'rebuild') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang cài lại ...</span>';
                }
                elseif ($server->status_server == 'change_ip') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang đổi IP ...</span>';
                }
                elseif ($server->status_server == 'expire') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã hết hạn</span>';
                }
                elseif ($server->status_server == 'suspend') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đang bị khoá</span>';
                }
                elseif ($server->status_server == 'admin_off') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Admin tắt</span>';
                }
                elseif ($server->status_server == 'cancel') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã hủy</span>';
                }
                elseif ($server->status_server == 'delete_server') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã xóa</span>';
                }
                elseif ($server->status_server == 'change_user') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Chuyển khách hàng</span>';
                }
                else {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
                }
              } else {
                $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Chưa được tạo</span>';
              }
              // gia của vps
              $total = 0;
              if ( !empty($server->amount) ) {
                $total = $server->amount;
              }
              else {
                $total = !empty( $product->pricing[$server->billing_cycle] ) ? $product->pricing[$server->billing_cycle] : 0;
                if (!empty($server->server_config)) {
                  $server_config = $server->server_config;
                  $pricing_addon = 0;
                  if ( !empty( $server_config->ram ) ) {
                      foreach ($server->server_config_rams as $server_config_ram) {
                          $pricing_addon += !empty($server_config_ram->product->pricing[$server->billing_cycle]) ? $server_config_ram->product->pricing[$server->billing_cycle] : 0;
                      }
                  }
                  if ( !empty( $server_config->ip ) ) {
                      foreach ($server->server_config_ips as $server_config_ip) {
                          $pricing_addon += !empty($server_config_ip->product->pricing[$server->billing_cycle]) ? $server_config_ip->product->pricing[$server->billing_cycle] : 0;
                      }
                  }
                  if ( !empty($server_config->disk2) ) {
                    $pricing_addon += $server_config->product_disk2->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk3) ) {
                    $pricing_addon += $server_config->product_disk3->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk4) ) {
                    $pricing_addon += $server_config->product_disk4->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk5) ) {
                    $pricing_addon += $server_config->product_disk5->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk6) ) {
                    $pricing_addon += $server_config->product_disk6->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk7) ) {
                    $pricing_addon += $server_config->product_disk7->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk8) ) {
                    $pricing_addon += $server_config->product_disk8->pricing[$server->billing_cycle];
                  }
                  $total += $pricing_addon;
                }
              }
              $server->amount = number_format( $total ,0,",",".");
            }
            $data['data'][] = $server;
        }
        // dd($data);
        return $data;
    }

    public function search_server($q , $qtt , $action)
    {
        if ( $action == 'on' ) {
            $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('ip', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_server', 'on')
                      ->orWhere('status_server', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( $action == 'nearly' ) {
          $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_server', 'cancel')
                    ->orWhere('status_server', 'expire')
                    ->orWhere('status_server', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_server = $this->server->where('user_id', Auth::user()->id)
             ->where('ip', 'LIKE' , '%'.$q.'%')
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_server as $key => $server) {
          //  cấu hình
          // $product = $vps->product;
          $ip  = !empty ( $server->ip ) ? 1 : 0;
          $ram = $server->ram;
          $disk = $server->disk;
          $add_on_ip = 0;
          $add_on_ram = 0;
          $add_on_disk = 0;
          if ( !empty($server->server_config) ) {
            $add_on_ip = !empty($server->server_config->ip) ? $server->server_config->ip : 0;
            $add_on_ram = !empty($server->server_config->ram) ? $server->server_config->ram : 0;
            $add_on_disk = !empty($server->server_config->disk) ? $server->server_config->disk : 0;
          }

          $server->text_server_config = $ip . ' IP - ' . $ram . ' RAM - ' . $disk . ' Disk <br>';
          if ( !empty($add_on_ip) && !empty($add_on_ram) && !empty($add_on_disk) ) {
            $server->text_server_config .= $add_on_ip . ' IP - ' . $add_on_ram . ' RAM - ' . $add_on_disk . ' DISK';
          }
          // ngày tạo
          $server->date_create = date('d-m-Y', strtotime($server->date_create));
          // ngày kết thúc
          $server->next_due_date = date('d-m-Y', strtotime($server->next_due_date));
          // thời gian thuê
          $server->text_billing_cycle = $billing[$server->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($server->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($server->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $server->isExpire = $isExpire;
          $server->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($server->date_create);
          $next_due_date = new Carbon($server->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $server->total_time = $total_time;
          // trạng thái vps
          if (!empty( $server->status_server )) {
            if ($server->status_server == 'on') {
              $server->text_status_server = '<span class="text-success" data-id="' . $server->id . '">Đang bật</span>';
            }
            elseif ($server->status_server == 'progressing') {
              $server->text_status_server = '<span class="vps-progressing" data-id="' . $server->id . '">Đang tạo ...</span>';
            }
            elseif ($server->status_server == 'rebuild') {
              $server->text_status_server = '<span class="vps-progressing" data-id="' . $server->id . '">Đang cài lại ...</span>';
            }
            elseif ($server->status_server == 'change_ip') {
              $server->text_status_server = '<span class="vps-progressing" data-id="' . $server->id . '">Đang đổi IP ...</span>';
            }
            elseif ($server->status_server == 'expire') {
              $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã hết hạn</span>';
            }
            elseif ($server->status_server == 'suspend') {
              $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đang bị khoá</span>';
            }
            elseif ($server->status_server == 'admin_off') {
              $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Admin tắt</span>';
            }
            else {
              $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
            }
          } else {
            $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $server->amount = number_format( $server->amount ,0,",",".") . ' VNĐ';
          $data['data'][] = $server;
       }
       $data['total'] = $list_server->total();
       $data['perPage'] = $list_server->perPage();
       $data['current_page'] = $list_server->currentPage();
       // dd($data);
       return $data;
    }

    //check search colocation
    public function check_search_colocation($list_colocation, $serverQ)
    {
      if ( isset($listServer) ) {
        foreach ($listServer as $key => $server) {
          if ( !empty($server) ) {
            if ( $server->id == $serverQ->id ) {
              return false;
            }
          }
        }
      }
      return true;
    }
    // search colocation đang on
    public function select_colocation($request)
    {
        $data = [];
        $data['data'] = [];
        $status_vps = config('status_vps');
        $billing = config('billing');
        $qtt = !empty($request['sl']) ? $request['sl'] : 25;
        $action = !empty($request['action']) ? $request['action'] : 'use';
        $list_colocation = [];
        if ( $action == 'use' ) {
            if ( !empty($request['multi_q']) ) {
              $list_ip = explode(',', $request['multi_q']);
              foreach ($list_ip as $key => $ip) {

                  $serverQ = $this->colocation->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip', trim($ip))
                    ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                    ->orderBy('id', 'desc')->first();
                  if ( !empty($serverQ->id) ) {
                    $checkArr = $this->check_search_server($list_colocation, $serverQ);
                    if ($checkArr) {
                      $list_colocation[] = $serverQ;
                    }
                  } else {
                    $list_colocation[] = $serverQ;
                  }
              }
              $data['total'] = 0;
              $data['perPage'] = 0;
              $data['current_page'] = 0;
            }
            else {
              if (!empty($request['sort'])) {
                $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
                if ( $sort == 'DESC' || $sort == 'ASC' ) {
                  $sort = $this->mysql_real_escape_string($sort);
                } else {
                  $sort = $this->mysql_real_escape_string('DESC');
                }
                $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                  ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
              } else {
                $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                ->orderBy('id', 'desc')->paginate($qtt);
              }
              $data['total'] = $list_colocation->total();
              $data['perPage'] = $list_colocation->perPage();
              $data['current_page'] = $list_colocation->currentPage();
            }
        }
        elseif ( $action == 'on' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {

                $serverQ = $this->colocation->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                  ->orderBy('id', 'desc')->first();
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_colocation, $serverQ);
                  if ($checkArr) {
                    $list_colocation[] = $serverQ;
                  }
                } else {
                  $list_colocation[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          }
          else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_colocation->total();
            $data['perPage'] = $list_colocation->perPage();
            $data['current_page'] = $list_colocation->currentPage();
          }
        }
        elseif ( $action == 'nearly' ) {
         if ( !empty($request['multi_q']) ) {
           $list_ip = explode(',', $request['multi_q']);
           foreach ($list_ip as $key => $ip) {

               $serverQ = $this->colocation->where('user_id', Auth::user()->id)
                 ->where('status', 'Active')
                 ->where('ip', trim($ip))
                 ->where('status_colo', 'expire')
                 ->orderBy('id', 'desc')->first();
               if ( !empty($serverQ->id) ) {
                 $checkArr = $this->check_search_server($list_colocation, $serverQ);
                 if ($checkArr) {
                   $list_colocation[] = $serverQ;
                 }
               } else {
                 $list_colocation[] = $serverQ;
               }
           }
           $data['total'] = 0;
           $data['perPage'] = 0;
           $data['current_page'] = 0;
         }
         else {
           if (!empty($request['sort'])) {
             $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
             if ( $sort == 'DESC' || $sort == 'ASC' ) {
               $sort = $this->mysql_real_escape_string($sort);
             } else {
               $sort = $this->mysql_real_escape_string('DESC');
             }
             $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
               ->where('status', 'Active')
               ->where('status_colo', 'expire')
               ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
           } else {
             $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
             ->where('status', 'Active')
             ->where('status_colo', 'expire')
             ->orderBy('id', 'desc')->paginate($qtt);
           }
           $data['total'] = $list_colocation->total();
           $data['perPage'] = $list_colocation->perPage();
           $data['current_page'] = $list_colocation->currentPage();
         }
       }
        elseif ( $action == 'cancel' ) {
           if ( !empty($request['multi_q']) ) {
             $list_ip = explode(',', $request['multi_q']);
             foreach ($list_ip as $key => $ip) {

                 $serverQ = $this->colocation->where('user_id', Auth::user()->id)
                   ->where('status', 'Active')
                   ->where('ip', trim($ip))
                   ->whereIn('status_colo', [ 'cancel', 'delete_server', 'change_user' ])
                   ->orderBy('id', 'desc')->first();
                 if ( !empty($serverQ->id) ) {
                   $checkArr = $this->check_search_server($list_colocation, $serverQ);
                   if ($checkArr) {
                     $list_colocation[] = $serverQ;
                   }
                 } else {
                   $list_colocation[] = $serverQ;
                 }
             }
             $data['total'] = 0;
             $data['perPage'] = 0;
             $data['current_page'] = 0;
           }
           else {
             if (!empty($request['sort'])) {
               $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
               if ( $sort == 'DESC' || $sort == 'ASC' ) {
                 $sort = $this->mysql_real_escape_string($sort);
               } else {
                 $sort = $this->mysql_real_escape_string('DESC');
               }
               $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                 ->where('status', 'Active')
                 ->whereIn('status_colo', [ 'cancel', 'delete_server', 'change_user' ])
                 ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
             } else {
               $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
               ->where('status', 'Active')
               ->whereIn('status_colo', [ 'cancel', 'delete_server', 'change_user' ])
               ->orderBy('id', 'desc')->paginate($qtt);
             }
             $data['total'] = $list_colocation->total();
             $data['perPage'] = $list_colocation->perPage();
             $data['current_page'] = $list_colocation->currentPage();
           }
        }
        else {
            if ( !empty($request['multi_q']) ) {
              $list_ip = explode(',', $request['multi_q']);
              foreach ($list_ip as $key => $ip) {

                  $serverQ = $this->colocation->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip', trim($ip))
                    ->orderBy('id', 'desc')->first();
                  if ( !empty($serverQ->id) ) {
                    $checkArr = $this->check_search_server($list_colocation, $serverQ);
                    if ($checkArr) {
                      $list_colocation[] = $serverQ;
                    }
                  } else {
                    $list_colocation[] = $serverQ;
                  }
              }
              $data['total'] = 0;
              $data['perPage'] = 0;
              $data['current_page'] = 0;
            }
            else {
              if (!empty($request['sort'])) {
                $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
                if ( $sort == 'DESC' || $sort == 'ASC' ) {
                  $sort = $this->mysql_real_escape_string($sort);
                } else {
                  $sort = $this->mysql_real_escape_string('DESC');
                }
                $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
              } else {
                $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->orderBy('id', 'desc')->paginate($qtt);
              }
              $data['total'] = $list_colocation->total();
              $data['perPage'] = $list_colocation->perPage();
              $data['current_page'] = $list_colocation->currentPage();
            }
        }
        foreach ($list_colocation as $key => $colocation) {
          //  addon ip
          $add_on_ip = '';
          $qtt_addon_ip = '';
          if ( !empty( $colocation->colocation_config->ip ) ) {
            $qtt_addon_ip = $colocation->colocation_config->ip;
            $add_on_ip .= '<button type="button" class="ml-1 btn btn-sm btn-outline-success float-right collapsed tooggle-plus" data-toggle="collapse" data-target="#service'. $colocation->id .'" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">';
            $add_on_ip .= '<i class="fas fa-plus"></i>';
            $add_on_ip .= '</button>';
            $add_on_ip .= '<div id="service'. $colocation->id .'" class="mt-4 collapse" style="">';
            foreach ($colocation->colocation_ips as $key => $colocation_ip) {
              $add_on_ip .=  $colocation_ip->ip . '<br />';
            }
            $add_on_ip .= '</div>';
          }
          $colocation->add_on_ip = $add_on_ip;
          $colocation->qtt_addon_ip = $qtt_addon_ip;
          // ngày tạo
          $colocation->date_create = date('d-m-Y', strtotime($colocation->date_create));
          // ngày kết thúc
          $colocation->next_due_date = date('d-m-Y', strtotime($colocation->next_due_date));
          // thời gian thuê
          $colocation->text_billing_cycle = $billing[$colocation->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($colocation->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($colocation->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $colocation->isExpire = $isExpire;
          $colocation->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($colocation->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $colocation->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($colocation->date_create);
          $next_due_date = new Carbon($colocation->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $colocation->total_time = $total_time;
          // trạng thái vps
          if (!empty( $colocation->status_colo )) {
            if ($colocation->status_colo == 'on') {
              $colocation->text_status_server = '<span class="text-success" data-id="' . $colocation->id . '">Đang bật</span>';
            }
            elseif ($colocation->status_colo == 'progressing') {
              $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang tạo ...</span>';
            }
            elseif ($colocation->status_colo == 'rebuild') {
              $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang cài lại ...</span>';
            }
            elseif ($colocation->status_colo == 'change_ip') {
              $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang đổi IP ...</span>';
            }
            elseif ($colocation->status_colo == 'expire') {
              $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đã hết hạn</span>';
            }
            elseif ($colocation->status_colo == 'suspend') {
              $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đang bị khoá</span>';
            }
            elseif ($colocation->status_colo == 'admin_off') {
              $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Admin tắt</span>';
            }
            else {
              $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đã tắt</span>';
            }
          } else {
            $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Chưa được tạo</span>';
          }
          // gia của $colocation
          $amount = 0;
          if ( !empty($colocation->amount) ) {
            $amount = $colocation->amount;
          } else {
            $product = $colocation->product;
            $amount = !empty( $product->pricing[$colocation->billing_cycle] ) ? $product->pricing[$colocation->billing_cycle] : 0;
            if ( !empty($colocation->colocation_config_ips) ) {
              foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                $amount += !empty( $colocation_config_ip->product->pricing[$colocation->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colocation->billing_cycle] : 0;
              }
            }
          }
          $colocation->amount = number_format( $amount ,0,",",".");
          $data['data'][] = $colocation;
        }
        // dd($data);
        return $data;
    }

    public function search_colocation($q , $qtt , $action)
    {
        if ( $action == 'on' ) {
            $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('ip', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_colo', 'on')
                      ->orWhere('status_colo', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
       elseif ( $action == 'nearly' ) {
          $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_colo', 'cancel')
                    ->orWhere('status_colo', 'expire')
                    ->orWhere('status_colo', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
             ->where('ip', 'LIKE' , '%'.$q.'%')
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_colocation as $key => $colocation) {
         // ngày tạo
         $colocation->date_create = date('d-m-Y', strtotime($colocation->date_create));
         // ngày kết thúc
         $colocation->next_due_date = date('d-m-Y', strtotime($colocation->next_due_date));
         // thời gian thuê
         $colocation->text_billing_cycle = $billing[$colocation->billing_cycle];
         // kiểm tra vps gần hết hạn hay đã hết hạn
         $isExpire = false;
         $expired = false;
         if(!empty($colocation->next_due_date)){
             $next_due_date = strtotime(date('Y-m-d', strtotime($colocation->next_due_date)));
             $date = date('Y-m-d');
             $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
             if($next_due_date <= $date) {
               $isExpire = true;
             }
             $date_now = strtotime(date('Y-m-d'));
             if ($next_due_date < $date_now) {
                 $expired = true;
             }
         }
         $colocation->isExpire = $isExpire;
         $colocation->expired = $expired;
         // trạng thái vps
         if (!empty( $colocation->status_colo )) {
           if ($colocation->status_colo == 'on') {
             $colocation->text_status_server = '<span class="text-success" data-id="' . $colocation->id . '">Đang bật</span>';
           }
           elseif ($colocation->status_colo == 'progressing') {
             $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang tạo ...</span>';
           }
           elseif ($colocation->status_colo == 'rebuild') {
             $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang cài lại ...</span>';
           }
           elseif ($colocation->status_colo == 'change_ip') {
             $colocation->text_status_server = '<span class="vps-progressing" data-id="' . $colocation->id . '">Đang đổi IP ...</span>';
           }
           elseif ($colocation->status_colo == 'expire') {
             $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đã hết hạn</span>';
           }
           elseif ($colocation->status_colo == 'suspend') {
             $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đang bị khoá</span>';
           }
           elseif ($colocation->status_colo == 'admin_off') {
             $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Admin tắt</span>';
           }
           else {
             $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Đã tắt</span>';
           }
         } else {
           $colocation->text_status_server = '<span class="text-danger" data-id="' . $colocation->id . '">Chưa được tạo</span>';
         }
         // gia của $colocation
         $colocation->amount = number_format( $colocation->amount ,0,",",".") . ' VNĐ';
         $data['data'][] = $colocation;
       }
       $data['total'] = $list_colocation->total();
       $data['perPage'] = $list_colocation->perPage();
       $data['current_page'] = $list_colocation->currentPage();
       // dd($data);
       return $data;
    }
    // search select_email_hosting đang on
    public function select_email_hosting($qtt , $action)
    {
       if ( $action == 'on' ) {
            $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                // ->where('ip', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
       elseif ( $action == 'nearly' ) {
          $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_hosting', 'cancel')
                    ->orWhere('status_hosting', 'expire')
                    ->orWhere('status_hosting', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_email_hosting as $key => $email_hosting) {
          //  cấu hình
          $product = $email_hosting->product;
          $email_hosting->text_product = $product->name;
          // ngày tạo
          $email_hosting->date_create = date('d-m-Y', strtotime($email_hosting->date_create));
          // ngày kết thúc
          $email_hosting->next_due_date = date('d-m-Y', strtotime($email_hosting->next_due_date));
          // thời gian thuê
          $email_hosting->text_billing_cycle = $billing[$email_hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($email_hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($email_hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $email_hosting->isExpire = $isExpire;
          $email_hosting->expired = $expired;
          // trạng thái vps
          if (!empty( $email_hosting->status_hosting )) {
            if ($email_hosting->status_hosting == 'on') {
              $email_hosting->text_status_server = '<span class="text-success" data-id="' . $email_hosting->id . '">Đang bật</span>';
            }
            elseif ($email_hosting->status_hosting == 'progressing') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang tạo ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'rebuild') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang cài lại ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'change_ip') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang đổi IP ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'expire') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã hết hạn</span>';
            }
            elseif ($email_hosting->status_hosting == 'suspend') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đang bị khoá</span>';
            }
            elseif ($email_hosting->status_hosting == 'admin_off') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Admin tắt</span>';
            }
            else {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã tắt</span>';
            }
          } else {
            $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Chưa được tạo</span>';
          }
          // gia của $email_hosting
          $price_hosting = 0;
          if ( !empty($email_hosting->price_override) ) {
             $price_hosting = $email_hosting->price_override;
          } else {
             $price_hosting = !empty($email_hosting->detail_order->sub_total) ? $email_hosting->detail_order->sub_total : 0;

          }
          $email_hosting->amount = number_format( $price_hosting ,0,",",".") . ' VNĐ';
          $data['data'][] = $email_hosting;
       }
       $data['total'] = $list_email_hosting->total();
       $data['perPage'] = $list_email_hosting->perPage();
       $data['current_page'] = $list_email_hosting->currentPage();
       // dd($data);
       return $data;
    }

    public function search_email_hosting($q , $qtt , $action)
    {
       if ( $action == 'on' ) {
            $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                ->where('domain', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
       elseif ( $action == 'nearly' ) {
          $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'LIKE' , '%'.$q.'%')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_hosting', 'cancel')
                    ->orWhere('status_hosting', 'expire')
                    ->orWhere('status_hosting', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
             ->where('domain', 'LIKE' , '%'.$q.'%')
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_email_hosting as $key => $email_hosting) {
          //  cấu hình
          $product = $email_hosting->product;
          $email_hosting->text_product = $product->name;
          // ngày tạo
          $email_hosting->date_create = date('d-m-Y', strtotime($email_hosting->date_create));
          // ngày kết thúc
          $email_hosting->next_due_date = date('d-m-Y', strtotime($email_hosting->next_due_date));
          // thời gian thuê
          $email_hosting->text_billing_cycle = $billing[$email_hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($email_hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($email_hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $email_hosting->isExpire = $isExpire;
          $email_hosting->expired = $expired;
          // trạng thái vps
          if (!empty( $email_hosting->status_hosting )) {
            if ($email_hosting->status_hosting == 'on') {
              $email_hosting->text_status_server = '<span class="text-success" data-id="' . $email_hosting->id . '">Đang bật</span>';
            }
            elseif ($email_hosting->status_hosting == 'progressing') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang tạo ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'rebuild') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang cài lại ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'change_ip') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang đổi IP ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'expire') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã hết hạn</span>';
            }
            elseif ($email_hosting->status_hosting == 'suspend') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đang bị khoá</span>';
            }
            elseif ($email_hosting->status_hosting == 'admin_off') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Admin tắt</span>';
            }
            else {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã tắt</span>';
            }
          } else {
            $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Chưa được tạo</span>';
          }
          // gia của $email_hosting
          $price_hosting = 0;
          if ( !empty($email_hosting->price_override) ) {
             $price_hosting = $email_hosting->price_override;
          } else {
             $price_hosting = !empty($email_hosting->detail_order->sub_total) ? $email_hosting->detail_order->sub_total : 0;

          }
          $email_hosting->amount = number_format( $price_hosting ,0,",",".") . ' VNĐ';
          $data['data'][] = $email_hosting;
       }
       $data['total'] = $list_email_hosting->total();
       $data['perPage'] = $list_email_hosting->perPage();
       $data['current_page'] = $list_email_hosting->currentPage();
       // dd($data);
       return $data;
    }

    public function list_hosting_with_sl($request)
    {
        $data = [];
        $data['data'] = [];
        $status_vps = config('status_vps');
        $billing = config('billing');
        $type = $request['type'];
        // $n++;
        if ($type == 'use') {
          $sl = !empty($request['sl']) ? $request['sl'] : 30;
          if (!empty($request['sort'])) {
            // dd('da den 1');
            $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = 'DESC';
            }
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', [ 'on', 'off', 'admin_off', 'expire' ])
              ->orderByRaw('next_due_date ' . $sort)->with('product')->paginate($sl);
          } else {
            // dd('da den 2');
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', [ 'on', 'off', 'admin_off', 'expire' ])
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
          }
        }
        elseif ($type == 'on') {
          $sl = !empty($request['sl']) ? $request['sl'] : 30;
          if (!empty($request['sort'])) {
            // dd('da den 1');
            $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = 'DESC';
            }
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', [ 'on', 'off', 'admin_off' ])
              ->orderByRaw('next_due_date ' . $sort)->with('product')->paginate($sl);
          } else {
            // dd('da den 2');
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', [ 'on', 'off', 'admin_off' ])
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
          }
        }
        elseif ($type == 'expire') {
          $sl = !empty($request['sl']) ? $request['sl'] : 30;
          if (!empty($request['sort'])) {
            // dd('da den 1');
            $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = 'DESC';
            }
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->where('status_hosting', 'expire')
              ->orderByRaw('next_due_date ' . $sort)->with('product')->paginate($sl);
          } else {
            // dd('da den 2');
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->where('status_hosting', 'expire')
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
          }
        }
        elseif ($type == 'cancel') {
          $sl = !empty($request['sl']) ? $request['sl'] : 30;
          if (!empty($request['sort'])) {
            // dd('da den 1');
            $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = 'DESC';
            }
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', ['delete', 'cancel', 'change_user'])
              ->orderByRaw('next_due_date ' . $sort)->with('product')->paginate($sl);
          } else {
            // dd('da den 2');
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->whereIn('status_hosting', ['delete', 'cancel', 'change_user'])
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
          }
        }
        else {
          $sl = !empty($request['sl']) ? $request['sl'] : 30;
          if (!empty($request['sort'])) {
            // dd('da den 1');
            $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = 'DESC';
            }
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->orderByRaw('next_due_date ' . $sort)->with('product')->paginate($sl);
          } else {
            // dd('da den 2');
            $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'like', '%'. $request['q'] .'%')
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
          }
        }
      // if ( !empty($request['multi_q']) ) {
        foreach ($list_hosting as $key => $hosting) {
            //  cấu hình
            $product = $hosting->product;
            $hosting->product_name = $product->name;
            // ngày tạo
            $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
            // ngày kết thúc
            $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
            // thời gian thuê
            $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($hosting->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $hosting->isExpire = $isExpire;
            $hosting->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($hosting->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $hosting->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($hosting->date_create);
            $next_due_date = new Carbon($hosting->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $hosting->total_time = $total_time;
            // gia hosting
            $price_hosting = 0;
            if ( !empty($hosting->price_override) ) {
              $price_hosting = $hosting->price_override;
              if (!empty($hosting->order_upgrade_hosting)) {
                foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                    $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                }
              }
            } else {
              $product = $hosting->product;
              $price_hosting = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
            }
            $hosting->price_hosting = number_format( $price_hosting ,0,",",".");
            // trạng thái vps
            if (!empty( $hosting->status_hosting )) {
              if ($hosting->status_hosting == 'on') {
                $hosting->text_status_hosting = '<span class="text-success" data-id="' . $hosting->id . '">Đang bật</span>';
              }
              elseif ($hosting->status_hosting == 'expire') {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Đã hết hạn</span>';
              }
              elseif ($hosting->status_hosting == 'cancel') {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Đang hủy</span>';
              }
              elseif ($hosting->status_hosting == 'delete') {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Đã xóa</span>';
              }
              elseif ($hosting->status_hosting == 'admin_off') {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Admin tắt</span>';
              }
              elseif ($hosting->status_hosting == 'off') {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Đã tắt</span>';
              }
              else {
                $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Đã tắt</span>';
              }
            } else {
              $hosting->text_status_hosting = '<span class="text-danger" data-id="' . $hosting->id . '">Chưa được tạo</span>';
            }
            // trạng thái vps
            $data['data'][] = $hosting;
        }
        $data['total'] = $list_hosting->total();
        $data['perPage'] = $list_hosting->perPage();
        $data['current_page'] = $list_hosting->currentPage();
        return $data;
    }

    public function list_all_hosting_with_sl($sl)
    {
       $list_hosting = $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')->orderBy('id', 'desc')->with('product')->paginate($sl);
       $data = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_hosting as $key => $hosting) {
          //  cấu hình
          $product = $hosting->product;
          $hosting->product_name = $product->name;
          // khách hàng
          if (!empty($hosting->ma_kh)) {
              if(!empty($hosting->customer)) {
                  if(!empty($hosting->customer->customer_name)) {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                  }
                  else {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                  }
              }
              else {
                $hosting->customer_name = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_hosting($hosting->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $hosting->customer_name = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $hosting->customer_name =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $hosting->customer_name = 'Chính tôi';
              }
          }
          // ngày tạo
          $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
          // ngày kết thúc
          $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
          // thời gian thuê
          $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $hosting->isExpire = $isExpire;
          $hosting->expired = $expired;
          // trạng thái vps
          $data['data'][] = $hosting;
       }
       $data['total'] = $list_hosting->total();
       $data['perPage'] = $list_hosting->perPage();
       $data['current_page'] = $list_hosting->currentPage();
       return $data;
    }

    public function hosting_search($q, $sort)
    {
       if (!empty($sort)) {
           if ( $sort == 'DESC' || $sort == 'ASC' ) {
             $sort = $this->mysql_real_escape_string($sort);
           } else {
             $sort = $this->mysql_real_escape_string('DESC');
           }
         $list_hosting = $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')
                                ->where('domain', 'LIKE' , '%'.$q.'%')
                                ->where(function($query)
                                    {
                                        return $query
                                          ->orwhere('status_hosting', 'on')
                                          ->orWhere('status_hosting', 'off');
                                    })
                                ->orderByRaw('next_due_date ' . $sort)
                                ->with('product')->paginate(30);
       } else {
         $list_hosting = $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')
                                ->where('domain', 'LIKE' , '%'.$q.'%')
                                ->where(function($query)
                                    {
                                        return $query
                                          ->orwhere('status_hosting', 'on')
                                          ->orWhere('status_hosting', 'off');
                                    })
                                ->orderBy('id', 'desc')->with('product')->paginate(30);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_hosting as $key => $hosting) {
          //  cấu hình
          $product = $hosting->product;
          $hosting->product_name = $product->name;
          // khách hàng
          if (!empty($hosting->ma_kh)) {
              if(!empty($hosting->customer)) {
                  if(!empty($hosting->customer->customer_name)) {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                  }
                  else {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                  }
              }
              else {
                $hosting->customer_name = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_hosting($hosting->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $hosting->customer_name = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $hosting->customer_name =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $hosting->customer_name = 'Chính tôi';
              }
          }
          // ngày tạo
          $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
          // ngày kết thúc
          $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
          // thời gian thuê
          $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $hosting->isExpire = $isExpire;
          $hosting->expired = $expired;
          // gia hosting
          $price_hosting = 0;
          if ( !empty($hosting->price_override) ) {
             $price_hosting = $hosting->price_override;
             if (!empty($hosting->order_upgrade_hosting)) {
               foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                  $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
               }
             }
          } else {
             $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
             if (!empty($hosting->order_upgrade_hosting)) {
               foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                  $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
               }
             }
          }
          $hosting->price_hosting = $price_hosting;
          // trạng thái vps
          $data['data'][] = $hosting;
       }
       $data['total'] = $list_hosting->total();
       $data['perPage'] = $list_hosting->perPage();
       $data['current_page'] = $list_hosting->currentPage();
       return $data;
    }

    public function all_hosting_search($q, $sort)
    {
       if (!empty($sort)) {
           if ( $sort == 'DESC' || $sort == 'ASC' ) {
             $sort = $this->mysql_real_escape_string($sort);
           } else {
             $sort = $this->mysql_real_escape_string('DESC');
           }
         $list_hosting = $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')
                                ->where('domain', 'LIKE' , '%'.$q.'%')
                                ->orderByRaw('next_due_date ' . $sort)
                                ->with('product')->paginate(30);
       } else {
         $list_hosting = $this->hosting->where('user_id', Auth::user()->id)->where('date_create', '!=', '')
                                ->where('domain', 'LIKE' , '%'.$q.'%')
                                ->orderBy('id', 'desc')->with('product')->paginate(30);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_hosting as $key => $hosting) {
          //  cấu hình
          $product = $hosting->product;
          $hosting->product_name = $product->name;
          // khách hàng
          if (!empty($hosting->ma_kh)) {
              if(!empty($hosting->customer)) {
                  if(!empty($hosting->customer->customer_name)) {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                  }
                  else {
                    $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                  }
              }
              else {
                $hosting->customer_name = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_hosting($hosting->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $hosting->customer_name = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $hosting->customer_name =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $hosting->customer_name = 'Chính tôi';
              }
          }
          // ngày tạo
          $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
          // ngày kết thúc
          $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
          // thời gian thuê
          $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $hosting->isExpire = $isExpire;
          $hosting->expired = $expired;
          // trạng thái vps
          $data['data'][] = $hosting;
       }
       $data['total'] = $list_hosting->total();
       $data['perPage'] = $list_hosting->perPage();
       $data['current_page'] = $list_hosting->currentPage();
       return $data;
    }


    public function list_hosting_nearly_search($q, $sl)
    {
      if (empty($sl)) {
        $sl = 20;
      }
      $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                  ->where("domain", 'LIKE' , '%'.$q.'%')
                  ->where(function($query)
                  {
                      return $query
                        ->orwhere('status_hosting', 'cancel')
                        ->orwhere('status_hosting', 'admin_off')
                        ->orWhere('status_hosting', 'expire');
                  })
                  ->with('product')->orderBy('id', 'desc')->paginate($sl);
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_hosting as $key => $hosting) {
         //  cấu hình
         $product = $hosting->product;
         $hosting->product_name = $product->name;
         // khách hàng
         if (!empty($hosting->ma_kh)) {
             if(!empty($hosting->customer)) {
                 if(!empty($hosting->customer->customer_name)) {
                   $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                 }
                 else {
                   $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                 }
             }
             else {
               $hosting->customer_name = 'Chính tôi';
             }
         } else {
             $customer = $this->get_customer_with_hosting($hosting->id);
             if(!empty($customer)) {
                 if(!empty($customer->customer_name)) {
                   $hosting->customer_name = $customer->ma_customer . ' - ' . $customer->customer_name;
                 } else {
                   $hosting->customer_name =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                 }
             } else {
                 $hosting->customer_name = 'Chính tôi';
             }
         }
         // ngày tạo
         $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
         // ngày kết thúc
         $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
         // thời gian thuê
         $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
         // kiểm tra vps gần hết hạn hay đã hết hạn
         $isExpire = false;
         $expired = false;
         if(!empty($hosting->next_due_date)){
             $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
             $date = date('Y-m-d');
             $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
             if($next_due_date <= $date) {
               $isExpire = true;
             }
             $date_now = strtotime(date('Y-m-d'));
             if ($next_due_date < $date_now) {
                 $expired = true;
             }
         }
         $hosting->isExpire = $isExpire;
         $hosting->expired = $expired;
         // trạng thái vps
         $data['data'][] = $hosting;
      }
      return $data;

    }

    public function qtt_all_vps($user_id)
    {
       return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'cloudzone')->count();
    }


    public function qtt_all_vps_us($user_id)
    {
       return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'us')->count();
    }

    public function qtt_all_hosting($user_id)
    {
       return $this->hosting->where('user_id', $user_id)->where('status', 'Active')->count();
    }

    public function qtt_all_email_hosting($user_id)
    {
       return $this->email_hosting->where('user_id', $user_id)->where('status', 'Active')->count();
    }

    public function qtt_all_server($user_id)
    {
       return $this->server->where('user_id', $user_id)->where('status', 'Active')->count();
    }

    public function qtt_all_colocation($user_id)
    {
       return $this->colocation->where('user_id', $user_id)->where('status', 'Active')->count();
    }

    // list tất cả dịch vụ vps
    public function list_all_vps_with_sl($sl)
    {
       $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')->where('status', 'Active')->orderBy('id', 'desc')->with('product')->paginate($sl);
       $data = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       $data['data'] = [];
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       return $data;
    }
    // list tất cả dịch vụ vps
    public function list_all_vps_us_with_sl($sl)
    {
       $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')->orderBy('id', 'desc')->with('product')->paginate($sl);
       $data = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       $data['data'] = [];
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          $price_vps = 0;
          if ( !empty($vps->price_override) ) {
             $price_vps = $vps->price_override;
             $order_addon_vps = $vps->order_addon_vps;
             if (!empty($order_addon_vps)) {
               foreach ($order_addon_vps as $key => $addon_vps) {
                   $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
               }
             }
          } else {
              $price_vps = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
              $order_addon_vps = $vps->order_addon_vps;
              if (!empty($order_addon_vps)) {
                foreach ($order_addon_vps as $key => $addon_vps) {
                    $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                }
              }
          }
          $vps->price_vps = number_format( $price_vps ,0,",",".");
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       return $data;
    }

    public function all_vps_search($q, $sort, $sl)
    {
      if (!empty($sort)) {
          if ( $sort == 'DESC' || $sort == 'ASC' ) {
            $sort = $this->mysql_real_escape_string($sort);
          } else {
            $sort = $this->mysql_real_escape_string('DESC');
          }
         $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                          ->where('location', 'cloudzone')
                          ->orderBy('id', 'desc')
                          ->where('ip', 'LIKE' , '%'.$q.'%')
                          ->orderByRaw('next_due_date ' . $sort)
                          ->with('product')->paginate($sl);
      } else {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'cloudzone')
                         ->orderBy('id', 'desc')
                         ->where('ip', 'LIKE' , '%'.$q.'%')
                         ->with('product')->paginate($sl);
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        $product = $vps->product;
        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
        // Addon
        if (!empty($vps->vps_config)) {
          $vps_config = $vps->vps_config;
          if (!empty($vps_config)) {
            $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
            $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
            $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
          }
        }
        $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
        // khách hàng
        if (!empty($vps->ma_kh)) {
            if (!empty($vps->customer)) {
              if(!empty($vps->customer->customer_name)) {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
              }
              else {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
              }
            }
            else {
              $vps->link_customer = 'Chính tôi';
            }
        } else {
          $customer = $this->get_customer_with_vps($vps->id);
          if(!empty($customer)) {
            if(!empty($customer->customer_name)) {
              $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
            } else {
              $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
            }
          } else {
            $vps->link_customer = 'Chính tôi';
          }
        }
        // ngày tạo
        $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
        // ngày kết thúc
        $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
        // thời gian thuê
        $vps->text_billing_cycle = $billing[$vps->billing_cycle];
        // kiểm tra vps gần hết hạn hay đã hết hạn
        $isExpire = false;
        $expired = false;
        if(!empty($vps->next_due_date)){
          $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
          $date = date('Y-m-d');
          $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
          if($next_due_date <= $date) {
            $isExpire = true;
          }
          $date_now = strtotime(date('Y-m-d'));
          if ($next_due_date < $date_now) {
            $expired = true;
          }
        }
        $vps->isExpire = $isExpire;
        $vps->expired = $expired;
        // tổng thời gian thuê
        $total_time = '';
        $create_date = new Carbon($vps->date_create);
        $next_due_date = new Carbon($vps->next_due_date);
        if ( $next_due_date->diffInYears($create_date) ) {
          $year = $next_due_date->diffInYears($create_date);
          $total_time = $year . ' Năm ';
          $create_date = $create_date->addYears($year);
          $month = $next_due_date->diffInMonths($create_date);
          //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
          if ( $create_date != $next_due_date ) {
            $total_time .= $month . ' Tháng';
          }
        } else {
          $diff_month = $next_due_date->diffInMonths($create_date);
          if ( $create_date != $next_due_date ) {
            $total_time = $diff_month . ' Tháng';
          }
          else {
            $total_time = $diff_month . ' Tháng';
          }
        }
        $vps->total_time = $total_time;
        // trạng thái vps
        if (!empty( $vps->status_vps )) {
          if ($vps->status_vps == 'on') {
            $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
          }
          elseif ($vps->status_vps == 'progressing') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
          }
          elseif ($vps->status_vps == 'rebuild') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
          }
          elseif ($vps->status_vps == 'change_ip') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
          }
          elseif ($vps->status_vps == 'reset_password') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
          }
          elseif ($vps->status_vps == 'expire') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
          }
          elseif ($vps->status_vps == 'suspend') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
          }
          elseif ($vps->status_vps == 'admin_off') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
          }
          elseif ($vps->status_vps == 'delete_vps') {
              $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
          }
          elseif ($vps->status_vps == 'change_user') {
              $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
          }
          else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
          }
        } else {
          $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
        }
        $data['data'][] = $vps;
      }
      $data['total'] = $list_vps->total();
      $data['perPage'] = $list_vps->perPage();
      $data['current_page'] = $list_vps->currentPage();
      
      // dd($data);
      return $data;
    }

    public function search_multi_vps_all($ips)
    {
      $list_vps = [];
      foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'cloudzone')
                         ->orderBy('id', 'desc')
                         ->where('ip', trim($ip))
                         ->with('product')->first();
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        if ( isset($vps) ) {
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
            $vps_config = $vps->vps_config;
            if (!empty($vps_config)) {
              $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
              $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
              $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
            }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if (!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
            $customer = $this->get_customer_with_vps($vps->id);
            if(!empty($customer)) {
              if(!empty($customer->customer_name)) {
                $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
              } else {
                $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
              }
            } else {
              $vps->link_customer = 'Chính tôi';
            }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
            $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
            $date = date('Y-m-d');
            $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
            if($next_due_date <= $date) {
              $isExpire = true;
            }
            $date_now = strtotime(date('Y-m-d'));
            if ($next_due_date < $date_now) {
              $expired = true;
            }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
        }
        $data['data'][] = $vps;
      }
      $data['total'] = 0;
      $data['perPage'] = 0;
      $data['current_page'] = 0;
      // dd($data);
      return $data;
    }

    public function search_multi_vps_us_all($ips)
    {
      $list_vps = [];
      foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'us')
                         ->orderBy('id', 'desc')
                         ->where('ip', trim($ip))
                         ->with('product')->first();
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        if ( isset($vps) ) {
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
            $vps_config = $vps->vps_config;
            if (!empty($vps_config)) {
              $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
              $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
              $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
            }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if (!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
            $customer = $this->get_customer_with_vps($vps->id);
            if(!empty($customer)) {
              if(!empty($customer->customer_name)) {
                $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
              } else {
                $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
              }
            } else {
              $vps->link_customer = 'Chính tôi';
            }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
            $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
            $date = date('Y-m-d');
            $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
            if($next_due_date <= $date) {
              $isExpire = true;
            }
            $date_now = strtotime(date('Y-m-d'));
            if ($next_due_date < $date_now) {
              $expired = true;
            }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
        }
        $data['data'][] = $vps;
      }
      $data['total'] = 0;
      $data['perPage'] = 0;
      $data['current_page'] = 0;
      // dd($data);
      return $data;
    }

    public function all_vps_us_search($q, $sort)
    {
      if (!empty($sort)) {
          if ( $sort == 'DESC' || $sort == 'ASC' ) {
            $sort = $this->mysql_real_escape_string($sort);
          } else {
            $sort = $this->mysql_real_escape_string('DESC');
          }
         $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                          ->where('location', 'us')
                          ->orderBy('id', 'desc')
                          ->where('ip', 'LIKE' , '%'.$q.'%')
                          ->orderByRaw('next_due_date ' . $sort)
                          ->with('product')->paginate(30);
      } else {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'us')
                         ->orderBy('id', 'desc')
                         ->where('ip', 'LIKE' , '%'.$q.'%')
                         ->with('product')->paginate(30);
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        $product = $vps->product;
        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
        // Addon
        if (!empty($vps->vps_config)) {
          $vps_config = $vps->vps_config;
          if (!empty($vps_config)) {
            $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
            $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
            $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
          }
        }
        $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
        // khách hàng
        if (!empty($vps->ma_kh)) {
            if (!empty($vps->customer)) {
              if(!empty($vps->customer->customer_name)) {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
              }
              else {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
              }
            }
            else {
              $vps->link_customer = 'Chính tôi';
            }
        } else {
          $customer = $this->get_customer_with_vps($vps->id);
          if(!empty($customer)) {
            if(!empty($customer->customer_name)) {
              $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
            } else {
              $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
            }
          } else {
            $vps->link_customer = 'Chính tôi';
          }
        }
        // ngày tạo
        $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
        // ngày kết thúc
        $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
        // thời gian thuê
        $vps->text_billing_cycle = $billing[$vps->billing_cycle];
        // kiểm tra vps gần hết hạn hay đã hết hạn
        $isExpire = false;
        $expired = false;
        if(!empty($vps->next_due_date)){
          $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
          $date = date('Y-m-d');
          $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
          if($next_due_date <= $date) {
            $isExpire = true;
          }
          $date_now = strtotime(date('Y-m-d'));
          if ($next_due_date < $date_now) {
            $expired = true;
          }
        }
        $vps->isExpire = $isExpire;
        $vps->expired = $expired;
        // tổng thời gian thuê
        $total_time = '';
        $create_date = new Carbon($vps->date_create);
        $next_due_date = new Carbon($vps->next_due_date);
        if ( $next_due_date->diffInYears($create_date) ) {
          $year = $next_due_date->diffInYears($create_date);
          $total_time = $year . ' Năm ';
          $create_date = $create_date->addYears($year);
          $month = $next_due_date->diffInMonths($create_date);
          //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
          if ( $create_date != $next_due_date ) {
            $total_time .= $month . ' Tháng';
          }
        } else {
          $diff_month = $next_due_date->diffInMonths($create_date);
          if ( $create_date != $next_due_date ) {
            $total_time = $diff_month . ' Tháng';
          }
          else {
            $total_time = $diff_month . ' Tháng';
          }
        }
        $vps->total_time = $total_time;
        // trạng thái vps
        if (!empty( $vps->status_vps )) {
          if ($vps->status_vps == 'on') {
            $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
          }
          elseif ($vps->status_vps == 'progressing') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
          }
          elseif ($vps->status_vps == 'rebuild') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
          }
          elseif ($vps->status_vps == 'change_ip') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
          }
          elseif ($vps->status_vps == 'reset_password') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
          }
          elseif ($vps->status_vps == 'expire') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
          }
          elseif ($vps->status_vps == 'suspend') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
          }
          elseif ($vps->status_vps == 'admin_off') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
          }
          elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
          }
          elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
          }
          else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
          }
        } else {
          $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
        }
        $price_vps = 0;
        if ( !empty($vps->price_override) ) {
           $price_vps = $vps->price_override;
           $order_addon_vps = $vps->order_addon_vps;
           if (!empty($order_addon_vps)) {
             foreach ($order_addon_vps as $key => $addon_vps) {
                 $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
             }
           }
        } else {
            $price_vps = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
            $order_addon_vps = $vps->order_addon_vps;
            if (!empty($order_addon_vps)) {
              foreach ($order_addon_vps as $key => $addon_vps) {
                  $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
              }
            }
        }
        $vps->price_vps = number_format( $price_vps ,0,",",".");
        $data['data'][] = $vps;
      }
      $data['total'] = $list_vps->total();
      $data['perPage'] = $list_vps->perPage();
      $data['current_page'] = $list_vps->currentPage();
      // dd($data);
      return $data;
    }

    public function get_customer_with_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $invoice = $hosting->detail_order;
        $customer = $this->customer->where('ma_customer', $invoice->order->makh)->first();
        if (isset($customer)) {
            return $customer;
        } else {
            return false;
        }
    }

    public function get_customer_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $invoice = $vps->detail_order;
        if (!empty($invoice->order->makh)) {
          $customer = $this->customer->where('ma_customer', $invoice->order->makh)->first();
        } else {
          return false;
        }
        if (isset($customer)) {
            return $customer;
        } else {
            return false;
        }
    }
    
    public function list_hosting_use()
    {
        return $this->hosting->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_hosting', [ 'on', 'off', 'admin_off', 'expire' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_hosting_on()
    {
        return $this->hosting->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_hosting', [ 'on', 'off', 'admin_off' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_hosting_nearly_and_expire()
    {
        $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off')
                      ->orWhere('status_hosting', 'expire');
                })->orderBy('id', 'desc')->get();
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_hosting->count() > 0 ) {
            foreach ($list_hosting as $key => $hosting) {
              $check = false;
              if (!empty($hosting->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($hosting->next_due_date);
                if ($next_due_date->isPast()) {
                  $check = true;
                }
                elseif ( $next_due_date->diffInDays($now) <= 15 ) {
                  $check = true;
                }
              }

              if ( $check ) {
                  //  cấu hình
                  $product = $hosting->product;
                  $hosting->product_name = $product->name;
                  // khách hàng
                  if (!empty($hosting->ma_kh)) {
                      if(!empty($hosting->customer))
                          if(!empty($hosting->customer->customer_name)) {
                            $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                          }
                          else {
                            $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                          }
                      else {
                        $hosting->link_customer = 'Chính tôi';
                      }
                  } else {
                      $customer = $this->get_customer_with_hosting($hosting->id);
                      if(!empty($customer)) {
                          if(!empty($customer->customer_name)) {
                            $hosting->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                          } else {
                            $hosting->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                          }
                      } else {
                          $hosting->link_customer = 'Chính tôi';
                      }
                  }
                  // ngày tạo
                  $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
                  // ngày kết thúc
                  $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
                  // thời gian thuê
                  $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($hosting->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $hosting->isExpire = $isExpire;
                  $hosting->expired = $expired;
                  // trạng thái vps
                  if (!empty( $hosting->status_hosting )) {
                    if ($hosting->status_vps == 'on') {
                      $hosting->status_hosting = '<span class="text-success" data-id="' . $hosting->id . '">Đang bật</span>';
                    }
                    elseif ($hosting->status_hosting == 'expire') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã hết hạn</span>';
                    }
                    elseif ($hosting->status_hosting == 'suspend') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đang bị khoá</span>';
                    }
                    elseif ($hosting->status_hosting == 'admin_off') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Admin tắt</span>';
                    }
                    elseif ($hosting->status_hosting == 'cancel') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã hủy</span>';
                    }
                    else {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã tắt</span>';
                    }
                  } else {
                    $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Chưa được tạo</span>';
                  }
                  $data['data'][] = $hosting;
              }

            }
        }
        return $data;
    }

    public function list_email_hosting_nearly_and_expired()
    {
        $list_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off')
                      ->orWhere('status_hosting', 'expire');
                })->orderBy('id', 'desc')->get();
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_hosting->count() > 0 ) {
            foreach ($list_hosting as $key => $hosting) {
              $check = false;
              if (!empty($hosting->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($hosting->next_due_date);
                if ($next_due_date->isPast()) {
                  $check = true;
                }
                elseif ( $next_due_date->diffInDays($now) <= 15 ) {
                  $check = true;
                }
              }

              if ( $check ) {
                  //  cấu hình
                  $product = $hosting->product;
                  $hosting->product_name = $product->name;
                  // khách hàng
                  // ngày tạo
                  $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
                  // ngày kết thúc
                  $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
                  // thời gian thuê
                  $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($hosting->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $hosting->isExpire = $isExpire;
                  $hosting->expired = $expired;
                  // trạng thái vps
                  if (!empty( $hosting->status_hosting )) {
                    if ($hosting->status_hosting == 'on') {
                      $hosting->text_status_vps = '<span class="text-success" data-id="' . $hosting->id . '">Đang bật</span>';
                    }
                    elseif ($hosting->status_hosting == 'expire') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã hết hạn</span>';
                    }
                    elseif ($hosting->status_hosting == 'suspend') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đang bị khoá</span>';
                    }
                    elseif ($hosting->status_hosting == 'admin_off') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Admin tắt</span>';
                    }
                    elseif ($hosting->status_hosting == 'cancel') {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã hủy</span>';
                    }
                    else {
                      $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Đã tắt</span>';
                    }
                  } else {
                    $hosting->text_status_vps = '<span class="text-danger" data-id="' . $hosting->id . '">Chưa được tạo</span>';
                  }
                  $data['data'][] = $hosting;
              }

            }
        }
        return $data;
    }

    public function list_email_hosting_on()
    {
        return $this->email_hosting->where('user_id', Auth::user()->id)->with('product')
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_hosting', 'on')
                  ->orWhere('status_hosting', 'off');
            })
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_server_use()
    {
        return $this->server->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_server_on()
    {
        return $this->server->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function qtt_proxy_on()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->count();
    }

    public function qtt_proxy_expire()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->where('status', 'expire')
            ->count();
    }

    public function qtt_proxy_nearly()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status', ['cancel', 'change_user', 'delete'])
            ->count();
    }

    public function qtt_proxy_all()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->where('status', '!=', 'Pending')
            ->count();
    }

    public function list_proxy_use()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_proxy_on()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->orderBy('id', 'desc')->paginate(20);
    }
    public function list_proxy_expire()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->where('status', 'expire')
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_proxy_cancel()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status', ['cancel', 'change_user', 'delete'])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_proxy_all()
    {
        return $this->proxy->where('user_id', Auth::user()->id)->with('product')
            ->where('status', '!=', 'Pending')
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_colocation_use()
    {
        return $this->colocation->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password', 'expire' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function list_colocation_on()
    {
        return $this->colocation->where('user_id', Auth::user()->id)->with('product')
            ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->orderBy('id', 'desc')->paginate(20);
    }

    public function get_qtt_hosting_on($user_id)
    {
        return $this->hosting->where('user_id', $user_id)
            ->whereIn('status_hosting', [ 'on', 'off', 'admin_off' ])
            ->count();
    }

    public function get_qtt_email_hosting_on($user_id)
    {
        return $this->email_hosting->where('user_id', $user_id)
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_hosting', 'on')
                  ->orWhere('status_hosting', 'off');
            })
            ->count();
    }

    public function get_qtt_server_on($user_id)
    {
        return $this->server->where('user_id', $user_id)
            ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->count();
    }

    public function get_qtt_colocation_use($user_id)
    {
        return $this->colocation->where('user_id', $user_id)
            ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password', 'expire' ])
            ->count();
    }

    public function get_qtt_colocation_on($user_id)
    {
        return $this->colocation->where('user_id', $user_id)
            ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
            ->count();
    }


    public function get_qtt_domain_on($user_id)
    {
      return $this->domain->where('user_id', $user_id)->count();
    }
    // list dịch vụ chưa tạo
    public function list_vps_pending()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Pending')->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_nearly()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->where('status_vps', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }

    public function list_vps_cancel()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->where(function($query)
                    {
                        return $query
                          ->whereIn('status_vps', [ 'cancel', 'delete_vps', 'change_user' ]);
                    })
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }

    public function list_vps_us_nearly()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                    ->where('status_vps', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }

    public function list_vps_us_cancel()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                    ->where(function($query)
                    {
                        return $query
                        ->whereIn('status_vps', [ 'cancel', 'delete_vps', 'change_user' ]);
                    })
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }

    public function list_server_nearly()
    {
        $servers = $this->server->where('user_id', Auth::user()->id)
                    ->where('status_server', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $servers;

    }

    public function list_server_cancel()
    {
        $servers = $this->server->where('user_id', Auth::user()->id)
                    ->whereIn('status_server', ['cancel', 'delete_server', 'change_user'])
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $servers;

    }

    public function list_colocation_nearly()
    {
        $colocations = $this->colocation->where('user_id', Auth::user()->id)
                    ->where('status_colo', 'expire')
                    ->orderBy('id', 'desc')->paginate(20);
        return $colocations;

    }

    public function list_colocation_cancel()
    {
        $colocations = $this->colocation->where('user_id', Auth::user()->id)
                    ->whereIn('status_colo', ['cancel', 'delete_colo', 'change_user'])
                    ->orderBy('id', 'desc')->paginate(20);
        return $colocations;

    }

    public function qtt_vps_nearly($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'cloudzone')
                    ->where('status_vps', 'expire')
                    ->count();

    }

    public function qtt_vps_cancel($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel' , 'delete_vps', 'change_user', 'suspend'])
                    ->count();

    }

    public function qtt_vps_us_nearly($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'us')
                    ->where('status_vps', 'expire')
                    ->count();
    }

    public function qtt_vps_us_cancel($user_id)
    {
        return $this->vps->where('user_id', $user_id)->where('status', 'Active')->where('location', 'us')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->count();
    }

    public function list_vps_nearly_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->orderBy('id', 'desc')
                    ->with('product')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function list_vps_us_nearly_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                    ->where(function($query)
                    {
                        return $query
                          // ->orwhere('status_vps', 'cancel')
                          ->orWhere('status_vps', 'expire');
                    })
                    ->orderBy('id', 'desc')
                    ->with('product')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function search_vps_nearly($q, $sort, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        if (!empty($sort)) {
          if ( $sort == 'DESC' || $sort == 'ASC' ) {
            $sort = $this->mysql_real_escape_string($sort);
          } else {
            $sort = $this->mysql_real_escape_string('DESC');
          }
          $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->orderByRaw('next_due_date ' . $sort)
              ->where(function($query)
              {
                  return $query
                    ->orWhere('status_vps', 'expire');
              })
              ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('ip', 'LIKE', '%'.$q.'%')
                    ->where('location', 'cloudzone')
                    ->where(function($query)
                    {
                        return $query
                          // ->orwhere('status_vps', 'cancel')
                          ->orWhere('status_vps', 'expire');
                    })
                    ->orderBy('id', 'desc')
                    ->with('product')->paginate($sl);
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function search_multi_vps_nearly($ips)
    {
        $list_vps = [];
        foreach ($ips as $key => $ip) {
            $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('ip', trim($ip))
                      ->where('location', 'cloudzone')
                      ->where(function($query)
                      {
                          return $query
                            // ->orwhere('status_vps', 'cancel')
                            ->orWhere('status_vps', 'expire');
                      })
                      ->orderBy('id', 'desc')
                      ->with('product')->first();
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          if ( isset($vps) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer))
                    if(!empty($vps->customer->customer_name)) {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                    }
                    else {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                    }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'cancel') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }

    public function search_multi_vps_us_nearly($ips)
    {
        $list_vps = [];
        foreach ($ips as $key => $ip) {
            $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('ip', trim($ip))
                      ->where('location', 'us')
                      ->where(function($query)
                      {
                          return $query
                            // ->orwhere('status_vps', 'cancel')
                            ->orWhere('status_vps', 'expire');
                      })
                      ->orderBy('id', 'desc')
                      ->with('product')->first();
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          if ( isset($vps) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer))
                    if(!empty($vps->customer->customer_name)) {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                    }
                    else {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                    }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'cancel') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }

    public function search_multi_vps_cancel($ips)
    {
        $list_vps = [];
        foreach ($ips as $key => $ip) {
          $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('ip', trim($ip))
                    ->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->orderBy('id', 'desc')
                    ->with('product')->first();
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          if ( isset($vps) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer))
                    if(!empty($vps->customer->customer_name)) {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                    }
                    else {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                    }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'cancel') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }

    public function search_multi_vps_us_cancel($ips)
    {
        $list_vps = [];
        foreach ($ips as $key => $ip) {
          $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('ip', trim($ip))
                    ->where('location', 'us')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->orderBy('id', 'desc')
                    ->with('product')->first();
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          if ( isset($vps) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer))
                    if(!empty($vps->customer->customer_name)) {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                    }
                    else {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                    }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'cancel') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }

    public function search_vps_cancel($q, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('ip', 'LIKE', '%'.$q.'%')
                    ->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->with('product')->orderBy('id', 'desc')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function search_vps_us_nearly($q, $sort, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orWhere('status_vps', 'expire');
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'us')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->orWhere('status_vps', 'expire');
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function search_vps_us_cancel($q, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('ip', 'LIKE', '%'.$q.'%')
                    ->where('location', 'us')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->with('product')->orderBy('id', 'desc')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }

    public function list_hosting_nearly()
    {
        $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                    ->where('status_hosting', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(30);
        return $list_hosting;

    }

    public function list_hosting_cancel()
    {
        $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                    ->whereIn('status_hosting', ['delete', 'cancel', 'change_user'])
                    ->with('product')->orderBy('id', 'desc')->paginate(30);
        return $list_hosting;

    }

    public function list_email_hosting_nearly()
    {
        $list_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                    ->where(function($query)
                    {
                        return $query
                          ->orwhere('status_hosting', 'cancel')
                          ->orwhere('status_hosting', 'admin_off')
                          ->orWhere('status_hosting', 'expire');
                    })
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_hosting;

    }

    public function qtt_hosting_nearly($user_id)
    {
        $list_hosting = $this->hosting->where('user_id', $user_id)
                    ->where('status_hosting', 'expire')
                    ->count();
        return $list_hosting;

    }

    public function qtt_email_hosting_nearly($user_id)
    {
        $list_hosting = $this->email_hosting->where('user_id', $user_id)
                    ->where(function($query)
                    {
                        return $query
                          ->whereIn('status_hosting', ['admin_off', 'cancel', 'expire']);
                    })
                    ->count();
        return $list_hosting;

    }

    public function qtt_server_nearly($user_id)
    {
        $list = $this->server->where('user_id', $user_id)
                    ->where('status_server', 'expire')
                    ->count();
        return $list;

    }

    public function qtt_server_cancel($user_id)
    {
        $list = $this->server->where('user_id', $user_id)
                    ->whereIn('status_server', ['cancel', 'delete_server', 'change_user'])
                    ->count();
        return $list;

    }

    public function qtt_hosting_cancel($user_id)
    {
        $list = $this->hosting->where('user_id', $user_id)
                    ->whereIn('status_hosting', ['cancel', 'delete', 'change_user'])
                    ->count();
        return $list;

    }

    public function qtt_colocation_nearly($user_id)
    {
        $list_hosting = $this->colocation->where('user_id', $user_id)
                    ->where('status_colo', 'expire')
                    ->count();
        return $list_hosting;

    }

    public function qtt_colocation_cancel($user_id)
    {
        $list_hosting = $this->colocation->where('user_id', $user_id)
                    ->whereIn('status_colo', ['cancel', 'delete_colo', 'change_user'])
                    ->count();
        return $list_hosting;

    }

    public function list_hosting_nearly_with_sl($sl)
    {
      $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                  ->where(function($query)
                  {
                      return $query
                        ->orwhere('status_hosting', 'cancel')
                        ->orwhere('status_hosting', 'admin_off')
                        ->orWhere('status_hosting', 'expire');
                  })
                  ->with('product')->orderBy('id', 'desc')->paginate($sl);
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_hosting as $key => $hosting) {
         //  cấu hình
         $product = $hosting->product;
         $hosting->product_name = $product->name;
         // khách hàng
         if (!empty($hosting->ma_kh)) {
             if(!empty($hosting->customer)) {
                 if(!empty($hosting->customer->customer_name)) {
                   $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_name;
                 }
                 else {
                   $hosting->link_customer =  $hosting->customer->ma_customer . ' - ' .  $hosting->customer->customer_tc_name;
                 }
             }
             else {
               $hosting->customer_name = 'Chính tôi';
             }
         }
         else {
             $customer = $this->get_customer_with_hosting($hosting->id);
             if(!empty($customer)) {
                 if(!empty($customer->customer_name)) {
                   $hosting->customer_name = $customer->ma_customer . ' - ' . $customer->customer_name;
                 } else {
                   $hosting->customer_name =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                 }
             } else {
                 $hosting->customer_name = 'Chính tôi';
             }
         }
         // ngày tạo
         $hosting->date_create = date('d-m-Y', strtotime($hosting->date_create));
         // ngày kết thúc
         $hosting->next_due_date = date('d-m-Y', strtotime($hosting->next_due_date));
         // thời gian thuê
         $hosting->text_billing_cycle = $billing[$hosting->billing_cycle];
         // kiểm tra vps gần hết hạn hay đã hết hạn
         $isExpire = false;
         $expired = false;
         if(!empty($hosting->next_due_date)){
             $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
             $date = date('Y-m-d');
             $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
             if($next_due_date <= $date) {
               $isExpire = true;
             }
             $date_now = strtotime(date('Y-m-d'));
             if ($next_due_date < $date_now) {
                 $expired = true;
             }
         }
         $hosting->isExpire = $isExpire;
         $hosting->expired = $expired;
         // trạng thái vps
         $data['data'][] = $hosting;
      }
      return $data;

    }


    public function list_server_pending()
    {
        return $this->server->where('user_id', Auth::user()->id)->with('product')->where('status', 'Pending')->orderBy('id', 'desc')->paginate(20);
    }

    public function list_hosting_pending()
    {
        return $this->hosting->where('user_id', Auth::user()->id)->with('product')->where('status', 'Pending')->orderBy('id', 'desc')->paginate(20);
    }

    public function detail_vps($id)
    {
        return $this->vps->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    public function detail_hosting($id)
    {
        return $this->hosting->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    public function detail_server($id)
    {
        return $this->server->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    public function detail_proxy($id)
    {
        return $this->proxy->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    public function colocation_detail($id)
    {
        return $this->colocation->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    public function email_hosting_detail($id)
    {
        return $this->email_hosting->where('id', $id)->where('user_id', Auth::user()->id)->first();
    }

    /*
    * HOSTING
    */
    //user huy dich vu
    public function terminated_hosting($id)
    {
        $hosting = $this->hosting->where('user_id', Auth::user()->id )->where('id', $id)->first();
        if ( isset($hosting) ) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'hủy',
            'model' => 'User/Services_Hosting',
            'description' => 'dịch vụ Hosting <a target="_blank" target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
            'description_user' => 'dịch vụ Hosting <a target="_blank" target="_blank" href="/service/detail/' . $hosting->id . '?type=hosting">' . $hosting->domain . '</a> ',
            'service' => $hosting->domain,
          ];
          $this->log_activity->create($data_log);

          $terminated = $this->da->suspendHosting($hosting);
          return $terminated;
        } else {
          return false;
        }
    }
    //user huy dich vu
    public function terminated_vps($id)
    {
        $vps = $this->vps->where('user_id', Auth::user()->id )->where('id', $id)->first();
        if ( isset($vps) ) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'hủy',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
            'service' => $vps->ip,
          ];
          $this->log_activity->create($data_log);
          if ( $vps->vm_id < 1000000000 ) {
            $terminated = $this->dashboard->terminatedVps($vps);
          } else {
            $terminated = $this->dashboard->terminatedVpsUS($vps);
          }

          // $terminated = true;
          return $terminated;
        } else {
          return false;
        }
    }
    // user bat dich vu
    public function unSuspend($id)
    {
        $hosting = $this->hosting->find($id);
        if ($hosting->location == 'vn') {
           $unsuppend = $this->da->unSuspend($hosting);
        } else {
           $unsuppend = $this->da_si->unSuspend($hosting);
        }
        return $unsuppend;
    }
    // user tat dich vu
    public function suspend_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_Hosting',
          'description' => ' dịch vụ Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
          'description_user' => ' dịch vụ Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=hosting">' . $hosting->domain . '</a> ',
          'service' => $hosting->domain,
        ];
        $this->log_activity->create($data_log);

        if ($hosting->location == 'vn') {
            $suppend = $this->da->off($hosting);
        } else {
            $suppend = $this->da_si->off($hosting);
        }
        return $suppend;
    }
    // Gia han 1 hosting
    public function expired_hosting($id, $billing_cycle)
    {
        $hosting = $this->hosting->find($id);
        if (!empty($hosting)) {
            $product = $this->product->find($hosting->product_id);
            $invoiced = $hosting->detail_order;
            if (!empty($hosting->price_override)) {
              $total = $hosting->price_override;
            } else {
              $total = $product->pricing[$billing_cycle];
            }
            // create order
            $data_order = [
                'user_id' => Auth::user()->id,
                'total' =>  $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',//thiếu status gia hạn
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
            ];
            $order = $this->order->create($data_order);
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'hosting',
                'due_date' => $date,
                'description' => 'Gia hạn',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => Auth::user()->id,
                'addon_id' => 0,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // create history pay
            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => Auth::user()->id,
                'ma_gd' => 'GDEXH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $history_pay = $this->history_pay->create($data_history);
            // gui mail
            if ($order && $detail_order && $data_history) {
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $hosting->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'hosting',
                ];
                $this->order_expired->create($data_order_expired);

                $hosting->expired_id = $detail_order->id;
                $hosting->save();

                $email = $this->email->find($product->meta_product->email_expired);

                $billings = config('billing');
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn hosting'
                ];
                // try {
                //   $data_tb_send_mail = [
                //     'type' => 'mail_order_expire_hosting',
                //     'type_service' => 'hosting',
                //     'user_id' => $user->id,
                //     'status' => false,
                //     'content' => serialize($data),
                //   ];
                //   $this->send_mail->create($data_tb_send_mail);
                // } catch (\Exception $e) {
                //   report($e);
                //   return $detail_order->id;
                // }

                return $detail_order->id;
            }
        } else {
          return false;
        }
    }
    // gia hạn nhiều hostings
    public function expired_hostings($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        foreach ($list_id as $key => $id) {
            $hosting = $this->hosting->find($id);
            if (!empty($hosting)) {
                $product = $this->product->find($hosting->product_id);
                if ($hosting->billing_cycle == $billing_cycle) {
                    $total += !empty($hosting->price_override) ? $hosting->price_override : $product->pricing[$billing_cycle];
                } else {
                    $total += $product->pricing[$billing_cycle];
                }

            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'hosting',
            'due_date' => $date,
            'description' => 'Gia hạn',
            'status' => 'unpaid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => Auth::user()->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            $hosting = $this->hosting->find($id);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'hosting',
            ];
            $order_expired = $this->order_expired->create($data_order_expired);

            if (!empty($hosting)) {
                $hosting->expired_id = $detail_order->id;
                $hosting->save();
            }
            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => ' gia hạn Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
              'description_user' => ' gia hạn Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=hosting">' . $hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
              'service' => $hosting->domain,
            ];
            $this->log_activity->create($data_log);
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXH' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn hosting'
            ];
            // try {
            //   $data_tb_send_mail = [
            //     'type' => 'mail_order_expire_hosting',
            //     'type_service' => 'hosting',
            //     'user_id' => $user->id,
            //     'status' => false,
            //     'content' => serialize($data),
            //   ];
            //   $this->send_mail->create($data_tb_send_mail);
            // } catch (\Exception $e) {
            //   report($e);
            //   return $detail_order->id;
            // }


            return $detail_order->id;
        } else {
            return false;
        }

    }
    // Gia han 1 email hosting
    public function expired_email_hosting($id, $billing_cycle)
    {
        $email_hosting = $this->email_hosting->find($id);
        if (!empty($email_hosting)) {
            $product = $this->product->find($email_hosting->product_id);
            $invoiced = $email_hosting->detail_order;
            if (!empty($email_hosting->price_override)) {
              $total = $email_hosting->price_override;
            } else {
              $total = $product->pricing[$billing_cycle];
            }
            // create order
            $data_order = [
                'user_id' => Auth::user()->id,
                'total' =>  $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',//thiếu status gia hạn
                'type' => 'expired',
            ];
            $order = $this->order->create($data_order);
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Email Hosting',
                'due_date' => $date,
                'description' => 'Gia hạn',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => Auth::user()->id,
                'addon_id' => 0,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // create history pay
            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => Auth::user()->id,
                'ma_gd' => 'GDEEM' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $history_pay = $this->history_pay->create($data_history);
            // gui mail
            if ($order && $detail_order && $data_history) {
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $email_hosting->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'EmailHosting',
                ];
                $this->order_expired->create($data_order_expired);
                $data_restorge = [
                    'status' => true,
                    'invoice_id' => $detail_order->id,
                ];

                $email = $this->email->find($product->meta_product->email_expired);

                $billings = config('billing');
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $email_hosting->domain, '' ,$billings[$billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                ];


                try {
                  $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn Email Hosting', $data);
                } catch (\Exception $e) {
                  return $data_restorge;
                }

                return $data_restorge;
            }
        } else {
          $data_restorge = [
              'status' => false,
              'invoice_id' => '',
          ];
          return $data_restorge;
        }
    }
    // gia hạn nhiều hostings
    public function expired_list_email_hosting($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        foreach ($list_id as $key => $id) {
            $email_hosting = $this->email_hosting->find($id);
            if (!empty($email_hosting)) {
                $product = $this->product->find($email_hosting->product_id);
                if ($email_hosting->billing_cycle == $billing_cycle) {
                    $total += !empty($email_hosting->price_override) ? $email_hosting->price_override : $product->pricing[$billing_cycle];
                } else {
                    $total += $product->pricing[$billing_cycle];
                }

            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'Email Hosting',
            'due_date' => $date,
            'description' => 'Gia hạn',
            'status' => 'unpaid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => Auth::user()->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            $email_hosting = $this->email_hosting->find($id);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'EmailHosting',
            ];
            $order_expired = $this->order_expired->create($data_order_expired);

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => ' gia hạn Hosting <a target="_blank" href="/admin/hostings/detail/' . $email_hosting->id . '">' . $email_hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
              'description_user' => ' gia hạn Hosting <a target="_blank" href="/service/detail/' . $email_hosting->id . '?type=hosting">' . $email_hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
              'service' => $email_hosting->domain,
            ];
            $this->log_activity->create($data_log);
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEEM' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        $data_restorge = [
            'status' => true,
            'invoice_id' => $detail_order->id,
        ];
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $email_hosting->domain, '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
            ];
            try {
              $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn hosting', $data);
            } catch (\Exception $e) {
              return $data_restorge;
            }


            return $data_restorge;
        } else {
            $data_restorge = [
                'status' => false,
                'invoice_id' => '',
            ];
            return $data_restorge;
        }

    }
    /*
    * VPS
    */
    // bật VPS
    public function onVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->onVPS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'bật',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    // Auto gia hạn VPS
    public function autoRefurnVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ($vps->status_vps == 'delete_vps') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ($vps->status_vps == 'change_user') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $vps->auto_refurn = true;

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cài đặt',
          'model' => 'User/Services_VPS',
          'description' => ' tự động gia hạn dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' tự động gia hạn dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $vps->save();
    }
    // Auto gia hạn VPS
    public function offAutoRefurnVPS($id)
    {
        $vps = $this->vps->find($id);

        $vps->auto_refurn = false;

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cài đặt',
          'model' => 'User/Services_VPS',
          'description' => ' tắt tự động gia hạn dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' tắt tự động gia hạn dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $vps->save();
    }
    // tắt VPS
    public function offVPS($id)
    {
        $vps = $this->vps->find($id);
        $off = $this->dashboard->offVPS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $off;
    }
    // bật VPS
    public function restartVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }
        $on = $this->dashboard->restartVPS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'khởi động lại',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $on;
    }
    // bật VPS
    public function terminatedVps($id)
    {
        $vps = $this->vps->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_VPS',
          'description' => 'dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->domain . '</a> ',
          'description_user' => 'dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->domain . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        if ( $vps->vm_id < 1000000000 ) {
          $on = $this->dashboard->terminatedVps($vps);
        }
        else {
          $on = $this->dashboard->terminatedVpsUS($vps);
        }
        // $on = true;
        return $on;
    }
    /*
    * VPS US
    */
    // bật VPS
    public function onVPSUS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->onVPSUS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'bật',
          'model' => 'User/Services_VPS_US',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    // bật VPS
    public function restartVPSUS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->restartVPSUS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'Khởi động lại',
          'model' => 'User/Services_VPS_US',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    // tắt VPS
    public function offVPSUS($id)
    {
        $vps = $this->vps->find($id);
        $off = $this->dashboard->offVPSUS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $off;
    }
    // bật VPS
    public function terminatedVpsUS($id)
    {
        $vps = $this->vps->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_VPS_US',
          'description' => 'dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->domain . '</a> ',
          'description_user' => 'dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->domain . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        $on = $this->dashboard->terminatedVpsUS($vps);
        // $on = true;
        return $on;
    }
    // Hủy Server
    public function terminatedServer($id)
    {
        $server = $this->server->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_Server',
          'description' => 'dịch vụ Server <a target="_blank" href="/admin/servers/detail/' . $server->id . '">' . $server->ip . '</a> ',
          'description_user' => 'dịch vụ Server <a target="_blank" href="/service/server/detail/' . $server->id . '">' . $server->ip . '</a> ',
          'service' =>  $server->ip,
        ];
        $this->log_activity->create($data_log);
        $server->status_server = 'cancel';
        $terminated = $server->save();

        // $on = true;
        return $terminated;
    }
    // Hủy Colocation
    public function terminatedColocation($id)
    {
        $colocation = $this->colocation->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_Colocation',
          'description' => 'dịch vụ Colocation <a target="_blank" href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> ',
          'description_user' => 'dịch vụ Colocation <a target="_blank" href="/service/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> ',
          'service' =>  $colocation->ip,
        ];
        $this->log_activity->create($data_log);
        $colocation->status_colo = 'cancel';
        $terminated = $colocation->save();

        // $on = true;
        return $terminated;
    }
    // Hủy Email Hosting
    public function terminatedEmailHosting($id)
    {
        $email_hosting = $this->email_hosting->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_Email_Hosting',
          'description' => 'dịch vụ Email Hosting <a target="_blank" href="/admin/email_hosting/detail/' . $email_hosting->id . '">' . $email_hosting->domain . '</a> ',
          'description_user' => 'dịch vụ Email Hosting <a target="_blank" href="/service/email_hosting/detail/' . $email_hosting->id . '">' . $email_hosting->domain . '</a> ',
          'service' =>  $email_hosting->domain,
        ];
        $this->log_activity->create($data_log);
        $email_hosting->status_hosting = 'cancel';
        $terminated = $email_hosting->save();

        // $on = true;
        return $terminated;
    }
    // loadVpsWithIds($ids)
    public function loadVpsWithIds($ids)
    {
        // Log::info($ids);
        $data = [];
        foreach ($ids as $key => $id) {
            $data[] = $this->vps->find($id);
        }
        return $data;
    }
    // login_as_directadmin($id)
    public function login_as_directadmin($id)
    {
        $hosting = $this->hosting->find($id);
        $login_as = $this->da->login_as($hosting->user, $hosting->password);
        return $login_as;
    }
    // gia hạn 1 vps
    public function expired_vps($id, $billing_cycle)
    {

        $vps = $this->vps->find($id);
        if (!empty($vps)) {
            $product = $this->product->find($vps->product_id);
            $invoiced = $vps->detail_order;
            $total = 0;
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ( $vps->billing_cycle == $billing_cycle ) {
                      $total += $vps->price_override;
                    } else {
                      $billing_price = config('billingDashBoard');
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total = (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }

            $data_order = [
                'user_id' => Auth::user()->id,
                'total' => $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : '',
            ];
            $order = $this->order->create($data_order);

            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            if ($vps->location == 'cloudzone') {
              $data_order_detail = [
                  'order_id' => $order->id,
                  'type' => 'VPS',
                  'due_date' => $date,
                  'description' => 'Gia hạn',
                  'status' => 'unpaid',
                  'sub_total' => $total,
                  'quantity' => 1,
                  'user_id' => Auth::user()->id,
                  'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
              ];
            } else {
              $data_order_detail = [
                  'order_id' => $order->id,
                  'type' => 'VPS US',
                  'due_date' => $date,
                  'description' => 'Gia hạn',
                  'status' => 'unpaid',
                  'sub_total' => $total,
                  'quantity' => 1,
                  'user_id' => Auth::user()->id,
                  'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
              ];
            }

            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $vps->expired_id = $detail_order->id;
            $vps->save();

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'description_user' => ' gia hạn VPS <a target="_blank"  href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $email = $this->email->find($product->meta_product->email_expired);
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps',
            ];
            // try {
            //   $data_tb_send_mail = [
            //     'type' => 'mail_order_expire_vps',
            //     'type_service' => 'vps',
            //     'user_id' => $user->id,
            //     'status' => false,
            //     'content' => serialize($data),
            //   ];
            //   $this->send_mail->create($data_tb_send_mail);
            // } catch (\Exception $e) {
            //   report($e);
            //   return $detail_order->id;
            // }

            return $detail_order->id;

        } else {
            return false;
        }
    }

    public function get_total_server( $id, $billing_cycle )
    {
      $server = $this->server->find($id);
      $total = 0;
      $product = $server->product;
      $total = $product->pricing[ $billing_cycle ];
      if ( !empty($server->server_config) ) {
        $server_config = $server->server_config;
        $pricing_addon = 0;
        if ( !empty($server_config->ram) ) {
          foreach ($server->server_config_rams as $server_config_ram) {
            $pricing_addon += $server_config_ram->product->pricing[$billing_cycle];
          }
        }
        if ( !empty( $server_config->ip ) ) {
            foreach ($server->server_config_ips as $server_config_ip) {
                $pricing_addon += !empty($server_config_ip->product->pricing[$billing_cycle]) ? $server_config_ip->product->pricing[$billing_cycle] : 0;
            }
        }
        if ( !empty($server_config->disk2) ) {
          $pricing_addon += $server_config->product_disk2->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk3) ) {
          $pricing_addon += $server_config->product_disk3->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk4) ) {
          $pricing_addon += $server_config->product_disk4->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk5) ) {
          $pricing_addon += $server_config->product_disk5->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk6) ) {
          $pricing_addon += $server_config->product_disk6->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk7) ) {
          $pricing_addon += $server_config->product_disk7->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk8) ) {
          $pricing_addon += $server_config->product_disk8->pricing[$billing_cycle];
        }
        $total += $pricing_addon;
      }
      return $total;
    }

    // gia hạn nhieu server
    public function expired_list_server($list_id, $billing_cycle)
    {
        $totals = 0;
        $billings = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);
          if ( !empty($server->amount) ) {
            if ( $server->billing_cycle == $billing_cycle ) {
                $totals += $server->amount;
            } else {
              $totals += $this->get_total_server( $id, $billing_cycle );
            }
          } else {
            $totals += $this->get_total_server( $id, $billing_cycle );
          }
        }
        // dd($totals);
        $data_order = [
          'user_id' => Auth::user()->id,
          'total' => $totals,
          'status' => 'Pending',
          'description' => 'Gia hạn Server',
          'type' => 'expired',
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Server',
          'due_date' => $date,
          'description' => 'Gia hạn',
          'status' => 'unpaid',
          'sub_total' => $totals,
          'quantity' => count($list_id),
          'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);

        $user = $this->user->find(Auth::user()->id);
        $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXS' . strtoupper(substr(sha1(time()), 34, 39)),
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $totals,
          'status' => 'Pending',
          'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);

        $billings = config('billing');
        // Tạo lưu trữ khi gia hạn
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);

          $data_order_expired = [
            'detail_order_id' => $detail_order->id,
            'expired_id' => $id,
            'billing_cycle' => $billing_cycle,
            'type' => 'server',
          ];
          $this->order_expired->create($data_order_expired);
          // ghi log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/HistoryPay',
            'description' => ' gia hạn Server <a target="_blank" href="/admin/servers/detail/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'description_user' => ' gia hạn Server <a target="_blank" href="/service/server/detail/' . $server->id . '?type=vps">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'service' =>  $server->ip,
          ];
          $this->log_activity->create($data_log);
        }
        $data = [
          'status' => true,
          'invoice_id' => $detail_order->id,
        ];
        return $data;
    }

    // gia hạn 1 colocation
    public function expired_colocation($id, $billing_cycle)
    {
        $colocation = $this->colocation->find($id);
        if (!empty($colocation)) {
            $total = 0;
            $billing_price = config('billingDashBoard');
            $product = $this->product->find($colocation->product_id);
            if (!empty($colocation->amount)) {
                if ($colocation->billing_cycle == $billing_cycle) {
                  $total = $colocation->amount;
                } else {
                  $total_price_1 = ($colocation->amount * $billing_price[$billing_cycle]) / $billing_price[$colocation->billing_cycle];
                  $total = (int) round( $total_price_1 , -3);
                }
            } else {
                $total += $product->pricing[$billing_cycle];
                foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                  $total += !empty( $colocation_config_ip->product->pricing[$billing_cycle] ) ? $colocation_config_ip->product->pricing[$billing_cycle] : 0;
                }
            }

            $data_order = [
                'user_id' => Auth::user()->id,
                'total' => $total,
                'status' => 'Pending',
                'description' => 'Gia hạn Colocation',
                'type' => 'expired',
            ];
            $order = $this->order->create($data_order);

            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Colocation',
                'due_date' => $date,
                'description' => 'Gia hạn Colocation',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => Auth::user()->id,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'colocation',
            ];
            $this->order_expired->create($data_order_expired);

            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXC' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn Colocation <a target="_blank" href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'description_user' => ' gia hạn Colocation <a target="_blank" href="/service/colocation/detail/' . $colocation->id . '?type=vps">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'service' =>  $colocation->ip,
            ];
            $this->log_activity->create($data_log);

            // $email = $this->email->find($product->meta_product->email_expired);
            // $url = url('');
            // $url = str_replace(['http://','https://'], ['', ''], $url);
            // $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            // $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$vps->billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            // $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            // $data = [
            //     'content' => $content,
            //     'user_name' => $user->name,
            // ];
            // try {
            //   $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps', $data);
            // } catch (\Exception $e) {
            //   return $detail_order->id;
            // }
            $data = [
                'status' => true,
                'invoice_id' => $detail_order->id,
            ];
            return $data;

        } else {
            $data = [
                'status' => false,
            ];
            return $data;
        }
    }

    // gia hạn nhieu colocation
    public function expired_list_colocation($list_id, $billing_cycle)
    {
        $totals = 0;
        $billings = config('billingDashBoard');
        $billing_price = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
          $colocation = $this->colocation->find($id);
          if (!empty($colocation)) {
            $product = $this->product->find($colocation->product_id);
            if (!empty($colocation->amount)) {
                if ($colocation->billing_cycle == $billing_cycle) {
                    $totals += $colocation->amount;
                } else {
                  $total_price_1 = ($colocation->amount * $billing_price[$billing_cycle]) / $billing_price[$colocation->billing_cycle];
                  $totals += (int) round( $total_price_1 , -3);
                }
            } else {
                $totals += $product->pricing[$billing_cycle];
                foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                  $totals += !empty( $colocation_config_ip->product->pricing[$billing_cycle] ) ? $colocation_config_ip->product->pricing[$billing_cycle] : 0;
                }
            }
          } else {
            $data = [
              'status' => false,
            ];
            return $data;
          }
        }
        $data_order = [
          'user_id' => Auth::user()->id,
          'total' => $totals,
          'status' => 'Pending',
          'description' => 'Gia hạn Colocation',
          'type' => 'expired',
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Colocation',
          'due_date' => $date,
          'description' => 'Gia hạn',
          'status' => 'unpaid',
          'sub_total' => $totals,
          'quantity' => count($list_id),
          'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);

        $user = $this->user->find(Auth::user()->id);
        $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXC' . strtoupper(substr(sha1(time()), 34, 39)),
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $totals,
          'status' => 'Pending',
          'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);

        $billings = config('billing');
        // Tạo lưu trữ khi gia hạn
        foreach ($list_id as $key => $id) {
          $colocation = $this->colocation->find($id);

          $data_order_expired = [
            'detail_order_id' => $detail_order->id,
            'expired_id' => $id,
            'billing_cycle' => $billing_cycle,
            'type' => 'colocation',
          ];
          $this->order_expired->create($data_order_expired);
          // ghi log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/HistoryPay',
            'description' => ' gia hạn Colocation <a target="_blank" href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'description_user' => ' gia hạn Colocation <a target="_blank" <a href="/service/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'service' =>  $colocation->ip,
          ];
          $this->log_activity->create($data_log);
        }


            // $email = $this->email->find($product->meta_product->email_expired);
            // $url = url('');
            // $url = str_replace(['http://','https://'], ['', ''], $url);
            // $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            // $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$vps->billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            // $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            // $data = [
            //     'content' => $content,
            //     'user_name' => $user->name,
            // ];
            // try {
            //   $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps', $data);
            // } catch (\Exception $e) {
            //   return $detail_order->id;
            // }
        $data = [
          'status' => true,
          'invoice_id' => $detail_order->id,
        ];
        return $data;
    }

    // gia hạn nhiều vps
    public function gia_han_nhieu_vps($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        if ($vps->location == 'cloudzone') {
          $data_order_detail = [
              'order_id' => $order->id,
              'type' => 'VPS',
              'due_date' => $date,
              'description' => 'Gia hạn',
              'status' => 'unpaid',
              'sub_total' => $total / $quantity,
              'quantity' => $quantity,
              'user_id' => Auth::user()->id,
              'addon_id' => 0,
          ];
        }
        else {
          $data_order_detail = [
              'order_id' => $order->id,
              'type' => 'VPS US',
              'due_date' => $date,
              'description' => 'Gia hạn',
              'status' => 'unpaid',
              'sub_total' => $total / $quantity,
              'quantity' => $quantity,
              'user_id' => Auth::user()->id,
              'addon_id' => 0,
          ];
        }
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            // if (!empty($vps)) {
            //     $vps->expired_id = $detail_order->id;
            //     $vps->billing_cycle = $billing_cycle;
            //     $vps->save();
            // }
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');
            // ghi log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt hàng',
                'model' => 'User/Order_Services',
                'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'description_user' => ' gia hạn VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (Exception $e) {
              report($e);
            }

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps',
            ];
            // try {
            //   $data_tb_send_mail = [
            //     'type' => 'mail_order_expire_vps',
            //     'type_service' => 'vps',
            //     'user_id' => $user->id,
            //     'status' => false,
            //     'content' => serialize($data),
            //   ];
            //   $this->send_mail->create($data_tb_send_mail);
            // } catch (\Exception $e) {
            //   report($e);
            //   return $detail_order->id;
            // }

            return $detail_order->id;
        } else {
            return false;
        }
    }

    public function send_mail($subject, $data)
    {
        $this->user_email = Auth::user()->email;
        $this->subject = $subject;
        $mail = Mail::send('users.mails.orders', $data, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to($this->user_email)->subject($this->subject);
        });
    }

    public function checkTimeAddonVps($vps)
    {
      $now = Carbon::now();
      $next_due_date = new Carbon($vps->next_due_date);
      if ($next_due_date->diffInMonths($now) == 0) {
        return [
          'month' => 0,
          'day' => $next_due_date->diffInDays($now) + 1,
        ];
      } else {
        $month = $next_due_date->diffInMonths($now);
        $now = $now->addMonths($next_due_date->diffInMonths($now));
        return [
          'month' => $month,
          'day' => $next_due_date->diffInDays($now) + 1
        ];
      }
    }

    public function order_addon_vps($data)
    {
        $vps_id = $data['vpsId'];
        $id_addon_cpu = $data['id-addon-cpu'];
        $id_addon_ram = $data['id-addon-ram'];
        $id_addon_disk = $data['id-addon-disk'];
        $qtt_addon_cpu = $data['addon-cpu'];
        $qtt_addon_ram = $data['addon-ram'];
        $qtt_addon_disk = $data['addon-disk'];
        $billing_cycle = 'monthly';
        $product_addon_cpu = $this->product->find($id_addon_cpu);
        $product_addon_ram = $this->product->find($id_addon_ram);
        $product_addon_disk = $this->product->find($id_addon_disk);
        $total = 0;
        $vps = $this->vps->find($vps_id);

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        // dd($now , $next_due_date);
        if ($qtt_addon_cpu > 0) {
            if ($next_due_date->diffInMonths($now) == 0) {
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total += (int) round( $day * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle] , -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths($next_due_date->diffInMonths($now));
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total +=  $month * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle]  + (int) round($day * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle], -3);
            }
        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($qtt_addon_ram > 0) {

            if ($next_due_date->diffInMonths($now) == 0) {
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total += (int) round($day * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle], -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths($next_due_date->diffInMonths($now));
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total +=  $month * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle]  + (int) round($day * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle], -3);
            }

        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($qtt_addon_disk > 0) {

            if ($next_due_date->diffInMonths($now) == 0) {
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total += (int) round($day * $qtt_addon_disk / 10 * $product_addon_disk->pricing[$billing_cycle], -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths($next_due_date->diffInMonths($now));
              $day = ( $next_due_date->diffInDays($now) + 1 ) > 15 ? 1 : 0.5;
              $total +=  $month * $qtt_addon_disk * $product_addon_disk->pricing[$billing_cycle] / 10  + (int) round($day * $qtt_addon_disk * $product_addon_disk->pricing[$billing_cycle], -3) / 10;
            }

        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($next_due_date->diffInMonths($now) == 0) {
          $day = ( $next_due_date->diffInDays($now) + 1 );
          $month = 0;
        } else {
          $month = $next_due_date->diffInMonths($now);
          $now = $now->addMonths($next_due_date->diffInMonths($now));
          $day = ( $next_due_date->diffInDays($now) + 1 );
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'addon vps',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'addon_vps',
            'due_date' => $date,
            'description' => 'addon vps',
            'status' => 'unpaid',
            'sub_total' => $total,
            'quantity' => 1,
            'user_id' => Auth::user()->id,
            'addon_id' => '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDADV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '4',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_order_addon_vps = [
          'detail_order_id'  => $detail_order->id,
          'vps_id' => $vps_id,
          'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
          'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
          'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
          'ip' => !empty($qtt_addon_ip) ? $qtt_addon_ip : 0,
          'month' => $month,
          'day' => $day,
        ];
        $this->order_addon_vps->create($data_order_addon_vps);
        $this->user_email = $user->email;
        $this->subject = 'Xác nhận đặt hàng nâng cấp cấu hình VPS';

        if (!empty($month)) {
            $time = $month . ' Tháng ' . $day . ' Ngày';
        } else {
            $time = $day . ' Ngày';
        }

        $data = [
            'name' => $user->name,
            'amount' => $total,
            'ip' => $vps->ip,
            'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
            'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
            'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
            'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
            'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
            'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
            'billing_cycle' => $billing_cycle,
            'time' =>  $time,
            'subject' => 'Xác nhận đặt hàng nâng cấp cấu hình VPS'
        ];
        $text_addon = !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0;
        $text_addon .= ' CPU - ';
        $text_addon .= !empty($qtt_addon_ram) ? $qtt_addon_ram : 0;
        $text_addon .= ' RAM - ';
        $text_addon .= !empty($qtt_addon_disk) ? $qtt_addon_disk : 0;
        $text_addon .= ' DISK';
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'đặt hàng',
          'model' => 'User/HistoryPay',
          'description' => ' addon VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ' . $text_addon ,
          'description_user' => ' addon VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ' . $text_addon ,
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        // try {
        //   $data_tb_send_mail = [
        //     'type' => 'order_addon_vps',
        //     'type_service' => 'vps',
        //     'user_id' => $user->id,
        //     'status' => false,
        //     'content' => serialize($data),
        //   ];
        //   $this->send_mail->create($data_tb_send_mail);
        // } catch (\Exception $e) {
        //     report($e);
        //     return $detail_order->id;
        // }

        return $detail_order->id;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    // loadCusomter
    public function loadCusomter()
    {
        $customers = $this->customer->where('user_id', Auth::user()->id)->get();
        return $customers;
    }
    // updateCusomter
    public function updateCusomter($data)
    {
        $vps = $this->vps->find($data['vps_id']);
        if (isset($vps)) {
           $vps->ma_kh = $data['makh'];
           $vps->save();
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'cập nhật',
             'model' => 'User/Services_VPS',
             'description' => ' khách hàng cho VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
             'description_user' => ' khách hàng cho VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
             'service' =>  $vps->ip,
           ];
           $this->log_activity->create($data_log);

           return $vps;
        } else {
           return false;
        }
    }
    // updateDescription
    public function updateDescription($data)
    {
        $vps = $this->vps->find($data['vps_id']);
        if (isset($vps)) {
           $vps->description = $data['description'];
           $vps->save();
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'cập nhật',
             'model' => 'User/Services_VPS',
             'description' => ' ghi chú cho VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
             'description_user' => ' ghi chú cho VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
             'service' =>  $vps->ip,
           ];
           $this->log_activity->create($data_log);
           return $vps;
        } else {
           return false;
        }
    }
    // updateDescriptionProxy
    public function updateDescriptionProxy($data)
    {
        $proxy = $this->proxy->find($data['vps_id']);
        if (isset($proxy)) {
           $proxy->description = $data['description'];
           $proxy->save();
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'cập nhật',
             'model' => 'User/Services',
             'description' => ' ghi chú cho Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a>',
             'description_user' => ' ghi chú cho Proxy <a target="_blank" href="/service/detail/' . $proxy->id . '?type=proxy">' . $proxy->ip . '</a>',
             'service' =>  $proxy->ip,
           ];
           $this->log_activity->create($data_log);
           return $proxy;
        } else {
           return false;
        }
    }
    // rebuild_vps
    public function rebuild_vps($data)
    {
        $list_id = $data['id'];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' bị Admin tắt. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }

            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã bị khóa. Quý khách vui lòng không chọn các VPS có trạng đã khóa và liên hệ lại với chúng tôi để mở lại VPS',
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã hủy. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã chuyển. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'rebuild' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đang cài lại. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }

        }
        $os = $data['os'];
        $security = !empty($data['security']) ? true : false;
        if ( empty($security) ) {
            if ($os == 1) {
                $os = 11;
            }
            else if ($os == 2) {
                $os = 12;
            }
            else if ($os == 3) {
                $os = 13;
            }
            else if ($os == 31) {
                $os = 31;
            }
            else if ($os == 15) {
                $os = 31;
            }
            else {
                $os = 14;
            }
        }

        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $data_rebuild = [
              "mob" => "rebuild",
              "name" => $vps->vm_id,
              "template" => $os,
              'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            $rebuild = $this->dashboard->rebuild_vps($data_rebuild, $vps->vm_id, $vps);
            // $rebuild = true;
            if ($rebuild) {
                $vps->status_vps = 'rebuild';
                $vps->os = $os;
                $vps->security = !empty($security) ? true : false;
                $vps->save();
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'cài đặt lại',
                      'model' => 'User/Order',
                      'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    continue;
                }
            } else {
                $data = [
                  'success' => '',
                  'error' => 'VPS có ip: ' . $vps->ip . ' cài đặt lại không thành công',
                ];
                return $data;
            }
        }

        $data = [
          'success' => true,
        ];
        return $data;

    }

    // rebuild_vps
    public function rebuild_vps_us($data)
    {
        $list_id = $data['id'];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' bị Admin tắt. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }

            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã bị khóa. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã hủy. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã chuyển. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'rebuild' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đang cài lại. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'expire' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã hết hạn. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_ip' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đang đổi IP. Quý khách vui lòng kiểm tra lại',
                  ];
                  return $data;
            }

        }

        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $rebuild = $this->dashboard->rebuild_vps_us($vps);
            // $rebuild = true;
            if ($rebuild) {
                $vps->status_vps = 'rebuild';
                $vps->save();
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'cài đặt lại',
                      'model' => 'User/Order',
                      'description' => ' VPS US <a target="_blank" href="/admin/vps_us/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'description_user' => ' VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    report($e);
                    continue;
                }
            } else {
                $data = [
                  'success' => '',
                  'error' => 'VPS có ip: ' . $vps->ip . ' cài đặt lại không thành công',
                ];
                return $data;
            }
        }

        $data = [
          'success' => true,
        ];
        return $data;

    }

    public function request_change_ip($list_id)
    {
        $data = [
          'success' => [],
          'error' => '',
        ];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'error' => 6
                  ];
                  return $data;
            }
        }
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'error' => 5
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
        }

        $user = Auth::user();
        $product = [];
        if (!empty($user->group_user_id)) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->count() > 0) {
                    $product = $this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->first();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->count() > 0) {
                    $product = $this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->first();
                }
            }
        }
        if (empty($product)) {
            return false;
        }

        $total = $product->pricing->one_time_pay * count($list_id);

        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'change vps',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'change_ip',
            'due_date' => $date,
            'description' => 'change vps',
            'status' => 'unpaid',
            'sub_total' => $product->pricing->one_time_pay,
            'quantity' => count($list_id),
            'user_id' => $user->id,
            'addon_id' => '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '5',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        $string_list_ip_vps = '';
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $string_list_ip_vps = $vps->ip . '<br>';
            $data_change_ip = [
                'detail_order_id' => $detail_order->id,
                'vps_id' => $vps->id,
                'ip' => $vps->ip,
            ];
            $this->order_change_ip->create($data_change_ip);
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order',
              'description' => ' đổi IP VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
              'description_user' => ' đổi IP VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
        }
        $email = $this->email->find($product->meta_product->email_create);
        $this->user_email = $user->email;
        $this->subject = $email->meta_email->subject;
        // Thông tin gữi mail
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$list_ip_vps}', '{$list_ip_changed_vps}'];
        $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' , '', '', $order->created_at, '', '',number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ' ,number_format($total,0,",",".") . ' VNĐ' , count($list_id), '' , '', $url , $string_list_ip_vps , ''];
        $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
        $data = [
            'content' => $content,
            'user_name' => $user->name,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_addpress' => $user->user_meta->address,
            'user_phone' => $user->user_meta->user_phone,
            'order_id' => $order->id,
            'product_name' => $product->name,
            'domain' => '',
            'ip' => '',
            'billing_cycle' => '',
            'next_due_date' => '',
            'order_created_at' => $order->created_at,
            'services_username' => '',
            'services_password' => '',
            'sub_total' => $product->pricing->one_time_pay,
            'total' => $total,
            'qtt' => count($list_id),
            'os' => '',
            'token' => '',
            'security' => '',
            'list_ip_vps' => $string_list_ip_vps,
        ];

        try {
          $mail = Mail::send('users.mails.orders', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to($this->user_email)->subject($this->subject);
          });


          $mail = Mail::send('users.mails.send_mail_change_ip', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to('uthienth92@gmail.com')->subject('Khách hàng yêu cầu thay đổi IP VPS');
              $message->cc('support@cloudzone.vn')->subject('Khách hàng yêu cầu thay đổi IP VPS');
          });
        } catch (\Exception $e) {
          return $detail_order->id;
        }

        return $detail_order->id;
    }

    public function change_ip($list_id)
    {
        $data = [
          'success' => [],
          'error' => '',
        ];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'yêu cầu',
              'model' => 'User/Services_VPS',
              'description' => ' đổi ip cho VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
              'description_user' => ' đổi ip cho VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            // $change_ip = $this->dashboard->change_ip($vps);
            $change_ip  = true;
            if ($change_ip) {
                $vps->ip = $change_ip['ip_change'];
                $vps->save();
                $data['success'][] = $change_ip;
            }
        }
        return $data;
    }

    public function change_ip_vps_us($list_id)
    {
        $data = [
          'success' => [],
          'error' => '',
          'detail_order_id' => 0,
          'check' => 0
        ];
        $check = false;
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'success' => '',
                    'error' => 6
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'success' => '',
                    'error' => 5
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'delete_vps' ) {
                  $data = [
                    'success' => '',
                    'error' => 5
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'expire' ) {
              $data = [
                'success' => '',
                'error' => 7,
              ];
              return $data;
            }
            if ( $vps->status_vps == 'change_ip' ) {
              $data = [
                'success' => '',
                'error' => 8,
              ];
              return $data;
            }
        }

        $user = Auth::user();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
          return false;
        }
        $total = 20000 * count($list_id);
        // kiểm tra tài khoản
        if ( $credit->value <  $total ) {
          $data = [
            'success' => '',
            'error' => 1111,
          ];
          return $data;
        }

        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'change ip vps us',
            'makh' => '',
            'type' => 'vps_us'
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'change_ip',
            'due_date' => $date,
            'description' => 'change ip',
            'status' => 'unpaid',
            'sub_total' => 20000,
            'quantity' => count($list_id),
            'user_id' => $user->id,
            'addon_id' => '0',
            // 'paid_date' => date('Y-m-d'),
            'payment_method' => 1,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '5',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'method_gd_invoice' => 'credit',
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        $string_list_ip_vps = '';
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $string_list_ip_vps = $vps->ip . '<br>';
            $data_change_ip = [
                'detail_order_id' => $detail_order->id,
                'vps_id' => $vps->id,
                'ip' => $vps->ip,
            ];
            $this->order_change_ip->create($data_change_ip);
            if ( $vps->vm_id < 1000000000 ) {
              $reques_vcenter = $this->dashboard->admin_change_ip($vps);
            } else {
              $reques_vcenter = $this->dashboard->change_ip_vps_us($vps);
            }
            // $reques_vcenter = true;
            if ($reques_vcenter) {
              // đổi status
              $vps->status_vps = 'change_ip';
              $vps->save();
              $check = true;
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'yêu cầu',
                'model' => 'User/Order',
                'description' => ' đổi IP VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'description_user' => ' đổi IP VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            }
        }
        // ghi log giao dịch
        if ( $check ) {
          $order->status = 'Finish';
          $order->save();

          $detail_order->status = 'paid';
          $detail_order->paid_date = date('Y-m-d');
          $detail_order->save();

          $history_pay->status = 'Active';
          $history_pay->save();
          $data_log_payment = [
            'history_pay_id' => $history_pay->id,
            'before' => $credit->value,
            'after' => $credit->value - $total,
          ];
          $this->log_payment->create($data_log_payment);
          // trừ tiền
          $credit->value = $credit->value - $total;
          $credit->save();

          $data = [
              'success' => [],
              'error' => '',
              'detail_order_id' => $detail_order->id,
          ];
        } else {
          $data = [
            'success' => [],
            'error' => 'Yêu cầu đổi IP thất bại',
            'detail_order_id' => 0,
            'check' => 0
          ];
        }

        return $data;
    }

    public function reset_password($list_id)
    {
        $data = [
          'success' => true,
          'error' => '',
        ];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'error' => 6
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'error' => 5
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'delete_vps' ) {
                  $data = [
                    'error' => 5
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
        }
        foreach ($list_id as $key => $id) {
          $vps = $this->vps->find($id);
          $reset_password = $this->dashboard->reset_password($vps);
          if ($reset_password) {
            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt lại mật khẩu',
                'model' => 'User/Order',
                'description' => ' VPS US <a target="_blank" href="/admin/vps_us/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'description_user' => ' VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (\Exception $e) {
              report($e);
              continue;
            }
          } else {
            $data = [
              'success' => '',
              'error' => 'VPS có ip: ' . $vps->ip . ' đặt lại mật khẩu không thành công',
            ];
            return $data;
          }
        }
        return $data;
    }

    public function check_upgrade_hosting($id)
    {
       $upgrades = $this->product_upgrate->where("product_default_id", $id)->orderBy("product_upgrate_id", 'asc')->get();
       $data = [];
       if ($upgrades->count() > 0) {
         foreach ($upgrades as $key => $upgrade) {
            $product = $upgrade->product_upgrate;
            $data[$key]['product'] = $product;
            $data[$key]['pricing'] = $product->pricing;
         }
       }
       return $data;
    }

    public function upgrade_hosting($data)
    {
       $hosting = $this->hosting->find($data['hosting_id']);
       $product_upgrade = $this->product->find($data['product_upgrade_id']);

       $sub_total_hosting = $hosting->detail_order->sub_total;
       $sub_total_hosting_upgrade = $product_upgrade->pricing[$hosting->billing_cycle];

       $date_create_hosting = new Carbon($hosting->date_create);
       $next_due_date = new Carbon($hosting->next_due_date);
       if ($next_due_date->isPast()) {
          return false;
       } else {
          $total_day = $next_due_date->diffInDays($date_create_hosting);
          $now = new Carbon;
          $next_due_date = new Carbon($hosting->next_due_date);
          $total = ($sub_total_hosting_upgrade - $sub_total_hosting) / ($total_day / ( $next_due_date->diffInDays($now) + 1 ));
          $total = (int) round($total, -3);

          $data_order = [
              'user_id' => Auth::user()->id,
              'total' => $total,
              'status' => 'Pending',
              'description' => 'upgrade hosting',
              'makh' => '',
          ];
          $order = $this->order->create($data_order);
          //create order detail
          $date = date('Y-m-d');
          $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
          $data_order_detail = [
              'order_id' => $order->id,
              'type' => 'upgrade_hosting',
              'due_date' => $date,
              'description' => 'upgrade hosting',
              'status' => 'unpaid',
              'sub_total' => $total,
              'quantity' => 1,
              'user_id' => Auth::user()->id,
              'addon_id' => '0',
          ];
          $detail_order = $this->detail_order->create($data_order_detail);
          $user = $this->user->find(Auth::user()->id);
          $data_history = [
              'user_id' => $user->id,
              'ma_gd' => 'GDADV' . strtoupper(substr(sha1(time()), 34, 39)),
              'discription' => 'Hóa đơn số ' . $detail_order->id,
              'type_gd' => '8',
              'method_gd' => 'invoice',
              'date_gd' => date('Y-m-d'),
              'money'=> $total,
              'status' => 'Pending',
              'detail_order_id' => $detail_order->id,
          ];
          $this->history_pay->create($data_history);
          // vetify order
          $data_upgrade = [
            'detail_order_id'  => $detail_order->id,
            'hosting_id' => $hosting->id,
            'product_id' => $data['product_upgrade_id'],
          ];
          $this->order_upgrade_hosting->create($data_upgrade);
          // ghi log
          if ($product_upgrade->group_product->private == 0) {
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => 'gói nâng cấp Hosting <a href="admin/products/edit/' . $product_upgrade->id . '">' . $product_upgrade->name . '</a> cho Hosting <a href="/admin/hostings/detail/'. $hosting->id  .'">' . $hosting->domain . '</a>',
              'description_user' => 'gói nâng cấp Hosting ' . $product_upgrade->name . ' cho  Hosting <a href="/service/detail/'. $hosting->id  .'?type=hosting">' . $hosting->domain . '</a>',
              'service' =>  $hosting->domain,
            ];
          } else {
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => 'gói nâng cấp Hosting <a target="_blank" href="/admin/products/edit/' . $product_upgrade->id . '">' . $product_upgrade->name . '</a> cho  Hosting <a href="/admin/hostings/detail/'. $hosting->id  .'">' . $hosting->domain . '</a>',
              'description_user' => 'gói nâng cấp Hosting ' . $product_upgrade->name . ' cho  Hosting <a target="_blank" href="/service/detail/'. $hosting->id  .'?type=hosting">' . $hosting->domain . '</a>',
              'service' =>  $hosting->domain,
            ];
          }
          $this->log_activity->create($data_log);
          // gửi mail

          $data = [
              'name' => $user->name,
              'domain' => $hosting->domain,
              'product_name' => $product_upgrade->name,
              'amount' => $total,
              'subject' => 'Xác nhận đặt hàng nâng cấp Hosting'
          ];
          // try {
          //   $data_tb_send_mail = [
          //     'type' => 'order_upgrade_hosting',
          //     'type_service' => 'hosting',
          //     'user_id' => $user->id,
          //     'status' => false,
          //     'content' => serialize($data),
          //   ];
          //   $this->send_mail->create($data_tb_send_mail);
          // } catch (\Exception $e) {
          //   report($e);
          //   return $detail_order->id;
          // }

          return $detail_order->id;

       }

    }

    public function request_expired_vps($list_id_vps)
    {
        $data = [];
        $data['error'] = 0;
        $data['expire_billing_cycle'] = false;
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        $billingDashBoard = config('billingDashBoard');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_vps)) {
            foreach ($list_id_vps as $key => $id_vps) {
               $vps = $this->vps->find($id_vps);
               if ($data['expire_billing_cycle'] == false) {
                  if (!empty($vps->expire_billing_cycle)) {
                     $data['expire_billing_cycle'] = true;
                  }
               }
               if ( !empty($vps->expire_billing_cycle) ) {
                   $check = true;
                   if ( $vps->price_override == 0 ) {
                      $data['error'] = 1;
                   }
                   $total += $vps->price_override;
                   $billing_cycle = $vps->billing_cycle;
               }
               else if (!empty($vps->price_override)) {
                  $total += $vps->price_override;
                  $billing_cycle = $vps->billing_cycle;
               } else {
                  $product = $vps->product;
                  // $billing_cycle = $vps->billing_cycle;
                  if (!empty($billing_cycle)) {
                    $total += $product->pricing[$billing_cycle];
                  } else {
                    $total += !empty($product->pricing[$vps->billing_cycle]) ? $product->pricing[$vps->billing_cycle] : 0;
                  }
               }
            }
            if ($check) {
              if ( $total == 0 ) {
                $data['error'] = 1;
              }
              $data['price_override']['error'] = 0;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            }
            else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_vps as $key => $id_vps) {
                    $vps = $this->vps->find($id_vps);
                    $product = $vps->product;
                    if ( !empty($vps->price_override) ) {
                        // 1 tháng
                        if ( $billing_cycle == 'monthly' && $product->pricing['monthly'] != 0 ) {
                            $total_monthly += $vps->price_override;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? $vps->price_override * 2 : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? $vps->price_override  * 3 : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 6 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 12 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 24 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 36 : 0;
                        }
                        elseif ( $billing_cycle == 'twomonthly' && $product->pricing['twomonthly'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? $vps->price_override / 2 : 0;
                            $total_twomonthly += $vps->price_override;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  * 3/2 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 3 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 6 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 12 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 18 : 0;
                        }
                        elseif ( $billing_cycle == 'quarterly' && $product->pricing['quarterly'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  * 2/3 , -3) : 0;
                            $total_quarterly += $vps->price_override;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 2 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 4 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 8 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 12 : 0;
                        }
                        elseif ( $billing_cycle == 'semi_annually' && $product->pricing['semi_annually'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                            $total_semi_annually += $vps->price_override;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 2 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 4 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 6 : 0;
                        }
                        elseif ( $billing_cycle == 'annually' && $product->pricing['annually'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                            $total_annually += $vps->price_override;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 2 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 3 : 0;
                        }
                        elseif ( $billing_cycle == 'biennially' && $product->pricing['biennially'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 24 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 8 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                            $total_annually += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                            $total_biennially += $vps->price_override;
                            $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $vps->price_override * 3/2 , -3) : 0;
                        }
                        elseif ( $billing_cycle == 'triennially' && $product->pricing['triennially'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 36 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 18  , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_annually += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                            $total_triennially  += $vps->price_override;
                        }
                    }
                    else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                        if (!empty($vps->vps_config)) {
                            $addon_vps = $vps->vps_config;
                            $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                            $pricing_addon = 0;
                            foreach ($add_on_products as $key => $add_on_product) {
                                if (!empty($add_on_product->meta_product->type_addon)) {
                                  if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                    $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                    $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                    $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                    $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                    $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                    $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                    $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                  }
                                  if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                    $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                    $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                    $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                    $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                    $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                    $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                    $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                  }
                                  if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                    $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                    $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                    $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                    $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                    $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                    $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                    $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                  }
                                }
                            }
                        }
                    }

                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $vps = $this->vps->find($list_id_vps);
            if ($data['expire_billing_cycle'] == false) {
               if (!empty($vps->expire_billing_cycle)) {
                  $data['expire_billing_cycle'] = true;
               }
            }
            if ( !empty($vps->expire_billing_cycle) ) {
                $check = true;
                if ( $vps->price_override == 0 ) {
                  $data['error'] = 1;
                }
                $total += $vps->price_override;
                $data['price_override']['error'] = 0;
                $data['price_override']['billing_cycle'] = $vps->billing_cycle;
                $data['price_override']['text_billing_cycle'] = $billing[$vps->billing_cycle];
                $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            }
            else {
                $product = $vps->product;
                if ( !empty($vps->price_override) ) {
                    $billing_cycle = $vps->billing_cycle;
                    // dd($billing_cycle, $product->pricing['twomonthly'] );
                    // 1 tháng
                    if ( $billing_cycle == 'monthly' && $product->pricing['monthly'] != 0 ) {
                        $total_monthly = $vps->price_override;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? $vps->price_override * 2 : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? $vps->price_override  * 3 : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 6 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 12 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 24 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 36 : 0;
                    }
                    elseif ( $billing_cycle == 'twomonthly' && $product->pricing['twomonthly'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? $vps->price_override / 2 : 0;
                        $total_twomonthly = $vps->price_override;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  * 3/2 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 3 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 6 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 12 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 18 : 0;
                    }
                    elseif ( $billing_cycle == 'quarterly' && $product->pricing['quarterly'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  * 2/3 , -3) : 0;
                        $total_quarterly = $vps->price_override;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 2 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 4 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 8 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 12 : 0;
                    }
                    elseif ( $billing_cycle == 'semi_annually' && $product->pricing['semi_annually'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                        $total_semi_annually = $vps->price_override;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 2 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 4 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 6 : 0;
                    }
                    elseif ( $billing_cycle == 'annually' && $product->pricing['annually'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                        $total_annually = $vps->price_override;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 2 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 3 : 0;
                    }
                    elseif ( $billing_cycle == 'biennially' && $product->pricing['biennially'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 24 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 8 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                        $total_annually = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                        $total_biennially = $vps->price_override;
                        $total_triennially  = !empty($product->pricing['triennially']) ? (int) round( $vps->price_override * 3/2 , -3) : 0;
                    }
                    elseif ( $billing_cycle == 'triennially' && $product->pricing['triennially'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 36 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 18  , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_annually = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                        $total_triennially  = $vps->price_override;
                    }
                }
                else {
                    $total_monthly += $product->pricing['monthly'];
                    $total_twomonthly += $product->pricing['twomonthly'];
                    $total_quarterly += $product->pricing['quarterly'];
                    $total_semi_annually += $product->pricing['semi_annually'];
                    $total_annually += $product->pricing['annually'];
                    $total_biennially += $product->pricing['biennially'];
                    $total_triennially += $product->pricing['triennially'];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ( !empty($vps->price_override) ) {
                                    if ($vps->billing_cycle == 'monthly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 2 tháng
                                    if ($vps->billing_cycle == 'twomonthly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 3 tháng
                                    if ($vps->billing_cycle == 'quarterly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 6 tháng
                                    if ($vps->billing_cycle == 'semi_annually') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 1 năm
                                    if ($vps->billing_cycle == 'annually') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 2 năm
                                    if ($vps->billing_cycle == 'biennially') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 3 năm
                                    if ($vps->billing_cycle == 'triennially') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                        }
                                    }
                                }
                                else {
                                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                      $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                      $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                      $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                      $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                      $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                      $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                      $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                    }
                                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                      $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                      $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                      $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                      $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                      $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                      $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                      $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                    }
                                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                      $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                      $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                      $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                      $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                      $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                      $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                      $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                    }
                                }
                            }
                        }
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        return $data;
    }

    public function request_expired_list_server($list_id)
    {
        $data = [];
        $total = 0;
        $billing = config('billing');
        $total_monthly = 0;
        $total_twomonthly = 0;
        $total_quarterly = 0;
        $total_semi_annually = 0;
        $total_annually = 0;
        $total_biennially = 0;
        $total_triennially  = 0;
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);
          // dd($server);
          $billing_cycle = $server->billing_cycle;
          $product = $server->product;
          if ( !empty($server->amount) ) {
            // dd($billing_cycle, $product->pricing['quarterly'] , $server);
            // 1 tháng
            if ( $billing_cycle == 'monthly' ) {
                $total_monthly += $server->amount;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? $server->amount * 2 : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? $server->amount  * 3 : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 6 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 12 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 24 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 36 : 0;
            }
            elseif ( $billing_cycle == 'twomonthly' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? $server->amount / 2 : 0;
                $total_twomonthly += $server->amount;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  * 3/2 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 3 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 6 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 12 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 18 : 0;
            }
            elseif ( $billing_cycle == 'quarterly' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  * 2/3 , -3) : 0;
                $total_quarterly += $server->amount;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 2 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 4 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 8 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 12 : 0;
            }
            elseif ( $billing_cycle == 'semi_annually' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 2 , -3) : 0;
                $total_semi_annually += $server->amount;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 2 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 4 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 6 : 0;
            }
            elseif ( $billing_cycle == 'annually' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount / 4 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 2 , -3) : 0;
                $total_annually += $server->amount;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 2 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 3 : 0;
            }
            elseif ( $billing_cycle == 'biennially' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 24 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 8 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 4 , -3) : 0;
                $total_annually += !empty($product->pricing['biennially']) ? (int) round( $server->amount / 2 , -3) : 0;
                $total_biennially += $server->amount;
                $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $server->amount * 3/2 , -3) : 0;
            }
            elseif ( $billing_cycle == 'triennially' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 36 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 18  , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_annually += !empty($product->pricing['biennially']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $server->amount / 2 , -3) : 0;
                $total_triennially  += $server->amount;
            }
          }
          else {
              $total_monthly += $product->pricing['monthly'];
              $total_twomonthly += $product->pricing['twomonthly'];
              $total_quarterly += $product->pricing['quarterly'];
              $total_semi_annually += $product->pricing['semi_annually'];
              $total_annually += $product->pricing['annually'];
              $total_biennially += $product->pricing['biennially'];
              $total_triennially += $product->pricing['triennially'];
              if ( !empty($server->server_config) ) {
                 $server_config = $server->server_config;
                 if ( !empty($server_config->ram) ) {
                    foreach ($server->server_config_rams as $server_config_ram) {
                      if ( !empty( $server_config_ram->product->pricing ) ) {
                        if ( $total_monthly ) {
                          $total_monthly += $server_config_ram->product->pricing['monthly'];
                        }
                        if ( $total_twomonthly ) {
                          $total_twomonthly += $server_config_ram->product->pricing['twomonthly'];
                        }
                        if ( $total_quarterly ) {
                          $total_quarterly += $server_config_ram->product->pricing['quarterly'];
                        }
                        if ( $total_semi_annually ) {
                          $total_semi_annually += $server_config_ram->product->pricing['semi_annually'];
                        }
                        if ( $total_annually ) {
                          $total_annually += $server_config_ram->product->pricing['annually'];
                        }
                        if ( $total_biennially ) {
                          $total_biennially += $server_config_ram->product->pricing['biennially'];
                        }
                        if ( $total_triennially ) {
                          $total_triennially += $server_config_ram->product->pricing['triennially'];
                        }
                      }
                    }
                 }
                 if ( !empty($server_config->ip) ) {
                    foreach ($server->server_config_ips as $server_config_ip) {
                      if ( !empty( $server_config_ip->product->pricing ) ) {
                        if ( $total_monthly ) {
                          $total_monthly += $server_config_ip->product->pricing['monthly'];
                        }
                        if ( $total_twomonthly ) {
                          $total_twomonthly += $server_config_ip->product->pricing['twomonthly'];
                        }
                        if ( $total_quarterly ) {
                          $total_quarterly += $server_config_ip->product->pricing['quarterly'];
                        }
                        if ( $total_semi_annually ) {
                          $total_semi_annually += $server_config_ip->product->pricing['semi_annually'];
                        }
                        if ( $total_annually ) {
                          $total_annually += $server_config_ip->product->pricing['annually'];
                        }
                        if ( $total_biennially ) {
                          $total_biennially += $server_config_ip->product->pricing['biennially'];
                        }
                        if ( $total_triennially ) {
                          $total_triennially += $server_config_ip->product->pricing['triennially'];
                        }
                      }
                    }
                 }
                 if ( !empty($server_config->disk2) ) {
                    if ( !empty( $server_config->product_disk2->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk2->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk2->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk2->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk2->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk2->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk2->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk2->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk3) ) {
                    if ( !empty( $server_config->product_disk3->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk3->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk3->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk3->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk3->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk3->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk3->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk3->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk4) ) {
                    if ( !empty( $server_config->product_disk4->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk4->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk4->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk4->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk4->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk4->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk4->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk4->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk5) ) {
                    if ( !empty( $server_config->product_disk5->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk5->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk5->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk5->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk5->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk5->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk5->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk5->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk6) ) {
                    if ( !empty( $server_config->product_disk6->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk6->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk6->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk6->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk6->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk6->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk6->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk6->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk7) ) {
                    if ( !empty( $server_config->product_disk7->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk7->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk7->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk7->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk7->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk7->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk7->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk7->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk8) ) {
                    if ( !empty( $server_config->product_disk8->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk8->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk8->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk8->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk8->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk8->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk8->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk8->pricing['triennially'];
                      }
                    }
                 }
              }
          }
        }
        // $list_id_vps=[248, 243];
        $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
        $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
        $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
        $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
        $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
        $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
        $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
        $data['error'] = '';
        $data['billing_cycle'] = $billing_cycle;
        return $data;
    }

    public function request_expired_colocation($id)
    {
        $data = [];
        $total = 0;
        $billing = config('billing');
        $colocation = $this->colocation->find($id);
        // dd($colocation);
        $amount = $colocation->amount;
        $billing_cycle = $colocation->billing_cycle;
        $total_monthly = 0;
        $total_twomonthly = 0;
        $total_quarterly = 0;
        $total_semi_annually = 0;
        $total_annually = 0;
        $total_biennially = 0;
        $total_triennially  = 0;
        // $n++;
        if ( $billing_cycle == 'monthly' ) {
            $total_monthly = $amount;
            $total_twomonthly = $amount * 2;
            $total_quarterly = $amount  * 3;
            $total_semi_annually = $amount  * 6;
            $total_annually = $amount  * 12;
            $total_biennially = $amount  * 24;
            $total_triennially  = $amount * 36;
        }
        elseif ( $billing_cycle == 'twomonthly'  ) {
            $total_monthly = $amount / 2;
            $total_twomonthly = $amount;
            $total_quarterly = (int) round( $amount  * 3/2 , -3);
            $total_semi_annually = $amount  * 3;
            $total_annually = $amount  * 6;
            $total_biennially = $amount  * 12;
            $total_triennially  = $amount * 18;
        }
        elseif ( $billing_cycle == 'quarterly'  ) {
            $total_monthly = (int) round( $amount  / 3 , -3);
            $total_twomonthly = (int) round( $amount  * 2/3 , -3);
            $total_quarterly = $amount;
            $total_semi_annually = $amount  * 2;
            $total_annually = $amount  * 4;
            $total_biennially = $amount  * 8;
            $total_triennially  = $amount * 12;
        }
        elseif ( $billing_cycle == 'semi_annually'  ) {
            $total_monthly = (int) round( $amount  / 6 , -3);
            $total_twomonthly = (int) round( $amount  / 3 , -3);
            $total_quarterly = (int) round( $amount  / 2 , -3);
            $total_semi_annually = $amount;
            $total_annually = $amount  * 2;
            $total_biennially = $amount  * 4;
            $total_triennially  = $amount * 6;
        }
        elseif ( $billing_cycle == 'annually'  ) {
            $total_monthly = (int) round( $amount  / 12 , -3);
            $total_twomonthly = (int) round( $amount  / 6 , -3);
            $total_quarterly = (int) round( $amount  / 4 , -3);
            $total_semi_annually = (int) round( $amount  / 2 , -3);
            $total_annually = $amount;
            $total_biennially = $amount  * 2;
            $total_triennially  = $amount * 3;
        }
        elseif ( $billing_cycle == 'biennially'  ) {
            $total_monthly = (int) round( $amount  / 24 , -3);
            $total_twomonthly = (int) round( $amount  / 12 , -3);
            $total_quarterly = (int) round( $amount  / 8 , -3);
            $total_semi_annually = (int) round( $amount  / 4 , -3);
            $total_annually = (int) round( $amount / 2 , -3);
            $total_biennially = $amount;
            $total_triennially  = (int) round( $amount * 3/2 , -3);
        }
        elseif ( $billing_cycle == 'triennially'  ) {
            $total_monthly = (int) round( $amount  / 36 , -3);
            $total_twomonthly = (int) round( $amount  / 18  , -3);
            $total_quarterly = (int) round( $amount  / 12 , -3);
            $total_semi_annually = (int) round( $amount  / 6 , -3);
            $total_annually = (int) round( $amount  / 3 , -3);
            $total_biennially = (int) round( $amount / 2 , -3);
            $total_triennially  = $amount;
        }
        // $list_id_vps=[248, 243];
        $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
        $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
        $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
        $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
        $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
        $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
        $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
        $data['error'] = '';
        $data['billing_cycle'] = $billing_cycle;
        return $data;
    }

    public function request_expired_list_colocation($list_id)
    {
      $data = [];
      $total = 0;
      $billing = config('billing');
      $total_monthly = 0;
      $total_twomonthly = 0;
      $total_quarterly = 0;
      $total_semi_annually = 0;
      $total_annually = 0;
      $total_biennially = 0;
      $total_triennially  = 0;
      foreach ($list_id as $key => $id) {
        $colocation = $this->colocation->find($id);
        // dd($server);
        $billing_cycle = $colocation->billing_cycle;
        $product = $colocation->product;
        if ( !empty($colocation->amount) ) {
          // dd($billing_cycle, $product->pricing['quarterly'] , $server);
          // 1 tháng
          if ( $billing_cycle == 'monthly' ) {
              $total_monthly += $colocation->amount;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? $colocation->amount * 2 : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? $colocation->amount  * 3 : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $colocation->amount  * 6 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $colocation->amount  * 12 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $colocation->amount  * 24 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $colocation->amount * 36 : 0;
          }
          elseif ( $billing_cycle == 'twomonthly' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? $colocation->amount / 2 : 0;
              $total_twomonthly += $colocation->amount;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $colocation->amount  * 3/2 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $colocation->amount  * 3 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $colocation->amount  * 6 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $colocation->amount  * 12 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $colocation->amount * 18 : 0;
          }
          elseif ( $billing_cycle == 'quarterly' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $colocation->amount  / 3 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $colocation->amount  * 2/3 , -3) : 0;
              $total_quarterly += $colocation->amount;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $colocation->amount  * 2 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $colocation->amount  * 4 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $colocation->amount  * 8 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $colocation->amount * 12 : 0;
          }
          elseif ( $billing_cycle == 'semi_annually' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $colocation->amount  / 6 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $colocation->amount  / 3 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $colocation->amount  / 2 , -3) : 0;
              $total_semi_annually += $colocation->amount;
              $total_annually += !empty($product->pricing['annually']) ? $colocation->amount  * 2 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $colocation->amount  * 4 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $colocation->amount * 6 : 0;
          }
          elseif ( $billing_cycle == 'annually' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $colocation->amount  / 12 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $colocation->amount  / 6 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $colocation->amount / 4 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $colocation->amount  / 2 , -3) : 0;
              $total_annually += $colocation->amount;
              $total_biennially += !empty($product->pricing['biennially']) ? $colocation->amount  * 2 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $colocation->amount * 3 : 0;
          }
          elseif ( $billing_cycle == 'biennially' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $colocation->amount  / 24 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $colocation->amount  / 12 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $colocation->amount  / 8 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $colocation->amount  / 4 , -3) : 0;
              $total_annually += !empty($product->pricing['biennially']) ? (int) round( $colocation->amount / 2 , -3) : 0;
              $total_biennially += $colocation->amount;
              $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $colocation->amount * 3/2 , -3) : 0;
          }
          elseif ( $billing_cycle == 'triennially' ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $colocation->amount  / 36 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $colocation->amount  / 18  , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $colocation->amount  / 12 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $colocation->amount  / 6 , -3) : 0;
              $total_annually += !empty($product->pricing['biennially']) ? (int) round( $colocation->amount  / 3 , -3) : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $colocation->amount / 2 , -3) : 0;
              $total_triennially  += $colocation->amount;
          }
        }
        else {
            $total_monthly += $product->pricing['monthly'];
            $total_twomonthly += $product->pricing['twomonthly'];
            $total_quarterly += $product->pricing['quarterly'];
            $total_semi_annually += $product->pricing['semi_annually'];
            $total_annually += $product->pricing['annually'];
            $total_biennially += $product->pricing['biennially'];
            $total_triennially += $product->pricing['triennially'];
            if ( !empty($colocation->colocation_config_ips) ) {
              foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                $total_monthly += !empty( $colocation_config_ip->product->pricing['monthly'] ) ? $colocation_config_ip->product->pricing['monthly'] : 0;
                $total_twomonthly += !empty( $colocation_config_ip->product->pricing['twomonthly'] ) ? $colocation_config_ip->product->pricing['twomonthly'] : 0;
                $total_quarterly += !empty( $colocation_config_ip->product->pricing['quarterly'] ) ? $colocation_config_ip->product->pricing['quarterly'] : 0;
                $total_semi_annually += !empty( $colocation_config_ip->product->pricing['semi_annually'] ) ? $colocation_config_ip->product->pricing['semi_annually'] : 0;
                $total_annually += !empty( $colocation_config_ip->product->pricing['annually'] ) ? $colocation_config_ip->product->pricing['annually'] : 0;
                $total_biennially += !empty( $colocation_config_ip->product->pricing['biennially'] ) ? $colocation_config_ip->product->pricing['biennially'] : 0;
                $total_triennially += !empty( $colocation_config_ip->product->pricing['triennially'] ) ? $colocation_config_ip->product->pricing['triennially'] : 0;
              }
            }
        }
      }
      // $list_id_vps=[248, 243];
      $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
      $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
      $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
      $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
      $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
      $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
      $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
      $data['error'] = '';
      $data['price_override'] = '';
      $data['billing_cycle'] = $billing_cycle;
      return $data;
    }

    public function request_expired_hosting($list_id_hosting)
    {
        $data = [];
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_hosting)) {
            foreach ($list_id_hosting as $key => $id_hosting) {
               $hosting = $this->hosting->find($id_hosting);
               if (!empty($hosting->expire_billing_cycle)) {
                  $check = true;
                  $total += $hosting->price_override;
                  $billing_cycle = $hosting->billing_cycle;
               }
               else if (!empty($hosting->price_override)) {
                  $total += $hosting->price_override;
                  $billing_cycle = $hosting->billing_cycle;
               } else {
                  $product = $hosting->product;
                  if (!empty($billing_cycle)) {
                      $total += $product->pricing[$billing_cycle];
                  } else {
                      $total += $product->pricing[$hosting->billing_cycle];
                  }
               }
            }
            if ($check) {
              $data['price_override']['error'] = 0;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_hosting as $key => $id_hosting) {
                    $hosting = $this->hosting->find($id_hosting);
                    if ( !empty($hosting->price_override) ) {
                        // 1 tháng
                        if ($hosting->billing_cycle == 'monthly') {
                            $total_monthly += $hosting->price_override;
                        } else {
                            $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                        }
                        // 2 tháng
                        if ($hosting->billing_cycle == 'twomonthly') {
                            $total_twomonthly += $hosting->price_override;
                        } else {
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                        }
                        // 3 tháng
                        if ($hosting->billing_cycle == 'quarterly') {
                            $total_quarterly += $hosting->price_override;
                        } else {
                            $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                        }
                        // 6 tháng
                        if ($hosting->billing_cycle == 'semi_annually') {
                            $total_semi_annually += $hosting->price_override;
                        } else {
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                        }
                        // 1 năm
                        if ($hosting->billing_cycle == 'annually') {
                            $total_annually += $hosting->price_override;
                        } else {
                            $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                        }
                        // 2 năm
                        if ($hosting->billing_cycle == 'biennially') {
                            $total_biennially += $hosting->price_override;
                        } else {
                            $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                        }
                        // 3 năm
                        if ($hosting->billing_cycle == 'triennially') {
                            $total_triennially += $hosting->price_override;
                        } else {
                            $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                        }
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $hosting = $this->hosting->find($list_id_hosting);
            if (!empty($hosting->expire_billing_cycle)) {
               $data['expire_billing_cycle'] = 1;
               $total += $hosting->price_override;
               $billing_cycle = $hosting->billing_cycle;
               $data['price_override']['error'] = 0;
               $data['price_override']['billing_cycle'] = $billing_cycle;
               $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
               $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $product = $hosting->product;
                if ( !empty($hosting->price_override) ) {
                    // 1 tháng
                    if ($hosting->billing_cycle == 'monthly') {
                        $total_monthly += $hosting->price_override;
                    } else {
                        $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                    }
                    // 2 tháng
                    if ($hosting->billing_cycle == 'twomonthly') {
                        $total_twomonthly += $hosting->price_override;
                    } else {
                        $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                    }
                    // 3 tháng
                    if ($hosting->billing_cycle == 'quarterly') {
                        $total_quarterly += $hosting->price_override;
                    } else {
                        $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                    }
                    // 6 tháng
                    if ($hosting->billing_cycle == 'semi_annually') {
                        $total_semi_annually += $hosting->price_override;
                    } else {
                        $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                    }
                    // 1 năm
                    if ($hosting->billing_cycle == 'annually') {
                        $total_annually += $hosting->price_override;
                    } else {
                        $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                    }
                    // 2 năm
                    if ($hosting->billing_cycle == 'biennially') {
                        $total_biennially += $hosting->price_override;
                    } else {
                        $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                    }
                    // 3 năm
                    if ($hosting->billing_cycle == 'triennially') {
                        $total_triennially += $hosting->price_override;
                    } else {
                        $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                    }
                } else {
                    $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                    $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                    $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                    $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                    $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                    $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                    $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        $data['error'] = '';
        return $data;
    }

    public function request_expired_email_hosting($list_id_hosting)
    {
        $data = [];
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_hosting)) {
            foreach ($list_id_hosting as $key => $id_hosting) {
               $email_hosting = $this->email_hosting->find($id_hosting);
               if (!empty($email_hosting->expire_billing_cycle)) {
                  $check = true;
                  $total += $email_hosting->price_override;
                  $billing_cycle = $email_hosting->billing_cycle;
               }
               else if (!empty($email_hosting->price_override)) {
                  $total += $email_hosting->price_override;
                  $billing_cycle = $email_hosting->billing_cycle;
               } else {
                  $product = $email_hosting->product;
                  if (!empty($billing_cycle)) {
                      $total += $product->pricing[$billing_cycle];
                  } else {
                      $total += $product->pricing[$email_hosting->billing_cycle];
                  }
               }
            }
            if ($check) {
              $data['price_override']['expire_billing_cycle'] = 1;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_hosting as $key => $id_hosting) {
                    $email_hosting = $this->hosting->find($id_hosting);
                    if ( !empty($email_hosting->price_override) ) {
                        // 1 tháng
                        if ($email_hosting->billing_cycle == 'monthly') {
                            $total_monthly += $email_hosting->price_override;
                        } else {
                            $total_monthly += $product->pricing['monthly'];
                        }
                        // 2 tháng
                        if ($email_hosting->billing_cycle == 'twomonthly') {
                            $total_twomonthly += $email_hosting->price_override;
                        } else {
                            $total_twomonthly += $product->pricing['twomonthly'];
                        }
                        // 3 tháng
                        if ($email_hosting->billing_cycle == 'quarterly') {
                            $total_quarterly += $email_hosting->price_override;
                        } else {
                            $total_quarterly += $product->pricing['quarterly'];
                        }
                        // 6 tháng
                        if ($email_hosting->billing_cycle == 'semi_annually') {
                            $total_semi_annually += $email_hosting->price_override;
                        } else {
                            $total_semi_annually += $product->pricing['semi_annually'];
                        }
                        // 1 năm
                        if ($email_hosting->billing_cycle == 'annually') {
                            $total_annually += $email_hosting->price_override;
                        } else {
                            $total_annually += $product->pricing['annually'];
                        }
                        // 2 năm
                        if ($email_hosting->billing_cycle == 'biennially') {
                            $total_biennially += $email_hosting->price_override;
                        } else {
                            $total_biennially += $product->pricing['biennially'];
                        }
                        // 3 năm
                        if ($email_hosting->billing_cycle == 'triennially') {
                            $total_triennially += $email_hosting->price_override;
                        } else {
                            $total_triennially += $product->pricing['triennially'];
                        }
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $email_hosting = $this->email_hosting->find($list_id_hosting);
            if (!empty($email_hosting->expire_billing_cycle)) {
               $data['expire_billing_cycle'] = 1;
               $total += $email_hosting->price_override;
               $billing_cycle = $email_hosting->billing_cycle;
               $data['price_override']['error'] = 0;
               $data['price_override']['billing_cycle'] = $billing_cycle;
               $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
               $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $product = $email_hosting->product;
                if ( !empty($email_hosting->price_override) ) {
                    // 1 tháng
                    if ($email_hosting->billing_cycle == 'monthly') {
                        $total_monthly += $email_hosting->price_override;
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                    }
                    // 2 tháng
                    if ($email_hosting->billing_cycle == 'twomonthly') {
                        $total_twomonthly += $email_hosting->price_override;
                    } else {
                        $total_twomonthly += $product->pricing['twomonthly'];
                    }
                    // 3 tháng
                    if ($email_hosting->billing_cycle == 'quarterly') {
                        $total_quarterly += $email_hosting->price_override;
                    } else {
                        $total_quarterly += $product->pricing['quarterly'];
                    }
                    // 6 tháng
                    if ($email_hosting->billing_cycle == 'semi_annually') {
                        $total_semi_annually += $email_hosting->price_override;
                    } else {
                        $total_semi_annually += $product->pricing['semi_annually'];
                    }
                    // 1 năm
                    if ($email_hosting->billing_cycle == 'annually') {
                        $total_annually += $email_hosting->price_override;
                    } else {
                        $total_annually += $product->pricing['annually'];
                    }
                    // 2 năm
                    if ($email_hosting->billing_cycle == 'biennially') {
                        $total_biennially += $email_hosting->price_override;
                    } else {
                        $total_biennially += $product->pricing['biennially'];
                    }
                    // 3 năm
                    if ($email_hosting->billing_cycle == 'triennially') {
                        $total_triennially += $email_hosting->price_override;
                    } else {
                        $total_triennially += $product->pricing['triennially'];
                    }
                } else {
                    $total_monthly += $product->pricing['monthly'];
                    $total_twomonthly += $product->pricing['twomonthly'];
                    $total_quarterly += $product->pricing['quarterly'];
                    $total_semi_annually += $product->pricing['semi_annually'];
                    $total_annually += $product->pricing['annually'];
                    $total_biennially += $product->pricing['biennially'];
                    $total_triennially += $product->pricing['triennially'];
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        $data['error'] = '';
        return $data;
    }

    public function vps_console($id)
    {
       $vps =  $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
       if ( isset($vps) ) {
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'truy cập console',
             'model' => 'User/Services_VPS',
             'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
             'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
             'service' =>  $vps->ip,
           ];
           $this->log_activity->create($data_log);
           $data = $this->dashboard->vps_console($vps->vm_id);
           if ( $data ) {
             if ( $data['error'] == 0 ) {
               $data['ip'] = $vps->ip;
             }
             return $data;
           } else {
             return ['error' => 2];
           }
       } else {
          return ['error' => 1];
       }
    }

    public function check_hostings_with_user($list_id)
    {
      // dd($list_id);
       if ( is_array($list_id) ) {
         foreach ($list_id as $key => $id) {
            $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $id)->first();
            if (!isset($hosting)) {
               return true;
            }
         }
       } else {
         $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $list_id)->first();
         if (!isset($hosting)) {
            return true;
         }
       }
       return false;
    }

    public function check_list_vps_with_user($list_id)
    {
        if ( is_array($list_id) ) {
            foreach ($list_id as $key => $id) {
               $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
               if (!isset($vps)) {
                  return true;
               }
            }
        } else {
            $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $list_id)->first();
            if (!isset($vps)) {
               return true;
            }
        }
        // $n++;
        return false;
    }

    public function check_list_server_with_user($list_id)
    {
        foreach ($list_id as $key => $id) {
          $server = $this->server->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          if (!isset($server)) {
            return true;
          }
        }
        return false;
    }

    public function check_list_colocation_with_user($list_id)
    {
        foreach ($list_id as $key => $id) {
          $colocation = $this->colocation->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          if (!isset($colocation)) {
            return true;
          }
        }
        return false;
    }

    public function check_list_email_hosting_with_user($list_id)
    {
        foreach ($list_id as $key => $id) {
          $email_hosting = $this->email_hosting->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          if (!isset($email_hosting)) {
            return true;
          }
        }
        return false;
    }

    public function terminatedServers($list_id)
    {
      foreach ($list_id as $key => $id) {
          $server = $this->server->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          $server->status_server = 'cancel';
          $cancel = $server->save();
          if (!$cancel) {
            return false;
          }
        }
        return true;
    }

    public function terminatedColocations($list_id)
    {
      foreach ($list_id as $key => $id) {
          $colocation = $this->colocation->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          $colocation->status_colo = 'cancel';
          $cancel = $colocation->save();
          if (!$cancel) {
            return false;
          }
        }
        return true;
    }

    public function terminatedEmailHostings($list_id)
    {
      foreach ($list_id as $key => $id) {
          $email_hosting = $this->email_hosting->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          $email_hosting->status_hosting = 'cancel';
          $cancel = $email_hosting->save();
          if (!$cancel) {
            return false;
          }
        }
        return true;
    }

    public function check_hosting_with_user($id)
    {
        $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($hosting)) {
           return true;
        }
       return false;
    }

    public function check_server_with_user($id)
    {
        $server = $this->server->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($server)) {
           return true;
        }
        return false;
    }

    public function check_colocation_with_user($id)
    {
        $colocation = $this->colocation->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($colocation)) {
           return true;
        }
        return false;
    }

    public function check_email_hosting_with_user($id)
    {
        $email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($email_hosting)) {
           return true;
        }
        return false;
    }

    public function check_vps_with_user($id)
    {
        $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
        if (!isset($vps)) {
           return true;
        }
        return false;
    }

    public function check_vps_addon_with_user($id)
    {
      $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
        if (!isset($vps)) {
           return false;
        }
        return $vps;
    }

    public function log_payment_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $log_payments = $this->log_activity->where('service', $vps->ip)->where('user_id', $vps->user_id)
                      ->whereIn('action', ['thanh toán', 'gia hạn', 'cấu hình thêm', 'tạo', 'yêu cầu'])
                      ->orderBy('id', 'desc')->paginate(30);
        return $log_payments;
    }

    public function log_activities_with_vps($id)
    {
            $vps = $this->vps->find($id);
            $log_activities = $this->log_activity->where('user_id', Auth::id())->where('service', $vps->ip)
                            ->whereIn( 'action', [ 'tạo', 'tắt', 'bật dịch vụ', 'khởi động lại', 'truy cập console',
                            'gia hạn', 'cấu hình thêm', 'đổi IP', 'hủy dịch vụ', 'đặt lại mật khẩu', 'yêu cầu', 'cập nhật' ] )
                            ->paginate(30);
            return $log_activities;
    }

    public function log_payment_with_server($id)
    {
        $server = $this->server->find($id);
        $log_payments = $this->log_activity->where('service', $server->ip)->where('user_id', $server->user_id)
                            ->whereIn('action', ['thanh toán', 'gia hạn', 'cấu hình thêm', 'tạo', 'yêu cầu'])
                            ->orderBy('id', 'desc')->paginate(30);
        return $log_payments;
    }

    public function log_activities_with_server($id)
    {
            $server = $this->server->find($id);
            $log_activities = $this->log_activity->where('service', $server->ip)->where('user_id', Auth::id())
                            ->whereIn( 'action', [ 'tạo', 'tắt', 'bật dịch vụ', 'khởi động lại', 'truy cập console',
                            'gia hạn', 'cấu hình thêm', 'đổi IP', 'hủy dịch vụ', 'đặt lại mật khẩu', 'yêu cầu', 'cập nhật' ] )->paginate(30);
            return $log_activities;
    }

    public function log_payment_with_proxy($id)
    {
        $proxy = $this->proxy->find($id);
        $log_payments = $this->log_activity->where('service', $proxy->ip)->where('user_id', $proxy->user_id)
                            ->whereIn('action', ['thanh toán', 'gia hạn', 'cấu hình thêm', 'tạo', 'yêu cầu'])
                            ->orderBy('id', 'desc')->paginate(30);
        return $log_payments;
    }

    public function log_activities_with_proxy($id)
    {
            $proxy = $this->proxy->find($id);
            $log_activities = $this->log_activity->where('service', $proxy->ip)->where('user_id', Auth::id())
                            ->whereIn( 'action', [ 'tạo', 'tắt', 'bật dịch vụ', 'khởi động lại', 'truy cập console',
                            'gia hạn', 'cấu hình thêm', 'đổi IP', 'hủy dịch vụ', 'đặt lại mật khẩu', 'yêu cầu', 'cập nhật' ] )->paginate(30);
            return $log_activities;
    }

    public function log_payment_with_colocation($id)
    {
        $colo = $this->colocation->find($id);
        $log_payments = $this->log_activity->where('service', $colo->ip)->where('user_id', $colo->user_id)
                            ->whereIn('action', ['thanh toán', 'gia hạn', 'cấu hình thêm', 'tạo', 'yêu cầu'])
                            ->orderBy('id', 'desc')->paginate(30);
        return $log_payments;
    }

    public function log_activities_with_colocation($id)
    {
            $colo = $this->colocation->find($id);
            $log_activities = $this->log_activity->where('service', $colo->ip)->where('user_id', Auth::id())
                            ->whereIn( 'action', [ 'tạo', 'tắt', 'bật dịch vụ', 'khởi động lại', 'truy cập console',
                            'gia hạn', 'cấu hình thêm', 'đổi IP', 'hủy dịch vụ', 'đặt lại mật khẩu', 'yêu cầu', 'cập nhật' ] )->paginate(30);
            return $log_activities;
    }

    public function log_payment_with_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $log_payments = $this->log_activity->where('service', $hosting->domain)->where('user_id', $hosting->user_id)
                            ->where('action', 'thanh toán')
                            ->orderBy('id', 'desc')->paginate(25);
        return $log_payments;
    }

    public function log_activities_with_hosting($id)
    {
            $hosting = $this->hosting->find($id);
            $log_activities = $this->log_activity->where('service', $hosting->domain)
                            ->where(function($query)
                            {
                                return $query
                                  ->orwhere('action', 'tạo')
                                  ->orwhere('action', 'tắt')
                                  ->orwhere('action', 'bật dịch vụ')
                                  ->orwhere('action', 'gia hạn')
                                  ->orWhere('action', 'hủy dịch vụ');
                            })->paginate(25);
            return $log_activities;
    }


    public function export_excel($list_id_vps, $dataRequest)
    {
      $dataResponse = [];
      $head = [];
      if ( !empty($dataRequest['ip']) ) {
        $head[] = 'IP';
      }
      // if ( !empty($dataRequest['state']) ) {
      //   $head[] = 'Bang';
      // }
      if ( !empty($dataRequest['config']) ) {
        $head[] = 'Cấu hình';
      }
      if ( !empty($dataRequest['due_date']) ) {
        $head[] = 'Ngày kết thúc';
      }
      if ( !empty($dataRequest['password']) ) {
        $head[] = 'Mật khẩu';
      }
      $dataResponse[] = $head;
      foreach ($list_id_vps as $key => $id) {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
          $body = [];
          if ( !empty($dataRequest['ip']) ) {
            $body[] = $vps->ip;
          }
          // if ( !empty($dataRequest['state']) ) {
          //   $body[] = $vps->state;
          // }
          if ( !empty($dataRequest['config']) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $body[] = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          }
          if ( !empty($dataRequest['due_date']) ) {
            $body[] = date('d-m-Y', strtotime($vps->next_due_date));
          }
          if ( !empty($dataRequest['password']) ) {
            $body[] = $vps->password;
          }
          $dataResponse[] = $body;
        }
      }
      return $dataResponse;
    }

    public function export_excel_vps_us($list_id_vps, $dataRequest)
    {
      $dataResponse = [];
      $head = [];
      if ( !empty($dataRequest['ip']) ) {
        $head[] = 'IP';
      }
      if ( !empty($dataRequest['state']) ) {
        $head[] = 'Bang';
      }
      if ( !empty($dataRequest['config']) ) {
        $head[] = 'Cấu hình';
      }
      if ( !empty($dataRequest['due_date']) ) {
        $head[] = 'Ngày kết thúc';
      }
      if ( !empty($dataRequest['password']) ) {
        $head[] = 'Mật khẩu';
      }
      $dataResponse[] = $head;
      foreach ($list_id_vps as $key => $id) {
        $vps = $this->vps->find($id);
        if ( isset($vps) ) {
          $body = [];
          if ( !empty($dataRequest['ip']) ) {
            $body[] = $vps->ip;
          }
          if ( !empty($dataRequest['state']) ) {
            $body[] = $vps->state;
          }
          if ( !empty($dataRequest['config']) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $body[] = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          }
          if ( !empty($dataRequest['due_date']) ) {
            $body[] = date('d-m-Y', strtotime($vps->next_due_date));
          }
          if ( !empty($dataRequest['password']) ) {
            $body[] = $vps->password;
          }
          $dataResponse[] = $body;
        }
      }
      return $dataResponse;
    }

    public function check_os($list_id)
    {
      // dd($list_id);
      $check = false;
      $list_os_config = config('os');
      $list_os = [];
      foreach ($list_id as $key => $id) {
        $vps = $this->vps->where('user_id', Auth::id())->where('id', $id)->first();
        if ( isset($vps) ) {
          if (!empty($vps->product->vps_os)) {
            if ( $vps->product->vps_os->count() ) {
              if ( $check ) {
                $check_empty = false;
                $list_os_duplicate = [];
                foreach ($vps->product->vps_os as $key => $vps_os) {
                  foreach ($list_os as $key => $os) {
                    if ( $vps_os->id == $os['id'] ) {
                      $check_empty = true;
                      $list_os_duplicate[] = $os;
                    }
                  }
                }
                if ( $check_empty ) {
                  $list_os = $list_os_duplicate;
                } else {
                  return [
                    'error' => 2,
                    'ip' => $vps->ip,
                    'os' => [],
                    'type' => 0
                  ];
                }
              } else {
                foreach ($vps->product->vps_os as $key => $vps_os) {
                  $list_os[] = [
                    'id' => $vps_os->id,
                    'os' => $list_os_config[$vps_os->os],
                  ];
                }
              }
              $check = true;
            }
          }
        }
      }
      if ( $check ) {
        return [
          'error' => 0,
          'ip' => '',
          'os' => $list_os,
          'type' => 1
        ];
      } else {
        return [
          'error' => 0,
          'ip' => '',
          'os' => [],
          'type' => 0
        ];
      }
    }

    // list dịch vụ đang on
    public function list_proxy_use_with_sort($request)
    {
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      if ( !empty($request['multi_q']) ) {
        $list_proxy = [];
        $list_ip = explode(',', $request['multi_q']);
        foreach ($list_ip as $key => $ip) {
          $list_proxy[] = $this->proxy->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
              ->orderBy('id', 'desc')->first();
        }
        $data['total'] = 0;
        $data['perPage'] = 0;
        $data['current_page'] = 0;
      } else {
        $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
        if ( $sort == 'DESC' || $sort == 'ASC' ) {
          $sort = $this->mysql_real_escape_string($sort);
        } else {
          $sort = $this->mysql_real_escape_string('DESC');
        }
        $sl = !empty($request['sl']) ? $request['sl'] : 20;
        $list_proxy = $this->proxy->where('user_id', Auth::user()->id)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
              ->orderBy('id', 'desc')->paginate($sl);
        // dd('da den 2', $list_proxy);
        $data['total'] = $list_proxy->total();
        $data['perPage'] = $list_proxy->perPage();
        $data['current_page'] = $list_proxy->currentPage();
      }
      // dd('da den 1', $list_proxy);
      foreach ($list_proxy as $key => $proxy) {
          if ( isset($proxy) ) {
            // ngày tạo
            $proxy->date_create = date('d-m-Y', strtotime($proxy->created_at));
            // ngày kết thúc
            $proxy->next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
            // thời gian thuê
            $proxy->text_billing_cycle = $billing[$proxy->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($proxy->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date <= $date_now) {
                    $expired = true;
                }
            }
            $proxy->isExpire = $isExpire;
            $proxy->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($proxy->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $proxy->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($proxy->date_create);
            $next_due_date = new Carbon($proxy->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $proxy->total_time = $total_time;
            // trạng thái vps
            if (!empty( $proxy->status )) {
              if ($proxy->status == 'on') {
                $proxy->text_status_vps = '<span class="text-success" data-id="' . $proxy->id . '">Đang bật</span>';
              }
              elseif ($proxy->status == 'progressing') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang tạo ...</span>';
              }
              elseif ($proxy->status == 'rebuild') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang cài lại ...</span>';
              }
              elseif ($proxy->status == 'change_ip') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đổi IP ...</span>';
              }
              elseif ($proxy->status == 'reset_password') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($proxy->status == 'expire') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã hết hạn</span>';
              }
              elseif ($proxy->status == 'suspend') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đang bị khoá</span>';
              }
              elseif ($proxy->status == 'admin_off') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Admin tắt</span>';
              }
              elseif ($proxy->status == 'delete_vps') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($proxy->status == 'change_user') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã tắt</span>';
              }
            } else {
              $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($proxy->price_override)) {
              $sub_total = $proxy->price_override;
            } else {
                $product = $proxy->product;
                $sub_total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
            }
            $proxy->amount = $sub_total;
          }
          $data['data'][] = $proxy;
      }
       // dd($data);
      return $data;
    }

    // list dịch vụ đang on
    public function list_proxy_on_with_sort($request)
    {
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      if ( !empty($request['multi_q']) ) {
        $list_proxy = [];
        $list_ip = explode(',', $request['multi_q']);
        foreach ($list_ip as $key => $ip) {
          $list_proxy[] = $this->proxy->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
              ->orderBy('id', 'desc')->first();
        }
        $data['total'] = 0;
        $data['perPage'] = 0;
        $data['current_page'] = 0;
      } else {
        $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
        if ( $sort == 'DESC' || $sort == 'ASC' ) {
          $sort = $this->mysql_real_escape_string($sort);
        } else {
          $sort = $this->mysql_real_escape_string('DESC');
        }
        $sl = !empty($request['sl']) ? $request['sl'] : 20;
        $list_proxy = $this->proxy->where('user_id', Auth::user()->id)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
              ->orderBy('id', 'desc')->paginate($sl);
        // dd('da den 2', $list_proxy);
        $data['total'] = $list_proxy->total();
        $data['perPage'] = $list_proxy->perPage();
        $data['current_page'] = $list_proxy->currentPage();
      }
      // dd('da den 1', $list_proxy);
      foreach ($list_proxy as $key => $proxy) {
          if ( isset($proxy) ) {
            // ngày tạo
            $proxy->date_create = date('d-m-Y', strtotime($proxy->created_at));
            // ngày kết thúc
            $proxy->next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
            // thời gian thuê
            $proxy->text_billing_cycle = $billing[$proxy->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($proxy->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date <= $date_now) {
                    $expired = true;
                }
            }
            $proxy->isExpire = $isExpire;
            $proxy->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($proxy->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $proxy->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($proxy->date_create);
            $next_due_date = new Carbon($proxy->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $proxy->total_time = $total_time;
            // trạng thái vps
            if (!empty( $proxy->status )) {
              if ($proxy->status == 'on') {
                $proxy->text_status_vps = '<span class="text-success" data-id="' . $proxy->id . '">Đang bật</span>';
              }
              elseif ($proxy->status == 'progressing') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang tạo ...</span>';
              }
              elseif ($proxy->status == 'rebuild') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang cài lại ...</span>';
              }
              elseif ($proxy->status == 'change_ip') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đổi IP ...</span>';
              }
              elseif ($proxy->status == 'reset_password') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($proxy->status == 'expire') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã hết hạn</span>';
              }
              elseif ($proxy->status == 'suspend') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đang bị khoá</span>';
              }
              elseif ($proxy->status == 'admin_off') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Admin tắt</span>';
              }
              elseif ($proxy->status == 'delete_vps') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($proxy->status == 'change_user') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã tắt</span>';
              }
            } else {
              $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($proxy->price_override)) {
              $sub_total = $proxy->price_override;
            } else {
                $product = $proxy->product;
                $sub_total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
            }
            $proxy->amount = $sub_total;
          }
          $data['data'][] = $proxy;
      }
       // dd($data);
      return $data;
    }

    // list dịch vụ đang on
    public function list_proxy_expire_with_sort($request)
    {
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      if ( !empty($request['multi_q']) ) {
        $list_proxy = [];
        $list_ip = explode(',', $request['multi_q']);
        foreach ($list_ip as $key => $ip) {
          $list_proxy[] = $this->proxy->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('status', 'expire')
              ->orderBy('id', 'desc')->first();
        }
        $data['total'] = 0;
        $data['perPage'] = 0;
        $data['current_page'] = 0;
      } else {
        $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
        if ( $sort == 'DESC' || $sort == 'ASC' ) {
          $sort = $this->mysql_real_escape_string($sort);
        } else {
          $sort = $this->mysql_real_escape_string('DESC');
        }
        $sl = !empty($request['sl']) ? $request['sl'] : 20;
        $list_proxy = $this->proxy->where('user_id', Auth::user()->id)
              ->orderByRaw('next_due_date ' . $sort)
              ->where('status', 'expire')
              ->orderBy('id', 'desc')->paginate($sl);
        // dd('da den 2', $list_proxy);
        $data['total'] = $list_proxy->total();
        $data['perPage'] = $list_proxy->perPage();
        $data['current_page'] = $list_proxy->currentPage();
      }
      // dd('da den 1', $list_proxy);
      foreach ($list_proxy as $key => $proxy) {
          if ( isset($proxy) ) {
            // ngày tạo
            $proxy->date_create = date('d-m-Y', strtotime($proxy->created_at));
            // ngày kết thúc
            $proxy->next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
            // thời gian thuê
            $proxy->text_billing_cycle = $billing[$proxy->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($proxy->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date <= $date_now) {
                    $expired = true;
                }
            }
            $proxy->isExpire = $isExpire;
            $proxy->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($proxy->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $proxy->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($proxy->date_create);
            $next_due_date = new Carbon($proxy->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $proxy->total_time = $total_time;
            // trạng thái vps
            if (!empty( $proxy->status )) {
              if ($proxy->status == 'on') {
                $proxy->text_status_vps = '<span class="text-success" data-id="' . $proxy->id . '">Đang bật</span>';
              }
              elseif ($proxy->status == 'progressing') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang tạo ...</span>';
              }
              elseif ($proxy->status == 'rebuild') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang cài lại ...</span>';
              }
              elseif ($proxy->status == 'change_ip') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đổi IP ...</span>';
              }
              elseif ($proxy->status == 'reset_password') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($proxy->status == 'expire') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã hết hạn</span>';
              }
              elseif ($proxy->status == 'suspend') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đang bị khoá</span>';
              }
              elseif ($proxy->status == 'admin_off') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Admin tắt</span>';
              }
              elseif ($proxy->status == 'delete_vps') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($proxy->status == 'change_user') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã tắt</span>';
              }
            } else {
              $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($proxy->price_override)) {
              $sub_total = $proxy->price_override;
            } else {
                $product = $proxy->product;
                $sub_total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
            }
            $proxy->amount = $sub_total;
          }
          $data['data'][] = $proxy;
      }
       // dd($data);
      return $data;
    }

    // list dịch vụ xoa huy chuyen
    public function list_proxy_cancel_with_sort($request)
    {
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      if ( !empty($request['multi_q']) ) {
        $list_proxy = [];
        $list_ip = explode(',', $request['multi_q']);
        foreach ($list_ip as $key => $ip) {
          $list_proxy[] = $this->proxy->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->whereIn('status', [ 'cancel', 'delete', 'change_user' ])
              ->orderBy('id', 'desc')->first();
        }
        $data['total'] = 0;
        $data['perPage'] = 0;
        $data['current_page'] = 0;
      } else {
        $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
        if ( $sort == 'DESC' || $sort == 'ASC' ) {
          $sort = $this->mysql_real_escape_string($sort);
        } else {
          $sort = $this->mysql_real_escape_string('DESC');
        }
        $sl = !empty($request['sl']) ? $request['sl'] : 20;
        $list_proxy = $this->proxy->where('user_id', Auth::user()->id)
              ->orderByRaw('next_due_date ' . $sort)
              ->whereIn('status', [ 'cancel', 'delete', 'change_user' ])
              ->orderBy('id', 'desc')->paginate($sl);
        // dd('da den 2', $list_proxy);
        $data['total'] = $list_proxy->total();
        $data['perPage'] = $list_proxy->perPage();
        $data['current_page'] = $list_proxy->currentPage();
      }
      // dd('da den 1', $list_proxy);
      foreach ($list_proxy as $key => $proxy) {
          if ( isset($proxy) ) {
            // ngày tạo
            $proxy->date_create = date('d-m-Y', strtotime($proxy->created_at));
            // ngày kết thúc
            $proxy->next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
            // thời gian thuê
            $proxy->text_billing_cycle = $billing[$proxy->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($proxy->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date <= $date_now) {
                    $expired = true;
                }
            }
            $proxy->isExpire = $isExpire;
            $proxy->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($proxy->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $proxy->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($proxy->date_create);
            $next_due_date = new Carbon($proxy->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $proxy->total_time = $total_time;
            // trạng thái vps
            if (!empty( $proxy->status )) {
              if ($proxy->status == 'on') {
                $proxy->text_status_vps = '<span class="text-success" data-id="' . $proxy->id . '">Đang bật</span>';
              }
              elseif ($proxy->status == 'progressing') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang tạo ...</span>';
              }
              elseif ($proxy->status == 'rebuild') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang cài lại ...</span>';
              }
              elseif ($proxy->status == 'change_ip') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đổi IP ...</span>';
              }
              elseif ($proxy->status == 'reset_password') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($proxy->status == 'expire') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã hết hạn</span>';
              }
              elseif ($proxy->status == 'suspend') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đang bị khoá</span>';
              }
              elseif ($proxy->status == 'admin_off') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Admin tắt</span>';
              }
              elseif ($proxy->status == 'delete_vps') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($proxy->status == 'change_user') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã tắt</span>';
              }
            } else {
              $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($proxy->price_override)) {
              $sub_total = $proxy->price_override;
            } else {
                $product = $proxy->product;
                $sub_total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
            }
            $proxy->amount = $sub_total;
          }
          $data['data'][] = $proxy;
      }
       // dd($data);
      return $data;
    }

    // list dịch vụ tất cả
    public function list_proxy_all_with_sort($request)
    {
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      if ( !empty($request['multi_q']) ) {
        $list_proxy = [];
        $list_ip = explode(',', $request['multi_q']);
        foreach ($list_ip as $key => $ip) {
          $list_proxy[] = $this->proxy->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('status', '!=', 'Pending')
              ->orderBy('id', 'desc')->first();
        }
        $data['total'] = 0;
        $data['perPage'] = 0;
        $data['current_page'] = 0;
      } else {
        $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
        if ( $sort == 'DESC' || $sort == 'ASC' ) {
          $sort = $this->mysql_real_escape_string($sort);
        } else {
          $sort = $this->mysql_real_escape_string('DESC');
        }
        $sl = !empty($request['sl']) ? $request['sl'] : 20;
        $list_proxy = $this->proxy->where('user_id', Auth::user()->id)
              ->orderByRaw('next_due_date ' . $sort)
              ->where('status', '!=', 'Pending')
              ->orderBy('id', 'desc')->paginate($sl);
        // dd('da den 2', $list_proxy);
        $data['total'] = $list_proxy->total();
        $data['perPage'] = $list_proxy->perPage();
        $data['current_page'] = $list_proxy->currentPage();
      }
      // dd('da den 1', $list_proxy);
      foreach ($list_proxy as $key => $proxy) {
          if ( isset($proxy) ) {
            // ngày tạo
            $proxy->date_create = date('d-m-Y', strtotime($proxy->created_at));
            // ngày kết thúc
            $proxy->next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
            // thời gian thuê
            $proxy->text_billing_cycle = $billing[$proxy->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($proxy->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date <= $date_now) {
                    $expired = true;
                }
            }
            $proxy->isExpire = $isExpire;
            $proxy->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($proxy->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $proxy->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($proxy->date_create);
            $next_due_date = new Carbon($proxy->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $proxy->total_time = $total_time;
            // trạng thái vps
            if (!empty( $proxy->status )) {
              if ($proxy->status == 'on') {
                $proxy->text_status_vps = '<span class="text-success" data-id="' . $proxy->id . '">Đang bật</span>';
              }
              elseif ($proxy->status == 'progressing') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang tạo ...</span>';
              }
              elseif ($proxy->status == 'rebuild') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang cài lại ...</span>';
              }
              elseif ($proxy->status == 'change_ip') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đổi IP ...</span>';
              }
              elseif ($proxy->status == 'reset_password') {
                $proxy->text_status_vps = '<span class="vps-progressing" data-id="' . $proxy->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($proxy->status == 'expire') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã hết hạn</span>';
              }
              elseif ($proxy->status == 'suspend') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đang bị khoá</span>';
              }
              elseif ($proxy->status == 'admin_off') {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Admin tắt</span>';
              }
              elseif ($proxy->status == 'delete_vps') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($proxy->status == 'change_user') {
                  $proxy->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Đã tắt</span>';
              }
            } else {
              $proxy->text_status_vps = '<span class="text-danger" data-id="' . $proxy->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($proxy->price_override)) {
              $sub_total = $proxy->price_override;
            } else {
                $product = $proxy->product;
                $sub_total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
            }
            $proxy->amount = $sub_total;
          }
          $data['data'][] = $proxy;
      }
       // dd($data);
      return $data;
    }

    public function check_list_proxy_with_user($listId)
    {
      foreach ($listId as $key => $id) {
        $proxy = $this->proxy->where('id', $id)->where('user_id', Auth::id())->first();
        if ( !isset($proxy) ) {
          return true;
        }
      }
      return false;
    }

    public function request_expired_proxy($listId)
    {
      $data = [];
      $data['error'] = 0;
      $data['expire_billing_cycle'] = false;
      $total = 0;
      $billing_cycle = '';
      $price_override = 0;
      $check = false;
      $billing = config('billing');
      $billingDashBoard = config('billingDashBoard');
      // tổng giá
      $total_monthly = 0;
      $total_twomonthly = 0;
      $total_quarterly = 0;
      $total_semi_annually = 0;
      $total_annually = 0;
      $total_biennially = 0;
      $total_triennially = 0;
      foreach ($listId as $key => $id) {
        $proxy = $this->proxy->find($id);
        $product = $proxy->product;
        if ( !empty($proxy->price_override) ) {
          // 1 tháng
          if ( $billing_cycle == 'monthly' && $product->pricing['monthly'] != 0 ) {
              $total_monthly += $proxy->price_override;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? $proxy->price_override * 2 : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? $proxy->price_override  * 3 : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $proxy->price_override  * 6 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $proxy->price_override  * 12 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $proxy->price_override  * 24 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $proxy->price_override * 36 : 0;
          }
          elseif ( $billing_cycle == 'twomonthly' && $product->pricing['twomonthly'] != 0 ) {
              $total_monthly += !empty($product->pricing['monthly']) ? $proxy->price_override / 2 : 0;
              $total_twomonthly += $proxy->price_override;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $proxy->price_override  * 3/2 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $proxy->price_override  * 3 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $proxy->price_override  * 6 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $proxy->price_override  * 12 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $proxy->price_override * 18 : 0;
          }
          elseif ( $billing_cycle == 'quarterly' && $product->pricing['quarterly'] != 0 ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $proxy->price_override  / 3 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $proxy->price_override  * 2/3 , -3) : 0;
              $total_quarterly += $proxy->price_override;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? $proxy->price_override  * 2 : 0;
              $total_annually += !empty($product->pricing['annually']) ? $proxy->price_override  * 4 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $proxy->price_override  * 8 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $proxy->price_override * 12 : 0;
          }
          elseif ( $billing_cycle == 'semi_annually' && $product->pricing['semi_annually'] != 0 ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $proxy->price_override  / 6 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $proxy->price_override  / 3 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $proxy->price_override  / 2 , -3) : 0;
              $total_semi_annually += $proxy->price_override;
              $total_annually += !empty($product->pricing['annually']) ? $proxy->price_override  * 2 : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? $proxy->price_override  * 4 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $proxy->price_override * 6 : 0;
          }
          elseif ( $billing_cycle == 'annually' && $product->pricing['annually'] != 0  ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $proxy->price_override  / 12 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $proxy->price_override  / 6 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $proxy->price_override  / 4 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $proxy->price_override  / 2 , -3) : 0;
              $total_annually += $proxy->price_override;
              $total_biennially += !empty($product->pricing['biennially']) ? $proxy->price_override  * 2 : 0;
              $total_triennially  += !empty($product->pricing['triennially']) ? $proxy->price_override * 3 : 0;
          }
          elseif ( $billing_cycle == 'biennially' && $product->pricing['biennially'] != 0  ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $proxy->price_override  / 24 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $proxy->price_override  / 12 , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $proxy->price_override  / 8 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $proxy->price_override  / 4 , -3) : 0;
              $total_annually += !empty($product->pricing['biennially']) ? (int) round( $proxy->price_override / 2 , -3) : 0;
              $total_biennially += $proxy->price_override;
              $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $proxy->price_override * 3/2 , -3) : 0;
          }
          elseif ( $billing_cycle == 'triennially' && $product->pricing['triennially'] != 0  ) {
              $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $proxy->price_override  / 36 , -3) : 0;
              $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $proxy->price_override  / 18  , -3) : 0;
              $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $proxy->price_override  / 12 , -3) : 0;
              $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $proxy->price_override  / 6 , -3) : 0;
              $total_annually += !empty($product->pricing['biennially']) ? (int) round( $proxy->price_override  / 3 , -3) : 0;
              $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $proxy->price_override / 2 , -3) : 0;
              $total_triennially  += $proxy->price_override;
          }
        }
        else {
          $total_monthly += $product->pricing['monthly'];
          $total_twomonthly += $product->pricing['twomonthly'];
          $total_quarterly += $product->pricing['quarterly'];
          $total_semi_annually += $product->pricing['semi_annually'];
          $total_annually += $product->pricing['annually'];
          $total_biennially += $product->pricing['biennially'];
          $total_triennially += $product->pricing['triennially'];
        }
      }
      $data['price_override'] = '';
      $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
      $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
      $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
      $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
      $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
      $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
      $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
      return $data;
    }

    // gia hạn nhiều vps
    public function expired_list_proxy($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
            $proxy = $this->proxy->find($id);
            if (!empty($proxy)) {
                $product = $this->product->find($proxy->product_id);
                if (!empty($proxy->price_override)) {
                    if ($proxy->billing_cycle == $billing_cycle) {
                        $total += $proxy->price_override;
                    } else {
                      $total_price_1 = ($proxy->price_override * $billing_price[$billing_cycle]) / $billing_price[$proxy->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Proxy',
          'due_date' => $date,
          'description' => 'Gia hạn Proxy',
          'status' => 'unpaid',
          'sub_total' => $total / $quantity,
          'quantity' => $quantity,
          'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'proxy',
            ];
            $this->order_expired->create($data_order_expired);
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXP' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {
            $billings = config('billing');
            // ghi log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt hàng',
                'model' => 'User/Order_Services',
                'description' => ' gia hạn Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'description_user' => ' gia hạn Proxy <a target="_blank" href="/service/detail/' . $proxy->id . '?type=proxy">' . $proxy->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $proxy->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (Exception $e) {
              report($e);
            }
            return $detail_order->id;
        } else {
            return false;
        }
    }

    public function terminatedProxy($list_id)
    {
      foreach ($list_id as $key => $id) {
        $proxy = $this->proxy->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services',
          'description' => ' dịch vụ Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> ',
          'description_user' => ' dịch vụ Proxy <a target="_blank" href="/service/detail/' . $proxy->id . '?type=vps">' . $proxy->ip . '</a> ',
          'service' => $proxy->ip,
        ];
        $this->log_activity->create($data_log);

        $proxy->status = 'cancel';
        $proxy->save();
      }
      $data['success'] = true;
      return  $data;
    }

}
