<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\EmailHosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use App\Model\HistoryPay;
use App\Model\OrderAddonVps;
use App\Model\OrderExpired;
use App\Model\GroupUser;
use App\Model\LogActivity;
use App\Model\Domain;
use App\Model\VpsConfig;
use App\Model\SendMail;
use Mail;
use App\Factories\AdminFactories;
use Carbon\Carbon;
use App\Model\Customer;
use App\Model\OrderChangeVps;
use App\Model\ProductUpgrate;
use App\Model\OrderUpgradeHosting;
use App\Model\Colocation;
use App\Model\LogPayment;
use App\Model\Proxy;
use Illuminate\Support\Facades\Log;

class VpsRepositories {

    protected $user;
    protected $user_meta;
    protected $verify;
    protected $email;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $order_addon_vps;
    protected $server;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $order_vetify;
    protected $credit;
    protected $history_pay;
    protected $order_expired;
    protected $group_user;
    protected $customer;
    protected $order_change_ip;
    protected $product_upgrate;
    protected $order_upgrade_hosting;
    protected $log_activity;
    protected $domain;
    protected $colocation;
    protected $email_hosting;
    protected $vps_config;
    protected $send_mail;
    protected $log_payment;
    protected $proxy;
    // API
    protected $da;
    protected $da_si;
    protected $dashboard;

    public function __construct()
    {
        $this->user = new User;
        $this->group_user = new GroupUser;
        $this->email = new Email;
        $this->user_meta = new UserMeta;
        $this->verify = new Verify;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->order_addon_vps = new OrderAddonVps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->order_vetify = new OrderVetify;
        $this->credit = new Credit;
        $this->history_pay = new HistoryPay;
        $this->order_expired = new OrderExpired;
        $this->customer = new Customer;
        $this->order_change_ip = new OrderChangeVps;
        $this->product_upgrate = new ProductUpgrate;
        $this->order_upgrade_hosting = new OrderUpgradeHosting;
        $this->log_activity = new LogActivity();
        $this->domain = new Domain;
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
        $this->vps_config = new VpsConfig;
        $this->send_mail = new SendMail;
        $this->log_payment = new LogPayment;
        $this->proxy = new Proxy;
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
    }
    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }
    public function get_customer_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $invoice = $vps->detail_order;
        if (!empty($invoice->order->makh)) {
          $customer = $this->customer->where('ma_customer', $invoice->order->makh)->first();
        } else {
          return false;
        }
        if (isset($customer)) {
            return $customer;
        } else {
            return false;
        }
    }
    public function list_vps_on()
    {
        return $this->vps->where('user_id', Auth::user()->id)->
        where('status', 'Active')->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                  ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 
                  'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    public function list_vps_use()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
        ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                  ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 
                  'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_nearly()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->where('status_vps', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }
    public function vps_nearly_and_expire()
    {
        return view('users.services.list_vps_nearly_and_expire');
    }
    public function list_vps_cancel()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->where(function($query)
                    {
                        return $query
                          ->whereIn('status_vps', [ 'cancel', 'delete_vps', 'change_user' ]);
                    })
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }
    public function vps_all()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'cloudzone')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_us_on()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_us_use()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate(20);
    }

    public function list_vps_us_nearly()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                    ->where('status_vps', 'expire')
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }
    public function terminatedVpsUS($id)
    {
        $vps = $this->vps->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_VPS_US',
          'description' => 'dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->domain . '</a> ',
          'description_user' => 'dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->domain . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        $on = $this->dashboard->terminatedVpsUS($vps);
        // $on = true;
        return $on;
    }
    public function list_vps_nearly_and_expire()
    {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        // dd($list_vps);
        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( count($list_vps) > 0 ) {
            foreach ($list_vps as $key_list => $data_vps) {
              foreach ($data_vps as $key => $vps) {
                  //  cấu hình
                  $product = $vps->product;
                  $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                  $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                  $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                  // Addon
                  if (!empty($vps->vps_config)) {
                      $vps_config = $vps->vps_config;
                      if (!empty($vps_config)) {
                          $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                          $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                          $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                      }
                  }
                  $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  // khách hàng
                  if (!empty($vps->ma_kh)) {
                      if(!empty($vps->customer))
                          if(!empty($vps->customer->customer_name)) {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                          }
                          else {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                          }
                      else {
                        $vps->link_customer = 'Chính tôi';
                      }
                  } else {
                      $customer = $this->get_customer_with_vps($vps->id);
                      if(!empty($customer)) {
                          if(!empty($customer->customer_name)) {
                            $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                          } else {
                            $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                          }
                      } else {
                          $vps->link_customer = 'Chính tôi';
                      }
                  }
                  // ngày tạo
                  $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
                  // ngày kết thúc
                  $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
                  // thời gian thuê
                  $vps->text_billing_cycle = $billing[$vps->billing_cycle];
                  if ( $key_list == 'sub3' ) {
                    $vps->text_next_due_date = 'Hết hạn 3 ngày';
                  }
                  elseif ( $key_list == 'sub2' ) {
                    $vps->text_next_due_date = 'Hết hạn 2 ngày';
                  }
                  elseif ( $key_list == 'sub1' ) {
                    $vps->text_next_due_date = 'Hết hạn 1 ngày';
                  }
                  elseif ( $key_list == 'sub0' ) {
                    $vps->text_next_due_date = 'Hết hạn 0 ngày';
                  }
                  else {
                    $vps->text_next_due_date = 'Còn hạn ' . $key_list . ' ngày';
                  }
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($vps->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $vps->isExpire = $isExpire;
                  $vps->expired = $expired;
                  // tổng thời gian thuê
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $create_date != $next_due_date ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    if ( $create_date != $next_due_date ) {
                      $total_time = $diff_month . ' Tháng';
                    }
                    else {
                      $total_time = $diff_month . ' Tháng';
                    }
                  }
                  $vps->total_time = $total_time;
                  // trạng thái vps
                  if (!empty( $vps->status_vps )) {
                    if ($vps->status_vps == 'on') {
                      $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
                    }
                    elseif ($vps->status_vps == 'progressing') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
                    }
                    elseif ($vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
                    }
                    elseif ($vps->status_vps == 'change_ip') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
                    }
                    elseif ($vps->status_vps == 'reset_password') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
                    }
                    elseif ($vps->status_vps == 'expire') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
                    }
                    elseif ($vps->status_vps == 'suspend') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
                    }
                    elseif ($vps->status_vps == 'admin_off') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
                    }
                    elseif ($vps->status_vps == 'cancel') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
                    }
                    elseif ($vps->status_vps == 'delete_vps') {
                        $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
                    }
                    elseif ($vps->status_vps == 'change_user') {
                        $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
                    }
                    else {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
                    }
                  } else {
                    $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
                  }
                  // gia của vps
                  $sub_total = 0;
                  if (!empty($vps->price_override)) {
                    $sub_total = $vps->price_override;
                  } else {
                      $product = $vps->product;
                      $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                            }
                          }
                        }
                        $sub_total += $pricing_addon;
                      }
                  }
                  $vps->price_vps = $sub_total;
                  $data['data'][] = $vps;
              }

            }
        }
        return $data;
    }

    public function list_vps_us_cancel()
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'us')
                    ->where(function($query)
                    {
                        return $query
                        ->whereIn('status_vps', [ 'cancel', 'delete_vps', 'change_user' ]);
                    })
                    ->with('product')->orderBy('id', 'desc')->paginate(20);
        return $list_vps;

    }
    public function vps_us_all()
    {
        return $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')->where('location', 'us')
                ->orderBy('id', 'desc')->with('product')->paginate(20);
    }
    public function list_vps_us_nearly_and_expire()
    {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }

        $data = [
          'data' => [],
        ];
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( count($list_vps) > 0 ) {
            foreach ($list_vps as $key_list => $data_vps) {
              foreach ($data_vps as $key => $vps) {
                  //  cấu hình
                  $product = $vps->product;
                  $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                  $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                  $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                  // Addon
                  if (!empty($vps->vps_config)) {
                      $vps_config = $vps->vps_config;
                      if (!empty($vps_config)) {
                          $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                          $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                          $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                      }
                  }
                  $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
                  // khách hàng
                  if (!empty($vps->ma_kh)) {
                      if(!empty($vps->customer))
                          if(!empty($vps->customer->customer_name)) {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                          }
                          else {
                            $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                          }
                      else {
                        $vps->link_customer = 'Chính tôi';
                      }
                  } else {
                      $customer = $this->get_customer_with_vps($vps->id);
                      if(!empty($customer)) {
                          if(!empty($customer->customer_name)) {
                            $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                          } else {
                            $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                          }
                      } else {
                          $vps->link_customer = 'Chính tôi';
                      }
                  }
                  // ngày tạo
                  $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
                  // ngày kết thúc
                  $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
                  // thời gian thuê
                  $vps->text_billing_cycle = $billing[$vps->billing_cycle];
                  if ( $key_list == 'sub3' ) {
                    $vps->text_next_due_date = 'Hết hạn 3 ngày';
                  }
                  elseif ( $key_list == 'sub2' ) {
                    $vps->text_next_due_date = 'Hết hạn 2 ngày';
                  }
                  elseif ( $key_list == 'sub1' ) {
                    $vps->text_next_due_date = 'Hết hạn 1 ngày';
                  }
                  elseif ( $key_list == 'sub0' ) {
                    $vps->text_next_due_date = 'Hết hạn 0 ngày';
                  }
                  else {
                    $vps->text_next_due_date = 'Còn hạn ' . $key_list . ' ngày';
                  }
                  // kiểm tra vps gần hết hạn hay đã hết hạn
                  $isExpire = false;
                  $expired = false;
                  if(!empty($vps->next_due_date)){
                      $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                      $date = date('Y-m-d');
                      $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                      if($next_due_date <= $date) {
                        $isExpire = true;
                      }
                      $date_now = strtotime(date('Y-m-d'));
                      if ($next_due_date < $date_now) {
                          $expired = true;
                      }
                  }
                  $vps->isExpire = $isExpire;
                  $vps->expired = $expired;
                  // tổng thời gian thuê
                  $total_time = '';
                  $create_date = new Carbon($vps->date_create);
                  $next_due_date = new Carbon($vps->next_due_date);
                  if ( $next_due_date->diffInYears($create_date) ) {
                    $year = $next_due_date->diffInYears($create_date);
                    $total_time = $year . ' Năm ';
                    $create_date = $create_date->addYears($year);
                    $month = $next_due_date->diffInMonths($create_date);
                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                    if ( $create_date != $next_due_date ) {
                      $total_time .= $month . ' Tháng';
                    }
                  } else {
                    $diff_month = $next_due_date->diffInMonths($create_date);
                    if ( $create_date != $next_due_date ) {
                      $total_time = $diff_month . ' Tháng';
                    }
                    else {
                      $total_time = $diff_month . ' Tháng';
                    }
                  }
                  $vps->total_time = $total_time;
                  // trạng thái vps
                  if (!empty( $vps->status_vps )) {
                    if ($vps->status_vps == 'on') {
                      $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
                    }
                    elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
                    }
                    elseif ($vps->status_vps == 'rebuild') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
                    }
                    elseif ($vps->status_vps == 'change_ip') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
                    }
                    elseif ($vps->status_vps == 'reset_password') {
                      $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
                    }
                    elseif ($vps->status_vps == 'expire') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
                    }
                    elseif ($vps->status_vps == 'suspend') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
                    }
                    elseif ($vps->status_vps == 'admin_off') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
                    }
                    elseif ($vps->status_vps == 'delete_vps') {
                        $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
                    }
                    elseif ($vps->status_vps == 'change_user') {
                        $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
                    }
                    elseif ($vps->status_vps == 'cancel') {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
                    }
                    else {
                      $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
                    }
                  } else {
                    $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
                  }
                  // gia của vps
                  $price_vps = 0;
                  if ( !empty($vps->price_override) ) {
                     $price_vps = $vps->price_override;
                     $order_addon_vps = $vps->order_addon_vps;
                     if (!empty($order_addon_vps)) {
                       foreach ($order_addon_vps as $key => $addon_vps) {
                           $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                      $price_vps = !empty($vps->detail_order->sub_total) ? $vps->detail_order->sub_total : 0;
                      $order_addon_vps = $vps->order_addon_vps;
                      if (!empty($order_addon_vps)) {
                        foreach ($order_addon_vps as $key => $addon_vps) {
                            $price_vps += !empty($addon_vps->detail_order->sub_total) ? $addon_vps->detail_order->sub_total : 0;
                        }
                      }
                  }
                  $vps->price_vps = $price_vps;
                  $data['data'][] = $vps;
              }

            }
        }
        return $data;
    }
    public function list_vps_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_vps($q, $sort, $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'cloudzone')
              ->orderBy('id' , 'desc')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function list_vps_use_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       // dd($list_vps);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+5 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date <= $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_vps_use($q, $sort, $sl)
    {
        if (!empty($sort)) {
            if ( $sort == 'DESC' || $sort == 'ASC' ) {
              $sort = $this->mysql_real_escape_string($sort);
            } else {
              $sort = $this->mysql_real_escape_string('DESC');
            }
            $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                ->where('ip', 'LIKE' , '%'.$q.'%')
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
                })
                ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->paginate($sl);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_multi_vps_use($ips)
    {
       $list_vps = [];
       foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)
              ->where('ip', trim($ip))
              ->where('location', 'cloudzone')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ]);
              })
              ->orderBy('id', 'desc')->with('product')->first();
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          if ( isset($vps) ) {
            //  cấu hình
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer)) {
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
                }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            $text_day = '';
            $now = Carbon::now();
            $next_due_date = new Carbon($vps->next_due_date);
            $diff_date = $next_due_date->diffInDays($now);
            if ( $next_due_date->isPast() ) {
              $text_day = 'Hết hạn ' . $diff_date . ' ngày';
            } else {
              $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
            }
            $vps->text_day = $text_day;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                  $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
              }
              elseif ($vps->status_vps == 'change_user') {
                  $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    public function list_vps_nearly_with_sl($sl)
    {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->orderBy('id', 'desc')
                    ->with('product')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_vps_nearly($q, $sort, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        if (!empty($sort)) {
          if ( $sort == 'DESC' || $sort == 'ASC' ) {
            $sort = $this->mysql_real_escape_string($sort);
          } else {
            $sort = $this->mysql_real_escape_string('DESC');
          }
          $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')
              ->where('ip', 'LIKE' , '%'.$q.'%')
              ->orderByRaw('next_due_date ' . $sort)
              ->where(function($query)
              {
                  return $query
                    ->orWhere('status_vps', 'expire');
              })
              ->with('product')->paginate($sl);
        } else {
          $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('ip', 'LIKE', '%'.$q.'%')
                    ->where('location', 'cloudzone')
                    ->where(function($query)
                    {
                        return $query
                          // ->orwhere('status_vps', 'cancel')
                          ->orWhere('status_vps', 'expire');
                    })
                    ->orderBy('id', 'desc')
                    ->with('product')->paginate($sl);
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          $text_day = '';
          $now = Carbon::now();
          $next_due_date = new Carbon($vps->next_due_date);
          $diff_date = $next_due_date->diffInDays($now);
          if ( $next_due_date->isPast() ) {
            $text_day = 'Hết hạn ' . $diff_date . ' ngày';
          } else {
            $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
          }
          $vps->text_day = $text_day;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_vps_cancel($q, $sl)
    {
        if (empty($sl)) {
           $sl = 20;
        }
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('ip', 'LIKE', '%'.$q.'%')
                    ->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->with('product')->orderBy('id', 'desc')->paginate($sl);
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer))
                  if(!empty($vps->customer->customer_name)) {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                  }
                  else {
                    $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                  }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'cancel') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          // gia của vps
          $sub_total = 0;
          if (!empty($vps->price_override)) {
            $sub_total = $vps->price_override;
          } else {
              $product = $vps->product;
              $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
              if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($vps->user_id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                      $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                      $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                      $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                    }
                  }
                }
                $sub_total += $pricing_addon;
              }
          }
          $vps->price_vps = $sub_total;
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       // dd($data);
       return $data;
    }
    public function search_multi_vps_cancel($ips)
    {
        $list_vps = [];
        foreach ($ips as $key => $ip) {
          $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('ip', trim($ip))
                    ->where('location', 'cloudzone')
                    ->whereIn('status_vps', ['cancel', 'delete_vps', 'change_user'])
                    ->orderBy('id', 'desc')
                    ->with('product')->first();
        }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          if ( isset($vps) ) {
            $product = $vps->product;
            $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
            $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
            $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
            // Addon
            if (!empty($vps->vps_config)) {
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                    $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                    $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                }
            }
            $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
            // khách hàng
            if (!empty($vps->ma_kh)) {
                if(!empty($vps->customer))
                    if(!empty($vps->customer->customer_name)) {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                    }
                    else {
                      $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                    }
                else {
                  $vps->link_customer = 'Chính tôi';
                }
            } else {
                $customer = $this->get_customer_with_vps($vps->id);
                if(!empty($customer)) {
                    if(!empty($customer->customer_name)) {
                      $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                    } else {
                      $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                    }
                } else {
                    $vps->link_customer = 'Chính tôi';
                }
            }
            // ngày tạo
            $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
            // ngày kết thúc
            $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
            // thời gian thuê
            $vps->text_billing_cycle = $billing[$vps->billing_cycle];
            // kiểm tra vps gần hết hạn hay đã hết hạn
            $isExpire = false;
            $expired = false;
            if(!empty($vps->next_due_date)){
                $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                $date = date('Y-m-d');
                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                if($next_due_date <= $date) {
                  $isExpire = true;
                }
                $date_now = strtotime(date('Y-m-d'));
                if ($next_due_date < $date_now) {
                    $expired = true;
                }
            }
            $vps->isExpire = $isExpire;
            $vps->expired = $expired;
            // tổng thời gian thuê
            $total_time = '';
            $create_date = new Carbon($vps->date_create);
            $next_due_date = new Carbon($vps->next_due_date);
            if ( $next_due_date->diffInYears($create_date) ) {
              $year = $next_due_date->diffInYears($create_date);
              $total_time = $year . ' Năm ';
              $create_date = $create_date->addYears($year);
              $month = $next_due_date->diffInMonths($create_date);
              //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
              if ( $create_date != $next_due_date ) {
                $total_time .= $month . ' Tháng';
              }
            } else {
              $diff_month = $next_due_date->diffInMonths($create_date);
              if ( $create_date != $next_due_date ) {
                $total_time = $diff_month . ' Tháng';
              }
              else {
                $total_time = $diff_month . ' Tháng';
              }
            }
            $vps->total_time = $total_time;
            // trạng thái vps
            if (!empty( $vps->status_vps )) {
              if ($vps->status_vps == 'on') {
                $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
              }
              elseif ($vps->status_vps == 'progressing') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
              }
              elseif ($vps->status_vps == 'rebuild') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
              }
              elseif ($vps->status_vps == 'change_ip') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
              }
              elseif ($vps->status_vps == 'reset_password') {
                $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
              }
              elseif ($vps->status_vps == 'expire') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
              }
              elseif ($vps->status_vps == 'suspend') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
              }
              elseif ($vps->status_vps == 'admin_off') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
              }
              elseif ($vps->status_vps == 'cancel') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hủy</span>';
              }
              elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã xóa</span>';
              }
              else {
                $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
              }
            } else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
            }
            // gia của vps
            $sub_total = 0;
            if (!empty($vps->price_override)) {
              $sub_total = $vps->price_override;
            } else {
                $product = $vps->product;
                $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                  }
                  $sub_total += $pricing_addon;
                }
            }
            $vps->price_vps = $sub_total;
          }
          $data['data'][] = $vps;
       }
       $data['total'] = 0;
       $data['perPage'] = 0;
       $data['current_page'] = 0;
       // dd($data);
       return $data;
    }
    public function all_vps_search($q, $sort, $sl)
    {
      if (!empty($sort)) {
          if ( $sort == 'DESC' || $sort == 'ASC' ) {
            $sort = $this->mysql_real_escape_string($sort);
          } else {
            $sort = $this->mysql_real_escape_string('DESC');
          }
         $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                          ->where('location', 'cloudzone')
                          ->orderBy('id', 'desc')
                          ->where('ip', 'LIKE' , '%'.$q.'%')
                          ->orderByRaw('next_due_date ' . $sort)
                          ->with('product')->paginate($sl);
      } else {
        $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'cloudzone')
                         ->orderBy('id', 'desc')
                         ->where('ip', 'LIKE' , '%'.$q.'%')
                         ->with('product')->paginate($sl);
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        $product = $vps->product;
        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
        // Addon
        if (!empty($vps->vps_config)) {
          $vps_config = $vps->vps_config;
          if (!empty($vps_config)) {
            $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
            $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
            $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
          }
        }
        $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
        // khách hàng
        if (!empty($vps->ma_kh)) {
            if (!empty($vps->customer)) {
              if(!empty($vps->customer->customer_name)) {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
              }
              else {
                $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
              }
            }
            else {
              $vps->link_customer = 'Chính tôi';
            }
        } else {
          $customer = $this->get_customer_with_vps($vps->id);
          if(!empty($customer)) {
            if(!empty($customer->customer_name)) {
              $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
            } else {
              $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
            }
          } else {
            $vps->link_customer = 'Chính tôi';
          }
        }
        // ngày tạo
        $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
        // ngày kết thúc
        $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
        // thời gian thuê
        $vps->text_billing_cycle = $billing[$vps->billing_cycle];
        // kiểm tra vps gần hết hạn hay đã hết hạn
        $isExpire = false;
        $expired = false;
        if(!empty($vps->next_due_date)){
          $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
          $date = date('Y-m-d');
          $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
          if($next_due_date <= $date) {
            $isExpire = true;
          }
          $date_now = strtotime(date('Y-m-d'));
          if ($next_due_date < $date_now) {
            $expired = true;
          }
        }
        $vps->isExpire = $isExpire;
        $vps->expired = $expired;
        // tổng thời gian thuê
        $total_time = '';
        $create_date = new Carbon($vps->date_create);
        $next_due_date = new Carbon($vps->next_due_date);
        if ( $next_due_date->diffInYears($create_date) ) {
          $year = $next_due_date->diffInYears($create_date);
          $total_time = $year . ' Năm ';
          $create_date = $create_date->addYears($year);
          $month = $next_due_date->diffInMonths($create_date);
          //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
          if ( $create_date != $next_due_date ) {
            $total_time .= $month . ' Tháng';
          }
        } else {
          $diff_month = $next_due_date->diffInMonths($create_date);
          if ( $create_date != $next_due_date ) {
            $total_time = $diff_month . ' Tháng';
          }
          else {
            $total_time = $diff_month . ' Tháng';
          }
        }
        $vps->total_time = $total_time;
        // trạng thái vps
        if (!empty( $vps->status_vps )) {
          if ($vps->status_vps == 'on') {
            $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
          }
          elseif ($vps->status_vps == 'progressing') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
          }
          elseif ($vps->status_vps == 'rebuild') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
          }
          elseif ($vps->status_vps == 'change_ip') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
          }
          elseif ($vps->status_vps == 'reset_password') {
            $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
          }
          elseif ($vps->status_vps == 'expire') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
          }
          elseif ($vps->status_vps == 'suspend') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
          }
          elseif ($vps->status_vps == 'admin_off') {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
          }
          elseif ($vps->status_vps == 'delete_vps') {
              $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
          }
          elseif ($vps->status_vps == 'change_user') {
              $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
          }
          else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
          }
        } else {
          $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
        }
        $data['data'][] = $vps;
      }
      $data['total'] = $list_vps->total();
      $data['perPage'] = $list_vps->perPage();
      $data['current_page'] = $list_vps->currentPage();
      
      // dd($data);
      return $data;
    }
    public function list_all_vps_with_sl($sl)
    {
       $list_vps = $this->vps->where('user_id', Auth::user()->id)->where('location', 'cloudzone')->where('status', 'Active')->orderBy('id', 'desc')->with('product')->paginate($sl);
       $data = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       $data['data'] = [];
       foreach ($list_vps as $key => $vps) {
          //  cấu hình
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
              }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if(!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
              $customer = $this->get_customer_with_vps($vps->id);
              if(!empty($customer)) {
                  if(!empty($customer->customer_name)) {
                    $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
                  } else {
                    $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
                  }
              } else {
                  $vps->link_customer = 'Chính tôi';
              }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
          $data['data'][] = $vps;
       }
       $data['total'] = $list_vps->total();
       $data['perPage'] = $list_vps->perPage();
       $data['current_page'] = $list_vps->currentPage();
       return $data;
    }
    public function check_list_vps_with_user($list_id)
    {
        if ( is_array($list_id) ) {
            foreach ($list_id as $key => $id) {
               $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
               if (!isset($vps)) {
                  return true;
               }
            }
        } else {
            $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $list_id)->first();
            if (!isset($vps)) {
               return true;
            }
        }
        // $n++;
        return false;
    }
    public function search_multi_vps_all($ips)
    {
      $list_vps = [];
      foreach ($ips as $key => $ip) {
        $list_vps[] = $this->vps->where('user_id', Auth::user()->id)->where('status', 'Active')
                         ->where('location', 'cloudzone')
                         ->orderBy('id', 'desc')
                         ->where('ip', trim($ip))
                         ->with('product')->first();
      }
      $data = [];
      $data['data'] = [];
      $status_vps = config('status_vps');
      $billing = config('billing');
      foreach ($list_vps as $key => $vps) {
        //  cấu hình
        if ( isset($vps) ) {
          $product = $vps->product;
          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
          // Addon
          if (!empty($vps->vps_config)) {
            $vps_config = $vps->vps_config;
            if (!empty($vps_config)) {
              $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
              $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
              $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
            }
          }
          $vps->text_vps_config = $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk';
          // khách hàng
          if (!empty($vps->ma_kh)) {
              if (!empty($vps->customer)) {
                if(!empty($vps->customer->customer_name)) {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_name;
                }
                else {
                  $vps->link_customer =  $vps->customer->ma_customer . ' - ' .  $vps->customer->customer_tc_name;
                }
              }
              else {
                $vps->link_customer = 'Chính tôi';
              }
          } else {
            $customer = $this->get_customer_with_vps($vps->id);
            if(!empty($customer)) {
              if(!empty($customer->customer_name)) {
                $vps->link_customer = $customer->ma_customer . ' - ' . $customer->customer_name;
              } else {
                $vps->link_customer =  $customer->ma_customer . ' - ' .  $customer->customer_tc_name;
              }
            } else {
              $vps->link_customer = 'Chính tôi';
            }
          }
          // ngày tạo
          $vps->date_create = date('d-m-Y', strtotime($vps->date_create));
          // ngày kết thúc
          $vps->next_due_date = date('d-m-Y', strtotime($vps->next_due_date));
          // thời gian thuê
          $vps->text_billing_cycle = $billing[$vps->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($vps->next_due_date)){
            $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
            $date = date('Y-m-d');
            $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
            if($next_due_date <= $date) {
              $isExpire = true;
            }
            $date_now = strtotime(date('Y-m-d'));
            if ($next_due_date < $date_now) {
              $expired = true;
            }
          }
          $vps->isExpire = $isExpire;
          $vps->expired = $expired;
          // tổng thời gian thuê
          $total_time = '';
          $create_date = new Carbon($vps->date_create);
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInYears($create_date) ) {
            $year = $next_due_date->diffInYears($create_date);
            $total_time = $year . ' Năm ';
            $create_date = $create_date->addYears($year);
            $month = $next_due_date->diffInMonths($create_date);
            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
            if ( $create_date != $next_due_date ) {
              $total_time .= $month . ' Tháng';
            }
          } else {
            $diff_month = $next_due_date->diffInMonths($create_date);
            if ( $create_date != $next_due_date ) {
              $total_time = $diff_month . ' Tháng';
            }
            else {
              $total_time = $diff_month . ' Tháng';
            }
          }
          $vps->total_time = $total_time;
          // trạng thái vps
          if (!empty( $vps->status_vps )) {
            if ($vps->status_vps == 'on') {
              $vps->text_status_vps = '<span class="text-success" data-id="' . $vps->id . '">Đang bật</span>';
            }
            elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang tạo ...</span>';
            }
            elseif ($vps->status_vps == 'rebuild') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang cài lại ...</span>';
            }
            elseif ($vps->status_vps == 'change_ip') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đổi IP ...</span>';
            }
            elseif ($vps->status_vps == 'reset_password') {
              $vps->text_status_vps = '<span class="vps-progressing" data-id="' . $vps->id . '">Đang đặt lại mật khẩu...</span>';
            }
            elseif ($vps->status_vps == 'expire') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã hết hạn</span>';
            }
            elseif ($vps->status_vps == 'suspend') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đang bị khoá</span>';
            }
            elseif ($vps->status_vps == 'admin_off') {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Admin tắt</span>';
            }
            elseif ($vps->status_vps == 'delete_vps') {
                $vps->text_status_vps = '<span class="text-danger">Đã xóa</span>';
            }
            elseif ($vps->status_vps == 'change_user') {
                $vps->text_status_vps = '<span class="text-danger">Đã chuyển</span>';
            }
            else {
              $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Đã tắt</span>';
            }
          } else {
            $vps->text_status_vps = '<span class="text-danger" data-id="' . $vps->id . '">Chưa được tạo</span>';
          }
        }
        $data['data'][] = $vps;
      }
      $data['total'] = 0;
      $data['perPage'] = 0;
      $data['current_page'] = 0;
      // dd($data);
      return $data;
    }
    public function rebuild_vps($data)
    {
        $list_id = $data['id'];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' bị Admin tắt. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }

            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã bị khóa. Quý khách vui lòng không chọn các VPS có trạng đã khóa và liên hệ lại với chúng tôi để mở lại VPS',
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã hủy. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đã chuyển. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'rebuild' ) {
                  $data = [
                    'success' => 'error',
                    'error' => 'VPS có ip: ' . $vps->ip . ' đang cài lại. Quý khách vui lòng không chọn các VPS có trạng thái tắt do admin hoặc đang hủy',
                  ];
                  return $data;
            }

        }
        $os = $data['os'];
        $security = !empty($data['security']) ? true : false;
        if ( empty($security) ) {
            if ($os == 1) {
                $os = 11;
            }
            else if ($os == 2) {
                $os = 12;
            }
            else if ($os == 3) {
                $os = 13;
            }
            else if ($os == 31) {
                $os = 31;
            }
            else if ($os == 15) {
                $os = 31;
            }
            else {
                $os = 14;
            }
        }

        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $data_rebuild = [
              "mob" => "rebuild",
              "name" => $vps->vm_id,
              "template" => $os,
              'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            $rebuild = $this->dashboard->rebuild_vps($data_rebuild, $vps->vm_id, $vps);
            // $rebuild = true;
            if ($rebuild) {
                $vps->status_vps = 'rebuild';
                $vps->os = $os;
                $vps->security = !empty($security) ? true : false;
                $vps->save();
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'cài đặt lại',
                      'model' => 'User/Order',
                      'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                      'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    continue;
                }
            } else {
                $data = [
                  'success' => '',
                  'error' => 'VPS có ip: ' . $vps->ip . ' cài đặt lại không thành công',
                ];
                return $data;
            }
        }

        $data = [
          'success' => true,
        ];
        return $data;

    }
    public function check_hostings_with_user($list_id)
    {
      // dd($list_id);
       if ( is_array($list_id) ) {
         foreach ($list_id as $key => $id) {
            $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $id)->first();
            if (!isset($hosting)) {
               return true;
            }
         }
       } else {
         $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $list_id)->first();
         if (!isset($hosting)) {
            return true;
         }
       }
       return false;
    }
     public function request_expired_hosting($list_id_hosting)
    {
        $data = [];
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_hosting)) {
            foreach ($list_id_hosting as $key => $id_hosting) {
               $hosting = $this->hosting->find($id_hosting);
               if (!empty($hosting->expire_billing_cycle)) {
                  $check = true;
                  $total += $hosting->price_override;
                  $billing_cycle = $hosting->billing_cycle;
               }
               else if (!empty($hosting->price_override)) {
                  $total += $hosting->price_override;
                  $billing_cycle = $hosting->billing_cycle;
               } else {
                  $product = $hosting->product;
                  if (!empty($billing_cycle)) {
                      $total += $product->pricing[$billing_cycle];
                  } else {
                      $total += $product->pricing[$hosting->billing_cycle];
                  }
               }
            }
            if ($check) {
              $data['price_override']['error'] = 0;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_hosting as $key => $id_hosting) {
                    $hosting = $this->hosting->find($id_hosting);
                    if ( !empty($hosting->price_override) ) {
                        // 1 tháng
                        if ($hosting->billing_cycle == 'monthly') {
                            $total_monthly += $hosting->price_override;
                        } else {
                            $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                        }
                        // 2 tháng
                        if ($hosting->billing_cycle == 'twomonthly') {
                            $total_twomonthly += $hosting->price_override;
                        } else {
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                        }
                        // 3 tháng
                        if ($hosting->billing_cycle == 'quarterly') {
                            $total_quarterly += $hosting->price_override;
                        } else {
                            $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                        }
                        // 6 tháng
                        if ($hosting->billing_cycle == 'semi_annually') {
                            $total_semi_annually += $hosting->price_override;
                        } else {
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                        }
                        // 1 năm
                        if ($hosting->billing_cycle == 'annually') {
                            $total_annually += $hosting->price_override;
                        } else {
                            $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                        }
                        // 2 năm
                        if ($hosting->billing_cycle == 'biennially') {
                            $total_biennially += $hosting->price_override;
                        } else {
                            $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                        }
                        // 3 năm
                        if ($hosting->billing_cycle == 'triennially') {
                            $total_triennially += $hosting->price_override;
                        } else {
                            $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                        }
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $hosting = $this->hosting->find($list_id_hosting);
            if (!empty($hosting->expire_billing_cycle)) {
               $data['expire_billing_cycle'] = 1;
               $total += $hosting->price_override;
               $billing_cycle = $hosting->billing_cycle;
               $data['price_override']['error'] = 0;
               $data['price_override']['billing_cycle'] = $billing_cycle;
               $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
               $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $product = $hosting->product;
                if ( !empty($hosting->price_override) ) {
                    // 1 tháng
                    if ($hosting->billing_cycle == 'monthly') {
                        $total_monthly += $hosting->price_override;
                    } else {
                        $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                    }
                    // 2 tháng
                    if ($hosting->billing_cycle == 'twomonthly') {
                        $total_twomonthly += $hosting->price_override;
                    } else {
                        $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                    }
                    // 3 tháng
                    if ($hosting->billing_cycle == 'quarterly') {
                        $total_quarterly += $hosting->price_override;
                    } else {
                        $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                    }
                    // 6 tháng
                    if ($hosting->billing_cycle == 'semi_annually') {
                        $total_semi_annually += $hosting->price_override;
                    } else {
                        $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                    }
                    // 1 năm
                    if ($hosting->billing_cycle == 'annually') {
                        $total_annually += $hosting->price_override;
                    } else {
                        $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                    }
                    // 2 năm
                    if ($hosting->billing_cycle == 'biennially') {
                        $total_biennially += $hosting->price_override;
                    } else {
                        $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                    }
                    // 3 năm
                    if ($hosting->billing_cycle == 'triennially') {
                        $total_triennially += $hosting->price_override;
                    } else {
                        $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                    }
                } else {
                    $total_monthly += !empty($product->pricing['monthly']) ? $product->pricing['monthly'] : 0;
                    $total_twomonthly += !empty($product->pricing['twomonthly']) ? $product->pricing['twomonthly'] : 0;
                    $total_quarterly += !empty($product->pricing['quarterly']) ? $product->pricing['quarterly'] : 0;
                    $total_semi_annually += !empty($product->pricing['semi_annually']) ? $product->pricing['semi_annually'] : 0;
                    $total_annually += !empty($product->pricing['annually']) ? $product->pricing['annually'] : 0;
                    $total_biennially += !empty($product->pricing['biennially']) ? $product->pricing['biennially'] : 0;
                    $total_triennially += !empty($product->pricing['triennially']) ? $product->pricing['triennially'] : 0;
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        $data['error'] = '';
        return $data;
    }
    public function loadVpsWithIds($ids)
    {
        // Log::info($ids);
        $data = [];
        foreach ($ids as $key => $id) {
            $data[] = $this->vps->find($id);
        }
        return $data;
    }
    public function expired_hosting($id, $billing_cycle)
    {
        $hosting = $this->hosting->find($id);
        if (!empty($hosting)) {
            $product = $this->product->find($hosting->product_id);
            $invoiced = $hosting->detail_order;
            if (!empty($hosting->price_override)) {
              $total = $hosting->price_override;
            } else {
              $total = $product->pricing[$billing_cycle];
            }
            // create order
            $data_order = [
                'user_id' => Auth::user()->id,
                'total' =>  $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',//thiếu status gia hạn
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
            ];
            $order = $this->order->create($data_order);
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'hosting',
                'due_date' => $date,
                'description' => 'Gia hạn',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => Auth::user()->id,
                'addon_id' => 0,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // create history pay
            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => Auth::user()->id,
                'ma_gd' => 'GDEXH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $history_pay = $this->history_pay->create($data_history);
            // gui mail
            if ($order && $detail_order && $data_history) {
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $hosting->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'hosting',
                ];
                $this->order_expired->create($data_order_expired);

                $hosting->expired_id = $detail_order->id;
                $hosting->save();

                $email = $this->email->find($product->meta_product->email_expired);

                $billings = config('billing');
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn hosting'
                ];
                // try {
                //   $data_tb_send_mail = [
                //     'type' => 'mail_order_expire_hosting',
                //     'type_service' => 'hosting',
                //     'user_id' => $user->id,
                //     'status' => false,
                //     'content' => serialize($data),
                //   ];
                //   $this->send_mail->create($data_tb_send_mail);
                // } catch (\Exception $e) {
                //   report($e);
                //   return $detail_order->id;
                // }

                return $detail_order->id;
            }
        } else {
          return false;
        }
    }
    public function suspend_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_Hosting',
          'description' => ' dịch vụ Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
          'description_user' => ' dịch vụ Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=hosting">' . $hosting->domain . '</a> ',
          'service' => $hosting->domain,
        ];
        $this->log_activity->create($data_log);

        if ($hosting->location == 'vn') {
            $suppend = $this->da->off($hosting);
        } else {
            $suppend = $this->da_si->off($hosting);
        }
        return $suppend;
    }
    public function check_hosting_with_user($id)
    {
        $hosting = $this->hosting->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($hosting)) {
           return true;
        }
       return false;
    }
    public function unSuspend($id)
    {
        $hosting = $this->hosting->find($id);
        if ($hosting->location == 'vn') {
           $unsuppend = $this->da->unSuspend($hosting);
        } else {
           $unsuppend = $this->da_si->unSuspend($hosting);
        }
        return $unsuppend;
    }
    public function terminated_hosting($id)
    {
        $hosting = $this->hosting->where('user_id', Auth::user()->id )->where('id', $id)->first();
        if ( isset($hosting) ) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'hủy',
            'model' => 'User/Services_Hosting',
            'description' => 'dịch vụ Hosting <a target="_blank" target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
            'description_user' => 'dịch vụ Hosting <a target="_blank" target="_blank" href="/service/detail/' . $hosting->id . '?type=hosting">' . $hosting->domain . '</a> ',
            'service' => $hosting->domain,
          ];
          $this->log_activity->create($data_log);

          $terminated = $this->da->suspendHosting($hosting);
          return $terminated;
        } else {
          return false;
        }
    }
    public function check_vps_with_user($id)
    {
        $vps = $this->vps->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
        if (!isset($vps)) {
           return true;
        }
        return false;
    }
    public function expired_vps($id, $billing_cycle)

    {

        $vps = $this->vps->find($id);
        if (!empty($vps)) {
            $product = $this->product->find($vps->product_id);
            $invoiced = $vps->detail_order;
            $total = 0;
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ( $vps->billing_cycle == $billing_cycle ) {
                      $total += $vps->price_override;
                    } else {
                      $billing_price = config('billingDashBoard');
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total = (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }

            $data_order = [
                'user_id' => Auth::user()->id,
                'total' => $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : '',
            ];
            $order = $this->order->create($data_order);

            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            if ($vps->location == 'cloudzone') {
              $data_order_detail = [
                  'order_id' => $order->id,
                  'type' => 'VPS',
                  'due_date' => $date,
                  'description' => 'Gia hạn',
                  'status' => 'unpaid',
                  'sub_total' => $total,
                  'quantity' => 1,
                  'user_id' => Auth::user()->id,
                  'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
              ];
            } else {
              $data_order_detail = [
                  'order_id' => $order->id,
                  'type' => 'VPS US',
                  'due_date' => $date,
                  'description' => 'Gia hạn',
                  'status' => 'unpaid',
                  'sub_total' => $total,
                  'quantity' => 1,
                  'user_id' => Auth::user()->id,
                  'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
              ];
            }

            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            $user = $this->user->find(Auth::user()->id);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $vps->expired_id = $detail_order->id;
            $vps->save();

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'description_user' => ' gia hạn VPS <a target="_blank"  href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $email = $this->email->find($product->meta_product->email_expired);
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps',
            ];
            // try {
            //   $data_tb_send_mail = [
            //     'type' => 'mail_order_expire_vps',
            //     'type_service' => 'vps',
            //     'user_id' => $user->id,
            //     'status' => false,
            //     'content' => serialize($data),
            //   ];
            //   $this->send_mail->create($data_tb_send_mail);
            // } catch (\Exception $e) {
            //   report($e);
            //   return $detail_order->id;
            // }

            return $detail_order->id;

        } else {
            return false;
        }
    }
    public function onVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->onVPS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'bật',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    public function offVPS($id)
    {
        $vps = $this->vps->find($id);
        $off = $this->dashboard->offVPS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $off;
    }
    public function restartVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }
        $on = $this->dashboard->restartVPS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'khởi động lại',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $on;
    }
    public function terminatedVps($id)
    {
        $vps = $this->vps->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services_VPS',
          'description' => 'dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->domain . '</a> ',
          'description_user' => 'dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->domain . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        if ( $vps->vm_id < 1000000000 ) {
          $on = $this->dashboard->terminatedVps($vps);
        }
        else {
          $on = $this->dashboard->terminatedVpsUS($vps);
        }
        // $on = true;
        return $on;
    }
 
    public function terminated_vps($id)
    {
        $vps = $this->vps->where('user_id', Auth::user()->id )->where('id', $id)->first();
        if ( isset($vps) ) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'hủy',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
            'service' => $vps->ip,
          ];
          $this->log_activity->create($data_log);
          if ( $vps->vm_id < 1000000000 ) {
            $terminated = $this->dashboard->terminatedVps($vps);
          } else {
            $terminated = $this->dashboard->terminatedVpsUS($vps);
          }

          // $terminated = true;
          return $terminated;
        } else {
          return false;
        }
    }
    public function onVPSUS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->onVPSUS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'bật',
          'model' => 'User/Services_VPS_US',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    public function offVPSUS($id)
    {
        $vps = $this->vps->find($id);
        $off = $this->dashboard->offVPSUS($vps);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tắt',
          'model' => 'User/Services_VPS',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);
        return $off;
    }
    public function autoRefurnVPS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ($vps->status_vps == 'delete_vps') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ($vps->status_vps == 'change_user') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $vps->auto_refurn = true;

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cài đặt',
          'model' => 'User/Services_VPS',
          'description' => ' tự động gia hạn dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' tự động gia hạn dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $vps->save();
    }
    public function offAutoRefurnVPS($id)
    {
        $vps = $this->vps->find($id);

        $vps->auto_refurn = false;

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cài đặt',
          'model' => 'User/Services_VPS',
          'description' => ' tắt tự động gia hạn dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' tắt tự động gia hạn dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $vps->save();
    } public function gia_han_nhieu_vps($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        if ($vps->location == 'cloudzone') {
          $data_order_detail = [
              'order_id' => $order->id,
              'type' => 'VPS',
              'due_date' => $date,
              'description' => 'Gia hạn',
              'status' => 'unpaid',
              'sub_total' => $total / $quantity,
              'quantity' => $quantity,
              'user_id' => Auth::user()->id,
              'addon_id' => 0,
          ];
        }
        else {
          $data_order_detail = [
              'order_id' => $order->id,
              'type' => 'VPS US',
              'due_date' => $date,
              'description' => 'Gia hạn',
              'status' => 'unpaid',
              'sub_total' => $total / $quantity,
              'quantity' => $quantity,
              'user_id' => Auth::user()->id,
              'addon_id' => 0,
          ];
        }
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            // if (!empty($vps)) {
            //     $vps->expired_id = $detail_order->id;
            //     $vps->billing_cycle = $billing_cycle;
            //     $vps->save();
            // }
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');
            // ghi log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt hàng',
                'model' => 'User/Order_Services',
                'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'description_user' => ' gia hạn VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (Exception $e) {
              report($e);
            }

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps',
            ];
            // try {
            //   $data_tb_send_mail = [
            //     'type' => 'mail_order_expire_vps',
            //     'type_service' => 'vps',
            //     'user_id' => $user->id,
            //     'status' => false,
            //     'content' => serialize($data),
            //   ];
            //   $this->send_mail->create($data_tb_send_mail);
            // } catch (\Exception $e) {
            //   report($e);
            //   return $detail_order->id;
            // }

            return $detail_order->id;
        } else {
            return false;
        }
    }
    public function change_ip_vps_us($list_id)
    {
        $data = [
          'success' => [],
          'error' => '',
          'detail_order_id' => 0,
          'check' => 0
        ];
        $check = false;
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'success' => '',
                    'error' => 6
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'success' => '',
                    'error' => 5
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'delete_vps' ) {
                  $data = [
                    'success' => '',
                    'error' => 5
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'expire' ) {
              $data = [
                'success' => '',
                'error' => 7,
              ];
              return $data;
            }
            if ( $vps->status_vps == 'change_ip' ) {
              $data = [
                'success' => '',
                'error' => 8,
              ];
              return $data;
            }
        }

        $user = Auth::user();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
          return false;
        }
        $total = 20000 * count($list_id);
        // kiểm tra tài khoản
        if ( $credit->value <  $total ) {
          $data = [
            'success' => '',
            'error' => 1111,
          ];
          return $data;
        }

        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'change ip vps us',
            'makh' => '',
            'type' => 'vps_us'
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'change_ip',
            'due_date' => $date,
            'description' => 'change ip',
            'status' => 'unpaid',
            'sub_total' => 20000,
            'quantity' => count($list_id),
            'user_id' => $user->id,
            'addon_id' => '0',
            // 'paid_date' => date('Y-m-d'),
            'payment_method' => 1,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '5',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'method_gd_invoice' => 'credit',
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        $string_list_ip_vps = '';
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            $string_list_ip_vps = $vps->ip . '<br>';
            $data_change_ip = [
                'detail_order_id' => $detail_order->id,
                'vps_id' => $vps->id,
                'ip' => $vps->ip,
            ];
            $this->order_change_ip->create($data_change_ip);
            if ( $vps->vm_id < 1000000000 ) {
              $reques_vcenter = $this->dashboard->admin_change_ip($vps);
            } else {
              $reques_vcenter = $this->dashboard->change_ip_vps_us($vps);
            }
            // $reques_vcenter = true;
            if ($reques_vcenter) {
              // đổi status
              $vps->status_vps = 'change_ip';
              $vps->save();
              $check = true;
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'yêu cầu',
                'model' => 'User/Order',
                'description' => ' đổi IP VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'description_user' => ' đổi IP VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            }
        }
        // ghi log giao dịch
        if ( $check ) {
          $order->status = 'Finish';
          $order->save();

          $detail_order->status = 'paid';
          $detail_order->paid_date = date('Y-m-d');
          $detail_order->save();

          $history_pay->status = 'Active';
          $history_pay->save();
          $data_log_payment = [
            'history_pay_id' => $history_pay->id,
            'before' => $credit->value,
            'after' => $credit->value - $total,
          ];
          $this->log_payment->create($data_log_payment);
          // trừ tiền
          $credit->value = $credit->value - $total;
          $credit->save();

          $data = [
              'success' => [],
              'error' => '',
              'detail_order_id' => $detail_order->id,
          ];
        } else {
          $data = [
            'success' => [],
            'error' => 'Yêu cầu đổi IP thất bại',
            'detail_order_id' => 0,
            'check' => 0
          ];
        }

        return $data;
    }
    public function reset_password($list_id)
    {
        $data = [
          'success' => true,
          'error' => '',
        ];
        foreach ($list_id as $key => $id) {
            $vps = $this->vps->find($id);
            if ($vps->status_vps == 'suspend') {
                  $data = [
                    'error' => 6
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'admin_off') {
                  $data = [
                    'success' => '',
                    'error' => 4,
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'change_user' ) {
                  $data = [
                    'error' => 5
                  ];
                  return $data;
            }
            if ( $vps->status_vps == 'delete_vps' ) {
                  $data = [
                    'error' => 5
                  ];
                  return $data;
            }
            if ($vps->status_vps == 'cancel') {
                  $data = [
                    'success' => '',
                    'error' => 5,
                  ];
                  return $data;
            }
        }
        foreach ($list_id as $key => $id) {
          $vps = $this->vps->find($id);
          $reset_password = $this->dashboard->reset_password($vps);
          if ($reset_password) {
            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt lại mật khẩu',
                'model' => 'User/Order',
                'description' => ' VPS US <a target="_blank" href="/admin/vps_us/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                'description_user' => ' VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (\Exception $e) {
              report($e);
              continue;
            }
          } else {
            $data = [
              'success' => '',
              'error' => 'VPS có ip: ' . $vps->ip . ' đặt lại mật khẩu không thành công',
            ];
            return $data;
          }
        }
        return $data;
    }
    public function check_list_proxy_with_user($listId)
    {
      foreach ($listId as $key => $id) {
        $proxy = $this->proxy->where('id', $id)->where('user_id', Auth::id())->first();
        if ( !isset($proxy) ) {
          return true;
        }
      }
      return false;
    }
    public function expired_list_proxy($list_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
            $proxy = $this->proxy->find($id);
            if (!empty($proxy)) {
                $product = $this->product->find($proxy->product_id);
                if (!empty($proxy->price_override)) {
                    if ($proxy->billing_cycle == $billing_cycle) {
                        $total += $proxy->price_override;
                    } else {
                      $total_price_1 = ($proxy->price_override * $billing_price[$billing_cycle]) / $billing_price[$proxy->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
            'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Proxy',
          'due_date' => $date,
          'description' => 'Gia hạn Proxy',
          'status' => 'unpaid',
          'sub_total' => $total / $quantity,
          'quantity' => $quantity,
          'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_id as $key => $id) {
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $id,
                'billing_cycle' => $billing_cycle,
                'type' => 'proxy',
            ];
            $this->order_expired->create($data_order_expired);
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXP' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {
            $billings = config('billing');
            // ghi log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt hàng',
                'model' => 'User/Order_Services',
                'description' => ' gia hạn Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'description_user' => ' gia hạn Proxy <a target="_blank" href="/service/detail/' . $proxy->id . '?type=proxy">' . $proxy->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $proxy->ip,
              ];
              $this->log_activity->create($data_log);
            } catch (Exception $e) {
              report($e);
            }
            return $detail_order->id;
        } else {
            return false;
        }
    }
    
    public function terminatedProxy($list_id)
    {
      foreach ($list_id as $key => $id) {
        $proxy = $this->proxy->find($id);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'hủy',
          'model' => 'User/Services',
          'description' => ' dịch vụ Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> ',
          'description_user' => ' dịch vụ Proxy <a target="_blank" href="/service/detail/' . $proxy->id . '?type=vps">' . $proxy->ip . '</a> ',
          'service' => $proxy->ip,
        ];
        $this->log_activity->create($data_log);

        $proxy->status = 'cancel';
        $proxy->save();
      }
      $data['success'] = true;
      return  $data;
    }
    public function restartVPSUS($id)
    {
        $vps = $this->vps->find($id);
        if ($vps->status_vps == 'admin_off') {
              $data = [
                'error' => 4
              ];
              return $data;
        }

        if ($vps->status_vps == 'suspend') {
              $data = [
                'error' => 6
              ];
              return $data;
        }

        if ($vps->status_vps == 'cancel') {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        if ( $vps->status_vps == 'change_user' ) {
              $data = [
                'error' => 5
              ];
              return $data;
        }

        $on = $this->dashboard->restartVPSUS($vps);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'Khởi động lại',
          'model' => 'User/Services_VPS_US',
          'description' => ' dịch vụ VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
          'description_user' => ' dịch vụ VPS US <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
          'service' =>  $vps->ip,
        ];
        $this->log_activity->create($data_log);

        return $on;
    }
    public function loadCusomter()
    {
        $customers = $this->customer->where('user_id', Auth::user()->id)->get();
        return $customers;
    }
    public function updateCusomter($data)
    {
        $vps = $this->vps->find($data['vps_id']);
        if (isset($vps)) {
           $vps->ma_kh = $data['makh'];
           $vps->save();
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'cập nhật',
             'model' => 'User/Services_VPS',
             'description' => ' khách hàng cho VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
             'description_user' => ' khách hàng cho VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
             'service' =>  $vps->ip,
           ];
           $this->log_activity->create($data_log);

           return $vps;
        } else {
           return false;
        }
    }
    public function updateDescription($data)
    {
        $vps = $this->vps->find($data['vps_id']);
        if (isset($vps)) {
           $vps->description = $data['description'];
           $vps->save();
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'cập nhật',
             'model' => 'User/Services_VPS',
             'description' => ' ghi chú cho VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
             'description_user' => ' ghi chú cho VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
             'service' =>  $vps->ip,
           ];
           $this->log_activity->create($data_log);
           return $vps;
        } else {
           return false;
        }
    }
    public function check_server_with_user($id)
    {
        $server = $this->server->where('user_id', Auth::user()->id)->where('id', $id)->first();
        if (!isset($server)) {
           return true;
        }
        return false;
    }
    public function request_expired_list_server($list_id)
    {
        $data = [];
        $total = 0;
        $billing = config('billing');
        $total_monthly = 0;
        $total_twomonthly = 0;
        $total_quarterly = 0;
        $total_semi_annually = 0;
        $total_annually = 0;
        $total_biennially = 0;
        $total_triennially  = 0;
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);
          // dd($server);
          $billing_cycle = $server->billing_cycle;
          $product = $server->product;
          if ( !empty($server->amount) ) {
            // dd($billing_cycle, $product->pricing['quarterly'] , $server);
            // 1 tháng
            if ( $billing_cycle == 'monthly' ) {
                $total_monthly += $server->amount;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? $server->amount * 2 : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? $server->amount  * 3 : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 6 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 12 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 24 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 36 : 0;
            }
            elseif ( $billing_cycle == 'twomonthly' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? $server->amount / 2 : 0;
                $total_twomonthly += $server->amount;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  * 3/2 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 3 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 6 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 12 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 18 : 0;
            }
            elseif ( $billing_cycle == 'quarterly' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  * 2/3 , -3) : 0;
                $total_quarterly += $server->amount;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? $server->amount  * 2 : 0;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 4 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 8 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 12 : 0;
            }
            elseif ( $billing_cycle == 'semi_annually' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 2 , -3) : 0;
                $total_semi_annually += $server->amount;
                $total_annually += !empty($product->pricing['annually']) ? $server->amount  * 2 : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 4 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 6 : 0;
            }
            elseif ( $billing_cycle == 'annually' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount / 4 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 2 , -3) : 0;
                $total_annually += $server->amount;
                $total_biennially += !empty($product->pricing['biennially']) ? $server->amount  * 2 : 0;
                $total_triennially  += !empty($product->pricing['triennially']) ? $server->amount * 3 : 0;
            }
            elseif ( $billing_cycle == 'biennially' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 24 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 8 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 4 , -3) : 0;
                $total_annually += !empty($product->pricing['biennially']) ? (int) round( $server->amount / 2 , -3) : 0;
                $total_biennially += $server->amount;
                $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $server->amount * 3/2 , -3) : 0;
            }
            elseif ( $billing_cycle == 'triennially' ) {
                $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $server->amount  / 36 , -3) : 0;
                $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $server->amount  / 18  , -3) : 0;
                $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $server->amount  / 12 , -3) : 0;
                $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $server->amount  / 6 , -3) : 0;
                $total_annually += !empty($product->pricing['biennially']) ? (int) round( $server->amount  / 3 , -3) : 0;
                $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $server->amount / 2 , -3) : 0;
                $total_triennially  += $server->amount;
            }
          }
          else {
              $total_monthly += $product->pricing['monthly'];
              $total_twomonthly += $product->pricing['twomonthly'];
              $total_quarterly += $product->pricing['quarterly'];
              $total_semi_annually += $product->pricing['semi_annually'];
              $total_annually += $product->pricing['annually'];
              $total_biennially += $product->pricing['biennially'];
              $total_triennially += $product->pricing['triennially'];
              if ( !empty($server->server_config) ) {
                 $server_config = $server->server_config;
                 if ( !empty($server_config->ram) ) {
                    foreach ($server->server_config_rams as $server_config_ram) {
                      if ( !empty( $server_config_ram->product->pricing ) ) {
                        if ( $total_monthly ) {
                          $total_monthly += $server_config_ram->product->pricing['monthly'];
                        }
                        if ( $total_twomonthly ) {
                          $total_twomonthly += $server_config_ram->product->pricing['twomonthly'];
                        }
                        if ( $total_quarterly ) {
                          $total_quarterly += $server_config_ram->product->pricing['quarterly'];
                        }
                        if ( $total_semi_annually ) {
                          $total_semi_annually += $server_config_ram->product->pricing['semi_annually'];
                        }
                        if ( $total_annually ) {
                          $total_annually += $server_config_ram->product->pricing['annually'];
                        }
                        if ( $total_biennially ) {
                          $total_biennially += $server_config_ram->product->pricing['biennially'];
                        }
                        if ( $total_triennially ) {
                          $total_triennially += $server_config_ram->product->pricing['triennially'];
                        }
                      }
                    }
                 }
                 if ( !empty($server_config->ip) ) {
                    foreach ($server->server_config_ips as $server_config_ip) {
                      if ( !empty( $server_config_ip->product->pricing ) ) {
                        if ( $total_monthly ) {
                          $total_monthly += $server_config_ip->product->pricing['monthly'];
                        }
                        if ( $total_twomonthly ) {
                          $total_twomonthly += $server_config_ip->product->pricing['twomonthly'];
                        }
                        if ( $total_quarterly ) {
                          $total_quarterly += $server_config_ip->product->pricing['quarterly'];
                        }
                        if ( $total_semi_annually ) {
                          $total_semi_annually += $server_config_ip->product->pricing['semi_annually'];
                        }
                        if ( $total_annually ) {
                          $total_annually += $server_config_ip->product->pricing['annually'];
                        }
                        if ( $total_biennially ) {
                          $total_biennially += $server_config_ip->product->pricing['biennially'];
                        }
                        if ( $total_triennially ) {
                          $total_triennially += $server_config_ip->product->pricing['triennially'];
                        }
                      }
                    }
                 }
                 if ( !empty($server_config->disk2) ) {
                    if ( !empty( $server_config->product_disk2->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk2->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk2->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk2->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk2->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk2->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk2->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk2->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk3) ) {
                    if ( !empty( $server_config->product_disk3->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk3->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk3->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk3->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk3->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk3->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk3->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk3->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk4) ) {
                    if ( !empty( $server_config->product_disk4->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk4->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk4->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk4->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk4->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk4->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk4->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk4->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk5) ) {
                    if ( !empty( $server_config->product_disk5->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk5->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk5->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk5->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk5->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk5->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk5->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk5->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk6) ) {
                    if ( !empty( $server_config->product_disk6->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk6->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk6->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk6->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk6->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk6->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk6->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk6->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk7) ) {
                    if ( !empty( $server_config->product_disk7->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk7->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk7->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk7->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk7->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk7->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk7->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk7->pricing['triennially'];
                      }
                    }
                 }
                 if ( !empty($server_config->disk8) ) {
                    if ( !empty( $server_config->product_disk8->pricing ) ) {
                      if ( $total_monthly ) {
                        $total_monthly += $server_config->product_disk8->pricing['monthly'];
                      }
                      if ( $total_twomonthly ) {
                        $total_twomonthly += $server_config->product_disk8->pricing['twomonthly'];
                      }
                      if ( $total_quarterly ) {
                        $total_quarterly += $server_config->product_disk8->pricing['quarterly'];
                      }
                      if ( $total_semi_annually ) {
                        $total_semi_annually += $server_config->product_disk8->pricing['semi_annually'];
                      }
                      if ( $total_annually ) {
                        $total_annually += $server_config->product_disk8->pricing['annually'];
                      }
                      if ( $total_biennially ) {
                        $total_biennially += $server_config->product_disk8->pricing['biennially'];
                      }
                      if ( $total_triennially ) {
                        $total_triennially += $server_config->product_disk8->pricing['triennially'];
                      }
                    }
                 }
              }
          }
        }
        // $list_id_vps=[248, 243];
        $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
        $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
        $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
        $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
        $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
        $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
        $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
        $data['error'] = '';
        $data['billing_cycle'] = $billing_cycle;
        return $data;
    }
    public function expired_list_server($list_id, $billing_cycle)
    {
        $totals = 0;
        $billings = config('billingDashBoard');
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);
          if ( !empty($server->amount) ) {
            if ( $server->billing_cycle == $billing_cycle ) {
                $totals += $server->amount;
            } else {
              $totals += $this->get_total_server( $id, $billing_cycle );
            }
          } else {
            $totals += $this->get_total_server( $id, $billing_cycle );
          }
        }
        // dd($totals);
        $data_order = [
          'user_id' => Auth::user()->id,
          'total' => $totals,
          'status' => 'Pending',
          'description' => 'Gia hạn Server',
          'type' => 'expired',
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'Server',
          'due_date' => $date,
          'description' => 'Gia hạn',
          'status' => 'unpaid',
          'sub_total' => $totals,
          'quantity' => count($list_id),
          'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);

        $user = $this->user->find(Auth::user()->id);
        $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXS' . strtoupper(substr(sha1(time()), 34, 39)),
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $totals,
          'status' => 'Pending',
          'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);

        $billings = config('billing');
        // Tạo lưu trữ khi gia hạn
        foreach ($list_id as $key => $id) {
          $server = $this->server->find($id);

          $data_order_expired = [
            'detail_order_id' => $detail_order->id,
            'expired_id' => $id,
            'billing_cycle' => $billing_cycle,
            'type' => 'server',
          ];
          $this->order_expired->create($data_order_expired);
          // ghi log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/HistoryPay',
            'description' => ' gia hạn Server <a target="_blank" href="/admin/servers/detail/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'description_user' => ' gia hạn Server <a target="_blank" href="/service/server/detail/' . $server->id . '?type=vps">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
            'service' =>  $server->ip,
          ];
          $this->log_activity->create($data_log);
        }
        $data = [
          'status' => true,
          'invoice_id' => $detail_order->id,
        ];
        return $data;
    }
    public function terminatedServers($list_id)
    {
      foreach ($list_id as $key => $id) {
          $server = $this->server->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          $server->status_server = 'cancel';
          $cancel = $server->save();
          if (!$cancel) {
            return false;
          }
        }
        return true;
    }
    public function check_list_server_with_user($list_id)
    {
        foreach ($list_id as $key => $id) {
          $server = $this->server->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          if (!isset($server)) {
            return true;
          }
        }
        return false;
    }
    public function select_server($request)
    {
        $data = [];
        $data['data'] = [];
        $status_vps = config('status_vps');
        $billing = config('billing');
        $qtt = !empty($request['qtt']) ? $request['qtt'] : 25;
        $action = !empty($request['action']) ? $request['action'] : 'use';
        $list_server = [];
        if ( $action == 'use' ) {
            if ( !empty($request['multi_q']) ) {
              $list_ip = explode(',', $request['multi_q']);
              foreach ($list_ip as $key => $ip) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip', trim($ip))
                    ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                    ->orderBy('id', 'desc')->first();
                  if ( !isset($serverQ) ) {
                    $serverQ = $this->server->where('user_id', Auth::user()->id)
                      ->where('status', 'Active')
                      ->where('ip2', trim($ip))
                      ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                      ->orderBy('id', 'desc')->first();
                  }
                  if ( !empty($serverQ->id) ) {
                    $checkArr = $this->check_search_server($list_server, $serverQ);
                    if ($checkArr) {
                      $list_server[] = $serverQ;
                    }
                  } else {
                    $list_server[] = $serverQ;
                  }
              }
              $data['total'] = 0;
              $data['perPage'] = 0;
              $data['current_page'] = 0;
            } else {
              if (!empty($request['sort'])) {
                $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
                if ( $sort == 'DESC' || $sort == 'ASC' ) {
                  $sort = $this->mysql_real_escape_string($sort);
                } else {
                  $sort = $this->mysql_real_escape_string('DESC');
                }
                $list_server = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                  ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
              } else {
                $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
                ->orderBy('id', 'desc')->paginate($qtt);
              }
              $data['total'] = $list_server->total();
              $data['perPage'] = $list_server->perPage();
              $data['current_page'] = $list_server->currentPage();
            }
        }
        elseif ( $action == 'on' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        elseif ( $action == 'nearly' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->where('status_server', 'expire')
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->where('status_server', 'expire')
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->where('status_server', 'expire')
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->where('status_server', 'expire')
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        elseif ( $action == 'cancel' ) {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->whereIn('status_server', [ 'cancel', 'delete_server', 'change_user' ])
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        else {
          if ( !empty($request['multi_q']) ) {
            $list_ip = explode(',', $request['multi_q']);
            foreach ($list_ip as $key => $ip) {
                $serverQ = $this->server->where('user_id', Auth::user()->id)
                  ->where('status', 'Active')
                  ->where('ip', trim($ip))
                  ->orderBy('id', 'desc')->first();
                if ( !isset($serverQ) ) {
                  $serverQ = $this->server->where('user_id', Auth::user()->id)
                    ->where('status', 'Active')
                    ->where('ip2', trim($ip))
                    ->orderBy('id', 'desc')->first();
                }
                if ( !empty($serverQ->id) ) {
                  $checkArr = $this->check_search_server($list_server, $serverQ);
                  if ($checkArr) {
                    $list_server[] = $serverQ;
                  }
                } else {
                  $list_server[] = $serverQ;
                }
            }
            $data['total'] = 0;
            $data['perPage'] = 0;
            $data['current_page'] = 0;
          } else {
            if (!empty($request['sort'])) {
              $sort = !empty($request['sort']) ? $request['sort'] : 'DESC';
              if ( $sort == 'DESC' || $sort == 'ASC' ) {
                $sort = $this->mysql_real_escape_string($sort);
              } else {
                $sort = $this->mysql_real_escape_string('DESC');
              }
              $list_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status', 'Active')
                ->orderByRaw('next_due_date ' . $sort)->paginate($qtt);
            } else {
              $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status', 'Active')
              ->orderBy('id', 'desc')->paginate($qtt);
            }
            $data['total'] = $list_server->total();
            $data['perPage'] = $list_server->perPage();
            $data['current_page'] = $list_server->currentPage();
          }
        }
        foreach ($list_server as $key => $server) {
            if ( !empty($server) ) {
              //  cấu hình
              $server->ip = !empty($server->ip) ? $server->ip : '';
              $server->ip2 = !empty($server->ip2) ? $server->ip2 : '';
              // $product = $vps->product;
              if ( !empty($server->config_text) ) {
                $server->text_server_config = $server->config_text . '<br>';
              } else {
                $product = $server->product;
                $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                $server->text_server_config = $cpu . ' (' . $cores . '), ' . $ram . ', ' . $disk . ' (' . $cores . ')<br>';
              }
              $addonConfig = '';
              $addonConfig = $this->get_config_server($server->id);
              $server->text_server_config .= $addonConfig;
              // ngày tạo
              $server->date_create = date('d-m-Y', strtotime($server->date_create));
              // ngày kết thúc
              $server->next_due_date = date('d-m-Y', strtotime($server->next_due_date));
              // thời gian thuê
              $server->text_billing_cycle = $billing[$server->billing_cycle];
              // kiểm tra vps gần hết hạn hay đã hết hạn
              $isExpire = false;
              $expired = false;
              if(!empty($server->next_due_date)){
                  $next_due_date = strtotime(date('Y-m-d', strtotime($server->next_due_date)));
                  $date = date('Y-m-d');
                  $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                  if($next_due_date <= $date) {
                    $isExpire = true;
                  }
                  $date_now = strtotime(date('Y-m-d'));
                  if ($next_due_date < $date_now) {
                      $expired = true;
                  }
              }
              $server->isExpire = $isExpire;
              $server->expired = $expired;
              $text_day = '';
              $now = Carbon::now();
              $next_due_date = new Carbon($server->next_due_date);
              $diff_date = $next_due_date->diffInDays($now);
              if ( $next_due_date->isPast() ) {
                $text_day = 'Hết hạn ' . $diff_date . ' ngày';
              } else {
                $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
              }
              $server->text_day = $text_day;
              // tổng thời gian thuê
              $total_time = '';
              $create_date = new Carbon($server->date_create);
              $next_due_date = new Carbon($server->next_due_date);
              if ( $next_due_date->diffInYears($create_date) ) {
                $year = $next_due_date->diffInYears($create_date);
                $total_time = $year . ' Năm ';
                $create_date = $create_date->addYears($year);
                $month = $next_due_date->diffInMonths($create_date);
                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                if ( $create_date != $next_due_date ) {
                  $total_time .= $month . ' Tháng';
                }
              } else {
                $diff_month = $next_due_date->diffInMonths($create_date);
                if ( $create_date != $next_due_date ) {
                  $total_time = $diff_month . ' Tháng';
                }
                else {
                  $total_time = $diff_month . ' Tháng';
                }
              }
              $server->total_time = $total_time;
              // trạng thái vps
              if (!empty( $server->status_server )) {
                if ($server->status_server == 'on') {
                  $server->text_status_server = '<span class="text-success" data-id="' . $server->id . '">Đang bật</span>';
                }
                elseif ($server->status_server == 'off') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
                }
                elseif ($server->status_server == 'progressing') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang tạo ...</span>';
                }
                elseif ($server->status_server == 'rebuild') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang cài lại ...</span>';
                }
                elseif ($server->status_server == 'change_ip') {
                  $server->text_status_server = '<span class="server-progressing" data-id="' . $server->id . '">Đang đổi IP ...</span>';
                }
                elseif ($server->status_server == 'expire') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã hết hạn</span>';
                }
                elseif ($server->status_server == 'suspend') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đang bị khoá</span>';
                }
                elseif ($server->status_server == 'admin_off') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Admin tắt</span>';
                }
                elseif ($server->status_server == 'cancel') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã hủy</span>';
                }
                elseif ($server->status_server == 'delete_server') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã xóa</span>';
                }
                elseif ($server->status_server == 'change_user') {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Chuyển khách hàng</span>';
                }
                else {
                  $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Đã tắt</span>';
                }
              } else {
                $server->text_status_server = '<span class="text-danger" data-id="' . $server->id . '">Chưa được tạo</span>';
              }
              // gia của vps
              $total = 0;
              if ( !empty($server->amount) ) {
                $total = $server->amount;
              }
              else {
                $total = !empty( $product->pricing[$server->billing_cycle] ) ? $product->pricing[$server->billing_cycle] : 0;
                if (!empty($server->server_config)) {
                  $server_config = $server->server_config;
                  $pricing_addon = 0;
                  if ( !empty( $server_config->ram ) ) {
                      foreach ($server->server_config_rams as $server_config_ram) {
                          $pricing_addon += !empty($server_config_ram->product->pricing[$server->billing_cycle]) ? $server_config_ram->product->pricing[$server->billing_cycle] : 0;
                      }
                  }
                  if ( !empty( $server_config->ip ) ) {
                      foreach ($server->server_config_ips as $server_config_ip) {
                          $pricing_addon += !empty($server_config_ip->product->pricing[$server->billing_cycle]) ? $server_config_ip->product->pricing[$server->billing_cycle] : 0;
                      }
                  }
                  if ( !empty($server_config->disk2) ) {
                    $pricing_addon += $server_config->product_disk2->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk3) ) {
                    $pricing_addon += $server_config->product_disk3->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk4) ) {
                    $pricing_addon += $server_config->product_disk4->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk5) ) {
                    $pricing_addon += $server_config->product_disk5->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk6) ) {
                    $pricing_addon += $server_config->product_disk6->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk7) ) {
                    $pricing_addon += $server_config->product_disk7->pricing[$server->billing_cycle];
                  }
                  if ( !empty($server_config->disk8) ) {
                    $pricing_addon += $server_config->product_disk8->pricing[$server->billing_cycle];
                  }
                  $total += $pricing_addon;
                }
              }
              $server->amount = number_format( $total ,0,",",".");
            }
            $data['data'][] = $server;
        }
        // dd($data);
        return $data;
    }
    public function check_list_email_hosting_with_user($list_id)
    {
        foreach ($list_id as $key => $id) {
          $email_hosting = $this->email_hosting->where( 'user_id', Auth::user()->id )->where('id', $id)->first();
          if (!isset($email_hosting)) {
            return true;
          }
        }
        return false;
    }
    public function request_expired_email_hosting($list_id_hosting)
    {
        $data = [];
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_hosting)) {
            foreach ($list_id_hosting as $key => $id_hosting) {
               $email_hosting = $this->email_hosting->find($id_hosting);
               if (!empty($email_hosting->expire_billing_cycle)) {
                  $check = true;
                  $total += $email_hosting->price_override;
                  $billing_cycle = $email_hosting->billing_cycle;
               }
               else if (!empty($email_hosting->price_override)) {
                  $total += $email_hosting->price_override;
                  $billing_cycle = $email_hosting->billing_cycle;
               } else {
                  $product = $email_hosting->product;
                  if (!empty($billing_cycle)) {
                      $total += $product->pricing[$billing_cycle];
                  } else {
                      $total += $product->pricing[$email_hosting->billing_cycle];
                  }
               }
            }
            if ($check) {
              $data['price_override']['expire_billing_cycle'] = 1;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_hosting as $key => $id_hosting) {
                    $email_hosting = $this->hosting->find($id_hosting);
                    if ( !empty($email_hosting->price_override) ) {
                        // 1 tháng
                        if ($email_hosting->billing_cycle == 'monthly') {
                            $total_monthly += $email_hosting->price_override;
                        } else {
                            $total_monthly += $product->pricing['monthly'];
                        }
                        // 2 tháng
                        if ($email_hosting->billing_cycle == 'twomonthly') {
                            $total_twomonthly += $email_hosting->price_override;
                        } else {
                            $total_twomonthly += $product->pricing['twomonthly'];
                        }
                        // 3 tháng
                        if ($email_hosting->billing_cycle == 'quarterly') {
                            $total_quarterly += $email_hosting->price_override;
                        } else {
                            $total_quarterly += $product->pricing['quarterly'];
                        }
                        // 6 tháng
                        if ($email_hosting->billing_cycle == 'semi_annually') {
                            $total_semi_annually += $email_hosting->price_override;
                        } else {
                            $total_semi_annually += $product->pricing['semi_annually'];
                        }
                        // 1 năm
                        if ($email_hosting->billing_cycle == 'annually') {
                            $total_annually += $email_hosting->price_override;
                        } else {
                            $total_annually += $product->pricing['annually'];
                        }
                        // 2 năm
                        if ($email_hosting->billing_cycle == 'biennially') {
                            $total_biennially += $email_hosting->price_override;
                        } else {
                            $total_biennially += $product->pricing['biennially'];
                        }
                        // 3 năm
                        if ($email_hosting->billing_cycle == 'triennially') {
                            $total_triennially += $email_hosting->price_override;
                        } else {
                            $total_triennially += $product->pricing['triennially'];
                        }
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $email_hosting = $this->email_hosting->find($list_id_hosting);
            if (!empty($email_hosting->expire_billing_cycle)) {
               $data['expire_billing_cycle'] = 1;
               $total += $email_hosting->price_override;
               $billing_cycle = $email_hosting->billing_cycle;
               $data['price_override']['error'] = 0;
               $data['price_override']['billing_cycle'] = $billing_cycle;
               $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
               $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            } else {
                $product = $email_hosting->product;
                if ( !empty($email_hosting->price_override) ) {
                    // 1 tháng
                    if ($email_hosting->billing_cycle == 'monthly') {
                        $total_monthly += $email_hosting->price_override;
                    } else {
                        $total_monthly += $product->pricing['monthly'];
                    }
                    // 2 tháng
                    if ($email_hosting->billing_cycle == 'twomonthly') {
                        $total_twomonthly += $email_hosting->price_override;
                    } else {
                        $total_twomonthly += $product->pricing['twomonthly'];
                    }
                    // 3 tháng
                    if ($email_hosting->billing_cycle == 'quarterly') {
                        $total_quarterly += $email_hosting->price_override;
                    } else {
                        $total_quarterly += $product->pricing['quarterly'];
                    }
                    // 6 tháng
                    if ($email_hosting->billing_cycle == 'semi_annually') {
                        $total_semi_annually += $email_hosting->price_override;
                    } else {
                        $total_semi_annually += $product->pricing['semi_annually'];
                    }
                    // 1 năm
                    if ($email_hosting->billing_cycle == 'annually') {
                        $total_annually += $email_hosting->price_override;
                    } else {
                        $total_annually += $product->pricing['annually'];
                    }
                    // 2 năm
                    if ($email_hosting->billing_cycle == 'biennially') {
                        $total_biennially += $email_hosting->price_override;
                    } else {
                        $total_biennially += $product->pricing['biennially'];
                    }
                    // 3 năm
                    if ($email_hosting->billing_cycle == 'triennially') {
                        $total_triennially += $email_hosting->price_override;
                    } else {
                        $total_triennially += $product->pricing['triennially'];
                    }
                } else {
                    $total_monthly += $product->pricing['monthly'];
                    $total_twomonthly += $product->pricing['twomonthly'];
                    $total_quarterly += $product->pricing['quarterly'];
                    $total_semi_annually += $product->pricing['semi_annually'];
                    $total_annually += $product->pricing['annually'];
                    $total_biennially += $product->pricing['biennially'];
                    $total_triennially += $product->pricing['triennially'];
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        $data['error'] = '';
        return $data;
    }
    public function select_email_hosting($qtt , $action)
    {
       if ( $action == 'on' ) {
            $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                // ->where('ip', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
       elseif ( $action == 'nearly' ) {
          $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_hosting', 'cancel')
                    ->orWhere('status_hosting', 'expire')
                    ->orWhere('status_hosting', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_email_hosting as $key => $email_hosting) {
          //  cấu hình
          $product = $email_hosting->product;
          $email_hosting->text_product = $product->name;
          // ngày tạo
          $email_hosting->date_create = date('d-m-Y', strtotime($email_hosting->date_create));
          // ngày kết thúc
          $email_hosting->next_due_date = date('d-m-Y', strtotime($email_hosting->next_due_date));
          // thời gian thuê
          $email_hosting->text_billing_cycle = $billing[$email_hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($email_hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($email_hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $email_hosting->isExpire = $isExpire;
          $email_hosting->expired = $expired;
          // trạng thái vps
          if (!empty( $email_hosting->status_hosting )) {
            if ($email_hosting->status_hosting == 'on') {
              $email_hosting->text_status_server = '<span class="text-success" data-id="' . $email_hosting->id . '">Đang bật</span>';
            }
            elseif ($email_hosting->status_hosting == 'progressing') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang tạo ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'rebuild') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang cài lại ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'change_ip') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang đổi IP ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'expire') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã hết hạn</span>';
            }
            elseif ($email_hosting->status_hosting == 'suspend') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đang bị khoá</span>';
            }
            elseif ($email_hosting->status_hosting == 'admin_off') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Admin tắt</span>';
            }
            else {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã tắt</span>';
            }
          } else {
            $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Chưa được tạo</span>';
          }
          // gia của $email_hosting
          $price_hosting = 0;
          if ( !empty($email_hosting->price_override) ) {
             $price_hosting = $email_hosting->price_override;
          } else {
             $price_hosting = !empty($email_hosting->detail_order->sub_total) ? $email_hosting->detail_order->sub_total : 0;

          }
          $email_hosting->amount = number_format( $price_hosting ,0,",",".") . ' VNĐ';
          $data['data'][] = $email_hosting;
       }
       $data['total'] = $list_email_hosting->total();
       $data['perPage'] = $list_email_hosting->perPage();
       $data['current_page'] = $list_email_hosting->currentPage();
       // dd($data);
       return $data;
    }
    public function search_email_hosting($q , $qtt , $action)
    {
       if ( $action == 'on' ) {
            $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                ->where('domain', 'LIKE' , '%'.$q.'%')
                // ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->paginate($qtt);
        }
       elseif ( $action == 'nearly' ) {
          $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
              ->where('domain', 'LIKE' , '%'.$q.'%')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_hosting', 'cancel')
                    ->orWhere('status_hosting', 'expire')
                    ->orWhere('status_hosting', 'suspend');
              })
              ->orderBy('id', 'desc')->paginate($qtt);
       }
       else {
         $list_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
             ->where('domain', 'LIKE' , '%'.$q.'%')
             ->where('status', 'Active')
             ->orderBy('id', 'desc')->paginate($qtt);
       }
       $data = [];
       $data['data'] = [];
       $status_vps = config('status_vps');
       $billing = config('billing');
       foreach ($list_email_hosting as $key => $email_hosting) {
          //  cấu hình
          $product = $email_hosting->product;
          $email_hosting->text_product = $product->name;
          // ngày tạo
          $email_hosting->date_create = date('d-m-Y', strtotime($email_hosting->date_create));
          // ngày kết thúc
          $email_hosting->next_due_date = date('d-m-Y', strtotime($email_hosting->next_due_date));
          // thời gian thuê
          $email_hosting->text_billing_cycle = $billing[$email_hosting->billing_cycle];
          // kiểm tra vps gần hết hạn hay đã hết hạn
          $isExpire = false;
          $expired = false;
          if(!empty($email_hosting->next_due_date)){
              $next_due_date = strtotime(date('Y-m-d', strtotime($email_hosting->next_due_date)));
              $date = date('Y-m-d');
              $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
              if($next_due_date <= $date) {
                $isExpire = true;
              }
              $date_now = strtotime(date('Y-m-d'));
              if ($next_due_date < $date_now) {
                  $expired = true;
              }
          }
          $email_hosting->isExpire = $isExpire;
          $email_hosting->expired = $expired;
          // trạng thái vps
          if (!empty( $email_hosting->status_hosting )) {
            if ($email_hosting->status_hosting == 'on') {
              $email_hosting->text_status_server = '<span class="text-success" data-id="' . $email_hosting->id . '">Đang bật</span>';
            }
            elseif ($email_hosting->status_hosting == 'progressing') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang tạo ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'rebuild') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang cài lại ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'change_ip') {
              $email_hosting->text_status_server = '<span class="vps-progressing" data-id="' . $email_hosting->id . '">Đang đổi IP ...</span>';
            }
            elseif ($email_hosting->status_hosting == 'expire') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã hết hạn</span>';
            }
            elseif ($email_hosting->status_hosting == 'suspend') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đang bị khoá</span>';
            }
            elseif ($email_hosting->status_hosting == 'admin_off') {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Admin tắt</span>';
            }
            else {
              $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Đã tắt</span>';
            }
          } else {
            $email_hosting->text_status_server = '<span class="text-danger" data-id="' . $email_hosting->id . '">Chưa được tạo</span>';
          }
          // gia của $email_hosting
          $price_hosting = 0;
          if ( !empty($email_hosting->price_override) ) {
             $price_hosting = $email_hosting->price_override;
          } else {
             $price_hosting = !empty($email_hosting->detail_order->sub_total) ? $email_hosting->detail_order->sub_total : 0;

          }
          $email_hosting->amount = number_format( $price_hosting ,0,",",".") . ' VNĐ';
          $data['data'][] = $email_hosting;
       }
       $data['total'] = $list_email_hosting->total();
       $data['perPage'] = $list_email_hosting->perPage();
       $data['current_page'] = $list_email_hosting->currentPage();
       // dd($data);
       return $data;
    }
    public function request_expired_vps($list_id_vps)
    {
        $data = [];
        $data['error'] = 0;
        $data['expire_billing_cycle'] = false;
        $total = 0;
        $billing_cycle = '';
        $price_override = 0;
        $check = false;
        $billing = config('billing');
        $billingDashBoard = config('billingDashBoard');
        // $list_id_vps=[248, 243];
        if (is_array($list_id_vps)) {
            foreach ($list_id_vps as $key => $id_vps) {
               $vps = $this->vps->find($id_vps);
               if ($data['expire_billing_cycle'] == false) {
                  if (!empty($vps->expire_billing_cycle)) {
                     $data['expire_billing_cycle'] = true;
                  }
               }
               if ( !empty($vps->expire_billing_cycle) ) {
                   $check = true;
                   if ( $vps->price_override == 0 ) {
                      $data['error'] = 1;
                   }
                   $total += $vps->price_override;
                   $billing_cycle = $vps->billing_cycle;
               }
               else if (!empty($vps->price_override)) {
                  $total += $vps->price_override;
                  $billing_cycle = $vps->billing_cycle;
               } else {
                  $product = $vps->product;
                  // $billing_cycle = $vps->billing_cycle;
                  if (!empty($billing_cycle)) {
                    $total += $product->pricing[$billing_cycle];
                  } else {
                    $total += !empty($product->pricing[$vps->billing_cycle]) ? $product->pricing[$vps->billing_cycle] : 0;
                  }
               }
            }
            if ($check) {
              if ( $total == 0 ) {
                $data['error'] = 1;
              }
              $data['price_override']['error'] = 0;
              $data['price_override']['billing_cycle'] = $billing_cycle;
              $data['price_override']['text_billing_cycle'] = $billing[$billing_cycle];
              $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            }
            else {
                $total_monthly = 0;
                $total_twomonthly = 0;
                $total_quarterly = 0;
                $total_semi_annually = 0;
                $total_annually = 0;
                $total_biennially = 0;
                $total_triennially = 0;
                foreach ($list_id_vps as $key => $id_vps) {
                    $vps = $this->vps->find($id_vps);
                    $product = $vps->product;
                    if ( !empty($vps->price_override) ) {
                        // 1 tháng
                        if ( $billing_cycle == 'monthly' && $product->pricing['monthly'] != 0 ) {
                            $total_monthly += $vps->price_override;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? $vps->price_override * 2 : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? $vps->price_override  * 3 : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 6 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 12 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 24 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 36 : 0;
                        }
                        elseif ( $billing_cycle == 'twomonthly' && $product->pricing['twomonthly'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? $vps->price_override / 2 : 0;
                            $total_twomonthly += $vps->price_override;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  * 3/2 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 3 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 6 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 12 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 18 : 0;
                        }
                        elseif ( $billing_cycle == 'quarterly' && $product->pricing['quarterly'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  * 2/3 , -3) : 0;
                            $total_quarterly += $vps->price_override;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? $vps->price_override  * 2 : 0;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 4 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 8 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 12 : 0;
                        }
                        elseif ( $billing_cycle == 'semi_annually' && $product->pricing['semi_annually'] != 0 ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                            $total_semi_annually += $vps->price_override;
                            $total_annually += !empty($product->pricing['annually']) ? $vps->price_override  * 2 : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 4 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 6 : 0;
                        }
                        elseif ( $billing_cycle == 'annually' && $product->pricing['annually'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                            $total_annually += $vps->price_override;
                            $total_biennially += !empty($product->pricing['biennially']) ? $vps->price_override  * 2 : 0;
                            $total_triennially  += !empty($product->pricing['triennially']) ? $vps->price_override * 3 : 0;
                        }
                        elseif ( $billing_cycle == 'biennially' && $product->pricing['biennially'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 24 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 8 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                            $total_annually += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                            $total_biennially += $vps->price_override;
                            $total_triennially  += !empty($product->pricing['triennially']) ? (int) round( $vps->price_override * 3/2 , -3) : 0;
                        }
                        elseif ( $billing_cycle == 'triennially' && $product->pricing['triennially'] != 0  ) {
                            $total_monthly += !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 36 , -3) : 0;
                            $total_twomonthly += !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 18  , -3) : 0;
                            $total_quarterly += !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                            $total_semi_annually += !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                            $total_annually += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                            $total_biennially += !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                            $total_triennially  += $vps->price_override;
                        }
                    }
                    else {
                        $total_monthly += $product->pricing['monthly'];
                        $total_twomonthly += $product->pricing['twomonthly'];
                        $total_quarterly += $product->pricing['quarterly'];
                        $total_semi_annually += $product->pricing['semi_annually'];
                        $total_annually += $product->pricing['annually'];
                        $total_biennially += $product->pricing['biennially'];
                        $total_triennially += $product->pricing['triennially'];
                        if (!empty($vps->vps_config)) {
                            $addon_vps = $vps->vps_config;
                            $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                            $pricing_addon = 0;
                            foreach ($add_on_products as $key => $add_on_product) {
                                if (!empty($add_on_product->meta_product->type_addon)) {
                                  if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                    $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                    $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                    $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                    $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                    $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                    $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                    $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                  }
                                  if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                    $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                    $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                    $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                    $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                    $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                    $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                    $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                  }
                                  if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                    $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                    $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                    $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                    $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                    $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                    $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                    $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                  }
                                }
                            }
                        }
                    }

                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        else {
            $total_monthly = 0;
            $total_twomonthly = 0;
            $total_quarterly = 0;
            $total_semi_annually = 0;
            $total_annually = 0;
            $total_biennially = 0;
            $total_triennially = 0;
            $vps = $this->vps->find($list_id_vps);
            if ($data['expire_billing_cycle'] == false) {
               if (!empty($vps->expire_billing_cycle)) {
                  $data['expire_billing_cycle'] = true;
               }
            }
            if ( !empty($vps->expire_billing_cycle) ) {
                $check = true;
                if ( $vps->price_override == 0 ) {
                  $data['error'] = 1;
                }
                $total += $vps->price_override;
                $data['price_override']['error'] = 0;
                $data['price_override']['billing_cycle'] = $vps->billing_cycle;
                $data['price_override']['text_billing_cycle'] = $billing[$vps->billing_cycle];
                $data['price_override']['total'] = number_format($total,0,",",".") . ' VNĐ';
            }
            else {
                $product = $vps->product;
                if ( !empty($vps->price_override) ) {
                    $billing_cycle = $vps->billing_cycle;
                    // dd($billing_cycle, $product->pricing['twomonthly'] );
                    // 1 tháng
                    if ( $billing_cycle == 'monthly' && $product->pricing['monthly'] != 0 ) {
                        $total_monthly = $vps->price_override;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? $vps->price_override * 2 : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? $vps->price_override  * 3 : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 6 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 12 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 24 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 36 : 0;
                    }
                    elseif ( $billing_cycle == 'twomonthly' && $product->pricing['twomonthly'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? $vps->price_override / 2 : 0;
                        $total_twomonthly = $vps->price_override;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  * 3/2 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 3 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 6 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 12 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 18 : 0;
                    }
                    elseif ( $billing_cycle == 'quarterly' && $product->pricing['quarterly'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  * 2/3 , -3) : 0;
                        $total_quarterly = $vps->price_override;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? $vps->price_override  * 2 : 0;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 4 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 8 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 12 : 0;
                    }
                    elseif ( $billing_cycle == 'semi_annually' && $product->pricing['semi_annually'] != 0 ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                        $total_semi_annually = $vps->price_override;
                        $total_annually = !empty($product->pricing['annually']) ? $vps->price_override  * 2 : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 4 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 6 : 0;
                    }
                    elseif ( $billing_cycle == 'annually' && $product->pricing['annually'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 2 , -3) : 0;
                        $total_annually = $vps->price_override;
                        $total_biennially = !empty($product->pricing['biennially']) ? $vps->price_override  * 2 : 0;
                        $total_triennially  = !empty($product->pricing['triennially']) ? $vps->price_override * 3 : 0;
                    }
                    elseif ( $billing_cycle == 'biennially' && $product->pricing['biennially'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 24 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 8 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 4 , -3) : 0;
                        $total_annually = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                        $total_biennially = $vps->price_override;
                        $total_triennially  = !empty($product->pricing['triennially']) ? (int) round( $vps->price_override * 3/2 , -3) : 0;
                    }
                    elseif ( $billing_cycle == 'triennially' && $product->pricing['triennially'] != 0  ) {
                        $total_monthly = !empty($product->pricing['monthly']) ? (int) round( $vps->price_override  / 36 , -3) : 0;
                        $total_twomonthly = !empty($product->pricing['twomonthly']) ? (int) round( $vps->price_override  / 18  , -3) : 0;
                        $total_quarterly = !empty($product->pricing['quarterly']) ? (int) round( $vps->price_override  / 12 , -3) : 0;
                        $total_semi_annually = !empty($product->pricing['semi_annually']) ? (int) round( $vps->price_override  / 6 , -3) : 0;
                        $total_annually = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override  / 3 , -3) : 0;
                        $total_biennially = !empty($product->pricing['biennially']) ? (int) round( $vps->price_override / 2 , -3) : 0;
                        $total_triennially  = $vps->price_override;
                    }
                }
                else {
                    $total_monthly += $product->pricing['monthly'];
                    $total_twomonthly += $product->pricing['twomonthly'];
                    $total_quarterly += $product->pricing['quarterly'];
                    $total_semi_annually += $product->pricing['semi_annually'];
                    $total_annually += $product->pricing['annually'];
                    $total_biennially += $product->pricing['biennially'];
                    $total_triennially += $product->pricing['triennially'];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ( !empty($vps->price_override) ) {
                                    if ($vps->billing_cycle == 'monthly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 2 tháng
                                    if ($vps->billing_cycle == 'twomonthly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 3 tháng
                                    if ($vps->billing_cycle == 'quarterly') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 6 tháng
                                    if ($vps->billing_cycle == 'semi_annually') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 1 năm
                                    if ($vps->billing_cycle == 'annually') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 2 năm
                                    if ($vps->billing_cycle == 'biennially') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                        }
                                    }
                                    // 3 năm
                                    if ($vps->billing_cycle == 'triennially') {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                          $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                          $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                          $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                          $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                          $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                          $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                          $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                          $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                          $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                          $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                        }
                                    }
                                }
                                else {
                                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                      $total_monthly += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                      $total_twomonthly += $addon_vps->cpu * $add_on_product->pricing['twomonthly'];
                                      $total_quarterly += $addon_vps->cpu * $add_on_product->pricing['quarterly'];
                                      $total_semi_annually += $addon_vps->cpu * $add_on_product->pricing['semi_annually'];
                                      $total_annually += $addon_vps->cpu * $add_on_product->pricing['annually'];
                                      $total_biennially += $addon_vps->cpu * $add_on_product->pricing['biennially'];
                                      $total_triennially += $addon_vps->cpu * $add_on_product->pricing['triennially'];
                                    }
                                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                      $total_monthly += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                      $total_twomonthly += $addon_vps->ram * $add_on_product->pricing['twomonthly'];
                                      $total_quarterly += $addon_vps->ram * $add_on_product->pricing['quarterly'];
                                      $total_semi_annually += $addon_vps->ram * $add_on_product->pricing['semi_annually'];
                                      $total_annually += $addon_vps->ram * $add_on_product->pricing['annually'];
                                      $total_biennially += $addon_vps->ram * $add_on_product->pricing['biennially'];
                                      $total_triennially += $addon_vps->ram * $add_on_product->pricing['triennially'];
                                    }
                                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                      $total_monthly += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                      $total_twomonthly += $addon_vps->disk * $add_on_product->pricing['twomonthly'] / 10;
                                      $total_quarterly += $addon_vps->disk * $add_on_product->pricing['quarterly'] / 10;
                                      $total_semi_annually += $addon_vps->disk * $add_on_product->pricing['semi_annually'] / 10;
                                      $total_annually += $addon_vps->disk * $add_on_product->pricing['annually'] / 10;
                                      $total_biennially += $addon_vps->disk * $add_on_product->pricing['biennially'] / 10;
                                      $total_triennially += $addon_vps->disk * $add_on_product->pricing['triennially'] / 10;
                                    }
                                }
                            }
                        }
                    }
                }
                $data['price_override'] = '';
                $data['total']['monthly'] = number_format($total_monthly,0,",",".") . ' VNĐ';
                $data['total']['twomonthly'] = number_format($total_twomonthly,0,",",".") . ' VNĐ';
                $data['total']['quarterly'] = number_format($total_quarterly,0,",",".") . ' VNĐ';
                $data['total']['semi_annually'] = number_format($total_semi_annually,0,",",".") . ' VNĐ';
                $data['total']['annually'] = number_format($total_annually,0,",",".") . ' VNĐ';
                $data['total']['biennially'] = number_format($total_biennially,0,",",".") . ' VNĐ';
                $data['total']['triennially'] = number_format($total_triennially,0,",",".") . ' VNĐ';
            }
        }
        return $data;
    }
}
