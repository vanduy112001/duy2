<?php

namespace App\Repositories\User;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\DomainProduct;
use App\Model\Hosting;
use App\Model\Vps;
use Carbon\Carbon;
use App\Model\Email;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\Vietcombank;
use App\Model\HistoryPay;
use App\Model\DetailOrder;
use App\Model\LogActivity;
use App\Model\Momo;
use App\Model\Order;
use App\Model\OrderExpired;
use App\Model\LogPayment;
use App\Model\SendMail;
use App\Model\MailMarketing;
use App\Model\Proxy;
use App\Model\Server;
use App\Model\Colocation;
//API DOMAIN
use App\Services\Domain;
use Mail;
use GuzzleHttp\Client;
use http\Env\Request;

use App\Factories\AdminFactories;
use App\Factories\UserFactories;
// Job Mail
use App\Jobs\SendMailCronNearly;
use App\Jobs\MailErrorAutoRefund;
use App\Jobs\MailErrorAutoRefundUser;
use App\Jobs\MailFinishAutoRefund;
use App\Jobs\MailOrder;
use App\Jobs\OrderUpgradeHosting;
use App\Jobs\FinishOrderUpgradeHosting;
use App\Jobs\OrderAddonVPS;
use App\Jobs\FinishOrderAddonVPS;
use App\Jobs\CreateUser;
use App\Jobs\AdminSendMail;
use App\Jobs\FinishPayment;
use App\Jobs\AdminCreateFinish;
use App\Jobs\FinishRebuildVps;
use App\Jobs\RequestPayment;
use App\Jobs\MailErrorCreateVPS;
use App\Jobs\MailErrorCreateVPSUS;
use App\Jobs\MailErrorExpireVps;
use App\Jobs\MailErrorUpgradeVps;
use App\Jobs\MailErrorRebuildVps;
use App\Jobs\MailVerifyUser;
use App\Jobs\MailTutorialUser;
use App\Jobs\MailMarketingUser;
use App\Jobs\MailMarketingTiny;
use App\Jobs\SendMailForgot;
use App\Jobs\MailFinishChangeIp;
use App\Jobs\MailPaymentChangeIp;
use App\Jobs\MailMarketingVPSUK;
use App\Jobs\MailUpdateFeature;
use App\Jobs\MailTemplateMarketing;
use App\Jobs\SendMailCronNearlyProxy;
use App\Jobs\SendMailCronNearlyServer;
use App\Jobs\SendMailCronNearlyColocation;

class CronTab
{

    protected $group_product;
    protected $product;
    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;
    protected $subject;
    protected $user_email;
    protected $readnotification;
    protected $notification;
    protected $vcb;
    protected $history_pay;
    protected $admin_payment;
    protected $config_vcb;
    protected $log_activity;
    protected $momo;
    protected $config_momo;
    protected $email;
    protected $order;
    protected $detail_order;
    protected $order_expired;
    protected $log_payment;
    protected $send_mail;
    protected $mail_marketing;
    protected $order_addon_vps;
    protected $proxy;
    protected $server;
    protected $colo;
    // API
    protected $dashboard;
    protected $da;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->email = new Email;
        $this->mail_marketing = new MailMarketing;
        $this->domain_product = new DomainProduct;
        $this->vps = new Vps;
        $this->hosting = new Hosting;
        $this->user = new User;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->vcb = new Vietcombank();
        $this->momo = new Momo();
        $this->detail_order = new DetailOrder();
        $this->history_pay = new HistoryPay;
        $this->log_activity = new LogActivity();
        $this->order = new Order;
        $this->order_expired = new OrderExpired;
        $this->log_payment = new LogPayment;
        $this->send_mail = new SendMail;
        $this->proxy = new Proxy;
        $this->server = new Server;
        $this->colo = new Colocation;

        $this->config_vcb = config('vcb');
        $this->config_momo = config('config_momo');
        $this->admin_payment = AdminFactories::paymentRepositories();
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->transMomo = UserFactories::moMoRepository();
    }

    public function deleteVPS()
    {
        $list_vps = $this->vps->where('status', 'Active')
            ->whereIn('status_vps', [ 'expire' ])->get();
            // 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password',
        foreach ($list_vps as $key => $vps) {
            try {
                if (!empty($vps->next_due_date)) {
                    $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                    $now = new Carbon($now);
                    $next_due_date = new Carbon($vps->next_due_date);
                    $diff_date = $next_due_date->diffInDays($now);
                    if ($next_due_date->isPast()) {
                        if ( $diff_date >= 5 ) {
                            if ($vps->location == 'us') {
                                $vps->status_vps = 'delete_vps';
                                $vps->save();
                                try {
                                    $data_log = [
                                        'user_id' => 999999,
                                        'action' => 'xóa',
                                        'model' => 'CronTab/CronTab_VPS',
                                        'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
                                        'service' => $vps->ip
                                    ];
                                    $this->log_activity->create($data_log);
                                } catch (Exception $e) {
                                    report($e);
                                }
                            }
                            else {
                                $vps->status_vps = 'delete_vps';
                                $vps->save();
                                try {
                                    $data_log = [
                                      'user_id' => 999999,
                                      'action' => 'xóa',
                                      'model' => 'CronTab/CronTab_VPS',
                                      'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
                                      'service' => $vps->ip
                                    ];
                                    $this->log_activity->create($data_log);
                                } catch (Exception $e) {
                                    report($e);
                                }
                            }
                        }
                    }
                }
            } catch (Exception $e) {
                report($e);
            }
        }
    }

    public function deleteProxy()
    {
        $list_proxies = $this->proxy->where('status', 'expire')->get();
        foreach ($list_proxies as $key => $proxy) {
            try {
                if (!empty($proxy->next_due_date)) {
                    $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                    $now = new Carbon($now);
                    $next_due_date = new Carbon($proxy->next_due_date);
                    $diff_date = $next_due_date->diffInDays($now);
                    if ($next_due_date->isPast()) {
                        if ( $diff_date >= 5 ) {
                            try {
                                $proxy->status = 'delete';
                                $proxy->save();
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'xóa',
                                    'model' => 'CronTab/CronTab_Proxy',
                                    'description' => ' dịch vụ Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> ',
                                    'service' => $proxy->ip
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                        }
                    }
                }
            } catch (\Throwable $th) {
                //throw $th;
                report($th);
            }
        }
    }

    public function deleteServer()
    {
        $list_server = $this->server->where('status', 'expire')->get();
        foreach ($list_server as $key => $server) {
            try {
                if (!empty($server->next_due_date)) {
                    $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                    $now = new Carbon($now);
                    $next_due_date = new Carbon($server->next_due_date);
                    $diff_date = $next_due_date->diffInDays($now);
                    if ($next_due_date->isPast()) {
                        if ( $diff_date >= 5 ) {
                            try {
                                $server->status_server = 'delete_server';
                                $server->save();
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'xóa',
                                    'model' => 'CronTab/CronTab_Server',
                                    'description' => ' dịch vụ Server <a target="_blank" href="/admin/servers/detail/' . $server->id . '">' . $server->ip . '</a> ',
                                    'service' => $server->ip
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                        }
                    }
                }
            } catch (\Throwable $th) {
                //throw $th;
                report($th);
            }
        }
    }

    public function deleteColocation()
    {
        $list_colocation = $this->colo->where('status_colo', 'expire')->get();
        // dd($list_colocation);
        foreach ($list_colocation as $key => $colocation) {
            try {
                if (!empty($colocation->next_due_date)) {
                    $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                    $now = new Carbon($now);
                    $next_due_date = new Carbon($colocation->next_due_date);
                    $diff_date = $next_due_date->diffInDays($now);
                    if ($next_due_date->isPast()) {
                        if ( $diff_date >= 5 ) {
                            try {
                                $colocation->status_colo = 'delete_colo';
                                $colocation->save();
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'xóa',
                                    'model' => 'CronTab/CronTab_Colocation',
                                    'description' => ' dịch vụ colocation <a target="_blank" href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> ',
                                    'service' => $colocation->ip
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                        }
                    }
                }
            } catch (\Throwable $th) {
                //throw $th;
                report($th);
            }
        }
    }

    public function deleteHosting()
    {
        $list_hosting = $this->hosting->where('status', 'Active')->where('status', '!=', 'delete')->get();
        foreach ($list_hosting as $key => $hosting) {
            try {
              if (!empty($hosting->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $now = new Carbon($now);
                $next_due_date = new Carbon($hosting->next_due_date);
                $diff_date = $next_due_date->diffInDays($now);
                if ($next_due_date->isPast()) {
                    if ( $diff_date == 10 ) {
                        if ($hosting->location == 'vn') {
                            $this->da->delete_hosting($hosting);
                            try {
                                $data_log = [
                                'user_id' => 9999,
                                'action' => 'xóa',
                                'model' => 'CronTab/CronTab_Hosting',
                                'description' => 'dịch vụ Hosting <a target="_blank" href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
                                'service' => $hosting->domain
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                        }
                    }
                }
              }
            } catch (\Exception $e) {
              report($e);
            }
        }
    }

    public function expiredVPS()
    {
        $billing = config('billing');
        $data_nearly_expired_7 = []; //VPS gần hết hạn 5
        $data_nearly_expired_4 = [];// VPS gần hết hạn 4
        $data_nearly_expired_3 = []; // VPS gần hết hạn 3
        $data_nearly_expired_2 = [];// VPS gần hết hạn 2
        $data_nearly_expired_1 = []; // VPS gần hết hạn 1
        $data_nearly_expired_0 = []; //gần hết hạn 0
        $data_nearly_expired_minus_1 = []; //hết hạn 1 ngày
        $data_nearly_expired_minus_2 = []; //hết hạn 2 ngày
        $users = $this->user->get();
        foreach ($users as $key1 => $user) {
            try {
                $check = false;
                $check_us = false;
                $list_vps = $user->vps;
                $data = [];
                // Số lượng
                $quantity_7 = 0;
                $quantity_4 = 0;
                $quantity_3 = 0;
                $quantity_2 = 0;
                $quantity_1 = 0;
                $quantity_0 = 0;
                $quantity_minus_1 = 0;
                $quantity_minus_2 = 0;
                $quantity_minus_3 = 0;
                // tổng tiền
                $total_7 = 0;
                $total_4 = 0;
                $total_3 = 0;
                $total_2 = 0;
                $total_1 = 0;
                $total_0 = 0;
                $total_minus_1 = 0;
                $total_minus_2 = 0;
                $total_minus_3 = 0;
                //list ip
                $list_ip_7 = [];
                $list_ip_4 = [];
                $list_ip_3 = [];
                $list_ip_2 = [];
                $list_ip_1 = [];
                $list_ip_0 = [];
                $list_minus_1 = [];
                $list_minus_2 = [];
                $list_minus_3 = [];
                // ngày hết hạn
                $next_due_date_7 = '';
                $next_due_date_4 = '';
                $next_due_date_3 = '';
                $next_due_date_2 = '';
                $next_due_date_1 = '';
                $next_due_date_0 = '';
                $next_due_date_minus_1 = '';
                $next_due_date_minus_2 = '';
                $next_due_date_minus_3 = '';
                // VPS US
                $data_us = [];
                // Số lượng
                $quantity_7_us = 0;
                $quantity_4_us = 0;
                $quantity_3_us = 0;
                $quantity_2_us = 0;
                $quantity_1_us = 0;
                $quantity_0_us = 0;
                $quantity_minus_1_us = 0;
                $quantity_minus_2_us = 0;
                $quantity_minus_3_us = 0;
                // tổng tiền
                $total_7_us = 0;
                $total_4_us = 0;
                $total_3_us = 0;
                $total_2_us = 0;
                $total_1_us = 0;
                $total_0_us = 0;
                $total_minus_1_us = 0;
                $total_minus_2_us = 0;
                $total_minus_3_us = 0;
                //list ip
                $list_ip_7_us = [];
                $list_ip_4_us = [];
                $list_ip_3_us = [];
                $list_ip_2_us = [];
                $list_ip_1_us = [];
                $list_ip_0_us = [];
                $list_minus_1_us = [];
                $list_minus_2_us = [];
                $list_minus_3_us = [];
                // ngày hết hạn
                $next_due_date_7_us = '';
                $next_due_date_4_us = '';
                $next_due_date_3_us = '';
                $next_due_date_2_us = '';
                $next_due_date_1_us = '';
                $next_due_date_0_us = '';
                $next_due_date_minus_1_us = '';
                $next_due_date_minus_2_us = '';
                $next_due_date_minus_3_us = '';
                // Chay list VPS
                foreach ($list_vps as $key => $vps) {
                    try {
                        if ($vps->status_vps != 'delete_vps' && $vps->status_vps != 'change_user' && $vps->status_vps != 'cancel') {
                            if (!empty($vps->next_due_date)) {
                                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                                $now = new Carbon($now);
                                $next_due_date = new Carbon($vps->next_due_date);
                                $diff_date = $next_due_date->diffInDays($now);
                                if ($next_due_date->isPast()) {
                                    // 0 ngày
                                    if ( $diff_date == 0 ) {
                                        if ($vps->location == 'us') {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                              $cpu = $product->meta_product->cpu;
                                              $ram = $product->meta_product->memory;
                                              $disk = $product->meta_product->disk;
                                              // dd($product);
                                              if ( !empty($vps->price_override) ) {
                                                $total_0_us += $vps->price_override;
                                                $quantity_0_us++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_0_us[] = $text;
                                                $next_due_date_0_us = $vps->next_due_date;
                                              } else {
                                                if (!empty($product->pricing[$vps->billing_cycle])) {
                                                  $total_0_us += $product->pricing[$vps->billing_cycle];

                                                  $quantity_0_us++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_ip_0_us[] = $text;
                                                  $next_due_date_0_us = $vps->next_due_date;
                                                }
                                              }
                                          }
                                        }
                                        else {
                                            $product = $this->product->find($vps->product_id);
                                            if ( !empty($product->meta_product) ) {
                                                $cpu = $product->meta_product->cpu;
                                                $ram = $product->meta_product->memory;
                                                $disk = $product->meta_product->disk;
                                                // dd($product);
                                                if ( !empty($vps->price_override) ) {
                                                  $total_0 += $vps->price_override;
                                                  $quantity_0++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_ip_0[] = $text;
                                                  $next_due_date_0 = $vps->next_due_date;
                                                } else {
                                                  if (!empty($product->pricing[$vps->billing_cycle])) {
                                                    $total_0 += $product->pricing[$vps->billing_cycle];
                                                    if (!empty($vps->vps_config)) {
                                                        $addon_vps = $vps->vps_config;
                                                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                        $pricing_addon = 0;
                                                        foreach ($add_on_products as $key => $add_on_product) {
                                                            if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $cpu += $addon_vps->cpu;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $ram += $addon_vps->ram;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                  $disk += $addon_vps->disk;
                                                                }
                                                            }
                                                        }
                                                        $total_0 += $pricing_addon;
                                                    }

                                                    $quantity_0++;
                                                    $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                    $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                    $list_ip_0[] = $text;
                                                    $next_due_date_0 = $vps->next_due_date;
                                                  }
                                                }
                                            }
                                        }
                                    }
                                    if ( $diff_date == 1 ) {
                                        if ( $vps->location == 'cloudzone') {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                                $cpu = $product->meta_product->cpu;
                                                $ram = $product->meta_product->memory;
                                                $disk = $product->meta_product->disk;
                                                // dd($product);
                                                if ( !empty($vps->price_override) ) {
                                                  $total_minus_1 += $vps->price_override;
                                                  $quantity_minus_1++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_minus_1[] = $text;
                                                  $next_due_date_minus_1 = $vps->next_due_date;
                                                } else {
                                                  if (!empty($product->pricing[$vps->billing_cycle])) {
                                                    $total_minus_1 += $product->pricing[$vps->billing_cycle];
                                                    if (!empty($vps->vps_config)) {
                                                        $addon_vps = $vps->vps_config;
                                                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                        $pricing_addon = 0;
                                                        foreach ($add_on_products as $key => $add_on_product) {
                                                            if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $cpu += $addon_vps->cpu;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $ram += $addon_vps->ram;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                  $disk += $addon_vps->disk;
                                                                }
                                                            }
                                                        }
                                                        $total_minus_1 += $pricing_addon;
                                                    }

                                                    $quantity_minus_1++;
                                                    $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                    $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                    $list_minus_1[] = $text;
                                                    $next_due_date_minus_1 = $vps->next_due_date;
                                                  }
                                                }
                                        }
                                        }
                                        else {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                                $cpu = $product->meta_product->cpu;
                                                $ram = $product->meta_product->memory;
                                                $disk = $product->meta_product->disk;
                                                // dd($product);
                                                if ( !empty($vps->price_override) ) {
                                                  $total_minus_1_us += $vps->price_override;
                                                  $quantity_minus_1_us++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_minus_1_us[] = $text;
                                                  $next_due_date_minus_1_us = $vps->next_due_date;
                                                } else {
                                                  if (!empty($product->pricing[$vps->billing_cycle])) {
                                                    $total_minus_1_us += $product->pricing[$vps->billing_cycle];
                                                    $quantity_minus_1_us++;
                                                    $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                    $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                    $list_minus_1_us[] = $text;
                                                    $next_due_date_minus_1_us = $vps->next_due_date;
                                                  }
                                                }
                                          }
                                        }
                                    }
                                    if ( $diff_date == 2 ) {
                                        if ( $vps->location == 'cloudzone') {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                                $cpu = $product->meta_product->cpu;
                                                $ram = $product->meta_product->memory;
                                                $disk = $product->meta_product->disk;
                                                // dd($product);
                                                if ( !empty($vps->price_override) ) {
                                                  $total_minus_2 += $vps->price_override;
                                                  $quantity_minus_2++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_minus_2[] = $text;
                                                  $next_due_date_minus_2 = $vps->next_due_date;
                                                } else {
                                                  if (!empty($product->pricing[$vps->billing_cycle])) {
                                                    $total_minus_2 += $product->pricing[$vps->billing_cycle];
                                                    if (!empty($vps->vps_config)) {
                                                        $addon_vps = $vps->vps_config;
                                                        $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                        $pricing_addon = 0;
                                                        foreach ($add_on_products as $key => $add_on_product) {
                                                            if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $cpu += $addon_vps->cpu;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                  $ram += $addon_vps->ram;
                                                                }
                                                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                  $disk += $addon_vps->disk;
                                                                }
                                                            }
                                                        }
                                                        $total_minus_2 += $pricing_addon;
                                                    }

                                                    $quantity_minus_2++;
                                                    $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                    $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                    $list_minus_2[] = $text;
                                                    $next_due_date_minus_2 = $vps->next_due_date;
                                                  }
                                                }
                                        }
                                        }
                                        else {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                                $cpu = $product->meta_product->cpu;
                                                $ram = $product->meta_product->memory;
                                                $disk = $product->meta_product->disk;
                                                // dd($product);
                                                if ( !empty($vps->price_override) ) {
                                                  $total_minus_2_us += $vps->price_override;
                                                  $quantity_minus_2_us++;
                                                  $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                  $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                  $list_minus_2_us[] = $text;
                                                  $next_due_date_minus_2_us = $vps->next_due_date;
                                                } else {
                                                  if (!empty($product->pricing[$vps->billing_cycle])) {
                                                    $total_minus_2_us += $product->pricing[$vps->billing_cycle];
                                                    $quantity_minus_2_us++;
                                                    $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                    $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                    $list_minus_2_us[] = $text;
                                                    $next_due_date_minus_2_us = $vps->next_due_date;
                                                  }
                                                }
                                          }
                                        }
                                    }
                                    if ( $diff_date == 3 ) {
                                        if ( $vps->location == 'cloudzone') {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                            $cpu = $product->meta_product->cpu;
                                            $ram = $product->meta_product->memory;
                                            $disk = $product->meta_product->disk;
                                            // dd($product);
                                            if ( !empty($vps->price_override) ) {
                                              $total_minus_3 += $vps->price_override;
                                              $quantity_minus_3++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_minus_3[] = $text ;
                                              $next_due_date_minus_3 = $vps->next_due_date;
                                            }
                                            else {
                                              if (!empty($product->pricing[$vps->billing_cycle])) {
                                                $total_minus_3 += $product->pricing[$vps->billing_cycle];
                                                if (!empty($vps->vps_config)) {
                                                    $addon_vps = $vps->vps_config;
                                                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                    $pricing_addon = 0;
                                                    foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                              $cpu += $addon_vps->cpu;
                                                            }
                                                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                              $ram += $addon_vps->ram;
                                                            }
                                                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                              $disk += $addon_vps->disk;
                                                            }
                                                        }
                                                    }
                                                    $total_minus_3 += $pricing_addon;
                                                }

                                                $quantity_minus_3++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_minus_3[] = $text ;
                                                $next_due_date_minus_3 = $vps->next_due_date;
                                              }
                                            }
                                        }
                                        }
                                        else {
                                          $product = $this->product->find($vps->product_id);
                                          if ( !empty($product->meta_product) ) {
                                            $cpu = $product->meta_product->cpu;
                                            $ram = $product->meta_product->memory;
                                            $disk = $product->meta_product->disk;
                                            // dd($product);
                                            if ( !empty($vps->price_override) ) {
                                              $total_minus_3_us += $vps->price_override;
                                              $quantity_minus_3_us++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_minus_3_us[] = $text ;
                                              $next_due_date_minus_3_us = $vps->next_due_date;
                                            }
                                            else {
                                              if (!empty($product->pricing[$vps->billing_cycle])) {
                                                $total_minus_3_us += $product->pricing[$vps->billing_cycle];
                                                $quantity_minus_3_us++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_minus_3_us[] = $text ;
                                                $next_due_date_minus_3_us = $vps->next_due_date;
                                              }
                                            }
                                          }
                                        }
                                    }
                                }
                                // 5 ngày
                                elseif ( $diff_date == 5 ) {
                                    if ( $vps->location == 'cloudzone') {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                          $cpu = $product->meta_product->cpu;
                                          $ram = $product->meta_product->memory;
                                          $disk = $product->meta_product->disk;
                                          // dd($product);
                                          if (!empty($vps->price_override)) {
                                            $total_7 += $vps->price_override;
                                            $quantity_7++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_7[] = $text;
                                            $next_due_date_7 = $vps->next_due_date;
                                          } else {
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_7 += $product->pricing[$vps->billing_cycle];

                                              if (!empty($vps->vps_config)) {
                                                  $addon_vps = $vps->vps_config;
                                                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                  $pricing_addon = 0;
                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                            $cpu += $addon_vps->cpu;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                            $ram += $addon_vps->ram;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                            $disk += $addon_vps->disk;
                                                          }
                                                      }
                                                  }
                                                  $total_7 += $pricing_addon;
                                              }

                                              $quantity_7++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_7[] = $text;
                                              $next_due_date_7 = $vps->next_due_date;

                                            }
                                          }

                                      }
                                    }
                                    else {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                          $cpu = $product->meta_product->cpu;
                                          $ram = $product->meta_product->memory;
                                          $disk = $product->meta_product->disk;
                                          // dd($product);
                                          if (!empty($vps->price_override)) {
                                            $total_7_us += $vps->price_override;
                                            $quantity_7_us++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_7_us[] = $text;
                                            $next_due_date_7_us = $vps->next_due_date;
                                          } else {
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_7_us += $product->pricing[$vps->billing_cycle];
                                              $quantity_7_us++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_7_us[] = $text;
                                              $next_due_date_7_us = $vps->next_due_date;
                                            }
                                          }
                                      }
                                    }
                                }
                                // 4 ngày
                                elseif ( $diff_date == 4 ) {
                                    if ( $vps->location == 'cloudzone') {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                          $cpu = $product->meta_product->cpu;
                                          $ram = $product->meta_product->memory;
                                          $disk = $product->meta_product->disk;
                                          // dd($product);
                                          if (!empty($vps->price_override)) {
                                            $total_4 += $vps->price_override;
                                            $quantity_4++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_4[] = $text;
                                            $next_due_date_4 = $vps->next_due_date;
                                          } else {
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_4 += $product->pricing[$vps->billing_cycle];

                                              if (!empty($vps->vps_config)) {
                                                  $addon_vps = $vps->vps_config;
                                                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                  $pricing_addon = 0;
                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                            $cpu += $addon_vps->cpu;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                            $ram += $addon_vps->ram;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                            $disk += $addon_vps->disk;
                                                          }
                                                      }
                                                  }
                                                  $total_4 += $pricing_addon;
                                              }

                                              $quantity_4++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_4[] = $text;
                                              $next_due_date_4 = $vps->next_due_date;

                                            }
                                          }
                                      }

                                    }
                                    else {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                          $cpu = $product->meta_product->cpu;
                                          $ram = $product->meta_product->memory;
                                          $disk = $product->meta_product->disk;
                                          // dd($product);
                                          if (!empty($vps->price_override)) {
                                            $total_4_us += $vps->price_override;
                                            $quantity_4_us++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_4_us[] = $text;
                                            $next_due_date_4_us = $vps->next_due_date;
                                          } else {
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_4_us += $product->pricing[$vps->billing_cycle];
                                              $quantity_4_us++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_4_us[] = $text;
                                              $next_due_date_4_us = $vps->next_due_date;
                                            }
                                          }
                                      }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    if ( $vps->location == 'cloudzone') {
                                        $product = $this->product->find($vps->product_id);
                                        if ( !empty($product->meta_product) ) {
                                            $cpu = $product->meta_product->cpu;
                                            $ram = $product->meta_product->memory;
                                            $disk = $product->meta_product->disk;
                                            if (!empty($vps->price_override)) {
                                                $total_3 += $vps->price_override;
                                                $quantity_3++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_3[] = $text;
                                                $next_due_date_3 = $vps->next_due_date;
                                            }
                                            else {
                                            // dd($product);
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                                $total_3 += $product->pricing[$vps->billing_cycle];

                                                if (!empty($vps->vps_config)) {
                                                    $addon_vps = $vps->vps_config;
                                                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                    $pricing_addon = 0;
                                                    foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                $cpu += $addon_vps->cpu;
                                                            }
                                                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                $ram += $addon_vps->ram;
                                                            }
                                                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                $disk += $addon_vps->disk;
                                                            }
                                                        }
                                                    }
                                                    $total_3 += $pricing_addon;
                                                }

                                                $quantity_3++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_3[] = $text;
                                                $next_due_date_3 = $vps->next_due_date;
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        $product = $this->product->find($vps->product_id);
                                        if ( !empty($product->meta_product) ) {
                                            $cpu = $product->meta_product->cpu;
                                            $ram = $product->meta_product->memory;
                                            $disk = $product->meta_product->disk;
                                            if (!empty($vps->price_override)) {
                                                $total_3_us += $vps->price_override;
                                                $quantity_3_us++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_3_us[] = $text;
                                                $next_due_date_3_us = $vps->next_due_date;
                                            }
                                            else {
                                            // dd($product);
                                            if (!empty($product->pricing[$vps->billing_cycle])) {
                                                $total_3_us += $product->pricing[$vps->billing_cycle];
                                                $quantity_3_us++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_3_us[] = $text;
                                                $next_due_date_3_us = $vps->next_due_date;
                                                }
                                            }
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    if ( $vps->location == 'cloudzone') {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                        $cpu = $product->meta_product->cpu;
                                        $ram = $product->meta_product->memory;
                                        $disk = $product->meta_product->disk;
                                        if (!empty($vps->price_override)) {
                                            $total_2 += $vps->price_override;
                                            $quantity_2++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_2[] = $text;
                                            $next_due_date_2 = $vps->next_due_date;
                                        }
                                        else {
                                          // dd($product);
                                          if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_2 += $product->pricing[$vps->billing_cycle];

                                                if (!empty($vps->vps_config)) {
                                                  $addon_vps = $vps->vps_config;
                                                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                  $pricing_addon = 0;
                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                            $cpu += $addon_vps->cpu;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                            $ram += $addon_vps->ram;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                            $disk += $addon_vps->disk;
                                                          }
                                                      }
                                                  }
                                                  $total_2 += $pricing_addon;
                                                }

                                                $quantity_2++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_2[] = $text;
                                                $next_due_date_2 = $vps->next_due_date;
                                            }
                                        }
                                    }
                                    }
                                    else {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                        $cpu = $product->meta_product->cpu;
                                        $ram = $product->meta_product->memory;
                                        $disk = $product->meta_product->disk;
                                        if (!empty($vps->price_override)) {
                                            $total_2_us += $vps->price_override;
                                            $quantity_2_us++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_2_us[] = $text;
                                            $next_due_date_2_us = $vps->next_due_date;
                                        }
                                        else {
                                          // dd($product);
                                          if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_2_us += $product->pricing[$vps->billing_cycle];
                                                $quantity_2_us++;
                                                $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                                $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                                $list_ip_2_us[] = $text;
                                                $next_due_date_2_us = $vps->next_due_date;
                                          }
                                        }
                                      }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    if ( $vps->location == 'cloudzone') {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                        $cpu = $product->meta_product->cpu;
                                        $ram = $product->meta_product->memory;
                                        $disk = $product->meta_product->disk;
                                        if (!empty($vps->price_override)) {
                                            $total_1 += $vps->price_override;
                                            $quantity_1++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_1[] = $text;
                                            $next_due_date_1 = $vps->next_due_date;
                                        }
                                        else {
                                          // dd($product);
                                          if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_1 += $product->pricing[$vps->billing_cycle];

                                              if (!empty($vps->vps_config)) {
                                                  $addon_vps = $vps->vps_config;
                                                  $add_on_products = $this->get_addon_product_private($vps->user_id);
                                                  $pricing_addon = 0;
                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                            $cpu += $addon_vps->cpu;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                            $ram += $addon_vps->ram;
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                            $disk += $addon_vps->disk;
                                                          }
                                                      }
                                                  }
                                                  $total_1 += $pricing_addon;
                                              }

                                              $quantity_1++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_1[] =  $text;
                                              $next_due_date_1 = $vps->next_due_date;
                                            }
                                        }
                                      }
                                    }
                                    else {
                                      $product = $this->product->find($vps->product_id);
                                      if ( !empty($product->meta_product) ) {
                                        $cpu = $product->meta_product->cpu;
                                        $ram = $product->meta_product->memory;
                                        $disk = $product->meta_product->disk;
                                        if (!empty($vps->price_override)) {
                                            $total_1_us += $vps->price_override;
                                            $quantity_1_us++;
                                            $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                            $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                            $list_ip_1_us[] = $text;
                                            $next_due_date_1_us = $vps->next_due_date;
                                        }
                                        else {
                                          // dd($product);
                                          if (!empty($product->pricing[$vps->billing_cycle])) {
                                              $total_1_us += $product->pricing[$vps->billing_cycle];
                                              $quantity_1_us++;
                                              $text = $vps->ip . " (Cấu hình: $cpu CPU $ram RAM $disk DISK) - ";
                                              $text .= !empty($billing[$vps->billing_cycle]) ? $billing[$vps->billing_cycle] : '1 Tháng';
                                              $list_ip_1_us[] =  $text;
                                              $next_due_date_1_us = $vps->next_due_date;
                                            }
                                        }
                                      }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception $e) {
                        report($e);
                    }
                }

                $this->subject = 'Thông báo VPS sắp hết hạn';
                $this->user_email = $user->email;
                $data['name'] = $user->name;
                $data_us['name'] = $user->name;
                /* THÔNG BÁO VPS VN */
                $qtt_nearly = 0;
                $qtt_expire = 0;
                if ( $quantity_7 || $quantity_4 || $quantity_3 || $quantity_2 || $quantity_1
                    || $quantity_0 || $quantity_minus_1 || $quantity_minus_2 || $quantity_minus_3 )
                {
                    $check = true;
                    $title = '';
                    $qtt_nearly = $quantity_7 + $quantity_4 + $quantity_3 + $quantity_2 + $quantity_1 + $quantity_0;
                    $qtt_expire = $quantity_minus_1 + $quantity_minus_2 + $quantity_minus_3;
                    if ( $qtt_nearly && $qtt_expire ) {
                        $title = 'VPS VN - Quý khách có ' . $qtt_nearly . ' VPS gần hết hạn và có ' . $qtt_expire . ' VPS đã hết hạn';
                    }
                    elseif ($qtt_nearly) {
                        $title = 'VPS VN - Quý khách có ' . $qtt_nearly . ' VPS gần hết hạn';
                    }
                    elseif ($qtt_expire) {
                        $title = 'VPS VN - Quý khách có ' . $qtt_expire . ' VPS đã hết hạn';
                    }
                    // Tạo thông báo cho user
                    $content = '';
                    $content .= '<p>Cloudzone kính gửi thông tin các VPS sắp hết hạn của quý khách:  </p>';
                    // - Danh sách VPS sắp hết hạn: <br>
                    if ( $qtt_nearly ) {
                        $content .= '<p style="font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_nearly . ' VPS sắp hết hạn</p>';
                    }
                    if ($quantity_7 > 0) {
                        // dd('da den');
                        $data['expire_5'] = [
                            'next_due_date' => $next_due_date_7,
                            'quantity' => $quantity_7,
                            'amount' => $total_7,
                            'list_ip' => $list_ip_7,
                        ];
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 5 ngày: <br>';
                        foreach ($list_ip_7 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_7 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_7)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_7,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';

                    }
                    if ($quantity_4 > 0) {
                        // dd('da den');
                        $data['expire_4'] = [
                            'next_due_date' => $next_due_date_4,
                            'quantity' => $quantity_4,
                            'amount' => $total_4,
                            'list_ip' => $list_ip_4,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 4 ngày: <br>';
                        foreach ($list_ip_4 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_4 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_4)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_4,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_3 > 0) {
                        // dd('da den');
                        $data['expire_3'] = [
                            'next_due_date' => $next_due_date_3,
                            'quantity' => $quantity_3,
                            'amount' => $total_3,
                            'list_ip' => $list_ip_3,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 3 ngày: <br>';
                        foreach ($list_ip_3 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_3 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_3)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_3,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_2 > 0) {
                        // dd('da den');
                        $data['expire_2'] = [
                            'next_due_date' => $next_due_date_2,
                            'quantity' => $quantity_2,
                            'amount' => $total_2,
                            'list_ip' => $list_ip_2,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 2 ngày: <br>';
                        foreach ($list_ip_2 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_2 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_2)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_2,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_1 > 0) {
                        // dd('da den');
                        $data['expire_1'] = [
                            'next_due_date' => $next_due_date_1,
                            'quantity' => $quantity_1,
                            'amount' => $total_1,
                            'list_ip' => $list_ip_1,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 1 ngày: <br>';
                        foreach ($list_ip_1 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_1 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_1)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_1,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_0 > 0) {
                        // dd('da den');
                        $data['expire_0'] = [
                            'next_due_date' => $next_due_date_0,
                            'quantity' => $quantity_0,
                            'amount' => $total_0,
                            'list_ip' => $list_ip_0,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS còn hạn 0 ngày: <br>';
                        foreach ($list_ip_0 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_0 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_0,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ( $qtt_expire ) {
                        $content .= '<p  style="margin-top: 10px;font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_expire . ' VPS đã hết hạn</p>';
                    }
                    if ($quantity_minus_1 > 0) {
                        // dd('da den');
                        $data['expire_minus_1'] = [
                            'next_due_date' => $next_due_date_minus_1,
                            'quantity' => $quantity_minus_1,
                            'amount' => $total_minus_1,
                            'list_ip' => $list_minus_1,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS hết hạn 1 ngày: <br>';
                        foreach ($list_minus_1 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_1 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_1)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_1,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_minus_2 > 0) {
                        // dd('da den');
                        $data['expire_minus_2'] = [
                            'next_due_date' => $next_due_date_minus_2,
                            'quantity' => $quantity_minus_2,
                            'amount' => $total_minus_2,
                            'list_ip' => $list_minus_2,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS hết hạn 2 ngày: <br>';
                        foreach ($list_minus_2 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_2 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_2)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_2,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    if ($quantity_minus_3 > 0) {
                        // dd('da den');
                        $data['expire_minus_3'] = [
                            'next_due_date' => $next_due_date_minus_3,
                            'quantity' => $quantity_minus_3,
                            'amount' => $total_minus_3,
                            'list_ip' => $list_minus_3,
                        ];
                        // Tạo thông báo cho user
                        $content .= '<p>';
                        $content .= '- Danh sách VPS hết hạn 3 ngày: <br>';
                        foreach ($list_minus_3 as $key => $ip) {
                            $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                        }
                        $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_3 . '<br />';
                        $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_3)) . '<br />';
                        $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_3,0,",",".") . ' VNĐ<br />';
                        $content .= '</p>';
                    }
                    $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b> <br>';
                    $content .= 'Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó,
                                và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
                                Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                    $data_notification = [
                        'name' => !empty($title) ? $title : 'Thông báo VPS VN sắp hết hạn',
                        'content' => $content,
                        'status' => 'Active',
                    ];
                    $infoNotification = $this->notification->create($data_notification);
                    $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                }
                if ( $check ) {
                    $this->subject = 'Thông báo VPS hết hạn';
                    $data['subject'] = 'Thông báo VPS VN hết hạn';
                    try {
                        $data_tb_send_mail = [
                            'type' => 'mail_cron_nearly',
                            'type_service' => 'vps',
                            'user_id' => $user->id,
                            'status' => false,
                            'content' => serialize($data),
                        ];
                        $this->send_mail->create($data_tb_send_mail);
                    } catch (Exception $e) {
                        report($e);
                    }
                }
                /* THÔNG BÁO VPS US */
                if ( $quantity_7_us || $quantity_4_us || $quantity_3_us || $quantity_2_us || $quantity_1_us
                    || $quantity_0_us || $quantity_minus_1_us || $quantity_minus_2_us || $quantity_minus_3_us )
                {
                  $check_us = true;
                  $title_us = '';
                  $qtt_nearly_us = $quantity_7_us + $quantity_4_us + $quantity_3_us + $quantity_2_us + $quantity_1_us + $quantity_0_us;
                  $qtt_expire_us = $quantity_minus_1_us + $quantity_minus_2_us + $quantity_minus_3_us;
                  if ( $qtt_nearly_us && $qtt_expire_us ) {
                    $title_us = 'VPS US - Quý khách có ' . $qtt_nearly_us . ' VPS gần hết hạn và có ' . $qtt_expire_us . ' VPS đã hết hạn';
                  }
                  elseif ($qtt_nearly_us) {
                    $title = 'VPS US - Quý khách có ' . $qtt_nearly_us . ' VPS gần hết hạn';
                  }
                  elseif ($qtt_expire_us) {
                    $title = 'VPS US - Quý khách có ' . $qtt_expire_us . ' VPS đã hết hạn';
                  }
                  // Tạo thông báo cho user
                  $content_us = '';
                  $content_us .= '<p>Cloudzone kính gửi thông tin các VPS US sắp hết hạn của quý khách:  </p>';
                  // - Danh sách VPS sắp hết hạn: <br>
                  if ( $qtt_nearly_us ) {
                    $content_us .= '<p style="font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_nearly_us . ' VPS sắp hết hạn</p>';
                  }
                  if ($quantity_7_us > 0) {
                      // dd('da den');
                       $data_us['expire_5'] = [
                           'next_due_date' => $next_due_date_7_us,
                           'quantity' => $quantity_7_us,
                           'amount' => $total_7_us,
                           'list_ip' => $list_ip_7_us,
                       ];
                      $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS còn hạn 5 ngày: <br>';
                      foreach ($list_ip_7_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_7_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_7_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_7_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';

                  }
                  if ($quantity_4_us > 0) {
                      // dd('da den');
                       $data_us['expire_4'] = [
                           'next_due_date' => $next_due_date_4_us,
                           'quantity' => $quantity_4_us,
                           'amount' => $total_4_us,
                           'list_ip' => $list_ip_4_us,
                      ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS còn hạn 4 ngày: <br>';
                      foreach ($list_ip_4_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_4_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_4_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_4_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_3_us > 0) {
                      // dd('da den');
                       $data_us['expire_3'] = [
                           'next_due_date' => $next_due_date_3,
                           'quantity' => $quantity_3,
                           'amount' => $total_3,
                           'list_ip' => $list_ip_3,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS còn hạn 3 ngày: <br>';
                      foreach ($list_ip_3_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_3_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_3_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_3_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_2_us > 0) {
                      // dd('da den');
                       $data_us['expire_2'] = [
                           'next_due_date' => $next_due_date_2_us,
                           'quantity' => $quantity_2_us,
                           'amount' => $total_2_us,
                           'list_ip' => $list_ip_2_us,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS còn hạn 2 ngày: <br>';
                      foreach ($list_ip_2_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_2_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_2_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_2_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_1_us > 0) {
                      // dd('da den');
                       $data_us['expire_1'] = [
                           'next_due_date' => $next_due_date_1_us,
                           'quantity' => $quantity_1_us,
                           'amount' => $total_1_us,
                           'list_ip' => $list_ip_1_us,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS còn hạn 1 ngày: <br>';
                      foreach ($list_ip_1_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_1_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_1_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_1_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_0_us > 0) {
                      // dd('da den');
                       $data_us['expire_0'] = [
                           'next_due_date' => $next_due_date_0_us,
                           'quantity' => $quantity_0_us,
                           'amount' => $total_0_us,
                           'list_ip' => $list_ip_0_us,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '&nbsp;&nbsp;- Danh sách VPS còn hạn 0 ngày: <br>';
                      foreach ($list_ip_0_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_0_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_0_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ( $qtt_expire_us ) {
                    $content_us .= '<p style="margin-top: 10px;font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_expire_us . ' VPS đã hết hạn</p>';
                  }
                  if ($quantity_minus_1_us > 0) {
                      // dd('da den');
                       $data_us['expire_minus_1'] = [
                           'next_due_date' => $next_due_date_minus_1_us,
                           'quantity' => $quantity_minus_1_us,
                           'amount' => $total_minus_1_us,
                           'list_ip' => $list_minus_1_us,
                       ];
                      $content_us .= '<p>';
                      // Tạo thông báo cho user
                      $content_us .= '- Danh sách VPS hết hạn 1 ngày: <br>';
                      foreach ($list_minus_1_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_1_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_1_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_1_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_minus_2_us > 0) {
                      // dd('da den');
                       $data_us['expire_minus_2'] = [
                           'next_due_date' => $next_due_date_minus_2_us,
                           'quantity' => $quantity_minus_2_us,
                           'amount' => $total_minus_2_us,
                           'list_ip' => $list_minus_2_us,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS hết hạn 2 ngày: <br>';
                      foreach ($list_minus_2_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_2_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_2_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_2_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  if ($quantity_minus_3_us > 0) {
                      // dd('da den');
                       $data_us['expire_minus_3'] = [
                           'next_due_date' => $next_due_date_minus_3_us,
                           'quantity' => $quantity_minus_3_us,
                           'amount' => $total_minus_3_us,
                           'list_ip' => $list_minus_3_us,
                       ];
                      // Tạo thông báo cho user
                     $content_us .= '<p>';
                      $content_us .= '- Danh sách VPS hết hạn 3 ngày: <br>';
                      foreach ($list_minus_3_us as $key => $ip) {
                          $content_us .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                      }
                      $content_us .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_3_us . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_3_us)) . '<br />';
                      $content_us .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_3_us,0,",",".") . ' VNĐ<br />';
                     $content_us .= '</p>';
                  }
                  $content_us .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b> <br>';
                  $content_us .= 'Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó,
                              và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
                              Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                  $data_notification_us = [
                      'name' => !empty($title_us) ? $title_us : 'Thông báo VPS US sắp hết hạn',
                      'content' => $content_us,
                      'status' => 'Active',
                  ];
                  $infoNotification_us = $this->notification->create($data_notification_us);
                  $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification_us->id ]);
                }
                if ( $check_us ) {
                    $this->subject = 'Thông báo VPS US hết hạn';
                    $data['subject'] = 'Thông báo VPS US hết hạn';
                    try {
                        $data_tb_send_mail = [
                            'type' => 'mail_cron_nearly',
                            'type_service' => 'vps_us',
                            'user_id' => $user->id,
                            'status' => false,
                            'content' => serialize($data_us),
                        ];
                        $this->send_mail->create($data_tb_send_mail);
                    } catch (Exception $e) {
                        report($e);
                    }
                }
            } catch (Exception $e) {
                report($e);
            }

        }
    }

    public function expiredProxy()
    {
        $billing = config('billing');
        $users = $this->user->get();
        foreach ($users as $key1 => $user) {
            try {
                $check = false;
                $list_proxies = $user->proxies->whereNotIn('status', ['delete', 'change_user', 'cancel', 'Pending']);
                if ($list_proxies->count() > 0) {
                    // dd($list_proxies);
                    $data = [];
                    // Số lượng
                    $quantity_7 = 0;
                    $quantity_4 = 0;
                    $quantity_3 = 0;
                    $quantity_2 = 0;
                    $quantity_1 = 0;
                    $quantity_0 = 0;
                    $quantity_minus_1 = 0;
                    $quantity_minus_2 = 0;
                    $quantity_minus_3 = 0;
                    // tổng tiền
                    $total_7 = 0;
                    $total_4 = 0;
                    $total_3 = 0;
                    $total_2 = 0;
                    $total_1 = 0;
                    $total_0 = 0;
                    $total_minus_1 = 0;
                    $total_minus_2 = 0;
                    $total_minus_3 = 0;
                    //list ip
                    $list_ip_7 = [];
                    $list_ip_4 = [];
                    $list_ip_3 = [];
                    $list_ip_2 = [];
                    $list_ip_1 = [];
                    $list_ip_0 = [];
                    $list_minus_1 = [];
                    $list_minus_2 = [];
                    $list_minus_3 = [];
                    // ngày hết hạn
                    $next_due_date_7 = '';
                    $next_due_date_4 = '';
                    $next_due_date_3 = '';
                    $next_due_date_2 = '';
                    $next_due_date_1 = '';
                    $next_due_date_0 = '';
                    $next_due_date_minus_1 = '';
                    $next_due_date_minus_2 = '';
                    $next_due_date_minus_3 = '';
                    // Chay list Proxy
                    foreach ($list_proxies as $key => $proxy) {
                        try {
                            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                            $now = new Carbon($now);
                            $next_due_date = new Carbon($proxy->next_due_date);
                            $diff_date = $next_due_date->diffInDays($now);
                            if ( $next_due_date->isPast() ) {
                                // 0 ngày
                                if ( $diff_date == 0 ) {
                                    $proxy->status = 'expire';
                                    $proxy->save();
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_0 += $proxy->price_override;
                                            $quantity_0++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_0[] = $text;
                                            $next_due_date_0 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_0 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_0++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_0[] = $text;
                                            $next_due_date_0 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_minus_1 += $proxy->price_override;
                                            $quantity_minus_1++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_minus_1[] = $text;
                                            $next_due_date_minus_1 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_minus_1 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_minus_1++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_minus_1[] = $text;
                                            $next_due_date_minus_1 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_minus_2 += $proxy->price_override;
                                            $quantity_minus_2++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_minus_2[] = $text;
                                            $next_due_date_minus_2 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_minus_2 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_minus_2++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_minus_2[] = $text;
                                            $next_due_date_minus_2 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_minus_3 += $proxy->price_override;
                                            $quantity_minus_3++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_minus_3[] = $text;
                                            $next_due_date_minus_3 = $proxy->next_due_date;
                                        } else {
                                            if (!empty($product->pricing[$proxy->billing_cycle])) {
                                                $total_minus_3 += $product->pricing[$proxy->billing_cycle];
                                                $quantity_minus_3++;
                                                $text = $proxy->ip . ' - ';
                                                $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                                $list_minus_3[] = $text;
                                                $next_due_date_minus_3 = $proxy->next_due_date;
                                            }
                                        }
                                    }
                                }
                            } else {
                                // 5 ngày
                                if ( $diff_date == 5 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_7 += $proxy->price_override;
                                            $quantity_7++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_7[] = $text;
                                            $next_due_date_7 = $proxy->next_due_date;
                                        } else {
                                            if (!empty($product->pricing[$proxy->billing_cycle])) {
                                                $total_7 += $product->pricing[$proxy->billing_cycle];
                                                $quantity_7++;
                                                $text = $proxy->ip . ' - ';
                                                $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                                $list_ip_7[] = $text;
                                                $next_due_date_7 = $proxy->next_due_date;
                                            }
                                        }
                                    }
                                }
                                // 4 ngày
                                elseif ( $diff_date == 4 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_4 += $proxy->price_override;
                                            $quantity_4++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_4[] = $text;
                                            $next_due_date_4 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_4 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_4++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_4[] = $text;
                                            $next_due_date_4 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_3 += $proxy->price_override;
                                            $quantity_3++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_3[] = $text;
                                            $next_due_date_3 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_3 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_3++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_3[] = $text;
                                            $next_due_date_3 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_2 += $proxy->price_override;
                                            $quantity_2++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_2[] = $text;
                                            $next_due_date_2 = $proxy->next_due_date;
                                        } else {
                                          if (!empty($product->pricing[$proxy->billing_cycle])) {
                                            $total_2 += $product->pricing[$proxy->billing_cycle];
                                            $quantity_2++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_2[] = $text;
                                            $next_due_date_2 = $proxy->next_due_date;
                                          }
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    $product = $this->product->find($proxy->product_id);
                                    if ( !empty($product->meta_product) ) {
                                        if (!empty($proxy->price_override)) {
                                            $total_1 += $proxy->price_override;
                                            $quantity_1++;
                                            $text = $proxy->ip . ' - ';
                                            $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                            $list_ip_1[] = $text;
                                            $next_due_date_1 = $proxy->next_due_date;
                                        } else {
                                            if (!empty($product->pricing[$proxy->billing_cycle])) {
                                                $total_1 += $product->pricing[$proxy->billing_cycle];
                                                $quantity_1++;
                                                $text = $proxy->ip . ' - ';
                                                $text .= !empty($billing[$proxy->billing_cycle]) ? $billing[$proxy->billing_cycle] : ' 1 Tháng';
                                                $list_ip_1[] = $text;
                                                $next_due_date_1 = $proxy->next_due_date;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (\Throwable $th) {
                            report($th);
                        }
                    }

                    $data['name'] = $user->name;
                    /* THÔNG BÁO Proxy */
                    $qtt_nearly = 0;
                    $qtt_expire = 0;
                    if ( $quantity_7 || $quantity_4 || $quantity_3 || $quantity_2 || $quantity_1
                    || $quantity_0 || $quantity_minus_1 || $quantity_minus_2 || $quantity_minus_3 ) {
                        // dd($list_ip_7);
                        $check = true;
                        $title = '';
                        $qtt_nearly = $quantity_7 + $quantity_4 + $quantity_3 + $quantity_2 + $quantity_1 + $quantity_0;
                        $qtt_expire = $quantity_minus_1 + $quantity_minus_2 + $quantity_minus_3;
                        if ( $qtt_nearly && $qtt_expire ) {
                            $title = 'Proxy - Quý khách có ' . $qtt_nearly . ' Proxy gần hết hạn và có ' . $qtt_expire . ' Proxy đã hết hạn';
                        }
                        elseif ($qtt_nearly) {
                            $title = 'Proxy - Quý khách có ' . $qtt_nearly . ' Proxy gần hết hạn';
                        }
                        elseif ($qtt_expire) {
                            $title = 'Proxy - Quý khách có ' . $qtt_expire . ' Proxy đã hết hạn';
                        }
                        // Tạo thông báo cho user
                        $content = '';
                        $content .= '<p>Cloudzone kính gửi thông tin các Proxy sắp hết hạn của quý khách:  </p>';
                        // - Danh sách VPS sắp hết hạn: <br>
                        if ( $qtt_nearly ) {
                            $content .= '<p style="font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_nearly . ' Proxy sắp hết hạn</p>';
                        }
                        if ($quantity_7 > 0) {
                            // dd('da den');
                            $data['expire_5'] = [
                                'next_due_date' => $next_due_date_7,
                                'quantity' => $quantity_7,
                                'amount' => $total_7,
                                'list_ip' => $list_ip_7,
                            ];
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 5 ngày: <br>';
                            foreach ($list_ip_7 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_7 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_7)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_7,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
    
                        }
                        if ($quantity_4 > 0) {
                            // dd('da den');
                            $data['expire_4'] = [
                                'next_due_date' => $next_due_date_4,
                                'quantity' => $quantity_4,
                                'amount' => $total_4,
                                'list_ip' => $list_ip_4,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 4 ngày: <br>';
                            foreach ($list_ip_4 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_4 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_4)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_4,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_3 > 0) {
                            // dd('da den');
                            $data['expire_3'] = [
                                'next_due_date' => $next_due_date_3,
                                'quantity' => $quantity_3,
                                'amount' => $total_3,
                                'list_ip' => $list_ip_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 3 ngày: <br>';
                            foreach ($list_ip_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_2 > 0) {
                            // dd('da den');
                            $data['expire_2'] = [
                                'next_due_date' => $next_due_date_2,
                                'quantity' => $quantity_2,
                                'amount' => $total_2,
                                'list_ip' => $list_ip_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 2 ngày: <br>';
                            foreach ($list_ip_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_1 > 0) {
                            // dd('da den');
                            $data['expire_1'] = [
                                'next_due_date' => $next_due_date_1,
                                'quantity' => $quantity_1,
                                'amount' => $total_1,
                                'list_ip' => $list_ip_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 1 ngày: <br>';
                            foreach ($list_ip_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_0 > 0) {
                            // dd('da den');
                            $data['expire_0'] = [
                                'next_due_date' => $next_due_date_0,
                                'quantity' => $quantity_0,
                                'amount' => $total_0,
                                'list_ip' => $list_ip_0,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy còn hạn 0 ngày: <br>';
                            foreach ($list_ip_0 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_0 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_0,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ( $qtt_expire ) {
                            $content .= '<p  style="margin-top: 10px;font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_expire . ' Proxy đã hết hạn</p>';
                        }
                        if ($quantity_minus_1 > 0) {
                            // dd('da den');
                            $data['expire_minus_1'] = [
                                'next_due_date' => $next_due_date_minus_1,
                                'quantity' => $quantity_minus_1,
                                'amount' => $total_minus_1,
                                'list_ip' => $list_minus_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy hết hạn 1 ngày: <br>';
                            foreach ($list_minus_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_2 > 0) {
                            // dd('da den');
                            $data['expire_minus_2'] = [
                                'next_due_date' => $next_due_date_minus_2,
                                'quantity' => $quantity_minus_2,
                                'amount' => $total_minus_2,
                                'list_ip' => $list_minus_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy hết hạn 2 ngày: <br>';
                            foreach ($list_minus_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_3 > 0) {
                            // dd('da den');
                            $data['expire_minus_3'] = [
                                'next_due_date' => $next_due_date_minus_3,
                                'quantity' => $quantity_minus_3,
                                'amount' => $total_minus_3,
                                'list_ip' => $list_minus_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Proxy hết hạn 3 ngày: <br>';
                            foreach ($list_minus_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b> <br>';
                        $content .= 'Lưu ý: Proxy sau khi hết hạn sẽ bị tắt. Dữ liệu Proxy của quý khách sẽ được lưu trữ trong vài ngày sau đó,
                                    và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
                                    Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                        $data_notification = [
                            'name' => !empty($title) ? $title : 'Thông báo Proxy sắp hết hạn',
                            'content' => $content,
                            'status' => 'Active',
                        ];
                        $infoNotification = $this->notification->create($data_notification);
                        $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                    }
                    if ( $check ) {
                        $this->subject = 'Thông báo Proxy hết hạn';
                        $data['subject'] = !empty($title) ? $title : 'Thông báo Proxy hết hạn';
                        try {
                            $data_tb_send_mail = [
                                'type' => 'mail_cron_nearly',
                                'type_service' => 'proxy',
                                'user_id' => $user->id,
                                'status' => false,
                                'content' => serialize($data),
                            ];
                            $this->send_mail->create($data_tb_send_mail);
                        } catch (Exception $e) {
                            report($e);
                        }
                    }
                }
            } catch (\Throwable $th) {
                report($th);
            }
        }
    }

    public function expiredServer()
    {
        $billing = config('billing');
        $users = $this->user->get();
        foreach ($users as $key1 => $user) {
            try {
                $check = false;
                $list_server = $user->servers->where('status', 'Active')->whereNotIn('status_server', ['delete_server', 'change_user', 'cancel']);
                if ($list_server->count() > 0) {
                    // dd($list_server);
                    $data = [];
                    // Số lượng
                    $quantity_7 = 0;
                    $quantity_4 = 0;
                    $quantity_3 = 0;
                    $quantity_2 = 0;
                    $quantity_1 = 0;
                    $quantity_0 = 0;
                    $quantity_minus_1 = 0;
                    $quantity_minus_2 = 0;
                    $quantity_minus_3 = 0;
                    // tổng tiền
                    $total_7 = 0;
                    $total_4 = 0;
                    $total_3 = 0;
                    $total_2 = 0;
                    $total_1 = 0;
                    $total_0 = 0;
                    $total_minus_1 = 0;
                    $total_minus_2 = 0;
                    $total_minus_3 = 0;
                    //list ip
                    $list_ip_7 = [];
                    $list_ip_4 = [];
                    $list_ip_3 = [];
                    $list_ip_2 = [];
                    $list_ip_1 = [];
                    $list_ip_0 = [];
                    $list_minus_1 = [];
                    $list_minus_2 = [];
                    $list_minus_3 = [];
                    // ngày hết hạn
                    $next_due_date_7 = '';
                    $next_due_date_4 = '';
                    $next_due_date_3 = '';
                    $next_due_date_2 = '';
                    $next_due_date_1 = '';
                    $next_due_date_0 = '';
                    $next_due_date_minus_1 = '';
                    $next_due_date_minus_2 = '';
                    $next_due_date_minus_3 = '';
                    // Chay list Server
                    foreach ($list_server as $key => $server) {
                        // dd($server);
                        try {
                            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                            $now = new Carbon($now);
                            $next_due_date = new Carbon($server->next_due_date);
                            $diff_date = $next_due_date->diffInDays($now);
                            if ( $next_due_date->isPast() ) {
                                // 0 ngày
                                if ( $diff_date == 0 ) {
                                    $server->status_server = 'expire';
                                    $server->save();
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_0 += $server->amount;
                                        $quantity_0++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_0[] = $text;
                                        $next_due_date_0 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_0 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_0++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_0[] = $text;
                                            $next_due_date_0 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_minus_1 += $server->amount;
                                        $quantity_minus_1++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_minus_1[] = $text;
                                        $next_due_date_minus_1 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_minus_1 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_minus_1++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_minus_1[] = $text;
                                            $next_due_date_minus_1 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_minus_2 += $server->amount;
                                        $quantity_minus_2++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_minus_2[] = $text;
                                        $next_due_date_minus_2 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_minus_2 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_minus_2++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_minus_2[] = $text;
                                            $next_due_date_minus_2 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_minus_3 += $server->amount;
                                        $quantity_minus_3++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_minus_3[] = $text;
                                        $next_due_date_minus_3 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_minus_3 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_minus_3++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_minus_3[] = $text;
                                            $next_due_date_minus_3 = $server->next_due_date;
                                        }
                                    }
                                }
                            } else {
                                // 5 ngày
                                if ( $diff_date == 5 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    // dd($product->pricing);
                                    if (!empty($server->amount)) {
                                        $total_7 += $server->amount;
                                        $quantity_7++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_7[] = $text;
                                        $next_due_date_7 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_7 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_7++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_7[] = $text;
                                            $next_due_date_7 = $server->next_due_date;
                                        }
                                    }
                                    // dd($server, $total_7, $list_ip_7);
                                }
                                // 4 ngày
                                elseif ( $diff_date == 4 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_4 += $server->amount;
                                        $quantity_4++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_4[] = $text;
                                        $next_due_date_4 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_4 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_4++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_4[] = $text;
                                            $next_due_date_4 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_3 += $server->amount;
                                        $quantity_3++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_3[] = $text;
                                        $next_due_date_3 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_3 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_3++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_3[] = $text;
                                            $next_due_date_3 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_2 += $server->amount;
                                        $quantity_2++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_2[] = $text;
                                        $next_due_date_2 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_2 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_2++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_2[] = $text;
                                            $next_due_date_2 = $server->next_due_date;
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    // Cấu hình
                                    if ( !empty($server->config_text) ) {
                                        $server->specs = $server->config_text;
                                    } else {
                                        $product = $server->product;
                                        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
                                    }
                                    
                                    $addonConfig = '';
                                    $addonConfig = $this->get_config_server($server->id);
                                    $server->specs .= " - HĐH $server->os - $addonConfig";
                                    if (!empty($server->amount)) {
                                        $total_1 += $server->amount;
                                        $quantity_1++;
                                        $text = $server->ip . ' - ';
                                        $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                        $text .= '<br>- Cấu hình: ' . $server->specs;
                                        $list_ip_1[] = $text;
                                        $next_due_date_1 = $server->next_due_date;
                                    } else {
                                        if (!empty($product->pricing[$server->billing_cycle])) {
                                            $total_1 += $this->get_total_server($server->id, $server->billing_cycle);
                                            $quantity_1++;
                                            $text = $server->ip . ' - ';
                                            $text .= !empty($billing[$server->billing_cycle]) ? $billing[$server->billing_cycle] : ' 1 Tháng';
                                            $text .= '<br>- Cấu hình: ' . $server->specs;
                                            $list_ip_1[] = $text;
                                            $next_due_date_1 = $server->next_due_date;
                                        }
                                    }
                                }
                            }
                        } catch (\Throwable $th) {
                            report($th);
                        }
                    }
                    // dd($list_ip_7);
                    $data['name'] = $user->name;
                    /* THÔNG BÁO Server */
                    $qtt_nearly = 0;
                    $qtt_expire = 0;
                    if ( $quantity_7 || $quantity_4 || $quantity_3 || $quantity_2 || $quantity_1
                    || $quantity_0 || $quantity_minus_1 || $quantity_minus_2 || $quantity_minus_3 ) {
                        // dd($list_ip_7);
                        $check = true;
                        $title = '';
                        $qtt_nearly = $quantity_7 + $quantity_4 + $quantity_3 + $quantity_2 + $quantity_1 + $quantity_0;
                        $qtt_expire = $quantity_minus_1 + $quantity_minus_2 + $quantity_minus_3;
                        if ( $qtt_nearly && $qtt_expire ) {
                            $title = 'Server - Quý khách có ' . $qtt_nearly . ' Server gần hết hạn và có ' . $qtt_expire . ' Server đã hết hạn';
                        }
                        elseif ($qtt_nearly) {
                            $title = 'Server - Quý khách có ' . $qtt_nearly . ' Server gần hết hạn';
                        }
                        elseif ($qtt_expire) {
                            $title = 'Server - Quý khách có ' . $qtt_expire . ' Server đã hết hạn';
                        }
                        // Tạo thông báo cho user
                        $content = '';
                        $content .= '<p>Cloudzone kính gửi thông tin các Server sắp hết hạn của quý khách:  </p>';
                        // - Danh sách VPS sắp hết hạn: <br>
                        if ( $qtt_nearly ) {
                            $content .= '<p style="font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_nearly . ' Server sắp hết hạn</p>';
                        }
                        if ($quantity_7 > 0) {
                            // dd('da den');
                            $data['expire_5'] = [
                                'next_due_date' => $next_due_date_7,
                                'quantity' => $quantity_7,
                                'amount' => $total_7,
                                'list_ip' => $list_ip_7,
                            ];
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 5 ngày: <br>';
                            foreach ($list_ip_7 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_7 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_7)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_7,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
    
                        }
                        if ($quantity_4 > 0) {
                            // dd('da den');
                            $data['expire_4'] = [
                                'next_due_date' => $next_due_date_4,
                                'quantity' => $quantity_4,
                                'amount' => $total_4,
                                'list_ip' => $list_ip_4,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 4 ngày: <br>';
                            foreach ($list_ip_4 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_4 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_4)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_4,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_3 > 0) {
                            // dd('da den');
                            $data['expire_3'] = [
                                'next_due_date' => $next_due_date_3,
                                'quantity' => $quantity_3,
                                'amount' => $total_3,
                                'list_ip' => $list_ip_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 3 ngày: <br>';
                            foreach ($list_ip_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_2 > 0) {
                            // dd('da den');
                            $data['expire_2'] = [
                                'next_due_date' => $next_due_date_2,
                                'quantity' => $quantity_2,
                                'amount' => $total_2,
                                'list_ip' => $list_ip_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 2 ngày: <br>';
                            foreach ($list_ip_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_1 > 0) {
                            // dd('da den');
                            $data['expire_1'] = [
                                'next_due_date' => $next_due_date_1,
                                'quantity' => $quantity_1,
                                'amount' => $total_1,
                                'list_ip' => $list_ip_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 1 ngày: <br>';
                            foreach ($list_ip_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_0 > 0) {
                            // dd('da den');
                            $data['expire_0'] = [
                                'next_due_date' => $next_due_date_0,
                                'quantity' => $quantity_0,
                                'amount' => $total_0,
                                'list_ip' => $list_ip_0,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server còn hạn 0 ngày: <br>';
                            foreach ($list_ip_0 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_0 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_0,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ( $qtt_expire ) {
                            $content .= '<p  style="margin-top: 10px;font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_expire . ' Server đã hết hạn</p>';
                        }
                        if ($quantity_minus_1 > 0) {
                            // dd('da den');
                            $data['expire_minus_1'] = [
                                'next_due_date' => $next_due_date_minus_1,
                                'quantity' => $quantity_minus_1,
                                'amount' => $total_minus_1,
                                'list_ip' => $list_minus_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server hết hạn 1 ngày: <br>';
                            foreach ($list_minus_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_2 > 0) {
                            // dd('da den');
                            $data['expire_minus_2'] = [
                                'next_due_date' => $next_due_date_minus_2,
                                'quantity' => $quantity_minus_2,
                                'amount' => $total_minus_2,
                                'list_ip' => $list_minus_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server hết hạn 2 ngày: <br>';
                            foreach ($list_minus_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_3 > 0) {
                            // dd('da den');
                            $data['expire_minus_3'] = [
                                'next_due_date' => $next_due_date_minus_3,
                                'quantity' => $quantity_minus_3,
                                'amount' => $total_minus_3,
                                'list_ip' => $list_minus_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Server hết hạn 3 ngày: <br>';
                            foreach ($list_minus_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b> <br>';
                        $content .= 'Lưu ý: Server sau khi hết hạn sẽ bị tắt. Dữ liệu Server của quý khách sẽ được lưu trữ trong vài ngày sau đó,
                                    và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
                                    Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                        $data_notification = [
                            'name' => !empty($title) ? $title : 'Thông báo Server sắp hết hạn',
                            'content' => $content,
                            'status' => 'Active',
                        ];
                        $infoNotification = $this->notification->create($data_notification);
                        $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                    }
                    if ( $check ) {
                        $this->subject = 'Thông báo Server hết hạn';
                        $data['subject'] = !empty($title) ? $title : 'Thông báo Server hết hạn';
                        try {
                            $data_tb_send_mail = [
                                'type' => 'mail_cron_nearly',
                                'type_service' => 'server',
                                'user_id' => $user->id,
                                'status' => false,
                                'content' => serialize($data),
                            ];
                            $this->send_mail->create($data_tb_send_mail);
                        } catch (Exception $e) {
                            report($e);
                        }
                    }
                }
            } catch (\Throwable $th) {
                report($th);
            }
        }
    }
    
    public function get_config_server($serverId)
    {
        $server = $this->server->find($serverId);
        if ( !empty($server->server_config) ) {
            $text_config = '';
            $server_config = $server->server_config;
            $data_config = [];
            if ( !empty($server_config->disk2) ) {
                $data_config[] = [
                    'id' => $server_config->disk2,
                    'name' => $server_config->product_disk2->name,
                    'qtt' => 1
                ];
            }
            if ( !empty($server_config->disk3) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk3) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                        $data_config[] = [
                            'id' => $server_config->disk3,
                            'name' => $server_config->product_disk3->name,
                            'qtt' => 1
                        ];
                }
            }
            if ( !empty($server_config->disk4) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk4) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                    $data_config[] = [
                        'id' => $server_config->disk4,
                        'name' => $server_config->product_disk4->name,
                        'qtt' => 1
                    ];
                }
            }
            if ( !empty($server_config->disk5) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk5) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                    $data_config[] = [
                        'id' => $server_config->disk5,
                        'name' => $server_config->product_disk5->name,
                        'qtt' => 1
                    ];
                }
            }
            if ( !empty($server_config->disk6) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk6) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                    $data_config[] = [
                        'id' => $server_config->disk6,
                        'name' => $server_config->product_disk6->name,
                        'qtt' => 1
                    ];
                }
            }
            if ( !empty($server_config->disk7) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk7) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                    $data_config[] = [
                        'id' => $server_config->disk7,
                        'name' => $server_config->product_disk7->name,
                        'qtt' => 1
                    ];
                }
            }
            if ( !empty($server_config->disk8) ) {
                $check = true;
                foreach ($data_config as $key => $data) {
                    if ($data['id'] == $server_config->disk8) {
                        $data_config[$key]['qtt'] = $data['qtt'] + 1;
                        $check = false;
                    }
                }
                if ( $check ) {
                    $data_config[] = [
                        'id' => $server_config->disk8,
                        'name' => $server_config->product_disk8->name,
                        'qtt' => 1
                    ];
                }
            }
            if ( !empty($server_config->ram) ) {
                $add_ram = 0;
                foreach ($server->server_config_rams as $server_config_ram) {
                    $add_ram += !empty($server_config_ram->product->meta_product->memory) ? $server_config_ram->product->meta_product->memory : 0;
                }
                $text_config .= 'Addon RAM: '. $add_ram .' GB RAM <br>';
            }
            if ( count($data_config) ) {
                $text_config .= 'Addon Disk (';
                foreach ($data_config as $key => $data) {
                    $text_config .= ( $data['qtt'] . ' &#215; ' . $data['name']  );
                    if ( count( $data_config ) - 1 > $key ) {
                        $text_config .= ' - ';
                    }
                }
                $text_config .= ')';
            }
            if ( !empty($server_config->ip) ) {
                $text_config .= '<br>';
                $add_ram = 0;
                foreach ($server->server_config_ips as $server_config_ip) {
                    $add_ram += !empty($server_config_ip->product->meta_product->ip) ? $server_config_ip->product->meta_product->ip : 0;
                }
                $text_config .= 'Addon IP: '. $add_ram .' IP Public';
            }
            return $text_config;
        } else {
            return '';
        }
    }

    public function get_total_server( $id, $billing_cycle )
    {
      $server = $this->server->find($id);
      $total = 0;
      $product = $server->product;
      $total = $product->pricing[ $billing_cycle ];
      if ( !empty($server->server_config) ) {
        $server_config = $server->server_config;
        $pricing_addon = 0;
        if ( !empty($server_config->ram) ) {
          foreach ($server->server_config_rams as $server_config_ram) {
            $pricing_addon += $server_config_ram->product->pricing[$billing_cycle];
          }
        }
        if ( !empty( $server_config->ip ) ) {
            foreach ($server->server_config_ips as $server_config_ip) {
                $pricing_addon += !empty($server_config_ip->product->pricing[$billing_cycle]) ? $server_config_ip->product->pricing[$billing_cycle] : 0;
            }
        }
        if ( !empty($server_config->disk2) ) {
          $pricing_addon += $server_config->product_disk2->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk3) ) {
          $pricing_addon += $server_config->product_disk3->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk4) ) {
          $pricing_addon += $server_config->product_disk4->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk5) ) {
          $pricing_addon += $server_config->product_disk5->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk6) ) {
          $pricing_addon += $server_config->product_disk6->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk7) ) {
          $pricing_addon += $server_config->product_disk7->pricing[$billing_cycle];
        }
        if ( !empty($server_config->disk8) ) {
          $pricing_addon += $server_config->product_disk8->pricing[$billing_cycle];
        }
        $total += $pricing_addon;
      }
      return $total;
    }

    public function expiredColocation()
    {
        $billing = config('billing');
        $users = $this->user->get();
        foreach ($users as $key1 => $user) {
            try {
                $check = false;
                $list_colocation = $user->colocations->where('status', 'Active')->whereNotIn('status_colo', ['delete_colo', 'change_user', 'cancel']);
                if ($list_colocation->count() > 0) {
                    // dd($list_colocation);
                    $data = [];
                    // Số lượng
                    $quantity_7 = 0;
                    $quantity_4 = 0;
                    $quantity_3 = 0;
                    $quantity_2 = 0;
                    $quantity_1 = 0;
                    $quantity_0 = 0;
                    $quantity_minus_1 = 0;
                    $quantity_minus_2 = 0;
                    $quantity_minus_3 = 0;
                    // tổng tiền
                    $total_7 = 0;
                    $total_4 = 0;
                    $total_3 = 0;
                    $total_2 = 0;
                    $total_1 = 0;
                    $total_0 = 0;
                    $total_minus_1 = 0;
                    $total_minus_2 = 0;
                    $total_minus_3 = 0;
                    //list ip
                    $list_ip_7 = [];
                    $list_ip_4 = [];
                    $list_ip_3 = [];
                    $list_ip_2 = [];
                    $list_ip_1 = [];
                    $list_ip_0 = [];
                    $list_minus_1 = [];
                    $list_minus_2 = [];
                    $list_minus_3 = [];
                    // ngày hết hạn
                    $next_due_date_7 = '';
                    $next_due_date_4 = '';
                    $next_due_date_3 = '';
                    $next_due_date_2 = '';
                    $next_due_date_1 = '';
                    $next_due_date_0 = '';
                    $next_due_date_minus_1 = '';
                    $next_due_date_minus_2 = '';
                    $next_due_date_minus_3 = '';
                    // Chay list colocation
                    foreach ($list_colocation as $key => $colocation) {
                        // dd($colocation);
                        try {
                            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                            $now = new Carbon($now);
                            $next_due_date = new Carbon($colocation->next_due_date);
                            $diff_date = $next_due_date->diffInDays($now);
                            if ( $next_due_date->isPast() ) {
                                // 0 ngày
                                if ( $diff_date == 0 ) {
                                    $colocation->status_colo = 'expire';
                                    $colocation->save();
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_0 += $colocation->amount;
                                        $quantity_0++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_0[] = $text;
                                        $next_due_date_0 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_0 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_0++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_0[] = $text;
                                            $next_due_date_0 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_minus_1 += $colocation->amount;
                                        $quantity_minus_1++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_minus_1[] = $text;
                                        $next_due_date_minus_1 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_minus_1 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_minus_1++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$secolocationrver->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_minus_1[] = $text;
                                            $next_due_date_minus_1 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_minus_2 += $colocation->amount;
                                        $quantity_minus_2++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_minus_2[] = $text;
                                        $next_due_date_minus_2 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_minus_2 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_minus_2++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_minus_2[] = $text;
                                            $next_due_date_minus_2 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_minus_3 += $colocation->amount;
                                        $quantity_minus_3++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_minus_3[] = $text;
                                        $next_due_date_minus_3 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_minus_3 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_minus_3++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_minus_3[] = $text;
                                            $next_due_date_minus_3 = $colocation->next_due_date;
                                        }
                                    }
                                }
                            } else {
                                // 5 ngày
                                if ( $diff_date == 5 ) {
                                    // Cấu hình
                                    // dd($product->pricing);
                                    if (!empty($colocation->amount)) {
                                        $total_7 += $colocation->amount;
                                        $quantity_7++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_7[] = $text;
                                        $next_due_date_7 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_7 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_7++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_7[] = $text;
                                            $next_due_date_7 = $colocation->next_due_date;
                                        }
                                    }
                                    // dd($colocation, $total_7, $list_ip_7);
                                }
                                // 4 ngày
                                elseif ( $diff_date == 4 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_4 += $colocation->amount;
                                        $quantity_4++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_4[] = $text;
                                        $next_due_date_4 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_4 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_4++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_4[] = $text;
                                            $next_due_date_4 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 3 ngày
                                elseif ( $diff_date == 3 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_3 += $colocation->amount;
                                        $quantity_3++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_3[] = $text;
                                        $next_due_date_3 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_3 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_3++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_3[] = $text;
                                            $next_due_date_3 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 2 ngày
                                elseif ( $diff_date == 2 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_2 += $colocation->amount;
                                        $quantity_2++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_2[] = $text;
                                        $next_due_date_2 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_2 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_2++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_2[] = $text;
                                            $next_due_date_2 = $colocation->next_due_date;
                                        }
                                    }
                                }
                                // 1 ngày
                                elseif ( $diff_date == 1 ) {
                                    // Cấu hình
                                    if (!empty($colocation->amount)) {
                                        $total_1 += $colocation->amount;
                                        $quantity_1++;
                                        $text = $colocation->ip . ' - ';
                                        if ( !empty($colocation->colocation_config->ip) ) {
                                            $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                        }
                                        $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                        $list_ip_1[] = $text;
                                        $next_due_date_1 = $colocation->next_due_date;
                                    } else {
                                        $product = $colocation->product;
                                        if (!empty($product->pricing[$colocation->billing_cycle])) {
                                            $total_1 += $this->get_total_colocation($colocation->id, $colocation->billing_cycle);
                                            $quantity_1++;
                                            $text = $colocation->ip . ' - ';
                                            if ( !empty($colocation->colocation_config->ip) ) {
                                                $text .= $colocation->colocation_config->ip . ' IP Addon - ';
                                            }
                                            $text .= !empty($billing[$colocation->billing_cycle]) ? $billing[$colocation->billing_cycle] : ' 1 Tháng';
                                            $list_ip_1[] = $text;
                                            $next_due_date_1 = $colocation->next_due_date;
                                        }
                                    }
                                }
                            }
                        } catch (\Throwable $th) {
                            report($th);
                        }
                    }
                    // dd($list_ip_7);
                    $data['name'] = $user->name;
                    /* THÔNG BÁO Colocation */
                    $qtt_nearly = 0;
                    $qtt_expire = 0;
                    if ( $quantity_7 || $quantity_4 || $quantity_3 || $quantity_2 || $quantity_1
                    || $quantity_0 || $quantity_minus_1 || $quantity_minus_2 || $quantity_minus_3 ) {
                        // dd($list_ip_7);
                        $check = true;
                        $title = '';
                        $qtt_nearly = $quantity_7 + $quantity_4 + $quantity_3 + $quantity_2 + $quantity_1 + $quantity_0;
                        $qtt_expire = $quantity_minus_1 + $quantity_minus_2 + $quantity_minus_3;
                        // dd($qtt_nearly, $qtt_expire);
                        if ( $qtt_nearly && $qtt_expire ) {
                            $title = 'Colocation - Quý khách có ' . $qtt_nearly . ' Colocation gần hết hạn và có ' . $qtt_expire . ' Colocation đã hết hạn';
                        }
                        elseif ($qtt_nearly) {
                            $title = 'Colocation - Quý khách có ' . $qtt_nearly . ' Colocation gần hết hạn';
                        }
                        elseif ($qtt_expire) {
                            $title = 'Colocation - Quý khách có ' . $qtt_expire . ' Colocation đã hết hạn';
                        }
                        // Tạo thông báo cho user
                        $content = '';
                        $content .= '<p>Cloudzone kính gửi thông tin các Colocation sắp hết hạn của quý khách:  </p>';
                        // - Danh sách VPS sắp hết hạn: <br>
                        if ( $qtt_nearly ) {
                            $content .= '<p style="font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_nearly . ' Colocation sắp hết hạn</p>';
                        }
                        if ($quantity_7 > 0) {
                            // dd('da den');
                            $data['expire_5'] = [
                                'next_due_date' => $next_due_date_7,
                                'quantity' => $quantity_7,
                                'amount' => $total_7,
                                'list_ip' => $list_ip_7,
                            ];
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 5 ngày: <br>';
                            foreach ($list_ip_7 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_7 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_7)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_7,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
    
                        }
                        if ($quantity_4 > 0) {
                            // dd('da den');
                            $data['expire_4'] = [
                                'next_due_date' => $next_due_date_4,
                                'quantity' => $quantity_4,
                                'amount' => $total_4,
                                'list_ip' => $list_ip_4,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 4 ngày: <br>';
                            foreach ($list_ip_4 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_4 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_4)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_4,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_3 > 0) {
                            // dd('da den');
                            $data['expire_3'] = [
                                'next_due_date' => $next_due_date_3,
                                'quantity' => $quantity_3,
                                'amount' => $total_3,
                                'list_ip' => $list_ip_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 3 ngày: <br>';
                            foreach ($list_ip_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_2 > 0) {
                            // dd('da den');
                            $data['expire_2'] = [
                                'next_due_date' => $next_due_date_2,
                                'quantity' => $quantity_2,
                                'amount' => $total_2,
                                'list_ip' => $list_ip_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 2 ngày: <br>';
                            foreach ($list_ip_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_1 > 0) {
                            // dd('da den');
                            $data['expire_1'] = [
                                'next_due_date' => $next_due_date_1,
                                'quantity' => $quantity_1,
                                'amount' => $total_1,
                                'list_ip' => $list_ip_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 1 ngày: <br>';
                            foreach ($list_ip_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_0 > 0) {
                            // dd('da den');
                            $data['expire_0'] = [
                                'next_due_date' => $next_due_date_0,
                                'quantity' => $quantity_0,
                                'amount' => $total_0,
                                'list_ip' => $list_ip_0,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation còn hạn 0 ngày: <br>';
                            foreach ($list_ip_0 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_0 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_0,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ( $qtt_expire ) {
                            $content .= '<p  style="margin-top: 10px;font-size: 16px;font-weight: bold;">* Danh sách ' . $qtt_expire . ' Colocation đã hết hạn</p>';
                        }
                        if ($quantity_minus_1 > 0) {
                            // dd('da den');
                            $data['expire_minus_1'] = [
                                'next_due_date' => $next_due_date_minus_1,
                                'quantity' => $quantity_minus_1,
                                'amount' => $total_minus_1,
                                'list_ip' => $list_minus_1,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation hết hạn 1 ngày: <br>';
                            foreach ($list_minus_1 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_1 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_1)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_1,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_2 > 0) {
                            // dd('da den');
                            $data['expire_minus_2'] = [
                                'next_due_date' => $next_due_date_minus_2,
                                'quantity' => $quantity_minus_2,
                                'amount' => $total_minus_2,
                                'list_ip' => $list_minus_2,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation hết hạn 2 ngày: <br>';
                            foreach ($list_minus_2 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_2 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_2)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_2,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        if ($quantity_minus_3 > 0) {
                            // dd('da den');
                            $data['expire_minus_3'] = [
                                'next_due_date' => $next_due_date_minus_3,
                                'quantity' => $quantity_minus_3,
                                'amount' => $total_minus_3,
                                'list_ip' => $list_minus_3,
                            ];
                            // Tạo thông báo cho user
                            $content .= '<p>';
                            $content .= '- Danh sách Colocation hết hạn 3 ngày: <br>';
                            foreach ($list_minus_3 as $key => $ip) {
                                $content .= '&nbsp;&nbsp;&nbsp;&nbsp; + IP: ' . $ip . '<br />';
                            }
                            $content .= '&nbsp;&nbsp;- Số lượng: ' . $quantity_minus_3 . '<br />';
                            $content .= '&nbsp;&nbsp;- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_minus_3)) . '<br />';
                            $content .= '&nbsp;&nbsp;- Thành tiền: ' .  number_format($total_minus_3,0,",",".") . ' VNĐ<br />';
                            $content .= '</p>';
                        }
                        $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b> <br>';
                        $content .= 'Lưu ý: Colocation sau khi hết hạn sẽ bị tắt. Dữ liệu Colocation của quý khách sẽ được lưu trữ trong vài ngày sau đó,
                                    và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
                                    Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                        $data_notification = [
                            'name' => !empty($title) ? $title : 'Thông báo Colocation sắp hết hạn',
                            'content' => $content,
                            'status' => 'Active',
                        ];
                        $infoNotification = $this->notification->create($data_notification);
                        $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                    }
                    if ( $check ) {
                        $this->subject = 'Thông báo Colocation hết hạn';
                        $data['subject'] = !empty($title) ? $title : 'Thông báo Colocation hết hạn';
                        try {
                            $data_tb_send_mail = [
                                'type' => 'mail_cron_nearly',
                                'type_service' => 'colocation',
                                'user_id' => $user->id,
                                'status' => false,
                                'content' => serialize($data),
                            ];
                            $this->send_mail->create($data_tb_send_mail);
                        } catch (Exception $e) {
                            report($e);
                        }
                    }
                }
            } catch (\Throwable $th) {
                report($th);
            }
        }
    }

    public function get_total_colocation($id, $billing_cycle)
    {
        $colo = $this->colo->find($id);
        $amount = 0;
        if ( !empty($colo->amount) ) {
            $amount = $colo->amount;
        } else {
            $product = $colo->product;
            $amount = !empty( $product->pricing[$colo->billing_cycle] ) ? $product->pricing[$colo->billing_cycle] : 0;
            if ( !empty($colo->colocation_config_ips) ) {
              foreach ($colo->colocation_config_ips as $key => $colocation_config_ip) {
                $amount += !empty( $colocation_config_ip->product->pricing[$colo->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colo->billing_cycle] : 0;
              }
            }
        }
        return $amount;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    public function expiredHosting()
    {
        $data_nearly_expired_15 = []; //VPS gần hết hạn 7
        $data_nearly_expired_10 = []; // VPS gần hết hạn 3
        $data_nearly_expired_5 = []; // VPS gần hết hạn 3
        $data_nearly_expired_0 = []; //gần hết hạn 0
        // invoice
        $billing = [
            'monthly' => '1 Tháng',
            'twomonthly' => '2 Tháng',
            'quarterly' => '3 Tháng',
            'semi_annually' => '6 Tháng',
            'annually' => '1 Năm',
            'biennially' => '2 Năm',
            'triennially' => '3 Năm',
        ];
        $users = $this->user->get();
        foreach ($users as $key1 => $user) {
            try {
              $hostings = $user->hostings;
              $quantity_15 = 0;
              $quantity_10 = 0;
              $quantity_5 = 0;
              $quantity_0 = 0;
              $total_15 = 0;
              $total_10 = 0;
              $total_5 = 0;
              $total_0 = 0;
              $list_hosting_15 = [];
              $list_hosting_10 = [];
              $list_hosting_5 = [];
              $list_hosting_0 = [];
              $next_due_date_15 = '';
              $next_due_date_10 = '';
              $next_due_date_5 = '';
              $next_due_date_0 = '';
              if ($hostings->count() > 0) {
                  foreach ($hostings as $key => $hosting) {
                      try {
                        if ($hosting->status_hosting != 'expire' && $hosting->status_hosting != 'delete') {
                          if (!empty($hosting->next_due_date)) {
                              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                              $now = new Carbon($now);
                              $next_due_date = new Carbon($hosting->next_due_date);
                              if ($next_due_date->isPast()) {
                                  $diff_date = $next_due_date->diffInDays($now);
                                  if ($diff_date == 0) {
                                      $product = $this->product->find($hosting->product_id);
                                      $this->da->expire_hosting($hosting);
                                      // dd($product);
                                      if (!empty($product->pricing[$hosting->billing_cycle])) {
                                        $total_0 += $product->pricing[$hosting->billing_cycle];
                                        $quantity_0++;
                                        $list_hosting_0[] = $hosting;
                                        $next_due_date_0 = $hosting->next_due_date;
                                      }
                                  }
                                  elseif ($diff_date == 10) {
                                      if ($hosting->location == 'vn') {
                                        $this->da->deleteAccount($hosting);
                                        try {
                                            $data_log = [
                                              'user_id' => 9999,
                                              'action' => 'xóa',
                                              'model' => 'CronTab/CronTab_Hosting',
                                              'description' => 'dịch vụ Hosting <a target="_blank" href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> ',
                                              'service' => $hosting->domain
                                            ];
                                            $this->log_activity->create($data_log);
                                        } catch (Exception $e) {
                                            report($e);
                                        }
                                      }
                                  }

                              }
                              else {
                                  $diff_date = $next_due_date->diffInDays($now);
                                  if ($diff_date == 15) {
                                      $product = $this->product->find($hosting->product_id);
                                      // dd($product);
                                      if (!empty($product->pricing[$hosting->billing_cycle])) {
                                        $total_15 += $product->pricing[$hosting->billing_cycle];
                                        $quantity_15++;
                                        $list_hosting_15[] = $hosting;
                                        $next_due_date_15 = $hosting->next_due_date;
                                      }
                                  }

                                  if ($diff_date == 10) {
                                      $product = $this->product->find($hosting->product_id);
                                      // dd($product);
                                      if (!empty($product->pricing[$hosting->billing_cycle])) {
                                        $total_10 += $product->pricing[$hosting->billing_cycle];
                                        $quantity_10++;
                                        $list_hosting_10[] = $hosting;
                                        $next_due_date_10 = $hosting->next_due_date;
                                      }
                                  }

                                  if ($diff_date == 5) {
                                      $product = $this->product->find($hosting->product_id);
                                      // dd($product);
                                      if (!empty($product->pricing[$hosting->billing_cycle])) {
                                        $total_5 += $product->pricing[$hosting->billing_cycle];
                                        $quantity_5++;
                                        $list_hosting_5[] = $hosting;
                                        $next_due_date_5 = $hosting->next_due_date;
                                      }
                                  }

                              }

                          }
                        }
                        else {
                          if ($hosting->status_hosting != 'delete') {
                            if (!empty($hosting->next_due_date)) {
                                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                                $now = new Carbon($now);
                                $next_due_date = new Carbon($hosting->next_due_date);
                                if ($next_due_date->isPast()) {
                                  $diff_date = $next_due_date->diffInDays($now);
                                  if ($diff_date == 15) {
                                      $this->da->delete_hosting($hosting);
                                      // dd($product);
                                  }
                                }
                            }
                          }
                        }
                      } catch (\Exception $e) {
                        report($e);
                      }

                  }
              }
              // dd($quantity_15);
              if ($quantity_15 > 0) {
                  // dd('da den');
                  $this->subject = 'Thông báo Hosting sắp hết hạn';
                  $this->user_email = $user->email;
                  $data = [
                       'name' => $user->name,
                       'next_due_date' => $next_due_date_15,
                       'quantity' => $quantity_15,
                       'amount' => $total_15,
                       'list_domain' => $list_hosting_15,
                       'billing' => $billing,
                  ];
                  try {
                      // Mail::send('users.mails.mail_cron_nearly_hosting', $data, function($message){
                      //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      //     $message->to($this->user_email)->subject($this->subject);
                      //     $message->cc('support@cloudzone.vn')->subject($this->subject);
                      // });
                      $data_tb_send_mail = [
                          'type' => 'mail_cron_nearly',
                          'type_service' => 'hosting',
                          'user_id' => $user->id,
                          'status' => false,
                          'content' => serialize($data),
                      ];
                      $this->send_mail->create($data_tb_send_mail);
                  } catch (Exception $e) {
                      report($e);
                  }
                  // Tạo thông báo cho user
                  $content = '';
                  $content .= '<p>Thông báo Hosting sắp hết hạn ,<br />Cloudzone kính gửi thông tin các Hosting sắp hết hạn của quý khách: <br />- Danh sách Hosting sắp hết hạn: <br>';
                  foreach ($list_hosting_15 as $key => $domain_hosting) {
                      $content .= '&nbsp;&nbsp; + Domain: ' . $domain_hosting->domain . '<br />';
                  }
                  $content .= '- Số lượng: ' . $quantity_15 . '<br />';
                  $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_15)) . '<br />';
                  $content .= '- Thành tiền: ' .  number_format($total_15,0,",",".") . ' VNĐ<br />';
                  $content .= 'Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red"> bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                  $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

                  $data_notification = [
                      'name' => "Thông báo có $quantity_15 Hosting sắp hết hạn (còn 15 ngày)",
                      'content' => $content,
                      'status' => 'Active',
                  ];
                  $infoNotification = $this->notification->create($data_notification);
                  $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                  // Tạo log
                  $data_log = [
                     'user_id' => 999999,
                     'action' => 'thông báo hết hạn',
                     'model' => 'CronTab/Hosting_nearly_15',
                     'description' => 'khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> có ' . $quantity_15 . ' Hosting sắp hết hạn (còn 15 ngày)',
                  ];
                  $this->log_activity->create($data_log);
              }

              // dd($quantity_10);
              if ($quantity_10 > 0) {
                  // dd('da den');
                  $this->subject = 'Thông báo Hosting sắp hết hạn';
                  $this->user_email = $user->email;
                  $data = [
                       'name' => $user->name,
                       'next_due_date' => $next_due_date_10,
                       'quantity' => $quantity_10,
                       'amount' => $total_10,
                       'list_domain' => $list_hosting_10,
                       'billing' => $billing,
                   ];
                  try {
                      // Mail::send('users.mails.mail_cron_nearly_hosting', $data, function($message){
                      //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      //     $message->to($this->user_email)->subject($this->subject);
                      //     $message->cc('support@cloudzone.vn')->subject($this->subject);
                      // });
                      $data_tb_send_mail = [
                          'type' => 'mail_cron_nearly',
                          'type_service' => 'hosting',
                          'user_id' => $user->id,
                          'status' => false,
                          'content' => serialize($data),
                      ];
                      $this->send_mail->create($data_tb_send_mail);
                  } catch (Exception $e) {
                      report($e);
                  }
                  // Tạo thông báo cho user
                  $content = '';
                  $content .= '<p>Thông báo Hosting sắp hết hạn ,<br />Cloudzone kính gửi thông tin các Hosting sắp hết hạn của quý khách: <br />- Danh sách Hosting sắp hết hạn: <br>';
                  foreach ($list_hosting_10 as $key => $domain_hosting) {
                      $content .= '&nbsp;&nbsp; + Domain: ' . $domain_hosting->domain . '<br />';
                  }
                  $content .= '- Số lượng: ' . $quantity_10 . '<br />';
                  $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_10)) . '<br />';
                  $content .= '- Thành tiền: ' .  number_format($total_10,0,",",".") . ' VNĐ<br />';
                  $content .= 'Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red"> bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                  $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

                  $data_notification = [
                      'name' => "Thông báo có $quantity_10 Hosting sắp hết hạn (còn 10 ngày)",
                      'content' => $content,
                      'status' => 'Active',
                  ];
                  $infoNotification = $this->notification->create($data_notification);
                  $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                  // Tạo log
                  $data_log = [
                     'user_id' => 999999,
                     'action' => 'thông báo hết hạn',
                     'model' => 'CronTab/Hosting_nearly_10',
                     'description' => 'khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> có ' . $quantity_10 . ' Hosting sắp hết hạn (còn 10 ngày)',
                  ];
                  $this->log_activity->create($data_log);
              }

              // dd($quantity_10);
              if ($quantity_5 > 0) {
                  // dd('da den');
                  $this->subject = 'Thông báo Hosting sắp hết hạn';
                  $this->user_email = $user->email;
                  $data = [
                       'name' => $user->name,
                       'next_due_date' => $next_due_date_5,
                       'quantity' => $quantity_5,
                       'amount' => $total_5,
                       'list_domain' => $list_hosting_5,
                       'billing' => $billing,
                   ];
                  try {
                      // Mail::send('users.mails.mail_cron_nearly_hosting', $data, function($message){
                      //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      //     $message->to($this->user_email)->subject($this->subject);
                      //     $message->cc('support@cloudzone.vn')->subject($this->subject);
                      // });
                      $data_tb_send_mail = [
                          'type' => 'mail_cron_nearly',
                          'type_service' => 'hosting',
                          'user_id' => $user->id,
                          'status' => false,
                          'content' => serialize($data),
                      ];
                      $this->send_mail->create($data_tb_send_mail);
                  } catch (Exception $e) {
                      report($e);
                  }
                  // Tạo thông báo cho user
                  $content = '';
                  $content .= '<p>Thông báo Hosting sắp hết hạn ,<br />Cloudzone kính gửi thông tin các Hosting sắp hết hạn của quý khách: <br />- Danh sách Hosting sắp hết hạn: <br>';
                  foreach ($list_hosting_5 as $key => $domain_hosting) {
                      $content .= '&nbsp;&nbsp; + Domain: ' . $domain_hosting->domain . '<br />';
                  }
                  $content .= '- Số lượng: ' . $quantity_5 . '<br />';
                  $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_5)) . '<br />';
                  $content .= '- Thành tiền: ' .  number_format($total_5,0,",",".") . ' VNĐ<br />';
                  $content .= 'Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red"> bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                  $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

                  $data_notification = [
                      'name' => "Thông báo có $quantity_5 Hosting sắp hết hạn (còn 5 ngày)",
                      'content' => $content,
                      'status' => 'Active',
                  ];
                  $infoNotification = $this->notification->create($data_notification);
                  $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                  // Tạo log
                  $data_log = [
                     'user_id' => 999999,
                     'action' => 'thông báo hết hạn',
                     'model' => 'CronTab/Hosting_nearly_5',
                     'description' => 'khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> có' . $quantity_5 . ' Hosting  sắp hết hạn (còn 5 ngày)',
                  ];
                  $this->log_activity->create($data_log);
              }

              // dd($quantity_10);
              if ($quantity_0 > 0) {
                  // dd('da den');
                  $this->subject = 'Thông báo Hosting hết hạn';
                  $this->user_email = $user->email;
                  $data = [
                       'name' => $user->name,
                       'next_due_date' => $next_due_date_0,
                       'quantity' => $quantity_0,
                       'amount' => $total_0,
                       'list_domain' => $list_hosting_0,
                       'billing' => $billing,
                   ];
                   try {
                      // Mail::send('users.mails.mail_cron_nearly_hosting', $data, function($message){
                      //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      //     $message->to($this->user_email)->subject($this->subject);
                      //     $message->cc('support@cloudzone.vn')->subject($this->subject);
                      // });
                      $data_tb_send_mail = [
                          'type' => 'mail_cron_nearly',
                          'type_service' => 'hosting',
                          'user_id' => $user->id,
                          'status' => false,
                          'content' => serialize($data),
                      ];
                      $this->send_mail->create($data_tb_send_mail);
                   } catch (Exception $e) {
                       report($e);
                   }
                  // Tạo thông báo cho user
                  $content = '';
                  $content .= '<p>Thông báo Hosting hết hạn ,<br />Cloudzone kính gửi thông tin các Hosting sắp hết hạn của quý khách: <br />- Danh sách Hosting hết hạn: <br>';
                  foreach ($list_hosting_0 as $key => $domain_hosting) {
                      $content .= '&nbsp;&nbsp; + Domain: ' . $domain_hosting->domain . '<br />';
                  }
                  $content .= '- Số lượng: ' . $quantity_0 . '<br />';
                  $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($next_due_date_0)) . '<br />';
                  $content .= '- Thành tiền: ' .  number_format($total_0,0,",",".") . ' VNĐ<br />';
                  $content .= 'Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red"> bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                  $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

                  $data_notification = [
                      'name' => "Thông báo có $quantity_0 Hosting sắp hết hạn (còn 0 ngày)",
                      'content' => $content,
                      'status' => 'Active',
                  ];
                  $infoNotification = $this->notification->create($data_notification);
                  $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                  // Tạo log
                  $data_log = [
                     'user_id' => 999999,
                     'action' => 'thông báo hết hạn',
                     'model' => 'CronTab/Hosting_nearly_0',
                     'description' => 'khách hàng <a target="_blank" href="/admin/users/detail/' . $user->id . '">#' . $user->name . '</a> có ' . $quantity_0 . ' Hosting sắp hết hạn (còn 0 ngày)',
                  ];
                  $this->log_activity->create($data_log);
              }
            } catch (\Exception $e) {
              report($e);
            }
        }

    }

    public function check_gd_vcb()
    {
        $data_json = [
            'username' => $this->config_vcb['username'],
            'password' => $this->config_vcb['password'],
            'account_number' => $this->config_vcb['account_number'],
        ];
        $data_json = json_encode($data_json);
        $client = new Client([
            'headers' => [
            'Content-Type' => 'application/json',
            ],
        ]);
        $response = $client->post(
            $this->config_vcb['url'],
            [
                'body' => $data_json,
            ]
        );
        $response = json_decode($response->getBody()->getContents());
        // dd($response, $data_json, $this->config_vcb['url']);
        $list_vcb = $this->vcb->get();
        $list_payment = $this->history_pay->where('status', 'Pending')->orWhere('status' , 'confirm')->get();
        foreach ($response->data->ChiTietGiaoDich as $key => $value) {
            $stc = str_replace(' - ', '' , $value->SoThamChieu );
            if ($list_vcb->count() > 0) {
                $check = true;
                foreach ($list_vcb as $key => $vcb) {
                   if ($vcb->sothamchieu == $stc) {
                     $check = false;
                   }
                }
                if ($check) {
                    if ((int) str_replace(',', '' , $value->SoTienGhiCo ) > 0) {
                      $data_vcb = [
                          'sothamchieu' => $stc,
                          'total' => (int) str_replace(',', '' , $value->SoTienGhiCo ),
                          'mota' => $value->MoTa,
                          'ngaygd' => date('Y-m-d', strtotime($value->NgayGiaoDich)),
                      ];
                      $create_vcb = $this->vcb->create($data_vcb);
                      $mota = $value->MoTa;
                      foreach ($list_payment as $key => $payment) {
                          $magd = $payment->ma_gd;
                          if ( is_numeric(strpos( strtoupper($mota), $magd )) ) {
                             if ( (int) $payment->money <= (int) str_replace(',', '' , $value->SoTienGhiCo ) ) {
                                 $payment->magd_api = $stc;
                                 $payment->type_gd_api = 'vietcombank';
                                 $payment->money = (int) str_replace(',', '' , $value->SoTienGhiCo );
                                 $payment->save();
                                 $this->admin_payment->confirm_payment($payment->id);
                                 $data_log = [
                                   'user_id' => 999999,
                                   'action' => 'tự động xác nhận dịch vụ',
                                   'model' => 'CronTab/Confirm_payment',
                                   'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng vietcombank',
                                 ];
                                 $this->log_activity->create($data_log);
                             }
                          }
                      }
                    }
                }
            } else {
                if ((int) str_replace(',', '' , $value->SoTienGhiCo ) > 0) {
                  $data_vcb = [
                      'sothamchieu' => $stc,
                      'total' => (int) str_replace(',', '' , $value->SoTienGhiCo ),
                      'mota' => $value->MoTa,
                      'ngaygd' => date('Y-m-d', strtotime($value->NgayGiaoDich)),
                  ];
                  $create_vcb = $this->vcb->create($data_vcb);
                  $mota = $value->MoTa;
                  foreach ($list_payment as $key => $payment) {
                      $magd = $payment->ma_gd;
                      if ( is_numeric(strpos( strtoupper($mota), $magd )) ) {
                         if ( (int) $payment->money <= (int) str_replace(',', '' , $value->SoTienGhiCo ) ) {
                             $payment->magd_api = $stc;
                             $payment->type_gd_api = 'vietcombank';
                             $payment->save();
                             $user = $payment->user;
                             $user_credit = $user->credit;
                             $user_credit->value += (int) str_replace(',', '' , $value->SoTienGhiCo ) - (int) $payment->money;
                             $user_credit->save();
                             $this->admin_payment->confirm_payment($payment->id);
                         }
                      }
                  }
                }
            }
        }
    }

    public function delete_invoice_expired()
    {
        $list_invoices = $this->detail_order->where('status', 'unpaid')->get();
        foreach ($list_invoices as $key => $invoice) {
            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
            $now = new Carbon($now);
            $due_date = new Carbon($invoice->due_date);
            if ($due_date->isPast()) {
                $diff_date = $due_date->diffInDays($now);
                // 7 ngày
                if ( $diff_date >= 1 ) {
                   foreach ($invoice->history_pay as $key => $history_pay) {
                      $check =  $history_pay->delete();
                   }
                   $order = $invoice->order;
                   if ($order->type == 'order') {
                      $order = $invoice->order;
                      if ( $invoice->type == 'VPS' || $invoice->type == 'NAT-VPS' ) {
                         if ($invoice->vps->count() > 0) {
                           foreach ($invoice->vps as $key => $vps) {
                              if ( !empty($vps->vps_config) ) {
                                 $vps_config = $vps->vps_config;
                                 $vps_config->delete();
                              }
                              $vps->delete();
                           }
                         }
                      }
                      elseif ( $invoice->type == 'Hosting' || $invoice->type == 'Hosting-Singapore' ) {
                        if ($invoice->hostings->count() > 0) {
                          foreach ($invoice->hostings as $key => $hosting) {
                             $hosting->delete();
                          }
                        }
                      }
                      elseif ( $invoice->type == 'Domain' ) {
                        if ($invoice->domains->count() > 0) {
                          foreach ($invoice->domains as $key => $domain) {
                             $domain->delete();
                          }
                        }
                      }
                      elseif ( $invoice->type == 'addon_vps' ) {
                        if ($invoice->order_addon_vps->count() > 0) {
                          foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
                             $order_addon_vps->delete();
                          }
                        }
                      }
                      elseif ( $invoice->type == 'change_ip' ) {
                        if ($invoice->order_change_vps->count() > 0) {
                          foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                             $order_change_vps->delete();
                          }
                        }
                      }
                      elseif ( $invoice->type == 'upgrade_hosting' ) {
                        if (!empty($invoice->order_upgrade_hosting)) {
                          $order_upgrade_hosting = $invoice->order_upgrade_hosting;
                          $order_upgrade_hosting->delete();
                        }
                      }

                   } else {
                       if ( $invoice->type == 'VPS' || $invoice->type == 'NAT-VPS' ) {
                         if ($invoice->expired_vps->count() > 0) {
                            foreach ($invoice->expired_vps as $key => $expired_vps) {
                               $expired_vps->delete();
                            }
                         }
                       }
                       elseif ( $invoice->type == 'hosting' || $invoice->type == 'Hosting-Singapore' ) {
                         if ($invoice->expired_hostings->count() > 0) {
                           foreach ($invoice->expired_hostings as $key => $expired_hosting) {
                              $expired_hosting->delete();
                           }
                         }
                       }
                   }
                   $invoice->delete();
                   $data_log = [
                     'user_id' => 999999,
                     'action' => 'xóa',
                     'model' => 'CronTab/Delete_Order',
                     'description' => ' đơn đặt hàng ID: ' . $order->id . ' ',
                   ];
                   $this->log_activity->create($data_log);
                   $order->delete();
                }
            }
        }
    }


    public function momo()
    {
        $transMomo = $this->transMomo->getTrans();
        $list_momo = $this->momo->get();
        $list_payment = $this->history_pay->where('status', 'Pending')->orWhere('status' , 'confirm')->orderBy('id', 'desc')->get();
        if ( !empty($transMomo) ) {
            foreach ($transMomo as $key => $momo) {
              if ($list_momo->count() > 0) {
                $check = true;
                foreach ($list_momo as $key => $dtb_momo) {
                 if ($dtb_momo->id_momo == $momo['tranId']) {
                   $check = false;
                 }
                }
                if ($check) {
                  if ((int) $momo['amount'] > 0) {
                    $data_momo = [
                      'id_momo' => $momo['tranId'],
                      'number_phone' => !empty($momo['partnerId']) ? $momo['partnerId'] : '',
                      'amount' => (int) $momo['amount'],
                      'content' => !empty($momo['comment']) ? $momo['comment'] : '',
                      'date_trade' => date('Y-m-d'),
                    ];
                    $create_momo = $this->momo->create($data_momo);
                    $comment = !empty($momo['comment']) ? $momo['comment'] : '';
                    foreach ($list_payment as $key => $payment) {
                      $magd = $payment->ma_gd;
                      if ( is_numeric(strpos( strtoupper($comment), $magd )) ) {
                         if ( (int) $payment->money <= (int) $momo['amount'] ) {
                           $payment->magd_api = $momo['partnerId'];
                           $payment->type_gd_api = 'momo';
                            $payment->money = (int) $momo['amount'];
                            $payment->save();
                           $this->admin_payment->confirm_payment($payment->id);
                           $data_log = [
                             'user_id' => 999999,
                             'action' => 'tự động xác nhận dịch vụ',
                             'model' => 'CronTab/Confirm_payment',
                             'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng momo',
                           ];
                           $this->log_activity->create($data_log);
                         }
                         else {
                            if ($payment->type_gd == 1) {
                              $payment->money = $momo['amount'];
                              $payment->magd_api = $momo['partnerId'];
                              $payment->type_gd_api = 'momo';
                              $payment->save();
                              $this->admin_payment->confirm_payment($payment->id);

                              $data_log = [
                                'user_id' => 999999,
                                'action' => 'tự động xác nhận dịch vụ',
                                'model' => 'CronTab/Confirm_payment',
                                'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng vietcombank',
                              ];
                              $this->log_activity->create($data_log);
                            } else {
                              $user = $payment->user;
                              $data_history = [
                                'user_id' => $user->id,
                                'ma_gd' => 'GDMM' . strtoupper(substr(sha1(time()), 34, 39)),
                                'discription' => 'Cộng tiền vào tài khoản',
                                'type_gd' => '1',
                                'method_gd' => 'credit',
                                'date_gd' => date('Y-m-d'),
                                'money'=> $momo['amount'],
                                'status' => 'Active',
                              ];
                              $create = $this->history_pay->create($data_history);

                              $user_credit = $user->credit;
                              $user_credit->value += (int) $momo['amount'];
                              $user_credit->total += (int) $momo['amount'];
                              $user_credit->save();

                              $data_log = [
                               'user_id' => $user->id,
                               'action' => 'Cộng tiền vào tài khoản',
                               'model' => 'Admin/VCB',
                               'description_user' => ' cộng ' . number_format($momo['amount'],0,",",".") . ' VNĐ vào số dư tài khoản do thanh toán dịch vụ thiếu ' . number_format( (int) $payment->money - (int) $momo['amount'] ,0,",",".") . ' VNĐ',
                              ];
                              $this->log_activity->create($data_log);
                              $data_log = [
                               'user_id' => 999999,
                               'action' => 'Cộng tiền vào tài khoản',
                               'model' => 'Admin/VCB',
                               'description' => ' cộng ' . number_format($momo['amount'],0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> do thanh toán dịch vụ thiếu ' . number_format( (int) $payment->money - (int) $momo['amount'] ,0,",",".") . ' VNĐ',
                              ];
                              $this->log_activity->create($data_log);

                              // gữi mail
                              try {
                                $data = [
                                  'name' => $user->name,
                                  'amount' => $momo['amount'],
                                  'ma_gd' => $create->ma_gd,
                                  'credit' => $user->credit->value,
                                  'payment' => $payment,
                                  'method' => 'MoMo',
                                ];

                                $this->user_email = $user->email;
                                $this->subject = 'Nạp tiền vào tài khoản tại CLOUDZONE thành công';

                                  $mail = Mail::send('users.mails.finish_pay_error', $data, function($message){
                                  $message->from('support@cloudzone.vn', 'Portal Cloudzone');
                                  $message->to($this->user_email)->subject($this->subject);
                                });

                              } catch (\Exception $e) {
                                report($e);
                                continue;
                              }
                            }
                         }
                     }
                   }
                 }
               }
              }
              else {
                if ((int) $momo['amount'] > 0) {
                  $data_momo = [
                    'id_momo' => $momo['tranId'],
                    'number_phone' => !empty($momo['partnerId']) ? $momo['partnerId'] : '',
                    'amount' => (int) $momo['amount'],
                    'content' => !empty($momo['comment']) ? $momo['comment'] : '',
                    'date_trade' => date('Y-m-d'),
                  ];
                  $create_momo = $this->momo->create($data_momo);
                  $comment = !empty($momo['comment']) ? $momo['comment'] : '';
                  foreach ($list_payment as $key => $payment) {
                    $magd = $payment->ma_gd;
                    if ( is_numeric(strpos( strtoupper($comment), $magd )) ) {
                     if ( (int) $payment->money <= (int) $momo['amount'] ) {
                       $payment->magd_api = $momo['partnerId'];
                       $payment->type_gd_api = 'momo';
                       $payment->save();
                       $user = $payment->user;
                       $user_credit = $user->credit;
                       $user_credit->value += (int) $momo['amount'] - (int) $payment->money;
                       $user_credit->save();
                       $this->admin_payment->confirm_payment($payment->id);
                     }
                   }
                 }
               }
              }
            }
        }
    }

    public function delete_momo()
    {
        for ($i=0; $i < 5; $i++) {
          $momos = $this->momo->paginate(100);
          foreach ($momos as $key => $momo) {
            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
            $now = new Carbon($now);
            $create_at = new Carbon($momo->created_at);
            // dd($create_at->diffInDays($now), $create_at , $now);
            if ( $create_at->diffInDays($now) >= 20 ) {
              $momo->delete();
            }
          }
        }
    }

    public function delete_log_activity()
    {
        for ($i=0; $i < 5; $i++) {
            $log_activities = $this->log_activity->paginate(1000);
            foreach ($log_activities as $key => $log) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $now = new Carbon($now);
                $create_at = new Carbon($log->created_at);
                // dd($create_at->diffInDays($now), $create_at , $now);
                if ( $create_at->diffInDays($now) >= 20 ) {
                    $log->delete();
                }
            }
        }
    }

    public function delete_payment()
    {
        $list_payment = $this->history_pay->where('status', 'confirm')->orWhere('status', 'Pending')->get();
        if ( $list_payment->count() > 0 ) {
            foreach ($list_payment as $key => $payment) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $now = new Carbon($now);
                $create_at = new Carbon($payment->created_at);
                // dd($create_at->diffInDays($now), $create_at , $now);
                if ( $create_at->diffInDays($now) >= 3 ) {
                  $payment->delete();
                }
            }
        }

        $list_notification = $this->notification->get();
        foreach ($list_notification as $key => $notification) {
            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
            $now = new Carbon($now);
            $create_at = new Carbon($notification->created_at);
            // dd($create_at->diffInDays($now), $create_at , $now);
            if ( $create_at->diffInDays($now) >= 10 ) {
                $this->readnotification->where('notification_id', $notification->id)->delete();
                $notification->delete();
            }
        }

    }

    public function delete_order()
    {
      try {
        $list_order = $this->order->where('status', 'Pending')->get();
        // dd($list_order);
        foreach ($list_order as $key => $order) {
          try {
            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
            $now = new Carbon($now);
            $create_at = new Carbon($order->created_at);
            if ( $create_at->diffInDays($now) >= 10 ) {
              $detail_orders = $order->detail_orders;
              foreach ($detail_orders as $key => $detail_order) {
                try {
                  $this->vps->where('detail_order_id', $detail_order->id)->delete();
                  $this->hosting->where('detail_order_id', $detail_order->id)->delete();
                  $this->history_pay->where('detail_order_id', $detail_order->id)->delete();
                  $this->order_expired->where('detail_order_id', $detail_order->id)->delete();
                  $detail_order->delete();
                } catch (\Exception $e) {
                    report($e);
                }
              }
              $order->delete();
            }
          } catch (\Exception $e) {
            report($e);
          }
        }
      } catch (\Exception $e) {
        report($e);
      }
    }

    public static function encryptDecrypt($data, $key, $mode = 'ENCRYPT')
  	{
  		if (strlen($key) < 32) {
  			$key = str_pad($key, 32, 'x');
  		}

  		$key = substr($key, 0, 32);
  		$iv = pack('C*', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  		if ($mode === 'ENCRYPT') {
  			return base64_encode(openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));
  		}
  		else {
  			return openssl_decrypt(base64_decode($data), 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
  		}
  	}

    public function autoRefurn()
    {
        $list_vps_refurn = $this->vps->where('ip', '!=', "")->where('status_vps', '!=', 'delete_vps')
                                ->where('status_vps', '!=', 'change_user')
                                ->where('status_vps', '!=', 'cancel')->where('auto_refurn', true)
                                ->with('user_vps')->orderBy('id', 'desc')->get();
        // dd($list_vps_refurn);
        if ( $list_vps_refurn->count() > 0 ) {
            // dd($list_vps_refurn);
            foreach ($list_vps_refurn as $key => $vps) {
                try {
                    if (!empty($vps->next_due_date)) {
                        $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                        $now = new Carbon($now);
                        $next_due_date = new Carbon($vps->next_due_date);
                        if ( $vps->location == 'cloudzone' ) {
                            if ($next_due_date->isPast()) {
                                if ( $next_due_date->diffInDays($now) < 5 ) {
                                    try {
                                       $auto_refurn = $this->autoRefurnVpsVn($vps);
                                        if ( !$auto_refurn ) {
                                            try {
                                                $data_tb_send_mail = [
                                                    'type' => 'admin_mail_cron_auto_refurn_error',
                                                    'type_service' => 'vps',
                                                    'user_id' => $vps->user_vps->id,
                                                    'service_id' => $vps->id,
                                                    'status' => false,
                                                ];
                                                $this->send_mail->create($data_tb_send_mail);
                                            } catch (Exception $e) {
                                                report($e);
                                            }
                                        }
                                    } catch (Exception $e) {
                                        report($e);
                                    }
                                }
                            }
                            else {
                                if ( $next_due_date->diffInDays($now) <= 5 ) {
                                    try {
                                        $auto_refurn = $this->autoRefurnVpsVn($vps);
                                        if ( !$auto_refurn ) {
                                            try {
                                                $data_tb_send_mail = [
                                                    'type' => 'admin_mail_cron_auto_refurn_error',
                                                    'type_service' => 'vps',
                                                    'user_id' => $vps->user_vps->id,
                                                    'service_id' => $vps->id,
                                                    'status' => false,
                                                ];
                                                $this->send_mail->create($data_tb_send_mail);
                                            } catch (Exception $e) {
                                                report($e);
                                            }
                                        }
                                    } catch (Exception $e) {
                                        report($e);
                                    }
                                }
                            }
                        }
                        else {
                            if ($next_due_date->isPast()) {
                                if ( $next_due_date->diffInDays($now) <= 4 ) {
                                    try {
                                        $auto_refurn = $this->autoRefurnVpsUs($vps);
                                    } catch (Exception $e) {
                                        report($e);
                                    }
                                }
                            }
                            else {
                                if ( $next_due_date->diffInDays($now) <= 5 ) {
                                    try {
                                        $auto_refurn = $this->autoRefurnVpsUs($vps);
                                    } catch (Exception $e) {
                                        report($e);
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception $e) {
                    report($e);
                }
            }
        }
    }

    public function autoRefurnVpsVn($vps)
    {
        $billings = config('billing');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $billing_price = config('billingDashBoard');
        $user = $this->user->find($vps->user_id);
        $credit = $user->credit;
        $billing_cycle = $vps->billing_cycle;
        $total = 0;
        $product = $this->product->find($vps->product_id);
        $invoiced = $vps->detail_order;
        if (!empty($vps->price_override)) {
            $total = $vps->price_override;
        }
        else {
            $total += $product->pricing[$billing_cycle];
            if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($user->id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                    }
                  }
                }
                $total += $pricing_addon;
            }
        }
        if ( $credit->value < $total ) {
            // Tạo thông báo cho user
            $content = '';
            $content .= 'Thông báo tự động gia hạn VPS thất bại <br />';
            $content .= 'Nguyên nhân: Số dư tài khoản không đủ để thực hiện gia hạn VPS <br />';
            $content .= '<br />Cloudzone kính gửi thông tin VPS sắp hết hạn của quý khách: <br />';
            $content .= '- IP: ' . $vps->ip . '<br />';
            $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($vps->next_due_date)) . '<br />';
            $content .= '- Thành tiền: ' .  number_format($total,0,",",".") . ' VNĐ<br />';
            $content .= '- Số dư tài khoản: ' .  number_format($credit->value,0,",",".") . ' VNĐ<br />';
            $content .= 'Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó,
            và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
            Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
            $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

            $data_notification = [
                'name' => "Thông báo tự động gia hạn VPS " . $vps->ip . " thất bại",
                'content' => $content,
                'status' => 'Active',
            ];
            $infoNotification = $this->notification->create($data_notification);
            $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                // Tạo log
            $data_log = [
                'user_id' => 999999,
                'action' => 'tư động',
                'model' => 'CronTab',
                'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> thất bại.',
                'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $mail_system = $this->email->where('type', 3)->where('type_service', 'error_auto_refurn')->first();
            if ( isset($mail_system) ) {
                $cpu = 0;
                $ram = 0;
                $disk = 0;
                if(!empty($product->meta_product)) {
                    $cpu = $product->meta_product->cpu;
                    $ram = $product->meta_product->memory;
                    $disk = $product->meta_product->disk;
                } else {
                    $cpu = 1;
                    $ram = 1;
                    $disk = 20;
                }
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services_config = $cpu . ' CPU ' . $ram . ' RAM ' . $disk . ' DISK';
                $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$ip}',
                '{$services_username}', '{$services_password}', '{$services_config}', '{$sub_total} ', '{$total}',
                '{$state}', '{$next_due_date}'];
                $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0,
                url(''), '',  $vps->ip, $vps->user, $vps->password, $services_config, number_format($total,0,",","."),
                number_format($total,0,",","."), $total,$vps->state, date('d-m-Y', strtotime( $next_due_date )) ];
                $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'Tự động gia hạn VPS thất bại',
                ];
            } else {
                $data = [
                    'name' => $user->name,
                    'ip' => $vps->ip,
                    'next_due_date' => $vps->next_due_date,
                    'total' => $total,
                    'credit' => $credit->value,
                ];
            }
            try {
                $data_tb_send_mail = [
                    'type' => 'mail_cron_auto_refurn_error',
                    'type_service' => 'vps',
                    'user_id' => $user->id,
                    'service_id' => $vps->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (Exception $e) {
                report($e);
            }

            return false;
        }

        $date_star = $vps->next_due_date;
        $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
        $data_expired[] = [
            'package' => (int) $total,
            'leased_time' => $billing_price[$billing_cycle],
            'type' => ($vps->type_vps == 'nat_vps') ? 5 : 0,
            'vm_id' => $vps->vm_id,
            'end_date' => $due_date
        ];
        try {
            $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id);
        } catch (Exception $e) {
            return false;
        }
        // $reques_vcenter = true;
        if ($reques_vcenter) {
            // $reques_vcenter->error == 0
            if ($reques_vcenter->error == 0) {
                // create order
                $data_order = [
                    'user_id' => $user->id,
                    'total' => $total,
                    'status' => 'Finish',
                    'description' => 'expired vps',
                    'type' => 'expired',
                    'makh' => '',
                ];
                $order = $this->order->create($data_order);
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'VPS',
                    'due_date' => $date,
                    'paid_date' => date('Y-m-d'),
                    'description' => 'Tự động gia hạn VPS',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $history_pay = $this->history_pay->create($data_history);
                // ghi log giao dịch
                $data_log_payment = [
                    'history_pay_id' => $history_pay->id,
                    'before' => $credit->value,
                    'after' => $credit->value - $total,
                ];
                $this->log_payment->create($data_log_payment);
                // Trừ tiền tài khoản
                if (!empty($credit)) {
                    $credit->value = $credit->value - $total;
                    $credit->save();
                }
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $vps->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'vps',
                ];
                $order_expired_vps = $this->order_expired->create($data_order_expired);
                $vps->next_due_date = $due_date;
                $vps->status_vps = 'on';
                $vps->save();
                // ghi log
                $data_log = [
                    'user_id' => 999999,
                    'action' => 'tự động',
                    'model' => 'CronTab',
                    'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                    'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                try {
                    // gui mail
                    $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
                } catch (\Exception $e) {
                    report($e);
                }
            } else {
                // ghi log
                $data_log = [
                    'user_id' => 999999,
                    'action' => 'tự động',
                    'model' => 'CronTab',
                    'description' => ' gia hạn thất bại VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                ];
                $this->log_activity->create($data_log);
            }
        }
        return true;
    }

    public function autoRefurnVpsUs($vps)
    {
        $billings = config('billing');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $billing_price = config('billingDashBoard');
        $user = $this->user->find($vps->user_id);
        $credit = $user->credit;
        $billing_cycle = $vps->billing_cycle;
        $total = 0;
        $product = $this->product->find($vps->product_id);
        $invoiced = $vps->detail_order;
        if (!empty($vps->price_override)) {
            $total = $vps->price_override;
        }
        else {
            $total += $product->pricing[$billing_cycle];
            if (!empty($vps->vps_config)) {
                $addon_vps = $vps->vps_config;
                $add_on_products = $this->get_addon_product_private($user->id);
                $pricing_addon = 0;
                foreach ($add_on_products as $key => $add_on_product) {
                  if (!empty($add_on_product->meta_product->type_addon)) {
                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                    }
                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                    }
                  }
                }
                $total += $pricing_addon;
            }
        }
        if ( $credit->value < $total ) {
            // Tạo thông báo cho user
            $content = '';
            $content .= 'Thông báo tự động gia hạn VPS US thất bại <br />';
            $content .= 'Nguyên nhân: Số dư tài khoản không đủ để thực hiện gia hạn VPS <br />';
            $content .= '<br />Cloudzone kính gửi thông tin VPS sắp hết hạn của quý khách: <br />';
            $content .= '- IP: ' . $vps->ip . '<br />';
            $content .= '- Ngày hết hạn: ' . date('m-d-Y', strtotime($vps->next_due_date)) . '<br />';
            $content .= '- Thành tiền: ' .  number_format($total,0,",",".") . ' VNĐ<br />';
            $content .= '- Số dư tài khoản: ' .  number_format($credit->value,0,",",".") . ' VNĐ<br />';
            $content .= 'Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó,
            và <b style="color:red"> bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn.
            Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
            $content .= '<br /> <br /><b>Cloudzone xin cảm ơn quý khách!</b>';

            $data_notification = [
                'name' => "Thông báo tự động gia hạn VPS US thất bại",
                'content' => $content,
                'status' => 'Active',
            ];
            $infoNotification = $this->notification->create($data_notification);
            $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                // Tạo log
            $data_log = [
                'user_id' => 999999,
                'action' => 'tư động',
                'model' => 'CronTab',
                'description' => ' gia hạn VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> thất bại.',
                'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $data = [
                'name' => $user->name,
                'ip' => $vps->ip,
                'next_due_date' => $vps->next_due_date,
                'total' => $total,
                'credit' => $credit->value,
            ];
            try {
                $data_tb_send_mail = [
                    'type' => 'mail_cron_auto_refurn_error',
                    'type_service' => 'vps',
                    'user_id' => $user->id,
                    'service_id' => $vps->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (Exception $e) {
                report($e);
            }

            return false;
        }

        $date_star = $vps->next_due_date;
        $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
        $data_expired[] = [
            'package' => (int) $total,
            'leased_time' => $billing_price[$billing_cycle],
            'type' => 0,
            'vm_id' => $vps->vm_id,
            'end_date' => $due_date
        ];
        try {
            $reques_vcenter = $this->dashboard->expired_vps_us($data_expired, $vps->vm_id);
        } catch (Exception $e) {
            return false;
        }
        // $reques_vcenter = true;
        if ($reques_vcenter) {
            // $reques_vcenter->error == 0
            if ($reques_vcenter->error == 0) {
                // create order
                $data_order = [
                    'user_id' => $user->id,
                    'total' => $total,
                    'status' => 'Finish',
                    'description' => 'expired vps',
                    'type' => 'expired',
                    'makh' => '',
                ];
                $order = $this->order->create($data_order);
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'VPS US',
                    'due_date' => $date,
                    'paid_date' => date('Y-m-d'),
                    'description' => 'Tự động gia hạn VPS US',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXVU' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $history_pay = $this->history_pay->create($data_history);
                // ghi log giao dịch
                $data_log_payment = [
                    'history_pay_id' => $history_pay->id,
                    'before' => $credit->value,
                    'after' => $credit->value - $total,
                ];
                $this->log_payment->create($data_log_payment);
                // Trừ tiền tài khoản
                if (!empty($credit)) {
                    $credit->value = $credit->value - $total;
                    $credit->save();
                }
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $vps->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'vps',
                ];
                $order_expired_vps = $this->order_expired->create($data_order_expired);
                $vps->next_due_date = $due_date;
                $vps->status_vps = 'on';
                $vps->save();
                // ghi log
                $data_log = [
                    'user_id' => 999999,
                    'action' => 'tự động',
                    'model' => 'CronTab',
                    'description' => ' gia hạn VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                    'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                try {
                    // gui mail
                    $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
                } catch (\Exception $e) {
                    report($e);
                }
            } else {
                // ghi log
                $data_log = [
                    'user_id' => 999999,
                    'action' => 'tự động',
                    'model' => 'CronTab',
                    'description' => ' gia hạn thất bại VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
                ];
                $this->log_activity->create($data_log);
            }
        }
        return true;
    }

    public function send_mail_finish_expired($type, $order , $detail_order, $order_expired)
    {
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if ($type == 'vps') {
            $services = $order_expired->vps;
            // dd($order_expired , $order_expired->vps, $services);
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);

            $address = !empty( $user->user_meta->address ) ? $user->user_meta->address : '';
            $phone = !empty( $user->user_meta->phone ) ? $user->user_meta->phone : '';

            $user = $this->user->find($services->user_id);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $address, $phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $data = [
                'content' => $content,
                'user_name' => $user->name,
            ];

            try {
                $data_tb_send_mail = [
                    'type' => 'mail_cron_auto_refurn_finish',
                    'type_service' => 'vps',
                    'user_id' => $user->id,
                    'service_id' => $services->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (Exception $e) {
               report($e);
            }
        }
        elseif ($type == 'hosting') {
            $services = $order_expired->hosting;
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);
            $user = $this->user->find($services->user_id);

            $address = !empty($user->user_meta->address) ? $user->user_meta->address : '';
            $address = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';

            $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $address, $phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $this->user_email = $this->user->find($services->user_id)->email;
            $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ';

            $data = [
                'content' => $content,
            ];

            try {
                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_email)->subject($this->subject);
                });
            } catch (Exception $e) {
                report($e);
            }
        }
    }

    public function loc_vps_trung_nhau()
    {
        $list_vps = $this->vps->orderBy('id', 'desc')->paginate(3000);
        $data = [];
        $id_trung = [];
        foreach ($list_vps as $vps) {
            $check = true;
            if ( $vps->status_vps != 'delete_vps' && $vps != 'change_user' && $vps != 'cancel' && !empty($vps->id) ) {
                if ( count($id_trung) ) {
                    foreach ($id_trung as $key => $id_vps) {
                        if ( $vps->id == $id_vps ) {
                            $check = false;
                        }
                    }
                }
                if ($check) {
                    foreach ($list_vps as $vps2) {
                        if ( $vps->id != $vps2->id ) {
                            if ( $vps->vm_id == $vps2->vm_id ) {
                                if ( $vps2->status_vps != 'delete_vps' && $vps2 != 'change_user' && !empty($vps->id) ) {
                                    $data[$vps->id]['vps'] = $vps;
                                    $data[$vps->id]['vps_trung'][$vps2->id] = $vps2;
                                    $id_trung[] = $vps2->id;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $data;
    }

    function rand_string() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < 512; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function send_mail_cron()
    {
        $list_mail = $this->send_mail->whereType('mail_cron_nearly')->whereStatus(false)->take(100)->get();
        if ( $list_mail->count() ) {
            foreach ($list_mail as $mail) {
                if ( $mail->type_service == 'vps' ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        $data['subject'] = "Thông báo VPS hết hạn";
                        // gửi mail
                        SendMailCronNearly::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_VPS',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
                elseif ( $mail->type_service == 'vps_us' ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        $data['subject'] = "Thông báo VPS US hết hạn";
                        // gửi mail
                        SendMailCronNearly::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_VPS',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
                elseif ( $mail->type_service == 'proxy' ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        $data['subject'] = "Thông báo Proxy hết hạn";
                        // gửi mail
                        SendMailCronNearlyProxy::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_VPS',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
                elseif ( $mail->type_service == 'server' ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        $data['subject'] = "Thông báo Server hết hạn";
                        // gửi mail
                        SendMailCronNearlyServer::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_VPS',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
                elseif ( $mail->type_service == 'colocation' ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        $data['subject'] = "Thông báo Colocation hết hạn";
                        // gửi mail
                        SendMailCronNearlyColocation::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_VPS',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
                elseif ( $mail->type_service == 'hosting'  ) {
                    try {
                        $user = $mail->user;
                        $data = unserialize($mail->content);
                        // gửi mail
                        SendMailCronNearly::dispatch($user , $data)->delay(now()->addMinute(1));
                        // đánh dấu mail đã gửi
                        $mail->status = true;
                        $mail->send_time = date('Y-m-d H:i:s');
                        $mail->save();
                    } catch (Exception $e) {
                        report($e);
                        try {
                            $data_log = [
                                'user_id' => 999999,
                                'action' => 'send_mail_error',
                                'model' => 'CronTab/CronTab_Send_Mail_Nearly_Hosting',
                                'description' => 'gửi mai thất bại #' . $mail->id,
                            ];
                            $this->log_activity->create($data_log);
                        } catch (Exception $e) {
                            report($e);
                        }
                        continue;
                    }
                }
            }
        }

    }

    public function send_mail_once_minute()
    {
        $list_mail = $this->send_mail->where('type', '!=' ,'mail_cron_nearly')->whereStatus(false)->take(10)->get();
        // dd($list_mail);
        if ( $list_mail->count() ) {
            foreach ($list_mail as $mail) {
                // Lỗi tự động gia hạn VPS
                if ( $mail->type == 'admin_mail_cron_auto_refurn_error' ) {
                    if ( $mail->type_service == 'vps' ) {
                        $user = $mail->user;
                        $vps = $this->vps->find( $mail->service_id );
                        try {
                            $data = [
                                'name' => $user->name,
                                'ip' => $vps->ip,
                            ];
                            $subject = 'Tự động gia hạn VPS thất bại';
                            // gửi mail
                            MailErrorAutoRefund::dispatch($data, $subject)->delay(now()->addMinute(1));
                            // đánh dấu mail đã gửi 2021-06-10 09:43:47
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                        } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                        }
                    }
                    elseif ( $mail->type_service == 'vps_us' ) {
                        $user = $mail->user;
                        $vps = $this->vps->find( $mail->service_id );
                        try {
                            $data = [
                                'name' => $user->name,
                                'ip' => $vps->ip,
                            ];
                            $subject = 'Tự động gia hạn VPS US thất bại';
                            // gửi mail
                            MailErrorAutoRefund::dispatch($data, $subject)->delay(now()->addMinute(1));
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                        } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                        }
                    }
                }
                elseif ( $mail->type == 'mail_cron_auto_refurn_error' ) {
                    if ( $mail->type_service == 'vps' ) {
                        $user = $mail->user;
                        $vps = $this->vps->find( $mail->service_id );
                        try {
                            $data = unserialize($mail->content);
                            $subject = 'Tự động gia hạn VPS thất bại';
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'error_auto_refurn')->first();
                            if ( isset($mail_system) ) {
                                MailOrder::dispatch($user, $data);
                            } else {
                                MailErrorAutoRefundUser::dispatch($user, $data, $subject)->delay(now()->addMinute(1));
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                        } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                        }
                    }
                    elseif ( $mail->type_service == 'vps_us' ) {
                        $user = $mail->user;
                        $vps = $this->vps->find( $mail->service_id );
                        try {
                            $data = unserialize($mail->content);
                            $subject = 'Tự động gia hạn VPS US thất bại';
                            // gửi mail
                            MailErrorAutoRefundUser::dispatch($user, $data, $subject)->delay(now()->addMinute(1));
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                        } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                        }
                    }
                }
                elseif ( $mail->type == 'mail_cron_auto_refurn_finish' ) {
                    if ( $mail->type_service == 'vps' ) {
                        $user = $mail->user;
                        $vps = $this->vps->find( $mail->service_id );
                        try {
                            $data = unserialize($mail->content);
                            // dd($data, $user, $vps);
                            $subject = 'Tự động gia hạn VPS thành công';
                            // gửi mail
                            MailFinishAutoRefund::dispatch($user, $data, $subject)->delay(now()->addMinute(1));
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                        } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                        }
                    }
                }
                elseif (
                    $mail->type == 'mail_order_vps' || $mail->type == 'mail_order_vps_us' || $mail->type == 'mail_order_hosting' ||
                    $mail->type == 'mail_order_expire_vps' || $mail->type == 'mail_order_expire_vps_us' || $mail->type == 'mail_order_expire_hosting' ||
                    $mail->type == 'mail_order_change_ip' || $mail->type == 'mail_order_server'
                ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailOrder::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif (
                    $mail->type == 'mail_finish_order_vps' || $mail->type == 'mail_finish_order_hosting' || $mail->type == 'mail_finish_order_email_hosting' ||
                    $mail->type == 'mail_finish_order_expire_vps' || $mail->type == 'mail_finish_order_expire_hosting' ||
                    $mail->type == 'mail_finish_order_server' || $mail->type == 'mail_finish_order_expire_server' || $mail->type == 'mail_finish_config_server'
                    || $mail->type == 'mail_finish_order_colo' || $mail->type == 'mail_order_colo' || $mail->type == 'mail_finish_payment_colo'
                    || $mail->type == 'mail_finish_order_expire_proxy' || $mail->type == 'mail_finish_order_proxy' || $mail->type == 'mail_finish_payment_colo'
                ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailOrder::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'order_upgrade_hosting' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            OrderUpgradeHosting::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'order_addon_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            OrderAddonVPS::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'finish_order_upgarde_hosting' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            FinishOrderUpgradeHosting::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'finish_order_addon_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_addon_vps')->first();
                            if ( isset($mail_system) ) {
                                MailOrder::dispatch($user, $data);
                            } else {
                                FinishOrderAddonVPS::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_create_user' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            CreateUser::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'admin_send_mail' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            AdminSendMail::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'admin_create_payment' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            AdminCreateFinish::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'finish_payment' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_payment')->first();
                            if ( isset($mail_system) ) {
                                MailOrder::dispatch($user, $data);
                            }
                            else {
                                FinishPayment::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'request_payment' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_request_payment = $this->email->where('type', 3)->where('type_service', 'request_payment')->first();
                            if ( isset( $mail_request_payment ) ) {
                                $data['subject'] = !empty($mail_request_payment->meta_email->subject) ? $mail_request_payment->meta_email->subject : 'Yêu cầu nạp tiền vào tài khoản tại CLOUDZONE';
                                MailOrder::dispatch($user, $data);
                            } else {
                                RequestPayment::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'finish_pay_error' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            FinishPayment::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_finish_rebuild_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            FinishRebuildVps::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_create_error_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailErrorCreateVPS::dispatch($data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_create_error_vps_us' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailErrorCreateVPSUS::dispatch($data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_expire_error_vps' || $mail->type == 'mail_expire_error_vps_us' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailErrorExpireVps::dispatch($data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_upgrade_error_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailErrorUpgradeVps::dispatch($data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_rebuild_error_vps' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailErrorRebuildVps::dispatch($data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_verify_user' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'create_user')->first();
                            if ( isset($mail_system) ) {
                                // dd($data);
                                MailOrder::dispatch($user, $data);
                            } else {
                                MailVerifyUser::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_tutorial_user' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'verify_user')->first();
                            if ( isset($mail_system) ) {
                                // dd($data);
                                MailOrder::dispatch($user, $data);
                            } else {
                                MailTutorialUser::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_marketing_user' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailMarketingUser::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_marketing_tiny' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailMarketingTiny::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_marketing_vps_uk' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailMarketingVPSUK::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'test_mail_template_marketing' || $mail->type == 'mail_template_marketing' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailTemplateMarketing::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_update_feature' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            // dd($user, $data);
                            MailUpdateFeature::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_payment_change_ip' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            MailPaymentChangeIp::dispatch($user, $data);
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_order_finish_change_ip' ) {
                    $user = $mail->user;
                    try {
                            $data = unserialize($mail->content);
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_change_ip')->first();
                            if ( isset($mail_system) ) {
                                MailOrder::dispatch($user, $data);
                            } else {
                                MailFinishChangeIp::dispatch($user, $data);
                            }
                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
                elseif ( $mail->type == 'mail_forgot_password' ) {
                    $user = $mail->user;
                    try {
                            // gửi mail
                            $mail_system = $this->email->where('type', 3)->where('type_service', 'forgot_password')->first();
                            if ( isset($mail_system) ) {
                                MailOrder::dispatch($user, $data);
                            } else {
                                SendMailForgot::dispatch($user);
                            }                            // đánh dấu mail đã gửi
                            $mail->status = true;
                            $mail->send_time = date('Y-m-d H:i:s');
                            $mail->save();
                    } catch (\Throwable $th) {
                            report($th);
                            try {
                                $data_log = [
                                    'user_id' => 999999,
                                    'action' => 'send_mail_error',
                                    'model' => 'CronTab/CronTab_Send_Mail',
                                    'description' => 'gửi mai thất bại #' . $mail->id,
                                ];
                                $this->log_activity->create($data_log);
                            } catch (Exception $e) {
                                report($e);
                            }
                            continue;
                    }
                }
            }
        }
    }

    public function delete_send_mail()
    {
        for ($i=0; $i < 5; $i++) {
            $send_mails = $this->send_mail->paginate(1000);
            foreach ($send_mails as $key => $send_mail) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $now = new Carbon($now);
                $create_at = new Carbon($send_mail->created_at);
                // dd($create_at->diffInDays($now), $create_at , $now);
                if ( $create_at->diffInDays($now) >= 30 ) {
                    $send_mail->delete();
                }
            }
        }
    }

    public function send_mail_tutorial()
    {
        $list_users = $this->user->whereTutorial(false)->get();
        foreach ($list_users as $key => $user) {
          try {
              $data_user = [
                  'name' => $user->name,
                  'subject' => 'Hướng dẫn sử dụng tài khoản Portal Cloudzone'
              ];

              $data_tb_send_mail = [
                  'type' => 'mail_tutorial_user',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data_user),
              ];
              $this->send_mail->create($data_tb_send_mail);

              $user->tutorial  = true;
              $user->save();
          } catch (\Throwable $th) {
              //throw $th;
              report($th);
          }
        }
    }

    public function send_mail_update_feature()
    {
        $list_users = $this->user->whereTutorial(false)->get();
        foreach ($list_users as $key => $user) {
          try {
            $check = true;
            if ( $user->group_user_id == 3 ) {
                $check = false;
                $user->tutorial  = true;
                $user->save();
            }
            if ($check) {
                $data_user = [
                  'name' => $user->name,
                  'subject' => 'Trải Nghiệm Tốt Hơn Với Nhiều Chức Năng Mới Cập Nhật Trên Web Portal'
                ];

                $data_tb_send_mail = [
                  'type' => 'mail_update_feature',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data_user),
                ];
                $this->send_mail->create($data_tb_send_mail);

                $user->tutorial  = true;
                $user->save();
            }
          } catch (\Throwable $th) {
              //throw $th;
              report($th);
              $user->tutorial  = true;
              $user->save();
          }
        }
    }

    public function send_mail_marketing()
    {
        $list_users = $this->user->whereMarketing(false)->take(200)->get();
        foreach ($list_users as $key => $user) {
          try {
              $data_user = [
                  'name' => $user->name,
                  'subject' => 'HƯỚNG DẪN SỬ DỤNG TÀI KHOẢN WEB PORTAL'
              ];

              $data_tb_send_mail = [
                  'type' => 'mail_marketing_user',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data_user),
              ];
              $this->send_mail->create($data_tb_send_mail);

              $user->marketing  = true;
              $user->save();
          } catch (\Throwable $th) {
              //throw $th;
              report($th);
          }
        }
    }

    public function send_mail_marketing_tiny()
    {
        $list_users = $this->user->whereMarketing(false)->take(200)->get();
        foreach ($list_users as $key => $user) {
          try {
              $create_at = new Carbon($user->created_at);
              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
              $now = new Carbon($now);
              if ( $now->diffInDays($create_at) > 2 ) {
                  $data_user = [
                      'name' => $user->name,
                      'subject' => 'Chỉ với 70k/tháng sở hữu ngay gói VPS Tiny'
                  ];

                  $data_tb_send_mail = [
                      'type' => 'mail_marketing_tiny',
                      'user_id' => $user->id,
                      'status' => false,
                      'content' => serialize($data_user),
                  ];
                  $this->send_mail->create($data_tb_send_mail);

                  $user->marketing  = true;
                  $user->save();
              }
          } catch (\Throwable $th) {
              //throw $th;
              report($th);
          }
        }
    }

    public function mail_marketing_template()
    {
        $list_email = $this->email->where('type', 2)->where('status', true)->get();
        foreach ($list_email as $key => $email) {
            if ( !empty($email->mail_marketing) ) {
                $mail_marketing = $email->mail_marketing;
            }
            else {
                $mail_marketing = $this->mail_marketing->create([ 'email_id' => $email->id, 'qtt' => 0 ]);
            }
            $check_time = false;
            if ( !empty($email->meta_email->date) ) {
                $time = time();
                $email_time = $email->meta_email->date . ' ';
                $email_time .= !empty($email->meta_email->time) ? $email->meta_email->time : '0:0';
                // dd($time > strtotime($email_time) ? '1' : '2');
                if ( $time > strtotime($email_time) ) {
                    $check_time = true;
                }
            } else {
                $check_time = true;
            }
            // dd($check_time);
            if ( $check_time ) {
                $list_user = $this->user->where('id', '>', $mail_marketing->qtt)->take(200)->get();
                if ($list_user->count()) {
                    $check_date = true;
                    $qtt = $mail_marketing->qtt;
                    foreach ($list_user as $key => $user) {
                        if ( $user->id != 1 ) {
                            try {
                                $check = false;
                                if ( !empty($email->meta_email->type_group) ) {
                                    foreach ($email->email_group_users as $key => $email_group_user) {
                                        if ( $email_group_user->group_user_id == $user->group_user_id ) {
                                            $check = true;
                                        }
                                    }
                                } else {
                                    $check = true;
                                }
                                if ( $check ) {
                                    if ( !empty($user->send_mail_marketing) ) {
                                        $now = time();
                                        $time_send_mail_marketing = strtotime( $user->send_mail_marketing );
                                        $one_day = 24 * 60 * 60;
                                        if ( $now - $time_send_mail_marketing > $one_day ) {
                                            $url = url('');
                                            $url = str_replace(['http://','https://'], ['', ''], $url);

                                            $address = !empty($user->user_meta->address) ? $user->user_meta->address : '';
                                            $phone = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';

                                            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$url}'];
                                            $replace = [ $user->id , $user->name,  $user->email , $address , $phone, $url ];
                                            $content = str_replace($search, $replace, $email->meta_email->content);

                                            $data_user = [
                                                'name' => $user->name,
                                                'subject' => $email->meta_email->subject,
                                                'content' => $content,
                                            ];

                                            $data_tb_send_mail = [
                                                'type' => 'mail_template_marketing',
                                                'user_id' => $user->id,
                                                'status' => false,
                                                'content' => serialize($data_user),
                                            ];
                                            $this->send_mail->create($data_tb_send_mail);

                                            $user->send_mail_marketing = date('Y-m-d H-i-s');
                                            $user->save();
                                            $qtt = $user->id;
                                        } else {
                                            $check_date = false;
                                            break;
                                        }
                                    } else {
                                        $url = url('');
                                        $url = str_replace(['http://','https://'], ['', ''], $url);

                                        $address = !empty($user->user_meta->address) ? $user->user_meta->address : '';
                                        $phone = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';

                                        $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$url}'];
                                        $replace = [ $user->id , $user->name,  $user->email , $address , $phone, $url ];
                                        $content = str_replace($search, $replace, $email->meta_email->content);

                                        $data_user = [
                                            'name' => $user->name,
                                            'subject' => $email->meta_email->subject,
                                            'content' => $content,
                                        ];

                                        $data_tb_send_mail = [
                                            'type' => 'mail_template_marketing',
                                            'user_id' => $user->id,
                                            'status' => false,
                                            'content' => serialize($data_user),
                                        ];
                                        $this->send_mail->create($data_tb_send_mail);

                                        $user->send_mail_marketing = date('Y-m-d H-i-s');
                                        $user->save();
                                        $qtt = $user->id;
                                    }
                                }
                            } catch (\Exception $e) {
                                report($e);
                            }
                        }

                    }
                    try {
                      $mail_marketing->qtt = $qtt;
                      $mail_marketing->save();
                      // dd($mail_marketing, $qtt);
                    } catch (\Exception $e) {
                      report($e);
                    }
                }
            }
        }
    }

    public function send_mail_marketing_vps_uk()
    {
        $list_users = $this->user->whereMarketing(false)->take(200)->get();
        foreach ($list_users as $key => $user) {
          try {
              $check = true;
              if ( $user->group_user_id == 3 ) {
                $check = false;
                $user->marketing  = true;
                $user->save();
              }
              if ( $check ) {
                $create_at = new Carbon($user->created_at);
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $now = new Carbon($now);
                if ( $now->diffInDays($create_at) > 2 ) {
                    $data_user = [
                      'name' => $user->name,
                      'subject' => 'Chỉ với 100k/tháng, sở hữu ngay gói Vps UK 1-1-20'
                    ];

                    $data_tb_send_mail = [
                      'type' => 'mail_marketing_vps_uk',
                      'user_id' => $user->id,
                      'status' => false,
                      'content' => serialize($data_user),
                    ];
                    $this->send_mail->create($data_tb_send_mail);

                    $user->marketing  = true;
                    $user->save();
                }
              }
          } catch (\Throwable $th) {
              //throw $th;
              report($th);
              $user->marketing  = true;
              $user->save();
          }
        }
    }

    public function reset_mail_marketing()
    {
        $list_users = $this->user->get();
        foreach ($list_users as $key => $user) {
          $user->marketing  = false;
          $user->save();
        }
    }

}
