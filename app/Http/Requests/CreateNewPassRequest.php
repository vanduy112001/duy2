<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewPassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'new_password'    => 'bail|required|min:6|max:64|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&"*()\-_=+{};:,<.>]).{8,255}+$/',
            'password_confirm' => 'bail|required|same:new_password'
        ];
    }

    public function messages()
    {
        return [
            'new_password.required' => 'Mật khẩu mới không được để trống',
            'new_password.min' => 'Mật khẩu mới không được nhỏ hơn 6 kí tự',
            'new_password.max' => 'Mật khẩu mới không được vượt quá 64 kí tự',
            'new_password.regex' => 'Mật khẩu mới phải có ký tự chữ số, chữ hoa, chữ thường và ký tự đặc biệt',
            'password_confirm.required' => 'Nhập lại mật khẩu mới không được để trống',
            'password_confirm.same' => 'Nhập lại mật khẩu mới không đúng',
        ];
    }
}
