<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PromotionValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:50',
            'code' => 'bail|required|max:50',
            'type' => 'bail|required|not_in:0',
            'value' => 'bail|required',
            'start_date' => 'bail|required',
            'end_date' => 'bail|required',
            'max_uses' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề không được để trống',
            'name.max' => 'Số ký tự của tiêu đề phải lớn hơn :max',
            'code.required' => 'Mã khuyến mãi không được để trống',
            'code.max' => 'Số ký tự của mã khuyến mãi phải lớn hơn :max',
            'type.required' => 'Loại mã khuyến mãi không được để trống',
            'type.not_in' => 'Loại mã khuyến mãi không được để trống',
            'value.required' => 'Giá trị không được để trống',
            'start_date.required' => 'Ngày bắt đầu không được để trống',
            'end_date.required' => 'Ngày kết thúc không được để trống',
            'max_uses.required' => 'Số lượng sử dụng không được để trống',
        ];
    }

}
