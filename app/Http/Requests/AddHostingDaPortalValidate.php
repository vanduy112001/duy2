<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddHostingDaPortalValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'bail|required',
            'product_id' => 'bail|required',
            'billing_cycle' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Lỗi không chọn khách hàng',
            'product_id.required' => 'Lỗi không chọn sản phẩm',
            'billing_cycle.required' => 'Lỗi không chọn thời gian thuê',
        ];
    }

}
