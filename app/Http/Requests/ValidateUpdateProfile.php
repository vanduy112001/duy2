<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateUpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|min:2|max:20|regex:/(^[\pL0-9 ]+)$/u',
            'phone' => 'numeric',
            'address' => 'max:200',
            'email' => 'bail|email|max:255|unique:users,email,'.$this->id
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name không được để trống',
            'name.min' => 'Name không được nhỏ hơn :min ký tự',
            'name.max' => 'Name không được lớn hơn :max ký tự',
            'name.regex' => 'Không được đặt tên với ký tự đặt biệt',
            'phone.numeric' => 'Số điện thoại phải là ký tự số',
            'address.max' => 'Địa chỉ không được lớn hơn :max ký tự',
            'email.email' => 'Email không đúng định dạng',
            'email.max' => 'Email không được lớn hơn :max ký tự',
            'email.unique' => 'Email đã được đăng ký'
        ];
    }
}

