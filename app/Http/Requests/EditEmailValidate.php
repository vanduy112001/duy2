<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditEmailValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|min:3',
            'subject' => 'bail|required|min:6|max:200',
            'content' => 'bail|required|min:20'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tiều đề không được để trống',
            'name.min' => 'Tổng số ký tự của tiêu đề không được nhỏ hơn :min',
            'subject.required' => 'Subject không được để trống',
            'subject.min' => 'Tông số ký tự của subject không được nhỏ hơn :min',
            'subject.max' => 'Tông số ký tự của subject không được lớn hơn :max',
            'content.required' => 'Nội dung không được để trống',
            'content.min' => 'Tông số ký tự của nội dung không được nhỏ hơn :min',
        ];
    }
}
