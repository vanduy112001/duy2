<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmailServiceValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required',
            'subject' => 'bail|required',
            'content' => 'bail|required',
            'type_service' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tiêu đề không được để trống',
            'subject.required' => 'Subject không được để trống',
            'content.required' => 'Nội dung không được để trống',        
            'type_service.required' => 'Loại email không được để trống',        
        ];
    }

}
