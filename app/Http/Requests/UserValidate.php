<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required',
            'email'    => 'bail|required|email|unique:users',
            'password' => 'bail|required|min:6|confirmed',
            'password_confirmation' => 'bail|required|same:password',
            'phone'  => 'bail|min:10|numeric',
        ];
    }

    public function messages()
    {
        return [
            'name.required'     =>  'Tên không được để trống',
            'email.required'    =>  'Email không được để trống',
            'email.email'       =>  'Email không đúng định dạng',
            'email.unique' =>  'Email đã có người sử dụng',
            'password.required' =>  'Mật khẩu không được để trống',
            'password.min'    =>  'Mật khẩu không được ngắn hơn 6 ký tự',
            'password_confirmation.required' => 'Nhập lại mật khẩu không được để trống',
            'password_confirmation.same' => 'Nhập lại mật khẩu không đúng',
            'phone.numeric'     =>  'Số điện thoại phải là ký tự số',
            'phone.min'     =>  'Số điện thoại không đúng',
        ];
    }
}
