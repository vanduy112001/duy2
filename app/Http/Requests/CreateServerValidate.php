<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateServerValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
          'user_id' => 'bail|required|not_in:0',
          'product_id' => 'bail|required|not_in:0',
          'billing_cycle' => 'bail|required',
          'ip' => 'bail|required',
          'os_server' => 'bail|required|not_in:0',
      ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Lỗi không chọn khách hàng',
            'user_id.not_in' => 'Lỗi không chọn khách hàng',
            'product_id.required' => 'Lỗi không chọn sản phẩm',
            'billing_cycle.required' => 'Lỗi không chọn thời gian thuê',
            'ip.required' => 'Lỗi không nhập trường IP',
            'os_server.required' => 'Lỗi không chọn hệ điều hành',
            'os_server.not_in' => 'Lỗi không chọn hệ điều hành',
        ];
    }
}
