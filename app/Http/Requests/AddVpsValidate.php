<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddVpsValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'bail|required',
            'product_id' => 'bail|required',
            'ip' => 'bail|required',
            'sevices_username' => 'bail|required',
            'sevices_password' => 'bail|required',
            'status' => 'bail|required',
            'os' => 'bail|required',
            'date_create' => 'bail|required',
            'created_at' => 'bail|required',
            'billing_cycle' => 'bail|required',
            'vm_id' => 'bail|required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Lỗi không chọn khách hàng',
            'product_id.required' => 'Lỗi không chọn sản phẩm',
            'billing_cycle.required' => 'Lỗi không chọn thời gian thuê',
            'ip.required' => 'IP không được để trống',
            'sevices_username.required' => 'User không được để trống',
            'sevices_password.required' => 'Password không được để trống',
            'status.required' => 'Lỗi không chọn trạng thái',
            'os.required' => 'Lỗi không chọn hệ điều hành',
            'date_create.required' => 'Ngày đặt hàng không được để trống',
            'created_at.required' => 'Ngày tạo không được để trống',
            'next_due_date.required' => 'Ngày kết thúc không được để trống',
            'total.required' => 'Thành tiền không được để trống',
            'vm_id.required' => 'VM ID không được để trống',
        ];
    }

}
