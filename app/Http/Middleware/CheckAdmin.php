<?php

namespace App\Http\Middleware;

use App\Factories\AdminFactories;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $userRepo = AdminFactories::loginRepositories();
            $user = $userRepo->detail(Auth::user()->id);
            if(isset($user)) {
                if(!empty($user->user_meta)) {
                    if ($user->user_meta->role == 'admin') {
                        return $next($request);
                    } else {
                        redirect(route('index'));
                    }
                }
            }
            return redirect(route('index'));
        } catch (\Throwable $th) {
            return redirect(route('admin.login'));
        }

    }
}
