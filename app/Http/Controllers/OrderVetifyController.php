<?php

namespace App\Http\Controllers;

use App\Model\OrderVetify;
use Illuminate\Http\Request;

class OrderVetifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OrderVetify  $orderVetify
     * @return \Illuminate\Http\Response
     */
    public function show(OrderVetify $orderVetify)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OrderVetify  $orderVetify
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderVetify $orderVetify)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OrderVetify  $orderVetify
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderVetify $orderVetify)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OrderVetify  $orderVetify
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderVetify $orderVetify)
    {
        //
    }
}
