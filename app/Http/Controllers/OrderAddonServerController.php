<?php

namespace App\Http\Controllers;

use App\Model\OrderAddonServer;
use Illuminate\Http\Request;

class OrderAddonServerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OrderAddonServer  $orderAddonServer
     * @return \Illuminate\Http\Response
     */
    public function show(OrderAddonServer $orderAddonServer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OrderAddonServer  $orderAddonServer
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderAddonServer $orderAddonServer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OrderAddonServer  $orderAddonServer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderAddonServer $orderAddonServer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OrderAddonServer  $orderAddonServer
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderAddonServer $orderAddonServer)
    {
        //
    }
}
