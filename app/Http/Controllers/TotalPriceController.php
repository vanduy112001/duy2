<?php

namespace App\Http\Controllers;

use App\Model\TotalPrice;
use Illuminate\Http\Request;

class TotalPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\TotalPrice  $totalPrice
     * @return \Illuminate\Http\Response
     */
    public function show(TotalPrice $totalPrice)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\TotalPrice  $totalPrice
     * @return \Illuminate\Http\Response
     */
    public function edit(TotalPrice $totalPrice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\TotalPrice  $totalPrice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TotalPrice $totalPrice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\TotalPrice  $totalPrice
     * @return \Illuminate\Http\Response
     */
    public function destroy(TotalPrice $totalPrice)
    {
        //
    }
}
