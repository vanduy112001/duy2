<?php

namespace App\Http\Controllers\User;

use App\CmndVerify;
use Illuminate\Http\Request;

class CmndVerifyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CmndVerify  $cmndVerify
     * @return \Illuminate\Http\Response
     */
    public function show(CmndVerify $cmndVerify)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CmndVerify  $cmndVerify
     * @return \Illuminate\Http\Response
     */
    public function edit(CmndVerify $cmndVerify)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CmndVerify  $cmndVerify
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CmndVerify $cmndVerify)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CmndVerify  $cmndVerify
     * @return \Illuminate\Http\Response
     */
    public function destroy(CmndVerify $cmndVerify)
    {
        //
    }
}
