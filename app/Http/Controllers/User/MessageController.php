<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;

class MessageController extends Controller
{
    protected $message;

    public function __construct()
    {
        $this->message = UserFactories::messageRepositories();
    }

    public function loadChat()
    {
    	$list_message = $this->message->load_chat_by_user();
    	return json_encode($list_message);
    }

    public function sendMessage(Request $request)
    {
    	$sendMessage = $this->message->sendMessage($request->all());
    	return json_encode($sendMessage);
    }
}
