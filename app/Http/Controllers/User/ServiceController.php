<?php

namespace App\Http\Controllers\User;

use App\Helpers\GroupProduct;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use Illuminate\Support\Facades\Auth;
use Hash;
use Excel;
use App\Exports\ExportVpsUs;

class ServiceController extends Controller
{
    protected $service;
    protected $user_repo;

    public function __construct()
    {
        $this->service = UserFactories::serviceRepositories();
        $this->user_repo = UserFactories::userRepositories();
    }

    public function index()
    {
        $list_vps = $this->service->list_vps();
        $servers = $this->service->list_server();
        $hostings = $this->service->list_hosting();
        $billing = config('billing');
        return view('users.services.all', compact('list_vps', 'servers', 'hostings', 'billing'));
    }

    public function on()
    {
        $list_vps = $this->service->list_vps_on();
        $servers = $this->service->list_server_on();
        $hostings = $this->service->list_hosting_on();
        $billing = config('billing');
        
        return view('users.services.index', compact('list_vps', 'servers', 'hostings', 'billing'));
    }

    public function vps_on()
    {
         
        $list_vps = $this->service->list_vps_on();
   //     return $list_vps;
        $billing = config('billing');
        return view('users.services.list_vps_on', compact('list_vps' , 'billing'));
    }

    public function vps_use()
    {
        $list_vps = $this->service->list_vps_use();
        $billing = config('billing');
        return view('users.services.list_vps_use', compact('list_vps' , 'billing'));
    }

    public function vps_us_on()
    {
        $list_vps = $this->service->list_vps_us_on();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_on', compact('list_vps' , 'billing'));
    }

    public function vps_us_use()
    {
        $list_vps = $this->service->list_vps_us_use();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_use', compact('list_vps' , 'billing'));
    }

    public function hosting_use()
    {
        $hostings = $this->service->list_hosting_use();
        $billing = config('billing');
        return view('users.services.list_hosting_use', compact('hostings' , 'billing'));
    }

    public function hosting_on()
    {
        $hostings = $this->service->list_hosting_on();
        $billing = config('billing');
        return view('users.services.list_hosting_on', compact('hostings' , 'billing'));
    }

    public function email_hosting_on()
    {
        $email_hostings = $this->service->list_email_hosting_on();
        $billing = config('billing');
        return view('users.services.email_hosting.list_email_hosting_on', compact('email_hostings' , 'billing'));
    }

    public function server_use()
    {
        $servers = $this->service->list_server_use();
        $billing = config('billing');
        return view('users.services.list_server_use', compact('servers' , 'billing'));
    }

    public function server_on()
    {
        $servers = $this->service->list_server_on();
        $billing = config('billing');
        return view('users.services.list_server_on', compact('servers' , 'billing'));
    }

    public function colocation_use()
    {
        $list_colocations = $this->service->list_colocation_use();
        $billing = config('billing');
        return view('users.services.colocation.list_colo_use', compact('list_colocations' , 'billing'));
    }

    public function colocation_on()
    {
        $list_colocations = $this->service->list_colocation_on();
        $billing = config('billing');
        return view('users.services.colocation.list_colo_on', compact('list_colocations' , 'billing'));
    }

    public function vps_nearly()
    {
        $list_vps = $this->service->list_vps_nearly();
        $billing = config('billing');
        return view('users.services.list_vps_nearly', compact('list_vps' , 'billing'));
    }

    public function vps_cancel()
    {
        $list_vps = $this->service->list_vps_cancel();
        $billing = config('billing');
        return view('users.services.list_vps_cancel', compact('list_vps' , 'billing'));
    }

    public function vps_nearly_and_expire()
    {
        return view('users.services.list_vps_nearly_and_expire');
    }

    public function list_vps_nearly_and_expire()
    {
        $data = $this->service->list_vps_nearly_and_expire();
        return json_encode($data);
    }

    public function vps_us_nearly()
    {
        $list_vps = $this->service->list_vps_us_nearly();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_nearly', compact('list_vps' , 'billing'));
    }

    public function vps_us_cancel()
    {
        $list_vps = $this->service->list_vps_us_cancel();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_cancel', compact('list_vps' , 'billing'));
    }

    public function vps_us_nearly_and_expire()
    {
        return view('users.services.vps_us.list_vps_us_nearly_and_expire');
    }

    public function list_vps_us_nearly_and_expire()
    {
        $data = $this->service->list_vps_us_nearly_and_expire();
        return json_encode($data);
    }

    public function server_nearly()
    {
        $servers = $this->service->list_server_nearly();
        $billing = config('billing');
        return view('users.services.list_server_nearly', compact('servers' , 'billing'));
    }

    public function server_cancel()
    {
        $servers = $this->service->list_server_cancel();
        $billing = config('billing');
        return view('users.services.list_server_cancel', compact('servers' , 'billing'));
    }

    public function server_nearly_and_expire()
    {
        return view('users.services.list_server_nearly_and_expire');
    }

    public function list_server_nearly_and_expire()
    {
      $data = $this->service->list_server_nearly_and_expire();
      return json_encode($data);
    }

    public function colocation_nearly()
    {
      // dd('da den');
        $list_colocations = $this->service->list_colocation_nearly();
        $billing = config('billing');
        return view('users.services.colocation.list_colo_nearly', compact('list_colocations' , 'billing'));
    }

    public function colocation_cancel()
    {
      // dd('da den');
        $list_colocations = $this->service->list_colocation_cancel();
        $billing = config('billing');
        return view('users.services.colocation.list_colocation_cancel', compact('list_colocations' , 'billing'));
    }

    public function colocation_nearly_and_expire()
    {
      // dd('da den');
        return view('users.services.colocation.list_colocation_nearly_and_expire');
    }

    public function list_colocation_nearly_and_expire()
    {
      $data = $this->service->list_colocation_nearly_and_expire();
      return json_encode($data);
    }

    public function email_hosting_nearly()
    {
      // dd('da den');
        $email_hostings = $this->service->list_email_hosting_nearly();
        $billing = config('billing');
        return view('users.services.email_hosting.list_email_hosting_nearly', compact('email_hostings' , 'billing'));
    }

    public function email_hosting_nearly_and_expired()
    {
      // dd('da den');
        return view('users.services.email_hosting.list_email_hosting_nearly_and_expired');
    }

    public function hosting_nearly()
    {
        $hostings = $this->service->list_hosting_nearly();
        $billing = config('billing');
        return view('users.services.list_hosting_nearly', compact('hostings' , 'billing'));
    }

    public function hosting_cancel()
    {
        $hostings = $this->service->list_hosting_cancel();
        $billing = config('billing');
        return view('users.services.list_hosting_cancel', compact('hostings' , 'billing'));
    }

    public function list_hosting_nearly_and_expire()
    {
        $email_hostings = $this->service->list_hosting_nearly_and_expire();
        return json_encode($email_hostings);
    }

    public function list_email_hosting_nearly_and_expired()
    {
        $email_hostings = $this->service->list_email_hosting_nearly_and_expired();
        return json_encode($email_hostings);
    }

    public function hosting_nearly_and_expire()
    {
        return view('users.services.list_hosting_nearly_and_expire');
    }

    public function vps_all()
    {
       $list_vps = $this->service->vps_all();
       $billing = config('billing');
       return view('users.services.list_vps_all', compact('list_vps' , 'billing'));
    }

    public function vps_us_all()
    {
       $list_vps = $this->service->vps_us_all();
       $billing = config('billing');
       return view('users.services.vps_us.list_vps_us_all', compact('list_vps' , 'billing'));
    }

    public function server_all()
    {
       $servers = $this->service->server_all();
       $billing = config('billing');
       return view('users.services.list_server_all', compact('servers' , 'billing'));
    }

    public function colocation_all()
    {
       $list_colocations = $this->service->colocation_all();
       $billing = config('billing');
       return view('users.services.colocation.list_colo_all', compact('list_colocations' , 'billing'));
    }

    public function email_hosting_all()
    {
       $email_hostings = $this->service->list_email_hosting();
       $billing = config('billing');
       return view('users.services.email_hosting.list_email_hosting_all', compact('email_hostings' , 'billing'));
    }

    public function hosting_all()
    {
       $hostings = $this->service->hosting_all();
       $billing = config('billing');
       return view('users.services.list_hosting_all', compact('hostings' , 'billing'));
    }

    public function pending()
    {
        $list_vps = $this->service->list_vps_pending();
        $servers = $this->service->list_server_pending();
        $hostings = $this->service->list_hosting_pending();
        return view('users.services.index', compact('list_vps', 'servers', 'hostings'));
    }

    public function servive_nearly()
    {
        $list_vps = $this->service->list_vps_nearly();
        $servers = $this->service->list_server_pending();
        $hostings = $this->service->list_hosting_nearly();
        $billing = config('billing');
        return view('users.services.nearly', compact('list_vps', 'servers', 'hostings', 'billing'));
    }

    public function detail($id, Request $request)
    {
        $type = $request->get('type');
        $billings = config('billing');
        $list_os = config('listOs');
        $payment_methods = config('payment_method');
        switch ($type){
            case 'vps':
                $detail = $this->service->detail_vps($id);
                $log_payments = $this->service->log_payment_with_vps($id);
                $log_activities = $this->service->log_activities_with_vps($id);
                break;

            case 'hosting':
                $detail = $this->service->detail_hosting($id);
                $log_payments = $this->service->log_payment_with_hosting($id);
                $log_activities = $this->service->log_activities_with_hosting($id);
                break;

            case 'server':
                $detail = $this->service->detail_server($id);
                $log_payments = $this->service->log_payment_with_server($id);
                $log_activities = $this->service->log_activities_with_server($id);
                break;

            case 'proxy':
                $detail = $this->service->detail_proxy($id);
                $log_payments = $this->service->log_payment_with_proxy($id);
                $log_activities = $this->service->log_activities_with_proxy($id);
                break;
        }
        // dd('da den', $detail);
        $isExpired = false;
        if(!empty($detail)){
            if(!empty($detail->next_due_date) && date('Y-m-d',strtotime($detail->next_due_date)) < date('Y-m-d')){
                $isExpired = true;
            }
        } else {
           return redirect()->route('service.index')->with('fails', 'Dịch vụ này không phải của quý khách');
        }

        return view('users.services.detail', compact('detail', 'type', 'billings', 'payment_methods','isExpired', 'log_payments', 'log_activities', 'list_os'));
    }

    public function server_detail($id)
    {
        $type = 'server';
        $detail = $this->service->detail_server($id);
        $billings = config('billing');
        $payment_methods = config('payment_method');
        $isExpired = false;
        if(!empty($detail)){
            if(!empty($detail->next_due_date) && date('Y-m-d',strtotime($detail->next_due_date)) < date('Y-m-d')){
                $isExpired = true;
            }
        } else {
           return redirect()->route('service.servers.on')->with('fails', 'Dịch vụ này không phải của quý khách');
        }
        $log_payments = $this->service->log_payment_with_server($id);
        $log_activities = $this->service->log_activities_with_server($id);
        return view('users.services.detail_server', compact('detail', 'type', 'billings', 'payment_methods','isExpired', 'log_payments', 'log_activities'));
    }

    public function colocation_detail($id)
    {
        $type = 'colocation';
        $detail = $this->service->colocation_detail($id);
        $billings = config('billing');
        $payment_methods = config('payment_method');
        $isExpired = false;
        $log_payments = $this->service->log_payment_with_colocation($id);
        $log_activities = $this->service->log_activities_with_colocation($id);
        if(!empty($detail)){
            if(!empty($detail->next_due_date) && date('Y-m-d',strtotime($detail->next_due_date)) < date('Y-m-d')){
                $isExpired = true;
            }
        } else {
           return redirect()->route('service.colocation.on')->with('fails', 'Dịch vụ này không phải của quý khách');
        }

        return view('users.services.colocation.detail', compact('detail', 'type', 'billings', 'payment_methods','isExpired', 'log_payments', 'log_activities'));
    }

    public function email_hosting_detail($id)
    {
        $type = 'email_hosting';
        $detail = $this->service->email_hosting_detail($id);
        $billings = config('billing');
        $payment_methods = config('payment_method');
        $isExpired = false;
        if(!empty($detail)){
            if(!empty($detail->next_due_date) && date('Y-m-d',strtotime($detail->next_due_date)) < date('Y-m-d')){
                $isExpired = true;
            }
        } else {
           return redirect()->route('service.email_hosting.on')->with('fails', 'Dịch vụ này không phải của quý khách');
        }

        return view('users.services.email_hosting.detail', compact('detail', 'type', 'billings', 'payment_methods','isExpired'));
    }
    /**
     * Gia hạn
     */
    public function extend($id,Request $request)
    {
        $user = Auth::user();
        $method_pay_in = config('pay_in');

        $type = $request->get('type');
        switch ($type){
            case 'vps':
                $detail = $this->service->detail_vps($id);
                break;

            case 'hosting':
                $detail = $this->service->detail_hosting($id);

                break;

            case 'server':
                $detail = $this->service->detail_server($id);

                break;
        }

        $product = GroupProduct::get_product($detail->product_id);
        $billings = config('billing');
        $payment_methods = config('payment_method');
        return view('users.services.extend',compact('user','method_pay_in','type','detail','product','billings'));
    }

    /**
     * Hủy dịch vụ
    */
    public function terminated(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');
        switch ($type) {
          case 'hosting':
            $terminated = $this->service->terminated_hosting($id);
            break;

          default:
            return json_encode(false);
        }
        return json_encode($terminated);
    }

    public function cancel_sevices(Request $request)
    {
        $type = $request->get('type');
        // return json_encode($type);
        $id = $request->get('id');
        switch ($type) {
          case 'hosting':
            $terminated = $this->service->terminated_hosting($id);
            break;

          case 'vps':
            $terminated = $this->service->terminated_vps($id);
            break;
          case 'server':
            $terminated = $this->service->terminatedServer($id);
            break;
          case 'proxy':
            $list_id = [ $id ];
            $terminated = $this->service->terminatedProxy($list_id);
            break;

          default:
            return json_encode(false);
        }
        return json_encode($terminated);
    }

    /**
     * Hành động trong trang index services
    */
    public function action_services(Request $request)
    {
        $type = $request->get('type');
        $list_id = $request->get('id');
        $action = $request->get('action');
        switch ($type) {
            // Hosting
          case 'hosting':
            $check_hostings_with_user = $this->service->check_hostings_with_user($list_id);
            if ($check_hostings_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
                  case 'on':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->service->unSuspend($id);
                          if (!empty($action_services['error'])) {
                              $hosting = $this->service->detail_hosting($id);
                              return json_encode($data['error'] = 'Hosting ' . $hosting->domain . ' do Admin Cloudzone suspend nên không thể Unsuspend này được. Vui lòng liên hệ với chúng tôi để mở lại Hosting.');
                          }
                      }
                      return json_encode(true);
                    break;
                  case 'off':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->service->suspend_hosting($id);
                          if (!$action_services) {
                              return json_encode(false);
                          }
                      }
                      return json_encode(true);
                    break;
                  case 'delete':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->service->terminated_hosting($id);
                          if (!$action_services) {
                              return json_encode(false);
                          }
                      }
                      return json_encode(true);
                    break;

                  case 'expired':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      $billing_cycle = $request->get('billing_cycle');
                      $action_services = $this->service->expired_hostings($list_id, $billing_cycle);
                      return json_encode($action_services);
                    break;

                  default:
                    return json_encode(false);
                    break;
            }
            break;
          // VPS
          case 'vps':
            $check_vps_with_user = $this->service->check_list_vps_with_user($list_id);
            if ($check_vps_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
              case 'on':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->onVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->offVPS($id);
                        if ( empty($action_services['success']) ) {
                            return json_encode($action_services);
                        }
                        $li[] = $id;
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                        'text' => $li
                    ];
                    return json_encode($data_status);
                break;
              case 'delete':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->terminatedVps($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'auto_refurn':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->autoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'restart':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->restartVPS($id);
                        if (!empty($action_services)) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'expired':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->service->gia_han_nhieu_vps($list_id, $billing_cycle);
                    return json_encode($expired);
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'change_ip':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $change_ip = $this->service->request_change_ip($list_id);
                    return json_encode($change_ip);
                break;
            break;
          }
          // VPS US
          case 'vps_us':
            $check_vps_with_user = $this->service->check_list_vps_with_user($list_id);
            if ($check_vps_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
              case 'on':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->onVPSUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->offVPSUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'delete':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->terminatedVpsUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'auto_refurn':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->autoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off_auto_refurn':
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->service->offAutoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'expired':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->service->gia_han_nhieu_vps($list_id, $billing_cycle);
                    return json_encode($expired);
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'change_ip':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $expired = $this->service->change_ip_vps_us($list_id);
                    return json_encode($expired);
                break;
              case 'reset_password':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $expired = $this->service->reset_password($list_id);
                    return json_encode($expired);
                break;

            break;
          }
          // PROXY
          case 'proxy':
            $list_id = $request->get('id');
            $check_proxy_with_user = $this->service->check_list_proxy_with_user($list_id);
            if ( $check_proxy_with_user ) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
                case 'expired':
                    $expired = $this->service->expired_list_proxy($list_id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                    break;
                case 'delete':
                    $expired = $this->service->terminatedProxy($list_id);
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                    break;
                default:
                    return json_encode(false);
            }
            break;
          default:
            return json_encode(false);
        }
        return json_encode(true);

    }

    //loadStatusVPS
    public function loadStatusVPS(Request $request)
    {
        $ids = $request->get('ids');
        $list_vps = $this->service->loadVpsWithIds($ids);
        return json_encode($list_vps);
    }
    // login_directadmin
    public function login_directadmin($id)
    {
        $hosting = $this->service->detail_hosting($id);
        if ($hosting->location == 'vn') {
            if (!empty($hosting->server_hosting_id)) {
                $server_hosting = $hosting->server_hosting;
                $url = "https://" . $server_hosting->host . ':' . $server_hosting->port;
                // dd($url);
                return redirect($url);
            } else {
                return redirect('https://daportal.cloudzone.vn:2222/');
            }
        }
        elseif ($hosting->location == 'si') {
            if (!empty($hosting->server_hosting_id)) {
                $server_hosting = $hosting->server_hosting;
                $url = "https://" . $server_hosting->host . ':' . $server_hosting->port;
                // dd($url);
                return redirect($url);
            } else {
                return redirect('https://singnode-01.cloudzone.vn:2222/');
            }
        }

    }
    // Hanh dong cua 1 dich vu
    public function action_one_service(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        switch ($type) {
          // Hosting
            case 'hosting':
                $check_hosting_with_user = $this->service->check_hosting_with_user($id);
                if ($check_hosting_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->service->expired_hosting($id, $billing_cycle);
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'suspend') {
                    $suspend = $this->service->suspend_hosting($id);
                    if ($suspend) {
                        return json_encode(true);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'unsuspend') {
                    $unsuspend = $this->service->unSuspend($id);
                    if ($unsuspend) {
                        return json_encode($unsuspend);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'terminated') {
                    $unsuspend = $this->service->terminated_hosting($id);
                    if ($unsuspend) {
                        return json_encode(true);
                    }
                    return json_encode(false);
                }
                break;

            case 'vps':
                $check_vps_with_user = $this->service->check_vps_with_user($id);
                if ($check_vps_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->service->expired_vps($id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'on') {
                    $on = $this->service->onVPS($id);
                    if (!empty($on['error'])) {
                        return json_encode($on['error']);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'off') {
                    $off = $this->service->offVPS($id);
                    if (!empty($off['error'])) {
                        return json_encode($off);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'restart') {
                    $restart = $this->service->restartVPS($id);
                    if (!empty($restart['error'])) {
                        return json_encode($restart);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->service->terminatedVps($id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;

            case 'vps_us':
                $check_vps_with_user = $this->service->check_vps_with_user($id);
                if ($check_vps_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->service->expired_vps($id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'on') {
                    $on = $this->service->onVPSUS($id);
                    if (!empty($on['error'])) {
                        return json_encode($on['error']);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'off') {
                    $off = $this->service->offVPSUS($id);
                    if (!empty($off['error'])) {
                        return json_encode($off);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'restart') {
                    $restart = $this->service->restartVPSUS($id);
                    $data_status = [
                        'error' => '',
                        'success' => true
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->service->terminatedVpsUS($id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;
            case 'proxy':
                $id = $request->get('id');
                $list_id = [ $id ];
                $check_proxy_with_user = $this->service->check_list_proxy_with_user($list_id);
                if ( $check_proxy_with_user ) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->service->expired_list_proxy($list_id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->service->terminatedProxy($list_id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;
        }
        return json_encode(false);
    }

    // Hanh dong cua 1 dich vu server
    public function action_server(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_server_with_user = $this->service->check_server_with_user($id);
        if ($check_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_list_server([$id] , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'on') {
           // $on = $this->service->onVPS($id);
           // if (!empty($on['error'])) {
           //     return json_encode($on['error']);
           // }
           // $data_status = [
           //     'error' => '',
           //     'success' => true,
           // ];
           // return json_encode($data_status);
        }
        elseif ($action == 'off') {
            // $off = $this->service->offVPS($id);
            // if (!empty($off['error'])) {
            //     return json_encode($off);
            // }
            // $data_status = [
            //     'error' => '',
            //     'success' => true,
            // ];
            // return json_encode($data_status);
        }
        elseif ($action == 'restart') {
            // $restart = $this->service->restartVPS($id);
            // if (!empty($restart['error'])) {
            //     return json_encode($restart);
            // }
            // $data_status = [
            //     'error' => '',
            //     'success' => true,
            // ];
            // return json_encode($data_status);
        }
        elseif ($action == 'terminated') {
            $terminated = $this->service->terminatedServer($id);
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    // Hanh dong cua nhieu dich vu server
    public function action_server_services(Request $request)
    {
        $list_id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_list_server_with_user = $this->service->check_list_server_with_user($list_id);
        if ($check_list_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_list_server($list_id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'delete') {
            $terminated = $this->service->terminatedServers($list_id);
            // $terminated = false;
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    // Hanh dong cua 1 dich vu colocation
    public function action_colocation(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_colocation_with_user = $this->service->check_colocation_with_user($id);
        if ($check_colocation_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_colocation($id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'terminated') {
            $terminated = $this->service->terminatedColocation($id);
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    // Hanh dong cua 1 dich vu action_email_hosting
    public function action_email_hosting(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_email_hosting_with_user = $this->service->check_email_hosting_with_user($id);
        if ($check_email_hosting_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_email_hosting($id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'terminated') {
            $terminated = $this->service->terminatedEmailHosting($id);
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    // Hanh dong cua nhieu dich vu server
    public function action_colocation_services(Request $request)
    {
        $list_id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_list_colocation_with_user = $this->service->check_list_colocation_with_user($list_id);
        if ($check_list_colocation_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_list_colocation($list_id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            // return $expired;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'delete') {
            $terminated = $this->service->terminatedColocations($list_id);
            // $terminated = false;
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    // Hanh dong cua nhieu dich vu server
    public function action_email_hosting_services(Request $request)
    {
        $list_id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_list_email_hosting_with_user = $this->service->check_list_email_hosting_with_user($list_id);
        if ($check_list_email_hosting_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->service->expired_list_email_hosting($list_id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            // return $expired;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'delete') {
            $terminated = $this->service->terminatedEmailHostings($list_id);
            // $terminated = false;
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }

    public function request_expired_list_server(Request $request)
    {
        $list_id = $request->get('list_id');
        // return json_encode(true);
        $check_list_server_with_user = $this->service->check_server_with_user($list_id);
        if ($check_list_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_list_server($list_id);
        return json_encode($data);
    }

    public function request_expired_list_colocation(Request $request)
    {
        $list_id = $request->get('list_id');
        // return json_encode(true);
        $check_list_colocation_with_user = $this->service->check_list_colocation_with_user($list_id);
        if ($check_list_colocation_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_list_colocation($list_id);
        return json_encode($data);
    }

    public function request_expired_list_email_hosting(Request $request)
    {
        $list_id = $request->get('list_id');
        // return json_encode(true);
        $check_list_email_hosting_with_user = $this->service->check_list_email_hosting_with_user($list_id);
        if ($check_list_email_hosting_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_email_hosting($list_id);
        return json_encode($data);
    }
    // Nâng cấp vps
    public function upgrade_vps(Request $request)
    {
        $vps_id = $request->get('vps_id');
        $qtt_addon_cpu = $request->get('addon_cpu');
        $qtt_addon_ram = $request->get('addon_ram');
        $qtt_addon_disk = $request->get('addon_disk');
        if ($qtt_addon_cpu == 0 && $qtt_addon_ram == 0 && $qtt_addon_disk == 0) {
            return false;
        }
        $upgrade = $this->service->order_addon_vps($request->all());
        return json_encode($upgrade);
    }

    // loadCusomter
    public function loadCusomter()
    {
        $customers = $this->service->loadCusomter();
        return json_encode($customers);
    }
    // updateCusomter
    public function updateCusomter(Request $request)
    {
        $vps = $this->service->updateCusomter($request->all());
        return json_encode($vps);
    }
    // updateDescription
    public function updateDescription(Request $request)
    {
        $vps = $this->service->updateDescription($request->all());
        return json_encode($vps);
    }
    // updateDescriptionProxy
    public function updateDescriptionProxy(Request $request)
    {
        $proxy = $this->service->updateDescriptionProxy($request->all());
        return json_encode($proxy);
    }
    // rebuild_vps
    public function rebuild_vps(Request $request)
    {
        $data = $this->service->rebuild_vps($request->all());
        return json_encode($data);
    }
    public function rebuild_vps_us(Request $request)
    {
        $data = $this->service->rebuild_vps_us($request->all());
        return json_encode($data);
    }

    public function check_upgrade_hosting($id)
    {
        $data = $this->service->check_upgrade_hosting($id);
        return json_encode($data);
    }

    public function upgrade_hosting(Request $request)
    {
        $data = $this->service->upgrade_hosting($request->all());
        return json_encode($data);
    }

    public function request_expired_vps(Request $request)
    {
        $check_vps_with_user = $this->service->check_list_vps_with_user($request->get('list_vps'));
        if ($check_vps_with_user) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->service->request_expired_vps($request->get('list_vps'));
        return json_encode($data);
    }

    public function request_expired_server(Request $request)
    {
        $check_server_with_user = $this->service->check_server_with_user($request->get('id'));
        if ($check_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_list_server([ $request->get('id') ]);
        return json_encode($data);
    }

    public function request_expired_colocation(Request $request)
    {
        $check_colocation_with_user = $this->service->check_colocation_with_user($request->get('id'));
        if ($check_colocation_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_list_colocation([ $request->get('id') ]);
        return json_encode($data);
    }

    public function request_expired_hosting(Request $request)
    {
        $check_hostings_with_user = $this->service->check_hostings_with_user($request->get('list_id'));
        // dd($check_hostings_with_user);
        if ($check_hostings_with_user) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->service->request_expired_hosting($request->get('list_id'));
        return json_encode($data);
    }

    public function request_expired_email_hosting(Request $request)
    {
        $check_email_hosting_with_user = $this->service->check_email_hosting_with_user($request->get('id'));
        if ($check_email_hosting_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->service->request_expired_email_hosting($request->get('id'));
        return json_encode($data);
    }

    public function list_vps_with_sl(Request $request)
    {
       $data = $this->service->list_vps_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_vps_use_with_sl(Request $request)
    {
       $data = $this->service->list_vps_use_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_vps_us_with_sl(Request $request)
    {
       $data = $this->service->list_vps_us_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_vps_us_use_with_sl(Request $request)
    {
       $data = $this->service->list_vps_us_use_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_vps_nearly_with_sl(Request $request)
    {
       $data = $this->service->list_vps_nearly_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_vps_us_nearly_with_sl(Request $request)
    {
       $data = $this->service->list_vps_us_nearly_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_hosting_with_sl(Request $request)
    {
       $data = $this->service->list_hosting_with_sl($request->all());
       return json_encode($data);
    }

    public function list_all_vps_with_sl(Request $request)
    {
       $data = $this->service->list_all_vps_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_all_vps_us_with_sl(Request $request)
    {
       $data = $this->service->list_all_vps_us_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_all_hosting_with_sl(Request $request)
    {
       $data = $this->service->list_all_hosting_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function list_hosting_nearly_with_sl(Request $request)
    {
       $data = $this->service->list_hosting_nearly_with_sl($request->get('sl'));
       return json_encode($data);
    }

    public function search_vps(Request $request)
    {
        $data = $this->service->search_vps($request->get('q') , $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function search_vps_use(Request $request)
    {
        $data = $this->service->search_vps_use($request->get('q') , $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function search_multi_vps_on(Request $request)
    {
        $data = $this->service->search_multi_vps_on( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_use(Request $request)
    {
        $data = $this->service->search_multi_vps_use( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_us_on(Request $request)
    {
        $data = $this->service->search_multi_vps_us_on( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_us_use(Request $request)
    {
        $data = $this->service->search_multi_vps_us_use( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_nearly(Request $request)
    {
        $data = $this->service->search_multi_vps_nearly( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_us_nearly(Request $request)
    {
        $data = $this->service->search_multi_vps_us_nearly( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_cancel(Request $request)
    {
        $data = $this->service->search_multi_vps_cancel( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_us_cancel(Request $request)
    {
        $data = $this->service->search_multi_vps_us_cancel( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_all(Request $request)
    {
        $data = $this->service->search_multi_vps_all( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_multi_vps_us_all(Request $request)
    {
        $data = $this->service->search_multi_vps_us_all( explode(',', $request->get('q')) );
        return json_encode($data);
    }

    public function search_vps_us(Request $request)
    {
        $data = $this->service->search_vps_us($request->get('q') , $request->get('sort') , $request->get('sl'));
        return json_encode($data);
    }

    public function search_vps_us_use(Request $request)
    {
        $data = $this->service->search_vps_us_use($request->get('q') , $request->get('sort') , $request->get('sl'));
        return json_encode($data);
    }

    public function hosting_search(Request $request)
    {
        $data = $this->service->hosting_search($request->get('q') , $request->get('sort'));
        return json_encode($data);
    }

    public function all_hosting_search(Request $request)
    {
        $data = $this->service->all_hosting_search($request->get('q') , $request->get('sort'));
        return json_encode($data);
    }

    public function search_vps_nearly(Request $request)
    {
        $data = $this->service->search_vps_nearly($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function search_vps_cancel(Request $request)
    {
        $data = $this->service->search_vps_cancel($request->get('q'), $request->get('sl'));
        return json_encode($data);
    }

    public function search_vps_us_nearly(Request $request)
    {
        $data = $this->service->search_vps_us_nearly($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function search_vps_us_cancel(Request $request)
    {
        $data = $this->service->search_vps_us_cancel($request->get('q'), $request->get('sl'));
        return json_encode($data);
    }

    public function list_hosting_nearly_search(Request $request)
    {
        $data = $this->service->list_hosting_nearly_search($request->get('q'), $request->get('hosting_sl'));
        return json_encode($data);
    }

    public function all_vps_search(Request $request)
    {
        $data = $this->service->all_vps_search($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function all_vps_us_search(Request $request)
    {
        $data = $this->service->all_vps_us_search($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }

    public function select_server(Request $request)
    {
        $data = $this->service->select_server($request->all());
        return json_encode($data);
    }

    public function search_server(Request $request)
    {
        $data = $this->service->search_server($request->get('q'), $request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }

    public function select_colocation(Request $request)
    {
        $data = $this->service->select_colocation($request->all());
        return json_encode($data);
    }

    public function search_colocation(Request $request)
    {
        $data = $this->service->search_colocation($request->get('q'), $request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }

    public function select_email_hosting(Request $request)
    {
        $data = $this->service->select_email_hosting($request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }

    public function search_email_hosting(Request $request)
    {
        $data = $this->service->search_email_hosting($request->get('q'), $request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }

    public function vps_console($id)
    {
        $data_console = $this->service->vps_console($id);
        if ($data_console['error'] == 0) {
            return view('users.services.console', compact('data_console'));
        } elseif ($data_console['error'] == 1) {
            return redirect()->route('service.vps.on')->with('fails', 'VPS này không có hoặc không thuộc về quý khách');
        } else {
            return redirect()->route('service.vps.on')->with('fails', 'Console VPS đang không khả dụng. VPS có thể đang bị tắt hoặc dịch vụ console đang có lỗi. Quý khách có thể khởi động lại VPS và thử lại hoặc liên hệ hỗ trợ.');
        }
    }

    // CHUYỂN USER - VPS
    public function change_user_by_vps(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id = explode(',', $request->get('list_vps'));
            if ( $this->service->check_list_vps_with_user($list_id) ) {
                return redirect()->route('service.vps.on')->with('fails', 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.');
            }
            $list_vps = [];
            foreach ($list_id as $key => $id) {
                $list_vps[] = $this->service->detail_vps($id);
            }
            return view('users.services.change_user_by_vps', compact('list_vps'));
        } else {
            return redirect()->route('service.vps.on')->with('fails', 'Quý khách đã không chọn VPS. Quý khách vui lòng chọn một hoặc nhiều VPS và bấm vào nút chuyển khách hàng.');
        }
    }

    public function change_user_by_vps_us(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id = explode(',', $request->get('list_vps'));
            if ( $this->service->check_list_vps_with_user($list_id) ) {
                return redirect()->route('service.vps_us.on')->with('fails', 'Trong các VPS US khách hàng vừa chọn. Có 1 hoặc nhiều VPS US không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS US được chọn.');
            }
            $list_vps = [];
            foreach ($list_id as $key => $id) {
                $list_vps[] = $this->service->detail_vps($id);
            }
            return view('users.services.vps_us.change_user_by_vps', compact('list_vps'));
        } else {
            return redirect()->route('service.vps_us.on')->with('fails', 'Quý khách không chọn VPS US. Quý khách vui lòng chọn VPS US và bấm vào nút chuyển khách hàng.');
        }
    }

    public function form_change_user_by_vps(Request $request)
    {
        if ( empty($request->get('list_vps')) ) {
            return redirect()->route('service.vps.on')->with('fails', 'Quý khách đã không chọn VPS. Quý khách vui lòng chọn một hoặc nhiều VPS và bấm vào nút chuyển khách hàng.');
        }
        if ( $this->service->check_list_vps_with_user($request->get('list_vps')) ) {
            return redirect()->route('service.vps.on')->with('fails', 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.');
        }
        $check_user_changed = $this->user_repo->detail_user_by_email($request->get('email'));
        if ( $check_user_changed['error'] == 1 || $check_user_changed['error'] == 2 ) {
            return redirect()->route('service.vps.on')->with('fails', 'Không có khách hàng này trong dữ liệu. Quý khách vui lòng kiểm tra lại email hoặc tạo tài khoản bằng email này.');
        }
        elseif ( $check_user_changed['error'] == 3 ) {
            return redirect()->route('service.vps.on')->with('fails', 'Lỗi chuyển VPS. Quý khách không thể chuyển các VPS này cho cùng một tài khoản. Quý khách vui lòng kiểm tra lại.');
        }
        // dd($check_user_changed);
        $change = $this->service->change_user_by_vps($request->all());
        if ( $change['error'] == 0 ) {
            return redirect()->route('service.vps.on')->with('success', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thành công.');
        }
        elseif ( $change['error'] == 1 ) {
            return redirect()->route('service.vps.on')->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
        }
        else {
            return redirect()->route('service.vps.on')->with('fails', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thất bại.');
        }
    }

    public function form_change_user_by_vps_us(Request $request)
    {
        if ( empty($request->get('list_vps')) ) {
            return redirect()->route('service.vps_us.on')->with('fails', 'Quý khách đã không chọn VPS. Quý khách vui lòng chọn một hoặc nhiều VPS và bấm vào nút chuyển khách hàng.');
        }
        if ( $this->service->check_list_vps_with_user($request->get('list_vps')) ) {
            return redirect()->route('service.vps_us.on')->with('fails', 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.');
        }
        $check_user_changed = $this->user_repo->detail_user_by_email($request->get('email'));
        if ( $check_user_changed['error'] == 1 || $check_user_changed['error'] == 2 ) {
            return redirect()->route('service.vps_us.on')->with('fails', 'Không có khách hàng này trong dữ liệu. Quý khách vui lòng kiểm tra lại email hoặc tạo tài khoản bằng email này.');
        }
        elseif ( $check_user_changed['error'] == 2 ) {
            return redirect()->route('service.vps_us.on')->with('fails', 'Lỗi chuyển VPS. Quý khách không thể chuyển các VPS này cho cùng 1 tài khoản. Quý khách vui lòng kiểm tra lại email hoặc tạo tài khoản bằng email này.');
        }
        // dd($request->all());
        $change = $this->service->change_user_by_vps_us($request->all());
        if ( $change['error'] == 0 ) {
            return redirect()->route('service.vps_us.on')->with('success', 'Chuyển VPS US cho khách hàng ' . $change['user']['email'] . ' thành công.');
        }
        elseif ( $change['error'] == 1 ) {
            return redirect()->route('service.vps_us.on')->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
        }
        else {
            return redirect()->route('service.vps_us.on')->with('fails', 'Chuyển VPS US cho khách hàng ' . $change['user']['email'] . ' thất bại.');
        }
    }

    // XUẤT FILE EXCEL
    public function get_export_excel_by_vps(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id = explode(',', $request->get('list_vps'));
            if ( $this->service->check_list_vps_with_user($list_id) ) {
                return redirect()->back()->with('fails', 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.');
            }
            $list_vps = [];
            foreach ($list_id as $key => $id) {
                $list_vps[] = $this->service->detail_vps($id);
            }
            return view('users.services.export_vps', compact('list_vps'));
        } else {
            return redirect()->back()->with('fails', 'Quý khách không chọn VPS. Quý khách vui lòng chọn VPS và bấm vào nút xuất file excel.');
        }
    }

    public function form_export_excel_by_vps(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id_vps = $request->get('list_vps');
            // $this->service->check_list_vps_with_user($list_id_vps)
            if ( $this->service->check_list_vps_with_user($list_id_vps) ) {
                return json_encode([
                    'error' => 1,
                    'content' => 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.',
                    'file_name' => ''
                ]);
            }
            $data = $this->service->export_excel($list_id_vps, $request->all());

            if ( !empty($request->get('file_name')) ) {
                $unicode = array(
                    'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                    'd'=>'đ|Đ',
                    'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                    'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
                    'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
                    'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
                    'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
                );
                $str_name = strtolower( $request->get('file_name') );
                foreach($unicode as $nonUnicode => $uni){
                    $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
                }
                $str_name = str_replace( ' ', '_', $str_name );
            }
            else {
                $str_name = 'vps_' . date('m_d_Y');
            }

            $path = 'public/excel/' . $str_name . '.xlsx';
            $export = new ExportVpsUs($data);
            Excel::store($export, $path);
            return json_encode(
                [
                    'error' => 0,
                    'content' => '/storage/excel/' . $str_name . '.xlsx' ,
                    'file_name' => $str_name . '.xlsx',
                ]
            );
        }
        else {
            return json_encode([
                'error' => 1,
                'content' => 'Quý khách không chọn VPS. Quý khách vui lòng chọn VPS và bấm vào nút xuất file excel.',
                'file_name' => ''
            ]);
        }
    }

    public function get_export_excel_by_vps_us(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id = explode(',', $request->get('list_vps'));
            if ( $this->service->check_list_vps_with_user($list_id) ) {
                return redirect()->back()->with('fails', 'Trong các VPS US khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.');
            }
            $list_vps = [];
            foreach ($list_id as $key => $id) {
                $list_vps[] = $this->service->detail_vps($id);
            }
            return view('users.services.vps_us.export_vps', compact('list_vps'));
        } else {
            return redirect()->back()->with('fails', 'Quý khách không chọn VPS US. Quý khách vui lòng chọn VPS và bấm vào nút xuất file excel.');
        }
    }

    public function form_export_excel_by_vps_us(Request $request)
    {
        if ( !empty($request->get('list_vps')) ) {
            $list_id_vps = $request->get('list_vps');
            // $this->service->check_list_vps_with_user($list_id_vps)
            if ( $this->service->check_list_vps_with_user($list_id_vps) ) {
                return json_encode([
                    'error' => 1,
                    'content' => 'Trong các VPS khách hàng vừa chọn. Có 1 hoặc nhiều VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại các VPS được chọn.',
                    'file_name' => ''
                ]);
            }
            $data = $this->service->export_excel_vps_us($list_id_vps, $request->all());

            if ( !empty($request->get('file_name')) ) {
                $unicode = array(
                    'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                    'd'=>'đ|Đ',
                    'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                    'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
                    'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
                    'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
                    'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
                );
                $str_name = strtolower( $request->get('file_name') );
                foreach($unicode as $nonUnicode => $uni){
                    $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
                }
                $str_name = str_replace( ' ', '_', $str_name );
            }
            else {
                $str_name = 'vps_' . date('m_d_Y');
            }

            $path = 'public/excel/' . $str_name . '.xlsx';
            $export = new ExportVpsUs($data);
            Excel::store($export, $path);
            return json_encode(
                [
                    'error' => 0,
                    'content' => '/storage/excel/' . $str_name . '.xlsx' ,
                    'file_name' => $str_name . '.xlsx',
                ]
            );
        }
        else {
            return json_encode([
                'error' => 1,
                'content' => 'Quý khách không chọn VPS. Quý khách vui lòng chọn VPS và bấm vào nút xuất file excel.',
                'file_name' => ''
            ]);
        }
    }

    public function check_os(Request $request)
    {
        $data = $this->service->check_os($request->get('list_id'));
        return json_encode($data);
    }

    public function proxy_use()
    {
        $list_proxy = $this->service->list_proxy_use();
        $billing = config('billing');
        return view('users.services.proxies.list_proxy', compact('list_proxy' , 'billing'));
    }

    public function proxy_on()
    {
        $list_proxy = $this->service->list_proxy_on();
        $billing = config('billing');
        return view('users.services.proxies.list_proxy_on', compact('list_proxy' , 'billing'));
    }

    public function proxy_expire()
    {
        $list_proxy = $this->service->list_proxy_expire();
        $billing = config('billing');
        return view('users.services.proxies.list_proxy_expire', compact('list_proxy' , 'billing'));
    }

    public function proxy_cancel()
    {
        $list_proxy = $this->service->list_proxy_cancel();
        $billing = config('billing');
        return view('users.services.proxies.list_proxy_cancel', compact('list_proxy' , 'billing'));
    }

    public function proxy_all()
    {
        $list_proxy = $this->service->list_proxy_all();
        $billing = config('billing');
        return view('users.services.proxies.list_proxy_all', compact('list_proxy' , 'billing'));
    }

    public function list_proxy_use_with_sort(Request $request)
    {
        // dd($request->all());
        $list_proxy = $this->service->list_proxy_use_with_sort($request->all());
        return json_encode($list_proxy);
    }

    public function list_proxy_on_with_sort(Request $request)
    {
        // dd($request->all());
        $list_proxy = $this->service->list_proxy_on_with_sort($request->all());
        return json_encode($list_proxy);
    }

    public function list_proxy_cancel_with_sort(Request $request)
    {
        // dd($request->all());
        $list_proxy = $this->service->list_proxy_cancel_with_sort($request->all());
        return json_encode($list_proxy);
    }

    public function list_proxy_all_with_sort(Request $request)
    {
        // dd($request->all());
        $list_proxy = $this->service->list_proxy_all_with_sort($request->all());
        return json_encode($list_proxy);
    }

    public function list_proxy_expire_with_sort(Request $request)
    {
        // dd($request->all());
        $list_proxy = $this->service->list_proxy_expire_with_sort($request->all());
        return json_encode($list_proxy);
    }

    public function request_expired_proxy(Request $request)
    {
        $id = $request->get('id');
        $list_id = [ $id ];
        $check_proxy_with_user = $this->service->check_list_proxy_with_user($list_id);
        if ( $check_proxy_with_user ) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->service->request_expired_proxy($list_id);
        return json_encode($data);
    }

    public function request_expired_list_proxy(Request $request)
    {
        $list_id = $request->get('list_id');
        $check_proxy_with_user = $this->service->check_list_proxy_with_user($list_id);
        if ( $check_proxy_with_user ) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->service->request_expired_proxy($list_id);
        return json_encode($data);
    }

}
