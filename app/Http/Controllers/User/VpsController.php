<?php

namespace App\Http\Controllers\User;

use App\Factories\UserFactories;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class VpsController extends Controller
{
    protected $vpsData;
    //protected $user_repo;

    public function __construct()
    {
        $this->vpsData = UserFactories::VpsRepositories();
     
    }
    public function vps_on()
    {
         
        $list_vps = $this->vpsData->list_vps_on();

        $billing = config('billing');
        return view('users.services.list_vps_on', compact('list_vps' , 'billing'));
    }
    public function vps_use()
    {
        $list_vps = $this->vpsData->list_vps_use();
        $billing = config('billing');
        return view('users.services.list_vps_use', compact('list_vps' , 'billing'));
    }

    public function vps_nearly()
    {
        $list_vps = $this->vpsData->list_vps_nearly();
        $billing = config('billing');
        return view('users.services.list_vps_nearly', compact('list_vps' , 'billing'));
    }
    public function vps_cancel()
    {
        $list_vps = $this->vpsData->list_vps_cancel();
        $billing = config('billing');
        return view('users.services.list_vps_cancel', compact('list_vps' , 'billing'));
    }
    public function vps_all()
    {
       $list_vps = $this->vpsData->vps_all();
       $billing = config('billing');
       return view('users.services.list_vps_all', compact('list_vps' , 'billing'));
    }
    public function vps_nearly_and_expire()
    {
        return view('users.services.list_vps_nearly_and_expire');
    }
    public function list_vps_nearly_and_expire()
    {
        $data = $this->vpsData->list_vps_nearly_and_expire();
        return json_encode($data);
    }
    public function vps_us_on()
    {
        $list_vps = $this->vpsData->list_vps_us_on();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_on', compact('list_vps' , 'billing'));
    }
    public function vps_us_use()
    {
        $list_vps = $this->vpsData->list_vps_us_use();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_use', compact('list_vps' , 'billing'));
    }
    public function vps_us_nearly()
    {
        $list_vps = $this->vpsData->list_vps_us_nearly();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_nearly', compact('list_vps' , 'billing'));
    }
    public function vps_us_cancel()
    {
        $list_vps = $this->vpsData->list_vps_us_cancel();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_cancel', compact('list_vps' , 'billing'));
    }
    
    public function vps_us_all()
    {
       $list_vps = $this->vpsData->vps_us_all();
       $billing = config('billing');
       return view('users.services.vps_us.list_vps_us_all', compact('list_vps' , 'billing'));
    }
    public function list_vps_us_nearly_and_expire()
    {
        $data = $this->vpsData->list_vps_us_nearly_and_expire();
        return json_encode($data);
    }

    public function list_vps_with_sl(Request $request)
    {
       $data = $this->vpsData->list_vps_with_sl($request->get('sl'));
       return json_encode($data);
    }
    public function search_vps(Request $request)
    {
        $data = $this->vpsData->search_vps($request->get('q') , $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }
    public function list_vps_use_with_sl(Request $request)
    {
       $data = $this->vpsData->list_vps_use_with_sl($request->get('sl'));
       return json_encode($data);
    }public function search_vps_use(Request $request)
    {
        $data = $this->vpsData->search_vps_use($request->get('q') , $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }
    public function search_multi_vps_use(Request $request)
    {
        $data = $this->vpsData->search_multi_vps_use( explode(',', $request->get('q')) );
        return json_encode($data);
    }
    public function list_vps_nearly_with_sl(Request $request)
    {
       $data = $this->vpsData->list_vps_nearly_with_sl($request->get('sl'));
       return json_encode($data);
    }
    public function search_vps_nearly(Request $request)
    {
        $data = $this->vpsData->search_vps_nearly($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }
    public function search_vps_cancel(Request $request)
    {
        $data = $this->vpsData->search_vps_cancel($request->get('q'), $request->get('sl'));
        return json_encode($data);
    }
    public function search_multi_vps_cancel(Request $request)
    {
        $data = $this->vpsData->search_multi_vps_cancel( explode(',', $request->get('q')) );
        return json_encode($data);
    }
    public function request_expired_vps(Request $request)
    {
        $check_vps_with_user = $this->vpsData->check_list_vps_with_user($request->get('list_vps'));
        if ($check_vps_with_user) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->vpsData->request_expired_vps($request->get('list_vps'));
        return json_encode($data);
    }
    public function action_services(Request $request)
    {
        $type = $request->get('type');
        $list_id = $request->get('id');
        $action = $request->get('action');
        switch ($type) {
            // Hosting
          case 'hosting':
            $check_hostings_with_user = $this->vpsData->check_hostings_with_user($list_id);
            if ($check_hostings_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
                  case 'on':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->vpsData->unSuspend($id);
                          if (!empty($action_services['error'])) {
                              $hosting = $this->vpsData->detail_hosting($id);
                              return json_encode($data['error'] = 'Hosting ' . $hosting->domain . ' do Admin Cloudzone suspend nên không thể Unsuspend này được. Vui lòng liên hệ với chúng tôi để mở lại Hosting.');
                          }
                      }
                      return json_encode(true);
                    break;
                  case 'off':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->vpsData->suspend_hosting($id);
                          if (!$action_services) {
                              return json_encode(false);
                          }
                      }
                      return json_encode(true);
                    break;
                  case 'delete':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      foreach ($list_id as $key => $id) {
                          $action_services = $this->vpsData->terminated_hosting($id);
                          if (!$action_services) {
                              return json_encode(false);
                          }
                      }
                      return json_encode(true);
                    break;

                  case 'expired':
                      if (!isset($list_id)) {
                          $data_status = [
                              'error' => 9999,
                          ];
                          return json_encode($data_status);
                      }
                      $billing_cycle = $request->get('billing_cycle');
                      $action_services = $this->vpsData->expired_hostings($list_id, $billing_cycle);
                      return json_encode($action_services);
                    break;

                  default:
                    return json_encode(false);
                    break;
            }
            break;
          // VPS
          case 'vps':
            $check_vps_with_user = $this->vpsData->check_list_vps_with_user($list_id);
            if ($check_vps_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
              case 'on':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->onVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->offVPS($id);
                        if ( empty($action_services['success']) ) {
                            return json_encode($action_services);
                        }
                        $li[] = $id;
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                        'text' => $li
                    ];
                    return json_encode($data_status);
                break;
              case 'delete':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->terminatedVps($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'auto_refurn':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->autoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'restart':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->restartVPS($id);
                        if (!empty($action_services)) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'expired':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->vpsData->gia_han_nhieu_vps($list_id, $billing_cycle);
                    return json_encode($expired);
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'change_ip':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $change_ip = $this->vpsData->request_change_ip($list_id);
                    return json_encode($change_ip);
                break;
            break;
          }
          // VPS US
          case 'vps_us':
            $check_vps_with_user = $this->vpsData->check_list_vps_with_user($list_id);
            if ($check_vps_with_user) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
              case 'on':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->onVPSUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->offVPSUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'delete':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->terminatedVpsUS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'auto_refurn':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->autoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'off_auto_refurn':
                    foreach ($list_id as $key => $id) {
                        $action_services = $this->vpsData->offAutoRefurnVPS($id);
                        if (!empty($action_services['error'])) {
                            return json_encode($action_services);
                        }
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'expired':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->vpsData->gia_han_nhieu_vps($list_id, $billing_cycle);
                    return json_encode($expired);
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                break;
              case 'change_ip':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $expired = $this->vpsData->change_ip_vps_us($list_id);
                    return json_encode($expired);
                break;
              case 'reset_password':
                    if (!isset($list_id)) {
                        $data_status = [
                            'error' => 9999,
                        ];
                        return json_encode($data_status);
                    }
                    $expired = $this->vpsData->reset_password($list_id);
                    return json_encode($expired);
                break;

            break;
          }
          // PROXY
          case 'proxy':
            $list_id = $request->get('id');
            $check_proxy_with_user = $this->vpsData->check_list_proxy_with_user($list_id);
            if ( $check_proxy_with_user ) {
                $data_status = [
                    'error' => 9998,
                ];
                return json_encode($data_status);
            }
            switch ($action) {
                case 'expired':
                    $expired = $this->vpsData->expired_list_proxy($list_id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                    break;
                case 'delete':
                    $expired = $this->vpsData->terminatedProxy($list_id);
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                    break;
                default:
                    return json_encode(false);
            }
            break;
          default:
            return json_encode(false);
        }
        return json_encode(true);

    }
    
    public function list_all_vps_with_sl(Request $request)
    {
       $data = $this->vpsData->list_all_vps_with_sl($request->get('sl'));
       return json_encode($data);
    }
    public function all_vps_search(Request $request)
    {
        $data = $this->vpsData->all_vps_search($request->get('q'), $request->get('sort'), $request->get('sl'));
        return json_encode($data);
    }
    public function search_multi_vps_all(Request $request)
    {
        $data = $this->vpsData->search_multi_vps_all( explode(',', $request->get('q')) );
        return json_encode($data);
    }
    public function rebuild_vps(Request $request)
    {
        $data = $this->vpsData->rebuild_vps($request->all());
        return json_encode($data);
    }
    public function request_expired_hosting(Request $request)
    {
        $check_hostings_with_user = $this->vpsData->check_hostings_with_user($request->get('list_id'));
        // dd($check_hostings_with_user);
        if ($check_hostings_with_user) {
            $data_status = [
                'error' => 9998,
            ];
            return json_encode($data_status);
        }
        $data = $this->vpsData->request_expired_hosting($request->get('list_id'));
        return json_encode($data);
    }
    public function loadStatusVPS(Request $request)
    {
        $ids = $request->get('ids');
        $list_vps = $this->vpsData->loadVpsWithIds($ids);
        return json_encode($list_vps);
    }
 
    public function action_one_service(Request $request)
    {
        $type = $request->get('type');
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        switch ($type) {
          // Hosting
            case 'hosting':
                $check_hosting_with_user = $this->vpsData->check_hosting_with_user($id);
                if ($check_hosting_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $billing_cycle = $request->get('billing_cycle');
                    $expired = $this->vpsData->expired_hosting($id, $billing_cycle);
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'suspend') {
                    $suspend = $this->vpsData->suspend_hosting($id);
                    if ($suspend) {
                        return json_encode(true);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'unsuspend') {
                    $unsuspend = $this->vpsData->unSuspend($id);
                    if ($unsuspend) {
                        return json_encode($unsuspend);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'terminated') {
                    $unsuspend = $this->vpsData->terminated_hosting($id);
                    if ($unsuspend) {
                        return json_encode(true);
                    }
                    return json_encode(false);
                }
                break;

            case 'vps':
                $check_vps_with_user = $this->vpsData->check_vps_with_user($id);
                if ($check_vps_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->vpsData->expired_vps($id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'on') {
                    $on = $this->vpsData->onVPS($id);
                    if (!empty($on['error'])) {
                        return json_encode($on['error']);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'off') {
                    $off = $this->vpsData->offVPS($id);
                    if (!empty($off['error'])) {
                        return json_encode($off);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'restart') {
                    $restart = $this->vpsData->restartVPS($id);
                    if (!empty($restart['error'])) {
                        return json_encode($restart);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->vpsData->terminatedVps($id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;

            case 'vps_us':
                $check_vps_with_user = $this->vpsData->check_vps_with_user($id);
                if ($check_vps_with_user) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->vpsData->expired_vps($id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'on') {
                    $on = $this->vpsData->onVPSUS($id);
                    if (!empty($on['error'])) {
                        return json_encode($on['error']);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'off') {
                    $off = $this->vpsData->offVPSUS($id);
                    if (!empty($off['error'])) {
                        return json_encode($off);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'restart') {
                    $restart = $this->vpsData->restartVPSUS($id);
                    $data_status = [
                        'error' => '',
                        'success' => true
                    ];
                    return json_encode($data_status);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->vpsData->terminatedVpsUS($id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;
            case 'proxy':
                $id = $request->get('id');
                $list_id = [ $id ];
                $check_proxy_with_user = $this->vpsData->check_list_proxy_with_user($list_id);
                if ( $check_proxy_with_user ) {
                    $data_status = [
                        'error' => 9998,
                    ];
                    return json_encode($data_status);
                }
                if ($action == 'expired') {
                    $expired = $this->vpsData->expired_list_proxy($list_id , $request->get('billing_cycle'));
                    if ($expired) {
                        return json_encode($expired);
                    }
                    return json_encode(false);
                }
                elseif ($action == 'terminated') {
                    $terminated = $this->vpsData->terminatedProxy($list_id);
                    if (!empty($terminated['error'])) {
                        return json_encode($terminated);
                    }
                    $data_status = [
                        'error' => '',
                        'success' => true,
                    ];
                    return json_encode($data_status);
                }
                break;
        }
        return json_encode(false);
    }
    public function loadCusomter()
    {
        $customers = $this->vpsData->loadCusomter();
        return json_encode($customers);
    }
    public function updateCusomter(Request $request)
    {
        $vps = $this->vpsData->updateCusomter($request->all());
        return json_encode($vps);
    }
    public function updateDescription(Request $request)
    {
        $vps = $this->vpsData->updateDescription($request->all());
        return json_encode($vps);
    }
    public function request_expired_server(Request $request)
    {
        $check_server_with_user = $this->vpsData->check_server_with_user($request->get('id'));
        if ($check_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->vpsData->request_expired_list_server([ $request->get('id') ]);
        return json_encode($data);
    }
    public function action_server(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_server_with_user = $this->vpsData->check_server_with_user($id);
        if ($check_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->vpsData->expired_list_server([$id] , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'on') {
           // $on = $this->service->onVPS($id);
           // if (!empty($on['error'])) {
           //     return json_encode($on['error']);
           // }
           // $data_status = [
           //     'error' => '',
           //     'success' => true,
           // ];
           // return json_encode($data_status);
        }
        elseif ($action == 'off') {
            // $off = $this->service->offVPS($id);
            // if (!empty($off['error'])) {
            //     return json_encode($off);
            // }
            // $data_status = [
            //     'error' => '',
            //     'success' => true,
            // ];
            // return json_encode($data_status);
        }
        elseif ($action == 'restart') {
            // $restart = $this->service->restartVPS($id);
            // if (!empty($restart['error'])) {
            //     return json_encode($restart);
            // }
            // $data_status = [
            //     'error' => '',
            //     'success' => true,
            // ];
            // return json_encode($data_status);
        }
        elseif ($action == 'terminated') {
            $terminated = $this->vpsData->terminatedServer($id);
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }
    
    public function request_expired_list_server(Request $request)
    {
        $list_id = $request->get('list_id');
        // return json_encode(true);
        $check_list_server_with_user = $this->vpsData->check_server_with_user($list_id);
        if ($check_list_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->vpsData->request_expired_list_server($list_id);
        return json_encode($data);
    }
    public function action_server_services(Request $request)
    {
        $list_id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        $check_list_server_with_user = $this->vpsData->check_list_server_with_user($list_id);
        if ($check_list_server_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        if ($action == 'expired') {
            $expired = $this->vpsData->expired_list_server($list_id , $request->get('billing_cycle'));
            // $expired['status'] = true;
            // $expired['invoice_id'] = 1;
            if ($expired['status']) {
                $data_status = [
                    'error' => '',
                    'success' => true,
                    'type' => 3,
                    'invoice_id' => $expired['invoice_id'],
                ];
            } else {
                $data_status = [
                    'error' => 3,
                    'success' => true,
                    'type' => '',
                    'invoice_id' => '',
                ];
            }
            return json_encode($data_status);
        }
        elseif ($action == 'delete') {
            $terminated = $this->vpsData->terminatedServers($list_id);
            // $terminated = false;
            if ($terminated) {
              $data_status = [
                  'error' => '',
                  'success' => true,
                  'type' => 4
              ];
            } else {
              $data_status = [
                  'error' => 4,
                  'success' => '',
                  'type' => ''
              ];
            }
            return json_encode($data_status);
        }
        return json_encode(false);
    }
    public function select_server(Request $request)
    {
        $data = $this->vpsData->select_server($request->all());
        return json_encode($data);
    }
    public function request_expired_list_email_hosting(Request $request)
    {
        $list_id = $request->get('list_id');
        // return json_encode(true);
        $check_list_email_hosting_with_user = $this->vpsData->check_list_email_hosting_with_user($list_id);
        if ($check_list_email_hosting_with_user) {
            $data = [
              'error' => 1,
              'success' => '',
              'type' => '',
            ];
            return json_encode($data);
        }
        $data = $this->vpsData->request_expired_email_hosting($list_id);
        return json_encode($data);
    }
    public function select_email_hosting(Request $request)
    {
        $data = $this->vpsData->select_email_hosting($request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }
    public function search_email_hosting(Request $request)
    {
        $data = $this->vpsData->search_email_hosting($request->get('q'), $request->get('qtt'), $request->get('action'));
        return json_encode($data);
    }
}
