<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use Illuminate\Support\Facades\Auth;

class TicketController extends Controller
{
    protected $ticketRepository;

    public function __construct()
    {
        $this->ticketRepository = UserFactories::ticketRepository();
    }

    public function getList(Request $request)
    {
        $list = $this->ticketRepository->getList(Auth::id(), []);

        return view('users.tickets.index', compact('list'));
    }

    public function detail($id)
    {
        $detail = $this->ticketRepository->getDetail($id);

        return view('users.tickets.detail', compact('detail'));
    }

    public function create(Request $request)
    {
        return view('users.tickets.create');
    }

    public function createHandle(Request $request)
    {
        $data = $request->all();
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $data['user_id'] = Auth::id();
        $save = $this->ticketRepository->save(null, $data);
        if ($save) {
            return redirect(route('user.ticket.getList'))->with('success',
                'Gửi ticket thành công, vui lòng chờ admin xử lý ticket của bạn');
        } else {
            return redirect(route('user.ticket.create'))->with('fails', 'Có lỗi xảy ra, vui lòng thử lại.');
        }
    }

    public function update($id, Request $request)
    {
        $detail = $this->ticketRepository->getDetail($id);

        return view('users.tickets.update', compact('detail'));
    }

    public function updateHandle($id, Request $request)
    {
        $data = $request->all();
        $validatedData = $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);

        $data['user_id'] = Auth::id();
        $save = $this->ticketRepository->save($id, $data);
        if ($save) {
            return redirect(route('user.ticket.getList'))->with('success',
                'Chỉnh sửa ticket thành công, vui lòng chờ admin xử lý ticket của bạn');
        } else {
            return redirect(route('user.ticket.update', ['id' => $id]))->with('fails',
                'Có lỗi xảy ra, vui lòng thử lại.');
        }
    }

    public function message(Request $request)
    {
        $data = $request->all();
        $validatedData = $request->validate([
            'content' => 'required',
            'ticket_id' => 'required',
        ]);
        $ticketId = $data['ticket_id'];
        $data['user_id'] = Auth::id();

        $save = $this->ticketRepository->saveMessage($data);

        if ($save) {
            return redirect(route('user.ticket.detail', ['id' => $ticketId]))->with('success',
                'Gửi tin nhắn thành công, vui lòng đợi phải hồi của admin, chúng tôi sẽ gửi thông báo qua email cho bạn. Xin cảm ơn.');
        } else {
            return redirect(route('user.ticket.detail', ['id' => $ticketId]))->with('fails',
                'Có lỗi xảy ra, vui lòng thử lại.');
        }
    }

    public function deleteHandle($id, Request $request)
    {

    }
}
