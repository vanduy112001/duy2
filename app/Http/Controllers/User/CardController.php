<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use PDF;
use App\Model\Hosting;
use App\Http\Requests\RegisterDomainValidate;
use Illuminate\Support\Facades\Auth;
use App\Model\LogActivity;

class CardController extends Controller
{
    protected $card;
    protected $customer;
    protected $payin;
    protected $domain_product;
    protected $domain;
    protected $product;
    protected $card_point;
    protected $log_activity;
    protected $service;

    public function __construct()
    {
        $this->card = UserFactories::homeRepositories();
        $this->customer = UserFactories::customerRepositories();
        $this->payin = UserFactories::payInRepositories();
        $this->domain_product = UserFactories::domainProductRepositories();
        $this->product = UserFactories::productRepositories();
        $this->domain = UserFactories::domainRepositories();
        $this->card_point = UserFactories::cardPointRepositories();
        $this->log_activity = new LogActivity();
        $this->service = UserFactories::serviceRepositories();
    }

    public function addCard($product_id, Request $request)
    {
        $check_product_with_user = $this->product->check_product_with_user($product_id);
        if ($check_product_with_user) {
            return redirect()->route('index')->with('fails', 'Sản phẩm / Dịch vụ không có thật. Vui lòng chọn gói sản phẩm / dịch vụ trong Đăng ký dịch vụ.');
        }
        $billing_cycle = $request->get('billing_cycle');
        $product = $this->card->detail_product($product_id);
        $list_os = config('os');
        $list_vps_os = $this->card->list_vps_os($product_id);
        $billings = config('billing');
        $list_customers = $this->customer->list_customer_with_user();
        $list_city = config('city');
        $states = config('states');
        $list_state = $this->product->list_state();
        $list_state_proxy = $this->product->list_state_proxy();
        return view('users.cards.add_card', compact('product', 'billings', 'list_os', 'list_customers', 'billing_cycle', 
        'list_vps_os', 'list_city', 'states', 'list_state', 'list_state_proxy') );
    }

    public function addCardServer($product_id, Request $request)
    {
        $check_product_with_user = $this->product->check_product_with_user($product_id);
        if ($check_product_with_user) {
            return redirect()->route('index')->with('fails', 'Sản phẩm / Dịch vụ không có thật. Vui lòng chọn gói sản phẩm / dịch vụ trong Đăng ký dịch vụ.');
        }
        $billing_cycle = $request->get('billing_cycle');
        $product = $this->card->detail_product($product_id);
        $list_os = config('listOsServer');
        $list_vps_os = $this->card->list_vps_os($product_id);
        $list_datacenter = $this->card->list_datacenter($product_id);
        $list_server_management = $this->card->list_server_management($product_id);
        $list_raid = $this->card->list_raid($product_id);
        $list_add_on_disk_server = $this->card->list_add_on_disk_server();
        $list_add_on_disk_ram = $this->card->list_add_on_disk_ram();
        $list_add_on_ip = $this->card->list_add_on_ip();
        $billings = config('billing');
        return view('users.cards.add_card_server', compact('product', 'billings', 'list_os', 'billing_cycle',
            'list_vps_os', 'list_datacenter', 'list_server_management', 'list_raid', 'list_add_on_disk_server', 
            'list_add_on_disk_ram', 'list_add_on_ip'
        ) );
    }

    public function addCardColocation($product_id, Request $request)
    {
        $check_product_with_user = $this->product->check_product_with_user($product_id);
        if ($check_product_with_user) {
            return redirect()->route('index')->with('fails', 'Sản phẩm / Dịch vụ không có thật. Vui lòng chọn gói sản phẩm / dịch vụ trong Đăng ký dịch vụ.');
        }
        $billing_cycle = $request->get('billing_cycle');
        $product = $this->card->detail_product($product_id);
        $list_datacenter = $this->card->list_datacenter($product_id);
        $billings = config('billing');
        $config_datacenter = config('datacenter');
        $list_add_on_ip = $this->card->list_add_on_ip_colo();
        // dd($list_add_on_ip);
        return view('users.cards.add_card_colocation', compact('product', 'billings', 'billing_cycle', 'list_datacenter', 
        'config_datacenter', 'list_add_on_ip') );
    }

    public function loadState(Request $request)
    {
        $list_state = $this->product->list_state_by_location($request->get('product_special'));
        return json_encode($list_state);
    }

    public function addCardAddon( Request $request, $vpsId)
    {
        $check_vps_with_user = $this->service->check_vps_addon_with_user($vpsId);
        if (!$check_vps_with_user) {
            return redirect()->route('service.vps.on')->with('fails', 'Dịch vụ VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'cancel' ) {
            return redirect()->route('service.vps.on')->with('fails', 'Dịch vụ VPS này đã hủy, không được phép nâng cấp. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'delete_vps' ) {
            return redirect()->route('service.vps.on')->with('fails', 'Dịch vụ VPS này đã xóa, không được phép nâng cấp. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'change_user' ) {
            return redirect()->route('service.vps.on')->with('fails', 'Dịch vụ VPS này đã chuyển, không được phép nâng cấp. Quý khách vui lòng kiểm tra lại.');
        }

        $billings = config('billing');
        $vps = $this->service->detail_vps($vpsId);
        $product_addons = $this->service->get_addon_product_private(Auth::id());
        $check_time_addon = $this->service->checkTimeAddonVps($vps);
        $listOs = config('listOs');
        return view('users.cards.add_card_addon', compact('vps','billings', 'product_addons', 'listOs', 'check_time_addon'));
    }

    public function orderAddon(Request $request)
    {
        $data = $request->all();
        $check_vps_with_user = $this->service->check_vps_addon_with_user($data['vpsId']);
        if (!$check_vps_with_user) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Dịch vụ VPS không có hoặc không thuộc quyền sở hữu của quý khách. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'cancel' ) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Dịch vụ VPS này đã hủy. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'delete_vps' ) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Dịch vụ VPS này đã xóa, không được phép nâng cấp. Quý khách vui lòng kiểm tra lại.');
        }
        else if ( $check_vps_with_user->status_vps == 'change_user' ) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Dịch vụ VPS này đã chuyển, không được phép nâng cấp. Quý khách vui lòng kiểm tra lại.');
        }

        if ( $data['addon-cpu'] == 0 && $data['addon-ram'] == 0 && $data['addon-disk'] == 0 ) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Đặt hàng thất bại, tất cả cấu hình thêm không được bằng 0. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
        }
        if ( $data['addon-cpu'] < 0 || $data['addon-ram'] < 0 || $data['addon-disk'] < 0 ) {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Đặt hàng thất bại, cấu hình thêm không được nhỏ hơn 0. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
        }
        // dd($data);
        $order = $this->service->order_addon_vps($data);

        if ($order) {
            return redirect()->route('order.check_out', ['invoice_id' => $order])->with('success', 'Quý khách đã đặt hàng thành công. Quý khách vui lòng kiểm tra email và thực hiện thanh toán theo hướng dẫn.');
        } else {
            return redirect()->route( 'user.addCardAddon', $request->get('vpsId') )->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
        }
    }

    public function check_billing(Request $request)
    {
        $id = $request->get('id');
        if (!empty($request->get('addon'))) {
            $product = $this->card->detail_product($id);
            $billings = config('billing');
            $add_on = $this->card->detail_product($request->get('addon'));
            $data = [
                'product' => $product,
                'billing' => $billings,
                'pricing' => $product->pricing,
                'group_product' => $product->group_product,
                'addon' => $add_on,
                'pricing_addon' => $add_on->pricing,
            ];
        } else {
            $product = $this->card->detail_product($id);
            $billings = config('billing');
            $data = [
                'product' => $product,
                'billing' => $billings,
                'pricing' => $product->pricing,
                'group_product' => $product->group_product,
                'addon' => '',
                'pricing_addon' => '',
            ];
        }
        return json_encode($data);
    }

    public function order_server(Request $request)
    {
        $check_product_with_user = $this->product->check_product_with_user($request->get('product_id'));
        if ($check_product_with_user) {
            return json_encode([
                'error' => 1,
                'total' => 0,
                'amount' => 0,
                'message' => 'Sản phẩm / Dịch vụ không có thật. Vui lòng chọn gói sản phẩm / dịch vụ trong Đăng ký dịch vụ.'
            ]);
        }
        $total = $this->card->total_order_server($request->all());
        return json_encode($total);
    }

    public function loadAmountColo(Request $request)
    {
        $check_product_with_user = $this->product->check_product_with_user($request->get('product_id'));
        if ($check_product_with_user) {
            return json_encode([
                'error' => 1,
                'total' => 0,
                'amount' => 0,
                'message' => 'Sản phẩm / Dịch vụ không có thật. Vui lòng chọn gói sản phẩm / dịch vụ trong Đăng ký dịch vụ.'
            ]);
        }
        $total = $this->card->total_order_colocation($request->all());
        return json_encode($total);
    }

    public function check_out(Request $request)
    {
        $id = $request->get('id');
        $invoice_id = $request->get('invoice_id');
        return view('users.cards.check_out', compact('id', 'invoice_id'));
    }

    public function order(Request $request)
    {
        // $check_credit_order = $this->card->check_credit_order($request->get('quantity'), $request->get('sub_total'));
        // if (!$check_credit_order) {
        //    return redirect()->route('user.addCard', $request->get('product_id'))->with('fails', 'Đặt hàng thất bại. Tài khoản của bạn không đủ để thực hiện đăng ký đơn hàng này. Vui lòng kiểm tra lại tài khoản và thông tin sản phẩm / dịch vụ.');
        // }
        // dd($request->all());
        $type_product = $request->get('type_product');
        if ($type_product == 'VPS') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric|max:100|min:1',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'quantity.min' => 'Số lượng phải lớn hơn 0',
                'quantity.max' => 'Số lượng phải nhỏ hơn 100',
            ] );
            if (!empty($request->get('addon'))) {
                $validate = $request->validate([
                    'addon' => 'bail|required|not_in:0',
                ],
                [
                    'addon.required' => 'Loại sản phẩm Addon không được để trống',
                    'addon.not_in' => 'Loại sản phẩm Addon không được để trống',
                ] );
            }
            // dd('da den');
            $data = $request->all();
            $order = $this->card->order_vps($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng VPS thành công.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }

        }
        elseif($type_product == 'NAT-VPS') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric|max:100|min:1',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'quantity.numeric' => 'Số lượng phải lớn hơn 0',
                'quantity.max' => 'Số lượng phải nhỏ hơn 100',
            ] );
            if (!empty($request->get('addon'))) {
                $validate = $request->validate([
                    'addon' => 'bail|required|not_in:0',
                ],
                [
                    'addon.required' => 'Loại sản phẩm Addon không được để trống',
                    'addon.not_in' => 'Loại sản phẩm Addon không được để trống',
                ] );
            }
            // dd('da den 1213131');
            $data = $request->all();
            $order = $this->card->order_nat_vps($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng NAT VPS thành công.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }
        }
        elseif($type_product == 'VPS-US') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'state' => 'bail|required',
                'quantity' => 'bail|required|numeric|max:100|min:1',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'state.required' => 'Bang không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'quantity.numeric' => 'Số lượng phải lớn hơn 0',
                'quantity.max' => 'Số lượng phải nhỏ hơn 100',
            ] );
            if ( !empty($request->get('add_state')) ) {
                foreach ($request->get('add_state_type_country') as $key => $type_country) {
                    if ( empty($type_country) ) {
                        return redirect()->back()->with('fails', 'Quốc gia (Mua nhiều bang) không được để trống');
                    }
                    if ( empty( $request->get('add_state_name')[$key] ) ) {
                        return redirect()->back()->with('fails', 'Bang (Mua nhiều bang) không được để trống');
                    }
                    if ( empty( $request->get('add_state_quantity')[$key] ) ) {
                        return redirect()->back()->with('fails', 'Số lượng (Mua nhiều bang) không được để trống');
                    } else {
                        if ( !is_numeric($request->get('add_state_quantity')[$key]) ) {
                            return redirect()->back()->with('fails', 'Số lượng (Mua nhiều bang) phải là ký tự số');
                        }
                        elseif ( $request->get('add_state_quantity')[$key] < 0 ) {
                            return redirect()->back()->with('fails', 'Số lượng (Mua nhiều bang) không được nhỏ hơn 0');
                        }
                        elseif ( $request->get('add_state_quantity')[$key] > 100 ) {
                            return redirect()->back()->with('fails', 'Số lượng (Mua nhiều bang) không được lớn hơn 100');
                        }
                    }
                }
            }
            // dd($request->all(), 'da den');
            $data = $request->all();
            $order = $this->card->order_vps_us($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng VPS US thành công.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }
        }
        elseif($type_product == 'Server') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->card->order_server($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng Server thành công.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }

        }
        elseif($type_product == 'Colocation') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'datacenter' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'datacenter.required' => 'Datacenter không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ]);
            $data = $request->all();
            $order = $this->card->order_colocation($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng Colocation thành công.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }

        }
        elseif($type_product == 'Hosting-Singapore') {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ]);

            $domain_validate = $request->get('domain');
            foreach ($domain_validate as $value) {
                if ($value == null) {
                    return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')] )->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận trường domain không được để trống.');
                }
                $hostings = Hosting::get();
                foreach ($hostings as $key => $hosting) {
                    if ($hosting->domain == $value) {
                        return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')] )->with('fails', 'Đặt hàng thất bại. Domain này đã được đăng ký, quý khách vui lòng đăng ký hosting với một domain khác.');
                    }
                }
            }
            if (!empty($request->get('addon'))) {
                $validate = $request->validate([
                    'addon' => 'bail|required|not_in:0',
                ],
                [
                    'addon.required' => 'Loại sản phẩm Addon không được để trống',
                    'addon.not_in' => 'Loại sản phẩm Addon không được để trống',
                ] );
            }
            // dd($request->get('one_time_pay'));
            if (!empty($request->get('one_time_pay')) ) {
                $data = $request->all();
                $order = $this->card->order_hosting_one_time_pay_singapore($data);
                if ($order) {
                    return redirect()->route('order.check_out', ['invoice_id' => $order->id])->with('success', 'Quý khách đã đặt hàng thành công. Quý khách vui lòng kiểm tra email và thực hiện thanh toán theo hướng dẫn.');
                } else {
                    return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
                }
            }
            $data = $request->all();
            $order = $this->card->order_hosting_singapore($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_out', ['invoice_id' => $order->id])->with('success', 'Quý khách đã đặt hàng thành công. Quý khách vui lòng kiểm tra email và thực hiện thanh toán theo hướng dẫn.');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }
        }
        elseif ($type_product == 'Proxy') {
            $validate = $request->validate([
                'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric|max:100|min:1',
                'state' => 'bail|required',
            ],
            [
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'quantity.min' => 'Số lượng phải lớn hơn 0',
                'quantity.max' => 'Số lượng phải nhỏ hơn 100',
                'state.required' => 'Bang không được để trống',
            ] );
            // dd($request->all());
            $data = $request->all();
            $order = $this->card->order_proxy($data);
            // $order = true;
            if ( $order['error'] == 1 ) {
                return redirect()->back()->with('fails', 'Hiện tại hệ thống chỉ còn tạo được ' . $order['qtt'] . ' Proxy. Quý khách vui lòng sửa lại đơn hàng và xác nhận lại.' );
            } 
            elseif ( $order['error'] == 2 ) {
                return redirect()->back()->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.' );
            } 
            else {
                $order = $order['detail_order'];
                if ($order) {
                    return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng Proxy thành công.');
                } else {
                    return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
                }
            }
        }
        else {
            $validate = $request->validate([
                'type_product' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric|max:100|min:1',
            ],
            [
                'type_product.required' => 'Loại sản phẩm không được để trống',
                'type_product.not_in' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'quantity.max' => 'Số lượng phải nhỏ hơn 100',
                'quantity.numeric' => 'Số lượng phải lớn hơn 0',
            ]);

            $domain_validate = $request->get('domain');
            foreach ($domain_validate as $value) {
                if ($value == null) {
                    return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')] )->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận trường domain không được để trống.');
                }
                $hostings = Hosting::get();
                foreach ($hostings as $key => $hosting) {
                    if ($hosting->domain == $value) {
                        return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')] )->with('fails', 'Đặt hàng thất bại. Domain này đã được đăng ký, quý khách vui lòng đăng ký hosting với một domain khác.');
                    }
                }
            }
            if (!empty($request->get('addon'))) {
                $validate = $request->validate([
                    'addon' => 'bail|required|not_in:0',
                ],
                [
                    'addon.required' => 'Loại sản phẩm Addon không được để trống',
                    'addon.not_in' => 'Loại sản phẩm Addon không được để trống',
                ] );
            }
            // dd($request->get('one_time_pay'));
            if (!empty($request->get('one_time_pay')) ) {
                $data = $request->all();
                $order = $this->card->order_hosting_one_time_pay($data);
                if ($order) {
                    return redirect()->route('order.check_invoices', $order->id)->with('success', 'Quý khách đã đặt hàng thành công.');
                } else {
                    return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
                }
            }
            $data = $request->all();
            $order = $this->card->order_hosting($data);
            // $order = true;
            if ($order) {
                return redirect()->route('order.check_invoices', $order->id)->with('success', 'Quý khách đã đặt hàng thành công. ');
            } else {
                return redirect()->route('user.addCard', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
            }
        }
    }

    public function form_order_server(Request $request)
    {
      $validate = $request->validate([
          'datacenter' => 'bail|required',
          'product_id' => 'bail|required|not_in:0',
          'os' => 'bail|required',
          'server_management' => 'bail|required',
          'billing' => 'bail|required',
          'quantity' => 'bail|required|numeric|max:100|min:1',
      ],
      [
          'datacenter.required' => 'Datacenter không được để trống',
          'product_id.required' => 'Sản phẩm không được để trống',
          'product_id.not_in' => 'Sản phẩm không được để trống',
          'os.required' => 'Hệ điều hành không được để trống',
          'server_management.required' => 'Server Management không được để trống',
          'billing.required' => 'Thời gian thuê không được để trống',
          'quantity.required' => 'Số lượng không được để trống',
          'quantity.numeric' => 'Số lượng phải là ký tự số',
          'quantity.min' => 'Số lượng phải lớn hơn 0',
          'quantity.max' => 'Số lượng phải nhỏ hơn 100',
      ] );
      // dd($request->all());
      $data = $request->all();
      $order = $this->card->order_server($data);
      // $order = true;
      if ($order) {
          return redirect()->route('order.check_invoices', $order->id)->with('success', 'Đặt hàng Server thành công.');
      } else {
          return redirect()->route('user.addCardServer', [$request->get('product_id'), 'billing_cycle' => $request->get('billing')])->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
      }
    }

    public function order_vetify($token)
    {
        $verify = $this->card->verify_order($token);
        if($verify) {
            return redirect()->route('order.check_out', ['id' => $verify->id])->with('verify_success', 'Xác nhật đặt hàng thành công. Quý khách vui lòng thanh toán để hoàn thành đơn đặt hàng.');
        } else {
            return redirect()->route('order.check_out')->with('fails', 'Xác nhật đặt hàng thất bại');
        }
    }

    public function list_invoices()
    {
        $invoices = $this->card->list_invoices_with_user();
        return view('users.cards.list_invoices', compact('invoices'));
    }

    public function list_invoices_progressing()
    {
        $invoices = $this->card->list_invoices_progressing_with_user();
        return view('users.cards.list_invoices_progressing', compact('invoices'));
    }

    public function list_invoices_with_sl(Request $request)
    {
        $invoices = $this->card->list_invoices_with_sl($request->get('sl'));
        return json_encode($invoices);
    }

    public function list_invoices_paid_with_sl(Request $request)
    {
        $invoices = $this->card->list_invoices_paid_with_sl($request->get('sl'));
        return json_encode($invoices);
    }

    public function list_invoices_unpaid_with_sl(Request $request)
    {
        $invoices = $this->card->list_invoices_unpaid_with_sl($request->get('sl'));
        return json_encode($invoices);
    }

    public function list_invoices_paid()
    {
        $invoices = $this->card->list_invoices_paid_with_user();
        return view('users.cards.list_invoices_paid', compact('invoices'));
    }

    public function list_invoices_unpaid()
    {
        $invoices = $this->card->list_invoices_unpaid_with_user();
        return view('users.cards.list_invoices_unpaid', compact('invoices'));
    }

    public function check_invoices($id)
    {
        $invoice = $this->card->check_invoice($id);
        if (isset($invoice)) {
            $billing_time = config('billing');
            $billing_dashboard = config('billingDashBoard');
            // dd($invoice->expired_hostings);
            return view('users.cards.check_invoice', compact('invoice', 'billing_time', 'billing_dashboard'));
        } else {
            return redirect()->route('order.list_invoices')->with('fails', 'Hóa đơn không có thật. Quý khách vui lòng chọn các hóa đơn trong danh sách.');
        }
    }

    public function payment_invoices($id)
    {
        $invoice = $this->card->check_invoice($id);
        if (isset($invoice)) {
            $billing_time = config('billing');
            $billing_dashboard = config('billingDashBoard');
            // dd($invoice->expired_hostings);
            return view('users.cards.check_invoice', compact('invoice', 'billing_time', 'billing_dashboard'));
        } else {
            return redirect()->route('order.list_invoices')->with('fails', 'Hóa đơn không có thật. Quý khách vui lòng chọn các hóa đơn trong danh sách.');
        }
    }



    public function print($id)
    {
        $invoice = $this->card->check_invoice($id);
        return view('users.cards.print', compact('invoice'));
    }

    public function pdf($id)
    {
        $invoice = $this->card->check_invoice($id);
        $pdf = PDF::loadView('users.cards.pdf', compact('invoice'));
        $file_name = 'Hoa-don-#'. $invoice->id . '.pdf';
        return $pdf->download($file_name);
    }
    // thanh toán khi order
    public function payment(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $check_vps = $this->card->check_vps_dashboard($invoice_id);
        // dd($check_vps);
        if ( !empty($check_vps['error']) ) {
            if ( $check_vps['error'] == 1 ) {
                if ( $check_vps['success'] == 0 ) {
                    return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công. Hệ thống đang khởi tạo dịch vụ. Quý khách vui lòng đợi trong ít phút.');
                } else {
                    return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công. Hệ thống đã hoàn thành ' . $check_vps['success'] . ' dịch vụ. Hệ thống đang khởi tạo ' . $check_vps['progressing'] . ' dịch vụ. Quý khách vui lòng đợi trong ít phút.');
                }
            } elseif ( $check_vps['error'] == 2 ) {

            }
        }
        if ($check_vps == 1) {
            $payment = $this->card->payment($invoice_id);
            if ( $payment == 3 ) {
                return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Đơn hàng đã được thanh toán. Quý khách vui lòng kiểm tra lại.');
            }
            elseif ( $payment == 5 ) {
                return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Số dư của quý khách không đủ để thực hiện giao dịch này.');
            }
            elseif ( $payment == 9999 ) {
                return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán không thành công. Tại một thời điểm,
                    quý khách chỉ được thanh toán một đơn hàng. Quý khách vui lòng quay lại sau.');
            }
        }
        elseif ($check_vps == 2)  {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
        }
        elseif ( $check_vps == 5 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Số dư của quý khách không đủ để thực hiện giao dịch này.');
        }
        elseif ( $check_vps == 9999 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán không thành công. Tại một thời điểm,
            quý khách chỉ được thanh toán một đơn hàng. Quý khách vui lòng quay lại sau.');
        }
        elseif ( $check_vps == 3 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Đơn hàng đã được thanh toán. Quý khách vui lòng kiểm tra lại.');
        }
        else {
            $payment = true;
        }
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }
    // thanh toán khi order upgrade_hosting
    public function payment_upgrade(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $payment = $this->card->payment_upgrade($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán nâng cấp dịch vụ Hosting thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán dịch vụ Hosting thất bại');
        }
    }

    // thanh toán khi gia hạn
    public function payment_expired_form(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $check_expired = $this->card->check_expired_dashboard($invoice_id);
        if ( !empty($check_expired["idSuccess"]) ) {
            $error = "Thanh toán thành công các VPS <br>";
            foreach ($check_expired['list_vps_success'] as $key => $vps_success) {
                $error .= "<span class='mr-4'>- " . $vps_success . "</span><br>";
            }
            $error .= "Thanh toán thất bại các VPS <br>";
            foreach ($check_expired['list_vps_error'] as $key => $vps_error) {
                $error .= "<span class='mr-4'>- " . $vps_error . "</span><br>";
            }
            $error .= "Thanh toán các VPS còn lại <a href='/order/check-invoices/". $check_expired["idError"] ."' style='color: white;'><u>Tại đây</u></a><br>";
            return redirect()->route('order.check_invoices', $check_expired['idSuccess'])->with('errorExpired', $error);
        }
        elseif ($check_expired == 1) {
            $payment = $this->card->payment_expired($invoice_id);
        }
        elseif ($check_expired == 2)  {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
        }
        elseif ( $check_expired == 5 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Số dư của quý khách không đủ để thực hiện giao dịch này.');
        }
        elseif ( $check_expired == 3 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Đơn hàng đã được thanh toán. Quý khách vui lòng kiểm tra lại.');
        }
        else {
            $payment = true;
        }
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }

    }
    // thanh toán cloudzone point khi gia hạn
    public function payment_expired_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_expired_cloudzone_point($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }

    }

    // thanh toán khi order addon
    public function payment_addon_form(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $payment = $this->card->payment_addon($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }

    }
    // thanh toán khi order addon
    public function payment_addon_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_addon_cloudzone_point($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }

    }

    // kiểm tra session khi thanh toán
    public function check_session_payment($request)
    {
        return $this->card->check_session_payment(Auth::id());
    }
    // khi thanh toan xong - trả session về false để kh thanh toán tiếp
    public function remove_session_payment($request)
    {
        return $this->card->remove_session_payment(Auth::id());
    }

    public function loadAddon(Request $request)
    {
        $add_ons = $this->card->loadAddon($request->get('type'));
        return json_encode($add_ons);
    }

    public function payment_order($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice($invoice_id);
        return view('users.cards.payment_order', compact('invoice','history_pay'));
    }

    public function payment_upgrade_hosting($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice($invoice_id);
        return view('users.cards.payment_order_upgrade', compact('invoice','history_pay'));
    }

    public function payment_domain_order($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice_domain($invoice_id);
        return view('users.cards.payment_domain_order', compact('invoice','history_pay'));
    }

    public function payment_addon($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice_addon($invoice_id);
        // dd($invoice, $history_pay);
        return view('users.cards.payment_addon', compact('invoice','history_pay'));
    }

    public function payment_expired($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice_expired($invoice_id);
        return view('users.cards.payment_expired', compact('invoice','history_pay'));
    }

    public function payment_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_off', $payment_off)->with('success', 'Yêu cầu thanh toán đơn đặt hàng thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function payment_upgrade_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_upgrade_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_off', $payment_off)->with('success', 'Yêu cầu thanh toán đơn đặt hàng thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function checkout_payment_off($id)
    {
        $pay_in_office = $this->payin->detail_pay_in_with_id($id);
        return view('users.cards.checkout_payment_off', compact('pay_in_office'));
    }

    public function payment_expired_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_expired_off', $payment_off);
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function checkout_payment_expired_off($id)
    {
        $pay_in_office = $this->payin->detail_pay_in_with_id($id);
        return view('users.cards.checkout_payment_expired_off', compact('pay_in_office'));
    }

    public function payment_addon_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_addon_off', $payment_off);
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function checkout_payment_addon_off($id)
    {
        $pay_in_office = $this->payin->detail_pay_in_with_id($id);
        return view('users.cards.checkout_payment_addon_off', compact('pay_in_office'));
    }

    public function payment_change_ip($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice($invoice_id);
        return view('users.cards.payment_change_ip', compact('invoice','history_pay'));
    }

    public function payment_change_ip_form(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $payment = $this->card->payment_change_ip_form($invoice_id);
        if ($payment == 2)  {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
        }
        elseif ( $payment == 5 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Số dư của quý khách không đủ để thực hiện giao dịch này.');
        }
        elseif ( $payment == 9999 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán không thành công. Tại một thời điểm,
            quý khách chỉ được thanh toán một đơn hàng. Quý khách vui lòng quay lại sau.');
        }
        elseif ( $payment == 3 ) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Đơn hàng đã được thanh toán. Quý khách vui lòng kiểm tra lại.');
        }
        else {
            $payment = true;
        }
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function payment_change_ip_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_change_ip_cloudzone_point($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }
    }

    public function payment_change_ip_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_change_ip_off', $payment_off);
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function checkout_payment_change_ip_off($id)
    {
        $pay_in_office = $this->payin->detail_pay_in_with_id($id);
        return view('users.cards.checkout_payment_change_ip_off', compact('pay_in_office'));
    }

    //add card domain
    public function addCardDomain(Request $request)
    {
        $domain = $request->get('domain');
        $ext = ltrim(strstr($domain, '.'), '.');
        // $product = $this->domain_product->get_domain_product($id);
        $product = $this->domain_product->get_domain_product_by_ext($ext);
        $billings = config('billing_domain');
        $list_city = config('city');
        $list_customers = $this->customer->list_customer_with_user();
        if ($product) {
            return view('users.cards.add_card_domain', compact('domain', 'product', 'billings', 'list_customers', 'list_city'));
        } else {
            return redirect()->route('user.domain.search')->with('fails', 'Truy vấn lỗi, vui lòng thử lại');
        }

    }
    //add card domain
    public function addCardDomainPromotion($event_id, Request $request)
    {
        $domain = $request->get('domain');
        $ext = ltrim(strstr($domain, '.'), '.');
        // $product = $this->domain_product->get_domain_product($id);
        $product = $this->domain_product->get_domain_product_by_ext($ext);
        $list_city = config('city');
        $list_customers = $this->customer->list_customer_with_user();
        return view('users.cards.add_card_domain_promotion', compact('domain', 'product' , 'list_customers', 'list_city', 'event_id'));
    }

    public function check_billing_domain(Request $request)
    {
        $id = $request->get('id');
        $product = $this->domain_product->get_domain_product($id);
        $billings = config('billing_domain');
        return $data = [
            'product' => $product,
            'billing' => $billings,
        ];
        // return json_encode($data);
    }
    public function order_domain(RegisterDomainValidate $request)
    {
        $data = $request->all();
        //Xác thực thông tin chứng minh nhân dân
        if (!empty($data['cmnd_before'])) {
            $validate = $request->validate([
                'cmnd_before'        =>  'bail|required|mimes:jpeg,jpg,png|max:24048'
            ],
            [
                'cmnd_before.required' => 'Ảnh chứng minh nhân mặt trước không được để trống',
                'cmnd_before.mimes' => 'Định dạng ảnh chứng minh nhân dân mặt trước không chính xác (Các định dạng ảnh cho phép jpeg,jpg,png)',
                'cmnd_before.max' => 'Ảnh chứng minh nhân mặt trước không vượt quá 24MB'
            ] );
        } elseif (empty($data['cmnd_before']) && empty($data['cmnd_before_in_profile'])) {
            $validate = $request->validate([
                'cmnd_before'        =>  'required'
            ],
            [
                'cmnd_before.required' => 'Ảnh chứng minh nhân mặt trước không được để trống',
            ] );
        }
        if (!empty($data['cmnd_after'])) {
            $validate = $request->validate([
                'cmnd_after'        =>  'bail|required|mimes:jpeg,jpg,png|max:24048'
            ],
            [
                'cmnd_after.required' => 'Ảnh chứng minh nhân mặt sau không được để trống',
                'cmnd_after.mimes' => 'Định dạng ảnh chứng minh nhân dân mặt sau không chính xác (Các định dạng ảnh cho phép jpeg,jpg,png)',
                'cmnd_after.max' => 'Ảnh chứng minh nhân mặt sau không vượt quá 24MB'
            ] );
        } elseif (empty($data['cmnd_after']) && empty($data['cmnd_after_in_profile'])) {
            $validate = $request->validate([
                'cmnd_after'        =>  'required'
            ],
            [
                'cmnd_after.required' => 'Ảnh chứng minh nhân mặt sau không được để trống',
            ] );
        }

        $order = $this->card->order_domain($data);
        if ($order) {
            return redirect()->route('order.check_out', ['invoice_id' => $order->id])->with('success', 'Quý khách đã đặt hàng tên miền thành công. Quý khách vui lòng kiểm tra email và thực hiện thanh toán theo hướng dẫn.');
        } else {
            $data = [
                'id' => $data['product_id'],
                'domain' => $data['domain']
            ];
            return redirect()->route('user.addCardDomain', $data)->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
        }
    }

    public function order_domain_promotion(RegisterDomainValidate $request)
    {
        $data = $request->all();
        // dd($data);
        $order = $this->card->order_domain_promotion($data);
        // $order = true;
        if ($order) {
            return redirect()->route('order.check_out', ['invoice_id' => $order->id])->with('promotion', 'Quý khách đã đặt hàng tên miền khuyến mãi thành công. Quý khách vui lòng kiểm tra email và làm theo hướng dẫn.');
        } else {
            $data = [
                'id' => $data['product_id'],
                'domain' => $data['domain']
            ];
            return redirect()->route('user.addCardDomainPromotion', $data)->with('fails', 'Đặt hàng thất bại. Quý khách vui lòng kiểm tra lại đơn hàng và xác nhận lại.');
        }

    }

    public function payment_domain_expired_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_domain_expired_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_off', $payment_off)->with('success', 'Yêu cầu thanh toán đơn đặt hàng thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function payment_domain_off(Request $request)
    {
        $type = $request->get('method_gd_invoice');
        $invoice_id = $request->get('invoice_id');
        $payment_off = $this->card->payment_domain_off($type, $invoice_id);
        if ($payment_off) {
            return redirect()->route('invoice.checkout_payment_off', $payment_off)->with('success', 'Yêu cầu thanh toán đơn đặt hàng thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }
    }

    public function order_domain_expired(Request $request)
    {
        $data = $request->all();
        $order_domain_expired = $this->card->order_domain_expired($data);
        if ($order_domain_expired) {
            return redirect()->route('order.check_out', ['invoice_id' => $order_domain_expired->id])->with('success', 'Quý khách đã đặt hàng gia hạn tên miền thành công. Quý khách vui lòng kiểm tra email và thực hiện thanh toán theo hướng dẫn.');
        } else {
            $user_id = Auth::user()->id;
            $domains = $this->domain->get_domain($user_id);
            return redirect()->route('user.domain.index', $domains)->with('fails', 'Gia hạn thất bại. Quý khách vui lòng kiểm tra lại.');
        }

    }

    public function payment_domain_expired_order($invoice_id)
    {
        $invoice = $this->card->check_invoice($invoice_id);
        $history_pay = $this->payin->check_invoice_domain_expired($invoice_id);
        return view('users.cards.payment_domain_expired_order', compact('invoice','history_pay'));
    }

    //thanh toán khi gia hạn domain
    public function payment_domain_expired(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_credit = $this->card->check_credit($invoice_id);
        if (!$check_credit) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Tài khoản của bạn không đủ để thực hiện thanh toán hóa đơn này');
        }
        $payment = $this->card->payment_domain_expired($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán thất bại');
        }

    }

    //thanh toán khi gia hạn domain
    public function payment_domain_expired_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        // dd($invoice_id);
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_domain_expired_point_cloudzone($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }

    }

    public function payment_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_order_with_point_cloudzone($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }
    }

    public function payment_upgrade_cloudzone_point(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        $check_point = $this->card->check_point($invoice_id);
        if (!$check_point) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Điểm Cloudzone Point của quý khách không đủ để thực hiện thanh toán hóa đơn này. Quý khách vui lòng chọn hình thức thành toán khác để hoàn thành thanh toán đơn hàng.');
        }
        $payment = $this->card_point->payment_order_upgrade_with_point_cloudzone($invoice_id);
        if ($payment) {
            return redirect()->route('order.check_invoices', $invoice_id)->with('success', 'Thanh toán bằng điểm Cloudzone Point thành công');
        } else {
            return redirect()->route('order.check_invoices', $invoice_id)->with('fails', 'Thanh toán bằng điểm Cloudzone Point thất bại');
        }
    }

    public function check_promotion(Request $request)
    {
        $data = $this->card->check_promotion($request->get('type_order'), $request->get('ma_km'), $request->get('product_id'), $request->get('billing'));
        return json_encode($data);
    }

    public function add_customer(Request $request)
    {
        $add_customer = $this->customer->card_add_customer($request->all());
        return json_encode($add_customer);

    }

    public function add_credit_invoice($invoice_id)
    {
      $invoice = $this->card->check_invoice($invoice_id);
      if (isset($invoice)) {
          $invoice->progressing = true;
          $invoice->save();
          // dd($invoice->expired_hostings);
          return redirect()->route('user.pay_in_online')->with('success', 'Hóa đơn được đưa vào danh sách chờ.');
      } else {
          return redirect()->route('order.list_invoices')->with('fails', 'Hóa đơn không có thật. Quý khách vui lòng chọn các hóa đơn trong danh sách.');
      }
    }

}
