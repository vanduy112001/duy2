<?php

namespace App\Http\Controllers;

use App\Model\MetaGroupProduct;
use Illuminate\Http\Request;

class MetaGroupProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MetaGroupProduct  $metaGroupProduct
     * @return \Illuminate\Http\Response
     */
    public function show(MetaGroupProduct $metaGroupProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\MetaGroupProduct  $metaGroupProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(MetaGroupProduct $metaGroupProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MetaGroupProduct  $metaGroupProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MetaGroupProduct $metaGroupProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MetaGroupProduct  $metaGroupProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(MetaGroupProduct $metaGroupProduct)
    {
        //
    }
}
