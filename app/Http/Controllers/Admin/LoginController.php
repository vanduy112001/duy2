<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Factories\AdminFactories;
use App\Model\LogActivity;

class LoginController extends Controller
{
    protected $login;
    protected $log_activity;

    public function __construct()
    {
        $this->login = AdminFactories::loginRepositories();
        $this->log_activity = new LogActivity();
    }

    public function index(Request $request)
    {
        if (Auth::check()) {
            $user = $this->login->detail(Auth::user()->id);
            if(!empty($user->user_meta->role)) {
                if($user->user_meta->role == 'admin') {
                    if ( $request->session()->has('admin_id') ) {
                        $request->session()->put('admin_id', Auth::user()->id);
                        $request->session()->put('admin_email', Auth::user()->email);
                        $request->session()->put('admin_passwd', Auth::user()->password);
                    }
                    return redirect()->route('admin.home');
                } else {
                    return redirect(route('index'));
                }

            } else {
                return view('admin.login');
            }
        } else {
            return view('admin.login');
        }
    }

    public function login(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Tạo log
            $data_log = [
               'user_id' => Auth::user()->id,
               'action' => 'đăng nhập',
               'model' => 'admin',
               'description' => '',
            ];
            $this->log_activity->create($data_log);
            // phân quyền user
            $role = $this->login->detail(Auth::user()->id)->user_meta->role;
            if ($role == 'admin' || $role == 'editor') {
                $request->session()->put('admin_id', Auth::user()->id);
                $request->session()->put('admin_email', Auth::user()->email);
                $request->session()->put('admin_passwd', Auth::user()->password);
                return redirect()->route('admin.home')->with('success', 'Đăng nhập thành công');
            } else {
                return redirect()->route('login');
            }

        } else {
            return redirect()->route('admin.login')->with('fails', 'Đăng nhập thất bại');
        }
    }

    public function logout(Request $request)
    {
        // Tạo log
        $data_log = [
           'user_id' => Auth::user()->id,
           'action' => 'logout',
           'model' => 'admin',
           'description' => '',
        ];
        $this->log_activity->create($data_log);
        $request->session()->forget('admin_id');
        $request->session()->forget('admin_email');
        $request->session()->forget('admin_passwd');
        $request->session()->forget('login_momo');
        Auth::logout();
        return redirect()->route('admin.login');
    }


    public function login_at_admin(Request $request)
    {
        $check = $this->login->check_login_at_admin($request->session()->get('admin_id'), $request->session()->get('admin_email'), $request->session()->get('admin_passwd') );
        if ($check) {
            Auth::logout();
            Auth::loginUsingId($request->session()->get('admin_id'));
            return redirect()->route('admin.home');
        } else {
            return redirect()->route('index');
        }
    }
}
