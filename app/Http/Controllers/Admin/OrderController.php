<?php

namespace App\Http\Controllers\Admin;

use App\Model\Order;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class OrderController extends Controller
{
    protected $order;
    protected $promotion;
    protected $product;

    public function __construct()
    {
        $this->order = AdminFactories::orderRepositories();
        $this->promotion = AdminFactories::promotionRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->server_hosting = AdminFactories::serverHostingRepositories();
    }

    public function index()
    {
        $orders = $this->order->list_orders();
        return view('admin.orders.index', compact('orders'));
    }

    public function create()
    {
        $users = $this->order->users();
        $promotions = $this->promotion->list();
        $type_orders = config('type_order');
        $type_products = config('type_product');
        $invoices = config('invoices');
        return view('admin.orders.create', compact('users', 'promotions', 'type_orders', 'type_products', 'invoices'));
    }

    public function list_product(Request $request)
    {
        $action = $request->get('action');
        $user_id = $request->get('user_id');
        $products = $this->product->get_product_with_action($action, $user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        if ($action == 'Hosting') {
            $data = [
                'product' => $products,
                'billing' => $billings,
                'invoices' => $invoices,
                'server_hosting' => $this->server_hosting->list_server_hosting_vn(),
            ];
        } else {
            $data = [
                'product' => $products,
                'billing' => $billings,
                'invoices' => $invoices
            ];
        }
        return json_encode($data);
    }

    public function list_product_addon(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_vps_with_user($user_id);
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_vps' => $list_vps,
        ];
        return json_encode($data);
    }

    public function list_expire_vps(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_vps_with_user_expire($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_vps' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function list_expire_vps_us(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_vps_us_with_user_expire($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_vps' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function list_expire_server(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_server_with_user_expire($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_server' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function list_expire_colocation(Request $request)
    {
        $user_id = $request->get('user_id');
        $list_vps = $this->order->list_expire_colocation($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'invoices' => $invoices,
            'list_colo' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function list_expire_hosting(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_hosting_with_user_expire($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_hosting' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function list_expire_email_hosting(Request $request)
    {
        $user_id = $request->get('user_id');
        $products = $this->product->get_addon_product($user_id);
        $list_vps = $this->order->get_email_hosting_with_user_expire($user_id);
        $billings = config('billing');
        $invoices = config('invoices');
        $data = [
            'product' => $products,
            'invoices' => $invoices,
            'list_hosting' => $list_vps,
            'billing' => $billings,
        ];
        return json_encode($data);
    }

    public function get_total_vps_expire(Request $request)
    {
        $total = $this->order->get_total_vps_expire($request->get('id'), $request->get('billing_cycle'));
        return json_encode($total);
    }

    public function get_total_hosting_expire(Request $request)
    {
        $total = $this->order->get_total_hosting_expire($request->get('id'), $request->get('billing_cycle'));
        return json_encode($total);
    }

    public function get_total_email_hosting_expire(Request $request)
    {
        $total = $this->order->get_total_email_hosting_expire($request->get('id'), $request->get('billing_cycle'));
        return json_encode($total);
    }

    public function choose_addon_vps(Request $request)
    {
        $user_id = $request->get('user_id');
        $vps_id = $request->get('vps_id');
        $data = $this->order->choose_addon_vps($user_id, $vps_id);
        return json_encode($data);
    }

    public function get_product(Request $request)
    {
        $id = $request->get('id');
        $product = $this->product->get_product_json($id);
        $billings = config('billing');
        $data = [
            'product' => $product,
            'group_product' => $product->group_product,
            'pricing' => $product->pricing,
            'billing' => $billings,
            'os_product' => !empty($product->vps_os) ? $product->vps_os : '',
            'os' => config('os'),
        ];
        return json_encode($data);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $type_product = $request->get('type_products');
        if ($type_product == 'VPS' || $type_product == 'NAT-VPS' || $type_product == 'VPS-US') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );

            $data = $request->all();
            if ($type_product == 'VPS' || $type_product == 'NAT-VPS') {
              $order = $this->order->create_order_vps($data);
            }
            else {
              $order = $this->order->create_order_vps_us($data);
            }

            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }

        }
        elseif ($type_product == 'Server') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'server_name' => 'bail|required',
                'chip' => 'bail|required',
                'ram' => 'bail|required',
                'disk' => 'bail|required',
                'ip' => 'bail|required',
                'amount' => 'bail|required',
                'location' => 'bail|required',
                // 'product_id' => 'bail|required|not_in:0',
                // 'os' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'server_name.required' => 'Dòng Server không được để trống',
                'chip.required' => 'Chip không được để trống',
                'ram.required' => 'RAM không được để trống',
                'disk.required' => 'DISK không được để trống',
                'ip.required' => 'IP không được để trống',
                'location.required' => 'Location không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'amount.required' => 'Giá dịch vụ không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );

            $data = $request->all();
            // dd($data);
            $order = $this->order->create_order_server($data);

            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng Server thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng Server thất bại');
            }
        }
        elseif ($type_product == 'Colocation') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'type_colo' => 'bail|required|not_in:0',
                'ip' => 'bail|required',
                'amount' => 'bail|required',
                'bandwidth' => 'bail|required',
                'location' => 'bail|required',
                'power' => 'bail|required',
                // 'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'type_colo.required' => 'Loại Colocation không được để trống',
                'ip.required' => 'IP không được để trống',
                'location.required' => 'Data Center không được để trống',
                'amount.required' => 'Giá dịch vụ không được để trống',
                'bandwidth.required' => 'Băng thông không được để trống',
                'power.required' => 'Công xuất nguồn không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );

            $data = $request->all();
            $order = $this->order->create_order_colocation($data);

            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng Server thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng Server thất bại');
            }
        }
        elseif ($type_product == 'Hosting' || $type_product == 'Hosting-Singapore') {
            $qtt = $request->get('quantity');
            if ($qtt == 1) {
                $validate = $request->validate([
                    'user_id' => 'bail|required',
                    'type_products' => 'bail|required|not_in:0',
                    'product_id' => 'bail|required|not_in:0',
                    'quantity' => 'bail|required|numeric',
                    'domain' => 'bail|required|unique:hostings,domain'
                ],
                [
                    'user_id.required' => 'Tên khách hàng không được để trống',
                    'type_products.required' => 'Loại sản phẩm không được để trống',
                    'product_id.required' => 'Sản phẩm không được để trống',
                    'product_id.not_in' => 'Sản phẩm không được để trống',
                    'quantity.required' => 'Số lượng không được để trống',
                    'quantity.numeric' => 'Số lượng phải là ký tự số',
                    'domain.required' => 'Trường domain không được để trống',
                    'domain.unique' => 'Domain đã được sử dụng',
                ] );

                $data = $request->all();
                $order = $this->order->create_order_hosting($data);
                if($order) {
                    return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
                } else {
                    return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
                }
            } else {
                $validate = $request->validate([
                    'user_id' => 'bail|required',
                    'type_products' => 'bail|required|not_in:0',
                    'product_id' => 'bail|required|not_in:0',
                    'quantity' => 'bail|required|numeric',
                    'domain' => 'bail|required|unique:hostings,domain'
                ],
                [
                    'user_id.required' => 'Tên khách hàng không được để trống',
                    'type_products.required' => 'Loại sản phẩm không được để trống',
                    'product_id.required' => 'Sản phẩm không được để trống',
                    'product_id.not_in' => 'Sản phẩm không được để trống',
                    'quantity.required' => 'Số lượng không được để trống',
                    'quantity.numeric' => 'Số lượng phải là ký tự số',
                    'domain.required' => 'Trường domain không được để trống',
                    'domain.unique' => 'Domain đã được sử dụng',
                ]);
                $data = $request->all();
                $order = $this->order->create_order_hostings($data);
                if($order) {
                    return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
                } else {
                    return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
                }
            }
        }
        elseif ($type_product == 'Email Hosting') {
            $qtt = $request->get('quantity');
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric',
                'domain' => 'bail|required|unique:hostings,domain'
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'domain.required' => 'Trường domain không được để trống',
                'domain.unique' => 'Domain đã được sử dụng',
            ] );

            $data = $request->all();
            $order = $this->order->create_order_email_hosting($data);
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Addon-VPS') {
          $validate = $request->validate([
              'user_id' => 'bail|required',
              'type_products' => 'bail|required|not_in:0',
              'vps_id' => 'bail|required|numeric',
          ],
          [
              'user_id.required' => 'Tên khách hàng không được để trống',
              'type_products.required' => 'Loại sản phẩm không được để trống',
              'vps_id.required' => 'VPS không được để trống',
              'vps_id.numeric' => 'VPS phải là ký tự số',
          ] );
          $add_on_cpu = $request->get('addon_cpu');
          $add_on_ram = $request->get('addon_ram');
          $add_on_disk = $request->get('addon_disk');
          if ($add_on_cpu == 0 && $add_on_ram == 0 && $add_on_disk == 0) {
              return redirect()->route('admin.orders.create')->with('fails', 'Vui lòng nhập số lượng Addon VPS');
          }
          $order = $this->order->create_order_addon_vps($request->all());
          if($order) {
              return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
          } else {
              return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
          }
        }
        elseif ($type_product == 'Addon-Server') {
          $validate = $request->validate([
              'user_id' => 'bail|required',
              'type_products' => 'bail|required|not_in:0',
              'server_id' => 'bail|required|numeric',
              'total_addon_server' => 'bail|required|numeric',
          ],
          [
              'user_id.required' => 'Tên khách hàng không được để trống',
              'type_products.required' => 'Loại sản phẩm không được để trống',
              'server_id.required' => 'Server không được để trống',
              'server_id.numeric' => 'Server phải là ký tự số',
              'total_addon_server.required' => 'Giá đơn hàng Addon Server không được để trống',
              'total_addon_server.numeric' => 'Giá đơn hàng Addon Server phải là ký tự số',
          ] );
          $add_on_ip = $request->get('addon_ip_server');
          $add_on_ram = $request->get('addon_ram_server');
          $add_on_disk = $request->get('addon_ram_disk');
          if ($add_on_ip == 0 && $add_on_ram == 0 && $add_on_disk == 0) {
              return redirect()->route('admin.orders.create')->with('fails', 'Vui lòng nhập số lượng Addon Server');
          }
          $order = $this->order->create_order_addon_server($request->all());
          if($order) {
              return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
          } else {
              return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
          }
        }
        elseif ($type_product == 'Change-IP') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'vps_id' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'vps_id.required' => 'VPS không được để trống',
                'vps_id.numeric' => 'VPS phải là ký tự số',
            ] );
            // dd($request->all());
            $order = $this->order->create_order_change_ip($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Upgrade-Hosting') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'hosting_id' => 'bail|required|numeric',
                'product_upgradet' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'hosting_id.required' => 'Hosting khách hàng không được để trống',
                'hosting_id.numeric' => 'Hosting phải là ký tự số',
                'product_upgradet.required' => 'Gói Hosting nâng cấp không được chọn',
                'product_upgradet.numeric' => 'Gói Hosting nâng cấp phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_upgrade_hosting($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn VPS') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'vps_id' => 'bail|required',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'vps_id.required' => 'VPS khách hàng không được để trống',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_expire_vps($request->all());
            if ( !empty($order['error']) ) {
                $error = "Thanh toán thành công các VPS <br>";
                foreach ($order['list_vps_success'] as $key => $vps_success) {
                    $error .= "<span class='mr-4'>- " . $vps_success . "</span><br>";
                }
                $error .= "Thanh toán thất bại các VPS <br>";
                foreach ($order['list_vps_error'] as $key => $vps_error) {
                    $error .= "<span class='mr-4'>- " . $vps_error . "</span><br>";
                }
                return redirect()->route('admin.orders.index')->with('error', $error);
            }
            elseif($order == 1) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn VPS US') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'vps_id' => 'bail|required',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'vps_id.required' => 'VPS khách hàng không được để trống',
                'vps_id.numeric' => 'VPS phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_expire_vps_us($request->all());
            if ( !empty($order['error']) ) {
                $error = "Thanh toán thành công các VPS <br>";
                foreach ($order['list_vps_success'] as $key => $vps_success) {
                    $error .= "<span class='mr-4'>- " . $vps_success . "</span><br>";
                }
                $error .= "Thanh toán thất bại các VPS <br>";
                foreach ($order['list_vps_error'] as $key => $vps_error) {
                    $error .= "<span class='mr-4'>- " . $vps_error . "</span><br>";
                }
                return redirect()->route('admin.orders.index')->with('error', $error);
            }
            elseif($order == 1) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn Hosting') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'hosting_id' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'hosting_id.required' => 'Hosting khách hàng không được để trống',
                'hosting_id.numeric' => 'Hosting phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_expire_hosting($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn Email Hosting') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'email_hosting_id' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'email_hosting_id.required' => 'Email Hosting khách hàng không được để trống',
                'email_hosting_id.numeric' => 'Email Hosting phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_email_expire_hosting($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn Server') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'server_id' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'server_id.required' => 'Server khách hàng không được để trống',
                'server_id.numeric' => 'Server phải là ký tự số',
            ] );
            $data = $request->all();
            $order = $this->order->create_order_expire_server($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        elseif ($type_product == 'Gia hạn Colocation') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'colo_id' => 'bail|required|numeric',
                'col_amount' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'colo_id.required' => 'Colocation của khách hàng không được để trống',
                'colo_id.numeric' => 'Colocation phải là ký tự số',
                'col_amount.required' => 'Giá cho thuê không được để trống',
                'col_amount.numeric' => 'Giá cho thuê phải là ký tự số',
            ] );
            $data = $request->all();
            // dd($data);
            $order = $this->order->create_order_expire_colocation($request->all());
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Tạo đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.index')->with('fails', 'Tạo đơn hàng thất bại');
            }
        }
        return redirect()->route('admin.orders.create')->with('fails', 'Vui lòng chọn Loại sản phẩm / Dịch vụ');
    }

    public function edit($id)
    {
        $order = $this->order->detail($id);
        $order_detail = $this->order->detail_order($id);
        // dd($order,$order_detail);
        $users = $this->order->users();
        $promotions = $this->promotion->list();
        $type_orders = config('type_order');
        $type_products = config('type_product');
        $products = $this->product->get_product_with_action($order_detail->type);
        $billings = config('billing');
        $invoices = config('invoices');
        return view('admin.orders.edit', compact('order', 'order_detail','users', 'promotions','type_orders', 'type_products', 'billings', 'invoices', 'products') );
    }

    public function update(Request $request)
    {
        $type_product = $request->get('type_products');
        if ($type_product == 'VPS') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );

            $data = $request->all();
            $order = $this->order->edit_order_vps($data);

            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Chỉnh sửa đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.edit')->with('fails', 'Chỉnh sửa đơn hàng thất bại');
            }

        } else if ($type_product == 'Server') {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'os' => 'bail|required',
                'quantity' => 'bail|required|numeric',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'os.required' => 'Hệ điều hành không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
            ] );

            $data = $request->all();
            $order = $this->order->edit_order_server($data);

            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Chỉnh sửa đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.edit')->with('fails', 'Chỉnh sửa đơn hàng thất bại');
            }
        }  else {
            $validate = $request->validate([
                'user_id' => 'bail|required',
                'type_products' => 'bail|required|not_in:0',
                'product_id' => 'bail|required|not_in:0',
                'quantity' => 'bail|required|numeric',
                'user' => 'bail|required',
                'password' => 'bail|required',
            ],
            [
                'user_id.required' => 'Tên khách hàng không được để trống',
                'type_products.required' => 'Loại sản phẩm không được để trống',
                'product_id.required' => 'Sản phẩm không được để trống',
                'product_id.not_in' => 'Sản phẩm không được để trống',
                'quantity.required' => 'Số lượng không được để trống',
                'quantity.numeric' => 'Số lượng phải là ký tự số',
                'domain.required' => 'Trường domain không được để trống',
                'user.required' => 'Tài khoản hosting không được để trống',
                'password.required' => 'Mật khẩu tài khoản hosting không được để trống',
            ]);
            $data = $request->all();
            $order = $this->order->edit_order_hosting($data);
            if($order) {
                return redirect()->route('admin.orders.index')->with('success', 'Chỉnh sửa đơn hàng thành công');
            } else {
                return redirect()->route('admin.orders.edit')->with('fails', 'Chỉnh sửa đơn hàng thất bại');
            }
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->order->delete($id);
        if($delete) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }

    }

    public function get_order_confirm(Request $request)
    {
        $id = $request->get('id');
        $order = $this->order->get_order_confirm($id);
        return json_encode($order);
    }

    public function form_order_confirm(Request $request)
    {
        $data = $request->all();
        // return json_encode($data);
        $confirm_vps = $this->order->confirm_order($data);
        if ($confirm_vps) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    public function filter_user(Request $request)
    {
       $data = $this->order->filter_user($request->get('user_id'), $request->get('status'));
       return json_encode($data);
    }

    public function filter_status(Request $request)
    {
       $data = $this->order->filter_status($request->get('user_id'), $request->get('status'));
       return json_encode($data);
    }

    public function get_addon_product(Request $request)
    {
      $data = $this->product->get_addon_product($request->get('user_id'));
      return json_encode($data);
    }

    public function get_product_upgrade_hosting(Request $request)
    {
      $data = $this->order->get_product_upgrade_hosting($request->get('user_id'));
      return json_encode($data);
    }

    public function choose_upgrade_hosting(Request $request)
    {
        $user_id = $request->get('user_id');
        $hosting_id = $request->get('hosting_id');
        $data = $this->order->choose_upgrade_hosting($user_id, $hosting_id);
        return json_encode($data);
    }

    public function choose_change_ip_vps(Request $request)
    {
       $pricing = $this->product->choose_change_ip_vps($request->get('user_id'));
       return json_encode($pricing);
    }

    public function edit_total_order(Request $request)
    {
       $edit_total = $this->order->edit_total_order($request->all());
       return json_encode($edit_total);
    }

    public function list_order_of_personal()
    {
       $data = $this->order->list_order_of_personal();
       return json_encode($data);
    }

    public function list_order_of_enterprise()
    {
       $data = $this->order->list_order_of_enterprise();
       return json_encode($data);
    }

}
