<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class TutorialController extends Controller
{
    protected $tutorial;

    public function __construct()
    {
       $this->tutorial = AdminFactories::tutorialRepositories();
    }

    public function index()
    {
       $tutorials = $this->tutorial->get_all_tutorial();
       return view('admin.tutorials.index', compact('tutorials'));
    }

    public function list_tutorial()
    {
        $tutorials = $this->tutorial->list_tutorial();
        return json_encode($tutorials);
    }

    public function detail(Request $request)
    {
        $tutorial = $this->tutorial->detail_tutorial($request->get('id'));
        return json_encode($tutorial);
    }

    public function delete(Request $request)
    {
        $tutorial = $this->tutorial->delete_tutorial($request->get('id'));
        return json_encode($tutorial);
    }

    public function actions(Request $request)
    {
       if ($request->get('action') == 'create') {
          $action = $this->tutorial->create($request->all());
          if ($action) {
             return json_encode('Tạo liên kết hướng dẫn thành công!');
          } else {
             return json_encode('Tạo liên kết hướng dẫn thất bại!');
          }
       } else {
           $action = $this->tutorial->update($request->all());
           if ($action) {
              return json_encode('Chỉnh sửa liên kết hướng dẫn thành công!');
           } else {
              return json_encode('Chỉnh sửa liên kết hướng dẫn thất bại!');
           }
       }
    }

}
