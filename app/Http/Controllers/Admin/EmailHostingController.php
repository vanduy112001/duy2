<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class EmailHostingController extends Controller
{
    protected $colo;
    protected $order;

    public function __construct()
    {
        $this->email_hosting = AdminFactories::emailHostingRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->order = AdminFactories::orderRepositories();
    }

    public function index()
    {
        // dd('da den');
        $email_hostings = $this->email_hosting->list_email_hostings();
        return view('admin.email_hosting.index', compact('email_hostings'));
    }

    public function detail($id)
    {
        // dd('da den');
        $detail = $this->email_hosting->detail($id);
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_with_action($detail->detail_order->type, $detail->user_id);
        return view('admin.email_hosting.detail', compact('detail' ,  'billings', 'users', 'products'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $update = $this->email_hosting->update($request->all());
        if ( $update ) {
            return redirect()->route('admin.email_hosting.index')->with('success', 'Chỉnh sửa Email Hosting '. $request->get('domain') .' thành công');
        } else {
            return redirect()->route('admin.email_hosting.detail', $request->get('ip'))->with('fails', 'Chỉnh sửa Email Hosting '. $request->get('domain') .' thất bại');
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->email_hosting->delete($request->get('id'));
        return json_encode($delete);
    }

}
