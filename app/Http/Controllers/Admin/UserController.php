<?php

namespace App\Http\Controllers\Admin;

use App\Mail\SendMailUser;
use App\Model\User;
use App\Factories\AdminFactories;
use Highlight\RegEx;
use Illuminate\Http\Request;
use App\Http\Requests\UserValidate;
use App\Http\Requests\UserUpdateValidate;
use App\Http\Requests\AdminPayMentValidate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Hash;
use DomPDF;
use PDF;
use SnappyImage;
use JasperPHP;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    protected $userRepository;
    protected $hostingRepository;
    protected $vpsRepository;
    protected $serverRepository;
    protected $payment;
    protected $admin_role;

    public function __construct()
    {
        $this->userRepository = AdminFactories::userRepositories();
        $this->hostingRepository = AdminFactories::hostingRepositories();
        $this->vpsRepository = AdminFactories::vpsRepositories();
        $this->serverRepository = AdminFactories::serverRepositories();
        $this->payment = AdminFactories::paymentRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->admin_role = AdminFactories::adminRoleRepositories();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Hiển thị danh sách users
    public function index(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'listUser') ) {
            if ($request->ajax()) {
                // dd($request->all());

                //Bắt request nhóm khách hàng ở phần tử thứ 3, tương ứng cột thứ 4 trong table view
                $user_type_group = $request->input('columns.3.search.value');
                $group_user = $request->input('columns.5.search.value');

                $columns = ['id', 'name', 'email', 'user_type', 'phone', 'group_user', 'credit_value', 'credit_total', 'company','source_reg_acc', 'created_at', 'action'];
                $limit = (int)$request->input('length');
                $start = (int)$request->input('start');
                $order = $columns[(int)$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                if(empty($request->input('search.value')))
                {
                    $totalFiltered = $this->userRepository->count_users($user_type_group, $group_user);
                    $users = $this->userRepository->get_all_to_datatale($user_type_group, $group_user, $start, $limit, $order, $dir);
                }
                else {
                    $search = $request->input('search.value');
                    $totalFiltered = $this->userRepository->count_users_with_search($user_type_group, $group_user, $search);
                    $users = $this->userRepository->get_all_search_to_datatable($user_type_group, $group_user, $search, $start, $limit, $order, $dir);
                }

                $data = [];
                if(!empty($users))
                {
                    foreach ($users as $user)
                    {
                        $action = '<a class="btn btn-primary btn-xs" href="'. route('admin.user.edit',['id' => $user->id]) .'" data-toggle="tooltip" title="Edit"> <i class="fas fa-edit"></i></a>
                            <a class="btn btn-danger btn-xs delete-user" href="javascript:void(0)" data-id="'. $user->id .'" data-toggle="tooltip" title="Delete"> <i class="fas fa-trash-alt"></i></a>';
                        $nestedData['id'] = $user->id;
                        $nestedData['user'] = $user;
                        $nestedData['name'] = '<a href="'.route('admin.user.detail',['id' => $user->id]).'">'.$user->name.'</a>';
                        $nestedData['email'] = $user->email;
                        $nestedData['user_type'] = !empty($user->enterprise) ? 'Doanh nghiệp' : 'Cá nhân';
                        $nestedData['phone'] = !empty($user->user_meta->phone) ? $user->user_meta->phone : '';
                        $nestedData['group_user'] = !empty($user->group_user->name) ? $user->group_user->name : 'Cơ bản';
                        $nestedData['credit_value'] = $user->credit_value;
                        $nestedData['credit_total'] = $user->credit_total;
                        $nestedData['role'] = isset($user->user_meta->role) ? $user->user_meta->role : '';
                        if ( !empty($user->social_account->provider) ) {
                            $source_reg_acc = '<span class="social-user">' . $user->social_account->provider;
                            $source_reg_acc .= '<button type="button" data-id="'. $user->id .'" data-name="'. $user->name .'" class="btn btn-sm btn-change-social" data-toggle="tooltip" data-placement="top" title="Đổi nguồn tạo tài khoản"><i class="fas fa-info-circle"></i></button>';
                            $source_reg_acc .= '</span>';
                        } else {
                            $source_reg_acc = 'Email';
                        }
                        
                        $nestedData['source_reg_acc'] = $source_reg_acc;
                        $nestedData['created_at'] = !empty($user->created_at) ? date('H:i:s d-m-Y', strtotime($user->created_at)) : '<b class="maudo">Không có ngày tạo</b>' ;
                        $nestedData['action'] = $action;
                        array_push($data, $nestedData);
                    }
                }

                $json_data = [
                    "draw"            => (int)$request->input('draw'),
                    "recordsTotal"    => (int)($totalFiltered),
                    "recordsFiltered" => (int)($totalFiltered),
                    "data"            => $data
                ];
                return response()->json($json_data);
            }
            return view('admin.users.list');
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }

    }

    // Hiển thị danh sách users
    public function get_all_user()
    {
        // $users = User::all();
        // return view('admin.users.list', compact('users'));
        $users = $this->userRepository->get_all_user();
        return $users;
    }

    // Tạo nạp tiền vào tài khoản
    public function addPayment()
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'addPayment') ) {
            $users = $this->userRepository->get_all_user();
            return view('admin.users.addPayment', compact('users'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function addPaymentForm(AdminPayMentValidate $request)
    {
        $create_payment = $this->userRepository->create_payment($request->all());
        if ($create_payment) {
           return redirect(route('admin.user.list'))->with('success', 'Tạo nạp tiền vào tài khoản khách hàng thành công');
       } else {
           return redirect(route('admin.user.list'))->with('success', 'Tạo nạp tiền vào tài khoản khách hàng thất bại');
       }
    }

   public function list_all_user()
   {
        $users = $this->userRepository->all();
        return json_encode($users);
    }

    public function list_user_personal()
    {
        $users = $this->userRepository->list_user_personal();

        return view('admin.users.list', compact('users'));
    }

    public function list_user_enterprise()
    {
        $users = $this->userRepository->list_user_enterprise();

        return view('admin.users.list', compact('users'));
    }
    // Chuyển đến trang form nhập user
    public function create(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'createUser') ) {
            $group_users = $this->userRepository->get_group_users();
            return view('admin.users.create', compact('group_users'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    // Chuyển đến trang form chỉnh sửa user
    public function edit($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'editUser') ) {
            $user = $this->userRepository->detail($id);
            $group_users = $this->userRepository->get_group_users();
            return view('admin.users.edit', compact('user', 'group_users'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    // Cập nhập lại dữ liệu người dùng
    public function update(Request $request)
    {
        $id = $request->get('user_id');
        if (!empty($request->get('new_password'))) {
            $data_user = [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => $request->get('new_password'),
                'group_user_id' => $request->get('group_user_id'),
                'enterprise' => !empty($request->get('enterprise')) ? true : false,
            ];
        } else {
            $data_user = [
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'group_user_id' => $request->get('group_user_id'),
                'enterprise' => !empty($request->get('enterprise')) ? true : false,
            ];
        }
        $data_user_meta = [
            'role' => $request->get('role'),
            'phone' => $request->get('phone'),
            'gender' => $request->get('gender'),
            'address' => $request->get('address'),
            'company' => $request->get('company'),
            'abbreviation_name' => $request->get('abbreviation_name'),
            'mst' => !empty($request->get('mst')) ? $request->get('mst') : null,
        ];
        $updateUser = $this->userRepository->update_user($id, $data_user);

        if ($updateUser) {
            $updateUserMeta = $this->userRepository->update_user_meta($id, $data_user_meta);
            if ($updateUserMeta) {
                return redirect(route('admin.user.list'))->with('success', 'Chỉnh sửa thông tin user thành công');
            } else {
                return redirect(route('admin.user.edit'))->with('error', 'Chỉnh sửa thông tin user thất bại');
            }
        } else {
            return redirect(route('admin.user.edit'))->with('error', 'Chỉnh sửa thông tin user thất bại');
        }
    }


    // Lưu form nhập user
    public function store(UserValidate $request)
    {

        $data_user = [
            'email' => $request->get('email'),
            'name' => $request->get('name'),
            'email_verified_at' => date("Y-m-d H:i:s"),
            'password' => $request->get('password'),
            'group_user_id' => $request->get('group_user_id'),
            'enterprise' => !empty($request->get('enterprise')) ? true : false,
        ];

        $data_user_meta = [
            'role' => 'user',
            'phone' => $request->get('phone'),
            'affiliate' => sha1(time()),
            'gender' => $request->get('gender'),
            'user_create' => Auth::user()->id,
            'address' => $request->get('address'),
            'company' => $request->get('company'),
            'abbreviation_name' => $request->get('abbreviation_name'),
            'mst' => !empty($request->get('mst')) ? $request->get('mst') : null,
        ];
        $user = $this->userRepository->create($data_user);

        if ($user) {
            $data_user_meta['user_id'] = $user->id;
            $user_meta = $this->userRepository->create_user_meta($data_user_meta);
            if ($user_meta) {
                return redirect(route('admin.user.list'))->with('success', 'Tạo user thành công');
            } else {
                return redirect(route('admin.user.create'))->with('error', 'Tạo user thất bại');
            }
        } else {
            return redirect(route('admin.user.create'))->with('error', 'Tạo user thất bại');
        }
    }

    public function quotation_create_user(Request $request)
    {
        try {
            $data_user = [
                'email' => $request->get('email'),
                'name' => $request->get('name'),
                'email_verified_at' => date("Y-m-d H:i:s"),
                'password' => $request->get('password'),
                'group_user_id' => $request->get('group_user_id'),
                'enterprise' => !empty($request->get('enterprise')) ? true : false,
            ];

            $data_user_meta = [
                'role' => 'user',
                'phone' => $request->get('phone'),
                'affiliate' => sha1(time()),
                'gender' => $request->get('gender'),
                'user_create' => Auth::user()->id,
                'address' => $request->get('address'),
                'company' => $request->get('company'),
                'mst' => !empty($request->get('mst')) ? $request->get('mst') : null,
            ];
            $user = $this->userRepository->create($data_user);
            $data_user_meta['user_id'] = $user->id;
            $user_meta = $this->userRepository->create_user_meta($data_user_meta);

            $data = [
                "error" => 0,
                "users" => $this->userRepository->all(),
            ];
            return json_encode($data);
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            $data = [
                "error" => 1,
                "users" => null,
            ];
            return json_encode($data);
        }

    }

    public function delete(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'deleteUser') ) {
            if (!empty($request->get('id'))) {
                $id = $request->get('id');
                $delete = $this->userRepository->delete($id);

                return $delete;
            }
        }
        else {
            return false;
        }

    }

    //Hàm xóa nhiều user được chọn
    public function multidelete(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'multideleteUser') ) {
            $id = $request->get('id');
            $delete = $this->userRepository->multidelete($id);
        }
        else {
            return false;
        }

        return $delete;
    }

    public function detail($id, Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'detailUser') ) {
            $user = $this->userRepository->detail($id);
            $hostings = $this->hostingRepository->listHostingByPageDetailUser($user->id);
            $vpsList = $this->vpsRepository->listVpsByUser($user->id);
            $servers = $this->serverRepository->listServersByUser($user->id);

            $hostingsUsed = $this->hostingRepository->countUsed($user->id);
            $vpsListUsed = $this->vpsRepository->countUsed($user->id);
            $serversUsed = $this->serverRepository->countUsed($user->id);

            $total_vps = $this->vpsRepository->totalVPSByUser($user->id);
            $total_hosting = $this->hostingRepository->totalByUser($user->id);

            $hostingsExpire = $this->hostingRepository->countExpire($user->id);
            $vpsExpire = $this->vpsRepository->countExpire($user->id);

            $hostingDelete = $this->hostingRepository->countDelete($user->id);
            $vpsDelete = $this->vpsRepository->countDelete($user->id);
            // dd($hostingsUsed, $vpsListUsed, $hostingsUsed, $vpsExpire, $total_vps, $total_hosting);
            return view('admin.users.detail', compact('user','vpsList','hostings','servers','hostingsUsed','vpsListUsed','serversUsed', 'total_vps', 'total_hosting', 'hostingsExpire', 'vpsExpire', 'hostingDelete', 'vpsDelete'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function edit_credit($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'updateCreditUser') ) {
            $user = $this->userRepository->detail($id);
            return view( 'admin.users.edit_credit', compact('user') );
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function update_credit_with_admin($id, Request $request)
    {
        $update = $this->userRepository->update_credit_with_admin($id, $request->all());
        if ( $update ) {
            return redirect()->route('admin.user.detail', $id)->with('success', 'Chỉnh sửa số dư tài khoản thành công');
        } else {
            return redirect()->route('admin.user.edit_credit', $id)->with('fails', 'Chỉnh sửa số dư tài khoản thất bại');
        }
    }

    public function list_hosting_by_user(Request $request)
    {
        $hostings = $this->hostingRepository->listHostingByUser($request->get('user_id'));
        return json_encode($hostings);
    }

    public function list_vps_by_user(Request $request)
    {
        $vpsList = $this->vpsRepository->list_vps_by_user($request->get('user_id'), $request->get('status_vps'));
        return json_encode($vpsList);
    }

    public function list_vps_us_by_user(Request $request)
    {
        $vpsList = $this->vpsRepository->list_vps_us_by_user($request->get('user_id'), $request->get('status_vps'));
        return json_encode($vpsList);
    }

    public function sendMail(Request $request)
    {
        try {
            $data = $request->all();
            if (empty($data['email'])) {
                return [
                    'status' => false,
                    'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
                ];
            }

            if (empty($data['subject'])) {
                return [
                    'status' => false,
                    'message' => 'Vui lòng nhập subject.',
                ];
            }

            if (empty($data['content'])) {
                return [
                    'status' => false,
                    'message' => 'Vui lòng nhập content.',
                ];
            }

            Mail::to($data['email'])->send(new SendMailUser($data['email'], $data['subject'], $data['content']));

            return [
                'status' => true,
                'message' => 'Gửi mail thành công!!',
            ];
        } catch (\Exception $exception) {
            return [
                'status' => false,
                'message' => $exception->getMessage(),
            ];
        }

    }

    public function login_user($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'loginAsUser') ) {
            $user = $this->userRepository->detail($id);
            if(!empty($user)) {
                Auth::logout();
                Auth::loginUsingId($id);
                return redirect()->route('index');
            } else {
                return redirect()->route('admin.user.detail', $id)->with('fails', 'Đăng nhập với user thất bại');
            }
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }

    }
    // chỉnh sửa tiền cho user
    public function updateCredit(Request $request)
    {
      $update_credit = $this->userRepository->updateCredit($request->all());
      return json_encode($update_credit);
    }
    // thêm cloudzone point cho user
    public function updateCloudzonePoint(Request $request)
    {
      $update_cp = $this->userRepository->updateCloudzonePoint($request->all());
      return json_encode($update_cp);
    }
    // chỉnh sửa tổng tiền đã nạp cho user
    public function updateCreditTotal(Request $request)
    {
        $update_credit = $this->userRepository->updateCreditTotal($request->all());
        return json_encode($update_credit);
    }
    // Active hoặc InActive User
    public function actionUser(Request $request)
    {
        $update_action = $this->userRepository->actionUser($request->all());
        return json_encode($update_action);
    }

    public function send_mail_with_user($id)
    {
        $user = $this->userRepository->detail($id);
        return view('admin.users.send_mail_with_user', compact('user'));
    }

    public function form_send_mail(Request $request)
    {
        $send_mail = $this->userRepository->send_mail_with_user($request->all());
        if ($send_mail) {
            return redirect()->route('admin.user.detail',  $request->get('user_id'))->with('success', 'Gửi mail cho khách hàng thành công');
        } else {
            return redirect()->route('admin.user.send_mail_with_user', $request->get('user_id'))->with('fails', 'Gửi mail cho khách hàng thất bại');
        }
    }

    public function history_pay($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'historyPayUser') ) {
            $payments = $this->payment->history_pay_with_user($id);
            $pay_in = config('pay_in');
            $type_payin = config('type_payin');
            $momo_status = config('momo_status');
            return view('admin.users.history_pay', compact('payments', 'pay_in', 'momo_status', 'type_payin', 'id'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function history_order($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'historyOrderUser') ) {
            return view('admin.users.history_order', compact('id'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function search_history_pay(Request $request)
    {
        $payments = $this->payment->search_history_pay($request->get('id'), $request->get('q'));
        return json_encode($payments);
    }

    public function quotation()
    {
        return view('admin.quotation.index');
    }

    public function history_quotation($id)
    {
        $list_quotation = $this->userRepository->list_quotation_by_user($id);
        $user = $this->userRepository->detail($id);
        return view('admin.users.history_quotation', compact('list_quotation', 'id', 'user'));
    }

    public function list_quotation(Request $request)
    {
        $q = $request->get('q');
        $data = $this->userRepository->list_quotation($q);
        return json_encode($data);
    }

    public function create_quotation()
    {
        $users = $this->userRepository->all();
        $userId = 0;
        $billings = config('billing');
        $group_users = $this->userRepository->get_group_users();
        return view('admin.quotation.create', compact('users', 'billings', 'group_users', 'userId'));
    }

    public function create_quotation_by_user($userId)
    {
        $users = $this->userRepository->all();
        $billings = config('billing');
        $group_users = $this->userRepository->get_group_users();
        return view('admin.quotation.create', compact('users', 'billings', 'group_users', 'userId'));
    }

    public function edit_quotation($id)
    {
        $users = $this->userRepository->all();
        $quotation = $this->userRepository->quotation_detail($id);
        $billings = config('billing');
        $group_users = $this->userRepository->get_group_users();
       //dd($quotation);
        return view('admin.quotation.edit', compact('users', 'billings', 'quotation', 'group_users'));
    }

    public function get_detail_quotation(Request $request)
    {
            
        $quotation = $this->userRepository->quotation_detail($request->get('id'));
        $billings = config('billing');
        $quotation_detail_0 = $quotation->quotation_details[0];
        $item = [
            'detail' => $quotation_detail_0,
            'group_products' => $this->product->get_group_product_private_with_type($quotation_detail_0->user_id, $quotation_detail_0->type_product),
            'product' => $this->userRepository->get_product_with_quotation($quotation_detail_0),
            'datacenter' => $this->product->detail_product($quotation_detail_0->product->id)->product_datacenters,
        ];
    
        $item_add = [];
        foreach ($quotation->quotation_details as $key => $quotation_detail) {
            if ( $key != 0 ) {
                $item_add[] = [
                    'detail' => $quotation_detail,
                    'group_products' => $this->product->get_group_product_private_with_type($quotation_detail->user_id, $quotation_detail->type_product),
                    'product' => $this->userRepository->get_product_with_quotation($quotation_detail),
                   'datacenter' => $this->product->detail_product($quotation_detail->product->id)->product_datacenters,
                  
                ];
            }
            
        }
        $data = [
            'billings' => $billings,
            'item' => $item,
            'item_add' => $item_add

        ];
   
        return json_encode($data);
    }

    public function store_quotation(Request $request)
    {
      // dd($request->all());       
         $quotation = $this->userRepository->store_quotation($request->all());
        if ($quotation) {
          return redirect()->route('admin.user.quotation')->with('success', "Tạo báo giá thành công");
      } else {
          return redirect()->route('admin.user.quotation')->with('fails', "Tạo báo giá thành công");
      }

    }

    public function update_quotation(Request $request)
    {
            //dd($request->all());
     return   $quotation = $this->userRepository->update_quotation($request->all());
        if ($quotation) {
          return redirect()->route('admin.user.quotation')->with('success', "Sửa báo giá thành công");
      } else {
          return redirect()->route('admin.user.quotation')->with('fails', "Sửa báo giá thành công");
      }

    }

    public function quotation_get_group_product(Request $request)
    {
        $user_id = $request->get('id');
        $type = $request->get('type_product');
        $data = $this->product->get_group_product_private_with_type($user_id, $type);  
        return json_encode($data);
    }

    public function get_product(Request $request)
    {   
         $request->all();
        // $product_id = $request->get('id');
        // $billing_cycle = $request->get('billing_cycle');
       
        $product = $this->product->get_product_with_quotation($request->all());
        $product['datacenter'] = $this->product->detail_product($product->id)->product_datacenters;
        //return $product;
        return json_encode($product);
    }

    public function delete_quotation(Request $request)
    {
        $quotation = $this->userRepository->delete_quotation($request->get('id'));
        return json_encode($quotation);
    }

    public function preview_quotation($id)
    {   
      
        $quotation = $this->userRepository->quotation_detail($id);
        $user = $this->userRepository->detail($quotation->user_id);
        $billings = [
            'monthly' => '1 tháng',
            'twomonthly' => '2 tháng',
            'quarterly' => '3 tháng',
            'semi_annually' => '6 tháng',
            'annually' => '1 năm',
            'biennially' => '2 năm',
            'triennially' => '3 năm',
            'one_time_pay' => 'Vĩnh viễn',
            'free' => 'Miễn phí',
        ];
        $billingDashBoard = config('billingDashBoard');
        return view('admin.quotation.preview', compact( 'billings', 'user', 'quotation', 'billingDashBoard'));
    }

    public function create_pdf_quotation($id)
    {
        $quotation = $this->userRepository->quotation_detail($id);
        $user = $this->userRepository->detail($quotation->user_id);
        $billings = [
            'monthly' => '01 tháng',
            'twomonthly' => '02 tháng',
            'quarterly' => '03 tháng',
            'semi_annually' => '06 tháng',
            'annually' => '1 năm',
            'biennially' => '2 năm',
            'triennially' => '3 năm',
            'one_time_pay' => 'Vĩnh viễn',
            'free' => 'Miễn phí',
        ];
        $billingDashBoard = config('billingDashBoard');
        $billings = [
            'monthly' => '01 tháng',
            'twomonthly' => '02 tháng',
            'quarterly' => '03 tháng',
            'semi_annually' => '06 tháng',
            'annually' => '1 năm',
            'biennially' => '2 năm',
            'triennially' => '3 năm',
            'one_time_pay' => 'Vĩnh viễn',
            'free' => 'Miễn phí',
        ];
        // return view('admin.quotation.test', compact( 'billings', 'user', 'quotation'));
        $unicode = array(
          'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
          'd'=>'đ|Đ',
          'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
          'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
          'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
          'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
          'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        $str_name = strtolower($quotation->title);
        foreach($unicode as $nonUnicode => $uni){
            $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
        }
        $str_name = str_replace( ' ', '_', $str_name );
        $path = '/storage/pdf/';
        //Load view
        $pdf = DomPDF::loadView('admin.quotation.create_pdf', compact( 'billings', 'user', 'quotation', 'billingDashBoard' ));
        $file_name = $str_name . '.pdf';
        try {
            if ( !empty($quotation->link) ) {
                if ( Storage::exists( 'public/pdf/' . $file_name ) ) {
                    Storage::delete( 'public/pdf/' . $file_name );
                    $quotation->link = '';
                    $quotation->save();
                }
            }
            // lưu vào storage
            Storage::put('public/pdf/' . $file_name, $pdf->output());
            // lưu link pdf vừa tạo vào database
            $quotation->link = '/storage/pdf/' . $file_name;
            $quotation->save();
            // test
            // $pdf = DomPDF::loadView('admin.quotation.test');
            // $pdf->download($file_name);
            // return $pdf->download('test.pdf');
            return redirect()->route('admin.user.quotation')->with('success', "Tạo tập tin báo giá thành công");
        } catch (\Throwable $th) {
            //throw $th;
            report($th);
            return redirect()->route('admin.user.quotation')->with('false', "Tạo tập tin báo giá thất bại");
        }


    }

    public function searchUser(Request $request)
    {
        $users = $this->userRepository->searchUser($request->get('q'));
        return $users;
    }

    public function change_social(Request $request, $id)
    {
        $change_social = $this->userRepository->change_social($id);
        return $change_social;
    }

    public function test($value='')
    {
              //
              // Storage::disk('public')->put($path, $pdf);
              // $quotation->link = $path . $str_name . '.pdf';
              // $quotation->save();
              // return $pdf->download($str_name . '.pdf');
    }

}
