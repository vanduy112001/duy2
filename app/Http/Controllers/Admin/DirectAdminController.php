<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DirectAdmin;
use App\Services\DashBoard;
use App\Factories\AdminFactories;
use App\Factories\UserFactories;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Product;
use App\Events\VpsEvent;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class DirectAdminController extends Controller
{
    protected $da;
    protected $da_si;
    protected $home;
    protected $user;
    protected $dashboard;
    protected $vps;
    protected $product;

    public function __construct()
    {
        $this->da = AdminFactories::directAdminRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
        $this->home = UserFactories::homeRepositories();
        $this->user = AdminFactories::userRepositories();
        $this->dashboard = new DashBoard();
        $this->vps = AdminFactories::vpsRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->server_hosting = AdminFactories::serverHostingRepositories();
    }

    public function index(Request $request)
    {
        // $token_zalo2 = $this->da_si->login_zalo();
        // dd('da den', $token_zalo2);
        // $vps = Vps::where('paid' , 'paid')->where('status', 'Pending')->first();
        // $config_billing_DashBoard = config('billingDashBoard');
        // $product = Product::find($vps->product_id);
        // $add_on = [];
        // $cpu = $product->meta_product->cpu;
        // $ram = $product->meta_product->ram;
        // $disk = $product->meta_product->disk;
        // if (!empty($vps->addon_id)) {
        //     $add_on = Product::find($vps->addon_id);
        //     if(!empty($add_on->meta_product->cpu)) {
        //         $add_on_cpu = $add_on->meta_product->cpu * $vps->addon_qtt;
        //     } else {
        //         $add_on_cpu = 0;
        //     }
        //     if (!empty($add_on->meta_product->memory)) {
        //         $add_on_ram = $add_on->meta_product->memory * $vps->addon_qtt;
        //     } else {
        //         $add_on_ram = 0;
        //     }
        //     if ($add_on->meta_product->disk) {
        //         $add_on_disk = $add_on->meta_product->disk * $vps->addon_qtt;
        //     } else {
        //         $add_on_disk = 0;
        //     }
        //     $cpu = $product->meta_product->cpu + $add_on_cpu;
        //     $ram = $product->meta_product->memory + $add_on_ram;
        //     $disk = $product->meta_product->disk + $add_on_disk;
        // }
        //
        // $created_at = date('Y-m-d', strtotime($vps->created_at));
        // $leased_time = $config_billing_DashBoard[$vps->billing_cycle];
        // $data = [
        //     'customer' => 91, // customer id dashboard
        //     'created_date' => date('Y-m-d', strtotime($vps->created_at)), // ngay tao
        //     'leased_time' => $config_billing_DashBoard[$vps->billing_cycle], // Thoi gian thue theo thang
        //     'end_date' => $vps->next_due_date, // Ngay het han
        //     'mod' => $vps->detail_order->sub_total, //Tong tien thanh toan
        //     'template' => $vps->os, //HDh : 1 - win 7, 2 - win 2012, 3- win 2016, 4 - CentOs7
        //     'cpu' => $cpu,
        //     'ram' => $ram,
        //     'disk' => $disk > 20 ? $disk : 20,
        // ];
        // // dd($vps, $data);
        // $result = $this->dashboard->createVPS($data);
        //  direct admin
        // $result = $this->dashboard->getUser();
        // gui len pusher
        // $user_id = 6;
        // $vps_ip = '192.168.1.1';
        // $event = event(new VpsEvent($user_id, $vps_ip));
        // dd($event, '<br> da den roi');
        // Test mail khi dashboar gui status_vps
        // $now = Carbon::now();
        // $next_due_date = new Carbon('2020-07-10');
        // if ($next_due_date->diffInMonths($now) == 0) {
        //   dd($next_due_date->diffInDays($now));
        //   dd( (int) round($next_due_date->diffInDays($now) * 80000 / 30, -3) );
        // } elseif ($next_due_date->diffInMonths($now) > 0) {
        //   // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
        //   $month = $next_due_date->diffInMonths($now);
        //   $now = $now->addMonths($next_due_date->diffInMonths($now));
        //   dd( (int) $month * 800000 , $next_due_date->diffInDays($now) );
        // }
        // $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
        // $now = new Carbon($now);
        // $next_due_date = new Carbon('2020-04-10');
        // dd($now, $next_due_date, $next_due_date->isFuture() ,$next_due_date->diffInDays($now));
        // $repo = UserFactories::cronTab();
        // $repo->expiredVPS();
        //check connect da
        // $data = [
        //   'type' => 'DirectAdmin',
        //   'host' => 'da.cloudzone.vn',
        //   'user_name' => 'whmcsresel',
        //   'post' => '2222',
        //   'password' => 'c6574aca319c5a714dcea44',
        // ];
        // $this->server_hosting->check_connection($data);
        // check connect da singapore
        // dd($this->da_si->showPackage());
        // $next_due_date = new Carbon('2020-04-10');
        // if ($next_due_date->isPast()) {
        //   dd('da den 1');
        // } else {
        //   dd('da den 2');
        // }
        // dd(Storage::url('05-05-2020-scan-after-hien-le-tan-icon-vietcombank.png'));
        $repo = UserFactories::cronTab();
        $fillter = $repo->deleteColocation();
        // $repo = UserFactories::moMoRepository();
        // $repo->getTrans();
        dd('da den');
        // $repo = AdminFactories::vpsRepositories();
        // $repo->get_product_vps_with_user(123113211);
        // session trong laravel
        // dd($request->session());
    }

    public function zalo_callback(Request $request)
    {
        $this->da_si->zalo_callback($request->all());
    }

    public function showPackage(Request $request)
    {
        if ($request->get('location') == 'vn') {
          $result = $this->da->showPackage();
          return json_encode($result);
        }
        elseif ($request->get('location') == 'si') {
          $result = $this->da_si->showPackage();
          return json_encode($result);
        }
    }
    // Thông tin gữi về
    // array:3 [▼
    //   "error" => "0"
    //   "text" => "Success"
    //   "details" => "All selected Users have been suspended"
    // ]
    public function getHostingDaPortal(Request $request)
    {
        $id = $request->get('id');
        $result = $this->da->getHostingDaPortal($id);
        return json_encode($result);
    }

    public function addHostingWithPortal($username, $id)
    {
        $users = $this->user->all();
        $products = $this->product->get_product_with_action_add_hosting('Hosting');
        $hosting = $this->da->getUserHostingDaPortal($username, $id);
        $billings = config('billing');
        if ($hosting) {
            return view('admin.hostings.addHostingWithPortal', compact('hosting', 'users', 'products', 'billings', 'id'));
        } else {
            return redirect()->route('admin.hostings.getHostingDaPortal', $id)->with('fails', 'Truy cập đến user của hosting Việt Nam không thành công.');
        }
    }

    public function getHostingDaSingapore(Request $request)
    {
        $id = $request->get('id');
        $result = $this->da_si->getHostingDaPortal($id);
        return json_encode($result);
    }

    public function addHostingSingaporeWithPortal($username, $id)
    {
        $users = $this->user->all();
        $products = $this->product->get_product_with_action('Hosting');
        $billings = config('billing');
        $hosting = $this->da_si->getUserHostingDaPortal($username);
        if ($hosting) {
            return view('admin.hostings.addHostingSingaporeWithPortal', compact('hosting', 'users', 'products', 'billings', 'id'));
        } else {
            return redirect()->route('admin.hostings.getHostingSingapore', $id)->with('fails', 'Truy cập đến user của hosting singapore không thành công.');
        }
    }


}
