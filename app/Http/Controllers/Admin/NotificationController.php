<?php

namespace App\Http\Controllers\Admin;

use App\Model\Notification;
use App\Model\Status;
use App\Factories\AdminFactories;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    protected $notificationRepository;

    //Hàm khởi tạo
    public function __construct()
    {
        $this->notificationRepository = AdminFactories::notificationRepositories();
    }

    public function index(Request $request)
    {
        // $notifications = Notification::orderBy('id', 'DESC')->get();
        $data = $request->all();
        $notifications = $this->notificationRepository->all($data);  
        $search = $request->get('keyword');   
        return view('admin.notifications.admin.index', compact('notifications', 'search'));
    }

    // Hàm tạo thông báo
    public function create()
    {
        $statuses = config('status_notification');
        return view('admin.notifications.admin.create', compact('statuses'));
    }

    //Hàm lưu thông báo
    public function store(Request $request)
    {
        $data_notification = [
            'name' =>  $request->get('name'), 
            'content' => $request->get('content'), 
            'status' => $request->get('status')
        ];
        $notification = $this->notificationRepository->create($data_notification);
        if( $notification ) {
            return redirect(route('admin.notification.index'))->with('success', 'Tạo thông báo thành công');
        } else {
            return redirect(route('admin.notification.index'))->with('error', 'Tạo thông báo thất bại');
        }
    }


    //Hàm sửa thông báo
    public function edit($id)
    {
        $statuses = config('status_notification');
        $notification = $this->notificationRepository->detail($id);
        return view('admin.notifications.admin.edit', compact('notification', 'statuses'));
    }

    //Hàm cập nhập thông báo (edit)
    public function update(Request $request)
    {
        $id = $request->get('notification_id');
        $data_notification = [
            'name' =>  $request->get('name'), 
            'content' => $request->get('content'), 
            'status' => $request->get('status')
        ];
        $notification = $this->notificationRepository->update($id, $data_notification);

        if($notification) {
            return redirect(route('admin.notification.index'))->with('success', 'Cập nhập thông báo thành công');
        } else {
            return redirect(route('admin.notification.index'))->with('error', 'Cập nhập thông báo thất bại');
        }        
    }

    //Hàm xóa thông báo
    public function delete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->notificationRepository->delete($id);
        return $delete;
    }

    //Hàm xóa nhiều thông báo được chọn
    public function multidelete(Request $request) 
    {
        $id = $request->get('id');
        $delete = $this->notificationRepository->multidelete($id);
        return $delete;
    }

    public function search(Request $request)
    {
        $value = $request->get('table_search');
        $seachs = $this->notificationRepository->seach($value);
        return view('admin.notifications.admin.search', compact('seachs'));
    }
}
