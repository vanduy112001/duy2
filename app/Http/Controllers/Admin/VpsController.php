<?php

namespace App\Http\Controllers\Admin;

use App\Model\Vps;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\AddVpsValidate;

class VpsController extends Controller
{
    protected $vps;
    protected $product;
    protected $order;

    public function __construct()
    {
        $this->vps = AdminFactories::vpsRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->order = AdminFactories::orderRepositories();
    }

    public function index()
    {
        $list_vps = $this->vps->get_vps();
        $billings = config('billing');
        // dd($list_vps);
        return view('admin.vps.index', compact('list_vps', 'billings'));
    }
    public function vps_on()
    {
         
        $list_vps = $this->service->list_vps_on();
   //     return $list_vps;
        $billing = config('billing');
        return view('users.services.list_vps_on', compact('list_vps' , 'billing'));
    }
    public function vps_use()
    {
        $list_vps = $this->service->list_vps_use();
        $billing = config('billing');
        return view('users.services.list_vps_use', compact('list_vps' , 'billing'));
    }
    public function vps_nearly()
    {
        $list_vps = $this->service->list_vps_nearly();
        $billing = config('billing');
        return view('users.services.list_vps_nearly', compact('list_vps' , 'billing'));
    }
    public function vps_cancel()
    {
        $list_vps = $this->service->list_vps_cancel();
        $billing = config('billing');
        return view('users.services.list_vps_cancel', compact('list_vps' , 'billing'));
    }
    public function vps_all()
    {
       $list_vps = $this->service->vps_all();
       $billing = config('billing');
       return view('users.services.list_vps_all', compact('list_vps' , 'billing'));
    }
    public function vps_nearly_and_expire()
    {
        return view('users.services.list_vps_nearly_and_expire');
    }
    public function list_vps_nearly_and_expire()
    {
        $data = $this->service->list_vps_nearly_and_expire();
        return json_encode($data);
    }
    public function vps_us_on()
    {
        $list_vps = $this->service->list_vps_us_on();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_on', compact('list_vps' , 'billing'));
    }
    public function vps_us_use()
    {
        $list_vps = $this->service->list_vps_us_use();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_use', compact('list_vps' , 'billing'));
    }
    public function vps_us_nearly()
    {
        $list_vps = $this->service->list_vps_us_nearly();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_nearly', compact('list_vps' , 'billing'));
    }
    public function vps_us_cancel()
    {
        $list_vps = $this->service->list_vps_us_cancel();
        $billing = config('billing');
        return view('users.services.vps_us.list_vps_us_cancel', compact('list_vps' , 'billing'));
    }
    public function vps_us_all()
    {
       $list_vps = $this->service->vps_us_all();
       $billing = config('billing');
       return view('users.services.vps_us.list_vps_us_all', compact('list_vps' , 'billing'));
    }
    public function vps_us_nearly_and_expire()
    {
        return view('users.services.vps_us.list_vps_us_nearly_and_expire');
    }
    public function list_vps_us_nearly_and_expire()
    {
        $data = $this->service->list_vps_us_nearly_and_expire();
        return json_encode($data);
    }
    public function detail($id)
    {
        $detail = $this->vps->detail_vps($id);
        $log_payments = $this->vps->log_payment_with_vps($id);
        $log_activities = $this->vps->log_activities_with_vps($id);
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_with_action($detail->detail_order->type, $detail->user_id);
        $status_vps = config('status_vps');
        return view('admin.vps.edit', compact('detail', 'products', 'billings', 'users', 'status_vps', 'log_payments', 'log_activities'));
    }

    public function createVPS()
    {
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_vps();
        $list_os = config('os');
        return view('admin.vps.create', compact('products', 'billings', 'users', 'list_os'));
    }

    public function create_sevices(AddVpsValidate $request)
    {
        $add = $this->vps->addVPS($request->all());
        if ($add) {
            return redirect()->route('admin.vps.index')->with('success', 'Thêm VPS vào Portal thành công');
        } else {
            return redirect()->route('admin.vps.createVPS')->with('fails', 'Thêm VPS vào Portal thất bại');
        }
    }

    public function delete_ajax_vps(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        switch ($action) {
            case 'delete':
                $delete_vps = $this->vps->delete_vps($id);
                if($delete_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'on':
                $off_vps = $this->vps->on_vps($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'restart':
                $off_vps = $this->vps->restart_vps($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'off':
                $off_vps = $this->vps->off_vps($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            default:
                return false;
                break;
        }
    }

    public function action(Request $request)
    {
        $vps_id = $request->get('vps_id');
        $action = $request->get('action');

        if ($action == 'on') {
            $result = $this->vps->on_action($vps_id);
        }
        elseif ($action == 'off') {
            $result = $this->vps->off_action($vps_id);
        }
        elseif ($action == 'restart') {
            $result = $this->vps->restart_action($vps_id);
        }
        elseif ($action == 'change_ip') {
            $result = $this->vps->change_ip_action($vps_id);
        }
        elseif ($action == 'delete') {
            $result = $this->vps->delete_action($vps_id);
        } else {
            $result = $this->vps->cancel_action($vps_id);
        }
        return json_encode($result);
    }

    // action trong file edit vps
    public function edit_action(Request $request)
    {
        $action = $request->get('action');
        $vps_id = $request->get('id');
        if ($action == 'create') {
            $result = $this->vps->detail_create_action($vps_id);
        }
        elseif ($action == 'on') {
            $result = $this->vps->detail_on_action($vps_id);
        }
        elseif ($action == 'paid') {
            $result = $this->vps->detail_paid_action($vps_id);
        }
        elseif ($action == 'unpaid') {
            $result = $this->vps->detail_unpaid_action($vps_id);
        }
        elseif ($action == 'off') {
            $result = $this->vps->detail_off_action($vps_id);
        }
        elseif ($action == 'delete') {
            $result = $this->vps->detail_delete_action($vps_id);
        } else {
            $result = $this->vps->detail_cancel_action($vps_id);
        }
        return $result;
    }
    
    // sevices update
    public function update_sevices(Request $request)
    {
        $validate = $request->validate([
            'user_id' => 'bail|required',
            'product_id' => 'bail|required|not_in:0',
            'ip' => 'bail|required',
            'sevices_username' => 'bail|required',
            'sevices_password' => 'bail|required',
        ],
        [
            'user_id.required' => 'Tên khách hàng không được để trống',
            'product_id.required' => 'Sản phẩm không được để trống',
            'product_id.required' => 'Sản phẩm không được để trống',
            'ip.required' => 'IP không được để trống',
            'sevices_username.required' => 'User không được để trống',
            'sevices_password.required' => 'Mật khẩu không được để trống',
        ] );
        $data = $request->all();
        // dd($data);
        if ($data['type'] == 'vps') {
            $update_sevice = $this->vps->update_sevice_vps($data);
            if ($update_sevice) {
                return redirect()->route('admin.vps.index')->with('success', 'Chỉnh sửa VPS thành công');
            } else {
                return redirect()->route('admin.vps.detail', $data['id'])->with('fails', 'Chỉnh sửa VPS thất bại');
            }
        }

    }
    // loadBillings
    public function loadBillings()
    {
        $billings = config('billing');
        return json_encode($billings);
    }
    // loadIPVPS
    public function loadIPVPS(Request $request)
    {
        $order = $this->order->detail($request->get('id'));
        $data = [
            'ip' => $this->vps->loadIPVcenter($request->get('dropship'), $request->get('data_center')),
            'order' => $order,
            'detail_order' => $order->detail_orders[0],
        ];
        return json_encode($data);
    }

    public function confirm_order(Request $request)
    {
        $data = $request->all();
        $confirm_vps = $this->order-confirm_order($data);
        if ($confirm_vps) {
            return true;
        } else {
            return false;
        }
    }

    public function search(Request $request)
    {
        $q = $request->get('q');
        $status = $request->get('status');
        $sort = $request->get('sort');
        $qtt = $request->get('qtt');
        $data = $this->vps->search($q, $status, $sort, $qtt);
        return json_encode($data);
    }

    public function search_multi_vps_vn(Request $request)
    {
        $q = explode(',', $request->get('q'));
        $data = $this->vps->search_multi_vps_vn($q);
        return json_encode($data);
    }

    public function select_status_vps(Request $request)
    {
        $q = $request->get('q');
        $status = $request->get('status');
        $sort = $request->get('sort');
        $qtt = $request->get('qtt');
        $data = $this->vps->select_status_vps($q, $status, $sort, $qtt);
        return json_encode($data);
    }

    public function search_user(Request $request)
    {
        $q = $request->get('q');
        $data = $this->vps->search_user($q);
        return json_encode($data);
    }

    public function search_user_form(Request $request)
    {
        $q = $request->get('search_user');
        $list_vps = $this->vps->search_user_form($q);
        return view('admin.vps.search_user', compact('list_vps', 'q'));
    }

    public function list_vps_of_personal()
    {
        $data = $this->vps->list_vps_of_personal();
        return json_encode($data);
    }

    public function list_vps_of_enterprise()
    {
        $data = $this->vps->list_vps_of_enterprise();
        return json_encode($data);
    }

    public function console($id)
    {
        $data_console = $this->vps->vps_console($id);
        // $data_console['error'] = 0;
        // $data_console['vm_host'] = 1;
        // $data_console['vm_ticket'] = 2;
        if ($data_console) {
          return view('admin.vps.console', compact('data_console'));
        } else {
          return redirect()->route('admin.vps.index')->with('fails', 'Lỗi không thể kết nối đến Dashboard');
        }
    }

    public function change_user(Request $request)
    {
        $list_id_vps = explode(',', $request->get('id'));
        if ( is_array($list_id_vps) ) {
            $list_vps = [];
            $check_vps = $this->vps->check_mutil_vps_with_change_user($list_id_vps);
            if ( $check_vps['error'] == 0 ) {
                $list_vps = $check_vps['content'];
                $list_user = $this->vps->list_all_user();
                return view('admin.vps.change_user', compact('list_vps', 'list_user'));
            }
            else {
                if ( $check_vps['error'] == 1 ) {
                    return redirect()->route('admin.vps.index')->with('fails', $check_vps['text_error']);
                }
                elseif ( $check_vps['error'] == 2 ) {
                    return redirect()->route('admin.vps.index')->with('fails', $check_vps['text_error']);
                }
                else {
                    return redirect()->route('admin.vps.index')->with('fails', $check_vps['text_error']);
                }
            }
        } else {
            $check_vps = $this->vps->check_vps_with_change_user($list_id_vps);
            if ( $check_vps['error'] == 0 ) {
                $list_vps = $check_vps['content'];
                $list_user = $this->vps->list_all_user();
                return view('admin.vps.change_user', compact('list_vps', 'list_user'));
            } else {
                if ( $check_vps['error'] == 1 ) {
                    return redirect()->route('admin.vps.index')->with('fails', 'Không có VPS được chọn trong dữ liệu.');
                }
                elseif ( $check_vps['error'] == 2 ) {
                    return redirect()->route('admin.vps.index')->with('fails', 'VPS chưa được tạo.');
                }
                else {
                    return redirect()->route('admin.vps.index')->with('fails', 'VPS được chọn không được ở trạng thái "đã xóa" - "đã chuyển" - "đã hủy". Vui lòng kiểm tra lại.');
                }
            }
        }
    }

    public function form_change_user(Request $request)
    {
        // dd($request->all());
        $list_id_vps = $request->get('id');
        $check_vps = $this->vps->check_mutil_vps_with_change_user($list_id_vps);
        if ( $check_vps['error'] == 0 ) {
            $change = $this->vps->change_user_by_vps($request->all());
            if ( $change['error'] == 0 ) {
                return redirect()->route('admin.vps.index')->with('success', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thành công.');
            } 
            elseif ( $change['error'] == 1 ) {
                return redirect()->back()->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
            }
            else {
                return redirect()->back()->with('fails', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thất bại.');
            }
        }
        else {
            if ( $check_vps['error'] == 1 ) {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
            elseif ( $check_vps['error'] == 2 ) {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
            else {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
        }
    }

}
