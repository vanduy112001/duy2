<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $user;
    protected $product;
    protected $email;

    public function __construct()
    {
        $this->user = AdminFactories::apiUserRepository();
        $this->product = AdminFactories::apiProductRepository();
        $this->email = AdminFactories::emailRepositories();
    }

    public function listGroupProductByGroupUser(Request $request, $groupUserId) {
        try {
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách nhóm sản phẩm thành công',
                'content' => $this->product->listGroupProductByGroupUser($groupUserId),
                'groupUser' => $this->user->detailGroupUser($groupUserId),
            ]);
        } catch (Exception $e) {
            report($e);
            return response()->json([
                'error' => 1,
                'message' => 'Lấy danh sách nhóm sản phẩm thất bại',
            ]);
        }
    }

    public function createGroupProduct(Request $request)
    {
        try {
            $response = $this->product->createGroupProduct($request->all());
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }

    public function updateGroupProduct(Request $request)
    {
        try {
            $response = $this->product->updateGroupProduct($request->all());
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }

    public function detailGroupProduct(Request $request, $groupProductId)
    {
        try {
            $response = $this->product->detailGroupProduct($groupProductId);
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $groupProductId);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }
/* Product */
    public function createProduct(Request $request)
    {
        try {
            $response = $this->product->createProduct( $request->all() );
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }

    public function getConfigProduct(Request $request)
    {
        try {
            $list_config_os = config('os');
            $list_os = [];
            foreach ($list_config_os as $key => $os) {
              $list_os[] = [
                'label' => $os,
                'value' => $key,
                'isChecked' => false
              ];
            }
            return response()->json([
                'error' => 0,
                'message' => '',
                'type_products' => config('type_product'),
                'emails' => $this->email->getTemplateEmail(),
                'config_os' => $list_os
            ]);
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }

    public function detailProduct(Request $request, $productId)
    {
        try {
            $response = $this->product->detailProduct($productId);
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $groupProductId);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn sản phẩm'
            ]);
        }
    }

}
