<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class InvoiceController extends Controller
{
    protected $invoice;
    public function __construct()
    {
        $this->invoice = AdminFactories::invoiceRepositories();
    }

    public function index()
    {
        $detail_orders = $this->invoice->get_detail_orders();
        $invoices = config('invoices');
        return view('admin.invoices.index', compact('detail_orders', 'invoices') );
    }

    public function edit($id)
    {
        $detail_order = $this->invoice->get_detail_order($id);
        $invoices = config('invoices');
        $billings = config('billing');
        return view('admin.invoices.edit', compact('detail_order', 'invoices', 'billings') );
    }

    public function filter_type(Request $request)
    {
        $data = $this->invoice->filter_type($request->get('type'), $request->get('user_id'), $request->get('status'));
        return json_encode($data);
    }

    public function filter_user(Request $request)
    {
        $data = $this->invoice->filter_user($request->get('type'), $request->get('user_id'), $request->get('status'));
        return json_encode($data);
    }

    public function filter_status(Request $request)
    {
        $data = $this->invoice->filter_status($request->get('type'), $request->get('user_id'), $request->get('status'));
        return json_encode($data);
    }

    public function detail_invoice(Request $request)
    {
      $data = $this->invoice->detail_invoice($request->get('id'));
      return json_encode($data);
    }

    public function delete_item(Request $request)
    {
        $id = $request->get('id');
        $id_parent = $request->get('parent');
        $type = $request->get('type');
        if ($type == 'VPS') {
            $delete_item = $this->invoice->delete_vps($id);
            $success = 'Xóa VPS thành công';
            $fails = 'Xóa VPS thất bại';
        } elseif ($type == 'Server') {
            $delete_item = $this->invoice->delete_server($id);
            $success = 'Xóa Server thành công';
            $fails = 'Xóa Server thất bại';
        } else {
            $delete_item = $this->invoice->delete_hosting($id);
            $success = 'Xóa Hosting thành công';
            $fails = 'Xóa Hosting thất bại';
        }
        if ($delete_item) {
            return redirect()->route('admin.invoices.edit', $id_parent)->with('success', $success);
        } else {
            return redirect()->route('admin.invoices.edit', $id_parent)->with('fails', $fails);
        }
    }

    public function paid($id, Request $request)
    {
        $action = $request->get('action');
        if ($action == 'mark_paid') {
            $paid = $this->invoice->mark_paid($id);
            $success = 'Gán thanh toán cho hóa đơn thành công';
            $fails = 'Gán thanh toán cho hóa đơn thất bại';
        } elseif($action == 'unpaid') {
            $paid = $this->invoice->unpaid($id);
            $success = 'Gán chưa thanh toán cho hóa đơn thành công';
            $fails = 'Gán chưa thanh toán cho hóa đơn thất bại';
        } else {
            $paid = $this->invoice->cancel($id);
            $success = 'Gán hủy cho hóa đơn thành công';
            $fails = 'Gán hủy thanh toán cho hóa đơn thất bại';
        }

        if ($paid) {
            return redirect()->route('admin.invoices.edit', $id)->with('success', $success);
        } else {
            return redirect()->route('admin.invoices.edit', $id)->with('fails', $fails);
        }
    }

    public function invoices_paid(Request $request)
    {
        $data = $request->get('value');
        $type = $request->get('type');
        $data = explode(',', $data);
        if ($type == 'mark_paid') {
            $paid = $this->invoice->invoices_mark_paid($data);
            $success = 'Gán thanh toán cho tất cả hóa đơn thành công';
            $fails = 'Gán thanh toán cho tất cả hóa đơn thất bại';
        } elseif ($type == 'unpaid') {
            $paid = $this->invoice->invoices_unpaid($data);
            $success = 'Gán chưa thanh toán cho tất cả hóa đơn thành công';
            $fails = 'Gán chưa thanh toán cho tất cả hóa đơn thất bại';
        } elseif ($type == 'cancel') {
            $paid = $this->invoice->invoices_cancel($data);
            $success = 'Gán hủy cho tất cả hóa đơn thành công';
            $fails = 'Gán hủy cho tất cả hóa đơn thất bại';
        } else {
            $paid = $this->invoice->invoices_delete($data);
            $success = 'Xóa tất cả các hóa đơn được chọn thành công';
            $fails = 'Xóa tất cả các hóa đơn được chọn thất bại';
        }

        if ($paid) {
            return redirect()->route('admin.invoices.index')->with('success', $success);
        } else {
            return redirect()->route('admin.invoices.index')->with('fails', $fails);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->invoice->delete($id);
        if ($delete) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

}
