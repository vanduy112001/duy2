<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class ColocationController extends Controller
{
    protected $colo;
    protected $order;
    protected $product;

    public function __construct()
    {
        $this->colo = AdminFactories::colocationRepositories();
        $this->order = AdminFactories::orderRepositories();
        $this->product = AdminFactories::productRepositories();
    }

    public function index()
    {
        $list_colos = $this->colo->list_colocations();
        $billings = config('billing');
        return view('admin.colocations.index', compact('list_colos', 'billings'));
    }

    public function detail($id)
    {
        $detail = $this->colo->detail($id);
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_with_action($detail->detail_order->type, $detail->user_id);
        $list_product_addon = $this->product->get_product_addon_by_type('addon_colo', 'addon_ip',$detail->user_id);
        // dd($list_product_addon);
        return view('admin.colocations.detail', compact('detail', 'billings', 'users', 'products', 'list_product_addon'));
    }

    public function getProductAddonIpColocation(Request $request)
    {
        try {
            $list_product_addon = $this->product->get_product_addon_by_type('addon_colo', 'addon_ip',$request->get('user_id'));
            return [
                'error' => 0,
                'addons' => $list_product_addon
            ];
        } catch (\Throwable $th) {
            report($th);
            return [
                'error' => 1,
                'addons' => []
            ];
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->colo->delete($request->get('id'));
        return json_encode($delete);
    }

    public function getColocation(Request $request)
    {
        $listColocation = $this->colo->listColocation($request->all());
        return json_encode($listColocation);
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $update = $this->colo->update($request->all());
        if ( $update ) {
            return redirect()->route('admin.colo.index')->with('success', 'Chỉnh sửa Colocation '. $request->get('ip') .' thành công');
        } else {
            return redirect()->route('admin.colocation.detail', $request->get('ip'))->with('fails', 'Chỉnh sửa Colocation '. $request->get('ip') .' thất bại');
        }
    }

    public function create(Request $request)
    {
        $billings = config('billing');
        $users = $this->order->users();
        return view('admin.colocations.create', compact('billings', 'users'));
    }

    public function store(Request $request)
    {
        $store = $this->colo->store($request->all());
        if ( $store ) {
            return redirect()->route('admin.colo.index')->with('success', 'Tạo Colocation thành công');
        } else {
            return redirect()->back()->with('fails', 'Tạo Colocation thất bại');
        }
    }

    public function list_product(Request $request)
    {
      $list_products = $this->colo->listProductByUser($request->get('user_id'));
      return json_encode($list_products);
    }

}
