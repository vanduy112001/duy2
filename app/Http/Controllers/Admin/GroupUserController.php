<?php

namespace App\Http\Controllers\Admin;

use App\Model\GroupUser;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use Illuminate\Support\Facades\Auth;

class GroupUserController extends Controller
{
    protected $user;
    protected $group_user;
    protected $admin_role;

    public function __construct()
    {
        $this->user = AdminFactories::userRepositories();
        $this->admin_role = AdminFactories::adminRoleRepositories();

    }

    public function index()
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'listGroupUser') ) {
            return view('admin.group_users.index');
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }  
    }

    public function loadGroupUser()
    {
       $group_users = $this->user->loadGroupUser();
       return json_encode($group_users);
    }

    public function create_group_user(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'createGroupUser') ) {
            $action = $request->get('action');
            if ($action == 'create') {
                $data_create = [
                    'name' => $request->get('name'),
                ];
                $create = $this->user->create_group_user($data_create);
                return json_encode($create);
            }
            else if ($action == 'update') {
                $data_update = [
                    'name' => $request->get('name'),
                ];
                $id = $request->get('id');
                $update = $this->user->update_group_user($data_update, $id);
                return json_encode($update);
            }
            return json_encode(false);
        }
        else {
            return json_encode(false);
        }
    }

    public function detail_group_user($id)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'detailUser') ) {
            $group_user = $this->user->detail_group_user($id);
            return view('admin.group_users.detail', compact('group_user'));
        }
        else {
            return redirect()->route('admin.home')->with('fails', 'Truy cập thất bại. Tài khoản của bạn không được phép truy cập vào trang này.');
        }
    }

    public function delete_group_user(Request $request)
    {
        if ( $this->admin_role->checkRole(Auth::id() , 'deleteGroupUser') ) {
            $id = $request->get('id');
            $delete = $this->user->delete_group_user($id);
            return json_encode($id);
        }
        else {
            return json_encode(false);
        }
    }
    // Nhóm user
    public function load_user_with_group(Request $request)
    {
        $id = $request->get('id');
        $group_user = $this->user->detail_group_user($id);
        return json_encode($group_user->users);
    }

    public function load_users(Request $request)
    {
        $group_user_id = $request->get('id');
        $users = $this->user->load_users($group_user_id);
        return json_encode($users);
    }

    public function update_user_with_group_user(Request $request)
    {
        $type = $request->get('type');
        if ($type == 'user') {
            $update = $this->user->update_user_with_group_user($request->all());
        } elseif ($type == 'group_product') {
            $update = $this->user->update_group_product_with_group_user($request->all());
        }
        return json_encode($update);
    }

    public function delete_user_with_group_user(Request $request)
    {
        $type = $request->get('type');
        if ($type == 'user') {
            $update = $this->user->delete_user_with_group_user($request->get('id'));
        } elseif ($type == 'group_product') {
            $update = $this->user->delete_group_product_with_group_user($request->get('id'));
        }
        return json_encode($update);
    }
    // Nhóm sản phẩm load_group_product_with_group_user
    public function load_group_product_with_group_user(Request $request)
    {
        $id = $request->get('id');
        $group_user = $this->user->detail_group_user($id);
        return json_encode($group_user->group_products);
    }
    // load nhóm sản phẩm
    public function load_group_products(Request $request)
    {
      $group_user_id = $request->get('id');
      $group_products = $this->user->load_group_products($group_user_id);
      return json_encode($group_products);
    }
    // Nhóm đại lý API
    public function agency_api()
    {
        $users = $this->user->all();
        return view('admin.agency.index', compact('users'));
    }

    public function list_agency_api()
    {
        $data = $this->user->list_agency_api();
        return json_encode($data);
    }

    public function create_agency(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            $create = $this->user->create_agency($request->all());
            return json_encode($create);
        } else {
            $update = $this->user->update_agency($request->all());
            return json_encode($update);
        }
    }

    public function update_agency_api(Request $request)
    {
        $agency = $this->user->detail_agency_api($request->get('id'));
        return json_encode($agency);
    }

    public function delete_agency(Request $request)
    {
        $agency = $this->user->delete_agency($request->get('id'));
        return json_encode($agency);
    }

}
