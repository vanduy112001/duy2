<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class AdminRoleController extends Controller
{
    protected $adminRole;

    public function __construct()
    {
        $this->adminRole = AdminFactories::adminRoleRepositories();
    }

    public function index(Request $request)
    {
    	// dd('da den');
    	if($request->session()->has('login_admin_roles') == 'success') {
            return view('admin.admin_roles.index');
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function detail($groupId, Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
    		$detail = $this->adminRole->loadDetailGroup($groupId);
            $role_config = config('role');
            return view('admin.admin_roles.detail', compact('detail', 'role_config'));
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function listGroup(Request $request)
    {
    	return json_encode( $this->adminRole->listGroup( $request->get('qtt'), $request->get('q') ) );
    }

    public function login(Request $request)
    {
    	$check_login = $this->adminRole->login($request->all());
    	if ($check_login == true) {
          $request->session()->put('login_admin_roles', 'success');
          return redirect()->route('admin.admin_roles.index');
        } else {
          return view('admin.admin_roles.login')->with('fails', 'Đăng nhập thất bại');
        }
    }

    public function actionGroup(Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
            if ( $request->get('action') == 'create' ) {
            	return json_encode( $this->adminRole->createGroup($request->all()) );
	        } else {
	            return json_encode( $this->adminRole->editGroup($request->all()) );
	        }
        } else {
            return view('admin.admin_roles.login');
        }
    	    
    }

    public function loadDetailGroup(Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
            return json_encode( $this->adminRole->loadDetailGroup($request->get('id')) );
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function deleteGroup(Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
            $deleteGroup = $this->adminRole->deleteGroup($request->get('id'));
	        if ( $deleteGroup ) {
	            return json_encode(true);
	        } else {
	            return json_encode(false);
	        }
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function loadUsers(Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
	        return json_encode( $this->adminRole->loadUsers($request->get('id')) );
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function loadUserAdmin(Request $request)
    {
    	if($request->session()->has('login_admin_roles') == 'success') {
	        return json_encode( $this->adminRole->loadUserAdmin($request->get('id')) );
        } else {
            return view('admin.admin_roles.login');
        }
    }

    public function update_user_with_group(Request $request)
    {
        $type = $request->get('type');
        // return json_encode($request->all());
        if ($type == 'user') {
            $update = $this->adminRole->update_user_with_group($request->all());
        } elseif ($type == 'group_product') {
            $update = $this->adminRole->update_admin_role_meta($request->all());
        }
        return json_encode($update);
    }

	// Thêm user ra nhóm quản trị
	public function delete_user_with_group(Request $request)
    {
        $type = $request->get('type');
        if ($type == 'user') {
            $update = $this->adminRole->delete_user_with_group($request->all());
        } elseif ($type == 'group_product') {
            $update = $this->adminRole->delete_admin_role_meta($request->all());
        }
        return json_encode($update);
    }

    // lấy đặc quyền của nhóm
    public function load_admin_roles(Request $request)
    {
    	return json_encode( $this->adminRole->load_admin_roles( $request->get('id'), $request->get('role') ) );
    }

    public function load_roles(Request $request)
    {
    	return json_encode( $this->adminRole->load_roles( $request->get('id') ) );
    }

    

}
