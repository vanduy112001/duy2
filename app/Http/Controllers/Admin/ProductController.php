<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\ProductValidate;

class ProductController extends Controller
{
    protected $product;
    protected $email;
    protected $user;

    public function __construct()
    {
        $this->product = AdminFactories::productRepositories();
        $this->email = AdminFactories::emailRepositories();
        $this->user = AdminFactories::userRepositories();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return view('admin.products.index');
    }

    public function createProduct(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
            ];
            $product = $this->product->create_group_product($data, '', '');
        } else {
            $id = $request->get('id');
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
            ];
            $product = $this->product->update_group_product($id,$data, '', '', '');
        }
        return json_encode($product);
    }

    public function createProductByGroupUser(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
                'group_user_id' => !empty($request->get('groupId')) ? $request->get('groupId') : 0,
                'private' => true,
            ];
            $product = $this->product->create_group_product($data, '', '');
            return json_encode([
                'error' => 0,
                'groupProduct' => $this->product->list_private_group_user($request->get('groupId')),
            ]);
        } else {
            $id = $request->get('id');
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
            ];
            $product = $this->product->update_group_product($id,$data, '', '', '');
        }
        return json_encode([ 'error' => 1 ]);
    }
    //
    public function test()
    {
        dd('da den');
    }
    // list product
    public function getProduct()
    {
        $data = [
            'group_products' => $this->product->get_group_product(),
            'pricings' => $this->product->get_pricings(),
        ];
        return json_encode($data);
    }
    // edit product
    public function editGroup(Request $request)
    {
        $id = $request->get('id');
        $group_product = $this->product->detail_group_product($id);
        return json_encode($group_product);
    }
    // tạo sản phẩm và meta sản phẩm
    public function create()
    {  
        $group_products = $this->product->get_group_products();
        $type_products = config('type_product');
        return view('admin.products.create', compact('group_products', 'type_products') );
    }

    public function store(ProductValidate $request)
    {
        $data = [
            'name' => $request->get('name'),
            'group_product_id' => $request->get('group_product_id'),
            'type_product' => $request->get('type_product'),
            'module' => !empty($request->get('module')) ? $request->get('module') : '' ,
            'hidden' => !empty($request->get('hidden')) ? $request->get('hidden') : 0 ,
            'stt' => !empty($request->get('stt')) ? $request->get('stt') : 10,
            'package' => !empty($request->get('package')) ? $request->get('package') : '',
            'description' => !empty($request->get('description')) ? $request->get('description') : '',
        ];

        $create = $this->product->store($data);
        if ($create) {
            return redirect()->route('admin.product.index')->with('success', 'Tạo sản phẩm thành công');
        } else {
            return redirect()->route('admin.product.index')->with('fails', 'Tạo sản phẩm thất bại');
        }
    }
    // sửa sản phẩm
    public function edit($id)
    {
        $product = $this->product->detail_product($id);
        $group_products = $this->product->get_group_products();
        $type_products = config('type_product');
        $emails = $this->email->getGroupEmail();
        $type_addons = config('type_addon');
        $config_os = config('os');
        $list_vps_os = $this->product->list_vps_os($id);
        $list_product_hosting_default = [];
        if ($product->type_product == 'Hosting') {
           $list_product_hosting_default = $this->product->list_product_hosting_default();
        } elseif ($product->type_product == 'Hosting-Singapore') {
           $list_product_hosting_default = $this->product->list_product_hosting_singapore_default();
        }
        $config_os_server = config('listOsServer');
        $config_datacenter = config('datacenter');
        $config_raid = config('raid');
        $config_u = config('type_u');
        $config_server_management = config('server_management');
        $group_users = $this->user->get_group_users();
        $list_product_duplicate = $this->product->list_product_duplicate($id);
       // return $product->product_datacenters;
        return view('admin.products.edit', compact('product' , 'group_products', 'type_products', 'emails', 
        'type_addons', 'config_os', 'list_vps_os', 'list_product_hosting_default', 'config_os_server', 'config_raid', 
        'config_datacenter', 'config_server_management', 'group_users', 'list_product_duplicate', 'config_u'));
    }

    //update sản phẩm
    public function update(Request $request)
    {
        $id = $request->get('id');
        // dd($request->all());
        // chi tiết product
        $data_product = [
            'name' => $request->get('name'),
            'group_product_id' => $request->get('group_product_id'),
            'type_product' => $request->get('type_product'),
            'module' => !empty($request->get('module')) ? $request->get('module') : '' ,
            'hidden' => !empty($request->get('hidden')) ? $request->get('hidden') : 0 ,
            'stt' => !empty($request->get('stt')) ? $request->get('stt') : 10,
            'package' => !empty($request->get('package')) ? $request->get('package') : '',
            'description' => !empty($request->get('description')) ? $request->get('description') : '',
        ];
        // chi tiết meta_product
        $data_meta_product = [
            'product_id' => $id,
            'cpu' => !empty($request->get('cpu')) ? $request->get('cpu') : '',
            'memory' => !empty($request->get('ram')) ? $request->get('ram') : '',
            'disk' => !empty($request->get('disk')) ? $request->get('disk') : '',
            'bandwidth' => !empty($request->get('bandwidth')) ? $request->get('bandwidth') : '',
            'ip' => !empty($request->get('ip')) ? $request->get('ip') : '',
            'os' => !empty($request->get('os')) ? $request->get('os') : '',
            'email_id' => !empty($request->get('email_id')) ? $request->get('email_id') : '',
            'email_create' => !empty($request->get('email_id')) ? $request->get('email_create') : '',
            'storage' => !empty($request->get('storage')) ? $request->get('storage') : '',
            'domain' => !empty($request->get('domain')) ? $request->get('domain') : '',
            'sub_domain' => !empty($request->get('sub_domain')) ? $request->get('sub_domain') : '',
            'alias_domain' => !empty($request->get('alias_domain')) ? $request->get('alias_domain') : '',
            'database' => !empty($request->get('database')) ? $request->get('database') : '',
            'ftp' => !empty($request->get('ftp')) ? $request->get('ftp') : '',
            'panel' => !empty($request->get('panel')) ? $request->get('panel') : '',
            'name_server' => !empty($request->get('name_server')) ? $request->get('name_server') : '',
            'hidden' => '',
            'email_expired' => $request->get('email_expired'),
            'email_expired_finish' => $request->get('email_expired_finish'),
            'type_addon' =>!empty($request->get('type_addon')) ? $request->get('type_addon') : '',
            'product_special' =>!empty($request->get('product_special')) ? $request->get('product_special') : 0,
            'girf' =>!empty($request->get('girf')) ? $request->get('girf') : null,
            'promotion' =>!empty($request->get('promotion')) ? 1 : 0,
            'qtt_email' =>!empty($request->get('qtt_email')) ? $request->get('qtt_email') : '',
            'chassis' =>!empty($request->get('chassis')) ? $request->get('chassis') : '',
            'raid' =>!empty($request->get('raid')) ? $request->get('raid') : '',
            'datacenter' =>!empty($request->get('datacenter')) ? $request->get('datacenter') : '',
            'server_management' =>!empty($request->get('server_management')) ? $request->get('server_management') : '',
            'backup' =>!empty($request->get('backup')) ? $request->get('backup') : '',
            'cores' =>!empty($request->get('cores')) ? $request->get('cores') : '',
            'port_network' =>!empty($request->get('port_network')) ? $request->get('port_network') : '',
            'email_config_finish' => !empty($request->get('email_config_finish')) ? $request->get('email_config_finish') : 0,
        ];
        // chi tiết pricing
        // 1 tháng
        $monthly = !empty($request->get('monthly')) ? $request->get('monthly') : 0;
        $monthly = str_replace( '.', '', $monthly );
        $monthly = str_replace( ',', '', $monthly );
        // 2 tháng
        $twomonthly = !empty($request->get('twomonthly')) ? $request->get('twomonthly') : 0;
        $twomonthly = str_replace( '.', '', $twomonthly );
        $twomonthly = str_replace( ',', '', $twomonthly );
        // 3 tháng
        $quarterly = !empty($request->get('quarterly')) ? $request->get('quarterly') : 0;
        $quarterly = str_replace( '.', '', $quarterly );
        $quarterly = str_replace( ',', '', $quarterly );
        // 6 tháng
        $semi_annually = !empty($request->get('semi_annually')) ? $request->get('semi_annually') : 0;
        $semi_annually = str_replace( '.', '', $semi_annually );
        $semi_annually = str_replace( ',', '', $semi_annually );
        // 1 năm
        $annually = !empty($request->get('annually')) ? $request->get('annually') : 0;
        $annually = str_replace( '.', '', $annually );
        $annually = str_replace( ',', '', $annually );
        // 2 năm
        $biennially = !empty($request->get('biennially')) ? $request->get('biennially') : 0;
        $biennially = str_replace( '.', '', $biennially );
        $biennially = str_replace( ',', '', $biennially );
        // 3 năm
        $triennially = !empty($request->get('triennially')) ? $request->get('triennially') : 0;
        $triennially = str_replace( '.', '', $triennially );
        $triennially = str_replace( ',', '', $triennially );
        // vĩnh viễn
        $one_time_pay = !empty($request->get('one_time_pay')) ? $request->get('one_time_pay') : 0;
        $one_time_pay = str_replace( '.', '', $one_time_pay );
        $one_time_pay = str_replace( ',', '', $one_time_pay );
        // data
        $data_pricing = [
            'product_id' => $id,
            'type' => !empty($request->get('type')) ? $request->get('type') : 0,
            'monthly' => $monthly,
            'twomonthly' => $twomonthly,
            'quarterly' => $quarterly,
            'semi_annually' => $semi_annually,
            'annually' => $annually,
            'biennially' => $biennially,
            'triennially' => $triennially,
            'one_time_pay' => $one_time_pay,
        ];
        // data vps_os
        $list_vps_os = !empty($request->get('os_vps')) ? $request->get('os_vps') : [];
        $list_upgrate = !empty($request->get('upgrate')) ? $request->get('upgrate') : [];
        $datacenterSelect = !empty($request->get('datacenterSelect')) ? $request->get('datacenterSelect') : [];
        $data_chassis = [
            'product_id' => $id,
            'first' => !empty($request->get('drive1')) ? $request->get('drive1') : 0,
            'second' => !empty($request->get('drive2')) ? $request->get('drive2') : 0,
            'third' => !empty($request->get('drive3')) ? $request->get('drive3') : 0,
            'four' => !empty($request->get('drive4')) ? $request->get('drive4') : 0,
            'five' => !empty($request->get('drive5')) ? $request->get('drive5') : 0,
            'six' => !empty($request->get('drive6')) ? $request->get('drive6') : 0,
            'seven' => !empty($request->get('drive7')) ? $request->get('drive7') : 0,
            'eight' => !empty($request->get('drive8')) ? $request->get('drive8') : 0,
        ];
        // dd($data_chassis);
        $raidSelect = !empty($request->get('raidSelect')) ? $request->get('raidSelect') : [];
        $serverManagementSelect = !empty($request->get('serverManagementSelect')) ? $request->get('serverManagementSelect') : [];
        $update = $this->product->update($id, $data_product, $data_meta_product, $data_pricing, $list_vps_os, $list_upgrate, 
        $data_chassis, $datacenterSelect, $raidSelect, $serverManagementSelect);
        if ($update) {
            $this->product->update_duplicate($request->all());
            return redirect()->route('admin.product.edit', $id)->with('success', 'Chỉnh sửa sản phẩm thành công');
        } else {
            return redirect()->route('admin.product.edit', $id)->with('fails', 'Chỉnh sửa sản phẩm thất bại');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        if ($action == 'product') {
            $delete = $this->product->deleteProduct($id);
            if ($delete) {
                return json_encode("Xóa sản phẩm thành công!");
            } else {
                return json_encode("Xóa sản phẩm thất bại!");
            }
        } else {
            // return json_encode($id);
            $delete = $this->product->deleteGroupProduct($id);
            if ($delete) {
                return json_encode("Xóa nhóm sản phẩm thành công!");
            } else {
                return json_encode("Xóa nhóm sản phẩm thất bại!");
            }
        }

    }
    // product private
    public function index_private($id)
    {
        $group_products = $this->product->get_group_product();
        $group_product_privates = $this->product->list_private_product('');
        $group_users = $this->user->loadGroupUser();
        $users = $this->product->get_user();
        return view('admin.product_private.index', compact('id', 'group_products', 'users', 'group_product_privates', 'group_users'));
    }

    public function list_private_product(Request $request)
    {
        $id = $request->get('id');
        $data = [
            'group_products' => $this->product->list_private_product($id),
            'pricings' => $this->product->get_pricings($id),
        ];
        return json_encode($data);
    }

    public function list_private_group_user(Request $request)
    {
        $id = $request->get('select');
        $data = [
            'group_products' => $this->product->list_private_group_user($id),
            'pricings' => $this->product->get_pricings($id),
        ];
        return json_encode($data);
    }

    public function createPrivateProduct(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'private' => true,
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
                'group_user_id' => !empty($request->get('group_user_id')) ? $request->get('group_user_id') : 0,
            ];
            $user_id = $request->get('user_id');
            $group_product_duplicate = $request->get('group_product_duplicate');
            // model tạo
            $product = $this->product->create_group_product($data,$user_id, $group_product_duplicate);
        } else {
            $id = $request->get('id');
            $data = [
                'name' => $request->get('name'),
                'title' => $request->get('title'),
                'private' => true,
                'type' => $request->get('type'),
                'stt' => !empty($request->get('stt')) ? $request->get('stt') : 20,
                'hidden' => !empty($request->get('hidden')) ? true : false,
                'link' => !empty($request->get('link')) ? $request->get('link') : '',
                'group_user_id' => !empty($request->get('group_user_id')) ? $request->get('group_user_id') : 0,
            ];
            $user_id = $request->get('user_id');
            $group_product_duplicate = $request->get('group_product_duplicate');
            // model tạo
            $product = $this->product->update_group_product($id,$data, $user_id, $group_product_duplicate, '');
        }
        return json_encode($product);
    }

    public function editGroupPrivate(Request $request)
    {
        $id = $request->get('id');
        $group_product = $this->product->detail_group_product($id);
        $meta_group_product = $this->product->detail_meta_group_product($id);
        $group_products = $this->product->get_group_product();
        $users = $this->product->get_user();
        $data = [
          'group_product' => $group_product,
          'group_products' => $group_products,
          'users' => $users,
          'meta_group_product' => $meta_group_product,
        ];
        return json_encode($data);
    }

    // tạo sản phẩm và meta sản phẩm
    public function createPrivate($id)
    {
        $group_products = $this->product->get_group_private_products($id);
        $type_products = config('type_product');
        return view('admin.product_private.create', compact('id', 'group_products', 'type_products') );
    }
    //
    public function store_product_private(ProductValidate $request)
    {
        $data = [
            'name' => $request->get('name'),
            'group_product_id' => $request->get('group_product_id'),
            'type_product' => $request->get('type_product'),
            'module' => !empty($request->get('module')) ? $request->get('module') : '' ,
            'hidden' => !empty($request->get('hidden')) ? $request->get('hidden') : 0 ,
            'stt' => !empty($request->get('stt')) ? $request->get('stt') : 10,
            'package' => !empty($request->get('package')) ? $request->get('package') : '',
            'description' => !empty($request->get('description')) ? $request->get('description') : '',
        ];

        $create = $this->product->store($data);
        if ($create) {
            return redirect()->route('admin.product_private.index', $request->get('idGroupUser'))->with('success', 'Tạo sản phẩm thành công');
        } else {
            return redirect()->route('admin.product_private.createPrivate', $request->get('idGroupUser'))->with('fails', 'Tạo sản phẩm thất bại');
        }
    }
    // sửa sản phẩm
    public function editPrivate($grouUserId, $id)
    {
        $product = $this->product->detail_product($id);
        $group_products = $this->product->get_group_private_products($grouUserId
        );
        $type_products = config('type_product');
        $emails = $this->email->getGroupEmail();
        $type_addons = config('type_addon');
        $config_os = config('os');
        $list_vps_os = $this->product->list_vps_os($id);
        $list_product_hosting_default = [];
        if ($product->type_product == 'Hosting') {
           $list_product_hosting_default = $this->product->list_product_hosting_private_default();
        } elseif ($product->type_product == 'Hosting-Singapore') {
           $list_product_hosting_default = $this->product->list_product_hosting_singapore_private_default();
        }
        $config_os_server = config('listOsServer');
        $config_datacenter = config('datacenter');
        $config_raid = config('raid');
        $config_u = config('type_u');
        $config_server_management = config('server_management');
        return view('admin.product_private.edit', compact('grouUserId', 'product' , 'group_products', 'type_products', 
        'emails', 'type_addons', 'config_os', 'list_vps_os', 'list_product_hosting_default', 'config_os_server', 
        'config_datacenter', 'config_raid', 'config_server_management', 'config_u'));
    }
    //update sản phẩm
    public function update_product_private(Request $request)
    {
        $id = $request->get('id');
        // chi tiết product
        $data_product = [
            'name' => $request->get('name'),
            'group_product_id' => $request->get('group_product_id'),
            'type_product' => $request->get('type_product'),
            'module' => !empty($request->get('module')) ? $request->get('module') : '' ,
            'hidden' => !empty($request->get('hidden')) ? $request->get('hidden') : 0 ,
            'stt' => !empty($request->get('stt')) ? $request->get('stt') : 10,
            'package' => !empty($request->get('package')) ? $request->get('package') : '',
            'description' => !empty($request->get('description')) ? $request->get('description') : '',
        ];
        // chi tiết meta_product
        $data_meta_product = [
            'product_id' => $id,
            'cpu' => !empty($request->get('cpu')) ? $request->get('cpu') : '',
            'memory' => !empty($request->get('ram')) ? $request->get('ram') : '',
            'disk' => !empty($request->get('disk')) ? $request->get('disk') : '',
            'bandwidth' => !empty($request->get('bandwidth')) ? $request->get('bandwidth') : '',
            'ip' => !empty($request->get('ip')) ? $request->get('ip') : '',
            'os' => !empty($request->get('os')) ? $request->get('os') : '',
            'email_id' => !empty($request->get('email_id')) ? $request->get('email_id') : '',
            'email_create' => !empty($request->get('email_id')) ? $request->get('email_create') : '',
            'storage' => !empty($request->get('storage')) ? $request->get('storage') : '',
            'domain' => !empty($request->get('domain')) ? $request->get('domain') : '',
            'sub_domain' => !empty($request->get('sub_domain')) ? $request->get('sub_domain') : '',
            'alias_domain' => !empty($request->get('alias_domain')) ? $request->get('alias_domain') : '',
            'database' => !empty($request->get('database')) ? $request->get('database') : '',
            'ftp' => !empty($request->get('ftp')) ? $request->get('ftp') : '',
            'panel' => !empty($request->get('panel')) ? $request->get('panel') : '',
            'name_server' => !empty($request->get('name_server')) ? $request->get('name_server') : '',
            'hidden' => '',
            'email_expired' => $request->get('email_expired'),
            'email_expired_finish' => $request->get('email_expired_finish'),
            'type_addon' =>!empty($request->get('type_addon')) ? $request->get('type_addon') : '',
            'product_special' => !empty($request->get('product_special')) ? $request->get('product_special') : 0,
            'girf' =>!empty($request->get('girf')) ? $request->get('girf') : null,
            'promotion' =>!empty($request->get('promotion')) ? 1 : 0,
            'chassis' =>!empty($request->get('chassis')) ? $request->get('chassis') : '',
            'raid' =>!empty($request->get('raid')) ? $request->get('raid') : '',
            'datacenter' =>!empty($request->get('datacenter')) ? $request->get('datacenter') : '',
            'server_management' =>!empty($request->get('server_management')) ? $request->get('server_management') : '',
            'backup' =>!empty($request->get('backup')) ? $request->get('backup') : '',
            'cores' =>!empty($request->get('cores')) ? $request->get('cores') : '',
        ];
        // chi tiết pricing
        $data_pricing = [
            'product_id' => $id,
            'type' => !empty($request->get('type')) ? $request->get('type') : 0,
            'monthly' => !empty($request->get('monthly')) ? $request->get('monthly') : 0,
            'twomonthly' => !empty($request->get('twomonthly')) ? $request->get('twomonthly') : 0,
            'quarterly' => !empty($request->get('quarterly')) ? $request->get('quarterly') : 0,
            'semi_annually' => !empty($request->get('semi_annually')) ? $request->get('semi_annually') : 0,
            'annually' => !empty($request->get('annually')) ? $request->get('annually') : 0,
            'biennially' => !empty($request->get('biennially')) ? $request->get('biennially') : 0,
            'triennially' => !empty($request->get('triennially')) ? $request->get('triennially') : 0,
            'one_time_pay' => !empty($request->get('one_time_pay')) ? $request->get('one_time_pay') : 0,
        ];
        // data vps_os
        $list_vps_os = !empty($request->get('os_vps')) ? $request->get('os_vps') : [];
        $list_upgrate = !empty($request->get('upgrate')) ? $request->get('upgrate') : [];
        $data_chassis = [
            'product_id' => $id,
            'first' => !empty($request->get('drive1')) ? $request->get('drive1') : 0,
            'second' => !empty($request->get('drive2')) ? $request->get('drive2') : 0,
            'third' => !empty($request->get('drive3')) ? $request->get('drive3') : 0,
            'four' => !empty($request->get('drive4')) ? $request->get('drive4') : 0,
            'five' => !empty($request->get('drive5')) ? $request->get('drive5') : 0,
            'six' => !empty($request->get('drive6')) ? $request->get('drive6') : 0,
            'seven' => !empty($request->get('drive7')) ? $request->get('drive7') : 0,
        ];
        $datacenterSelect = !empty($request->get('datacenterSelect')) ? $request->get('datacenterSelect') : [];
        $raidSelect = !empty($request->get('raidSelect')) ? $request->get('raidSelect') : [];
        $serverManagementSelect = !empty($request->get('serverManagementSelect')) ? $request->get('serverManagementSelect') : [];
        $update = $this->product->update($id, $data_product, $data_meta_product, $data_pricing, $list_vps_os, 
        $list_upgrate, $data_chassis, $datacenterSelect, $raidSelect, $serverManagementSelect);
        $grouUserId = $request->get('grouUserId');
        if ($update) {
            return redirect()->route('admin.product_private.editPrivate', [$grouUserId,$id])->with('success', 'Chỉnh sửa sản phẩm thành công');
        } else {
            return redirect()->route('admin.product_private.editPrivate', [$grouUserId,$id])->with('fails', 'Chỉnh sửa sản phẩm thất bại');
        }
    }
    /**STATE */
    public function state_vps_us()
    {
        return view("admin.state.index");
    }

    public function list_state()
    {
        return json_encode($this->product->list_state());
    }

    public function action_state(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            return json_encode( $this->product->create_state( $request->all() ) );
        }
        elseif ( $action == 'update' ) {
            return json_encode( $this->product->update_state( $request->all() ) );
        }
        elseif ( $action == 'delete' ) {
            return json_encode( $this->product->delete_state( $request->all() ) );
        }
        elseif ( $action == 'on' ) {
            return json_encode( $this->product->on_state( $request->all() ) );
        }
        elseif ( $action == 'off' ) {
            return json_encode( $this->product->off_state( $request->all() ) );
        }
        return false;
    }

    public function detail_state(Request $request)
    {
        return json_encode($this->product->detail_state($request->get('id')));
    }

    /**STATE Cloudzone */
    public function state_cloudzone()
    {
        return view("admin.state.state_cloudzone");
    }

    public function list_state_cloudzone()
    {
        return json_encode($this->product->list_state_cloudzone());
    }

    /**STATE Proxy*/
    public function state_proxy()
    {
        return view("admin.state.state_proxy");
    }

    public function list_state_proxy()
    {
        return json_encode($this->product->list_state_proxy());
    }

    public function action_state_proxy(Request $request)
    {
        $action = $request->get('action');
        if ( $action == 'create' ) {
            return json_encode( $this->product->create_state_proxy( $request->all() ) );
        }
        elseif ( $action == 'update' ) {
            return json_encode( $this->product->update_state_proxy( $request->all() ) );
        }
        elseif ( $action == 'delete' ) {
            return json_encode( $this->product->delete_state_proxy( $request->all() ) );
        }
        elseif ( $action == 'on' ) {
            return json_encode( $this->product->on_state_proxy( $request->all() ) );
        }
        elseif ( $action == 'off' ) {
            return json_encode( $this->product->off_state_proxy( $request->all() ) );
        }
        return false;
    }

    public function detail_state_proxy(Request $request)
    {
        return json_encode($this->product->detail_state_proxy($request->get('id')));
    }

}
