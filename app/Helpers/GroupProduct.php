<?php

namespace App\Helpers;

use App\Factories\UserFactories;

class GroupProduct
{
	public static function get_group_product()
	{
		$repo = UserFactories::homeRepositories();
		$group_products = $repo->get_group_product();
		return $group_products;
	}
	public static function get_product($id)
	{
		$repo = UserFactories::homeRepositories();
		$product = $repo->detail_product($id);
		return $product;
	}
	public static function get_domain($id)
	{
		$repo = UserFactories::homeRepositories();
		$product = $repo->detail_domain($id);
		return $product;
	}
	public static function get_product_domain($id)
	{
		$repo = UserFactories::homeRepositories();
		$product_domain = $repo->detail_product_domain($id);
		return $product_domain;
	}

	public static  function get_total_product($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$total = $repo->total_product($user_id);
		return $total;
	}

	public  static function get_total_invoice_pending($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$total = $repo->total_invoice_pending($user_id);
		return $total;
	}

	public  static function get_total_server($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$total = $repo->total_server($user_id);
		return $total;
	}

	public  static function get_total_vps($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$total = $repo->total_vps($user_id);
		return $total;
	}

	public  static function get_total_hosting($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$total = $repo->total_hosting($user_id);
		return $total;
	}

	public static function get_group_product_private($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_private($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_group_product_vps_private($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_vps_private($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_group_product_server($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_server($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_group_product_proxy($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_proxy($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_group_product_vps_us_private($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_vps_us_private($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function list_group_products_vps_us($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->list_group_products_vps_us($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_group_product_hosting_private($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$group_private = $repo->get_group_product_hosting_private($user_id);
		// dd($group_private);
		return $group_private;
	}

	public static function get_addon_product_private($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$products = $repo->get_addon_product_private($user_id);
		// dd($products);
		return $products;
	}

	public static function get_hosting($detail_order_id)
	{
		$repo = UserFactories::homeRepositories();
		$hostings = $repo->get_hostings($detail_order_id);
		return $hostings;
	}

	public static function get_vps_expired($detail_order_id)
	{
		$repo = UserFactories::homeRepositories();
		$hostings = $repo->get_vps_expired($detail_order_id);
		return $hostings;
	}

	public static function get_customer_with_service($id, $type)
	{
		switch ($type) {
			case 'hosting':
				$repo = UserFactories::homeRepositories();
				$customer = $repo->get_customer_with_hosting($id);
				return $customer;
				break;
			case 'vps':
				$repo = UserFactories::homeRepositories();
				$customer = $repo->get_customer_with_vps($id);
				return $customer;
				break;
		}
	}
	public static function get_group_product_client()
	{
		$repo = UserFactories::clientRepositories();
		$group_products = $repo->get_group_product_at_client();
		return $group_products;
	}

	public static function check_customer($makh)
	{
		$repo = UserFactories::homeRepositories();
		$customer = $repo->check_customer($makh);
		return $customer;
	}
	public static function get_notifications()
	{
		$repo = UserFactories::notificationRepositories();
		$notifications = $repo->get_notifications();
		return $notifications;
	}
	public static function get_unread_notifications_number()
	{
		$repo = UserFactories::notificationRepositories();
		$unread_notifications_number = $repo->get_unread_notifications_number();
		return $unread_notifications_number;
	}
	public static function get_unread_notifications()
	{
		$repo = UserFactories::notificationRepositories();
		$unread_notifications = $repo->get_unread_notifications();
		return $unread_notifications;
	}

	public static function get_product_change_ip($user_id)
	{
		$repo = UserFactories::homeRepositories();
		$products = $repo->get_product_change_ip($user_id);
		// dd($products);
		return $products;
	}

	public static function get_hosting_with_domain($domain)
	{
		$repo = UserFactories::homeRepositories();
		$hosting = $repo->get_product_change_ip($domain);
		// dd($products);
		return $hosting;
	}

	public static function get_total_notificationRepositories($user_id)
	{
		$repo_noti = UserFactories::notificationRepositories();
		$notifications_read = $repo_noti->get_total_notificationRepositories($user_id);
		return $notifications_read;
	}

	public static function get_domain_by_id($id)
	{
		$repo = UserFactories::domainRepositories();
		$domain = $repo->get_domain_by_id($id);
		return $domain;
	}

	public static function get_event_promotion($id , $billing_cycle)
	{
			$repo = UserFactories::productRepositories();
			$domain = $repo->get_event_promotion($id, $billing_cycle);
			return $domain;
	}

	public static function total_price_use($user_id)
	{
			$repo = UserFactories::homeRepositories();
			$total_price_use = $repo->total_price_use($user_id);
			// dd($products);
			return $total_price_use;
	}

	public static function qttVpsPros()
	{
		$repo = UserFactories::serviceRepositories();
		return $repo->qttVpsPros();
	}

	public static function get_qtt_services_on($user_id)
	{
			$repo = UserFactories::serviceRepositories();
			$get_qtt_services_on = [
				'vps_on' => $repo->get_qtt_vps_on($user_id),
				'total_vps' => $repo->qtt_all_vps($user_id),
				'nearly_vps' => $repo->qtt_vps_nearly($user_id),
				'cancel_vps' => $repo->qtt_vps_cancel($user_id),
				'vps_us_on' => $repo->get_qtt_vps_us_on($user_id),
				'total_vps_us' => $repo->qtt_all_vps_us($user_id),
				'nearly_vps_us' => $repo->qtt_vps_us_nearly($user_id),
				'cancel_vps_us' => $repo->qtt_vps_us_cancel($user_id),
				'hosting_on' => $repo->get_qtt_hosting_on($user_id),
				'total_hosting' => $repo->qtt_all_hosting($user_id),
				'nearly_hosting' => $repo->qtt_hosting_nearly($user_id),
				'cancel_hosting' => $repo->qtt_hosting_cancel($user_id),
				'server_on' => $repo->get_qtt_server_on($user_id),
				'total_server' => $repo->qtt_all_server($user_id),
				'nearly_server' => $repo->qtt_server_nearly($user_id),
				'server_cancel' => $repo->qtt_server_cancel($user_id),
				'colocation_use' => $repo->get_qtt_colocation_use($user_id),
				'colocation_on' => $repo->get_qtt_colocation_on($user_id),
				'total_colocation' => $repo->qtt_all_colocation($user_id),
				'nearly_colocation' => $repo->qtt_colocation_nearly($user_id),
				'cancel_colocation' => $repo->qtt_colocation_cancel($user_id),
				'email_hosting_on' => $repo->get_qtt_email_hosting_on($user_id),
				'total_email_hosting' => $repo->qtt_all_email_hosting($user_id),
				'nearly_email_hosting' => $repo->qtt_email_hosting_nearly($user_id),
				'domain' => $repo->get_qtt_domain_on($user_id),
				'proxy_on' => $repo->qtt_proxy_on($user_id),
				'proxy_expire' => $repo->qtt_proxy_expire($user_id),
				'proxy_cancel' => $repo->qtt_proxy_nearly($user_id),
				'proxy_total' => $repo->qtt_proxy_all($user_id),
			];
			// dd($products);
			return $get_qtt_services_on;
	}

	public static function get_config_server($serverId)
	{
		$repo = UserFactories::serviceRepositories();
		$text_config_server = $repo->get_config_server($serverId);
		return $text_config_server;
	}

}
