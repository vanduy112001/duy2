<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vps extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_id', 'ip', 'os', 'billing_cycle', 'next_due_date',
        'status', 'user', 'password', 'date_create', 'paid', 'status_vps', 'addon_id', 'addon_qtt',
        'vm_id', 'expired_id', 'security', 'ma_kh', 'description', 'price_override', 'type_vps', 'promotion',
        'expire_billing_cycle', 'location', 'state', 'description_ip', 'auto_refurn'
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function vps_config()
    {
        return $this->hasOne('App\Model\VpsConfig');
    }
    public function user_vps()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function addon()
    {
        return $this->belongsTo('App\Model\Product', 'addon_id', 'id');
    }

    public function expired_detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder', 'expired_id', 'id');
    }

    public function order_addon_vps()
    {
        return $this->hasMany('App\Model\OrderAddonVps');
    }

    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }

    public function order_change_vps()
    {
        return $this->hasMany('App\Model\OrderChangeVps');
    }

    public function customer()
    {
        return $this->belongsTo('App\Model\Customer', 'ma_kh', 'ma_customer');
    }

}
