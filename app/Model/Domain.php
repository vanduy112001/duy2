<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_domain_id','status', 'billing_cycle' , 'customer_domain', 'owner_name', 'ownerid_number', 'owner_taxcode',
         'owner_address', 'owner_email', 'owner_phone', 'ui_name', 'uiid_number', 'ui_gender', 'ui_birthdate', 'ui_address',
          'ui_province', 'ui_email', 'ui_phone', 'admin_name', 'adminid_number', 'admin_gender', 'admin_birthdate', 'admin_address',
          'admin_province', 'admin_email', 'admin_phone', 'domain', 'domain_ext', 'domain_name', 'domain_year', 'password_domain', 'next_due_date', 'promotion',
          'cmnd_after', 'cmnd_before',
    ];
    public function domain_product()
    {
        return $this->belongsTo('App\Model\DomainProduct', 'product_domain_id', 'id');
    }
    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
    public function domain_expireds()
    {
        return $this->hasMany('App\Model\DomainExpired');
    }
}
