<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductPromotion extends Model
{
    protected $fillable = [
        'promotion_id', 'product_id',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    public function promotion()
    {
        return $this->belongsTo('App\Model\Promotion');
    }
}
