<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MessageMeta extends Model
{
    protected $fillable = [
    	'message_id', 'content','user_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function message()
    {
        return $this->belongsTo('App\Model\Message');
    }

}
