<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name', 'ip', 'host_name', 'noc', 'max_account', 'type', 'account', 'password', 'port',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    
}
