<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'users';
    public $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_verified_at', 'customer_id', 'group_user_id', 'enterprise', 'admin_role_id',
        'tutorial', 'marketing', 'order', 'send_mail_marketing'
    ];

    public function user_meta()
    {
        return $this->hasOne('App\Model\UserMeta', 'user_id', 'id');
    }

    public function agency()
    {
        return $this->hasOne('App\Model\Agency', 'user_id', 'id');
    }

    public function quotations()
    {
        return $this->hasMany('App\Model\Quotation');
    }

    public function social_account()
    {
        return $this->hasOne('App\Model\SocialAccount');
    }

    public function history_pays()
    {
        return $this->hasMany('App\Model\HistoryPay');
    }

    public function orders()
    {
        return $this->hasMany('App\Model\Order');
    }

    public function vps()
    {
        return $this->hasMany('App\Model\Vps');
    }
    public function proxies()
    {
        return $this->hasMany('App\Model\Proxy');
    }
    public function hostings()
    {
        return $this->hasMany('App\Model\Hosting', 'user_id', 'id');
    }
    public function servers()
    {
        return $this->hasMany('App\Model\Server');
    }
    public function colocations()
    {
        return $this->hasMany('App\Model\Colocation', 'user_id', 'id');
    }
    public function detail_orders()
    {
        return $this->hasMany('App\Model\DetailOrder');
    }
    public function verify()
    {
        return $this->hasOne('App\Model\Verify');
    }
    public function credit()
    {
        return $this->hasOne('App\Model\Credit');
    }
    public function customers()
    {
        return $this->hasMany('App\Model\Customer', 'customer_id', 'id');
    }
    public function group_user()
    {
        return $this->belongsTo('App\Model\GroupUser');
    }
    public function admin_role()
    {
        return $this->belongsTo('App\Model\AdminRole');
    }
    public function read_notifications()
    {
        return $this->hasMany('App\Model\ReadNotification');
    }
    public function domains()
    {
        return $this->hasMany('App\Model\Domain');
    }
    public function log_activities()
    {
        return $this->hasMany('App\Model\LogActivity');
    }
    public function cmnd_verifies()
    {
        return $this->hasMany('App\Model\CmndVerify');
    }
    public function promotion_meta()
    {
        return $this->hasMany('App\Model\PromotionMeta');
    }
    public function contracts()
    {
        return $this->hasMany('App\Model\Contract');
    }
    public function send_mails()
    {
        return $this->hasMany('App\Model\SendMail');
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * Add a mutator to ensure hashed passwords
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = Hash::make($password);
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
