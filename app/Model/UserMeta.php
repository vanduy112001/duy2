<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $table = 'user_metas';
    public $primaryKey = 'id';

    protected $fillable = [
        'user_id', 'role', 'phone', 'address', 'point', 'point_used', 'affiliate', 'gender',
        'date', 'user_create', 'avatar', 'makh', 'company', 'cmnd_after', 'cmnd_before', 'cmnd', 'mst', 'abbreviation_name'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
