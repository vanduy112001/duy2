<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_id', 'type', 'ip', 'os', 'billing_cycle',
        'next_due_date', 'status', 'date_create', 'paid', 'status_server', 'server_name' ,
        'chip', 'ram', 'disk', 'location', 'amount', 'user_name', 'password', 'description',
        'raid', 'server_management', 'ip2', 'rack', 'send_mail_create', 'config_text'
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function addon()
    {
        return $this->belongsTo('App\Model\Product', 'addon_id', 'id');
    }

    public function expired_detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder', 'expired_id', 'id');
    }

    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }

    public function server_config()
    {
        return $this->hasOne('App\Model\ServerConfig');
    }

    public function server_addon_servers()
    {
        return $this->hasMany('App\Model\OrderAddonServer');
    }

    public function server_config_rams()
    {
        return $this->hasMany('App\Model\ServerConfigRam');
    }

    public function server_config_ips()
    {
        return $this->hasMany('App\Model\ServerConfigIp');
    }

}
