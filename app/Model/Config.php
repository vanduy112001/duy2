<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $fillable = [
      'type', 'account', 'password', 'otp', 'confirm', 'imei', 'rkey', 'onesignal', 'setupkey'
    ];
}
