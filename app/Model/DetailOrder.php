<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailOrder extends Model
{
    protected $fillable = [
        'order_id', 'type', 'due_date', 'description', 'status', 'paid_date',
        'sub_total', 'quantity', 'payment_method', 'date_paid', 'user_id', 'purpose',
        'addon_id', 'progressing'
    ];

    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }

    public function vps()
    {
        return $this->hasMany('App\Model\Vps');
    }

    public function proxies()
    {
        return $this->hasMany('App\Model\Proxy');
    }

    public function hostings()
    {
        return $this->hasMany('App\Model\Hosting');
    }

    public function email_hostings()
    {
        return $this->hasMany('App\Model\EmailHosting');
    }

    public function servers()
    {
        return $this->hasMany('App\Model\Server');
    }

    public function colocations()
    {
        return $this->hasMany('App\Model\Colocation');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function history_pay()
    {
        return $this->hasMany('App\Model\HistoryPay');
    }

    public function order_addon_vps()
    {
        return $this->hasMany('App\Model\OrderAddonVps');
    }

    public function order_addon_servers()
    {
        return $this->hasMany('App\Model\OrderAddonServer');
    }

    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }

    public function order_change_vps()
    {
        return $this->hasMany('App\Model\OrderChangeVps');
    }

    public function expired_vps()
    {
        return $this->hasMany('App\Model\Vps' , 'id', 'expired_id');
    }
    public function expired_hostings()
    {
        return $this->hasMany('App\Model\Hosting' , 'id', 'expired_id');
    }
    public function expired_servers()
    {
        return $this->hasMany('App\Model\Server' , 'id', 'expired_id');
    }
    public function domains()
    {
        return $this->hasMany('App\Model\Domain');
    }
    public function domain_expireds()
    {
        return $this->hasMany('App\Model\DomainExpired');
    }

    public function order_upgrade_hosting()
    {
       return $this->hasOne('App\Model\OrderUpgradeHosting');
    }

}
