<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogActivity extends Model
{
  protected $fillable =
    [
      'user_id', 'action', 'model', 'description', 'service', 'description_user'
    ];

  public function user()
  {
     return $this->belongsTo('App\Model\User');
  }



}
