<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDatacenter extends Model
{
    protected $fillable = [
        'product_id', 'datacenter',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    
}
