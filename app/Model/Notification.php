<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [ 'name' , 'content', 'status'];

    public function status()
    {
    	return $this->belongsTo('App\Model\Status');
    }
    public function read_notifications()
    {
    	return $this->hasMany('App\Model\ReadNotification');
    }
}
