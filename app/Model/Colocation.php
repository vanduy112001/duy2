<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Colocation extends Model
{
  protected $fillable = [
      'detail_order_id', 'user_id', 'type_colo', 'ip', 'os', 'billing_cycle',
      'next_due_date', 'status', 'date_create', 'paid', 'status_colo',
      'bandwidth', 'power', 'location', 'amount', 'rack', 'product_id'
  ];

  public function detail_order()
  {
      return $this->belongsTo('App\Model\DetailOrder');
  }

  public function user()
  {
      return $this->belongsTo('App\Model\User');
  }

  public function product()
  {
      return $this->belongsTo('App\Model\Product');
  }

  public function order_expireds()
  {
      return $this->hasMany('App\Model\OrderExpired');
  }

    public function colocation_config()
    {
        return $this->hasOne('App\Model\ColocationConfig');
    }
    
    public function colocation_ips()
    {
        return $this->hasMany('App\Model\ColocationIp');
    }
    
    public function colocation_config_ips()
    {
        return $this->hasMany('App\Model\ColocationConfigIp');
    }

}
