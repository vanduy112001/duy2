<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class DomainProduct extends Model
{
    protected $fillable = [ 'local_type', 'type', 'annually', 'biennially', 'triennially', 'annually_exp', 'biennially_exp', 'triennially_exp', 'promotion' ];

    public function domains()
    {
        return $this->hasMany('App\Model\Domain');
    }


}
