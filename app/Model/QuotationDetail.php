<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuotationDetail extends Model
{
    protected $fillable = [
        'quotation_id', 'product_id', 'type_product', 'addon_cpu', 
        'addon_ram', 'addon_disk', 'qtt_ip', 'qtt', 'os', 'billing_cycle', 'datacenter',
        'domain', 'amount', 'start_date', 'end_date', 'price_billing_cycle'
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    public function quotation()
    {
        return $this->belongsTo('App\Model\Quotation');
    }

    public function quotation_server()
    {
        return $this->hasOne('App\Model\QuotationServer');
    }

}
