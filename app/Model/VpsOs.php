<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VpsOs extends Model
{
    protected $fillable = [
        'product_id', 'os',
    ];

    public function product()
    {
       return $this->belongsTo('App\Model\VPS');
    }

}
