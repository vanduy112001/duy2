<?php

namespace App\Factories;

use App\Repositories\User\LoginRepositories;
use App\Repositories\User\TicketRepository;
use App\Repositories\User\UserRepositories;
use App\Repositories\User\HomeRepositories;
use App\Repositories\User\ProductRepositories;
use App\Repositories\User\ServiceRepositories;
use App\Repositories\User\PayInRepositories;
use App\Repositories\User\UserCustomerRepositories;
use App\Repositories\User\UserHomeRepositories;
use App\Repositories\User\OrderRepositories;
use App\Repositories\User\ClientRepositories;
use App\Repositories\User\NotificationRepositories;
use App\Repositories\User\DomainProductRepositories;
use App\Repositories\User\DomainRepositories;
use App\Repositories\User\CronTab;
use App\Repositories\User\CardPointRepositories;
use App\Repositories\User\MoMoRepository;
use App\Repositories\User\MessageRepositories;
use App\Repositories\User\ContractRepositories;
use App\Repositories\User\VpsRepositories;

/* Factory
 */
class UserFactories
{

  /**
   * User Login Repositories
   */
  public static function loginRepositories()
  {
      return app(LoginRepositories::class);
  }
  /**
   * User Repositories
   */
  public static function userRepositories()
  {
      return app(UserRepositories::class);
  }
  /**
   * Home Repositories
   */
  public static function homeRepositories()
  {
      return app(UserHomeRepositories::class);
  }
  /**
   * Home Repositories
   */
  public static function productRepositories()
  {
      return app(ProductRepositories::class);
  }
  /**
   * Home Repositories
   */
  public static function serviceRepositories()
  {
      return app(ServiceRepositories::class);
  }
  /**
   * Pay In Repositories
   */
  public static function payInRepositories()
  {
      return app(PayInRepositories::class);
  }
  /**
   * Customer Repositories
   */
  public static function customerRepositories()
  {
      return app(UserCustomerRepositories::class);
  }
    /**
   * Order Repositories
   */
  public static function orderRepositories()
  {
      return app(OrderRepositories::class);
  }


    /**
     * @return  TicketRepository
     */
    public static function ticketRepository()
    {
        return app(TicketRepository::class);
    }
    /**
     * @return  ClientRepository
     */
    public static function clientRepositories()
    {
        return app(ClientRepositories::class);
    }
    /**
     * @return  NotificationRepository
     */
    public static function notificationRepositories()
    {
        return app(NotificationRepositories::class);
    }
    /**
     * @return  CronTab
     */
    public static function cronTab()
    {
        return app(CronTab::class);
    }
    /**
    * @return  DomainProductRepository
    */
   public static function domainProductRepositories()
   {
       return app(DomainProductRepositories::class);
   }
   /**
    * @return  DomainRepository
    */
   public static function domainRepositories()
   {
       return app(DomainRepositories::class);
   }
   /**
    * @return  CardPointRepositories
    */
   public static function cardPointRepositories()
   {
       return app(CardPointRepositories::class);
   }
   /**
    * @return  MoMoRepository
    */
   public static function moMoRepository()
   {
       return app(MoMoRepository::class);
   }
   /**
    * @return  MessageRepositories
    */
   public static function messageRepositories()
   {
       return app(MessageRepositories::class);
   }
   /**
    * @return  ContractRepositories
    */
    public static function contractRepositories()
    {
        return app(ContractRepositories::class);
    }
    public static function VpsRepositories(){
        return app(VpsRepositories::class);
    }

}
