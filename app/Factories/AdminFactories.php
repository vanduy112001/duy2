<?php

namespace App\Factories;

use App\Repositories\Admin\AuthRepositories;
use App\Repositories\Admin\EmailRepositories;
use App\Repositories\Admin\UserRepositories;
use App\Repositories\Admin\ProductRepositories;
use App\Repositories\Admin\LoginRepositories;
use App\Repositories\Admin\OrderRepositories;
use App\Repositories\Admin\PromotionRepositories;
use App\Repositories\Admin\InvoiceRepositories;
use App\Repositories\Admin\VpsRepositories;
use App\Repositories\Admin\NotificationRepositories;
use App\Repositories\Admin\HostingRepositories;
use App\Repositories\Admin\ServerRepositories;
use App\Repositories\Admin\PaymentRepositories;
use App\Repositories\Admin\HomeRepositories;
use App\Repositories\Admin\DirectAdminRepositories;
use App\Repositories\Admin\DashBoardRepositories;
use App\Repositories\Admin\TicketRepository;
use App\Repositories\Admin\DomainProductRepositories;
use App\Repositories\Admin\DirectAdminSingaporeRepositories;
use App\Repositories\Admin\ServerHostingRepositories;
use App\Repositories\Admin\DomainRepositories;
use App\Repositories\Admin\EventRepository;
use App\Repositories\Admin\TutorialRepositories;
use App\Repositories\Admin\CmndRepositories;
use App\Repositories\Admin\ColocationRepositories;
use App\Repositories\Admin\EmailHostingRepositories;
use App\Repositories\Admin\ContractRepositories;
use App\Repositories\Admin\AdminRoleRepositories;
use App\Repositories\Admin\MessageRepositories;
// api
use App\Repositories\Api\AgencyRepositories;
use App\Repositories\Api\ApiOrderRepositories;
use App\Repositories\Api\ApiServiceRepositories;
// apimobile
use App\Repositories\Api\Admin\ApiUserRepository;
use App\Repositories\Api\Admin\ApiProductRepository;

/* Factory
 */

class AdminFactories
{

    /**
     * Email Repositories
     */
    public static function emailRepositories()
    {
        return app(EmailRepositories::class);
    }

    /**
     * User Repositories
     */
    public static function userRepositories()
    {
        return app(UserRepositories::class);
    }

    /**
     * Product Repositories
     */
    public static function productRepositories()
    {
        return app(ProductRepositories::class);
    }
    /**
     * Login Repositories
     */
    public static function loginRepositories()
    {
        return app(LoginRepositories::class);
    }
    /**
     * Login Repositories
     */
    public static function authRepositories()
    {
        return app(AuthRepositories::class);
    }
    /**
     * Order Repositories
     */
    public static function orderRepositories()
    {
        return app(OrderRepositories::class);
    }
    /**
     * Promotion Repositories
     */
    public static function promotionRepositories()
    {
        return app(PromotionRepositories::class);
    }
    /*** Invoice Repositories
     */
    public static function invoiceRepositories()
    {
        return app(InvoiceRepositories::class);
    }
    /**
     * Invoice Repositories
     */
    public static function vpsRepositories()
    {
        return app(VpsRepositories::class);
    }

    /* Notification Repositories
   */
    public static function notificationRepositories()
    {
        return app(NotificationRepositories::class);
    }
    /* Hosting Repositories
   */
    public static function hostingRepositories()
    {
        return app(HostingRepositories::class);
    }
    /* Server Repositories
   */
    public static function serverRepositories()
    {
        return app(ServerRepositories::class);
    }
    /* Server Repositories
   */
    public static function paymentRepositories()
    {
        return app(PaymentRepositories::class);
    }
    /* Server Repositories
   */
    public static function homeRepositories()
    {
        return app(HomeRepositories::class);
    }

    /* DirectAdmin Repositories
   */
    public static function directAdminRepositories()
    {
        return app(DirectAdminRepositories::class);
    }
    /* DirectAdmin Singapore Repositories
   */
    public static function directAdminSingaporeRepositories()
    {
        return app(DirectAdminSingaporeRepositories::class);
    }
    /* DashBoard Repositories
   */
    public static function dashBoardRepositories()
    {
        return app(DashBoardRepositories::class);
    }

    /**
     * @return  TicketRepository
     */
    public static function ticketRepository()
    {
        return app(TicketRepository::class);
    }


    /* DomainProduct Repositories
   */
    public static function domainProductRepositories()
    {
        return app(DomainProductRepositories::class);
    }


    /**
     * @return  serverHostingRepositories
     */
    public static function serverHostingRepositories()
    {
        return app(ServerHostingRepositories::class);
    }

    /* DashBoard Repositories
   */
    public static function domainRepositories()
    {
        return app(DomainRepositories::class);
    }

    /* event Repository
   */
    public static function eventRepository()
    {
        return app(EventRepository::class);
    }
    /* Tutorial Repositories
   */
    public static function tutorialRepositories()
    {
        return app(TutorialRepositories::class);
    }
    /* Colocation Repositories
   */
    public static function colocationRepositories()
    {
        return app(ColocationRepositories::class);
    }
    /* EmailHosting Repositories
   */
    public static function emailHostingRepositories()
    {
        return app(EmailHostingRepositories::class);
    }
    /* Chứng minh nhân dân Repositories
   */
    public static function cmndRepositories()
    {
        return app(CmndRepositories::class);
    }
    /* Hợp đồng cung cấp dịch vụ Repositories
   */
    public static function contractRepositories()
    {
        return app(ContractRepositories::class);
    }
    /* adminRoleRepositories
   */
    public static function adminRoleRepositories()
    {
        return app(AdminRoleRepositories::class);
    }
    /* MessageRepositories
   */
    public static function messageRepositories()
    {
        return app(MessageRepositories::class);
    }


// Api
    public static function agencyRepositories()
    {
        return app(AgencyRepositories::class);
    }

    public static function apiOrderRepositories()
    {
        return app(ApiOrderRepositories::class);
    }

    public static function apiServiceRepositories()
    {
        return app(ApiServiceRepositories::class);
    }

    public static function apiUserRepository()
    {
        return app(ApiUserRepository::class);
    }

    public static function apiProductRepository()
    {
        return app(ApiProductRepository::class);
    }

}
