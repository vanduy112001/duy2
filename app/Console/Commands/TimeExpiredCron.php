<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Factories\UserFactories;

class TimeExpiredCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timeExpired:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiểm tra các dịch vụ gần hết hạn và đã hết hạn';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        $repo->expiredVPS();
        $repo->expiredHosting();
    }
}
