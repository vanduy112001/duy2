<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class DeleteVPS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteVPS:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete VPS 0h - 12h';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        $repo->deleteVPS();
        $repo->deleteProxy();
        $repo->deleteServer();
        $repo->deleteHosting();
        $repo->deleteColocation();
    }
}
