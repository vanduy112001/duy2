<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Factories\UserFactories;

class ApiPaymentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'payment:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Kiểm tra thanh toán các dịch vụ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        // $repo->check_gd_vcb();
        $repo->momo();
    }
}
