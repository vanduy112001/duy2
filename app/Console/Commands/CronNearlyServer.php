<?php

namespace App\Console\Commands;
use App\Factories\UserFactories;

use Illuminate\Console\Command;

class CronNearlyServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'NearlyServer:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Nhắc gia hạn Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        $repo->expiredServer();
    }
}
