<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class DeleteService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'deleteService:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Xóa các table cần xóa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        $repo->delete_order();
        $repo->delete_invoice_expired();
        // $repo->delete_log_activity();
        $repo->delete_payment();
        // $repo->delete_send_mail();
    }
}
