<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class MailErrorAutoRefundUser implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $user;
    protected $email_to;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $data, $subject)
    {
        $this->data = $data;
        $this->user = $user;
        $this->email_to = $user->email;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::send('admin.cron_mail.mail_cron_auto_refurn_error', $this->data, function($message){
                    $message->from('support@cloudzone.com.vn', 'Cloudzone Portal');
                    $message->to($this->email_to)->subject($this->subject);
            });
        } catch (Exception $e) {
            report($e);
        }
    }
}
