<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendMailCronNearlyColocation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $user;
    protected $email_to;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($user, $data)
    {
        $this->data = $data;
        $this->user = $user;
        $this->email_to = $user->email;
        $this->subject = !empty($data['subject']) ? $data['subject'] : 'Thông báo hết hạn';
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::send('users.mails.mail_cron_nearly_colocation', $this->data, function($message){
                 $message->from('support@cloudzone.com.vn', 'Cloudzone Portal');
                 $message->to($this->email_to)->subject($this->subject);
            });
        } catch (Exception $e) {
            report($e);
        }
    }
}
