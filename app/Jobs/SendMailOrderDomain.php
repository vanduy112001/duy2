<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailable;
use Mail;

class SendMailOrderDomain implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data_mail;
    public function __construct($data_mail)
    {
        $this->data_mail = $data_mail;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::send('users.mails.order-domain-confirm', $data_mail, function($message){
            $message->from('support@cloudzone.com.vn', 'Cloudzone Portal');
            $message->to($this->data_mail['user_email'])->subject('Xác thực đơn hàng Cloudzone - Domain '.$data_mail['domain']);
        });
    }
}
