<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class MailErrorUpgradeVps implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->subject = $data['subject'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            Mail::send('admin.cron_mail.error_upgrade_vps', $this->data, function($message){
                $message->from('support@cloudzone.com.vn', 'Cloudzone Portal');
                $message->to('support@cloudzone.com.vn')->subject($this->subject);
            });
        } catch (Exception $e) {
            report($e);
        }
    }
}
