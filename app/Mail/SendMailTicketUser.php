<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailTicketUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $ticketId;
    public $content;

    public function __construct($user, $ticketId, $content)
    {
        $this->user = $user;
        $this->ticketId = $ticketId;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'name' => $this->user->name,
            'email' => $this->user->email,
            'ticket_id' => $this->ticketId,
            'content' => $this->content,
        ];

        return $this->subject($this->subject)
            ->view('admin.tickets.email')->with($data);
    }
}
