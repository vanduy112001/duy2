<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




// admin login
Route::get('/admin/login', [
    'as' => 'admin.login',
    'uses' => 'Admin\LoginController@index'
]);
Route::post('/admin/login', [
    'as' => 'admin.checklogin',
    'uses' => 'Admin\LoginController@login'
]);
Route::get('/admin/logout', [
    'as' => 'admin.logout',
    'uses' => 'Admin\LoginController@logout'
]);
Route::get('/admin/login_at_admin', [
    'as' => 'admin.login_at_admin',
    'uses' => 'Admin\LoginController@login_at_admin'
]);

Route::group(['prefix' => 'admin','namespace' => 'Admin', 'middleware' => 'check.admin'], function() {

    //Notification Template
    Route::get('notifications/', [
        'as' => 'admin.notification.index',
        'uses' => 'NotificationController@index'
    ]);
    Route::get('notifications/create', [
        'as' => 'admin.notification.create',
        'uses' => 'NotificationController@create'
    ]);
    Route::post('notifications/store', [
        'as' => 'admin.notification.store',
        'uses' => 'NotificationController@store'
    ]);
    Route::get('notifications/edit/{id}', [
        'as' => 'admin.notification.edit',
        'uses' => 'NotificationController@edit'
    ]);
    Route::post('notifications/update', [
        'as' => 'admin.notification.update',
        'uses' => 'NotificationController@update'
    ]);
    Route::post('notifications/delete', [
        'as' => 'admin.notification.delete',
        'uses' => 'NotificationController@delete'
    ]);
    Route::post('notifications/multidelete', [
        'as' => 'admin.notification.multidelete',
        'uses' => 'NotificationController@multidelete'
    ]);
    Route::post('notifications/search', [
        'as' => 'admin.notification.search',
        'uses' => 'NotificationController@search'
    ]);

    //User Template
    Route::get('users/', [
        'as' => 'admin.user.list',
        'uses' => 'UserController@index'
    ]);
    Route::get('users/detail/{id}', [
        'as' => 'admin.user.detail',
        'uses' => 'UserController@detail'
    ]);
    Route::post('users/send-mail', [
        'as' => 'admin.user.sendMail',
        'uses' => 'UserController@sendMail'
    ]);
    Route::get('users/create', [
        'as' => 'admin.user.create',
        'uses' => 'UserController@create'
    ]);
    Route::get('users/addPayment', [
        'as' => 'admin.user.addPayment',
        'uses' => 'UserController@addPayment'
    ]);
    Route::post('users/addPayment', [
        'as' => 'admin.user.addPayment',
        'uses' => 'UserController@addPaymentForm'
    ]);
    Route::get('users/edit/{id}', [
        'as' => 'admin.user.edit',
        'uses' => 'UserController@edit'
    ]);
    Route::post('users/store', [
        'as' => 'admin.user.store',
        'uses' => 'UserController@store'
    ]);
    Route::post('users/update', [
        'as' => 'admin.user.update',
        'uses' => 'UserController@update'
    ]);
    Route::post('users/updateCloudzonePoint', [
        'as' => 'admin.user.updateCloudzonePoint',
        'uses' => 'UserController@updateCloudzonePoint'
    ]);
    Route::post('users/delete', [
        'as' => 'admin.user.delete',
        'uses' => 'UserController@delete'
    ]);
    Route::post('users/multidelete', [
        'as' => 'admin.user.multidelete',
        'uses' => 'UserController@multidelete'
    ]);

    Route::get('users/list_user_personal' , 'UserController@list_user_personal')->name('admin.user.list_user_personal');
    Route::get('users/list_user_enterprise' , 'UserController@list_user_enterprise')->name('admin.user.list_user_enterprise');
    Route::get('users/lich-su-giao-dich/{id}' , 'UserController@history_pay')->name('admin.user.history_pay');
    Route::get('users/lich-su-dat-hang/{id}' , 'UserController@history_order')->name('admin.user.history_order');
    Route::get('users/tim-kiem-lich-su-giao-dich' , 'UserController@search_history_pay')->name('admin.user.search_history_pay');
    Route::get('users/lich-su-bao-gia/{id}' , 'UserController@history_quotation')->name('admin.user.history_quotation');
    Route::get('users/change_social/{id}' , 'UserController@change_social')->name('admin.user.change_social');
    /*
      * Admin Role
    */
    Route::get('/admin-roles/danh-sach-vai-tro-quan-tri-vien.html', 'AdminRoleController@index')->name('admin.admin_roles.index');
    Route::get('/admin-roles/{groupId}/chi-tiet-nhom-vai-tro-quan-tri-vien.html', 'AdminRoleController@detail')->name('admin.admin_roles.detail');
    Route::post('/admin-roles/login', 'AdminRoleController@login')->name('admin.admin_roles.login');
    Route::post('/admin-roles/actionGroup', 'AdminRoleController@actionGroup')->name('admin.admin_roles.actionGroup');
    Route::post('/admin-roles/deleteGroup', 'AdminRoleController@deleteGroup')->name('admin.admin_roles.deleteGroup');
    Route::post('/admin-roles/update_user_with_group', 'AdminRoleController@update_user_with_group')->name('admin.admin_roles.update_user_with_group');
    Route::post('/admin-roles/delete_user_with_group', 'AdminRoleController@delete_user_with_group')->name('admin.admin_roles.delete_user_with_group');
    Route::get('/admin-roles/listGroup', 'AdminRoleController@listGroup')->name('admin.admin_roles.listGroup');
    Route::get('/admin-roles/loadDetailGroup', 'AdminRoleController@loadDetailGroup')->name('admin.admin_roles.loadDetailGroup');
    Route::get('/admin-roles/loadUsers', 'AdminRoleController@loadUsers')->name('admin.admin_roles.loadUsers');
    Route::get('/admin-roles/loadUserAdmin', 'AdminRoleController@loadUserAdmin')->name('admin.admin_roles.loadUserAdmin');
    Route::get('/admin-roles/load_admin_roles', 'AdminRoleController@load_admin_roles')->name('admin.admin_roles.load_admin_roles');
    Route::get('/admin-roles/load_roles', 'AdminRoleController@load_roles')->name('admin.admin_roles.load_roles');
/*
* Báo giá
*/
    Route::get('users/danh-sach-bao-gia' , 'UserController@quotation')->name('admin.user.quotation');
    Route::get('users/list_quotation' , 'UserController@list_quotation')->name('admin.user.list_quotation');
    Route::get('users/get_detail_quotation' , 'UserController@get_detail_quotation')->name('admin.user.get_detail_quotation');
    Route::get('users/quotation/get_group_product' , 'UserController@quotation_get_group_product')->name('admin.user.get_group_product');
    Route::get('users/quotation/get_product' , 'UserController@get_product')->name('admin.user.get_product');
    
    Route::get('users/quotation/delete_quotation' , 'UserController@delete_quotation')->name('admin.user.delete_quotation');
    Route::get('users/tao-bao-gia-moi' , 'UserController@create_quotation')->name('admin.quotation.create');
    Route::get('users/chinh-sua-bao-gia/{id}' , 'UserController@edit_quotation')->name('admin.quotation.edit');
    Route::get('users/quotation/create_pdf/{id}' , 'UserController@create_pdf_quotation')->name('admin.quotation.create_pdf');
    Route::get('users/xem-truoc-bao-gia/{id}' , 'UserController@preview_quotation')->name('admin.quotation.preview');
    Route::post('users/tao-bao-gia-moi' , 'UserController@store_quotation')->name('admin.quotation.store');
    Route::post('users/sua-bao-gia' , 'UserController@update_quotation')->name('admin.quotation.update');
    Route::post('users/quotation/create_user' , 'UserController@quotation_create_user')->name('admin.quotation.quotation_create_user');
    Route::get('users/tao-bao-gia-theo-khach-hang/{userId}' , 'UserController@create_quotation_by_user')->name('admin.quotation.create_by_user');
/*
* Khách hàng
*/
    Route::get('users/login-user/{id}' , 'UserController@login_user')->name('admin.user.login_user');
    Route::get('users/edit-credit/{id}' , 'UserController@edit_credit')->name('admin.user.edit_credit');
    Route::post('users/update-credit/{id}' , 'UserController@update_credit_with_admin')->name('admin.user.update_credit');
    Route::get('users/list_all_user' , 'UserController@list_all_user')->name('admin.user.list_all_user');
    Route::get('users/list_hosting_by_user' , 'UserController@list_hosting_by_user')->name('admin.user.list_hosting_by_user');
    Route::get('users/list_vps_by_user' , 'UserController@list_vps_by_user')->name('admin.user.list_vps_by_user');
    Route::get('users/list_vps_us_by_user' , 'UserController@list_vps_us_by_user')->name('admin.user.list_vps_us_by_user');
    Route::post('/users/updateCredit' , 'UserController@updateCredit')->name('admin.user.updateCredit');
    Route::post('/users/updateCreditTotal' , 'UserController@updateCreditTotal')->name('admin.user.updateCreditTotal');
    Route::post('/users/actionUser' , 'UserController@actionUser')->name('admin.user.actionUser');
    Route::get('/users/send_mail_with_user/{id}' , 'UserController@send_mail_with_user')->name('admin.user.send_mail_with_user');
    Route::post('/users/send_mail_with_user' , 'UserController@form_send_mail')->name('admin.user.form_send_mail');
    Route::get('users/searchUser' , 'UserController@searchUser')->name('admin.user.searchUser');
/*
* Admin Home
*/
    Route::get('/home', 'AdminController@index')->name('admin.home');
    Route::get('/giao-dich-momo', 'AdminController@momo')->name('admin.momo.index');
    Route::post('/dang-nhap-momo', 'AdminController@login_momo')->name('admin.momo.login');
    Route::get('/chinh-sua-giao-dich-momo/{id_momo}', 'AdminController@edit_momo')->name('admin.momo.edit');
    Route::post('/chinh-sua-giao-dich-momo/', 'AdminController@update_momo')->name('admin.momo.update');
    Route::get('/momo/search_momo', 'AdminController@search_momo')->name('admin.momo.search_momo');
    Route::get('/chart-dashboard', 'AdminController@chart_dashboard')->name('admin.chart_dashboard');
    Route::get('/chart-total-price', 'AdminController@chart_total_price')->name('admin.chart_total_price');
    Route::get('/chart-user', 'AdminController@chart_user')->name('admin.chart_user');
    Route::get('/chart-vps', 'AdminController@chart_vps')->name('admin.chart_vps');
    Route::get('/chart-hosting', 'AdminController@chart_hosting')->name('admin.chart_hosting');
    Route::get('/chart-payment', 'AdminController@chart_payment')->name('admin.chart_payment');
    Route::get('/load_siderbar_top', 'AdminController@load_siderbar_top')->name('admin.load_siderbar_top');
    Route::get('/load_all_services', 'AdminController@load_all_services')->name('admin.load_all_services');
    Route::get('/load_tong_thu', 'AdminController@load_tong_thu')->name('admin.load_tong_thu');
    // API cho đại lý
    Route::get('/quan-ly-dai-ly-api', 'GroupUserController@agency_api')->name('admin.agency.index');
    Route::get('/agency/danh-sach-agency', 'GroupUserController@list_agency_api')->name('admin.agency.list_agency_api');
    Route::get('/agency/chinh-sua-agency', 'GroupUserController@update_agency_api')->name('admin.agency.update_agency_api');
    Route::get('/agency/delete_agency', 'GroupUserController@delete_agency')->name('admin.agency.delete_agency');
    Route::post('/agency/create_agency', 'GroupUserController@create_agency')->name('admin.agency.create_agency');
    // Nhóm đại lý
    Route::get('/dai-ly', 'GroupUserController@index')->name('admin.user.groupUser');
    Route::get('/dai-ly/chi-tiet-dai-ly/{id}', 'GroupUserController@detail_group_user')->name('admin.user.detail_group_user');
    Route::get('/user/loadGroupUser', 'GroupUserController@loadGroupUser')->name('admin.user.loadGroupUser');
    Route::get('/user/load_user_with_group', 'GroupUserController@load_user_with_group')->name('admin.user.load_user_with_group');
    Route::get('/user/load_users', 'GroupUserController@load_users')->name('admin.user.load_users');
    Route::get('/user/load_group_product_with_group_user', 'GroupUserController@load_group_product_with_group_user')->name('admin.user.load_group_product_with_group_user');
    Route::get('/user/load_group_products', 'GroupUserController@load_group_products')->name('admin.user.load_group_products');
    Route::post('/user/create_group_user', 'GroupUserController@create_group_user')->name('admin.user.create_group_user');
    Route::post('/user/delete_group_user', 'GroupUserController@delete_group_user')->name('admin.user.delete_group_user');
    Route::post('/user/update_user_with_group_user', 'GroupUserController@update_user_with_group_user')->name('admin.user.update_user_with_group_user');
    Route::post('/user/delete_user_with_group_user', 'GroupUserController@delete_user_with_group_user')->name('admin.user.delete_user_with_group_user');
    // Email template
    Route::get('/email-template', 'EmailController@index')->name('email.index');
    Route::get('/email/listGroupEmail', 'EmailController@listGroupEmail')->name('email.listGroupEmail');
    Route::post('/email/CreateGroupEmail', 'EmailController@CreateGroupEmail')->name('email.CreateGroupEmail');
    Route::post('/email/editGroupEmail', 'EmailController@editGroupEmail')->name('email.editGroupEmail');
    Route::post('/email/createEmailTemplate', 'EmailController@createEmailTemplate')->name('email.createEmailTemplate');
    Route::get('/emails/editEmail', 'EmailController@editEmail')->name('email.editEmail');
    Route::get('/emails/deleteEmail/{id}', 'EmailController@deleteEmail')->name('email.deleteEmail');
    Route::post('/emails/updateTemplate', 'EmailController@updateTemplate')->name('email.updateEmail');
    // Email Marketing
    Route::get('/email-marketing', 'EmailController@list_email_marketing')->name('email.email_marketing.list');
    Route::get('/email-marketing/create-email', 'EmailController@create_email_marketing')->name('email.email_marketing.create');
    Route::get('/email-marketing/edit-email/{id}', 'EmailController@edit_email_marketing')->name('email.email_marketing.edit');
    Route::get('/email-marketing/send-email/{id}', 'EmailController@send_email_marketing')->name('email.email_marketing.send');
    Route::get('/email-marketing/clone-email/{id}', 'EmailController@clone_email_marketing')->name('email.email_marketing.clone');
    Route::get('/email-marketing/list-email-marketing', 'EmailController@get_list_email_marketing')->name('email.email_marketing.get_list');
    Route::post('/email-marketing/action-email-marketing', 'EmailController@action_email_marketing')->name('email.email_marketing.action');
    Route::post('/email-marketing/create-email', 'EmailController@store_email_marketing')->name('email.email_marketing.store');
    Route::post('/email-marketing/send-email', 'EmailController@form_send_email_marketing')->name('email.email_marketing.form_send_mail');
    Route::put('/email-marketing/update-email', 'EmailController@update_email_marketing')->name('email.email_marketing.update');
    // Email Service
    Route::get('/email-service', 'EmailController@list_email_service')->name('email.service.list');
    Route::get('/email-service/list-email-service', 'EmailController@get_list_email_service')->name('email.service.get');
    Route::get('/email-service/create-email', 'EmailController@create_email_service')->name('email.service.create');
    Route::get('/email-service/edit-email/{id}', 'EmailController@edit_email_service')->name('email.service.edit');
    Route::post('/email-service/create-email', 'EmailController@store_email_service')->name('email.service.store');
    Route::post('/email-service/action-email-service', 'EmailController@action_email_service')->name('email.service.action');
    Route::put('/email-service/update-email', 'EmailController@update_email_service')->name('email.service.update');
    // product
    Route::get('/products', 'ProductController@index')->name('admin.product.index');
    Route::post('/products/create-product', 'ProductController@createProduct')->name('admin.product.createProduct');
    Route::post('//products/create-product-by-group-user', 'ProductController@createProductByGroupUser')->name('admin.product.createProductByGroupUser');
    Route::get('/products/list-product', 'ProductController@getProduct')->name('admin.product.getProduct');
    Route::get('/products/editGroup', 'ProductController@editGroup')->name('admin.product.editGroup');
    Route::get('/products/create', 'ProductController@create')->name('admin.product.create');
    Route::get('/products/edit/{id}', 'ProductController@edit')->name('admin.product.edit');
    Route::post('/products/store', 'ProductController@store')->name('admin.product.store');
    Route::post('/products/update', 'ProductController@update')->name('admin.product.update');
    Route::get('/products/delete', 'ProductController@delete')->name('admin.product.delete');
    // product private
    Route::get('/product-privates/{id}', 'ProductController@index_private')->name('admin.product_private.index');
    Route::get('/product_privates/list-private-product', 'ProductController@list_private_product')->name('admin.product_private.list_private_product');
    Route::get('/product_privates/list_private_group_user', 'ProductController@list_private_group_user')->name('admin.product_private.list_private_group_user');
    Route::get('/product_privates/editGroup', 'ProductController@editGroupPrivate')->name('admin.product.editGroupPrivate');
    Route::post('/product_privates/create-group-product', 'ProductController@createPrivateProduct')->name('admin.product.createPrivateProduct');
    Route::get('/product_privates/{id}/create', 'ProductController@createPrivate')->name('admin.product_private.createPrivate');
    Route::get('/product_privates/{grouUserId}/edit/{id}', 'ProductController@editPrivate')->name('admin.product_private.editPrivate');
    Route::post('/product_private/store', 'ProductController@store_product_private')->name('admin.product_private.store');
    Route::post('/product_private/update', 'ProductController@update_product_private')->name('admin.product_private.update');
    //bang
    Route::get('/product/quan-ly-bang-vps-us', 'ProductController@state_vps_us')->name('admin.product.state_vps_us');
    Route::get('/products/list_state', 'ProductController@list_state')->name('admin.product.list_state');
    Route::get('/products/detail_state', 'ProductController@detail_state')->name('admin.product.detail_state');
    Route::post('/products/action_state', 'ProductController@action_state')->name('admin.product.action_state');
    // bang cloudzone
    Route::get('/product/quan-ly-bang-vps-us-cloudzone', 'ProductController@state_cloudzone')->name('admin.product.state_cloudzone');
    Route::get('/products/list_state_cloudzone', 'ProductController@list_state_cloudzone')->name('admin.product.list_state_cloudzone');
    // bang proxy
    Route::get('/product/quan-ly-bang-proxy', 'ProductController@state_proxy')->name('admin.product.state_proxy');
    Route::get('/products/list_state_proxy', 'ProductController@list_state_proxy')->name('admin.product.list_state_proxy');
    Route::get('/products/detail_state_proxy', 'ProductController@detail_state_proxy')->name('admin.product.detail_state_proxy');
    Route::post('/products/action_state_proxy', 'ProductController@action_state_proxy')->name('admin.product.action_state_proxy');
    // order
    Route::get('/orders', 'OrderController@index')->name('admin.orders.index');
    Route::get('/orders/create', 'OrderController@create')->name('admin.orders.create');
    Route::get('/orders/filter_user', 'OrderController@filter_user')->name('admin.orders.filter_user');
    Route::get('/orders/filter_status', 'OrderController@filter_status')->name('admin.orders.filter_status');
    Route::get('/orders/list_order_of_personal', 'OrderController@list_order_of_personal')->name('admin.orders.list_order_of_personal');
    Route::get('/orders/list_order_of_enterprise', 'OrderController@list_order_of_enterprise')->name('admin.orders.list_order_of_enterprise');
    Route::post('/orders/store', 'OrderController@store')->name('admin.orders.store');
    Route::post('/orders/update', 'OrderController@update')->name('admin.orders.update');
    Route::post('/orders/edit_total_order', 'OrderController@edit_total_order')->name('admin.orders.edit_total_order');
    Route::get('/orders/list_product', 'OrderController@list_product')->name('admin.orders.list_product');
    Route::get('/orders/list_product_addon', 'OrderController@list_product_addon')->name('admin.orders.list_product_addon');
    Route::get('/orders/list_expire_vps', 'OrderController@list_expire_vps')->name('admin.orders.list_expire_vps');
    Route::get('/orders/list_expire_vps_us', 'OrderController@list_expire_vps_us')->name('admin.orders.list_expire_vps_us');
    Route::get('/orders/list_expire_server', 'OrderController@list_expire_server')->name('admin.orders.list_expire_server');
    Route::get('/orders/list_expire_hosting', 'OrderController@list_expire_hosting')->name('admin.orders.list_expire_hosting');
    Route::get('/orders/list_expire_email_hosting', 'OrderController@list_expire_email_hosting')->name('admin.orders.list_expire_email_hosting');
    Route::get('/orders/list_expire_colocation', 'OrderController@list_expire_colocation')->name('admin.orders.list_expire_colocation');
    Route::get('/orders/get_total_vps_expire', 'OrderController@get_total_vps_expire')->name('admin.orders.get_total_vps_expire');
    Route::get('/orders/get_total_hosting_expire', 'OrderController@get_total_hosting_expire')->name('admin.orders.get_total_hosting_expire');
    Route::get('/orders/get_total_email_hosting_expire', 'OrderController@get_total_email_hosting_expire')->name('admin.orders.get_total_email_hosting_expire');
    Route::get('/orders/choose_addon_vps', 'OrderController@choose_addon_vps')->name('admin.orders.choose_addon_vps');
    Route::get('/orders/get_product_upgrade_hosting', 'OrderController@get_product_upgrade_hosting')->name('admin.orders.get_product_upgrade_hosting');
    Route::get('/orders/choose_upgrade_hosting', 'OrderController@choose_upgrade_hosting')->name('admin.orders.choose_upgrade_hosting');
    Route::get('/orders/choose_change_ip_vps', 'OrderController@choose_change_ip_vps')->name('admin.orders.choose_change_ip_vps');
    Route::get('/orders/get_product', 'OrderController@get_product')->name('admin.orders.get_product');
    Route::get('/orders/edit/{id}', 'OrderController@edit')->name('admin.orders.edit');
    Route::get('/orders/delete', 'OrderController@delete')->name('admin.orders.delete');
    Route::get('/orders/get_order_confirm', 'OrderController@get_order_confirm')->name('admin.orders.get_order_confirm');
    Route::get('/orders/form_order_confirm', 'OrderController@form_order_confirm')->name('admin.orders.form_order_confirm');
    Route::get('/orders/get_addon_product', 'OrderController@get_addon_product')->name('admin.orders.get_addon_product');
    // invoices
    Route::get('/invoices', 'InvoiceController@index')->name('admin.invoices.index');
    Route::get('/invoices/edit/{id}', 'InvoiceController@edit')->name('admin.invoices.edit');
    Route::get('/invoices/filter_type', 'InvoiceController@filter_type')->name('admin.invoices.filter_type');
    Route::get('/invoices/filter_user', 'InvoiceController@filter_user')->name('admin.invoices.filter_user');
    Route::get('/invoices/filter_status', 'InvoiceController@filter_status')->name('admin.invoices.filter_status');
    Route::get('/invoices/detail_invoice', 'InvoiceController@detail_invoice')->name('admin.invoices.detail_invoice');
    Route::get('/invoices/delete_item', 'InvoiceController@delete_item')->name('admin.invoices.delete_item');
    Route::get('/invoices/paid/{id}', 'InvoiceController@paid')->name('admin.invoices.paid');
    Route::get('/invoices/invoices-paid', 'InvoiceController@invoices_paid')->name('admin.invoices.invoices_paid');
    Route::get('/invoices/delete', 'InvoiceController@delete')->name('admin.invoices.delete');
    // VPS
    Route::get('/vps', 'VpsController@index')->name('admin.vps.index');
    Route::get('/vps/action', 'VpsController@action')->name('admin.vps.action');
    Route::get('/vps/edit/action', 'VpsController@edit_action')->name('admin.vps.edit_action');
    Route::get('/vps/detail/{id}', 'VpsController@detail')->name('admin.vps.detail');
    Route::get('/vps/console/{id}', 'VpsController@console')->name('admin.vps.console');
    Route::get('/vps/delete_ajax_vps', 'VpsController@delete_ajax_vps')->name('admin.vps.delete_ajax_vps');
    Route::get('/vps/search', 'VpsController@search')->name('admin.vps.search');
    Route::get('/vps/select_status_vps', 'VpsController@select_status_vps')->name('admin.vps.select_status_vps');
    Route::get('/vps/search_user', 'VpsController@search_user')->name('admin.vps.search_user_form');
    Route::get('/vps/tim-kiem-vps-theo-user', 'VpsController@search_user_form')->name('admin.vps.search_user_form');
    Route::get('/vps/list_vps_of_personal', 'VpsController@list_vps_of_personal')->name('admin.vps.list_vps_of_personal');
    Route::get('/vps/list_vps_of_enterprise', 'VpsController@list_vps_of_enterprise')->name('admin.vps.list_vps_of_enterprise');
    Route::get('/vps/search_multi_vps_vn', 'VpsController@search_multi_vps_vn')->name('admin.vps.search_multi_vps_vn');
    // VPS
    Route::get('/vps_us', 'VpsUSController@index')->name('admin.vps_us.index');
    Route::get('/vps_us/detail/{id}', 'VpsUSController@vps_us_detail')->name('admin.vps_us.vps_us_detail');
    Route::post('/vps_us/update_sevices', 'VpsUSController@update_sevices')->name('admin.vps_us.update_sevices');
    Route::get('/vps_us/select_status_vps', 'VpsUSController@select_status_vps')->name('admin.vps_us.select_status_vps');
    Route::get('/vps_us/search', 'VpsUSController@search')->name('admin.vps_us.search');
    Route::get('/vps_us/list_vps_of_personal', 'VpsUSController@list_vps_of_personal')->name('admin.vps_us.list_vps_of_personal');
    Route::get('/vps_us/list_vps_of_enterprise', 'VpsUSController@list_vps_of_enterprise')->name('admin.vps_us.list_vps_of_enterprise');
    Route::get('/vps_us/delete_ajax_vps', 'VpsUSController@delete_ajax_vps')->name('admin.vps_us.delete_ajax_vps');
    Route::get('/vps_us/action', 'VpsUSController@action')->name('admin.vps_us.action');
    Route::get('/vps_us/search_multi_vps_us', 'VpsUSController@search_multi_vps_us')->name('admin.vps_us.search_multi_vps_us');
    Route::get('/vps_us/change_user', 'VpsUSController@change_user')->name('admin.vps_us.change_user');
    Route::get('/vps_us/export_excel', 'VpsUSController@export_excel')->name('admin.vps_us.export_excel');
    Route::post('/vps_us/form_export_excel', 'VpsUSController@form_export_excel')->name('admin.vps_us.form_export_excel');
    Route::post('/vps_us/form_change_user', 'VpsUSController@form_change_user')->name('admin.vps_us.change_user');
    // Sevices update
    Route::post('/vps/update_sevices', 'VpsController@update_sevices')->name('admin.vps.update_sevices');
    Route::post('/vps/create_sevices', 'VpsController@create_sevices')->name('admin.vps.create_sevices');
    Route::get('/vps/loadBillings', 'VpsController@loadBillings')->name('admin.vps.loadBillings');
    Route::get('/vps/loadIPVPS', 'VpsController@loadIPVPS')->name('admin.vps.loadIPVPS');
    Route::get('/vps/createVPS', 'VpsController@createVPS')->name('admin.vps.createVPS');
    Route::get('/vps/change_user', 'VpsController@change_user')->name('admin.vps.change_user');
    Route::post('/vps/form_change_user', 'VpsController@form_change_user')->name('admin.vps.change_user');
    // Hosting
    Route::get('/hostings', 'HostingController@index')->name('admin.hostings.index');
    Route::get('/hostings/select_status_hosting', 'HostingController@select_status_hosting')->name('admin.hostings.select_status_hosting');
    Route::get('/hostings/get-hosting-daportal/{id}', 'HostingController@getHostingDaPortal')->name('admin.hostings.getHostingDaPortal');
    Route::get('/hostings/get-hosting-singapore/{id}', 'HostingController@getHostingSingapore')->name('admin.hostings.getHostingSingapore');
    Route::get('/hostings/detail/{id}', 'HostingController@detail')->name('admin.hosting.detail');
    Route::get('/hostings/delete_ajax_vps', 'HostingController@delete_ajax_vps')->name('admin.hosting.delete_ajax_vps');
    Route::get('/hostings/search', 'HostingController@search')->name('admin.hosting.search');
    Route::get('/hostings/list_hosting_of_personal', 'HostingController@list_hosting_of_personal')->name('admin.hosting.list_hosting_of_personal');
    Route::get('/hostings/list_hosting_of_enterprise', 'HostingController@list_hosting_of_enterprise')->name('admin.hosting.list_hosting_of_enterprise');
    Route::get('/hostings/search_user', 'HostingController@search_user')->name('admin.hosting.search_user');
    Route::post('/hostings/action_hosting', 'HostingController@action_hosting')->name('admin.hosting.action_hosting');
    Route::post('/hostings/update_sevices', 'HostingController@update_sevices')->name('admin.hosting.update_sevices');
    Route::post('/hostings/setHostingWithPortal', 'HostingController@setHostingWithPortal')->name('admin.hosting.setHostingWithPortal');
    Route::post('/hostings/setHostingSingaporeWithPortal', 'HostingController@setHostingSingaporeWithPortal')->name('admin.hosting.setHostingSingaporeWithPortal');
    Route::post('/hostings/multiple_action_hostings', 'HostingController@multiple_action_hostings')->name('admin.hosting.multiple_action_hostings');
    // Hosting Singapore
    Route::get('/hostings/hosting-singapore', 'HostingController@hosting_singapore')->name('admin.hosting_singapore.index');
    // Server
    Route::get('/servers', 'ServerController@index')->name('admin.server.index');
    Route::post('/servers/update', 'ServerController@update')->name('admin.server.update');
    Route::get('/servers/detail/{id}', 'ServerController@detail')->name('admin.server.detail');
    Route::get('/servers/delete_ajax_server', 'ServerController@delete_ajax_server')->name('admin.server.delete_ajax_vps');
    Route::get('/servers/create', 'ServerController@create')->name('admin.server.create');
    Route::post('/servers/store', 'ServerController@store')->name('admin.server.store');
    Route::post('/servers/sendMail', 'ServerController@sendMail')->name('admin.server.sendMail');
    Route::get('/servers/list_product', 'ServerController@list_product')->name('admin.server.list_product');
    Route::get('/servers/list_addon_server', 'ServerController@list_addon_server')->name('admin.server.list_addon_server');
    Route::get('/servers/loadTotal', 'ServerController@loadTotal')->name('admin.server.loadTotal');
    Route::get('/servers/gui-mail-hoan-thanh-cai-dat/{serverId}', 'ServerController@sendMailFinishConfig')->name('admin.server.sendMailFinishConfig');
    Route::get('/servers/getServer','ServerController@getServer')->name('admin.server.getServer');
    // Proxy
    Route::get('/proxy', 'ServerController@listProxy')->name('admin.proxy.listProxy');
    Route::get('/proxy/getProxy', 'ServerController@getProxy')->name('admin.proxy.getProxy');
    Route::get('/proxy/detail/{id}', 'ServerController@proxy_detail')->name('admin.proxy.detail');
    Route::get('/proxy/action_proxy', 'ServerController@action_proxy')->name('admin.proxy.action');
    Route::post('/proxy/update', 'ServerController@updateProxy')->name('admin.proxy.updateProxy');
    //    Payment
    Route::get('/payments/nap-tien', 'PayInController@index')->name('admin.payments.index');
    Route::post('/payments/select-with-user', 'PayInController@select_user')->name('admin.payments.select_user');
    Route::get('/payments/select-with-user/{user_id}/{method}', 'PayInController@select_with_user')->name('admin.payments.select_with_user');
    Route::get('/payments/invoice', 'PayInController@invoice')->name('admin.payments.invoice');
    Route::get('/payments/invoice_create', 'PayInController@invoice_create')->name('admin.payments.invoice_create');
    Route::get('/payments/invoice_addon', 'PayInController@invoice_addon')->name('admin.payments.invoice_addon');
    Route::get('/payments/invoice_upgrade_hosting', 'PayInController@invoice_upgrade_hosting')->name('admin.payments.invoice_upgrade_hosting');
    Route::get('/payments/invoice_change_id', 'PayInController@invoice_change_id')->name('admin.payments.invoice_change_ip');
    Route::get('/payments/invoice_domain', 'PayInController@invoice_domain')->name('admin.payments.invoice_domain');
    Route::get('/payments/invoice_domain_exp', 'PayInController@invoice_domain_exp')->name('admin.payments.invoice_domain_exp');
    Route::get('/payments/invoice_expired', 'PayInController@invoice_expired')->name('admin.payments.invoice_expired');
    Route::get('/payments/domain_invoice', 'PayInController@domain_invoice')->name('admin.payments.domain_invoice');
    Route::get('/payments/all', 'PayInController@all')->name('admin.payments.all');
    Route::get('/payment/delete', 'PayInController@delete_payment')->name('admin.payments.delete');
    Route::get('/payment/confirm', 'PayInController@confirm_payment')->name('admin.payments.confirm');
    Route::get('/payment/detail/{id}', 'PayInController@detail')->name('admin.payment.detail');
    Route::get('/payments/nap-tien-theo-user', 'PayInController@payment_with_user')->name('admin.payment.payment_with_user');
    Route::get('/payments/danh-sach-nap-tien-theo-user-sl', 'PayInController@list_payment_with_qtt')->name('admin.payment.list_payment_with_qtt');
    Route::get('/payments/list_payment_credit_of_personal', 'PayInController@list_payment_credit_of_personal')->name('admin.payment.list_payment_credit_of_personal');
    Route::get('/payments/list_payment_credit_of_enterprise', 'PayInController@list_payment_credit_of_enterprise')->name('admin.payment.list_payment_credit_of_enterprise');
    Route::get('/payments/list_payment_addon_of_personal', 'PayInController@list_payment_addon_of_personal')->name('admin.payment.list_payment_addon_of_personal');
    Route::get('/payments/list_payment_addon_of_enterprise', 'PayInController@list_payment_addon_of_enterprise')->name('admin.payment.list_payment_addon_of_enterprise');
    Route::get('/payments/list_payment_change_ip_of_personal', 'PayInController@list_payment_change_ip_of_personal')->name('admin.payment.list_payment_change_ip_of_personal');
    Route::get('/payments/list_payment_change_ip_of_enterprise', 'PayInController@list_payment_change_ip_of_enterprise')->name('admin.payment.list_payment_change_ip_of_enterprise');
    Route::get('/payments/list_payment_upgrade_hosting_of_personal', 'PayInController@list_payment_upgrade_hosting_of_personal')->name('admin.payment.list_payment_upgrade_hosting_of_personal');
    Route::get('/payments/list_payment_upgrade_hosting_of_enterprise', 'PayInController@list_payment_upgrade_hosting_of_enterprise')->name('admin.payment.list_payment_upgrade_hosting_of_enterprise');
    Route::get('/payments/list_payment_create_of_personal', 'PayInController@list_payment_create_of_personal')->name('admin.payment.list_payment_create_of_personal');
    Route::get('/payments/list_payment_create_of_enterprise', 'PayInController@list_payment_create_of_enterprise')->name('admin.payment.list_payment_create_of_enterprise');
    Route::get('/payments/list_payment_create_domain_of_personal', 'PayInController@list_payment_create_domain_of_personal')->name('admin.payment.list_payment_create_domain_of_personal');
    Route::get('/payments/list_payment_domain_create_of_enterprise', 'PayInController@list_payment_domain_create_of_enterprise')->name('admin.payment.list_payment_domain_create_of_enterprise');
    Route::get('/payments/list_payment_expire_domain_of_personal', 'PayInController@list_payment_expire_domain_of_personal')->name('admin.payment.list_payment_expire_domain_of_personal');
    Route::get('/payments/list_payment_domain_expire_of_enterprise', 'PayInController@list_payment_domain_expire_of_enterprise')->name('admin.payment.list_payment_domain_expire_of_enterprise');
    Route::get('/payments/list_payment_expired_of_personal', 'PayInController@list_payment_expired_of_personal')->name('admin.payment.list_payment_expired_of_personal');
    Route::get('/payments/list_payment_expired_of_enterprise', 'PayInController@list_payment_expired_of_enterprise')->name('admin.payment.list_payment_expired_of_enterprise');
    Route::get('/payments/list_payment_all_of_personal', 'PayInController@list_payment_all_of_personal')->name('admin.payment.list_payment_all_of_personal');
    Route::get('/payments/list_payment_all_of_enterprise', 'PayInController@list_payment_all_of_enterprise')->name('admin.payment.list_payment_all_of_enterprise');
    Route::get('/payments/nap-tien-theo-loai', 'PayInController@list_payment_with_type')->name('admin.payment.list_payment_with_type');
    Route::get('/payments/nap-tien-theo-trang-thai', 'PayInController@list_payment_with_status')->name('admin.payment.list_payment_with_status');
    Route::get('/payments/danh-sach-tao-moi-theo-sl', 'PayInController@list_payment_create_with_qtt')->name('admin.payment.list_payment_create_with_qtt');
    Route::get('/payments/danh-sach-tao-moi-theo-user', 'PayInController@list_payment_create_with_user')->name('admin.payment.list_payment_create_with_user');
    Route::get('/payments/danh-sach-addon-theo-sl', 'PayInController@list_payment_addon_with_qtt')->name('admin.payment.list_payment_addon_with_qtt');
    Route::get('/payments/list_upgrade_hosting_with_qtt', 'PayInController@list_upgrade_hosting_with_qtt')->name('admin.payment.list_upgrade_hosting_with_qtt');
    Route::get('/payments/list_change_ip_with_qtt', 'PayInController@list_change_ip_with_qtt')->name('admin.payment.list_change_ip_with_qtt');
    Route::get('/payments/list_expired_with_qtt', 'PayInController@list_expired_with_qtt')->name('admin.payment.list_expired_with_qtt');
    Route::get('/payments/list_domain_with_qtt', 'PayInController@list_domain_with_qtt')->name('admin.payment.list_domain_with_qtt');
    Route::get('/payments/list_domain_exp_with_qtt', 'PayInController@list_domain_exp_with_qtt')->name('admin.payment.list_domain_exp_with_qtt');
    Route::get('/payments/list_all_with_qtt', 'PayInController@list_all_with_qtt')->name('admin.payment.list_all_with_qtt');


    // kết nối với direct Admin
    Route::get('/da', 'DirectAdminController@index')->name('da.name');
    Route::get('/directAdmin/showPackage', 'DirectAdminController@showPackage')->name('da.showPackage');
    Route::get('/directAdmin/getHostingDaPortal', 'DirectAdminController@getHostingDaPortal')->name('admin.da.loadHostingDa');
    Route::get('/directAdmin/getHostingDaSingapore', 'DirectAdminController@getHostingDaSingapore')->name('admin.da.getHostingDaSingapore');
    Route::get('/directAdmin/addHostingWithPortal/{username}/{id}', 'DirectAdminController@addHostingWithPortal')->name('admin.da.addHostingWithPortal');
    Route::get('/directAdmin/addHostingSingaporeWithPortal/{username}/{id}', 'DirectAdminController@addHostingSingaporeWithPortal')->name('admin.da.addHostingSingaporeWithPortal');
    /**
     * ticket
     */
    Route::get('/tickets', 'TicketController@getList')->name('admin.ticket.getList');
    Route::post('/tickets/message', 'TicketController@message')->name('admin.ticket.message');
    Route::get('/tickets/change-status/{id}', 'TicketController@updateStatusHandle')->name('admin.ticket.changeStatus');
    Route::get('/tickets/detail/{id}', 'TicketController@detail')->name('admin.ticket.detail');
    Route::post('/tickets/delete', 'TicketController@delete')->name('admin.ticket.delete');
    Route::post('/tickets/deleteAll', 'TicketController@deleteAll')->name('user.ticket.deleteAll');

    // Domains
    Route::get('/domain-products', 'DomainProductController@index')->name('admin.domain-products.index');
    Route::get('/domain-products/create', 'DomainProductController@create')->name('admin.domain-products.create');
    Route::post('/domain-products/store', 'DomainProductController@store')->name('admin.domain-products.store');
    Route::post('/domain-products/update', 'DomainProductController@update')->name('admin.domain-products.update');
    Route::post('/domain-products/delete', 'DomainProductController@delete')->name('admin.domain-products.delete');
    Route::post('/domain-products/multidelete', 'DomainProductController@multidelete')->name('admin.domain-products.multidelete');
    Route::get('/domain-products/edit/{id}', 'DomainProductController@edit')->name('admin.domain-products.edit');
    Route::get('/domain', 'DomainController@index')->name('admin.domain.index');
    Route::get('/lay-thong-tin-domain', 'DomainController@get_info_domain')->name('admin.domain.get_info_domain');
    Route::post('/create_domain_with_PA', 'DomainController@create_domain_with_PA')->name('admin.domain.create_domain_with_PA');
    Route::get('/domain/detail/{id}', 'DomainController@detail_domain')->name('admin.domain.detail');
    Route::post('/domain/detail/update-next-due-date', 'DomainController@update_next_due_date')->name('admin.domain.detail.update_next_due_date');
    Route::post('/domain/update_sevices', 'DomainController@update_sevices')->name('admin.domain.update_sevices');
    Route::get('/domain/delete', 'DomainController@delete')->name('admin.domain.delete');
    Route::get('/domain/info', 'DomainController@info')->name('admin.domain.info');
    Route::post('/domain/multidelete', 'DomainController@multidelete')->name('admin.domain.multidelete');
    Route::post('/domain/change-pass-domain', 'DomainController@change_pass_domain')->name('admin.domain.change_pass_domain');
    Route::get('/domain/check-account-still', 'DomainController@check_account_still')->name('admin.domain.check_account_still');
    Route::get('/domain/create-domain-form', 'DomainController@create_domain_form')->name('admin.domain.create_domain_form');
    Route::get('/domain/auto-fill-domain-form', 'DomainController@auto_fill_domain_form')->name('admin.domain.auto_fill_domain_form');
    Route::post('/domain/create-domain', 'DomainController@create_domain')->name('admin.domain.create_domain');
    Route::get('/domain/get-product-domain', 'DomainController@get_product_domain')->name('admin.domain.get_product_domain');
    Route::get('/domain/get-domain', 'DomainController@get_domain')->name('admin.domain.get_domain');
    Route::post( '/domain/extend_domain_expired', 'DomainController@extend_domain_expired' )->name('admin.domain.extend_domain_expired');

    // Server Hosting
    Route::get('/server_hosting', 'ServerHostingController@index')->name('admin.server_hosting.index');
    Route::get('/server-hosting-singapore', 'ServerHostingController@server_si')->name('admin.server_hosting.server_si');
    Route::get('/server_hosting/them-server-hosting', 'ServerHostingController@create_server_hosting')->name('admin.server_hosting.create_server_hosting');
    Route::get('/server_hosting/chinh-sua-server-hosting/{id}', 'ServerHostingController@edit_server_hosting')->name('admin.server_hosting.edit_server_hosting');
    Route::get('/server_hosting/check_connection', 'ServerHostingController@check_connection')->name('admin.server_hosting.check_connection');
    Route::post('/server_hosting/delete', 'ServerHostingController@delete')->name('admin.server_hosting.delete');
    Route::post('/server_hosting/store', 'ServerHostingController@store')->name('admin.server_hosting.store');
    Route::post('/server_hosting/update', 'ServerHostingController@update')->name('admin.server_hosting.update');
    Route::post('/server_hosting/active', 'ServerHostingController@active')->name('admin.server_hosting.active');
    // event
    Route::get('/event', 'EventController@index')->name('admin.event.index');
    Route::get('/event/list_event', 'EventController@list_event')->name('admin.event.list_event');
    Route::get('/event/detail', 'EventController@detail')->name('admin.event.detail');
    Route::post('/event/actions', 'EventController@actions')->name('admin.event.actions');
    Route::get('/event/delete', 'EventController@delete')->name('admin.event.delete');
    // Hướng dẫn
    Route::get('/huong-dan', 'TutorialController@index')->name('admin.tutorial.index');
    Route::get('/huong-dan/danh-sach-huong-dan', 'TutorialController@list_tutorial')->name('admin.tutorial.list_tutorial');
    Route::post('/huong-dan/hanh-dong', 'TutorialController@actions')->name('admin.tutorial.actions');
    Route::get('/huong-dan/chi-tiet-huong-dan', 'TutorialController@detail')->name('admin.tutorial.detail');
    Route::post('/huong-dan/xoa-huong-dan', 'TutorialController@delete')->name('admin.tutorial.delete');
    // log activity
    Route::get('/activity-log', 'ActivityLogController@index')->name('admin.activity_log.index');
    Route::get('/activity-log/danh-sach-log', 'ActivityLogController@danh_sach_log')->name('admin.activity_log.danh_sach_log');
    Route::get('/activity-log/danh-sach-log-theo-hanh-dong', 'ActivityLogController@danh_sach_log_theo_hanh_dong')->name('admin.activity_log.danh_sach_log_theo_hanh_dong');

    //Xác nhận Chứng minh nhân dân
    Route::get('/users/cmnd-verifies', 'CmndVerifyController@index')->name('admin.users.cmnd_verifies');
    Route::get('/users/cmnd-verifies/detail', 'CmndVerifyController@detail')->name('admin.users.cmnd_verifies.detail');
    Route::get('/users/cmnd-verifies/confirm/', 'CmndVerifyController@confirm')->name('admin.users.cmnd_verifies.confirm');
    Route::get('/users/cmnd-verifies/deactive/', 'CmndVerifyController@deactive')->name('admin.users.cmnd_verifies.deactive');
    Route::post('/users/cmnd-verifies/delete/', 'CmndVerifyController@delete')->name('admin.users.cmnd_verifies.delete');
    Route::post('/users/cmnd-verifies/multidelete/', 'CmndVerifyController@multidelete')->name('admin.users.cmnd_verifies.multidelete');
    // Mã khuyến mãi
    Route::get('/khuyen-mai', 'PromotionController@index')->name('admin.promotion.index');
    Route::get('/khuyen-mai/tao-ma-khuyen-mai', 'PromotionController@create')->name('admin.promotions.create');
    Route::get('/khuyen-mai/chinh-sua-ma-khuyen-mai/{id}', 'PromotionController@edit')->name('admin.promotion.edit');
    Route::get('/khuyen-mai/xoa-khuyen-mai', 'PromotionController@delete')->name('admin.promotion.delete');
    Route::post('/khuyen-mai/tao-ma-khuyen-mai', 'PromotionController@store')->name('admin.promotion.store');
    Route::post('/khuyen-mai/chinh-sua-ma-khuyen-mai', 'PromotionController@update')->name('admin.promotion.update');
    // Colocation
    Route::get('/colocation', 'ColocationController@index')->name('admin.colo.index');
    Route::get('/colocation/detail/{id}', 'ColocationController@detail')->name('admin.colocation.detail');
    Route::get('/colocation/create', 'ColocationController@create')->name('admin.colocation.create');
    Route::get('/colocation/getColocation', 'ColocationController@getColocation')->name('admin.colocation.getColocation');
    Route::get('/colocation/getProductAddonIpColocation', 'ColocationController@getProductAddonIpColocation')->name('admin.colocation.getProductAddonIpColocation');
    Route::get('/colocations/list_product', 'ColocationController@list_product')->name('admin.colocation.list_product');
    Route::post('/colocation/store', 'ColocationController@store')->name('admin.colocation.store');
    Route::post('/colocation/update', 'ColocationController@update')->name('admin.colocation.update');
    Route::post('/colocation/delete', 'ColocationController@delete')->name('admin.colocation.delete');
    // Email Hosting
    Route::get('/email_hosting', 'EmailHostingController@index')->name('admin.email_hosting.index');
    Route::get('/email_hosting/detail/{id}', 'EmailHostingController@detail')->name('admin.email_hosting.detail');
    Route::post('/email_hosting/update', 'EmailHostingController@update')->name('admin.email_hosting.update');
    Route::post('/email_hosting/delete', 'EmailHostingController@delete')->name('admin.email_hosting.delete');

    // Hợp đồng cung cấp dịch vụ
    Route::get('/contracts', 'ContractController@index')->name('admin.contract.index');
    Route::get('/contracts/create-contract-form', 'ContractController@create_form')->name('admin.contract.create_form');
    Route::get('/contracts/add_data_contract_form', 'ContractController@add_data_contract_form')->name('admin.contract.add_data_contract_form');
    Route::get('/contracts/fill_amount_contract_form', 'ContractController@fill_amount_contract_form')->name('admin.contract.fill_amount_contract_form');
    Route::post('/contracts/create-data-contract', 'ContractController@create_data_contract')->name('admin.contract.create_data_contract');
    Route::post('/contracts/delete', 'ContractController@delete')->name('admin.contract.delete');
    Route::post('/contracts/multidelete', 'ContractController@multidelete')->name('admin.contract.multidelete');
    Route::get('/contracts/update-form/{id}', 'ContractController@update_form')->name('admin.contract.update_form');
    Route::post('/contracts/update/{id}', 'ContractController@update')->name('admin.contract.update');
    Route::get('/contracts/print_view/{id}', 'ContractController@print_view')->name('admin.contract.print_view');
    Route::post('/contracts/create_contract_detail', 'ContractController@create_contract_detail')->name('admin.contract.create_contract_detail');
    Route::post('/contracts/delete_contract_detail', 'ContractController@delete_contract_detail')->name('admin.contract.delete_contract_detail');
    Route::get('/contracts/modal_update_contract_detail', 'ContractController@modal_update_contract_detail')->name('admin.contract.modal_update_contract_detail');
    Route::post('/contracts/delete_contract_detail', 'ContractController@delete_contract_detail')->name('admin.contract.delete_contract_detail');
    Route::post('/contracts/update_contract_detail', 'ContractController@update_contract_detail')->name('admin.contract.update_contract_detail');
    Route::get('/contracts/get-target-from-user', 'ContractController@get_target_from_user')->name('admin.contract.get_target_from_user');
    Route::get('/contracts/get-target-from-user-action-update', 'ContractController@get_target_from_user_action_update')->name('admin.contract.get_target_from_user_action_update');
    Route::get('/contracts/get-target-detail', 'ContractController@get_target_detail')->name('admin.contract.get_target_detail');
    Route::get('/contracts/print_pdf/{id}', 'ContractController@print_pdf')->name('admin.contract.print_pdf');
    Route::post('/contracts/update_customer_info', 'ContractController@update_customer_info')->name('admin.contract.update_customer_info');
    Route::get('/contracts/get_target_product_from_user', 'ContractController@get_target_product_from_user')->name('admin.contract.get_target_product_from_user');
    Route::get('/contracts/get-target-detail-product', 'ContractController@get_target_detail_product')->name('admin.contract.get_target_detail_product');
    Route::get('/contracts/create-new-contract-number', 'ContractController@create_new_contract_number')->name('admin.contract.create_new_contract_number');
    Route::get('/contracts/add-product-to-contract', 'ContractController@add_product_to_contract')->name('admin.contract.add_product_to_contract');
    Route::post('/contracts/save-product-to-contract', 'ContractController@save_product_to_contract')->name('admin.contract.save_product_to_contract');
    Route::get('/contracts/user-contracts/{id}', 'ContractController@userContract')->name('admin.user_contract');
    Route::get('/contracts/user-contracts/create/{id}', 'ContractController@userContractCreate')->name('admin.user_contract.create');


    // Quản lý send mail
    Route::get('/send_mail', 'SendMailController@index')->name('admin.send_mail.index');
    Route::get('/send_mail/load_mail', 'SendMailController@load_mail')->name('admin.send_mail.load_mail');
    Route::get('/send_mail/detail_mail', 'SendMailController@detail_mail')->name('admin.send_mail.detail_mail');
    Route::get('/send_mail/loadFilterSendMail', 'SendMailController@loadFilterSendMail')->name('admin.send_mail.loadFilterSendMail');

    // Quản lý tinh nhắn
    Route::get('/messages', 'MessageController@index')->name('admin.messages.index');
    Route::get('/messages/loadMessageByUser', 'MessageController@loadMessageByUser')->name('admin.messages.loadMessageByUser');
    Route::get('/messages/userChat', 'MessageController@userChat')->name('user.messages.userChat');
    Route::post('/messages/sendMessage', 'MessageController@sendMessage')->name('admin.messages.sendMessage');
});

// user Login
Route::get('/login', 'User\LoginController@index')->name('login');
Route::get('/logout', 'User\LoginController@logout')->name('logout');
Route::get('/register', 'User\LoginController@register')->name('register');
// Route::get('/test_register', 'User\LoginController@test_register')->name('test_register');
Route::get('/forgot-password-form', 'User\LoginController@forgotPasswordForm')->name('forgot_password_form');
Route::post('/send-email-forgot', 'User\LoginController@sendEmailForgot')->name('send_email_forgot');
Route::get('/reset-password-forgot-form/{token}', 'User\LoginController@resetPasswordForgotForm')->name('reset_password_forgot_form');
Route::post('/change-password', 'User\LoginController@changePassword')->name('change_password');
Route::get('/sendmail', 'User\LoginController@sendmail')->name('sendmail');
Route::get('/check-verify/{token}', 'User\LoginController@verify')->name('verify');
// đăng nhập mạng xã hội
Route::get('/redirect/{social}', 'User\LoginController@redirect')->name('social.redirect');
Route::get('/callback/{social}', 'User\LoginController@callback')->name('social.callback');


Route::post('/checkLogin', 'User\LoginController@checkLogin')->name('checkLogin');
Route::post('/register', 'User\LoginController@checkRegister')->name('checkRegister');
Route::post('/test_register', 'User\LoginController@testCheckRegister')->name('testCheckRegister');


Route::group(['namespace' => 'User', 'middleware' => 'check.login'], function() {
  // Home
    Route::get('/', [
        'as' => 'index',
        'uses' => 'HomeController@index'
    ]);

    Route::get('/cap-nhat-thong-tin-ca-nhan', 'LoginController@updateProfile')->name('updateProfile');
    Route::post('/cap-nhat-thong-tin-ca-nhan', 'LoginController@form_update_profile')->name('form_update_profile');

    Route::get('/loadVpsPros', 'HomeController@loadVpsPros')->name('user.loadVpsPros');

    Route::get('/users/listCustomer', 'HomeController@listCustomer')->name('user.listCustomer');
    Route::get('/users/load_siderbar_top', 'HomeController@load_siderbar_top')->name('user.load_siderbar_top');
    Route::get('/users/load_siderbar_right', 'HomeController@load_siderbar_right')->name('user.load_siderbar_right');
    Route::get('/users/load_content_center', 'HomeController@load_content_center')->name('user.load_content_center');
    Route::get('/users/check_email', 'HomeController@check_email')->name('user.check_email');

    Route::get('/thong-tin-ca-nhan', 'HomeController@profile')->name('profile');
    Route::post('/chinh-sua-thong-tin-ca-nhan', 'HomeController@update_profile')->name('update_profile');
    Route::get('/thong-tin-ca-nhan/doi-mat-khau', 'HomeController@changePassForm')->name('change_pass_form');
    Route::post('/thong-tin-ca-nhan/cap-nhap-mat-khau', 'HomeController@updatePass')->name('update_pass');
    // ./ket thuc home
    // product
    Route::get( '/products/{id}', 'ProductController@index' )->name('user.product');
    Route::get( '/san-pham/vps/{id}', 'ProductController@product_vps' )->name('user.product.vps');
    Route::get( '/san-pham/vps_us/{id}', 'ProductController@product_vps_us' )->name('user.product.vps_us');
    Route::get( '/san-pham/hosting/{id}', 'ProductController@product_hosting' )->name('user.product.hosting');
    Route::get( '/check-event-product', 'ProductController@check_event' )->name('user.product.check_event');
    Route::get( '/san-pham/server/{id}', 'ProductController@product_server' )->name('user.product.server');
    Route::get( '/san-pham/colocation/{id}', 'ProductController@product_colocation' )->name('user.product.colocation');
    Route::get( '/san-pham/proxy/{id}', 'ProductController@product_proxy' )->name('user.product.proxy');
    // card
    Route::get('/add-card/{id}', 'CardController@addCard')->name('user.addCard');
    Route::get('/add-card-server/{id}', 'CardController@addCardServer')->name('user.addCardServer');
    Route::get('/add-card-colocation/{id}', 'CardController@addCardColocation')->name('user.addCardColocation');
    Route::get('/loadAddon', 'CardController@loadAddon')->name('user.loadAddon');
    Route::get('/order/loadAmountColo', 'CardController@loadAmountColo')->name('user.loadAmountColo');
    Route::get('/add-card/add-on/{id}', 'CardController@addCardAddon')->name('user.addCardAddon');
    Route::post('/add-card/orderAddon', 'CardController@orderAddon')->name('users.orders.createOrderAddon');
    Route::get('/order/check_billing', 'CardController@check_billing')->name('user.check_billing');
    Route::get('/order/order_server', 'CardController@order_server')->name('user.order_server');
    Route::get('/order/loadState', 'CardController@loadState')->name('user.loadState');
    Route::get('/check-out', 'CardController@check_out')->name('order.check_out');
    Route::get('/order/verify/{token}', 'CardController@order_vetify')->name('order.vetify');
    Route::get('/order/list-invoices/', 'CardController@list_invoices')->name('order.list_invoices');
    Route::get('/order/hoa-don/cho-thanh-toan', 'CardController@list_invoices_progressing')->name('order.list_invoices.progressing');
    Route::get('/order/list_invoices_with_sl/', 'CardController@list_invoices_with_sl')->name('order.list_invoices_with_sl');
    Route::get('/order/list_invoices_paid_with_sl/', 'CardController@list_invoices_paid_with_sl')->name('order.list_invoices_paid_with_sl');
    Route::get('/order/list_invoices_unpaid_with_sl/', 'CardController@list_invoices_unpaid_with_sl')->name('order.list_invoices_unpaid_with_sl');
    Route::get('/order/check-invoices/{id}', 'CardController@check_invoices')->name('order.check_invoices');
    Route::get('/order/payment-invoices/{id}', 'CardController@payment_invoices')->name('order.payment_invoices');
    Route::get('/order/check-invoices/print/{id}', 'CardController@print')->name('invoice.prince');
    Route::get('/order/check-invoices/pdf/{id}', 'CardController@pdf')->name('invoice.pdf');
    Route::post('/order', 'CardController@order')->name('user.order');
    Route::post('/form_order_server', 'CardController@form_order_server')->name('user.form_order_server');
    Route::post('/order/payment', 'CardController@payment')->name('invoice.payment');
    Route::post('/order/payment_cloudzone_point', 'CardController@payment_cloudzone_point')->name('invoice.payment_cloudzone_point');
    Route::post('/order/payment_upgrade_cloudzone_point', 'CardController@payment_upgrade_cloudzone_point')->name('invoice.payment_upgrade_cloudzone_point');
    Route::post('/order/payment_expired_form', 'CardController@payment_expired_form')->name('invoice.payment_expired_form');
    Route::post('/order/payment_expired_cloudzone_point', 'CardController@payment_expired_cloudzone_point')->name('invoice.payment_expired_cloudzone_point');
    Route::get('/order/payment_order/{invoice_id}', 'CardController@payment_order')->name('invoice.payment_invoice');
    Route::get('/order/payment_expired/{invoice_id}', 'CardController@payment_expired')->name('invoice.payment_expired');
    Route::post('/order/payment_upgrade', 'CardController@payment_upgrade')->name('invoice.payment_upgrade');
    Route::get('/order/add_credit_invoice/{invoice_id}', 'CardController@add_credit_invoice')->name('invoice.add_credit_with_invoice');

    Route::post('/order/payment_expired_off', 'CardController@payment_expired_off')->name('invoice.payment_expired_off');
    Route::post('/order/payment_off', 'CardController@payment_off')->name('invoice.payment_off');
    Route::post('/order/payment_upgrade_off', 'CardController@payment_upgrade_off')->name('invoice.payment_upgrade_off');
    Route::post('/order/payment_addon_off', 'CardController@payment_addon_off')->name('invoice.payment_addon_off');
    Route::get('/order/checkout_payment_off/{id}', 'CardController@checkout_payment_off')->name('invoice.checkout_payment_off');
    Route::get('/order/checkout_payment_expired_off/{id}', 'CardController@checkout_payment_expired_off')->name('invoice.checkout_payment_expired_off');
    Route::get('/order/checkout_payment_addon_off/{id}', 'CardController@checkout_payment_addon_off')->name('invoice.checkout_payment_addon_off');
    // nâng cấp cấu hình vps
    Route::get('/order/payment_addon/{invoice_id}', 'CardController@payment_addon')->name('invoice.payment_addon');
    Route::post('/order/payment_addon_form', 'CardController@payment_addon_form')->name('invoice.payment_addon_form');
    Route::post('/order/payment_addon_cloudzone_point', 'CardController@payment_addon_cloudzone_point')->name('invoice.payment_addon_cloudzone_point');
    // Thay đổi IP
    Route::get('/order/payment_change_ip/{invoice_id}', 'CardController@payment_change_ip')->name('invoice.payment_change_ip');
    Route::post('/order/payment_change_ip_form', 'CardController@payment_change_ip_form')->name('invoice.payment_change_ip_form');
    Route::post('/order/payment_change_ip_off', 'CardController@payment_change_ip_off')->name('invoice.payment_change_ip_off');
    Route::get('/pay-in-online/payment_change_ip_off', 'PayInController@payment_change_ip_off')->name('user.payment_change_ip_off');
    Route::get('/pay-in-online/payment_change_ip_off_cloudzone_point', 'PayInController@payment_change_ip_off_cloudzone_point')->name('user.payment_change_ip_off_cloudzone_point');
    Route::get('/order/checkout_payment_change_ip_off/{id}', 'CardController@checkout_payment_change_ip_off')->name('invoice.checkout_payment_change_ip_off');
    Route::post('/order/payment_change_ip_cloudzone_point', 'CardController@payment_change_ip_cloudzone_point')->name('invoice.payment_change_ip_cloudzone_point');

    Route::get('/order/list-invoices/paid', 'CardController@list_invoices_paid')->name('order.list_invoices.paid');
    Route::get('/order/list-invoices/unpaid', 'CardController@list_invoices_unpaid')->name('order.list_invoices.unpaid');
    // service
    Route::get('/service', 'ServiceController@index')->name('service.index');
    Route::get('/service/on', 'ServiceController@on')->name('service.on');
    // VPS
 
    Route::get('/dich-vu/vps/dang-su-dung', 'VpsController@vps_on')->name('service.vps.on');
 
    //  Route::get('/dich-vu/vps/dang-su-dung', 'ServiceController@vps_on')->name('service.vps.on');
    Route::get('/dich-vu/vps/con-han', 'VpsController@vps_use')->name('service.vps.use');
   // Route::get('/dich-vu/vps/con-han', 'ServiceController@vps_use')->name('service.vps.use');
   Route::get('/dich-vu/vps/da-het-han', 'VpsController@vps_nearly')->name('service.vps.nearly');
   // Route::get('/dich-vu/vps/da-het-han', 'ServiceController@vps_nearly')->name('service.vps.nearly');
   Route::get('/dich-vu/vps/da-huy', 'VpsController@vps_cancel')->name('service.vps.cancel');
  //  Route::get('/dich-vu/vps/da-huy', 'ServiceController@vps_cancel')->name('service.vps.cancel');
  Route::get('/dich-vu/vps/tat-ca-dich-vu', 'VpsController@vps_all')->name('service.vps.all'); 
  // Route::get('/dich-vu/vps/tat-ca-dich-vu', 'ServiceController@vps_all')->name('service.vps.all');
  Route::get('/dich-vu/vps/gan-het-han-va-qua-han', 'VpsController@vps_nearly_and_expire')
  ->name('service.vps.vps_nearly_and_expire');
 // Route::get('/dich-vu/vps/gan-het-han-va-qua-han', 'ServiceController@vps_nearly_and_expire')
 // ->name('service.vps.vps_nearly_and_expire');
 Route::get('/dich-vu/vps/list_vps_nearly_and_expire', 'VpsController@list_vps_nearly_and_expire')->name('service.vps.list_vps_nearly_and_expire');
  //  Route::get('/dich-vu/vps/list_vps_nearly_and_expire', 'ServiceController@list_vps_nearly_and_expire')->name('service.vps.list_vps_nearly_and_expire');

  Route::get('/dich-vu/vps_us/dang-su-dung', 'VpsController@vps_us_on')->name('service.vps_us.on');
  //Route::get('/dich-vu/vps_us/dang-su-dung', 'ServiceController@vps_us_on')->name('service.vps_us.on');
  Route::get('/dich-vu/vps_us/con-han', 'VpsController@vps_us_use')->name('service.vps_us.use');
  //  Route::get('/dich-vu/vps_us/con-han', 'ServiceController@vps_us_use')->name('service.vps_us.use');
  Route::get('/dich-vu/vps_us/da-het-han', 'VpsController@vps_us_nearly')->name('service.vps_us.nearly');
 // Route::get('/dich-vu/vps_us/da-het-han', 'ServiceController@vps_us_nearly')->name('service.vps_us.nearly');
 Route::get('/dich-vu/vps_us/da-huy', 'VpsController@vps_us_cancel')->name('service.vps_us.cancel');
 //   Route::get('/dich-vu/vps_us/da-huy', 'ServiceController@vps_us_cancel')->name('service.vps_us.cancel');
 Route::get('/dich-vu/vps_us/tat-ca-dich-vu', 'VpsController@vps_us_all')->name('service.vps_us.all');
 //   Route::get('/dich-vu/vps_us/tat-ca-dich-vu', 'ServiceController@vps_us_all')->name('service.vps_us.all');
 Route::get('/dich-vu/vps_us/gan-het-han-va-qua-han', 'VpsController@vps_us_nearly_and_expire')->name('service.vps_us.vps_us_nearly_and_expire'); 
 //  Route::get('/dich-vu/vps_us/gan-het-han-va-qua-han', 'ServiceController@vps_us_nearly_and_expire')->name('service.vps_us.vps_us_nearly_and_expire');
 Route::get('/dich-vu/vps/list_vps_us_nearly_and_expire', 'VpsController@list_vps_us_nearly_and_expire')->name('service.vps.list_vps_us_nearly_and_expire');
// Route::get('/dich-vu/vps/list_vps_us_nearly_and_expire', 'ServiceController@list_vps_us_nearly_and_expire')->name('service.vps.list_vps_us_nearly_and_expire');
    // chuyển khách hàng - VPS
    Route::get('/dich-vu/vps/chuyen-doi-khach-hang', 'ServiceController@change_user_by_vps')->name('service.vps.change_user_by_vps');
    Route::get('/dich-vu/vps_us/chuyen-doi-khach-hang', 'ServiceController@change_user_by_vps_us')->name('service.vps.change_user_by_vps_us');
    Route::post('/dich-vu/vps/chuyen-doi-khach-hang', 'ServiceController@form_change_user_by_vps')->name('service.vps.form_change_user_by_vps');
    Route::post('/dich-vu/vps_us/chuyen-doi-khach-hang', 'ServiceController@form_change_user_by_vps_us')->name('service.vps_us.form_change_user_by_vps_us');
    // Xuất file excel - VPS
    Route::get('/dich-vu/vps/xuat-file-excel', 'ServiceController@get_export_excel_by_vps')->name('service.vps.get_export_excel_by_vps');
    Route::get('/dich-vu/vps_us/xuat-file-excel', 'ServiceController@get_export_excel_by_vps_us')->name('service.vps.get_export_excel_by_vps_us');
    Route::post('/dich-vu/vps/form_export_excel_by_vps', 'ServiceController@form_export_excel_by_vps')->name('service.vps.form_export_excel_by_vps');
    Route::post('/dich-vu/vps/form_export_excel_by_vps_us', 'ServiceController@form_export_excel_by_vps_us')->name('service.vps.form_export_excel_by_vps_us');
    // Hosting
    Route::get('/dich-vu/hosting/dang-su-dung', 'ServiceController@hosting_use')->name('service.hosting.use');
    Route::get('/dich-vu/hosting/con-han', 'ServiceController@hosting_on')->name('service.hosting.on');
    Route::get('/dich-vu/hosting/het-han-huy', 'ServiceController@hosting_nearly')->name('service.hosting.nearly');
    Route::get('/dich-vu/hosting/huy-xoa', 'ServiceController@hosting_cancel')->name('service.hosting.cancel');
    Route::get('/dich-vu/hosting/gan-het-han-va-qua-han', 'ServiceController@hosting_nearly_and_expire')->name('service.hosting.hosting_nearly_and_expire');
    Route::get('/dich-vu/hosting/list_hosting_nearly_and_expire', 'ServiceController@list_hosting_nearly_and_expire')->name('service.hosting.list_hosting_nearly_and_expire');
    Route::get('/dich-vu/hosting/tat-ca-dich-vu', 'ServiceController@hosting_all')->name('service.hosting.all');
    Route::get('/services/list_hosting_nearly_search', 'ServiceController@list_hosting_nearly_search')->name('service.list_hosting_nearly_search');
    Route::get('/services/list_hosting_nearly_with_sl', 'ServiceController@list_hosting_nearly_with_sl')->name('service.list_hosting_nearly_with_sl');
    Route::get('/services/list_hosting_with_sl', 'ServiceController@list_hosting_with_sl')->name('service.list_hosting_with_sl');
    Route::get('/services/list_all_hosting_with_sl', 'ServiceController@list_all_hosting_with_sl')->name('service.list_all_hosting_with_sl');


    Route::get('/dich-vu/domain/dang-su-dung', 'ServiceController@domain_on')->name('service.domain.on');
    Route::get('/service/pending', 'ServiceController@pending')->name('service.peding');
    Route::get('/service/request_expired_vps', 'VpsController@request_expired_vps')->name('service.request_expired_vps');
    Route::get('/service/request_expired_server', 'VpsController@request_expired_server')->name('service.request_expired_server');
    Route::get('/service/request_expired_hosting', 'VpsController@request_expired_hosting')->name('service.request_expired_hosting');
    Route::get('/service/request_expired_list_server', 'VpsController@request_expired_list_server')->name('service.request_expired_list_server');
    Route::get('/service/detail/{id}', 'ServiceController@detail')->name('services.detail');
    Route::get('/service/server/detail/{id}', 'ServiceController@server_detail')->name('service.server_detail');
    Route::get('/service/terminated/{id}', 'ServiceController@terminated')->name('service.terminated');
    Route::get('/service/extend/{id}', 'ServiceController@extend')->name('service.extend');
    // VPS
    Route::get('/service/loadStatusVPS', 'VpsController@loadStatusVPS')->name('service.loadStatusVPS');
    // fix
    Route::get('/services/list_vps_with_sl', 'VpsController@list_vps_with_sl')->name('service.list_vps_with_sl');
    Route::get('/services/list_vps_use_with_sl', 'VpsController@list_vps_use_with_sl')->name('service.list_vps_use_with_sl');
    Route::get('/services/list_vps_us_with_sl', 'ServiceController@list_vps_us_with_sl')->name('service.list_vps_us_with_sl');
    Route::get('/services/list_vps_us_use_with_sl', 'ServiceController@list_vps_us_use_with_sl')->name('service.list_vps_us_use_with_sl');
    Route::get('/services/list_all_vps_with_sl', 'VpsController@list_all_vps_with_sl')->name('service.list_all_vps_with_sl');
    Route::get('/services/list_all_vps_us_with_sl', 'ServiceController@list_all_vps_us_with_sl')->name('service.list_all_vps_us_with_sl');
    Route::get('/services/list_vps_nearly_with_sl', 'VpsController@list_vps_nearly_with_sl')->name('service.list_vps_nearly_with_sl');
    Route::get('/services/list_vps_us_nearly_with_sl', 'ServiceController@list_vps_us_nearly_with_sl')->name('service.list_vps_us_nearly_with_sl');
   //-
    Route::get('/services/search_vps', 'VpsController@search_vps')->name('service.search_vps');
   
    Route::get('/services/search_vps_use', 'VpsController@search_vps_use')->name('service.search_vps_use');
    Route::get('/services/search_vps_us', 'ServiceController@search_vps_us')->name('service.search_vps_us');
    Route::get('/services/search_vps_us_use', 'ServiceController@search_vps_us_use')->name('service.search_vps_us_use');
    Route::get('/services/hosting_search', 'ServiceController@hosting_search')->name('service.hosting_search');
    Route::get('/services/all_hosting_search', 'ServiceController@all_hosting_search')->name('service.all_hosting_search');
    Route::get('/services/all_vps_search', 'VpsController@all_vps_search')->name('service.all_vps_search');
    Route::get('/services/all_vps_us_search', 'ServiceController@all_vps_us_search')->name('service.all_vps_us_search');
    Route::get('/services/search_vps_nearly', 'VpsController@search_vps_nearly')->name('service.search_vps_nearly');
    Route::get('/services/search_vps_cancel', 'VpsController@search_vps_cancel')->name('service.search_vps_cancel');
    Route::get('/services/search_vps_us_nearly', 'ServiceController@search_vps_us_nearly')->name('service.search_vps_us_nearly');
    Route::get('/services/search_vps_us_cancel', 'ServiceController@search_vps_us_cancel')->name('service.search_vps_us_cancel');
    Route::get('/service/login_directadmin/{id}', 'ServiceController@login_directadmin')->name('services.login_directadmin');
    Route::get('/services/vps/console/{id}', 'ServiceController@vps_console')->name('services.vps.console');
    Route::post('/service/terminated', 'ServiceController@terminated')->name('service.terminated');
    Route::post('/services/server/action-server', 'VpsController@action_server')->name('service.action-server');
    Route::post('/service/cancel_sevices', 'ServiceController@cancel_sevices')->name('service.cancel_sevices');
    Route::post('/services/action_services', 'VpsController@action_services')->name('service.action_services');
    Route::post('/services/action_server_services', 'VpsController@action_server_services')->name('service.action_server_services');
    Route::post('/services/action', 'VpsController@action_one_service')->name('service.action_one_service');
    Route::post('/services/upgrade_vps', 'ServiceController@upgrade_vps')->name('service.upgrade_vps');
    Route::post('/services/updateCusomter', 'VpsController@updateCusomter')->name('service.updateCusomter');
    Route::post('/services/updateDescription', 'VpsController@updateDescription')->name('service.updateDescription');
    Route::post('/services/rebuild_vps', 'VpsController@rebuild_vps')->name('service.rebuild_vps');
    Route::post('/services/rebuild_vps_us', 'ServiceController@rebuild_vps_us')->name('service.rebuild_vps_us');
    Route::get('/services/loadCusomter', 'VpsController@loadCusomter')->name('service.loadCusomter');
    Route::get('/services/servive_nearly', 'ServiceController@servive_nearly')->name('service.servive_nearly');
    Route::get('/services/check-upgrade/{id}', 'ServiceController@check_upgrade_hosting')->name('service.check_upgrade_hosting');
    Route::post('/services/upgrade_hosting', 'ServiceController@upgrade_hosting')->name('service.upgrade_hosting');
    Route::get('/order/payment_upgrade_hosting/{invoice_id}', 'CardController@payment_upgrade_hosting')->name('invoice.payment_upgrade_hosting');
    // search multi VPS
    Route::get('/services/search_multi_vps_on', 'ServiceController@search_multi_vps_on')->name('service.search_multi_vps_on');
    Route::get('/services/search_multi_vps_use', 'VpsController@search_multi_vps_use')->name('service.search_multi_vps_use');
    Route::get('/services/search_multi_vps_nearly', 'ServiceController@search_multi_vps_nearly')->name('service.search_multi_vps_nearly');
    Route::get('/services/search_multi_vps_cancel', 'VpsController@search_multi_vps_cancel')->name('service.search_multi_vps_cancel');
    Route::get('/services/search_multi_vps_all', 'VpsController@search_multi_vps_all')->name('service.search_multi_vps_all');
    Route::get('/services/search_multi_vps_us_on', 'ServiceController@search_multi_vps_us_on')->name('service.search_multi_vps_us_on');
    Route::get('/services/search_multi_vps_us_use', 'ServiceController@search_multi_vps_us_use')->name('service.search_multi_vps_us_use');
    Route::get('/services/search_multi_vps_us_nearly', 'ServiceController@search_multi_vps_us_nearly')->name('service.search_multi_vps_us_nearly');
    Route::get('/services/search_multi_vps_us_cancel', 'ServiceController@search_multi_vps_us_cancel')->name('service.search_multi_vps_us_cancel');
    Route::get('/services/search_multi_vps_us_all', 'ServiceController@search_multi_vps_us_all')->name('service.search_multi_vps_us_all');
    // User server
    Route::get('/dich-vu/server/dang-su-dung', 'ServiceController@server_use')->name('service.server.use');
    Route::get('/dich-vu/server/con-han', 'ServiceController@server_on')->name('service.server.on');
    Route::get('/dich-vu/server/da-het-han', 'ServiceController@server_nearly')->name('service.server.nearly');
    Route::get('/dich-vu/server/da-xoa-huy-chuyen', 'ServiceController@server_cancel')->name('service.server.cancel');
    Route::get('/dich-vu/server/gan-het-han-huy-hoac-qua-han', 'ServiceController@server_nearly_and_expire')->name('service.server.server_nearly_and_expire');
    Route::get('/dich-vu/server/list_server_nearly_and_expire', 'ServiceController@list_server_nearly_and_expire')->name('service.server.list_server_nearly_and_expire');
    Route::get('/dich-vu/server/tat-ca-dich-vu', 'ServiceController@server_all')->name('service.server.all');
    Route::get('/services/server/select_server', 'VpsController@select_server')->name('service.select_server');
    Route::get('/services/server/search_server', 'ServiceController@search_server')->name('service.search_server');
    // Proxy
    Route::get('/dich-vu/proxy/dang-su-dung', 'ServiceController@proxy_use')->name('service.proxy.use');
    Route::get('/dich-vu/proxy/con-han', 'ServiceController@proxy_on')->name('service.proxy.on');
    Route::get('/dich-vu/proxy/het-han', 'ServiceController@proxy_expire')->name('service.proxy.expire');
    Route::get('/dich-vu/proxy/huy-xoa-chuyen', 'ServiceController@proxy_cancel')->name('service.proxy.cancel');
    Route::get('/dich-vu/proxy/tat-ca', 'ServiceController@proxy_all')->name('service.proxy.all');
    Route::get('/services/proxy/list_proxy_use_with_sort', 'ServiceController@list_proxy_use_with_sort')->name('service.proxy.list_proxy_use_with_sort');
    Route::get('/services/proxy/list_proxy_on_with_sort', 'ServiceController@list_proxy_on_with_sort')->name('service.proxy.list_proxy_on_with_sort');
    Route::get('/services/proxy/list_proxy_cancel_with_sort', 'ServiceController@list_proxy_cancel_with_sort')->name('service.proxy.list_proxy_cancel_with_sort');
    Route::get('/services/proxy/list_proxy_all_with_sort', 'ServiceController@list_proxy_all_with_sort')->name('service.proxy.list_proxy_all_with_sort');
    Route::get('/services/proxy/list_proxy_expire_with_sort', 'ServiceController@list_proxy_expire_with_sort')->name('service.proxy.list_proxy_expire_with_sort');
    Route::get('/services/proxy/request_expired_proxy', 'ServiceController@request_expired_proxy')->name('service.proxy.request_expired_proxy');
    Route::get('/services/proxy/request_expired_list_proxy', 'ServiceController@request_expired_list_proxy')->name('service.proxy.request_expired_list_proxy');
    Route::post('/services/proxy/updateDescriptionProxy', 'ServiceController@updateDescriptionProxy')->name('service.proxy.updateDescriptionProxy');
    // User Colocation
    Route::get('/dich-vu/colocation/dang-su-dung', 'ServiceController@colocation_use')->name('service.colocation.use');
    Route::get('/dich-vu/colocation/con-han', 'ServiceController@colocation_on')->name('service.colocation.on');
    Route::get('/dich-vu/colocation/huy-xoa-chuyen', 'ServiceController@colocation_cancel')->name('service.colocation.cancel');
    Route::get('/service/request_expired_colocation', 'ServiceController@request_expired_colocation')->name('service.request_expired_colocation');
    Route::post('/services/colocation/action-server', 'ServiceController@action_colocation')->name('service.action_colocation');
    Route::post('/services/action_colocation_services', 'ServiceController@action_colocation_services')->name('service.action_colocation_services');
    Route::get('/service/request_expired_list_colocation', 'ServiceController@request_expired_list_colocation')->name('service.request_expired_list_colocation');
    Route::get('/service/colocation/detail/{id}', 'ServiceController@colocation_detail')->name('service.colocation_detail');
    Route::get('/dich-vu/colocation/da-het-han', 'ServiceController@colocation_nearly')->name('service.colocation.nearly');
    Route::get('/dich-vu/colocation/tat-ca-dich-vu', 'ServiceController@colocation_all')->name('service.colocation.all');
    Route::get('/services/colocation/select_colocation', 'ServiceController@select_colocation')->name('service.select_colocation');
    Route::get('/services/colocation/search_colocation', 'ServiceController@search_colocation')->name('service.search_colocation');
    Route::get('/dich-vu/colocation/gan-het-han-va-qua-han', 'ServiceController@colocation_nearly_and_expire')->name('service.colocation_nearly_and_expire');
    Route::get('/services/colocation/list_colocation_nearly_and_expire', 'ServiceController@list_colocation_nearly_and_expire')->name('service.list_colocation_nearly_and_expire');
    // User Email Hosting
    Route::get('/service/email_hosting/detail/{id}', 'ServiceController@email_hosting_detail')->name('service.email_hosting_detail');
    Route::get('/dich-vu/email_hosting/het-han-huy', 'ServiceController@email_hosting_nearly')->name('service.email_hosting.nearly');
    Route::get('/dich-vu/email_hosting/tat-ca-dich-vu', 'ServiceController@email_hosting_all')->name('service.email_hosting.all');
    Route::get('/dich-vu/email_hosting/dang-su-dung', 'ServiceController@email_hosting_on')->name('service.email_hosting.on');
    Route::get('/service/request_expired_email_hosting', 'ServiceController@request_expired_email_hosting')->name('service.request_expired_email_hosting');
    Route::post('/services/email_hosting/action_email_hosting', 'ServiceController@action_email_hosting')->name('service.action_email_hosting');
    Route::post('/services/email_hosting/action_email_hosting_services', 'ServiceController@action_email_hosting_services')->name('service.action_email_hosting_services');
    Route::get('/services/email_hosting/request_expired_list_email_hosting', 'VpsController@request_expired_list_email_hosting')->name('service.request_expired_list_email_hosting');
    Route::get('/services/email_hosting/select_email_hosting', 'VpsController@select_email_hosting')->name('service.select_email_hosting');
    Route::get('/services/email_hosting/search_email_hosting', 'VpsController@search_email_hosting')->name('service.search_email_hosting');
    Route::get('/dich-vu/email_hosting/gan-het-han-va-qua-han', 'ServiceController@email_hosting_nearly_and_expired')->name('service.email_hosting_nearly_and_expired');
    Route::get('/dich-vu/email_hosting/list_email_hosting_nearly_and_expired', 'ServiceController@list_email_hosting_nearly_and_expired')->name('service.list_email_hosting_nearly_and_expired');

    Route::get('service/check-os', 'ServiceController@check_os')->name('service.check_os');
    // nap tiền online
    Route::get('/pay-in-online', 'PayInController@index')->name('user.pay_in_online');
    Route::post('/pay-in-online', 'PayInController@request')->name('user.pay_in_online.request');
    Route::get('/pay-in-online/lich-su', 'PayInController@history_payment')->name('user.history_payment');
    Route::get('/nap-tien-tai-khoan/kiem-tra/{id}', 'PayInController@pay_checkout')->name('user.pay_in_online.pay_checkout');
    Route::get('/pay-in-online/check-invoices', 'PayInController@check_invoice')->name('user.check_invoice');
    Route::get('/pay-in-online/payment_order', 'PayInController@payment_order')->name('user.payment_order');
    Route::get('/pay-in-online/payment_order_upgrade', 'PayInController@payment_order_upgrade')->name('user.payment_order_upgrade');
    Route::get('/pay-in-online/payment_order_cloudzone_point', 'PayInController@payment_order_cloudzone_point')->name('user.payment_order_cloudzone_point');
    Route::get('/pay-in-online/payment_order_upgrade_cloudzone_point', 'PayInController@payment_order_upgrade_cloudzone_point')->name('user.payment_order_upgrade_cloudzone_point');
    Route::get('/pay-in-online/payment_expired', 'PayInController@payment_expired')->name('user.payment_expired');
    Route::get('/pay-in-online/payment_expired_cloudzone_point', 'PayInController@payment_expired_cloudzone_point')->name('user.payment_expired_cloudzone_point');
    Route::get('/pay-in-online/thanh-toan-momo', 'PayInController@payment_momo')->name('user.payment_momo');

    Route::get('/pay-in-online/payment_addon', 'PayInController@payment_addon')->name('user.payment_addon');
    Route::get('/pay-in-online/payment_addon_cloudzone_point', 'PayInController@payment_addon_cloudzone_point')->name('user.payment_addon_cloudzone_point');
    //    Thanh toán momo
    Route::get('/momo/return', 'MomoController@momoReturn')->name('user.momo.return');
    Route::get('/momo/notify', 'MomoController@momoNotify')->name('user.momo.notify');
    // quản lý khách hàng
    Route::get('/khach-hang/tao-khach-hang', 'CustomerController@create')->name('user.customer.create');
    Route::get('/khach-hang/sua-ho-so-khach-hang/{id}', 'CustomerController@editCustomer')->name('user.customer.edit');
    Route::get('/khach-hang', 'CustomerController@listCustomer')->name('user.customer.index');
    Route::get('/khach-hang/xoa-khach-hang', 'CustomerController@deleteCustomer')->name('user.customer.delete');
    Route::get('/khach-hang/danh-sach-dich-vu/{makh}', 'CustomerController@list_order_with_customer')->name('user.customer.list_order_with_customer');
    Route::get('/khach-hang/thong-tin-khach-hang/{id}', 'CustomerController@detailCustomer')->name('user.customer.detail');
    Route::post('/khach-hang/tao-khach-hang', 'CustomerController@storeCustomer')->name('user.customer.store');
    Route::post('/khach-hang/sua-ho-so-khach-hang', 'CustomerController@updateCustomer')->name('user.customer.update');

    // Quản lý order
    Route::get('/order/danh-sach-don-dat-hang', 'OrderController@listOrder')->name('user.order.list');
    Route::get('/order/huy-don-dat-hang', 'OrderController@cancelOrder')->name('user.order.cancel');
    Route::get('/orders/detail_invoice', 'OrderController@detail_invoice')->name('admin.orders.detail_invoice');
    // Auth::routes();

    Route::get('/tickets', 'TicketController@getList')->name('user.ticket.getList');
    Route::get('/tickets/create', 'TicketController@create')->name('user.ticket.create');
    Route::post('/tickets/create', 'TicketController@createHandle')->name('user.ticket.createHandle');
    Route::get('/tickets/update/{id}', 'TicketController@update')->name('user.ticket.update');
    Route::post('/tickets/update/{id}', 'TicketController@updateHandle')->name('user.ticket.updateHandle');
    Route::post('/tickets/message', 'TicketController@message')->name('user.ticket.message');
    Route::post('/tickets/delete/{id}', 'TicketController@deleteHandle')->name('user.ticket.deleteHandle');
    Route::get('/tickets/detail/{id}', 'TicketController@detail')->name('user.ticket.detail');

    // Xem notification
    Route::get('/notifications', 'NotificationController@index')->name('user.notification.index');
    Route::get('/notifications/detail/{id}', 'NotificationController@detail')->name('user.notification.detail');
    Route::get('/notifications/read', 'NotificationController@read_notification')->name('user.notification.read_notification');
    Route::get('/notifications/ajax_read', 'NotificationController@ajax_read')->name('user.notification.ajax_read');

    // Add card and order domain
    Route::get('/add-card-domain', 'CardController@addCardDomain')->name('user.addCardDomain');
    Route::get('/order/check_billing_domain', 'CardController@check_billing_domain')->name('user.check_billing_domain');
    Route::get('/order/kiem-tra-ma-khuyen-mai', 'CardController@check_promotion')->name('user.check_promotion');
    Route::post('/order-domain', 'CardController@order_domain')->name('user.order_domain');
    Route::get('/order/kiem-tra-ma-khuyen-mai', 'CardController@check_promotion')->name('user.check_promotion');
    Route::post('/order/them_thong_tin_khach_hang', 'CardController@add_customer')->name('user.order_add_customer');
    Route::post('/order-domain-promotion', 'CardController@order_domain_promotion')->name('user.order_domain_promotion');
    Route::get('/order/payment_domain_order/{invoice_id}', 'CardController@payment_domain_order')->name('invoice.payment_domain_order');
    Route::get('/pay-in-online/payment_domain_order', 'PayInController@payment_domain_order')->name('user.payment_domain_order');
    Route::get('/add-card-domain/auto_fill_oder_domain_form', 'DomainController@auto_fill_oder_domain_form')->name('user.domain.auto_fill_oder_domain_form');

    //Domain User
    Route::get('/service/domain', 'DomainController@index')->name('user.domain.index');
    Route::get( '/search-domain', 'DomainController@search_domain' )->name('user.domain.search');
    Route::get( '/domain-promotion/{id}', 'DomainController@domain_promotion' )->name('user.domain.domain_promotion');
    Route::get( '/search_domain_promotion', 'DomainController@search_domain_promotion' )->name('user.domain.search_domain_promotion');
    Route::post('/order/payment_domain_off', 'CardController@payment_domain_off')->name('invoice.payment_domain_off');
    Route::get('/domain/info', 'DomainController@info')->name('user.domain.info');
    Route::get('/add-card-domain-promotion/{id}', 'CardController@addCardDomainPromotion')->name('user.addCardDomainPromotion');
    //Gia hạn domain
    Route::get( '/domain/domain-product-expired-price', 'DomainController@domain_product_expired_price' )->name('user.domain.product_expired_price');
    Route::post('/order-domain-expired', 'CardController@order_domain_expired')->name('user.domain.order_domain_expired');
    Route::get('/order-domain-expired/check-out', 'CardController@check_out_domain_expired')->name('order.domain_expired.check_out');
    Route::get('/order/payment_domain_expired_order/{invoice_id}', 'CardController@payment_domain_expired_order')->name('invoice.payment_domain_expired_order');
    Route::get('/pay-in-online/payment_domain_expired_order', 'PayInController@payment_domain_expired_order')->name('user.payment_domain_expired_order');
    Route::get('/pay-in-online/payment_domain_expired_order_cloudzone_point', 'PayInController@payment_domain_expired_order_cloudzone_point')->name('user.payment_domain_expired_order_cloudzone_point');
    Route::post('/order/payment_domain_expired', 'CardController@payment_domain_expired')->name('invoice.payment_domain_expired');
    Route::post('/order/payment_domain_expired_cloudzone_point', 'CardController@payment_domain_expired_cloudzone_point')->name('invoice.payment_domain_expired_cloudzone_point');
    Route::post('/order/payment_domain_expired_off', 'CardController@payment_domain_expired_off')->name('invoice.payment_domain_expired_off');

    //Change pass domain
    Route::post('/domain/change-pass-domain', 'DomainController@change_pass_domain')->name('user.domain.change_pass_domain');

    Route::get('/nhat-ky-hoat-dong', 'ClientController@activity_log')->name('user.activity_log');
    Route::get('/activity-log/danh-sach-log', 'ClientController@list_activity_log')->name('user.list_activity_log');
    Route::get('/activity-log/danh-sach-log-theo-hanh-dong', 'ClientController@list_log_with_action')->name('user.list_log_with_action');

    /*
     * Message
    */
    Route::get('/messages/loadChat', 'MessageController@loadChat')->name('user.messages.loadChat');
    Route::post('/messages/sendMessage', 'MessageController@sendMessage')->name('user.messages.sendMessage');

    // Hợp đồng cung cấp dịch vụ - User
    // Route::get('/contracts', 'ContractController@index')->name('user.contract.index');
    // Route::get('/contracts/create', 'ContractController@create')->name('user.contract.create');
    // Route::post('/contracts/create-data-contract', 'ContractController@create_data_contract')->name('user.contract.create_data_contract');
    // Route::get('/contracts/detail/{id}', 'ContractController@detail')->name('user.contract.detail');
    // Route::post('/contracts/update/{id}', 'ContractController@update')->name('user.contract.update');
    // Route::get('/contracts/get-target-from-user', 'ContractController@get_target_from_user')->name('user.contract.get_target_from_user');
    // Route::get('/contracts/get-target-detail', 'ContractController@get_target_detail')->name('user.contract.get_target_detail');
    // Route::post('/contracts/create_contract_detail', 'ContractController@create_contract_detail')->name('user.contract.create_contract_detail');
    // Route::get('/contracts/print_pdf/{id}', 'ContractController@print_pdf')->name('user.contract.print_pdf');

});

// Route::get('/momo', 'User\ClientController@momo');
// Domain Client Page
Route::get('/client', 'User\ClientController@index')->name('client.index');
Route::get( '/client/products/{id}/{prodName}', 'User\ClientController@product' )->name('client.product');
Route::get( '/client/search-domain', 'User\ClientController@search_domain' )->name('client.domain.search');
Route::post( '/client/search-domain/result', 'User\ClientController@search_domain_result' )->name('client.domain.result');
Route::get('/chinh-sach-quyen-rieng-tu', 'User\ClientController@private_policy')->name('client.private_policy');
Route::get('/dieu-khoan-dich-vu', 'User\ClientController@term_service')->name('client.term_service');

Route::get('/zalo/callback' , 'Admin\DirectAdminController@zalo_callback')->name('zalo.callback');
