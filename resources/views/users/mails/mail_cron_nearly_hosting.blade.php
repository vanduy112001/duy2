<!DOCTYPE html>
<html>
<head>
  <title>Cloudzone Customer System</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
</head>
<body style="width: 100%;margin: auto;">
	<div class="send-mail" style="width: 90%;border: 1px solid #ddd;padding-top: 25px;padding-bottom: 10px;">
		<div class="mail-logo" style="text-align: center;border-bottom: 1px solid #ddd;padding-bottom: 30px;">
			<img src="https://portal.cloudzone.vn/images/logo-cloudzone-mail.png" alt="logo-mail" width="200px">
		</div>
		<div class="mail-content" style="font-size: 16px;font-family: Arial,sans-serif;padding-top: 35px;padding-bottom:35px;margin: 10px 10%;">
        Xin chào {{ $name }}, <br>
        Cloudzone kính gửi thông tin các Hosting sắp hết hạn của quý khách: <br>
        - Danh sách Hosting sắp hết hạn: <br>
        @foreach($list_domain as $domain)
        &nbsp;&nbsp; + Domain: {{ $domain->domain }} <br>
        &nbsp;&nbsp; + Thời gian thuê: {{ $billing[$domain->billing_cycle] }} <br>
        @endforeach
        - Số lượng: {{ $quantity }} <br>
        - Ngày hết hạn: {{ date('d-m-Y', strtotime( $next_due_date )) }} <br>
        - Thành tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }} <br>
        Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>
        <br><b>Cloudzone xin cảm ơn quý khách!</b>
		</div>
	</div>
  <div class="mail-footer" style="width: 90%;border: 1px solid #ddd;background: #f2f2f2;">
		<div style="padding: 15px 4% 10px;text-align: center;font-size: 13px;font-family: roboto;">
			<b>Công ty TNHH MTV Công nghệ Đại Việt Số</b><br>
            Địa chỉ: 257 Lê Duẩn , Đà Nẵng. <br>
            Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng. <br>
            Điện thoại: 08 8888 0043 - Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a><br>
            Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a>
            - Fanpage: <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a>
            <br>
            <div style="padding-top: 5px;">
                <a href="https://m.me/cloudzone.vn" style="padding-right: 5px;"><img src="https://portal.cloudzone.vn/images/icon-facebook.png" width="30px"></a>
            </div>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>
