@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin yêu cầu thanh toán: <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - Hóa đơn số: {{ $invoice_id }} <br>
  - Giao dịch số: {{ $history_pay_id }} <br>
  - Ma GD: {{ $magd }} <br>
  - Hình thức: {{ $type }}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
