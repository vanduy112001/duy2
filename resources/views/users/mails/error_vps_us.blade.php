@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    <b>Thông tin VPS</b> <br>
    - Customer ID: {{ $customer }} <br>
    - Khách hàng: {{ !empty($vps->user_vps->name) ?  $vps->user_vps->name : $vps->user_id }} <br>
    - CPU: {{ !empty($vps->product->meta_product->cpu) ? (float)$vps->product->meta_product->cpu : 1 }} <br>
    - RAM: {{ !empty($vps->product->meta_product->memory) ? (float)$vps->product->meta_product->memory : 1 }} <br>
    - Disk: {{ !empty($vps->product->meta_product->disk) ? (float)$vps->product->meta_product->disk : 1 }} <br>
    - Thời gian: {{ date('d-m-Y h:m:s') }}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
