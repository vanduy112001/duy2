@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
Xin chào, {{ $name }}<br>
Chào mừng bạn đến với Cloudzone<br>
Bạn đã thành công tạo tài khoản Cloudzone. Để kích hoạt tài khoản này, vui lòng click vào nút bên dưới để xác thực email của bạn.
<div class="mail-button" style="padding-bottom: 25px;">
	<a href="{{ route('verify', $token) }}" style="color: blue;" target="_blank">Kích hoạt tài khoản</a> <br>
	Hoặc copy đường link bên dưới và dán vào trình duyệt để kích hoạt tài khoản. <br>
	{{ route('verify', $token) }}
</div> <br>
<!-- kết thúc mail tạo tài khoản -->
Với tài khoản này, Quý khách có thể đặt mua, quản lý các dịch vụ và thanh toán trực tuyến các hóa đơn,
hợp đồng cũng như đề nghị sự hỗ trợ từ Phòng Kỹ thuật (qua ticket) cho các dịch vụ mà
Quý khách đang sử dụng tại CLOUDZONE. <br> <br>
Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br> <br>
Quý khách có thể tham khảo bảng giá các gói dịch vụ tại: <br>
<ol>
	<li>
		Dịch vụ Cloud Hosting:
		<a href="https://cloudzone.vn/cloud-hosting/">https://cloudzone.vn/cloud-hosting/</a>
	</li>
	<li>
		Dịch vụ Cloud VPS:
		<a href="https://cloudzone.vn/cloud-server/">https://cloudzone.vn/cloud-server/</a>
	</li>
</ol>
Tổng hợp các hướng dẫn sử dụng dịch vụ tại CLOUDZONE: <a href="http://support.cloudzone.vn/">http://support.cloudzone.vn/</a> <br>
Cập nhật các thông tin mới nhất tại Fanpage <a href="https://www.facebook.com/cloudzone.vn/">CLOUDZONE.VN</a> và
<a href="https://www.facebook.com/groups/cloudzone.vn/">group Cloudzone - cộng đồng hỗ trợ vps, hosting, máy chủ</a> <br>
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: Aria;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
