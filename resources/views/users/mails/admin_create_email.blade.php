@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
Xin chào, {{ $name }} <br>
Cảm ơn Quý khách đã đăng ký tài khoản tại CLOUDZONE. <br>
Dưới đây là thông tin đăng nhập tài khoản khách hàng của Quý khách: <br>
- <b>Tài khoản:</b> {{ $email }} <br>
- <b>Mật khẩu:</b> {{ $password }} <br>
- <b>Liên kết đăng nhập:</b> <a href="https://portal.cloudzone.vn/">Portal Cloudzone</a> <br> <br>
<!-- kết thúc mail tạo tài khoản -->
Với tài khoản này, Quý khách có thể đặt mua, quản lý các dịch vụ và thanh toán trực tuyến các hóa đơn,
hợp đồng cũng như đề nghị sự hỗ trợ từ Phòng Kỹ thuật (qua ticket) cho các dịch vụ mà
Quý khách đang sử dụng tại CLOUDZONE. <br> <br>
Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br> <br>
Quý khách có thể tham khảo bảng giá các gói dịch vụ tại: <br>
<ol>
	<li>
		Dịch vụ Cloud Hosting:
		<a href="https://cloudzone.vn/cloud-hosting/">https://cloudzone.vn/cloud-hosting/</a>
	</li>
	<li>
		Dịch vụ Cloud VPS:
		<a href="https://cloudzone.vn/cloud-server/">https://cloudzone.vn/cloud-server/</a>
	</li>
</ol>
Tổng hợp các hướng dẫn sử dụng dịch vụ tại CLOUDZONE: <a href="http://support.cloudzone.vn/">http://support.cloudzone.vn/</a> <br>
Cập nhật các thông tin mới nhất tại Fanpage <a href="https://www.facebook.com/cloudzone.vn/">CLOUDZONE.VN</a> và
<a href="https://www.facebook.com/groups/cloudzone.vn/">group Cloudzone - cộng đồng hỗ trợ vps, hosting, máy chủ</a> <br>
@endsection
@section('footer')
@endsection
