@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    <b>Khách hàng đăng ký thành công tên miền {{ $domain }}</b> <br>
    - Thông tin chi tiết:
    - ID user: {{ $user_id }}
    - Domain: {{ $domain }} <br>
    - Thời hạn: {{ $due_date }} <br>
    - Tổng cộng: {{ $total }}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
