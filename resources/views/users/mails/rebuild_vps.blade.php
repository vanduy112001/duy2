@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin VPS đã cài đặt lại <br>
  - ip: {{ $ip }} <br>
  - OS:
  @if($os == 1)
    Windows 7 <br>
    - Security: Có <br>
  @elseif($os == 2)
    Windows 2012 <br>
    - Security: Có <br>
  @elseif($os == 3)
    Windows 2016 <br>
    - Security: Có <br>
  @elseif($os == 4)
    CentOs 7 64bit <br>
    - Security: Có <br>
  @elseif($os == 11)
    Windows 7 <br>
    - Security: Không <br>
  @elseif($os == 12)
    Windows 2012 <br>
    - Security: Không <br>
  @elseif($os == 13)
    Windows 2016 <br>
    - Security: Không <br>
  @elseif($os == 14)
    CentOs 7 64bit <br>
    - Security: Không <br>
  @endif


<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
