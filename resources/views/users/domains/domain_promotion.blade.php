@extends('layouts.user2.app')
@section('title')
    {{-- <i class="fas fa-globe nav-icon"></i> Tìm tên miền --}}
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-globe"></i> Search domains</li>
@endsection
@section('content')

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@endif

{{-- <div class="card card-outline card-primary" style="margin-top: 20px"> --}}
<div class="box box-primary" style="margin-top: 20px">
    <div class="card-header bg-white">
        <h3 class="card-title  text-primary mb-0"><i class="fas fa-globe nav-icon"></i> Tìm tên miền</h3>
        <!-- /.card-tools -->
    </div>
    <div class="card-body">
        <form name="checkdomain" action="{{ route('client.domain.result') }}" method="POST" id="form-check-domain">
            {{ csrf_field() }}
            <div class="row pt-4">
                <div class="col-md-10" style="margin: auto;display: inline-flex;" id="input-checkdomain">
                <input type="text" name="domain" class="form-control domain-input" required value="{{ old('domain') }}" placeholder="Nhập tên miền để kiểm tra ..." autofocus autocomplete="off" required>
                    <span id="submit_checkdomain">
                        <input type="submit" class="btn btn-primary ml-1" name="checkdomain" value="Kiểm tra">
                    </span>
                </div>
            </div>

            <div class="row pt-3">
                <div class="radio label_ext" style="margin: auto;">
                    <label><input type="radio" name="ext" value="all" checked=""> Chọn tất cả</label>
                    <label><input type="radio" name="ext" value=".com"> com</label>
                    <label><input type="radio" name="ext" value=".net"> net</label>
                </div>
            </div>
        </form>
        {{-- Table bảng giá --}}
        <div class="row pt-3 pb-3" style="display: block;">
            <div id="form-check-domain-load">

            </div>
        </div>
    </div>
</div>
<input type="hidden" name="id" id="event_id" value="{{ $id }}">


@endsection

@section('scripts')
<script src="{{ url('libraries/greensock-js/src/minified/TweenMax.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#form-check-domain').submit(function(e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            $.ajax({
                url: '/search_domain_promotion',
                type: 'get',
                dataType: 'JSON',
                data: form.serialize(),
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#form-check-domain-load').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    var event_id = $('#event_id').val();
                    var html = '';
                    $.each(data, function (index, value){
                        if (value.result == 0) {
                            html += "Tên miền <a href='http://" + value.domain + "' target='_blank'>" + value.domain + "</a> đã đuợc đăng ký<br>";
                            // Hien thi thong tin whois
                            html += '<div class="card card-outline card-danger mt-3 mb-3 load">';
                            html += '<div class="card-body">';
                            html += '<div class="float-left text-danger"><h5><i class="fa fa-times-circle"></i> ' + value.domain + ' </h5></div>';
                            html += '<div class="float-right"><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#show-info-domain-'+ index +'">Xem whois </button></div>' ;
                            html += '</div>';
                            html += '</div>';
                            //modal hiển thị whois
                            html += '<div class="modal fade" id="show-info-domain-'+ index +'" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">';
                            html += '<div class="modal-dialog" role="document">';
                            html += '<div class="modal-content">';
                            html += '<div class="modal-header">';
                            html += '<h5 class="modal-title" id="exampleModalLabel">Thông tin whois của ' + value.domain + '</h5>';
                            html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'
                            html += '</div>';
                            html += '<div class="modal-body">';
                            html += value.info;
                            html += '</div>';
                            html += '<div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div>';
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        } else if (value.result == 1) {
                            html += "Tên miền <a href='http://" + value.domain + "' target='_blank'>" + value.domain + "</a> chưa đăng ký<br>";
                            // Hien thi thong tin whois
                            html += '<div class="card card-outline card-success mt-3 mb-3 load">';
                            html += '<div class="card-body">';
                            html += '<div class="float-left text-success"><h5><i class="fa fa-check-circle"></i> ' + value.domain + ' </h5></div>';
                            html += '<div class="float-right"> Phí đăng kí: <span class="text-danger font-weight-bold">0 VNĐ/năm</span> <a href="../add-card-domain-promotion/' + event_id + '?domain='+value.domain+'"><button type="button" class="btn btn-success"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Đăng ký </button></a></div>' ;
                            html += '</div>';
                            html += '</div>';
                            // Model form đặt hàng
                        } else {
                            html += '<div class="card card-outline card-danger mt-3 mb-3 load">';
                            html += '<div class="card-body">';
                            html += "<span style='color:#F00'>" + value.result + "</span>";
                            html += '</div>';
                            html += '</div>';
                        }
                    });
                    $('#form-check-domain-load').html(html);
                },
                error: function(error) {
                    $('#form-check-domain-load').html('<span class="text-danger">Tên miền không hợp lệ</span>');
                }
            });
        });
    });
    //Hàm thêm phân cách dấu chấm vào giá tiền
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

</script>
@endSection
