
@extends('layouts.user2.app')
@section('style')
@endsection
@section('title')
<div class="text-primary">Kiểm tra đơn hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Kiểm tra đơn hàng</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish text-center">
                @if(session("success"))
                    <div class="bg-success rounded">
                        <p class="text-light pt-4" >{{session("success")}}</p>
                        <div class="text-light pb-4">
                            Hoặc bấm <a href="{{ route('order.check_invoices', $invoice_id) }}" class="check-invoices">vào đây</a> để thanh toán.
                        </div>
                    </div>
                @elseif (session("promotion"))
                    <div class="bg-success rounded">
                        <p class="text-light pt-4" >{{session("promotion")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger rounded">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @elseif(session("finish"))
                    <div class="bg-success rounded">
                        <p class="text-light">{{session("finish")}}</p>
                    </div>
                @elseif(session("verify_success"))
                    <div class="bg-success rounded">
                        <p class="text-light">{{session("verify_success")}}</p>
                    </div>
                    <div class="check-invoice text-center">
                        <a href="" class="btn btn-outline-info">Đến thanh toán</a>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger-order" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-12 col text-center back-to-home pt-4 mt-4">
                <a href="{{ route('index') }}"><i class="fas fa-angle-left"></i> Trở về trang chủ</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection
