<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <title>{{ 'Trang quản lý dịch vụ - Cloudzone Portal' }}</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('css/template.css')}}">
    {{---------------------------------- Template AdminLTE --------------------------}}

    <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Fugaz+One&display=swap" rel="stylesheet">
    <link href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{ asset('css/user.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
    <link href="{{ asset('css/usercss.css') }}" rel="stylesheet">
</head>
<body id="page-top">
    <div id="wrapper">
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content" class="content-payment-order">
                <!-- End of Topbar -->
                <div class="content-row">
                    <h4 class="mt-4 mb-4 title-payment-order">Thanh toán đơn đặt hàng</h4>
                    <div class="content-form">
                      <div class="sap_tabs">
                        <div id="horizontalTab">
                          <div class="resp-tabs-container">
                              <div class="resp-tab-content">
                                <div class="payment-info text-center">
                                  <h3 class="mb-4">Hình thức thanh toán bằng số dư tài khoản</h3>
                                  <div class="info-company text-center mt-4">
                                      <b>Số dư tài khoản: </b> {{ !empty(Auth::user()->credit->value) ? number_format(Auth::user()->credit->value,0,",",".") . ' VNĐ' : '0 VNĐ' }} <br>
                                      <b>Tổng số tiền thanh toán: </b> {!!number_format($invoice->order->total,0,",",".") . ' VNĐ'!!} <br>
                                      <b>Loại giao dịch: </b> Gia hạn dịch vụ <br>
                                      @if (Auth::user()->credit->value >= $invoice->order->total)
                                        <form action="/order/payment_expired_form" method="post">
                                            @csrf
                                            <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
                                            <button type="submit" id="submit_payment" class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>
                                        </form>
                                      @else
                                        <span class="text-danger"><b>Lưu ý: </b> Số tiền của quý khách không đủ để thực hiện giao dịch. Vui lòng nạp tiền.</span>
                                        <br>
                                        <a href="/order/add_credit_invoice/{{$invoice->id}}" class="btn btn-danger mt-4 mb-4">Nạp tiền vào tài khoản</a>
                                      @endif
                                  </div>
                                </div>
                              </div>
                          </div>

                        </div>
                      </div>
                      <div class="text-center mt-4 mb-4">
                          <a href="{{ route('index') }}">Trở về trang chủ</a>
                      </div>
                    </div>
                </div>
                <!-- End of Main Content -->
                <!-- Footer -->
                @include('layouts/user/includes/footer')
                <!-- End of Footer -->
            </div>
            <input type="hidden" id="invoice_id" value="{{ $invoice->id }}">
            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>
        </div>
        <!-- code huong dan html: https://p.w3layouts.com/demos/payment_form_widget/web/ -->
        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('libraries/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('libraries/js/template.js')}}"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/user.min.js') }}"></script>
        <script src="{{ asset('js/all_page.js') }}"></script>

        <script src="{{ asset('js/payment_expired.js') }}"></script>
    </div>
</body>
</html>
