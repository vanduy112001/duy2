@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
Quản lý hóa đơn
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Quản lý hóa đơn</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
</div>
<div class="row form-inline">
  {{-- <div class="col-md-8">

  </div> --}}
  <div class="col-md-4 ml-auto" style="max-width: 235px;">
      Số lượng:
      <select class="form-control form-control-sm vp_classic invoice_classic">
        <option value="20" selected>30 hóa đơn</option>
        <option value="50">50 hóa đơn</option>
        <option value="100">100 hóa đơn</option>
        <option value="200">200 hóa đơn</option>
      </select>
  </div>
</div>
<div class="box box-default">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-sm table-bordered">
                <thead class="">
                    <th>Hóa đơn</th>
                    <th width="30%">Loại</th>
                    <th>Hóa đơn ngày</th>
                    <th>Ngày kết thúc</th>
                    <th>Tổng tiền</th>
                    <th>Trạng thái</th>
                </thead>
                <tbody>
                    @if ($invoices->count() > 0)
                        @foreach ($invoices as $invoice)
                            <tr>
                                <td><a href="{{ route('order.check_invoices', $invoice->id) }}">#{{ $invoice->id }}</a></td>
                                <td>
                                    @if(!empty( $invoice->order->type ))
                                      @if ($invoice->order->type == 'expired')
                                        @if ($invoice->type == 'Domain_exp')
                                            Gia hạn Domain
                                            <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                              <i class="fas fa-plus"></i>
                                            </button>
                                            <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                              @foreach ($invoice->order_expireds as $key => $order_expired)
                                                @if(!empty($order_expired->domain->domain))
                                                  {{ $order_expired->domain->domain }} <br>
                                                @endif
                                              @endforeach
                                            </div>
                                        @else
                                            Gia hạn
                                            @if($invoice->type == 'VPS')
                                              VPS
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->vps->ip))
                                                    {{ $order_expired->vps->ip }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'NAT-VPS')
                                              NAT VPS
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->vps->ip))
                                                    {{ $order_expired->vps->ip }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'VPS US')
                                              VPS US
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->vps->ip))
                                                    {{ $order_expired->vps->ip }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'Proxy')
                                                Proxy
                                                <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                  <i class="fas fa-plus"></i>
                                                </button>
                                                <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                  @foreach ($invoice->order_expireds as $key => $order_expired)
                                                    @if(!empty($order_expired->proxy->ip))
                                                      {{ $order_expired->proxy->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                  @endforeach
                                                </div>
                                            @elseif($invoice->type == 'Server')
                                              Server
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->server->ip))
                                                    {{ $order_expired->server->ip }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'Colocation')
                                              Colocation
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->colocation->ip))
                                                    {{ $order_expired->colocation->ip }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'hosting')
                                              Hosting
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse tooltip"
                                                data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->hosting->domain))
                                                    {{ $order_expired->hosting->domain }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @elseif($invoice->type == 'Email Hosting')
                                              Email Hosting
                                              <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                                <i class="fas fa-plus"></i>
                                              </button>
                                              <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                                @foreach ($invoice->order_expireds as $key => $order_expired)
                                                  @if(!empty($order_expired->email_hosting->domain))
                                                    {{ $order_expired->email_hosting->domain }} <br>
                                                  @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                  @endif
                                                @endforeach
                                              </div>
                                            @endif
                                        @endif
                                      @elseif($invoice->type == 'change_ip')
                                        Đổi IP
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->order_change_vps as $key => $order_change_vps)
                                            @if(!empty($order_change_vps->ip))
                                              IP cũ: {{ $order_change_vps->ip }} -
                                            @endif
                                            @if(!empty($order_change_vps->vps->ip))
                                              IP mới: {{ $order_change_vps->vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'addon_vps')
                                        Cấu hình thêm VPS
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->order_addon_vps as $key => $order_addon_vps)
                                            @if(!empty($order_addon_vps->vps->ip))
                                              {{ $order_addon_vps->vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'VPS')
                                        Đặt hàng VPS
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->vps as $key => $vps)
                                            @if(!empty($vps->ip))
                                              {{ $vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'NAT-VPS')
                                        Đặt hàng NAT VPS
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->vps as $key => $vps)
                                            @if(!empty($vps->ip))
                                              {{ $vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'VPS-US')
                                        Đặt hàng VPS US
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->vps as $key => $vps)
                                            @if(!empty($vps->ip))
                                              {{ $vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Proxy')
                                        Đặt hàng Proxy
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->proxies as $key => $proxy)
                                            @if(!empty($proxy->ip))
                                              {{ $proxy->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Hosting')
                                        Đặt hàng Hosting
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->hostings as $key => $hosting)
                                            @if(!empty($hosting->domain))
                                              {{ $hosting->domain }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Email Hosting')
                                        Đặt hàng Email Hosting
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->email_hostings as $key => $hosting)
                                            @if(!empty($hosting->domain))
                                              {{ $hosting->domain }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Server')
                                        Đặt hàng Server
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->servers as $key => $vps)
                                            @if(!empty($vps->ip))
                                              {{ $vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Colocation')
                                        Đặt hàng Colocation
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->colocations as $key => $vps)
                                            @if(@!empty($vps->ip))
                                              {{ $vps->ip }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'Domain')
                                        Đặt hàng Domain
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse tooltip"
                                        data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @foreach ($invoice->domains as $key => $hosting)
                                            @if(!empty($hosting->domain))
                                              {{ $hosting->domain }} <br>
                                            @else
                                              <span class="text-danger">Chưa tạo</span> <br>
                                            @endif
                                          @endforeach
                                        </div>
                                      @elseif($invoice->type == 'upgrade_hosting')
                                        Nâng cấp Hosting
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-plus" data-toggle="collapse"   data-target="#service{{$invoice->id}}"   data-placement="top" title="Chi tiết">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                        <div id="service{{$invoice->id}}" class="collapse mt-2 show-detail-service">
                                          @if( !empty($invoice->order_upgrade_hosting->hosting->domain) )
                                            {{ $invoice->order_upgrade_hosting->hosting->domain }} <br>
                                          @else
                                            <span class="text-danger">Chưa tạo</span> <br>
                                          @endif
                                        </div>
                                      @else
                                        Đặt hàng
                                      @endif
                                    @else
                                      Đặt hàng
                                    @endif
                                </td>
                                <td>{{ date('d-m-Y', strtotime($invoice->created_at)) }}</td>
                                <td>{{ date('d-m-Y', strtotime($invoice->due_date)) }}</td>
                                <td>{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : ($invoice->sub_total * $invoice->quantity) ,0,",",".") . ' VNĐ'!!}</td>
                                <td>
                                    @if (!empty($invoice->paid_date))
                                        <a href="{{ route('order.check_invoices', $invoice->id) }}" class="btn btn-outline-success btn-sm">Đã thanh toán</a>
                                    @else
                                        <a href="{{ route('order.check_invoices', $invoice->id) }}" class="btn btn-outline-danger btn-sm">Chưa thanh toán</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="6" class="text-center">Quý khách không có đơn hàng nào cả.</td>
                    @endif
                </tbody>
                <tfoot>
                    <td colspan="6" class="text-center link-right">
                        {{ $invoices->links() }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script src="{{ asset('js/user_list_invoice.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="collapse"]').tooltip();
        $(document).on("click", '.tooggle-plus', function () {
            $(this).children("i").removeClass("fa-plus");
            $(this).children("i").addClass("fa-minus");
            $(this).removeClass("tooggle-plus");
            $(this).addClass("toggle-minus");
        })
        $(document).on("click", '.toggle-minus', function () {
            $(this).children("i").removeClass("fa-minus");
            $(this).children("i").addClass("fa-plus");
            $(this).removeClass("toggle-minus");
            $(this).addClass("tooggle-plus");
        })
    });
</script>
@endsection
