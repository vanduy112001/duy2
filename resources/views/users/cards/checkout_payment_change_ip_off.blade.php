
@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Kiểm tra đơn hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Kiểm tra đơn hàng</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if ($pay_in_office->method_gd_invoice == 'pay_in_office')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light text-finish">Yêu cầu thanh toán đơn hàng đổi IP cho VPS bằng hình thức trực tiếp tại văn phòng thành công</p>
                      </div>
                      <div class="card-body mb-4">
                          <div class="company-name">Quý khách vui lòng đến địa chỉ của văn phòng để hoàn thành thanh toán.</div>
                          <div class="info-company">
                              <b>Địa chỉ văn phòng:</b> 67 - Nguyễn Thị Định - quận Sơn Trà - Thành phố Đà Nẵng <br><b>Điện thoại hỗ trợ:</b> 0236 4455 789
                              <br><b class="mt-3">Chi tiết đơn hàng:</b> <br>
                              - Loại giao dịch: Trực tiếp tại văn phòng <br>
                              - Số tiền thanh toán: {{ number_format($pay_in_office->detail_order->sub_total * $pay_in_office->detail_order->quantity,0,",",".") . ' VNĐ' }} <br>
                              - Hóa đơn số: #{{ $pay_in_office->detail_order_id }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd_invoice == 'bank_transfer_vietcombank')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light text-finish">Yêu cầu thanh toán đơn hàng đổi IP cho VPS bằng hình thức chuyển khoản ngân hàng Vietcombank thành công</p>
                      </div>
                      <div class="card-body">
                          <div class="company-name">Quý khách vui lòng chuyển khoản ngân hàng theo thông tin bên dưới và kèm theo mã giao dịch.</div>
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Bank Name:</b> Vietcombank <br>
                                  <b>Account Name:</b> Nguyen Thi Ai Hoa <br>
                                  <b>Account Number:</b> 0041000830880 <br>
                              </div>
                              <b class="mt-3">Chi tiết đơn hàng:</b> <br>
                              - Loại giao dịch: Chuyển khoản ngân hàng <br>
                              - Số tiền thanh toán: {{ number_format($pay_in_office->detail_order->sub_total * $pay_in_office->detail_order->quantity,0,",",".") . ' VNĐ' }} <br>
                              - Hóa đơn số: #{{ $pay_in_office->detail_order_id }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd_invoice == 'bank_transfer_techcombank')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light text-finish">Yêu cầu thanh toán đơn hàng đổi IP cho VPS bằng hình thức chuyển khoản ngân hàng Techcombank thành công</p>
                      </div>
                      <div class="card-body">
                          <div class="company-name">Quý khách vui lòng chuyển khoản ngân hàng theo thông tin bên dưới và kèm theo mã giao dịch.</div>
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Bank Name:</b> Techcombank <br>
                                  <b>Account Name:</b> Nguyen Thi Ai Hoa <br>
                                  <b>Account Number:</b> 19033827040017 <br>
                              </div>
                              <b class="mt-3">Chi tiết đơn hàng:</b> <br>
                              - Loại giao dịch: Chuyển khoản ngân hàng <br>
                              - Số tiền thanh toán: {{ number_format($pay_in_office->detail_order->sub_total * $pay_in_office->detail_order->quantity,0,",",".") . ' VNĐ' }} <br>
                              - Hóa đơn số: #{{ $pay_in_office->detail_order_id }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd_invoice == 'momo')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light text-finish">Yêu cầu thanh toán đơn hàng đổi IP cho VPS bằng hình thức thanh toán qua MoMo thành công</p>
                      </div>
                      <div class="card-body mb-4">
                          <div class="company-name">Quý khách vui lòng chuyển khoản MoMo theo thông tin bên dưới và kèm theo mã giao dịch.</div>
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Account Name:</b> Lê Minh Chí <br>
                                  <b>SĐT MoMo:</b> 0905 091 805 <br>
                              </div>
                              <b class="mt-3">Chi tiết đơn hàng:</b> <br>
                              - Loại giao dịch: Thanh toán qua MoMo <br>
                              - Số tiền thanh toán: {{ number_format($pay_in_office->detail_order->sub_total * $pay_in_office->detail_order->quantity,0,",",".") . ' VNĐ' }} <br>
                              - Hóa đơn số: #{{ $pay_in_office->detail_order_id }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                          </div>
                      </div>
                  </div>
                @endif
            </div>
            <div class="col-md-12 col text-center back-to-home pt-4 mt-4">
                <a href="{{ route('index') }}"><i class="fas fa-angle-left"></i> Trở về trang chủ</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
@endsection
