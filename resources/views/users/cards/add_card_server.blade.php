@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Thiết lập</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thiết lập</li>
@endsection
@section('content')
<div class="box box-primary pb-4">
    <div class="box-body">
        <div class="row pb-4">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger-order" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                
            </div>
            <div class="col-12 col-lg-7">
                <h4 class="text-primary">Tùy chọn cấu hình server</h4>
                <hr>
                <form action="{{ route('user.form_order_server') }}" method="post" id="form-user-order">
                <div class="product-server-info rounded">
                    <div class="product-server-title">
                      {{ $product->name }}
                      @if(!empty($product->pricing[$billing_cycle]))
                        ({!! number_format($product->pricing['monthly'],0,",",".") !!}/tháng)
                      @endif
                    </div>
                    <div class="product-detail">
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">Tên máy chủ</b>
                                <div class="item-title">{{ $product->meta_product->name_server }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">CPU</b>
                                <div class="item-title">{{ $product->meta_product->cpu }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">Cores</b>
                                <div class="item-title">{{ $product->meta_product->cores }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">RAM</b>
                                <input type="hidden" name="addon_ram" id="addon_ram" value="0">
                                <div class="item-title">{{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '0' }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            @if (!empty($list_add_on_disk_ram))
                                <div class="item-icon">
                                    <button type="button" class="btn btn-sm btn-outline-primary" id="btn_addon_ram" data-type="plus" data-toggle="tooltip" data-placement="top" title="Addon RAM"><i class="fas fa-plus"></i></button>
                                </div>
                            @endif
                        </div>
                        {{-- Addon ram --}}
                        @if (!empty($list_add_on_disk_ram))
                            {{-- addon-ram-hidden --}}
                            <div class="product-item select-item addon-ram-hidden" data-type="off" id="addon_ram_container">
                                <div class="item-text">
                                    <b class="item-label">Addon RAM</b>
                                    <div class="item-title">None</div>
                                </div>
                                <div class="item-amount">
                                    <div class="amount">0 ₫</div>
                                </div>
                                <div class="item-icon">
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="item-select">
                                    <div class="list-select">
                                        <div class="list-select-item selected" data-id="0" data-title="None" data-type="addon_ram">
                                            <div class="list-select-item-text">None</div>
                                            <div class="list-select-item-amount">0</div>
                                        </div>
                                        @foreach ($list_add_on_disk_ram as $key => $add_on_disk_ram)
                                            <div class="list-select-item" data-id="{{ $add_on_disk_ram->id }}" data-title="{{ $add_on_disk_ram->name }}" data-type="addon_ram">
                                                <div class="list-select-item-text">{{ $add_on_disk_ram->name }}</div>
                                                <div class="list-select-item-amount">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                        {{-- Disk --}}
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">DISK 1</b>
                                <div class="item-title">{{ !empty($product->product_drive->first) ? $product->product_drive->first : 'None' }}</div>
                            </div>
                            <div class="item-amount">
                                <div class="amount">0 ₫</div>
                            </div>
                        </div>
                        @if ( !empty($product->product_drive->second) )
                          <div class="product-item item-hover">
                              <div class="item-text">
                                  <b class="item-label">DISK 2</b>
                                  <div class="item-title">{{ !empty($product->product_drive->second) ? $product->product_drive->second : 'None' }}</div>
                                  <input type="hidden" name="disk2" id="disk2" value="0">
                              </div>
                              <div class="item-amount">
                                <div class="amount">0 ₫</div>
                              </div>
                          </div>
                        @else
                          <div class="product-item select-item" data-type="off" id="item-disk2">
                              <div class="item-text">
                                  <b class="item-label">DISK 2</b>
                                  <div class="item-title">{{ !empty($product->product_drive->third) ? $product->product_drive->third : 'None' }}</div>
                                  <input type="hidden" name="disk2" id="disk2" value="0">
                              </div>
                              <div class="item-amount">
                                  <div class="amount">0 ₫</div>
                              </div>
                              <div class="item-icon">
                                  <i class="fas fa-sort-down"></i>
                              </div>
                              <div class="item-select">
                                  <div class="list-select">
                                      <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk2">
                                          <div class="list-select-item-text">None</div>
                                          <div class="list-select-item-amount">0</div>
                                      </div>
                                      @if (!empty($list_add_on_disk_server))
                                          @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                              <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk2">
                                                  <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                  <div class="list-select-item-amount">
                                                      {{
                                                          !empty($add_on_disk_server->pricing[$billing_cycle]) ?
                                                          number_format($add_on_disk_server->pricing[$billing_cycle],0,",",".") : '0'
                                                      }}
                                                       ₫
                                                  </div>
                                              </div>
                                          @endforeach
                                      @endif
                                  </div>
                              </div>
                          </div>
                        @endif
                        <div class="product-item select-item" data-type="off" id="item-disk3">
                            <div class="item-text">
                                <b class="item-label">DISK 3</b>
                                <div class="item-title">{{ !empty($product->product_drive->third) ? $product->product_drive->third : 'None' }}</div>
                                <input type="hidden" name="disk3" id="disk3" value="0">
                            </div>
                            <div class="item-amount">
                                <div class="amount">0 ₫</div>
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk3">
                                        <div class="list-select-item-text">None</div>
                                        <div class="list-select-item-amount">0</div>
                                    </div>
                                    @if (!empty($list_add_on_disk_server))
                                        @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                            <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk3">
                                                <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                <div class="list-select-item-amount">
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="product-item select-item" data-type="off" id="item-disk4">
                            <div class="item-text">
                                <b class="item-label">DISK 4</b>
                                <div class="item-title">{{ !empty($product->product_drive->four) ? $product->product_drive->four : 'None' }}</div>
                                <input type="hidden" name="disk4" id="disk4" value="0">
                            </div>
                            <div class="item-amount">
                                <div class="amount">0 ₫</div>
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk4">
                                        <div class="list-select-item-text">None</div>
                                        <div class="list-select-item-amount">0</div>
                                    </div>
                                    @if (!empty($list_add_on_disk_server))
                                        @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                            <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk4">
                                                <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                <div class="list-select-item-amount">
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        @if ( !empty($product->meta_product->chassis) )
                            @if ($product->meta_product->chassis == 8)
                                <div class="product-item select-item" data-type="off" id="item-disk5">
                                    <div class="item-text">
                                        <b class="item-label">DISK 5</b>
                                        <div class="item-title">{{ !empty($product->product_drive->five) ? $product->product_drive->five : 'None' }}</div>
                                        <input type="hidden" name="disk5" id="disk5" value="0">
                                    </div>
                                    <div class="item-amount">
                                        <div class="amount">0 ₫</div>
                                    </div>
                                    <div class="item-icon">
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="item-select">
                                        <div class="list-select">
                                            <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk5">
                                                <div class="list-select-item-text">None</div>
                                                <div class="list-select-item-amount">0</div>
                                            </div>
                                            @if (!empty($list_add_on_disk_server))
                                                @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                                    <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk5">
                                                        <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                        <div class="list-select-item-amount">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item select-item" data-type="off" id="item-disk6">
                                    <div class="item-text">
                                        <b class="item-label">DISK 6</b>
                                        <div class="item-title">{{ !empty($product->product_drive->six) ? $product->product_drive->six : 'None' }}</div>
                                        <input type="hidden" name="disk6" id="disk6" value="0">
                                    </div>
                                    <div class="item-amount">
                                        <div class="amount">0 ₫</div>
                                    </div>
                                    <div class="item-icon">
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="item-select">
                                        <div class="list-select">
                                            <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk6">
                                                <div class="list-select-item-text">None</div>
                                                <div class="list-select-item-amount">0</div>
                                            </div>
                                            @if (!empty($list_add_on_disk_server))
                                                @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                                    <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk6">
                                                        <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                        <div class="list-select-item-amount">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item select-item" data-type="off" id="item-disk7">
                                    <div class="item-text">
                                        <b class="item-label">DISK 7</b>
                                        <div class="item-title">{{ !empty($product->product_drive->seven) ? $product->product_drive->seven : 'None' }}</div>
                                        <input type="hidden" name="disk7" id="disk7" value="0">
                                    </div>
                                    <div class="item-amount">
                                        <div class="amount">0 ₫</div>
                                    </div>
                                    <div class="item-icon">
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="item-select">
                                        <div class="list-select">
                                            <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk7">
                                                <div class="list-select-item-text">None</div>
                                                <div class="list-select-item-amount">0</div>
                                            </div>
                                            @if (!empty($list_add_on_disk_server))
                                                @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                                    <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk7">
                                                        <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                        <div class="list-select-item-amount">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="product-item select-item" data-type="off" id="item-disk8">
                                    <div class="item-text">
                                        <b class="item-label">DISK 8</b>
                                        <div class="item-title">{{ !empty($product->product_drive->eight) ? $product->product_drive->eight : 'None' }}</div>
                                        <input type="hidden" name="disk8" id="disk8" value="0">
                                    </div>
                                    <div class="item-amount">
                                        <div class="amount">0 ₫</div>
                                    </div>
                                    <div class="item-icon">
                                        <i class="fas fa-sort-down"></i>
                                    </div>
                                    <div class="item-select">
                                        <div class="list-select">
                                            <div class="list-select-item selected" data-id="0" data-title="None" data-type="disk8">
                                                <div class="list-select-item-text">None</div>
                                                <div class="list-select-item-amount">0</div>
                                            </div>
                                            @if (!empty($list_add_on_disk_server))
                                                @foreach ($list_add_on_disk_server as $key => $add_on_disk_server)
                                                    <div class="list-select-item" data-id="{{ $add_on_disk_server->id }}" data-title="{{ $add_on_disk_server->name }}" data-type="disk8">
                                                        <div class="list-select-item-text">{{ $add_on_disk_server->name }}</div>
                                                        <div class="list-select-item-amount">
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                        <div class="product-item select-item" data-type="off" id="item-raid">
                            <div class="item-text">
                                <b class="item-label">RAID</b>
                                <div class="item-title">{{ !empty($list_raid[0]->raid) ? $list_raid[0]->raid : 'None' }}</div>
                                <input type="hidden" name="raid" id="raid" value="{{ !empty($list_raid[0]->raid) ? $list_raid[0]->raid : 'None' }}">
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    @if (!empty($list_raid))
                                        @foreach ($list_raid as $key => $raid)
                                            <div class="list-select-item {{ $key == 0 ? 'selected' : '' }}" data-id="" data-title="{{ $raid->raid }}" data-pricing="0" data-type="raid">
                                                <div class="list-select-item-text">{{ $raid->raid }}</div>
                                                <div class="list-select-item-amount"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">Port Network</b>
                                <div class="item-title">{{ !empty($product->meta_product->port_network) ? $product->meta_product->port_network : 'None' }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">Băng thông</b>
                                <div class="item-title">{{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : 'None' }}</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                        </div>
                        <div class="product-item item-hover">
                            <div class="item-text">
                                <b class="item-label">IP</b>
                                <input type="hidden" name="addon_ip" id="addon_ip" value="0">
                                <div class="item-title">{{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0' }} IP Public</div>
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            @if (!empty($list_add_on_ip))
                                <div class="item-icon">
                                    <button type="button" class="btn btn-sm btn-outline-primary" id="btn_addon_ip" data-type="plus" data-toggle="tooltip" data-placement="top" title="Addon IP"><i class="fas fa-plus"></i></button>
                                </div>
                            @endif
                        </div>
                        {{-- Addon IP --}}
                        @if (!empty($list_add_on_ip))
                            {{-- addon-ip-hidden --}}
                            <div class="product-item select-item addon-ip-hidden" data-type="off" id="addon_ip_container">
                                <div class="item-text">
                                    <b class="item-label">Addon IP</b>
                                    <div class="item-title">None</div>
                                </div>
                                <div class="item-amount">
                                    <div class="amount">0 ₫</div>
                                </div>
                                <div class="item-icon">
                                    <i class="fas fa-sort-down"></i>
                                </div>
                                <div class="item-select">
                                    <div class="list-select">
                                        <div class="list-select-item selected" data-id="0" data-title="None" data-type="addon_ip">
                                            <div class="list-select-item-text">None</div>
                                            <div class="list-select-item-amount">0</div>
                                        </div>
                                        @foreach ($list_add_on_ip as $key => $add_on_ip)
                                            <div class="list-select-item" data-id="{{ $add_on_ip->id }}" data-title="{{ $add_on_ip->name }}" data-type="addon_ip">
                                                <div class="list-select-item-text">{{ $add_on_ip->name }}</div>
                                                <div class="list-select-item-amount">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="product-item select-item" data-type="off" id="item-datacenter">
                            <div class="item-text">
                                <b class="item-label">DataCenter</b>
                                <div class="item-title">{{ !empty($list_datacenter[0]->datacenter) ? $list_datacenter[0]->datacenter : 'None' }}</div>
                                <input type="hidden" name="datacenter" id="datacenter" value="{{ !empty($list_datacenter[0]->datacenter) ? $list_datacenter[0]->datacenter : 'None' }}">
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    @if (!empty($list_datacenter))
                                        @foreach ($list_datacenter as $key => $datacenter)
                                            <div class="list-select-item {{ $key == 0 ? 'selected' : '' }}" data-id="" data-title="{{ $datacenter->datacenter }}" data-pricing="0" data-type="datacenter">
                                                <div class="list-select-item-text">{{ $datacenter->datacenter }}</div>
                                                <div class="list-select-item-amount"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="product-item select-item" data-type="off" id="item-os">
                            <div class="item-text">
                                <b class="item-label">Hệ điều hành</b>
                                <div class="item-title">{{ !empty($list_os[$list_vps_os[0]->os]) ? $list_os[$list_vps_os[0]->os] : 'None' }}</div>
                                <input type="hidden" name="os" id="os" value="{{ !empty($list_os[$list_vps_os[0]->os]) ? $list_os[$list_vps_os[0]->os] : 'None' }}">
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    @if (!empty($list_vps_os))
                                        @foreach ($list_vps_os as $key => $os)
                                            <div class="list-select-item {{ $key == 0 ? 'selected' : '' }}" data-id="{{ $os->id }}" data-title="{{$list_os[$os->os]}}" data-pricing="0" data-type="os">
                                                <div class="list-select-item-text">{{$list_os[$os->os]}}</div>
                                                <div class="list-select-item-amount"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="product-item select-item" data-type="off" id="item-server-management">
                            <div class="item-text">
                                <b class="item-label">Server Management</b>
                                <div class="item-title">{{ !empty($list_server_management[0]->server_management) ? $list_server_management[0]->server_management : 'None' }}</div>
                                <input type="hidden" name="server_management" id="server_management" value="{{ !empty($list_server_management[0]->server_management) ? $list_server_management[0]->server_management : 'None' }}">
                            </div>
                            <div class="item-amount">
                                {{-- <div class="amount">1.000.000 ₫</div> --}}
                            </div>
                            <div class="item-icon">
                                <i class="fas fa-sort-down"></i>
                            </div>
                            <div class="item-select">
                                <div class="list-select">
                                    @if (!empty($list_server_management))
                                        @foreach ($list_server_management as $key => $server_management)
                                            <div class="list-select-item {{ $key == 0 ? 'selected' : '' }}" data-id="" data-title="{{ $server_management->server_management }}" data-pricing="0" data-type="server_management">
                                                <div class="list-select-item-text">{{ $server_management->server_management }}</div>
                                                <div class="list-select-item-amount"></div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <h4 class="text-primary">Thanh toán</h4>
                <hr>
                <div class="form-group">
                    <label for="">Thời gian thanh toán</label>
                    <select name="billing" class="form-control select2" id="billing">
                         <option value="" disabled>Chọn thời gian</option>
                         @php
                             $term = false;
                         @endphp
                         @foreach ($billings as $key => $billing)
                             @php
                                 $selected = '';
                                 $disabled = false;
                                 if (!$term) {
                                     if (!empty($product->pricing[$key])) {
                                         $term = true;
                                     }
                                 }
                                 if (empty($product->pricing[$key])) {
                                     $disabled = true;
                                 }
                             @endphp
                             @if(!$disabled)
                               <option value="{{$key}}" @if($key === $billing_cycle) selected @endif data-pricing="{{ $product->pricing[$key] }}">{!!number_format($product->pricing[$key],0,",",".") . ' ₫'!!} / {{$billing}}</option>
                             @endif
                         @endforeach
                     </select>
                     <input type="hidden" id="billing-price" name="billing-price" value="{{ $billing_cycle }}" data-pricing="{{ $product->pricing[$billing_cycle] }}">
                </div>
                <div class="form-group">
                    <label for="">Số lượng</label>
                    <input type="text" value="{{ !empty(old('quantity'))? old('quantity') : 1 }}" name="quantity" id="qtt" class="form-control" placeholder="Số lượng">
                </div>
                <div id="ordersumm">
                    <div class="card" style="margin: 30px 0;">
                        <div class="card-header">
                          <h3 class="card-title mb-0">Tóm tắt đơn hàng</h3>
                          <hr>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 summary">
                          <table class="table table-sm table-bordered" id="table-show-price">
                            <thead>
                              <tr>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th class="text-center" style="width: 125px">Chi phí</th>
                              </tr>
                            </thead>
                            <tbody class="show-select-addon">
                              <tr>
                                <td>{{ $product->group_product->name }} - {{ $product->name }}</td>
                                <td class="quantity-printf text-left"> 1 </td>
                                <td class="item_pricing_cell text-right"></td>
                              </tr>
                              <tr class="detail-addon-ram">
                              </tr>
                              <tr class="detail-disk-2">
                              </tr>
                              <tr class="detail-disk-3">
                              </tr>
                              <tr class="detail-disk-4">
                              </tr>
                              <tr class="detail-disk-5">
                              </tr>
                              <tr class="detail-disk-6">
                              </tr>
                              <tr class="detail-disk-7">
                              </tr>
                              <tr class="detail-disk-8">
                              </tr>
                              <tr class="detail-addon-ip">
                              </tr>
                              <tr class="detail-raid">
                              </tr>
                              <tr class="detail-os">
                              </tr>
                              <tr class="detail-datacenter">
                              </tr>
                              <tr class="detail-promotion">
                              </tr>
                            </tbody>
                            <tfoot style="background: #e7ffda">
                                <tr>
                                    <td class="text-right" colspan="2">Tổng cộng</td>
                                    <td class="total_pricing"></td>
                                </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="button_submit text-center">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}" id='product_id'>
                    <input type="hidden" name="type_product" value="{{ $product->type_product }}" id="type_product">
                    {{-- <input type="submit" class="btn btn-block btn-primary" value="Đặt hàng"> --}}
                </div>
            
            </div>
        </div>
        <div class="row text-center">
            <div class="col">
                <a href="" class="btn btn-secondary btn-sm"><i class="fa fa-chevron-circle-left"></i> Quay lại</a>
                <button type="submit" class="btn btn-primary btn-sm px-4">
                    <i class="fa fa-shopping-basket"></i> Đặt hàng
                </button>
            </div>
        </div>
    </form>
    </div>
</div>
<input type="hidden" id="total_order" value="{{ !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : $product->pricing['monthly'] }}">
{{-- Modal xoa product --}}
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order_server.js') }}?token={{ date('YmdH') }}"></script>
@endsection
