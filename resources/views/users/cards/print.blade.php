@extends('layouts.user.app_print')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Chi tiết hóa đơn</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Chi tiết hóa đơn</li>
@endsection
@section('content')
        <div id="invoice-page">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
                <!-- title row -->
                <div class="row invoice-header">
                    <div class="col col-md-6 logo">
                        <img src="{{ url('images/LogoCloud.png') }}" alt="cloudzone.vn" width="200">
                        <p class="invoice-number">Hóa đơn số #{{ $invoice->id }}</p>
                    </div>
                    <div class="col col-md-6 invoice-status text-center">
                        @if (!empty($invoice->paid_date))
                            <div class="paid">Đã thanh toán</div>
                            <div class="date">Ngày thanh toán: {{ date('d-m-Y', strtotime($invoice->paid_date)) }}</div>
                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->dua_date)) }}</div>
                        @else
                            <div class="unpaid">Chưa thanh toán</div>
                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->due_date)) }}</div>
                        @endif
                    </div>
                <!-- /.col -->
                </div>
                <hr>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <div class="invoice-info-header">
                            <strong>Nhà cung cấp dịch vụ</strong>
                        </div>
                        <address>
                            Công ty TNHH MTV Đại Việt Số<br>
                            257 Lê Duẩn, Tân Chính, Thanh Khê, Đà Nẵng<br>
                            SĐT: 08 8888 0043<br>
                            Email: info@cloudzone.vn
                        </address>
                    </div>
                    <!-- /.col -->
                    @php
                        $user = UserHelper::get_user(Auth::user()->id);
                    @endphp
                    <div class="col-sm-6 invoice-col text-right">
                        <div class="invoice-info-header">
                            <strong>Thông tin khách hàng</strong>
                        </div>
                        <address>
                            {{ $user->name }}<br>
                            {{ $user->user_meta->address }}<br>
                            SĐT: {{ $user->user_meta->phone }}<br>
                            Email: {{ $user->email }}
                        </address>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col col-md-12 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Mô tả chi tiết</th>
                                    <th class="text-center">Số tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($invoice->type == 'VPS' )
                                    <tr>
                                        <td>
                                            {{  GroupProduct::get_product($invoice->vps[0]->product_id)->name }}
                                        </td>
                                        <td class="text-center">
                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @elseif($invoice->type == 'Server')
                                    <tr>
                                        <td>
                                            {{  GroupProduct::get_product($invoice->servers[0]->product_id)->name }}
                                        </td>
                                        <td class="text-center">
                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>

                                {{-- In hóa đơn đặt hàng domain --}}
                                @elseif($invoice->type == 'Domain')
                                    @php
                                        $domains = $invoice->domains;
                                    @endphp
                                    @foreach ($domains as $domain)
                                        @php
                                            $product_domain = $domain->domain_product;
                                        @endphp
                                        <tr>
                                            <td>
                                                Đặt hàng tiên miền - {{  $domain->domain }}
                                            </td>
                                            <td class="text-center">
                                                {!!number_format($product_domain[$domain->billing_cycle],0,",",".") . ' VNĐ'!!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right"><b>Thời hạn</b></td>
                                            <td class="text-center">{{ $domain->domain_year }} Năm</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-right"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>

                                {{-- In hóa đơn gia hạn domain --}}
                                @elseif($invoice->type == 'Domain_exp')
                                    @php
                                        $billing_time_domain_exp = config('billing_domain_exp');
                                    @endphp
                                    @if ($invoice->order->type == 'expired')
                                        @php
                                            $domain_expireds = $invoice->domain_expireds;
                                        @endphp
                                        @foreach ($domain_expireds as $domain_expired)
                                            @php
                                                $domain = GroupProduct::get_domain_by_id($domain_expired->domain_id);
                                                $product_domain = GroupProduct::get_product_domain($domain->product_domain_id);
                                            @endphp
                                            <tr>
                                                <td>
                                                    Gia hạn tên miền - {{ $domain->domain }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($product_domain[$domain_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right"><b>Thời hạn</b></td>
                                                <td class="text-center">
                                                    {{ $billing_time_domain_exp[$domain_expired->billing_cycle] }} Năm
                                                    </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td class="text-right"><b>Thành tiền</b></td>
                                            <td class="text-center">{!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                        </tr> 
                                    @endif
                                    
                                @else
                                    @foreach ($invoice->hostings as $hosting)
                                    <tr>
                                        <td>
                                            {{  GroupProduct::get_product($hosting->product_id)->name }} - {{ $hosting->domain }}
                                        </td>
                                        <td class="text-center">
                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                        </td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-right"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
                {{-- row --}}
            </div>
        </div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        window.addEventListener("load", window.print());
    });
</script>
@endsection
