@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
Danh sách đơn đặt hàng
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Danh sách đơn đặt hàng</li>
@endsection
@section('content')
<div class="box box-default">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-sm table-bordered">
                <thead class="">
                    <th>ID</th>
                    <th>Ngày đặt hàng</th>
                    <th>Loại</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền thanh toán</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @if ($orders->count() > 0)
                    @foreach ($orders as $key=>$order)
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ date('d-m-Y H:i:s', strtotime($order->created_at)) }}</td>
                        @foreach ($order->detail_orders as $detail_order)
                            @if (!empty($detail_order->type))
                              @if ($order->type == 'expired')
                                <td>Gia hạn {{ $type_invoice[$detail_order->type] }}</td>
                              @else
                                @if ($detail_order->type == 'change_ip')
                                  <td>Đổi IP VPS</td>
                                @elseif ($detail_order->type == 'addon_vps')
                                  <td>Nâng cấp cấu hình VPS</td>
                                @elseif ($detail_order->type == 'upgrade_hosting')
                                  <td>Nâng cấp Hosting</td>
                                @else
                                  <td>Tạo {{ $type_invoice[$detail_order->type] }}</td>
                                @endif
                              @endif
                            @else
                              <td></td>
                            @endif
                            @if (!empty($detail_order->quantity))
                              <td>{{ $detail_order->quantity }}</td>
                            @else
                              <td>0</td>
                            @endif
                        @endforeach
                        {{-- <td>{{ $order->detail_orders->where('order_id', $order->id)->get()->type }}</td> --}}
                        {{-- <td>{{ $order->detail_orders->where('order_id', $order->id)->first()->quantity }}</td> --}}
                        <td>{!!number_format($order->total,0,",",".") . ' VNĐ'!!}</td>
                        <td class="status_order">
                            @if ($order->status == 'Active')
                            <span class="text-info">Đang chờ tạo</span>
                            @elseif ($order->status == 'Pending')
                            <span class="text-danger">Chưa thanh toán</span>
                            @elseif ($order->status == 'Finish')
                            <span class="text-success">Hoàn thành</span>
                            @elseif ($order->status == 'Cancel')
                            <span class="text-danger">Đã hủy</span>
                            @else
                            <span class="text-danger">{{ $order->status }}</span>
                            @endif
                        </td>
                        <td class="button-action">
                            <button type="button" name="button" class="btn btn-sm detail_invoice btn btn-warning" data-id="{{ !empty($order->detail_orders[0]->id) ? $order->detail_orders[0]->id : 0 }}" data-toggle="tooltip" title="Chi tiết đơn hàng"><i class="fas fa-edit text-white"></i></button>
                            @if($order->status == 'Pending')
                                <button class="btn btn-sm btn-danger cancel-order" data-id="{{ $order->id }}" data-n="{{$key}}"
                                data-toggle="tooltip" title="Hủy đơn hàng"><i class="fas fa-trash-alt"></i></button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <td colspan="7" class="text-center">Quý khách không có đơn đặt hàng.</td>
                    @endif
                </tbody>
                <tfoot>
                    <td colspan="7" class="text-center">
                        {{ $orders->links() }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
{{-- Modal hủy order --}}
<div class="modal fade" id="cancel-order">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Hủy đơn đặt hàng</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="notication-order" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                <input type="submit" class="btn btn-primary" id="button-order" value="Hủy đơn hàng">
                <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="detail_invoice">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Chi tiết đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-detail">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Đóng</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        //hiện tooltip
        $('[data-toggle="tooltip"]').tooltip();
        //Hiện modal
        $('.cancel-order').on('click', function () {
            $('#cancel-order').modal('show');
            var id = $(this).attr("data-id");
            var n = $(this).attr("data-n");
            var html = "Bạn có muốn hủy đơn hàng (<b class='text-danger'>ID: " + id + "</b>) này không?";
            $('#button-order').attr('data-id', id);
            $('#button-order').attr('data-n', n);
            $('#notication-order').html(html);
            $('#button-order').fadeIn();
            $('#button-finish').fadeOut();
            $(this).closest('tr').addClass('removeRow');
        });
        //Xác nhận hủy
        $('#button-order').on('click', function() {
            $('<tr>').removeClass('removeRow');
            var id = $(this).attr('data-id');
            var n = $(this).attr('data-n');
            $.ajax({
                type: "get",
                url: "{{ route('user.order.cancel') }}",
                data: {id: id},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-order').attr('disabled', true);
                    $('#notication-order').html(html);
                },
                success: function (data) {
                    if (data) {
                        var html = '<p class="text-danger">Hủy đơn đặt hàng thành công</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        $('.removeRow').fadeOut();
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                    } else {
                        var html = '<p class="text-danger>Hủy đơn đặt hàng thất bại</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        // $('tr .removeRow').removeClass('removeRow');
                    }
                    $('#button-order').hide();
                    $('#button-finish').fadeIn();
                },
                error: function (e) {
                    console.log(e);
                    var html = '<p class="text-danger">Hủy đơn đặt hàng bị lỗi!</p>';
                    $('#notication-order').html(html);
                    $('#button-order').attr('disabled', false);
                    $('<tr>').removeClass('removeRow');
                    $('#button-order').hide();
                    $('#button-finish').fadeIn();
                }
            });
        });

        $(document).on('click', '.detail_invoice', function () {
            $('#detail_invoice').modal('show');
            var id = $(this).attr('data-id');
            $.ajax({
              url: '/orders/detail_invoice?id=' + id,
              dataType: 'json',
              beforeSend: function(){
                  var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                  $('#notication-detail').html(html);
              },
              success: function (data) {
                  // console.log(data);
                  var html = '';
                  // Thông tin paid
                  html += '<div class="detail_order text-center">';
                  html += '<div class="paid">';
                  if (data.status == 'paid') {
                    html += '<span class="text-success">Đã thanh toán</span>';
                  } else if (data.status == 'unpaid') {
                    html += '<span class="text-danger">Chưa thanh toán</span>';
                  } else {
                    html += '<span class="text-default">Hủy</span>';
                  }
                  html += '</div>';
                  html += '<div class="date_paid">';
                  if (data.paid_date != '') {
                    html += '<b class="text-success">' + data.paid_date + '</b>';
                  } else {
                    html += '<b class="text-danger">Chưa thanh toán</b>';
                  }
                  html += '</div>';
                  html += '<div class="date_due">';
                  html += 'Ngày đặt hàng: <b>' + data.date_create + '</b>';
                  html += '</div>';
                  html += '<div class="date_due">';
                  if (data.status == 'unpaid') {
                    html += 'Ngày hết hạn: <b>' + data.due_date + '</b>';
                  }
                  html += '</div>';
                  html += '</div>';
                  // Thông tin đơn đặt hàng
                  html += '<div class="detail_order">';
                  html += '<div>';
                  html += '<h4>Thông tin dịch vụ / sản phẩm</h4>';
                  html += '</div>';
                  html += '<div class="m-3">';
                  html += '<table class="table table-hover table-bordered">';
                  if (data.type_order == 'expired') {
                    html += '<thead class="primary">';
                    html += '<th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data.services , function(index, service) {
                        html += '<tr>';
                        html += '<td>';
                        if (data.type == 'VPS') {
                          html += 'Gia hạn VPS - ';
                          html += '<a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.sevice_name +'</a>';
                        }
                        else if (data.type == 'VPS US') {
                          html += 'Gia hạn VPS US - ';
                          html += '<a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.sevice_name +'</a>';
                        }
                        else if (data.type == 'hosting') {
                          html += 'Gia hạn Hosting - ';
                          html += '<a href="/service/detail/'+ service.sevice_id +'?type=hosting">'+ service.sevice_name +'</a>';
                        }
                        else if (data.type == 'Domain') {
                          html += 'Gia hạn Domain - ';
                          html += '<a href="'+ service.sevice_name +'">'+ service.sevice_name +'</a>';
                        }
                        html += '</td>';
                        html += '<td>';
                        html += service.config;
                        html += '</td>';
                        html += '<td>';
                        html += data.date_create;
                        html += '</td>';
                        html += '<td>';
                        html += service.billing_cycle;
                        html += '</td>';
                        html += '<td>';
                        html += service.amount;
                        html += '</td>';
                        html += '</tr>';
                    });
                    html += '</tbody>';
                  }
                  else if (data.type_order == 'addon_vps') {
                    html += '<thead class="primary">';
                    html += '<th>IP</th><th>CPU</th><th>RAM</th><th>DISK</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data.services , function(index, service) {
                      html += '<tr>';
                      html += '<td>';
                      html += 'Addon VPS - ';
                      html += '<a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.sevice_name +'</a>';
                      html += '</td>';
                      html += '<td>' + service.cpu + '</td>';
                      html += '<td>' + service.ram + '</td>';
                      html += '<td>' + service.disk + '</td>';
                      html += '<td>';
                      html += data.date_create;
                      html += '</td>';
                      html += '<td>';
                      html += service.time;
                      html += '</td>';
                      html += '<td>';
                      html += service.amount;
                      html += '</td>';
                      html += '</tr>';
                    });
                    html += '</tbody>';
                  }
                  else if (data.type_order == 'change_ip') {
                    html += '<thead class="primary">';
                    html += '<th>IP</th><th>Ngày đặt hàng</th><th>Giá</th>';
                    html += '</thead>';
                    html += '<tbody>';
                    $.each(data.services , function(index, service) {
                      html += '<tr>';
                      html += '<td>';
                      html += 'Đổi IP VPS '+ service.sevice_name;
                      if (service.changed_ip != '') {
                          html += ' - <a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.changed_ip +'</a>';
                      }
                      html += '</td>';
                      html += '<td>';
                      html += data.date_create;
                      html += '</td>';
                      html += '<td>';
                      html += service.amount;
                      html += '</td>';
                      html += '</tr>';
                    });
                    html += '</tbody>';
                  }
                  else if (data.type_order == 'upgrade_hosting') {
                    html += '<thead class="primary">';
                    html += '<th>Hosting</th><th>Nâng cấp gói Hosting</th><th>Ngày đặt hàng</th><th>Giá</th>';
                    html += '</thead>';
                    html += '<tbody>';
                    html += '<tr>';
                    html += '<td>';
                    html += 'Nâng cấp Hosting - ';
                    html += '<a href="/service/detail/'+ data.services.sevice_id +'?type=hosting">'+ data.services.sevice_name +'</a>';
                    html += '</td>';
                    html += '<td>' + data.services.upgrade + '</td>';
                    html += '<td>';
                    html += data.date_create;
                    html += '</td>';
                    html += '<td>';
                    html += data.services.amount;
                    html += '</td>';
                    html += '</tr>';
                    html += '</tbody>';
                  }
                  else {
                    // console.log(data);
                    if (data.type == 'VPS-US') {
                      html += '<thead class="primary">';
                      html += '<th>Dịch vụ</th><th>Bang</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                      html += '</thead>';
                      html += '<tbody>';
                      $.each(data.services , function(index, service) {
                      // console.log(service);
                          html += '<tr>';
                          html += '<td>';
                          html += 'Tạo VPS US - ';
                          html += '<a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.sevice_name +'</a>';
                          html += '</td>';
                          html += '<td>';
                          html += service.state;
                          html += '</td>';
                          html += '<td>';
                          html += service.config;
                          html += '</td>';
                          html += '<td>';
                          html += data.date_create;
                          html += '</td>';
                          html += '<td>';
                          html += service.billing_cycle;
                          html += '</td>';
                          html += '<td>';
                          html += service.amount;
                          html += '</td>';
                          html += '</tr>';
                      });
                      html += '</tbody>';
                    } else {
                      html += '<thead class="primary">';
                      html += '<th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                      html += '</thead>';
                      html += '<tbody>';
                      $.each(data.services , function(index, service) {
                          html += '<tr>';
                          html += '<td>';
                          if (data.type == 'VPS' || data.type == 'NAT-VPS') {
                            html += 'Tạo '+ data.type +' - ';
                            html += '<a href="/service/detail/'+ service.sevice_id +'?type=vps">'+ service.sevice_name +'</a>';
                          } else if (data.type == 'Hosting' || data.type == 'Hosting-Singapore') {
                            html += 'Tạo '+ data.type +' - ';
                            html += '<a href="/service/detail/'+ service.sevice_id +'?type=hosting">'+ service.sevice_name +'</a>';
                          } else if (data.type == 'Domain') {
                            html += 'Tạo Domain - ';
                            html += '<a href="'+ service.sevice_name +'">'+ service.sevice_name +'</a>';
                          }
                          html += '</td>';
                          html += '<td>';
                          html += service.config;
                          html += '</td>';
                          html += '<td>';
                          html += data.date_create;
                          html += '</td>';
                          html += '<td>';
                          html += service.billing_cycle;
                          html += '</td>';
                          html += '<td>';
                          html += service.amount;
                          html += '</td>';
                          html += '</tr>';
                      });
                      html += '</tbody>';
                    }
                  }
                  html += '</table>';
                  html += '</div>';
                  html += '</div>';
                  $('#notication-detail').html(html);
              },
              error: function (e) {
                console.log(e);
                $('#notication-detail').html('<div  class="text-center"><span>Lỗi truy vấn đơn hàng!</span></div>');
              },
            });
        });

    });

</script>
@endsection
