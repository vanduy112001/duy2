@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Chuyển VPS cho khách hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS</li>
<li class="breadcrumb-item active"> Chuyển VPS</li>
@endsection
@section('content')
<div class="row mt-4">
    <div class="box box-primary col col-md-8"  style="margin:auto;min-height: 500px;">
        <div class="detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        <div class="form-changer-user m-4">
            <form action="{{ route('service.vps.form_change_user_by_vps') }}" method="post" id="form_change">
              @csrf()
              <div class="form-group row">
                <div class="col-md-12 mb-3">
                  <h5>Danh sách VPS:</h5>
                </div>
                @foreach( $list_vps as $key => $vps )
                  <div class="col-md-3">
                    <b>{{ $key + 1 }}.</b> {{ $vps->ip }}
                    <input type="hidden" name="list_vps[]" value="{{ $vps->id }}">
                  </div>
                @endforeach
              </div>
              <div class="form-group">
                <label for="email">Email khách hàng: </label>
                {{-- style="width: 50%" --}}
                <div class="input-group mb-3" > 
                  <input type="text" placeholder="Nhập email của khách hàng muốn chuyển và kiểm tra" name="email" id="email" value="" tabindex="1" class="form-control" />
                  <div class="input-group-append">
                    <button type="button" class="btn btn-outline-success" id="btn_check_user">Kiểm tra</button>
                  </div>
                </div>
                <div id="check_email" class="mb-3">
                    
                </div>
              </div>
              <div class="text-center">
                <input type="hidden" name="check_user" id="check_user" value="0">
                <input type="hidden" name="type" id="check_user" value="vn">
                <button type="button" id="btn_submit" class="btn btn-primary" >Xác nhận</button>
                <a href="{{ route('service.vps.on') }}" class="btn btn-default">Hủy</a>
              </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#btn_check_user').on('click', function(event) {
      event.preventDefault();
      $('#check_user').val(0);
      /* Act on the event */
      var email = $('#email').val();      
      $.ajax({
        url: '/users/check_email',
        data: {email: email},
        dataType: 'json',
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('#btn_check_user').attr('disabled', true);
          $('#check_email').html(html);
        },
        success: function (data) {
            // console.log(data);
          if(data.error == 0) {
            $('#check_user').val(1);
            var html = '<h5>Thông tin khách hàng: </h5>';
            html += '<b class="ml-2"> - Họ và tên: </b> ' + data.user.name + '<br>';
            html += '<b class="ml-2"> - Email: </b> ' + data.user.email + '<br>'; 
          } else {
            if ( data.error == 1 || data.error == 2 ) {
              var html = '<p class="text-danger">Không có khách hàng này trong dữ liệu. Quý khách vui lòng kiểm tra lại email hoặc tạo tài khoản bằng email này.</p>';
            }
            else {
              var html = '<p class="text-danger">Lỗi chuyển VPS. Quý khách không thể chuyển các VPS này cho cùng một tài khoản. Quý khách vui lòng kiểm tra lại.</p>';
            }
          }
          $('#check_email').html(html);
          $('#btn_check_user').attr('disabled', false);
        },
        error: function (e) {
          console.log(e); 
          var html = "<p class='text-danger'>Truy vấn khách hàng lỗi!</p>";
          $('#check_email').html(html);
          $('#btn_check_user').attr('disabled', false);
        }
      });
    });

    $('#btn_submit').on('click', function(event) {
      // console.log("da click");
      event.preventDefault();
      $(this).attr('disabled', true);
      /* Act on the event */
      if ( $('#check_user').val() != "0" ) {
        $(this).closest( "form" ).submit();
      } else {
        $(this).attr('disabled', false);
        alert('Quý khách vui lòng kiểm tra khách hàng muốn chuyển VPS.');
      }
    });
  });
</script>
@endsection
