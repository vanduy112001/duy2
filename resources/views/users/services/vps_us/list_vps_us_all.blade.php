@php
use Carbon\Carbon;
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    Tất cả dịch vụ VPS US
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS US</li>
<li class="breadcrumb-item active"> Tất cả dịch vụ</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <div class="button-list">
        </div>
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div class="list_all_vps text_list_vps" id="vps">
            <div class="box box-default">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 my-2">
                            <div class="row">
                                <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                    <select class="form-control form-control-sm vps_all_classic">
                                      <option value="20" selected>20</option>
                                      <option value="30">30</option>
                                      <option value="40">40</option>
                                      <option value="50">50</option>
                                      <option value="100">100</option>
                                    </select>
                                </div>
    
                                <div class="col-md-6 mb-2 px-1 ml-auto">
                                    <div class="input-group input-group-sm mb-0 mt-0">
                                        <input type="text" id="form_search_multi" class="form-control input-sm" placeholder="Tìm VPS theo 1 hoặc nhiều IP">
                                        <div class="input-group-append">
                                            <button id="btn_all_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
    
                                {{-- <div class="col-md-6 mt-2 title_list_vps">
                                    <span class="ml-3">Số lượng: </span>
                                    <select class="vps_all_classic">
                                    <option value="20" selected>20 VPS</option>
                                    <option value="30">30 VPS</option>
                                    <option value="40">40 VPS</option>
                                    <option value="50">50 VPS</option>
                                    <option value="100">100 VPS</option>
                                    </select>
                                </div> --}}
    
                                {{-- <div class="col-md-4 mb-3">
                                    <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Tìm kiếm VPS" id="all_vps_search">
                                    </div>
                                </div>
                                <div class="col-md-2 mt-2">
                                    <button class="btn btn-danger" type="button"  id="btn_all_multi">Tìm kiếm nhiều</button>
                                </div>
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <div class="mb-3" id="all_search_multi"></div>
                                </div> --}}
                            </div>
                        </div>

                        <table class="table table-bordered table-hover text-center">
                            <thead class="">
                                <th width="12%">IP</th>
                                <th width="15%">Cấu hình</th>
                                <th width="5%">Bang</th>
                                <th width="7%">Ngày tạo</th>
                                <th width="13%" class="sort_next_due_date_all_vps" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                    Ngày kết thúc <i class="fas fa-sort"></i>
                                    <input type="hidden" class="sort_type" value="">
                                </th>
                                <th>Ngày kết thúc</th>
                                <th>Tổng thời gian thuê</th>
                                <th width="7%">Ghi chú</th>
                                <th width="7%">Trạng thái</th>
                            </thead>
                            <tbody>
                                @if($list_vps->count() > 0)
                                    @foreach ($list_vps as $vps)
                                        @if ( !empty($vps->status_vps))
                                          @php
                                              $isExpire = false;
                                              $expired = false;
                                              if(!empty($vps->next_due_date)){
                                                  $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                                                  $date = date('Y-m-d');
                                                  $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                                  if($next_due_date <= $date) {
                                                    $isExpire = true;
                                                  }
                                                  $date_now = strtotime(date('Y-m-d'));
                                                  if ($next_due_date < $date_now) {
                                                      $expired = true;
                                                  }
                                              }
                                              $total_time = '';
                                              $create_date = new Carbon($vps->date_create);
                                              $next_due_date = new Carbon($vps->next_due_date);
                                              if ( $next_due_date->diffInYears($create_date) ) {
                                                $year = $next_due_date->diffInYears($create_date);
                                                $total_time = $year . ' Năm ';
                                                $create_date = $create_date->addYears($year);
                                                $month = $next_due_date->diffInMonths($create_date);
                                                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                if ( $month ) {
                                                    $total_time .= $month . ' Tháng';
                                                }
                                              } else {
                                                $diff_month = $next_due_date->diffInMonths($create_date);
                                                $total_time = $diff_month . ' Tháng';
                                              }
                                          @endphp
                                              <tr>
                                                  <td class="ip_vps" data-ip="{{ !empty($vps->ip) ? $vps->ip : '' }}">
                                                      @if (!empty($vps->ip))
                                                          <a href="{{ route('services.detail', $vps->id) }}?type=vps">{{$vps->ip}}</a>
                                                          @if($expired)
                                                            - <span class="text-danger">Hết hạn</span>
                                                          @elseif($isExpire)
                                                            - <span class="text-danger">Gần hết hạn</span>
                                                          @endif
                                                      @else
                                                          <span class="text-danger">Chưa tạo</span>
                                                      @endif
                                                  </td>
                                                  <td>
                                                    @php
                                                      $product = $vps->product;
                                                      $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                      $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                      $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                      // Addon

                                                      if (!empty($vps->vps_config)) {
                                                          $vps_config = $vps->vps_config;
                                                          if (!empty($vps_config)) {
                                                              $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                              $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                              $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                          }
                                                      }
                                                    @endphp
                                                    {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk
                                                  </td>
                                                  <td>
                                                      @if( !empty($vps->state) )
                                                        {{ $vps->state }}
                                                      @else
                                                        VPS US
                                                      @endif
                                                  </td>
                                                  {{--<td>
                                                      @if(!empty($vps->ma_kh))
                                                          @if(!empty($vps->customer))
                                                              @if(!empty($vps->customer->customer_name))
                                                                {{ $vps->customer->ma_customer }} - {{ $vps->customer->customer_name }}
                                                              @else
                                                                {{ $vps->customer->ma_customer }} - {{ $vps->customer->customer_tc_name }}
                                                              @endif
                                                          @else
                                                              Chính tôi
                                                          @endif
                                                      @else
                                                          @php
                                                              $customer = GroupProduct::get_customer_with_service($vps->id, 'vps');
                                                          @endphp
                                                          @if(!empty($customer))
                                                              @if(!empty($customer->customer_name))
                                                                {{ $customer->ma_customer }} - {{ $customer->customer_name }}
                                                              @else
                                                                {{ $customer->ma_customer }} - {{ $customer->customer_tc_name }}
                                                              @endif
                                                          @else
                                                              Chính tôi
                                                          @endif
                                                      @endif
                                                      <span>
                                                        <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                        <a href="#" class="text-secondary ml-2 button_edit_customer" data-id="{{ $vps->id }}" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a>
                                                      </span>
                                                  </td>--}}
                                                  <td>
                                                      @if (!empty($vps->date_create))
                                                          <span>{{ date('d-m-Y', strtotime($vps->date_create)) }}</span>
                                                      @else
                                                          <span class="text-danger">Chưa tạo</span>
                                                      @endif
                                                  </td>
                                                  <td class="next_due_date">
                                                      @if (!empty($vps->next_due_date))
                                                          <span class="@if($isExpire || $expired) text-danger @endif" >{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                                      @else
                                                          <span class="text-danger">Chưa tạo</span>
                                                      @endif
                                                  </td>
                                                  <td>
                                                    {{ $total_time }}
                                                  </td>
                                                  <td>
                                                      {{ $billing[$vps->billing_cycle] }}
                                                  </td>
                                                  <td>
                                                      <span class="text-description">
                                                        @if(!empty($vps->description))
                                                            {{ substr($vps->description, 0, 40) }}
                                                        @endif
                                                      </span>
                                                      <span>
                                                        <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                        <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $vps->id }}" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a>
                                                      </span>
                                                  </td>
                                                  <td class="vps-status">
                                                      @if(!empty($vps->status_vps))
                                                          @if ($vps->status_vps == 'on')
                                                            <span class="text-success" data-id="{{ $vps->id }}">Đang bật</span>
                                                          @elseif ($vps->status_vps == 'progressing')
                                                            <span class="vps-progressing" data-id="{{ $vps->id }}">Đang tạo ...</span>
                                                          @elseif ($vps->status_vps == 'rebuild')
                                                            <span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>
                                                          @elseif ($vps->status_vps == 'change_ip')
                                                            <span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>
                                                          @elseif ($vps->status_vps == 'expire')
                                                              <span class="text-danger" data-id="{{ $vps->id }}">Đã hết hạn</span>
                                                          @elseif ($vps->status_vps == 'suspend')
                                                              <span class="text-danger" data-id="{{ $vps->id }}">Đang bị khoá</span>
                                                          @elseif ($vps->status_vps == 'admin_off')
                                                            <span class="text-danger" data-id="{{ $vps->id }}">Admin tắt</span>
                                                          @elseif ($vps->status_vps == 'delete_vps')
                                                            <span class="text-danger" data-id="{{ $vps->id }}">Đã xóa</span>
                                                          @elseif ($vps->status_vps == 'change_user')
                                                            <span class="text-danger" data-id="{{ $vps->id }}">Đã chuyển</span>
                                                          @else
                                                            <span class="text-danger" data-id="{{ $vps->id }}">Đã tắt</span>
                                                          @endif
                                                      @else
                                                          <span class="text-danger" data-id="{{ $vps->id }}">Chưa được tạo</span>
                                                      @endif
                                                  </td>
                                              </tr>
                                            @endif
                                        @endforeach
                                    @else
                                        <td class="text-center text-danger" colspan="10">
                                            Không có dịch vụ VPS được sử dụng
                                        </td>
                                    @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                                <td colspan="10 " class="text-center link-right">
                                    {{ $list_vps->links()  }}
                                </td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service_vps_us.js') }}?token={{ date('YmdH') }}"></script>
@endsection
