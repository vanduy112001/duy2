@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Xuất FILE EXCEL cho VPS US</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS US</li>
<li class="breadcrumb-item active"> Xuất FILE EXCEL</li>
@endsection
@section('content')
<div class="row mt-4">
    <div class="box box-primary col col-md-8"  style="margin:auto;min-height: 500px;">
        <div class="detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        <div class="form-changer-user m-4">
            <div id="notification">
              
            </div>
            <form action="{{ route('service.vps.form_export_excel_by_vps_us') }}" method="post" id="form_export">
              @csrf()
              <div class="form-group row">
                <div class="col-md-12 mb-3">
                  <h5>Danh sách VPS US:</h5>
                </div>
                @foreach( $list_vps as $key => $vps )
                  <div class="col-md-3">
                    <b>{{ $key + 1 }}.</b> {{ $vps->ip }}
                    <input type="hidden" name="list_vps[]" value="{{ $vps->id }}">
                  </div>
                @endforeach
              </div>
              <div class="form-group">
                <label for="">Tên file</label>
                <input type="text" name="file_name" class="form-control" placeholder="Tên file">
              </div>
              <div class="form-group">
                <label for="">Chọn thuộc tính xuất excel</label>
                <div class="form-check">
                  <input class="form-check-input" name="ip" value="1" type="checkbox" checked>
                  <label class="form-check-label">Địa chỉ IP</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" name="state" value="1" type="checkbox" checked>
                  <label class="form-check-label">Bang</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" name="config" value="1" type="checkbox" checked>
                  <label class="form-check-label">Cấu hình</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" name="due_date" value="1" type="checkbox" checked>
                  <label class="form-check-label">Ngày kết thúc</label>
                </div>
                <div class="form-check">
                  <input class="form-check-input" name="password" value="1" type="checkbox" checked>
                  <label class="form-check-label">Mật khẩu</label>
                </div>
              </div>
              <div class="text-center">
                <a href="{{ route('service.vps.on') }}" class="btn btn-default">Hủy</a>
                <button type="submit" id="btn_submit" class="btn btn-primary ml-4" >Xác nhận</button>
              </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#form_export').on('submit', function(event) {
      event.preventDefault();
      var form = $(this).serialize();
      $.ajax({
        url: '/dich-vu/vps/form_export_excel_by_vps_us',
        type: 'post',
        dataType: 'json',
        data: form,
        success: function (data) {
          if (!data.error) {
            $('#export_excel').attr('href', data.content);
            var a = document.createElement('a');
            a.href = data.content;
            a.download = data.file_name;
            document.body.append(a);
            a.click();
            a.remove();

          } else {
            $('#notification').html(`
              <div class="bg-danger mt-4 mb-4 p-4">
                ${data.content}
              </div>
            `);
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<div class="bg-danger mt-4 mb-4 p-4">Lỗi truy xuất VPS</div>';
          $('#notification').html(html);
       }
      })
    })
  });
</script>
@endsection