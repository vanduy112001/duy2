@extends('layouts.user.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Tất cả dịch vụ</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-server"></i> Tất cả dịch vụ</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <div class="button-list">
            <div class="menu-vps">
                <button class="btn btn-primary btn-list-vps btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                    <span class="text">List VPS</span>
                </button>
            </div>
            <div class="menu-hosting">
                <button class="btn btn-danger btn-list-hosting btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-globe" aria-hidden="true"></i></span>
                    <span class="text">List Hosting</span>
                </button>
            </div>
            <div class="menu-server">
                <button class="btn btn-info btn-list-server btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-server" aria-hidden="true"></i> </span>
                    <span class="text">List Server</span>
                </button>
            </div>
        </div>
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        @include('users.services.includes.all-list-vps',compact('list_vps'))
        {{-- Kết thúc list VPS --}}
        {{-- List Hosting --}}
        @include('users.services.includes.all-list-hosting',compact('hostings'))
        {{-- Kết thúc list Hosting --}}
        {{-- List Server --}}
        @include('users.services.includes.list-server',compact('servers'))
        {{-- Kết thúc list Server --}}
    </div>
</div>
<!-- <div id="float-menu">
    <div class="menu-vps">
        <button class="btn btn-primary btn-list-vps"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="service-name">VPS</span></button>
    </div>
    <div class="menu-hosting">
        <button class="btn btn-danger btn-list-hosting"><i class="fa fa-globe" aria-hidden="true"></i> <span class="service-name">Hosting</span></button>
    </div>
    <div class="menu-server">
        <button class="btn btn-info btn-list-server"><i class="fa fa-server" aria-hidden="true"></i> <span class="service-name">Server</span></button>
    </div>
</div> -->
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}"></script>
@endsection
