@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ Email Hosting đang sử dụng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ Email Hosting</li>
<li class="breadcrumb-item active"> Đang sử dụng</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <!-- <div class="button-list">
            <div class="menu-vps">
                <button class="btn btn-primary btn-list-vps btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                    <span class="text">List VPS</span>
                </button>
            </div>
            <div class="menu-hosting">
                <button class="btn btn-danger btn-list-hosting btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-globe" aria-hidden="true"></i></span>
                    <span class="text">List Hosting</span>
                </button>
            </div>
            <div class="menu-server">
                <button class="btn btn-info btn-list-server btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-server" aria-hidden="true"></i> </span>
                    <span class="text">List Server</span>
                </button>
            </div>
        </div> -->
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div class="list_email_hosting" id="email_hosting">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <h3>Dịch vụ Email Hosting</h3>
                        <div class="col-md-12 row">
                          <div class="col-md-8 mt-2 title_list_vps">
                            <span class="ml-3">Số lượng: </span>
                            <select class="email_hosting_nearly">
                              <option value="20" selected>20</option>
                              <option value="30">30</option>
                              <option value="40">40</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                            </select>
                            <input type="hidden" id="service_action">
                          </div>
                          <div class="col-md-4 mb-3">
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fas fa-search"></i></span>
                              </div>
                              <input type="text" class="form-control" placeholder="Tìm kiếm Email Hosting" id="search_email_hosting_nearly">
                            </div>
                          </div>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead class="info">
                                <th>Tên sản phẩm</th>
                                <th>Domain</th>
                                <th>Ngày tạo</th>
                                <th>Ngày kết thúc</th>
                                <th>Thời gian thuê</th>
                                <th>Chi phí</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                            </thead>
                            <tbody>
                            @if($email_hostings->count() > 0)
                                @foreach ($email_hostings as $email_hosting)
                                    @php
                                        $isExpire = false;
                                        $expired = false;
                                        if(!empty($email_hosting->next_due_date)){
                                            $next_due_date = strtotime(date('Y-m-d', strtotime($email_hosting->next_due_date)));
                                            $date = date('Y-m-d');
                                            $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
                                            if($next_due_date <= $date) {
                                              $isExpire = true;
                                            }
                                            $date_now = strtotime(date('Y-m-d'));
                                            if ($next_due_date < $date_now) {
                                                $expired = true;
                                            }
                                        }
                                    @endphp
                                    <tr>
                                        <td>{{ $email_hosting->product->name }}</td>
                                        <td>
                                            @if (!empty($email_hosting->domain))
                                                <a href="{{ route( 'service.email_hosting_detail', $email_hosting->id ) }}">{{$email_hosting->domain}}</a>
                                                @if($expired)
                                                  - <span class="text-danger">Hết hạn</span>
                                                @elseif($isExpire)
                                                  - <span class="text-danger">Gần hết hạn</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                        </td>
                                        <td>
                                          @if (!empty($email_hosting->date_create))
                                              <span>{{ date('d-m-Y', strtotime($email_hosting->date_create)) }}</span>
                                          @else
                                              <span class="text-danger">Chưa tạo</span>
                                          @endif
                                        </td>
                                        <td>
                                          @if (!empty($email_hosting->next_due_date))
                                              <span>{{ date('d-m-Y', strtotime($email_hosting->next_due_date)) }}</span>
                                          @else
                                              <span class="text-danger">Chưa tạo</span>
                                          @endif
                                        </td>
                                        <td>
                                            {{ $billing[$email_hosting->billing_cycle] }}
                                        </td>
                                        <td>
                                          <?php
                                             $price_hosting = 0;
                                             if ( !empty($email_hosting->price_override) ) {
                                                $price_hosting = $email_hosting->price_override;
                                             } else {
                                                $price_hosting = !empty($email_hosting->detail_order->sub_total) ? $email_hosting->detail_order->sub_total : 0;

                                             }
                                          ?>
                                          <b>{!!number_format( $price_hosting ,0,",",".")!!} VNĐ</b>
                                        </td>
                                        <td class="email_hosting_status">
                                            @if ($email_hosting->status_hosting == 'on')
                                                <span class="text-success">Đang bật</span>
                                            @elseif ($email_hosting->status_hosting == 'off')
                                                <span class="text-danger">Đã tắt</span>
                                            @elseif ($email_hosting->status_hosting == 'cancel')
                                                <span class="text-danger">Đã hủy</span>
                                            @elseif ($email_hosting->status_hosting == 'delete_colo')
                                                <span class="text-danger">Đã xóa</span>
                                            @else
                                                <span class="text-secondary">Đã hủy</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if ($email_hosting->status_hosting != 'suspend')
                                                @if ($isExpire || $expired)
                                                    <button type="button" class="btn btn-warning button-action-email_hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Email Hosting" data-action="expired" data-id="{{ $email_hosting->id }}" data-ip="{{ $email_hosting->domain }}"><i class="fas fa-plus-circle"></i></button>
                                                @endif
                                                @if ($email_hosting->status_hosting != 'expire')
                                                  @if($email_hosting->status_hosting != 'cancel')
                                                      <button class="btn btn-outline-danger button-action-email_hosting terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Email Hosting" data-action="terminated" data-id="{{ $email_hosting->id }}" data-ip="{{ $email_hosting->domain }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                  @endif
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="9">
                                    Không có dịch vụ Email Hosting được sử dụng
                                </td>
                            @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                                <td colspan="9" class="text-center link-right">
                                    {{ $email_hostings->links()  }}
                                </td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
<!-- <div id="float-menu">
    <div class="menu-vps">
        <button class="btn btn-primary btn-list-vps"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="service-name">VPS</span></button>
    </div>
    <div class="menu-hosting">
        <button class="btn btn-danger btn-list-hosting"><i class="fa fa-globe" aria-hidden="true"></i> <span class="service-name">Hosting</span></button>
    </div>
    <div class="menu-server">
        <button class="btn btn-info btn-list-server"><i class="fa fa-server" aria-hidden="true"></i> <span class="service-name">Server</span></button>
    </div>
</div> -->
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button_email_hosting_terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button_email_hosting_rebuil" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_email_hosting_service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}"></script>
@endsection
