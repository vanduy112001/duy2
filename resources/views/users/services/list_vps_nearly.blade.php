@php
use Carbon\Carbon;
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    Dịch vụ VPS hết hạn
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS</li>
<li class="breadcrumb-item active"> Hết hạn / Hủy</li>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    @php
        $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
    @endphp
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-primary btn-sm">Tạo VPS</a>
      @endif
    @endforeach
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'nat_vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-success  btn-sm">Tạo NAT VPS</a>
      @endif
    @endforeach
  </div>
</div>
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div id="custom-tabs-two-tabContent" class="tab-content">
          <div class="list_vps_nearly text_list_vps tab-pane fade show active" id="vps" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
              <div class="box box-default">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="col-md-12 detail-course-finish">
                                @if(session("success"))
                                <div class="bg-success">
                                    <p class="text-light">{{session("success")}}</p>
                                </div>
                                @elseif(session("fails"))
                                <div class="bg-danger">
                                    <p class="text-light">{{session("fails")}}</p>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 my-2">                        
                                <div class="row">
                                    {{-- <div class="col-md-6 mt-2 title_list_vps">
                                    <h3>Dịch vụ VPS hết hạn</h3>
                                    <span class="ml-3">Số lượng: </span>
                                    <select class="vp_nearly_classic">
                                        <option value="20" selected>20 VPS</option>
                                        <option value="30">30 VPS</option>
                                        <option value="40">40 VPS</option>
                                        <option value="50">50 VPS</option>
                                        <option value="100">100 VPS</option>
                                    </select>
                                    </div> --}}

                                    <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                        <select class="form-control form-control-sm vp_nearly_classic">
                                          <option value="20" selected>20</option>
                                          <option value="30">30</option>
                                          <option value="40">40</option>
                                          <option value="50">50</option>
                                          <option value="100">100</option>
                                          <option value="200">200</option>
                                          <option value="500">500</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                                        <div class="input-group input-group-sm m-0">
                                            <select class="custom-select list_action_nearly" name="" id="">
                                                <option value="" selected disabled>Chọn hành động</option>
                                                <option value="expired">Gia hạn VPS</option>
                                                <option value="change_user">Chuyển khách hàng</option>
                                                <option value="export_vps">Xuất file excel</option>
                                            </select>
                                        </div>
                                    </div>

                                    {{-- <div class="col-md-4 mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                                        </div>
                                        <input type="text" class="form-control" placeholder="Tìm kiếm VPS" id="vps_nearly_search">
                                    </div>
                                    </div>
                                    <div class="col-md-2 mt-2">
                                    <button class="btn btn-danger" type="button"  id="btn_nearly_multi">Tìm kiếm nhiều</button>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6">
                                        <div class="mb-3" id="nearly_search_multi">
                                        </div>
                                    </div> --}}

                                    <div class="col-md-6 mb-2 px-1 ml-auto">
                                        <div class="input-group input-group-sm mb-0 mt-0">
                                            <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm VPS theo 1 hoặc nhiều IP">
                                            <div class="input-group-append">
                                                <button id="btn_nearly_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 text-primary">
                                        <b>
                                            Đã chọn: 
                                            <span class="qtt-checkbox">0</span> / <span class="total-item">{{ $list_vps->count() }}</span>
                                        </b>
                                    </div>
                                </div>
                          </div>

                          {{-- <div id="group_button">
                              <div class="table-responsive" style="display: block;">
                                  <button class="btn bg-gradient-secondary btn-action-vps-nearly btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                      <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                      <span class="text">Gia hạn VPS</span>
                                  </button>
                                  <button class="btn bg-purple btn_nearly_change_user btn-icon-split btn-sm mb-1" data-type="change_user">
                                      <span class="icon"><i class="fas fa-exchange-alt"></i></span>
                                      <span class="text"> Chuyển khách hàng</span>
                                  </button>
                                  <button class="btn bg-navy btn_nearly_export_vps btn-icon-split btn-sm mb-1" data-type="export_vps">
                                      <span class="icon"><i class="far fa-file-excel"></i></span>
                                      <span class="text"> Xuất file excel</span>
                                  </button>
                              </div>
                          </div> --}}

                          <table class="table table-sm table-bordered table-hover text-center">
                              <thead class="">
                                  <th width="3%"><input type="checkbox" class="vps_checkbox_nearly_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả IP VPS"></th>
                                  <th width="9%">IP</th>
                                  <th width="9%">Mật khẩu</th>
                                  <th width="9%">Cấu hình</th>
                                  {{-- <th width="5%">Loại</th> --}}
                                  <th width="7%">Ngày tạo</th>
                                  <th width="9%" class="sort_next_due_date_nearly" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                    Ngày kết thúc <i class="fas fa-sort"></i>
                                    <input type="hidden" class="sort_type" value="">
                                  </th>
                                  <th width="7%">Tổng thời gian thuê</th>
                                  <th width="7%">Thời gian thuê</th>
                                  <th width="7%">Chi phí (VNĐ)</th>
                                  <th width="5%">Ghi chú</th>
                                  <th width="5%" class="infoAuto text-center">
                                    Gia hạn
                                    <i class="fas fa-info-circle">
                                      <span class="auto_refurn_tooltip">
                                        Bật tính năng tự động gia hạn sẽ gia hạn tự động khi VPS gần hết hạn.
                                      </span>
                                    </i>
                                  </th>
                                  <th  width="5%">Trạng thái</th>
                                  <th  width="18%">Hành động</th>
                              </thead>
                              <tbody>
                                  @if(isset($list_vps))
                                      @foreach ($list_vps as $vps)
                                          @if ( !empty($vps->status_vps) )
                                            @if(!empty($vps->product))
                                                  @php
                                                    $isExpire = false;
                                                    $expired = false;
                                                    if(!empty($vps->next_due_date)){
                                                        $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                                                        $date = date('Y-m-d');
                                                        $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                                        $date_now = strtotime(date('Y-m-d'));
                                                        if ($next_due_date < $date_now) {
                                                            $expired = true;
                                                        }
                                                        $text_day = '';
                                                        $now = Carbon::now();
                                                        $next_due_date = new Carbon($vps->next_due_date);
                                                        $diff_date = $next_due_date->diffInDays($now);
                                                        if ( $next_due_date->isPast() ) {
                                                          $text_day = 'Hết hạn ' . $diff_date . ' ngày';
                                                        } else {
                                                          $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
                                                        }
                                                        $total_time = '';
                                                        $create_date = new Carbon($vps->date_create);
                                                        $next_due_date = new Carbon($vps->next_due_date);
                                                        if ( $next_due_date->diffInYears($create_date) ) {
                                                          $year = $next_due_date->diffInYears($create_date);
                                                          $total_time = $year . ' Năm ';
                                                          $create_date = $create_date->addYears($year);
                                                          $month = $next_due_date->diffInMonths($create_date);
                                                          //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                          if ( $month ) {
                                                              $total_time .= $month . ' Tháng';
                                                          }
                                                        } else {
                                                          $diff_month = $next_due_date->diffInMonths($create_date);
                                                          $total_time = $diff_month . ' Tháng';
                                                        }
                                                    }
                                                  @endphp
                                                  <tr>
                                                      <td><input type="checkbox" value="{{ $vps->id }}" data-ip="{{ $vps->ip }}" class="vps_nearly_checkbox"></td>
                                                      <td class="ip_vps" data-ip="{{ !empty($vps->ip) ? $vps->ip : '' }}">
                                                          @if (!empty($vps->ip))
                                                              <a href="{{ route('services.detail', $vps->id) }}?type=vps">{{$vps->ip}}</a>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>
                                                        <span class="pass_vps">{{ !empty($vps->password) ? $vps->password : '' }}</span>
                                                      </td>
                                                      <td>
                                                        @php
                                                          $product = $vps->product;
                                                          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                          // Addon

                                                          if (!empty($vps->addon_id)) {
                                                              $vps_config = $vps->vps_config;
                                                              if (!empty($vps_config)) {

                                                                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                              }
                                                          }
                                                        @endphp
                                                        {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk
                                                      </td>
                                                      <td>
                                                          @if (!empty($vps->date_create))
                                                              <span>{{ date('d-m-Y', strtotime($vps->date_create)) }}</span>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td class="next_due_date">
                                                          @if (!empty($vps->next_due_date))
                                                              <span class="text-danger" >{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                                              - <span class="text-danger">{{ $text_day }}</span>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>{{ $total_time }}</td>
                                                      <td>
                                                          {{ $billing[$vps->billing_cycle] }}
                                                      </td>
                                                      <td>
                                                         @php
                                                            $total = 0;
                                                            if ( !empty($vps->price_override) ) {
                                                               $total = $vps->price_override;
                                                            } else {
                                                                $product = $vps->product;
                                                                $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                                                                if (!empty($vps->vps_config)) {
                                                                  $addon_vps = $vps->vps_config;
                                                                  $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                                                                  $pricing_addon = 0;
                                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                                    if (!empty($add_on_product->meta_product->type_addon)) {
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                      }
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                      }
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                      }
                                                                    }
                                                                  }
                                                                  $total += $pricing_addon;
                                                                }
                                                            }
                                                         @endphp
                                                         {!!number_format( $total ,0,",",".")!!}
                                                      </td>
                                                      <td>
                                                          <span class="text-description">
                                                            @if(!empty($vps->description))
                                                                {{ substr($vps->description, 0, 40) }}
                                                            @endif
                                                          </span>
                                                          <span>
                                                            <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                            <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $vps->id }}"><i class="fas fa-edit"></i></a>
                                                          </span>
                                                      </td>
                                                      <td class="auto_refurn text-center">
                                                        <div class="form-group" style="padding-left: 10px;" @if( !empty($vps->auto_refurn) ) data-toggle="tooltip" data-placement="top" title="Bật" @else data-toggle="tooltip" data-placement="top" title="Tắt" @endif>
                                                          <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                            <input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch{{$vps->id}}"
                                                              data-id="{{$vps->id}}" data-type="vps"
                                                              @if( !empty($vps->auto_refurn) ) checked @endif
                                                            >
                                                            <label class="custom-control-label" for="customSwitch{{$vps->id}}"></label>
                                                          </div>
                                                        </div>
                                                      </td>
                                                      <td class="vps-status">
                                                          @if(!empty($vps->status_vps))
                                                              @if ($vps->status_vps == 'on')
                                                                <span class="text-success" data-id="{{ $vps->id }}">Đang bật</span>
                                                              @elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang tạo ...</span>
                                                              @elseif ($vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>
                                                              @elseif ($vps->status_vps == 'change_ip')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>
                                                              @elseif ($vps->status_vps == 'expire')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hết hạn</span>
                                                              @elseif ($vps->status_vps == 'admin_off')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Admin tắt</span>
                                                              @elseif ($vps->status_vps == 'delete_vps')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã xóa</span>
                                                              @elseif ($vps->status_vps == 'cancel')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hủy</span>
                                                              @else
                                                                <span class="text-danger" data-id="{{ $vps->id }}">Đã tắt</span>
                                                              @endif
                                                          @else
                                                              <span class="text-danger" data-id="{{ $vps->id }}">Chưa được tạo</span>
                                                          @endif
                                                      </td>
                                                      <td class="page-service-action text-left">
                                                        <button type="button" class="btn btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-plus-circle"></i></button>
                                                        <button type="button" class="btn bg-purple button-action-vps btn-icon-split btn-sm" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="{{ $vps->id }}"><i class="fas fa-exchange-alt"></i></button>
                                                        <button class="btn btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                      </td>
                                                  </tr>
                                            @endif
                                          @endif
                                      @endforeach
                                  @else
                                      <td class="text-center text-danger" colspan="8">
                                              Không có dịch vụ VPS nào hết hạn
                                      </td>
                                  @endif
                              </tbody>
                              <tfoot class="card-footer clearfix">
                                  <td colspan="14" class="text-center link-right">
                                      {{ $list_vps->links()  }}
                                  </td>
                              </tfoot>
                          </table>
                      </div>

                        <div class="col-md-4 px-1 my-2" style="max-width: 240px;">
                            <div class="input-group input-group-sm m-0">
                                <select class="custom-select list_action_nearly" name="" id="">
                                    <option value="" selected disabled>Chọn hành động</option>
                                    <option value="expired">Gia hạn VPS</option>
                                    <option value="change_user">Chuyển khách hàng</option>
                                    <option value="export_vps">Xuất file excel</option>
                                </select>
                            </div>
                        </div>

                  </div>

                  {{-- <div id="group_button">
                      <div class="table-responsive" style="display: block;">
                          <button class="btn bg-gradient-secondary btn-action-vps-nearly btn-icon-split btn-sm mb-1 expired" data-type="expired">
                              <span class="icon"><i class="fas fa-plus-circle"></i></span>
                              <span class="text">Gia hạn VPS</span>
                          </button>
                          <button class="btn bg-purple btn_nearly_change_user btn-icon-split btn-sm mb-1" data-type="change_user">
                              <span class="icon"><i class="fas fa-exchange-alt"></i></span>
                              <span class="text"> Chuyển khách hàng</span>
                          </button>
                          <button class="btn bg-navy btn_nearly_export_vps btn-icon-split btn-sm mb-1" data-type="export_vps">
                              <span class="icon"><i class="far fa-file-excel"></i></span>
                              <span class="text"> Xuất file excel</span>
                          </button>
                      </div>
                  </div> --}}

              </div>
          </div>

        {{-- Kết thúc list VPS --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-link="" data-type="action" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services-nearly">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice-nearly" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated-nearly" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil-nearly">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-multi-finish" data-link="" data-type="action" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}?token={{ date('YmdH') }}"></script>
@endsection
