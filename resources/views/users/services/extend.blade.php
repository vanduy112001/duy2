
@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <div class="text-primary">Gia hạn dịch vụ</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-server"></i> Gia hạn dịch vụ</li>
@endsection
@section('content')
    <div class="row">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger" style="margin-top:20px">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <form action="{{ route('user.pay_in_online.request') }}" method="post">
                    @csrf
                    <div class="form-info">
                        <div class="col col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Thông tin Khách hàng</h4>

                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Họ và tên</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ $user->name }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ $user->email }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Số dư tài khoản</label>
                                        </div>
                                        <div class="col col-md-9">
                                            @if(!empty($user->credit->value))
                                                {!!number_format($user->credit->value,0,",",".") . ' VNĐ'!!}
                                            @else
                                                0 VNĐ
                                            @endif
                                        </div>
                                    </div>
                                    {{--<div class="row form-group">--}}
                                        {{--<div class="col col-md-3 required">--}}
                                            {{--<label for="">Số tiền nạp</label>--}}
                                        {{--</div>--}}
                                        {{--<div class="col col-md-9">--}}
                                            {{--<input type="text" class="form-control" name="money" value="{{ !empty(old('money')) ? old('money') : 200000 }}">--}}
                                            {{--<div class="luu-y">--}}
                                                {{--<span class="text-danger">Lưu ý:</span> Số tiền nạp vào tối thiểu là: 200.000 đ--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                                <div class="col-md-6">
                                    <h4>Thông tin Dịch vụ</h4>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Tên dịch vụ</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ $product->name }} ({{$product->group_product->name }})

                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Giá</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ !empty($detail->detail_order->sub_total) ? number_format($detail->detail_order->sub_total,0,",",".") . ' VNĐ' : '0 VNĐ' }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Thời gian thuê</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ !empty($billings[$detail->billing_cycle]) ? $billings[$detail->billing_cycle] : '' }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Ngày hết hạn</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3">
                                            <label for="">Gia hạn đến</label>
                                        </div>
                                        <div class="col col-md-9">
                                            {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row form-group mt-4">
                                <div class="col-md-3 required">
                                    <label for="">Thanh toán qua</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="row">
                                        <div class="col-md-4 text-center">
                                            <div class="form-group clearfix">
                                                <div class="icheck-primary d-inline">
                                                    <input type="radio" name="method_pay" id="pay_in_office" value="pay_in_office" checked>
                                                    <label for="pay_in_office"></label>
                                                </div>
                                                <div class="title-payment mt-4">
                                                    <i class="fas fa-home"></i> <br>
                                                    <label for="pay_in_office">Trực tiếp tại văn phòng</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <div class="form-group clearfix">
                                                <div class="icheck-primary d-inline">
                                                    <input type="radio" name="method_pay" id="bank_transfer" value="bank_transfer">
                                                    <label for="bank_transfer"></label>
                                                </div>
                                                <div class="title-payment mt-4">
                                                    <i class="far fa-credit-card"></i> <br>
                                                    <label for="bank_transfer">Chuyển khoản ngân hàng</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-center">
                                            <div class="form-group clearfix">
                                                <div class="icheck-primary d-inline">
                                                    <input type="radio" name="method_pay" id="momo" value="momo">
                                                    <label for="momo"></label>
                                                </div>
                                                <div class="title-payment-momo mt-4">
                                                    <img src="{{ asset('images/momo.png') }}" alt="Thanh toán qua momo"> <br>
                                                    <label for="momo">Thanh toán qua MOMO</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- <select name="method_pay" class="form-control" id="pay_in">
                                    <option value="" disabled selected>---Chọn hình thức thanh toán---</option>
                                    @foreach ($method_pay_in as $key => $pay_in)
                                    @php
                                        $selected = '';
                                        if(old('method_pay') == $key) {
                                            $selected = 'selected';
                                        }
                                    @endphp
                                            <option value="{{ $key }}" {{ $selected }}>{{ $pay_in }}</option>
                                    @endforeach
                                        </select> -->
                                    <div class="selected-pay-in text-center">
                                    </div>
                                </div>
                            </div>
                            <div class="button-pay-in text-center">
                                <a href="{{ route('index') }}" class="btn btn-secondary">Hủy</a>
                                <input type="submit" class="btn btn-primary" value="Tiến hành thanh toán">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/pay_in.js')  }}"></script>
@endsection
