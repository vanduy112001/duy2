@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    Dịch vụ VPS đã hủy - đã xóa - đã chuyển
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS</li>
<li class="breadcrumb-item active"> Hủy</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
  <div class="col-md-12">
    @php
        $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
    @endphp
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-primary btn-sm">Tạo VPS</a>
      @endif
    @endforeach
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'nat_vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-success btn-sm">Tạo NAT VPS</a>
      @endif
    @endforeach
  </div>
</div>
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div id="custom-tabs-two-tabContent" class="tab-content">
          <div class="list_vps_nearly text_list_vps tab-pane fade show active" id="vps" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
              <div class="box box-default">
                  <div class="box-body table-responsive">
                        <div class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="col-md-12 detail-course-finish">
                                @if(session("success"))
                                <div class="bg-success">
                                    <p class="text-light">{{session("success")}}</p>
                                </div>
                                @elseif(session("fails"))
                                <div class="bg-danger">
                                    <p class="text-light">{{session("fails")}}</p>
                                </div>
                                @endif
                            </div>
                            <div class="col-md-12 my-2">
                                <div class="row">
                                    <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                        <select class="form-control form-control-sm vp_cancel_classic">
                                            <option value="20" selected>20</option>
                                            <option value="30">30</option>
                                            <option value="40">40</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                    </div>
    
                                    <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                                        <button class="btn bg-secondary btn_nearly_export_vps btn-icon-split btn-sm mb-1" data-type="export_vps">
                                            <span class="icon"><i class="far fa-file-excel"></i></span>
                                            <span class="text"> Xuất file excel</span>
                                        </button>
                                    </div>
    
                                    <div class="col-md-6 mb-2 px-1 ml-auto">
                                        <div class="input-group input-group-sm mb-0 mt-0">
                                            <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm VPS theo 1 hoặc nhiều IP">
                                            <div class="input-group-append">
                                                <button id="btn_cancel_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                          <table class="table table-bordered table-hover text-center">
                              <thead class="">
                                  <th width="3%"><input type="checkbox" class="vps_checkbox_nearly_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả IP VPS"></th>
                                  <th>IP</th>
                                  <th>Cấu hình</th>
                                  <th>Ngày tạo</th>
                                  <th>Ngày kết thúc</th>
                                  <th width="7%">Tổng thời gian thuê</th>
                                  <th width="7%">Chu kỳ thanh toán</th>
                                  <th>Chi phí</th>
                                  <th width="7%">Ghi chú</th>
                                  <th>Trạng thái</th>
                              </thead>
                              <tbody>
                                  @if(isset($list_vps))
                                      @foreach ($list_vps as $vps)
                                          @if ( !empty($vps->status_vps) )
                                            @if(!empty($vps->product))
                                                @php
                                                    $total_time = '';
                                                    if(!empty($vps->next_due_date)) {
                                                        $create_date = new Carbon($vps->date_create);
                                                        $next_due_date = new Carbon($vps->next_due_date);
                                                        if ( $next_due_date->diffInYears($create_date) ) {
                                                            $year = $next_due_date->diffInYears($create_date);
                                                            $total_time = $year . ' Năm ';
                                                            $create_date = $create_date->addYears($year);
                                                            $month = $next_due_date->diffInMonths($create_date);
                                                            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                            if ( $month ) {
                                                                $total_time .= $month . ' Tháng';
                                                            }
                                                        } else {
                                                            $diff_month = $next_due_date->diffInMonths($create_date);
                                                            $total_time = $diff_month . ' Tháng';
                                                        }
                                                    }
                                                @endphp
                                                  <tr>
                                                      <td><input type="checkbox" value="{{ $vps->id }}" data-ip="{{ $vps->ip }}" class="vps_nearly_checkbox"></td>
                                                      <td class="ip_vps text-left" data-ip="{{ !empty($vps->ip) ? $vps->ip : '' }}">
                                                          @if (!empty($vps->ip))
                                                              <a href="{{ route('services.detail', $vps->id) }}?type=vps">{{$vps->ip}}</a>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>
                                                        @php
                                                          $product = $vps->product;
                                                          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                          // Addon

                                                          if (!empty($vps->addon_id)) {
                                                              $vps_config = $vps->vps_config;
                                                              if (!empty($vps_config)) {

                                                                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                              }
                                                          }
                                                        @endphp
                                                        {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk
                                                      </td>
                                                      <td>
                                                          @if (!empty($vps->date_create))
                                                              <span>{{ date('d-m-Y', strtotime($vps->date_create)) }}</span>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td class="next_due_date">
                                                          @if (!empty($vps->next_due_date))
                                                              {{ date('d-m-Y', strtotime($vps->next_due_date)) }}
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>{{ $total_time }}</td>
                                                      <td>
                                                          {{ $billing[$vps->billing_cycle] }}
                                                      </td>
                                                      <td>
                                                         @php
                                                            $total = 0;
                                                            if ( !empty($vps->price_override) ) {
                                                               $total = $vps->price_override;
                                                            } else {
                                                                $product = $vps->product;
                                                                $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                                                                if (!empty($vps->vps_config)) {
                                                                  $addon_vps = $vps->vps_config;
                                                                  $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                                                                  $pricing_addon = 0;
                                                                  foreach ($add_on_products as $key => $add_on_product) {
                                                                    if (!empty($add_on_product->meta_product->type_addon)) {
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                                                      }
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                                                      }
                                                                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                                                      }
                                                                    }
                                                                  }
                                                                  $total += $pricing_addon;
                                                                }
                                                            }
                                                         @endphp
                                                         {!!number_format( $total ,0,",",".")!!} VNĐ
                                                      </td>
                                                      <td>
                                                          <span class="text-description">
                                                            @if(!empty($vps->description))
                                                                {{ substr($vps->description, 0, 40) }}
                                                            @endif
                                                          </span>
                                                          <span>
                                                            <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                            <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $vps->id }}"><i class="fas fa-edit"></i></a>
                                                          </span>
                                                      </td>
                                                      <td class="vps-status">
                                                          @if(!empty($vps->status_vps))
                                                              @if ($vps->status_vps == 'on')
                                                                <span class="text-success" data-id="{{ $vps->id }}">Đang bật</span>
                                                              @elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang tạo ...</span>
                                                              @elseif ($vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>
                                                              @elseif ($vps->status_vps == 'change_ip')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>
                                                              @elseif ($vps->status_vps == 'expire')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hết hạn</span>
                                                              @elseif ($vps->status_vps == 'admin_off')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Admin tắt</span>
                                                              @elseif ($vps->status_vps == 'delete_vps')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã xóa</span>
                                                              @elseif ($vps->status_vps == 'change_user')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã chuyển</span>
                                                              @elseif ($vps->status_vps == 'cancel')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hủy</span>
                                                              @else
                                                                <span class="text-danger" data-id="{{ $vps->id }}">Đã tắt</span>
                                                              @endif
                                                          @else
                                                              <span class="text-danger" data-id="{{ $vps->id }}">Chưa được tạo</span>
                                                          @endif
                                                      </td>
                                                  </tr>
                                            @endif
                                          @endif
                                      @endforeach
                                  @else
                                      <td class="text-center text-danger" colspan="8">
                                              Không có dịch vụ VPS nào hết hạn
                                      </td>
                                  @endif
                              </tbody>
                              <tfoot class="card-footer clearfix">
                                  <td colspan="11" class="text-center link-right">
                                      {{ $list_vps->links()  }}
                                  </td>
                              </tfoot>
                          </table>
                      </div>

                      <div id="group_button">
                        <div class="table-responsive" style="display: block;">
                          <button class="btn bg-secondary btn_nearly_export_vps btn-icon-split btn-sm mb-1" data-type="export_vps">
                            <span class="icon"><i class="far fa-file-excel"></i></span>
                            <span class="text"> Xuất file excel</span>
                          </button>
                        </div>
                      </div>

                  </div>
              </div>
          </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}?token={{ date('YmdH') }}"></script>
@endsection
