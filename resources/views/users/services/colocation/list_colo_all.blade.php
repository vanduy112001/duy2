@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ Colocation</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ Colocation</li>
<li class="breadcrumb-item active"> Tất cả</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div class="list_colocation" id="colocation">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                      <div class="col-md-12 my-2">
                          <div class="row">
                              <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                  <select class="form-control form-control-sm vps_all_classic">
                                    <option value="20" selected>20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                  </select>
                              </div>

                              <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                              </div>

                              <div class="col-md-6 mb-2 px-1 ml-auto">
                                  <div class="input-group input-group-sm mb-0 mt-0">
                                      <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm Colocation theo 1 hoặc nhiều IP">
                                      <div class="input-group-append">
                                          <button id="btn_all_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                      </div>
                                  </div>
                              </div>
                              <div class="col-md-12 text-primary">
                                <b>
                                    Đã chọn:
                                    <span class="qtt-checkbox">0</span> / <span class="total-item">{{ $list_colocations->count() }}</span>
                                </b>
                              </div>
                          </div>
                      </div>
                        <table class="table table-bordered table-hover text-center">
                            <thead class="info">
                                <th>IP</th>
                                <th>Loại</th>
                                <th>Addon IP</th>
                                <th>Ngày tạo</th>
                                <th width="10%" class="sort_next_due_date_all" data-sort="" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                    Ngày kết thúc <i class="fas fa-sort"></i>
                                    <input type="hidden" class="sort_type" value="">
                                </th>
                                <th>Tổng thời gian thuê</th>
                                <th>Chu kỳ thanh toán</th>
                                <th>Chi phí (VNĐ)</th>
                                <th>Trạng thái</th>
                            </thead>
                            <tbody>
                              @if($list_colocations->count() > 0)
                                  @foreach ($list_colocations as $colocation)
                                        @php
                                            $total_time = '';
                                            if(!empty($colocation->next_due_date)){
                                                $create_date = new Carbon($colocation->date_create);
                                                $next_due_date = new Carbon($colocation->next_due_date);
                                                if ( $next_due_date->diffInYears($create_date) ) {
                                                    $year = $next_due_date->diffInYears($create_date);
                                                    $total_time = $year . ' Năm ';
                                                    $create_date = $create_date->addYears($year);
                                                    $month = $next_due_date->diffInMonths($create_date);
                                                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                    if ( $month ) {
                                                        $total_time .= $month . ' Tháng';
                                                    }
                                                } else {
                                                    $diff_month = $next_due_date->diffInMonths($create_date);
                                                    $total_time = $diff_month . ' Tháng';
                                                }
                                            }
                                        @endphp
                                      <tr>
                                          <td>
                                              @if (!empty($colocation->ip))
                                                  <a href="{{ route( 'service.colocation_detail', $colocation->id ) }}">{{$colocation->ip}}</a>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>
                                              {{ $colocation->type_colo }}
                                          </td>
                                          <td>
                                              {{ !empty( $colocation->colocation_config->ip ) ? $colocation->colocation_config->ip : '' }}
                                              @if(!empty( $colocation->colocation_config->ip ))
                                                <button type="button" class="ml-1 btn btn-sm btn-outline-success float-right collapsed tooggle-plus" data-toggle="collapse" data-target="#service{{ $colocation->id }}" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                                <div id="service{{ $colocation->id }}" class="mt-4 collapse" style="">
                                                    @foreach ($colocation->colocation_ips as $colocation_ip)
                                                        {{ $colocation_ip->ip }} <br>
                                                    @endforeach
                                                </div>
                                              @endif
                                          </td>
                                          <td>
                                            @if (!empty($colocation->date_create))
                                                <span>{{ date('d-m-Y', strtotime($colocation->date_create)) }}</span>
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                          </td>
                                          <td class="next_due_date">
                                              @if (!empty($colocation->next_due_date))
                                                  <span class="" >{{ date('d-m-Y', strtotime($colocation->next_due_date)) }}</span>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>{{ $total_time }}</td>
                                          <td>
                                              {{ $billing[$colocation->billing_cycle] }}
                                          </td>
                                          <td>
                                            @php
                                              $amount = 0;
                                              if ( !empty($colocation->amount) ) {
                                                $amount = $colocation->amount;
                                              } else {
                                                $product = $colocation->product;
                                                $amount = !empty( $product->pricing[$colocation->billing_cycle] ) ? $product->pricing[$colocation->billing_cycle] : 0;
                                                if ( !empty($colocation->colocation_config_ips) ) {
                                                  foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                                                    $amount += !empty( $colocation_config_ip->product->pricing[$colocation->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colocation->billing_cycle] : 0;
                                                  }
                                                }
                                              }
                                            @endphp
                                            {!!number_format( $amount ,0,",",".")!!}
                                          </td>
                                          <td class="colocation_status">
                                            @if ($colocation->status_colo == 'delete_colo')
                                              <span class="text-danger" data-id="{{ $colocation->id }}">Đã xóa</span>
                                            @elseif ($colocation->status_colo == 'expire')
                                                <span class="text-danger" data-id="{{ $colocation->id }}">Đã hết hạn</span>
                                            @elseif ($colocation->status_colo == 'cancel')
                                                <span class="text-danger" data-id="{{ $colocation->id }}">Đã hủy</span>
                                            @elseif ($colocation->status_colo == 'change_user')
                                              <span class="text-danger" data-id="{{ $colocation->id }}">Đã chuyển</span>
                                            @else
                                              <span class="text-danger" data-id="{{ $colocation->id }}">Đã hủy</span>
                                            @endif
                                          </td>
                                      </tr>
                                  @endforeach
                              @else
                                  <td class="text-center text-danger" colspan="15">
                                      Không có dịch vụ Colocation được sử dụng
                                  </td>
                              @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                                <td colspan="15" class="text-center link-right">
                                    {{ $list_colocations->links()  }}
                                </td>
                            </tfoot>
                        </table>
                    </div>
                    <div id="group_button">
                        <!-- <div class="table-responsive" style="display: block;">
                            <button class="btn bg-gradient-secondary btn-action-colocation btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                <span class="text">Gia hạn Colocation</span>
                            </button>
                            <button class="btn btn-danger btn-action-colocation btn-icon-split btn-sm mb-1" data-type="delete">
                                <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <span class="text">Hủy Colocation</span>
                            </button>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
<!-- <div id="float-menu">
    <div class="menu-vps">
        <button class="btn btn-primary btn-list-vps"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="service-name">VPS</span></button>
    </div>
    <div class="menu-hosting">
        <button class="btn btn-danger btn-list-hosting"><i class="fa fa-globe" aria-hidden="true"></i> <span class="service-name">Hosting</span></button>
    </div>
    <div class="menu-server">
        <button class="btn btn-info btn-list-server"><i class="fa fa-server" aria-hidden="true"></i> <span class="service-name">Server</span></button>
    </div>
</div> -->
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">
  
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-success" id="button-rebuil">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-multi-finish" data-link="" data-type="action" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
  <!-- Model action từng services -->
  <div class="modal fade" id="modal-service">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa đơn đặt hàng</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-service" class="text-center">
  
              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
            <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
  </div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service_colo.js') }}?token={{ date('YmdH') }}"></script>
@endsection
