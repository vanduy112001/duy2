@php
    $product = GroupProduct::get_product($detail->product_id);
@endphp
<div class="col-md-6">
    @if ($detail->status_hosting == 'on')
        <div class="product-status product-status-on">
            <div class="product-icon text-center">
                                <span class="fa-stack fa-lg">
                                    <i class="fas fa-circle fa-stack-2x"></i>
                                    <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                </span>
                <h3>{{ $product->name }}</h3>
                <h4>{{ $product->group_product->name }}</h4>
            </div>
            <div class="product-status-text">
                @if($isExpired)
                    Đã hết hạn
                @else
                    Đang sử dụng
                @endif
            </div>

        </div>
        <div class="row">
            <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                <a href="" id="button_terminated" data-id="{{ $detail->id }}" data-domain="{{ $detail->domain }}" data-type="hosting" class="btn btn-block btn-danger">Hủy dịch vụ</a>
            </div>
        </div>
        @if($isExpired)
            <br>
            <div class="row">
                <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                    <a href="{{ route('service.extend', $detail->id) }}?type=vps"
                       class="btn btn-block btn-success">Gia hạn ngay</a>
                </div>
            </div>
        @endif
    @elseif  ($detail->status_hosting == 'off' || $detail->status_hosting == 'admin_off')
        <div class="product-status product-status-off">
            <div class="product-icon text-center">
                                  <span class="fa-stack fa-lg">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                  </span>
                <h3>{{ $product->name }}</h3>
                <h4>{{ $product->group_product->name }}</h4>
            </div>
            <div class="product-status-text">
                Đang tắt
            </div>
        </div>
        <div class="row">
            <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                <a href="" id="button_terminated" data-id="{{ $detail->id }}" data-type="hosting" class="btn btn-block btn-danger">Hủy dịch vụ</a>
            </div>
        </div>
    @else
        <div class="product-status product-status-cancelled">
            <div class="product-icon text-center">
                                  <span class="fa-stack fa-lg">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                  </span>
                <h3>{{ $product->name }}</h3>
                <h4>{{ $product->group_product->name }}</h4>
            </div>
            <div class="product-status-text">
                @if($detail->status_hosting == 'cancel')
                    Đợi hủy
                @else
                    Đã hủy
                @endif
            </div>
        </div>
    @endif
</div>
@include('users.services.includes.detail-right-content',compact('detail','payment_methods'))
