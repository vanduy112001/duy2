@php
    $user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Hồ sơ khách hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item"><a href="{{ route('user.customer.index') }}"> Danh sách khách hàng</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Danh sách đơn hàng / {{$makh}}</li>
@endsection
@section('content')
<div class="row">
  <div class="title col-md-12">
      <div class="title-body">
          <div class="row">
              <div class="col-md-12 detail-course-finish">
                  @if(session("success"))
                      <div class="bg-success">
                          <p class="text-light">{{session("success")}}</p>
                      </div>
                  @elseif(session("fails"))
                      <div class="bg-danger">
                          <p class="text-light">{{session("fails")}}</p>
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger" style="margin-top:20px;width:100%;">
                          <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
              </div>
          </div>
      </div>
  </div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-12 detail-course-finish table-responsive">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <table class="table table-bordered table-hover">
                        <thead class="primary">
                            <th>STT</th>
                            <th>Dịch vụ</th>
                            <th>Ngày hết hạn</th>
                            <th>Số lượng</th>
                            <th>Thành tiền</th>
                            <th>Trạng thái</th>
                            <th>Hành động</th>
                        </thead>
                        @if ( $orders->count() > 0 )
                          @foreach($orders as $key => $order)
                            <tr>
                              <td>{{ $key + 1 }}</td>
                              <td>{{ $order->detail_orders[0]->type }}</td>
                              <td>{{ date('m-d-Y' , strtotime($order->detail_orders[0]->due_date)) }}</td>
                              <td>{{ $order->detail_orders[0]->quantity }}</td>
                              <td>{{ $order->total }}</td>
                              <td>
                                @if (!empty($order->verify))
                                    @if ($order->status == 'Active')
                                        <span class="text-success"><i class="fas fa-check"></i> Đã kích hoạt</span>
                                    @elseif ($order->status == 'Pending')
                                        <span class="text-danger"><i class="fas fa-times"></i> Chưa kích hoạt</span>
                                    @else
                                        <span class="text-default"><i class="fas fa-ban"></i> Hủy</span>
                                    @endif
                                @else
                                    <button disabled="disabled" class="btn btn-outline-danger">Chưa xác nhận</button>
                                @endif
                              </td>
                              <td>
                                <a href="#" class="btn btn-success" title="Gia hạn"><i class="fas fa-sync-alt"></i></a>
                              </td>
                            </tr>
                          @endforeach
                        @else
                            <tbody>
                                <tr>
                                    <td colspan="6" class="text-center text-danger">Không có đơn hàng nào của khách hàng {{$makh}}.</td>
                                </tr>
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa customer --}}
<div class="modal fade" id="delete-customer">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa hồ sơ khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-customer" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-customer" value="Xóa khách hàng">
          <button type="button" class="btn btn-danger" id="button-finish"  data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<!-- <script src="{{ asset('js/cutomer.js')  }}"></script> -->
@endsection
