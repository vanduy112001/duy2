@php
    $user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('/libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Thanh toán trực tuyến</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thanh toán trực tuyến</li>
@endsection
@section('content')
<div class="row">
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <form action="{{ route('user.pay_in_online.request') }}" method="post">
                @csrf
                <div class="form-info">
                    <h4>Thông tin giao dịch</h4>
                    <div class="col col-md-12">
                        <div class="luu-y mb-4">
                            Lưu ý: Hạn mức của tài khoản là: 100.000.000 đ
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="">Họ và tên</label>
                            </div>
                            <div class="col col-md-9">
                                {{ $user->name }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="">Email</label>
                            </div>
                            <div class="col col-md-9">
                                {{ $user->email }}
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3">
                                <label for="">Số dư tài khoản</label>
                            </div>
                            <div class="col col-md-9">
                                @if(!empty($user->credit->value))
                                    {!!number_format($user->credit->value,0,",",".") . ' VNĐ'!!}
                                @else
                                    0 VNĐ
                                @endif
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col col-md-3 required">
                                <label for="">Số tiền nạp</label>
                            </div>
                            <div class="col col-md-9">
                                <input type="text" class="form-control" name="money" value="{{ !empty(old('money')) ? old('money') : 200000 }}">
                                <div class="luu-y">
                                    <span class="text-danger">Lưu ý:</span> Số tiền nạp vào tối thiểu là: 200.000 đ
                                </div>
                            </div>
                        </div>
                        <div class="row form-group mt-4">
                            <div class="col-md-3 required">
                                <label for="">Thanh toán qua</label>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <!-- <div class="col-md-4 text-center">
                                      <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                          <input type="radio" name="method_pay" id="pay_in_office" value="pay_in_office" checked>
                                          <label for="pay_in_office"></label>
                                        </div>
                                        <div class="title-payment mt-4">
                                            <i class="fas fa-home"></i> <br><br>
                                            <label for="pay_in_office">Trực tiếp tại văn phòng</label>
                                        </div>
                                      </div>
                                    </div> -->
                                    <div class="col-md-4 text-center">
                                      <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                          <input type="radio" name="method_pay" id="bank_transfer_vietcombank" value="bank_transfer_vietcombank" checked>
                                          <label for="bank_transfer_vietcombank"></label>
                                        </div>
                                        <div class="title-payment-momo mt-4">
                                            <img src="{{ asset('images/vietcom.png') }}" alt="Thanh toán qua Vietcombank"> <br><br>
                                            <label for="bank_transfer">Chuyển khoản ngân hàng Vietcombank</label>
                                        </div>
                                      </div>
                                    </div>
                                </div>
                                <div class="selected-pay-in text-center">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-4">
                          <div class="col col-md-3 required">
                          </div>
                          <div class="col col-md-9 button-pay-in text-center">
                            <a href="{{ route('index') }}" class="btn btn-secondary">Hủy</a>
                            <input type="submit" class="btn btn-primary" value="Tiến hành thanh toán">
                          </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('js/pay_in.js')  }}"></script>
@endsection
