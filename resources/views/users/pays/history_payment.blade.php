@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    Lịch sử giao dịch
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thanh toán trực tuyến</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
                <!-- form search -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-sm table-bordered text-center">
                    <thead class="">
                        <th width="20%">Mã giao dịch</th>
                        <th>Hình thức</th>
                        <th>Số tiền</th>
                        <th>Số dư sau thanh toán</th>
                        <th>Ngày thanh toán</th>
                        <th>Trạng thái</th>
                    </thead>
                    <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                            <td>
                                {{ $payment->ma_gd }}
                            </td>
                            <td class="text-left">
                                {{ $type_payin[$payment->type_gd] }}

                                @if ($payment->type_gd != 1 && $payment->type_gd != 98 && $payment->type_gd != 99)
                                    @if( !empty($payment->detail_order->order) )
                                        <button type="button" class="btn btn-sm btn-outline-secondary float-right tooggle-minus" data-toggle="collapse" data-target="#service{{$payment->id}}"   data-placement="top" title="Ẩn chi tiết">
                                            <i class="fas fa-minus"></i>
                                        </button>
                                        <div id="service{{$payment->id}}" class="collapse show mt-2 show-detail-service">
                                            @if( $payment->detail_order->order->type == 'expired' )
                                                @if ($payment->detail_order->type == 'Domain_exp')
                                                    @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                      @if(!empty($order_expired->domain->domain))
                                                        {{ $order_expired->domain->domain }} <br>
                                                      @endif
                                                    @endforeach
                                                @else
                                                    @if($payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'NAT-VPS' || $payment->detail_order->type == 'VPS US')
                                                        @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                            @if(!empty($order_expired->vps->ip))
                                                              {{ $order_expired->vps->ip }} <br>
                                                            @else
                                                              <span class="text-danger">Chưa tạo</span> <br>
                                                            @endif
                                                        @endforeach
                                                    @elseif ( $payment->detail_order->type == 'Proxy' )
                                                          @if ( !empty($payment->detail_order->order_expireds) )
                                                            @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                                              @if ( !empty( $order_expired->proxy->ip ) )
                                                                {{ $order_expired->proxy->ip }} <br>
                                                              @else
                                                                <span class="text-danger">Đã xóa</span> <br>
                                                              @endif
                                                            @endforeach
                                                          @endif
                                                    @elseif($payment->detail_order->type == 'Server')
                                                        @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                            @if(!empty($order_expired->server->ip))
                                                              {{ $order_expired->server->ip }} <br>
                                                            @else
                                                              <span class="text-danger">Chưa tạo</span> <br>
                                                            @endif
                                                        @endforeach
                                                    @elseif($payment->detail_order->type == 'Colocation')
                                                        @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                            @if(!empty($order_expired->colocation->ip))
                                                              {{ $order_expired->colocation->ip }} <br>
                                                            @else
                                                              <span class="text-danger">Chưa tạo</span> <br>
                                                            @endif
                                                        @endforeach
                                                    @elseif($payment->detail_order->type == 'hosting')
                                                        @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                            @if(!empty($order_expired->hosting->domain))
                                                              {{ $order_expired->hosting->domain }} <br>
                                                            @else
                                                              <span class="text-danger">Chưa tạo</span> <br>
                                                            @endif
                                                        @endforeach
                                                    @elseif($payment->detail_order->type == 'Email Hosting')
                                                        @foreach ($payment->detail_order->order_expireds as $key => $order_expired)
                                                            @if(!empty($order_expired->email_hosting->domain))
                                                              {{ $order_expired->email_hosting->domain }} <br>
                                                            @else
                                                              <span class="text-danger">Chưa tạo</span> <br>
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                @endif
                                            @elseif( $payment->detail_order->type == 'change_ip')
                                                @foreach ($payment->detail_order->order_change_vps as $key => $order_change_vps)
                                                    @if(!empty($order_change_vps->vps->ip))
                                                      {{ $order_change_vps->vps->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'addon_vps')
                                                @foreach ($payment->detail_order->order_addon_vps as $key => $order_addon_vps)
                                                    @if(!empty($order_addon_vps->vps->ip))
                                                      {{ $order_addon_vps->vps->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'NAT-VPS' || $payment->detail_order->type == 'VPS US' || $payment->detail_order->type == 'VPS-US')
                                                @foreach ($payment->detail_order->vps as $key => $vps)
                                                    @if(!empty($vps->ip))
                                                      {{ $vps->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'Hosting')
                                                @foreach ($payment->detail_order->hostings as $key => $hosting)
                                                    @if(!empty($hosting->domain))
                                                      {{ $hosting->domain }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'Email Hosting')
                                                @foreach ($payment->detail_order->email_hostings as $key => $hosting)
                                                    @if(!empty($hosting->domain))
                                                      {{ $hosting->domain }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'Server')
                                                @foreach ($payment->detail_order->servers as $key => $vps)
                                                    @if(!empty($vps->ip))
                                                      {{ $vps->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'Colocation')
                                                @foreach ($payment->detail_order->colocations as $key => $vps)
                                                    @if(!empty($vps->ip))
                                                      {{ $vps->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif ( $payment->detail_order->type == 'Proxy' )
                                                @foreach ( $payment->detail_order->proxies as $proxy )
                                                    @if ( !empty($proxy->ip) )
                                                      {{ $proxy->ip }} <br>
                                                    @else
                                                      <span class="text-danger">Đang chờ tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'Domain')
                                                @foreach ($payment->detail_order->domains as $key => $hosting)
                                                    @if(!empty($hosting->domain))
                                                      {{ $hosting->domain }} <br>
                                                    @else
                                                      <span class="text-danger">Chưa tạo</span> <br>
                                                    @endif
                                                @endforeach
                                            @elseif($payment->detail_order->type == 'upgrade_hosting')
                                                @if( !empty($payment->detail_order->order_upgrade_hosting->hosting->domain) )
                                                    {{ $payment->detail_order->order_upgrade_hosting->hosting->domain }} <br>
                                                @else
                                                    <span class="text-danger">Chưa tạo</span> <br>
                                                @endif
                                            @endif
                                        </div>
                                    @endif
                                @endif
                            </td>
                            <td>
                                @php $class = '';  @endphp
                                @if( $payment->type_gd == 1 || $payment->type_gd == 98 )
                                    <strong class=" text-success">+</strong>
                                    @php $class = 'text-success'; @endphp
                                @else
                                    <strong class="text-danger">-</strong>
                                    @php $class = 'text-danger'; @endphp
                                @endif
                                <span class="{{$class}}">{!!number_format($payment->money,0,",",".") . ' đ'!!} </span>
                            </td>
                            <td>
                                @if ($payment->status == 'Active')
                                  @if ( $payment->type_gd == 1 || $payment->type_gd == 98 || $payment->type_gd == 99 )
                                    @if ( !empty($payment->log_payment->after) )
                                      {!!number_format($payment->log_payment->after,0,",",".") . ' đ'!!}
                                    @else
                                        0 đ
                                    @endif
                                  @else
                                    @if ( !empty($payment->log_payment->after) )
                                      {!!number_format($payment->log_payment->after,0,",",".") . ' đ'!!}
                                    @else
                                        0 đ
                                    @endif
                                  @endif
                                @endif
                            </td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($payment->updated_at)) }}</td>
                            <td class="text-left">
                                @if ( empty($payment->detail_order->id) )
                                    @if ( $payment->status == 'Active' )
                                        <span class="text-success">Đã thanh toán</span>
                                    @else 
                                        <span class="text-danger">Chưa thanh toán</span>
                                    @endif
                                @else
                                    @if (!empty($payment->detail_order->paid_date))
                                        <a href="{{ route('order.check_invoices', $payment->detail_order->id) }}" class="btn btn-sm btn-outline-success">Đã thanh toán</a>
                                    @else
                                        <a href="{{ route('order.check_invoices', $payment->detail_order->id) }}" class="btn btn-sm btn-outline-danger">Chưa thanh toán</a>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot class="card-footer clearfix">
                    <td colspan="9" class="text-center">
                        {{ $payments->links()  }}
                    </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>sss
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="collapse"]').tooltip();
        $(document).on("click", '.tooggle-plus', function () {
            $(this).children("i").removeClass("fa-plus");
            $(this).children("i").addClass("fa-minus");
            $(this).removeClass("tooggle-plus");
            $(this).addClass("toggle-minus");
        })
        $(document).on("click", '.toggle-minus', function () {
            $(this).children("i").removeClass("fa-minus");
            $(this).children("i").addClass("fa-plus");
            $(this).removeClass("toggle-minus");
            $(this).addClass("tooggle-plus");
        })
    });
</script>
@endsection
