
@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
Kiểm tra đơn hàng
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Kiểm tra đơn hàng</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if ($pay_in_office->method_gd == 'pay_in_office')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light">Yêu cầu nạp tiền vào tài khoản thành công</p>
                      </div>
                      <div class="card-body mb-4">
                          <div class="info-company">
                              <b>Địa chỉ:</b> 257 - Lê Duẩn - Thành phố Đà Nẵng <br><b>Văn phòng:</b> 67 - Nguyễn Thị Định - quận Sơn Trà - Thành phố Đà Nẵng <br><b>Số điện thoại:</b> 0236.44.55.789
                              <br><b>Thông tin chi tiết:</b> <br>
                              - Loại giao dịch: Trực tiếp tại văn phòng <br>
                              - Số dư tài khoản: {{ number_format(Auth::user()->credit->value,0,",",".") . ' VNĐ' }} <br>
                              - Số tiền nạp: {{ number_format($pay_in_office->money,0,",",".") . ' VNĐ' }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd == 'bank_transfer_vietcombank')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light">Yêu cầu nạp tiền vào tài khoản thành công</p>
                      </div>
                      <div class="card-body">
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Bank Name:</b> Vietcombank <br>
                                  <b>Account Name:</b> Nguyen Thi Ai Hoa <br>
                                  <b>Account Number:</b> 0041000830880 <br>
                              </div>
                              <b>Thông tin chi tiết:</b> <br>
                              - Loại giao dịch: Chuyển khoản ngân hàng <br>
                              - Số dư tài khoản: {{ number_format(Auth::user()->credit->value,0,",",".") . ' VNĐ' }} <br>
                              - Số tiền nạp: {{ number_format($pay_in_office->money,0,",",".") . ' VNĐ' }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                              - Hướng dẫn: nạp tiền vào tài khoản <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-nap-tien-vao-tai-khoan-cloudzone-portal/" target="_blank">tại đây</a>. <br>
                              - <b class="payin-note">Lưu ý: </b> Quý khách ghi nội dung mã giao dịch khi thực hiện chuyển khoản. Hệ thống sẽ tự động cập nhật 
                              số dư tài khoản trong vòng 5 đến 10 phút. Hệ thống cập nhật số dư xong sẽ gửi email thông báo cho quý khách.<br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd == 'bank_transfer_techcombank')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light">Yêu cầu nạp tiền vào tài khoản thành công</p>
                      </div>
                      <div class="card-body">
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Bank Name:</b> Techcombank <br>
                                  <b>Account Name:</b> Nguyen Thi Ai Hoa <br>
                                  <b>Account Number:</b> 19033827040017 <br>
                              </div>
                              <b>Thông tin chi tiết:</b> <br>
                              - Loại giao dịch: Chuyển khoản ngân hàng <br>
                              - Số dư tài khoản: {{ number_format(Auth::user()->credit->value,0,",",".") . ' VNĐ' }} <br>
                              - Số tiền nạp: {{ number_format($pay_in_office->money,0,",",".") . ' VNĐ' }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                              - Hướng dẫn: nạp tiền vào tài khoản <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-nap-tien-vao-tai-khoan-cloudzone-portal/" target="_blank">tại đây</a>. <br>
                              - <b class="payin-note">Lưu ý: </b> Quý khách ghi nội dung mã giao dịch khi thực hiện chuyển khoản. Hệ thống sẽ tự động cập nhật số dư tài khoản trong vòng 5 đến 10 phút. 
                              Hệ thống cập nhật số dư xong sẽ gửi email thông báo cho quý khách.<br>
                          </div>
                      </div>
                  </div>
                @elseif ($pay_in_office->method_gd == 'momo')
                  <div class="rounded recharge-finish card card-primary card-outline">
                      <div class="bg-success">
                          <p class="text-light">Yêu cầu nạp tiền vào tài khoản thành công</p>
                      </div>
                      <div class="card-body mb-4">
                          <div class="info-company">
                              <div class="info-bank">
                                  <b>Account Name:</b> Lê Minh Chí <br>
                                  <b>SĐT MoMo:</b> 0905 091 805 <br>
                              </div>
                              <b>Thông tin chi tiết:</b> <br>
                              - Loại giao dịch: Thanh toán qua MoMo <br>
                              - Số dư tài khoản: {{ number_format(Auth::user()->credit->value,0,",",".") . ' VNĐ' }} <br>
                              - Số tiền nạp: {{ number_format($pay_in_office->money,0,",",".") . ' VNĐ' }} <br>
                              - Mã giao dịch: {{ $pay_in_office->ma_gd }} <br>
                              - Hướng dẫn: nạp tiền vào tài khoản <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-nap-tien-vao-tai-khoan-cloudzone-portal/" target="_blank">tại đây</a>. <br>
                              - <b class="payin-note">Lưu ý: </b> Quý khách ghi nội dung mã giao dịch khi thực hiện chuyển khoản. Hệ thống sẽ tự đọng cập nhật số dư tài khoản <br>
                          </div>
                      </div>
                  </div>
                @endif
            </div>
            <div class="col-md-12 col text-center back-to-home pt-4 mt-4">
                <a href="{{ route('index') }}"><i class="fas fa-angle-left"></i> Trở về trang chủ</a>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
@endsection
