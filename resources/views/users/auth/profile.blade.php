@php
$user = UserHelper::get_user(Auth::user()->id);
$total_product = GroupProduct::get_total_product(Auth::user()->id);
$total_invoice = GroupProduct::get_total_invoice_pending(Auth::user()->id);
$total_server = GroupProduct::get_total_server(Auth::user()->id);
$total_vps = GroupProduct::get_total_vps(Auth::user()->id);
$total_hosting = GroupProduct::get_total_hosting(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" type="text/css"
    href="{{ url('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
<i class="feather icon-user bg-c-blue"></i> Thông tin tài khoản
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> {{ $user->name }}</li>
@endsection
@section('content')
<!-- Content Row -->
<div class="col-md-12 detail-course-finish p-0 rounded">
    @if(session("success") && empty(session("warning")))
        <div class="bg-success rounded" style="width:100%;">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("success") && session("warning"))
        <div class="bg-success rounded" style="width:100%;">
            <p class="text-light">{{session("success")}}</p>
        </div>
        <div class="bg-warning rounded" style="width:100%">
            <p class="text-light" style="padding: 10px 25px 10px;">{{session("warning")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger rounded" style="width:100%;">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger rounded" style="margin-top:20px;width:100%;">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="row">
    <div class="col-xl-4 col-lg-5 col-md-5 col-sm-12">
        <div class="box box-primary">
            <div class="box-body">
                <h3 style="font-size: 20px;text-transform: uppercase;margin-top: 14px;font-weight: 600">Thông tin dịch
                    vụ</h3>
                <p><b>Tổng số dịch vụ / sản phẩm: </b> {{ $total_invoice }}</p>
                <p>
                    @if( !empty($user->credit->value) )
                        <b>Số dư tài khoản: </b> <span class="text-danger"> {!!number_format($user->credit->value,0,",",".")
                            . ' VNĐ'!!}</span>
                    @else
                        <b>Số dư tài khoản: </b> <span class="text-danger"> 0 VNĐ</span>
                    @endif
                </p>
                <p>
                    @if( !empty($user->credit->total) )
                        <b>Tổng tiền đã nạp: </b> <span class="text-danger">{!!number_format($user->credit->total,0,",",".")
                        . ' VNĐ'!!}</span> </p>
                    @else
                        <b>Số dư tài khoản: </b> <span class="text-danger"> 0 VNĐ</span>
                    @endif
                <hr>
                <div class="text-center">
                    <a href="{{ route('user.pay_in_online') }}" class="btn btn-warning btn-icon-split btn-sm">
                        <span class="icon"><i class="far fa-money-bill-alt"></i></span>
                        <span class="text">Nạp tiền</span>
                    </a>
                    <a href="{{ route('user.history_payment') }}" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon"><i class="fas fa-donate nav-icon"></i></span>
                        <span class="text">Tất cả giao dịch</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-8 col-lg-7 col-md-7 col-sm-12">
        <div class="box box-primary">
            <div class="box-body">
                <form action="{{ route('update_profile') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group mb-5 mt-3">
                        <div class="profile-avatar">
                            <img src="@php
                          if(!empty($user->user_meta->avatar)) {
                          if (!empty(Storage::disk('public')->exists($user->user_meta->avatar))) {
                          echo Storage::url($user->user_meta->avatar);
                      } else {
                      echo $user->user_meta->avatar;
                  }
                } else {
                echo asset('images/avatar5.png');
                }
                @endphp" alt="avatar" width="auto" height="175">
                        </div>
                        <div class="update-avatar custom-file text-center">
                            <input type="file" id="validatedCustomFile" name="avatar" value=""
                                placeholder="Cập nhật avatar">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Họ và tên</label>
                            <input type="text" name="name" value="{{ $user->name }}" class="form-control">
                        </div>
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Email</label>
                            <input type="text" name="email" value="{{ $user->email }}" disabled class="form-control">
                        </div>
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Số điện thoại</label>
                            <input type="text" name="phone" value="{{ $user->user_meta->phone }}" class="form-control">
                        </div>
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Ngày sinh</label>
                            <input type="text" name="date" value="{{ $user->user_meta->date }}" id="datepicker"
                                class="form-control">
                        </div>
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Giới tính</label>
                            <select class="form-control" name="gender">
                                <option value="" disabled selected>Chọn giới tính</option>
                                <option value="Nam" {{ $user->user_meta->gender == 'Nam' ? 'selected' : '' }}>Nam
                                </option>
                                <option value="Nữ" {{ $user->user_meta->gender == 'Nữ' ? 'selected' : '' }}>Nữ</option>
                            </select>
                        </div>
                        <div class="col-xl-6 col-lg-12 mb-4">
                            <label for="">Địa chỉ</label>
                            <input type="text" name="address" value="{{ $user->user_meta->address }}"
                                class="form-control">
                        </div>
                    </div>
                    <div class="row form-group add-cmnd rounded" style="margin: 20px 0;border: dashed 2px rgb(223, 74, 88);padding-bottom: 20px;display: none;">
                        <div class="col-md-10 text-center mx-auto mt-4">
                            <label for="">Số chứng minh nhân dân</label>
                            <input type="number" name="cmnd" value="{{ $user->user_meta->cmnd }}" class="form-control" placeholder="Nhập số chứng minh thư">
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-12 text-center mt-3">
                            <p><b>Ảnh CMND mặt trước</b></p>
                            @if (!empty($user->user_meta->cmnd_before))
                                <a href="{{ asset($user->user_meta->cmnd_before) }}" target="_blank">
                                    <img src="{{ asset($user->user_meta->cmnd_before) }}" alt="Scan CMND mặt trước" style="width: 200px; height: 120px">
                                </a> <br>
                            @endif
                            @if ($user->cmnd_verifies->count()>0)
                                @if ( !empty($user->cmnd_verifies) && ($user->cmnd_verifies->last()->active === 0) )
                                    <div class="bg-danger p-2" style="width:200px; margin:auto">Chờ xác nhận</div>
                                @else
                                    <div class="custom-file mt-3" style="width: 200px">
                                        <input type="file" class="custom-file-input" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước">
                                        <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-before">Chọn ảnh</label>
                                    </div>
                                @endif
                            @else 
                                <div class="custom-file mt-3" style="width: 200px">
                                    <input type="file" class="custom-file-input" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước">
                                    <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-before">Chọn ảnh</label>
                                </div>
                            @endif
                        </div>
                        <div class="col-xl-6 col-lg-12 col-md-12 text-center mt-3">
                            <p><b>Ảnh CMND mặt sau</b></p>
                            @if (!empty($user->user_meta->cmnd_after))
                                <a href="{{ url($user->user_meta->cmnd_after) }}" target="_blank">
                                    <img src="{{ asset($user->user_meta->cmnd_after) }}" alt="Scan CMND mặt sau" style="width: 200px; height: 120px">
                                </a> <br>
                            @endif
                            @if ($user->cmnd_verifies->count()>0)
                                @if ( !empty($user->cmnd_verifies) && ($user->cmnd_verifies->last()->active === 0) )
                                    <div class="bg-danger p-2" style="width:200px; margin:auto">Chờ xác nhận</div>
                                @else
                                    <div class="custom-file mt-3" style="width: 200px">
                                        <input type="file" class="custom-file-input" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau">
                                        <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-after">Chọn ảnh</label>
                                    </div>
                                @endif
                            @else 
                                <div class="custom-file mt-3" style="width: 200px">
                                    <input type="file" class="custom-file-input" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau">
                                    <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-after">Chọn ảnh</label>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <div class="float-left">
                                <button
                                    class="btn waves-effect waves-dark btn-primary btn-outline-primary btn-add-cmnd"><i
                                        class="fa fa-user"></i> Thêm CMND</button>
                            </div>
                            <div class="float-right">
                                <input type="submit" value="Cập nhập" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script src="{{ url('/libraries/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true
        });
        $('.btn-add-cmnd').click(function(e){
            e.preventDefault();
            $('.add-cmnd').toggle(500);
        });
    });
    //Chọn file ảnh
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
</script>
@endsection