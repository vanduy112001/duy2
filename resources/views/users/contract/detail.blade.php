@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fa fa-paste bg-c-blue"></i> Hợp đồng @if ($contract->status === 'pending') (Đang chờ xét duyệt) @endif
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-paste"></i> Tạo hợp đồng</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>  
    <div class="box box-primary">
        <form action="{{ route('user.contract.update', ['id' => $contract->id ]) }}" method="post">
            <div class="box-body">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="company">Công ty</label>
                                <input type="text" id="company" name="company" value="{{ $contract->company ? $contract->company : '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="deputy">Người đại diện</label>
                                <input type="text" id="deputy" name="deputy" value="{{ $contract->deputy ? $contract->deputy : '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="deputy_position">Chức vụ người đại diện</label>
                                <input type="text" id="deputy_position" name="deputy_position" value="{{ $contract->deputy_position ? $contract->deputy_position : '' }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="address">Địa chỉ</label>
                                <input type="text" id="address" name="address" value="{{ $contract->address ? $contract->address : '' }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" value="{{ $contract->email ? $contract->email : '' }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" id="phone" name="phone" value="{{ $contract->phone ? $contract->phone : '' }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="mst">Mã số thuế</label>
                                <input type="text" id="mst" name="mst" value="{{ $contract->mst ? $contract->mst : '' }} " class="form-control">
                            </div>
                        </div>
                    </div>
            </div>
            <hr>
            <div class="row mt-3 mb-5 row-add-products">
                <div class="col-12">
                    <h4 class="text-center mb-4">Thêm thông tin sản phẩm trong hợp đồng</h4>
                </div>
                <div class="col-12 show-products">
                    <table class="table table-bordered table-sm table-hover">
                        
                        @if ($contract->contract_details)
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Loại sản phẩm</th>
                                <th>Thời gian thuê</th>
                                <th>Đơn giá(vnđ)</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contract->contract_details as $key => $contract_detail)
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td>
                                        @if ($contract_detail->product_type === 'vps')
                                            @php $vps = UserHelper::get_contract_detail_vps($contract_detail->target_id) @endphp
                                            <span class="btn btn-info btn-sm">Vps</span> IP <a href="{{ route('admin.vps.detail', ['id' => $vps->id]) }}" target="_blank">{{ $vps->ip ? $vps->ip : '' }}</a> - Type: {{ $vps->product->name ? $vps->product->name : '' }}
                                        @elseif($contract_detail->product_type === 'server')
                                            @php $server = UserHelper::get_contract_detail_server($contract_detail->target_id) @endphp
                                            <span class="btn btn-danger btn-sm">Server</span> IP: <a href="{{ route('admin.server.detail', ['id' => $server->id]) }}" target="_blank">{{ $server->ip ? $server->ip : '' }}</a> - Type: {{ $server->type ? $server->type : '' }} - Name: {{ $server->server_name ? $server->server_name : '' }}
                                        @elseif($contract_detail->product_type === 'hosting')
                                            @php $hosting = UserHelper::get_contract_detail_hosting($contract_detail->target_id) @endphp
                                            <span class="btn bg-indigo btn-sm">Hosting</span> <a href="{{ route('admin.hosting.detail', ['id' => $hosting->id]) }}" target="_blank">{{ $hosting->domain ? $hosting->domain : '' }} </a>
                                        @elseif($contract_detail->product_type === 'domain')
                                            @php $domain = UserHelper::get_contract_detail_domain($contract_detail->target_id) @endphp
                                            <span class="btn btn-success btn-sm">Domain</span> <a href="{{ route('admin.domain.detail', ['id' => $domain->id]) }}" target="_blank">{{ $domain->domain ? $domain->domain : '' }}</a>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : ''}}</td>
                                    <td class="text-center">{{ $contract_detail->amount ? number_format($contract_detail->amount, 0, ',', '.') : ''}}</td>
                                    <td class="text-center">{{ $contract_detail->note ? $contract_detail->note : ''}}</td>
                                    <td>
                                        <a class="btn btn-danger btn-sm delete-contract-product" href="javascript:void(0)" data-id="{{ $contract_detail->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                        
                    </table>

                    <div class="col-12 text-center">
                        <button class="btn btn-sm btn-success add-products" data-toggle="modal" data-target="#add-products"><i class="fa fa-plus" aria-hidden="true"></i> Thêm sản phẩm</button>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('user.contract.index') }}" class="btn btn-sm btn-secondary p-1"><i class="fas fa-angle-double-left"></i> Danh sách hợp đồng</a>
                <div class="float-right">
                    <input type="submit" class="btn btn-sm btn-primary p-1" value="Cập nhập">
                </div>
            </div>
        </form>
    </div>

    <!-- Modal thêm sản phẩm -->
    <div class="modal fade" id="add-products" tabindex="-1" role="dialog" aria-labelledby="add-products" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 class="add-product-title"><b>Thêm sản phẩm cho hợp đồng</b></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <form action="" method="POST" id="form-create-product">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="product_type">Chọn loại sản phẩm</label>
                                <select class="form-control select2" name="add_product_type" id="add_product_type" data-user-id="{{ $contract->user_id }}" required style="width:100%;">
                                    <option value="" disabled selected>Chọn loại sản phẩm</option>
                                    <option value="vps">VPS</option>
                                    <option value="hosting">Hosting</option>
                                    <option value="domain">Domain</option>
                                    <option value="server">Server</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12" id="load-taget">
                            <div class="form-group">
                                <label for="product_type">Chọn sản phẩm</label>
                                <select class="form-control select2" name="add_target_id" id="add_target_id" required style="width:100%;">
                                    <option value="" disabled selected>Chọn sản phẩm</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 target-info">
                            
                        </div>
                        <input type="hidden" id="add_contract_id" name="add_contract_id" value="{{ $contract->id }}" class="form-control">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            <button type="button" class="btn btn-primary" id="submit-add-product">Thêm</button>
            </div>
        </div>
        </div>
    </div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        // $('#date_create').datepicker({
        //   autoclose: true,
        //   format: 'dd-mm-yyyy'
    	// });
        // $('#date_end').datepicker({
        //   autoclose: true,
        //   format: 'dd-mm-yyyy'
        // });

        $('.add-products').on('click', function(e){
            e.preventDefault();
        });

        $('#add-products #add_product_type').on('change', function() {
            var product_type = $(this).val();
            var html = '<option value="" disabled selected>Chọn sản phẩm</option>';
            $.ajax({
                url: "{{ route('user.contract.get_target_from_user') }}",
                data: {product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {
                    $('#add-products #load-taget #add_target_id').html(html);
                },
                success: function(data) {
                    console.log(data);
                    if (data && data.length > 0) {
                        if (product_type === 'vps' || product_type === 'server') {
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - IP: ' + target.ip + '</option>';
                            });
                        } else if (product_type === 'domain'){
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - Domain: ' + target.domain + '</option>';
                            }); 
                        } else if (product_type === 'hosting'){
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - Hosting: ' + target.domain + '</option>';
                            }); 
                        }
                        $('#add-products #load-taget #add_target_id').html(html);
                        $('#add-products [type="text"]').prop('disabled', false);
                    } else {
                        $('#add-products #load-taget #add_target_id').html(html);
                        $('#add-products [type="text"]').prop('disabled', true);
                    }
                    // console.log(data);
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        $('#add-products #add_target_id').on('change', function(){
            var target_id = $(this).val();
            var product_type = $('#add-products #add_product_type').val();
            var html = '';
            $.ajax({
                url: "{{ route('user.contract.get_target_detail') }}",
                data: {target_id: target_id, product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {
                },
                success: function(data) {
                    console.log(data);
                },
                error: function(e) {
                    console.error(e);
                } 
            });
        });

        $('#add-products #submit-add-product').on('click', function() {
            var data = $('form#form-create-product').serialize();
            $.ajax({
                url: "{{ route('user.contract.create_contract_detail') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}", 
                    data
                },
                beforeSend: function(){
                    $('#add-products .modal-body').html('<div class="text-center"><div class="spinner-border spinner-border-sm text-info" ></div></div>');
                },
                success: function(data) {
                    console.log(data);
                    if (data.status === true) {
                        $('#add-products .modal-body').html('<p class="text-success text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('user.contract.detail', [ 'id' => $contract->id ]) }}" class="btn btn-success">Hoàn thành</a>');
                    } else if(data.status === false) {
                        $('#add-products .modal-body').html('<p class="text-danger text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('user.contract.detail', [ 'id' => $contract->id ]) }}" class="btn btn-default">Hủy</a>');
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

    })
</script>

@endsection
