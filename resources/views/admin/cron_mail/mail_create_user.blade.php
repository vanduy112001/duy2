<!DOCTYPE html>
<html>
<head>
  <title>Cloudzone Customer System</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
</head>
<body style="width: 100%;margin: auto;">
	<div class="send-mail" style="width: 90%;border: 1px solid #ddd;padding-top: 25px;padding-bottom: 10px;">
		<div class="mail-logo" style="text-align: center;border-bottom: 1px solid #ddd;padding-bottom: 30px;">
			<img src="https://portal.cloudzone.vn/images/logo-cloudzone-mail.png" alt="logo-mail" width="200px">
		</div>
		<div class="mail-content" style="font-size: 16px;font-family: Arial,sans-serif;padding-top: 35px;padding-bottom:35px;margin: 10px 10%;">
            <!-- Nội dung mail tạo tài khoản -->
            Kính chào {{ $name }}, <br>
            Chúc mừng Quý khách hàng đã đăng ký thành công tài khoản sử dụng Portal của Cloudzone<br>
            Dưới đây là thông tin đăng nhập tài khoản khách hàng của Quý khách: <br>
            - <b>Tài khoản:</b> {{ $email }} <br>
            - <b>Mật khẩu:</b> {{ $password }} <br>
            - <b>Liên kết đăng nhập:</b> <a href="https://portal.cloudzone.vn/">Portal Cloudzone</a> <br>
            <!-- kết thúc mail tạo tài khoản -->
            Với tài khoản này, Quý khách hoàn toàn có thể chủ động:<br>
            <ul>
                <li>
                    Đặt mua các dịch vụ: Cloud VPS Việt Nam, VPS US/UK/…., đăng ký hosting email, tên miền
                </li>
                <li>
                    Quản lý dịch vụ đang dùng: tự động gia hạn dịch vụ và thanh toán trực tuyến các hóa đơn
                </li>
            </ul>
            Tham khảo bảng giá các gói dịch vụ của chúng tôi: <br>
            <ul>
                <li>
                    Dịch vụ Cloud Hosting:
                    <a href="https://cloudzone.vn/cloud-hosting/">https://cloudzone.vn/cloud-hosting/</a>
                </li>
                <li>
                    Dịch vụ Cloud VPS:
                    <a href="https://cloudzone.vn/cloud-server/">https://cloudzone.vn/cloud-server/</a>
                </li>
            </ul>
            Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ. <br>
            Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br>
            Trân trọng! <br>
		</div>
	</div>
  <div class="mail-footer" style="width: 90%;border: 1px solid #ddd;background: #f2f2f2;">
		<div style="padding: 15px 4% 10px;text-align: center;font-size: 13px;font-family: roboto;">
			<b>Công ty TNHH MTV Công nghệ Đại Việt Số</b><br>
            Địa chỉ: 257 Lê Duẩn , Đà Nẵng. <br>
            Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng. <br>
            Điện thoại: 08 8888 0043 - Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a><br>
            Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a>
            - Fanpage: <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a>
            <br>
            <div style="padding-top: 5px;">
                <a href="https://m.me/cloudzone.vn" style="padding-right: 5px;"><img src="https://portal.cloudzone.vn/images/icon-facebook.png" width="30px"></a>
            </div>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>
