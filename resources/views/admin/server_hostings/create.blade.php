@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Tạo Server Hosting
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Server Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Thêm Server Hosting</h3>
    </div>
    <form action="{{ route('admin.server_hosting.store') }}" method="post" class="p-4">
        <div class="box-body">
            <div class="form-group">
                <label for="name">Tên Server</label>
                <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" placeholder="Server Name">
            </div>
            <div class="form-group">
                <label for="type_server_hosting">Loại Server Hosting</label>
                <select class="form-control" id="type_server_hosting" name="type_server_hosting">
                    @foreach($type_server_hosting as $type)
                        <?php
                            $selected = '';
                            if ($type == old('type_server_hosting')) {
                                $selected = 'selected';
                            }
                         ?>
                         <option value="{{ $type }}" {{ $selected }}>{{ $type }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="host">Domain</label>
                <input type="text" name="host" id="host" class="form-control" value="{{ old('host') }}" placeholder="Domain">
            </div>
            <div class="form-group">
                <label for="post">Port</label>
                <input type="text" name="port" class="form-control" id="post" value="{{ old('post') }}" placeholder="Post">
            </div>
            <div class="form-group">
                <label for="ip">IP</label>
                <input type="text" name="ip" class="form-control" id="ip" value="{{ old('ip') }}" placeholder="IP của Server">
            </div>
            <div class="form-group">
                <label for="ip">Location</label>
                <select class="form-control" name="location">
                    @foreach($location_hosting as $key => $location)
                      <?php
                          $selected = '';
                          if ($key == old('location')) {
                            $selected = 'selected';
                          }
                       ?>
                       <option value="{{$key}}">{{$location}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="user_name" >Username</label>
                <input type="text" name="user_name" class="form-control" id="user_name" value="{{ old('user_name') }}" placeholder="User">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control" id="password" value="{{ old('password') }}" placeholder="Password">
            </div>
            <div class="form-group">
                <button type="button" name="button" class="btn btn-default" id="test">Kiểm tra kết nối</button>
                <div id="notication-test" class="mb-2  mt-2">

                </div>
            </div>
        </div>
        <div class="card-footer text-right button-form">
            @csrf
            <input type="submit" value="Thêm Server Hosting" class="btn btn-primary">
        </div>
    </form>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-invoice" data-type="mark_paid">Thanh toán</button>
            <button class="btn btn-outline-warning btn-action-invoice" data-type="unpaid">Chưa thanh toán</button>
            <button class="btn btn-outline-secondary btn-action-invoice" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-invoice" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="delete-invoice">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-invoice" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="create_server_hosting">
@endsection
@section('scripts')
<script src="{{ asset('js/create_server_hosting.js') }}"></script>
@endsection
