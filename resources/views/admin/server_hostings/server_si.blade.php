@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Server Hosting Singapore
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Server Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
              <a href="{{ route('admin.server_hosting.create_server_hosting') }}" class="btn btn-primary">Thêm Server Hosting</a>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <!-- <th></th> -->
                    <th>ID</th>
                    <th>Serve Name</th>
                    <th>Host</th>
                    <th>Địa điểm</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    <tr>
                        <td>#0</td>
                        <td>Server DaPortal</td>
                        <td>{{ $default['host'] }}</td>
                        <td>Việt Nam</td>
                        <td>Mặc định</td>
                        <td>
                            <a href="{{ route('admin.hostings.getHostingSingapore', 0) }}" class="btn btn-info" style="font-size: 10px;" data-toggle="tooltip" title="Danh sách Hosting"><i class="fas fa-chart-bar"></i></a>
                            <form action="{{ route('admin.server_hosting.active') }}" method="post" style="display:inline;">
                                @csrf
                                <input type="hidden" name="id" value="0">
                                <input type="hidden" name="type" value="active">
                                <button type="submmit" name="button" data-name="Server Hosting mặc định" class="btn btn-success active-server-hosting" data-toggle="tooltip" title="Kích hoạt Server Hosting"><i class="fas fa-check-circle"></i></button>
                            </form>
                        </td>
                    </tr>
                        @foreach($server_hostings as $server_hosting)
                            <tr>
                                <td>
                                    <a href="{{ route('admin.server_hosting.edit_server_hosting', $server_hosting->id) }}">#{{ $server_hosting->id }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.server_hosting.edit_server_hosting', $server_hosting->id) }}">{{ $server_hosting->name }}</a>
                                </td>
                                <td>
                                    {{ $server_hosting->host }}
                                </td>
                                <td>
                                   @if($server_hosting->location == 'vn')
                                      Việt Nam
                                   @elseif($server_hosting->location == 'si')
                                      Singapore
                                   @else
                                      US
                                   @endif
                                </td>
                                <td class="status">
                                    @if($server_hosting->active)
                                      <b class="text-success">Kích hoạt</b>
                                    @else
                                      <b class="text-danger">Chưa kích hoạt</b>
                                    @endif
                                </td>
                                <td>
                                    <a class="btn btn-info" href="{{ route('admin.hostings.getHostingSingapore', $server_hosting->id) }}" style="font-size: 10px;" data-toggle="tooltip" title="Danh sách Hosting"><i class="fas fa-chart-bar"></i></a>
                                    @if(!$server_hosting->active)
                                        <form action="{{ route('admin.server_hosting.active') }}" method="POST" style="display:inline;">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $server_hosting->id }}">
                                            <input type="hidden" name="type" value="active">
                                            <button type="submmit" name="button" class="btn btn-success active-server-hosting" data-name="{{ $server_hosting->name }}" data-toggle="tooltip" title="Kích hoạt Server Hosting"><i class="fas fa-check-circle"></i></button>
                                        </form>
                                    @else
                                        <form action="{{ route('admin.server_hosting.active') }}" method="POST" style="display:inline;">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $server_hosting->id }}">
                                            <input type="hidden" name="type" value="deactive">
                                            <button type="submmit" name="button" class="btn btn-info deactive-server-hosting" data-name="{{ $server_hosting->name }}" data-toggle="tooltip" title="Tắt kích hoạt Server Hosting"><i class="fas fa-times-circle"></i></button>
                                        </form>
                                    @endif
                                    <button type="button" name="button" data-id="{{ $server_hosting->id }}" data-name="{{ $server_hosting->name }}" class="btn btn-danger delete-server-hosting" data-toggle="tooltip" title="Xóa Server Hosting"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="10" class="text-center">
                        {{ $server_hostings->links()  }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-invoice" data-type="mark_paid">Thanh toán</button>
            <button class="btn btn-outline-warning btn-action-invoice" data-type="unpaid">Chưa thanh toán</button>
            <button class="btn btn-outline-secondary btn-action-invoice" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-invoice" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="delete_invoice">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_invoice">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button_finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="list_server_hosting_si">
@endsection
@section('scripts')
<script src="{{ asset('js/admin_server_hosting.js') }}"></script>
@endsection
