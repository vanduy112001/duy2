@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/lou-multi-select/css/multi-select.css') }}" />
@endsection
@section('title')
    <i class="nav-icon fas fa-cubes"></i> Quản lý thông tin sản phẩm
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Sản phẩm</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa sản phẩm</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <h3>Chỉnh sửa sản phẩm</h3>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <form action="{{ route('admin.product_private.update') }}" method="post">
                @csrf
                <div class="card card-default">
                    <div class="card-header">
                        <ul  class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="custom-tabs-two-home-tab" data-toggle="pill" href="#detail">Thông tin sản phẩm</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="custom-tabs-two-pricing-tab" data-toggle="pill" href="#pricing">Pricing</a>
                            </li>
                            <li class="nav-item"><a class="nav-link" id="custom-tabs-two-module-tab" data-toggle="pill" href="#module">Package</a></li>
                            <li class="nav-item"><a class="nav-link" id="custom-tabs-two-module-tab-upgrate" data-toggle="pill" href="#upgrate">Nâng cấp</a></li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div id="custom-tabs-two-tabContent" class="tab-content">
                            {{-- chi tiết sản phẩm --}}
                            <div id="detail" class="tab-pane fade show active" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="name" >Tên sản phẩm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="name" value="{{ $product->name }}" class="form-control" id="name" placeholder="Tên sản phẩm">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="group_product" >Nhóm sản phẩm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <select name="group_product_id" id="group_product" class="form-control">
                                            <option value="" disabled>--- Chọn nhóm sản phẩm</option>
                                            @foreach ($group_products as $group_product)
                                                @php
                                                    $selected = '';
                                                    if($group_product->id == $product->group_product_id ) {
                                                        $selected = 'selected';
                                                    }
                                                @endphp
                                                <option value="{{ $group_product->id  }}" {{$selected}}>{{ $group_product->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="type_product" >Loại sản phẩm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <select name="type_product" id="type_product" class="form-control">
                                            <option value="" disabled>--- Chọn loại sản phẩm</option>
                                            @foreach ($type_products as $type_product)
                                                @php
                                                    $selected = '';
                                                    if($type_product == $product->type_product ) {
                                                        $selected = 'selected';
                                                    }
                                                @endphp
                                                <option value="{{ $type_product  }}" {{$selected}}>{{ $type_product }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div id="config">
                                    @if ($product->type_product == 'Server')
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="name_server" class="">Tên máy chủ</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="name_server" value="{{ !empty($product->meta_product->name_server) ? $product->meta_product->name_server : '' }}" class="form-control" id="name_server" placeholder="Tên máy chủ">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="cpu" class="">CPU</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="cpu" value="{{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '' }}" class="form-control" id="cpu" placeholder="CPU">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="cores" class="">Cores</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="cores" value="{{ !empty($product->meta_product->cores) ? $product->meta_product->cores : '' }}" class="form-control" id="cores" placeholder="Cores">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="ram" class="">RAM</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="ram" value="{{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '' }}" class="form-control" id="ram" placeholder="Memory">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="chassis" class="">Số lượng ổ đĩa</label>
                                            </div>
                                            <div class="col-md-10">
                                                <select name="chassis" id="chassis" class="form-control">
                                                    @if(!empty( $product->meta_product->chassis ))
                                                        <option value="4" @if($product->meta_product->chassis == 4) selected @endif>4 ổ đĩa (SATA/SSD)</option>
                                                        <option value="8" @if($product->meta_product->chassis == 8) selected @endif>8 ổ đĩa (SATA/SSD)</option>
                                                    @else
                                                        <option value="4">4 ổ đĩa (SATA/SSD)</option>
                                                        <option value="8">8 ổ đĩa (SATA/SSD)</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div id="chassisDrive">
                                            @if ( !empty( $product->meta_product->chassis ) )
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive1" class="">Disk số 1</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive1" value="{{ $product->product_drive->first }}" class="form-control" id="drive1" placeholder="Disk số 1">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive2" class="">Disk số 2</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive2" value="{{ $product->product_drive->second }}" class="form-control" id="drive2" placeholder="Disk số 2">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive3" class="">Disk số 3</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive3" value="{{ $product->product_drive->third }}" class="form-control" id="drive3" placeholder="Disk số 3">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive4" class="">Disk số 4</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive4" value="{{ $product->product_drive->four }}" class="form-control" id="drive4" placeholder="Disk số 4">
                                                    </div>
                                                </div>
                                                <div id="chassis8">
                                                    @if ( $product->meta_product->chassis == 8 )
                                                        <div class="row form-group">
                                                            <div class="col-md-2">
                                                                <label for="drive5" class="">Disk số 5</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="text" name="drive5" value="{{ $product->product_drive->five }}" class="form-control" id="drive5" placeholder="Disk số 5">
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-md-2">
                                                                <label for="drive6" class="">Disk số 6</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="text" name="drive6" value="{{ $product->product_drive->six }}" class="form-control" id="drive6" placeholder="Disk số 6">
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-md-2">
                                                                <label for="drive7" class="">Disk số 7</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="text" name="drive7" value="{{ $product->product_drive->seven }}" class="form-control" id="drive7" placeholder="Disk số 7">
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-md-2">
                                                                <label for="drive8" class="">Disk số 8</label>
                                                            </div>
                                                            <div class="col-md-10">
                                                                <input type="text" name="drive8" value="{{ $product->product_drive->eight }}" class="form-control" id="drive8" placeholder="Disk số 8">
                                                            </div>
                                                        </div>
                                                    @endif
                                                </div>
                                            @else
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive1" class="">Disk số 1</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive1" value="" class="form-control" id="drive1" placeholder="Disk số 1">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive2" class="">Disk số 2</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive2" value="" class="form-control" id="drive2" placeholder="Disk số 2">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive3" class="">Disk số 3</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive3" value="" class="form-control" id="drive3" placeholder="Disk số 3">
                                                    </div>
                                                </div>
                                                <div class="row form-group">
                                                    <div class="col-md-2">
                                                        <label for="drive4" class="">Disk số 4</label>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <input type="text" name="drive4" value="" class="form-control" id="drive4" placeholder="Disk số 4">
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="raid" class="">Raid</label>
                                            </div>
                                            <div class="col-md-10">
                                                {{-- <input type="text" name="raid" value="{{ !empty($product->meta_product->raid) ? $product->meta_product->raid : '' }}" class="form-control" id="raid" placeholder="Raid"> --}}
                                                <select class="form-control selectRaid" id="raid" multiple data-placeholder="Chọn Raid" name="raidSelect[]">
                                                    {{-- <option value="All">Chọn tất cả</option> --}}
                                                    @foreach ($config_raid as $raid)
                                                        @php
                                                            $selected = '';
                                                            if ( !empty( $product->product_raids ) ) {
                                                                foreach ($product->product_raids as $key => $product_raid) {
                                                                    if ($product_raid->raid == $raid) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $raid }}" {{ $selected }}>{{ $raid }}</option>
                                                    @endforeach
                                                </select>    
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="port_network" class="">Port Network</label>
                                            </div>
                                            <div class="col-md-10">
                                                <select name="port_network" id="port_network" class="form-control">
                                                    @if ( !empty($product->meta_product->port_network) )
                                                        <option value="100 Mb" {{ $product->meta_product->port_network == '100 Mb' ? 'selected' : '' }}>100 Mb</option>
                                                        <option value="1 Gb" {{ $product->meta_product->port_network == '1 Gb' ? 'selected' : '' }}>1 Gb</option>
                                                        <option value="10 Gb" {{ $product->meta_product->port_network == '10 Gb' ? 'selected' : '' }}>10 Gb</option>
                                                    @else
                                                        <option value="100 Mb">100 MB</option>
                                                        <option value="1 Gb">1 GB</option>
                                                        <option value="10 Gb">10 GB</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="bandwidth" class="">Băng thông</label>
                                            </div>
                                            <div class="col-md-10">
                                                <select name="bandwidth" id="bandwidth" class="form-control">
                                                    @if ( !empty($product->meta_product->bandwidth) )
                                                        <option value="100 Mb trong nước/10 Mb quốc tế (chia sẻ)" {{ $product->meta_product->bandwidth == '100 Mb trong nước/10 Mb quốc tế (chia sẻ)' ? 'selected' : '' }}>100 MB trong nước/10 MB quốc tế  (chia sẻ)</option>
                                                        <option value="1 Gb trong nước/10 Mb quốc tế (chia sẻ)" {{ $product->meta_product->bandwidth == '1 Gb trong nước/10 Mb quốc tế (chia sẻ)' ? 'selected' : '' }}>1 Gb trong nước/10 Mb quốc tế (chia sẻ)</option>
                                                    @else
                                                        <option value="100 Mb trong nước/10 Mb quốc tế (chia sẻ)">100 Mb trong nước/10 Mb quốc tế (chia sẻ)</option>
                                                        <option value="1 Gb trong nước/10 Mb quốc tế (chia sẻ)">1 Gb trong nước/10 Mb quốc tế (chia sẻ)</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="ip" class="">IP</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="ip" value="{{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '' }}" class="form-control" id="ip" placeholder="Số IP public">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="datacenter" class="">Datacenter</label>
                                            </div>
                                            <div class="col-md-10">
                                                <select class="form-control select2" id="datacenter" multiple data-placeholder="Chọn Datacenter" name="datacenterSelect[]">
                                                    {{-- <option value="All">Chọn tất cả</option> --}}
                                                    @foreach ($config_datacenter as $datacenter)
                                                        @php
                                                            $selected = '';
                                                            if ( !empty( $product->product_datacenters ) ) {
                                                                foreach ($product->product_datacenters as $key => $product_datacenter) {
                                                                    if ($product_datacenter->datacenter == $datacenter) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $datacenter }}" {{ $selected }}>{{ $datacenter }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="os_vps" class="">Hệ điều hành (cho Server)</label>
                                            </div>
                                            <div class="col-md-10" id="select_product_form">
                                                <select class="form-control select2" id="os_vps" multiple data-placeholder="Chọn hệ điều hành cho Server" name="os_vps[]">
                                                    @if (!empty($list_vps_os))
                                                      @foreach($config_os_server as $key => $os)
                                                        <?php
                                                            $selected = '';
                                                            foreach ($list_vps_os  as $vps_os) {
                                                               if ($key == $vps_os->os) {
                                                                 $selected = 'selected';
                                                               }
                                                            }
                                                        ?>
                                                        <option value="{{$key}}" {{$selected}}>{{$os}}</option>
                                                      @endforeach
                                                    @else
                                                      @foreach($config_os_server as $key => $os)
                                                        <option value="{{$key}}">{{$os}}</option>
                                                      @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <select class="form-control select2" id="server_management" multiple data-placeholder="Chọn Server Management" name="serverManagementSelect[]">
                                                @foreach ($config_server_management as $server_management)
                                                    @php
                                                        $selected = '';
                                                        if ( !empty( $product->server_managements ) ) {
                                                            foreach ($product->server_managements as $key => $server_management) {
                                                                if ($server_management->server_management == $server_management) {
                                                                    $selected = 'selected';
                                                                }
                                                            }
                                                        }
                                                    @endphp
                                                    <option value="{{ $server_management }}" {{ $selected }}>{{ $server_management }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    @if ($product->type_product == 'VPS' || $product->type_product == 'VPS-US' || $product->type_product == 'NAT-VPS')
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="cpu" >CPU</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="cpu" value="{{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '' }}" class="form-control" id="cpu" placeholder="CPU">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="ram" >RAM</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="ram" value="{{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '' }}" class="form-control" id="ram" placeholder="Memory">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="disk" >DISK</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="disk" value="{{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '' }}" class="form-control" id="disk" placeholder="Disk">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="bandwidth" >Băng thông</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="bandwidth" value="{{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '' }}" class="form-control" id="bandwidth" placeholder="Băng thông">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="ip" >IP</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="ip" value="{{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '' }}" class="form-control" id="ip" placeholder="Số IP public">
                                            </div>
                                        </div>
                                    @endif
                                    @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS' || $product->type_product == 'VPS-US')
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="os" >Hệ điều hành (content)</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="os" value="{{ !empty($product->meta_product->os) ? $product->meta_product->os : '' }}" class="form-control" id="os" placeholder="Hệ điều hành">
                                            </div>
                                        </div>
                                    @endif
                                    @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS')
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="os" >Hệ điều hành (cho VPS)</label>
                                            </div>
                                            <div class="col-md-10" id="select_product_form">
                                                <select class="form-control select2" id="os_vps" multiple data-placeholder="Chọn hệ điều hành cho VPS" name="os_vps[]">
                                                    @if (!empty($list_vps_os))
                                                      @foreach($config_os as $key => $os)
                                                        <?php
                                                            $selected = '';
                                                            foreach ($list_vps_os as  $vps_os) {
                                                               if ($key == $vps_os->os) {
                                                                 $selected = 'selected';
                                                               }
                                                            }
                                                        ?>
                                                        <option value="{{$key}}" {{$selected}}>{{$os}}</option>
                                                      @endforeach
                                                    @else
                                                      @foreach($config_os as $key => $os)
                                                        <option value="{{$key}}">{{$os}}</option>
                                                      @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($product->type_product == 'Colocation')
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="raid" class="">Loại U</label>
                                            </div>
                                            <div class="col-md-10">
                                                {{-- <input type="text" name="raid" value="{{ !empty($product->meta_product->raid) ? $product->meta_product->raid : '' }}" class="form-control" id="raid" placeholder="Raid"> --}}
                                                <select class="form-control" name="raidSelect[]">
                                                    {{-- <option value="All">Chọn tất cả</option> --}}
                                                    @foreach ($config_u as $u)
                                                        @php
                                                            $selected = '';
                                                            if ( !empty( $product->os ) ) {
                                                                if ($product->os == $u) {
                                                                    $selected = 'selected';
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $u }}" {{ $selected }}>{{ $u }}</option>
                                                    @endforeach
                                                </select>    
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="bandwidth" class="">Băng thông</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="bandwidth" value="{{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '' }}" class="form-control" id="bandwidth" placeholder="Băng thông">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="ip" class="">IP</label>
                                            </div>
                                            <div class="col-md-10">
                                                <input type="text" name="ip" value="{{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '' }}" class="form-control" id="ip" placeholder="Số IP public">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col-md-2">
                                                <label for="datacenter" class="">Datacenter</label>
                                            </div>
                                            <div class="col-md-10">
                                                <select class="form-control select2" id="datacenter" multiple data-placeholder="Chọn Datacenter" name="datacenterSelect[]">
                                                    {{-- <option value="All">Chọn tất cả</option> --}}
                                                    @foreach ($config_datacenter as $datacenter)
                                                        @php
                                                            $selected = '';
                                                            if ( !empty( $product->product_datacenters ) ) {
                                                                foreach ($product->product_datacenters as $key => $product_datacenter) {
                                                                    if ($product_datacenter->datacenter == $datacenter) {
                                                                        $selected = 'selected';
                                                                    }
                                                                }
                                                            }
                                                        @endphp
                                                        <option value="{{ $datacenter }}" {{ $selected }}>{{ $datacenter }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif
                                    @if ($product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore')
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="storage" >Storage</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="storage" value="{{ !empty($product->meta_product->storage) ? $product->meta_product->storage : '' }}" class="form-control" id="storage" placeholder="Storage">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="domain" >Số lượng domain</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="domain" value="{{ !empty($product->meta_product->domain) ? $product->meta_product->domain : '' }}" class="form-control" id="domain" placeholder="Số lượng domain">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="sub_domain" >Số lượng sub domain</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="sub_domain" value="{{ !empty($product->meta_product->sub_domain) ? $product->meta_product->sub_domain : '' }}" class="form-control" id="sub_domain" placeholder="Số lượng sub domain">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="alias_domain" >Alias domain</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="alias_domain" value="{{ !empty($product->meta_product->alias_domain) ? $product->meta_product->alias_domain : '' }}" class="form-control" id="alias_domain" placeholder="Alias Domain">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="database" >Database</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="database" value="{{ !empty($product->meta_product->database) ? $product->meta_product->database : '' }}" class="form-control" id="database" placeholder="Database">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="ftp" >Ftp</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="ftp" value="{{ !empty($product->meta_product->ftp) ? $product->meta_product->ftp : '' }}" class="form-control" id="ftp" placeholder="Ftp">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="panel" >Control panel</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="panel" value="{{ !empty($product->meta_product->panel) ? $product->meta_product->panel : '' }}" class="form-control" id="panel" placeholder="Control panel">
                                            </div>
                                        </div>
                                    @endif
                                    @if ( $product->type_product == 'Addon-VPS' || $product->type_product == 'Addon-Server' || $product->type_product == 'Addon-Colocation' )
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="cpu" >CPU</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="cpu" value="{{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '' }}" class="form-control" id="cpu" placeholder="CPU">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="ram" >RAM</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="ram" value="{{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '' }}" class="form-control" id="ram" placeholder="Memory">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="disk" >DISK</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="disk" value="{{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '' }}" class="form-control" id="disk" placeholder="Disk">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="type_addon" >Loại Addon</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <select class="form-control" id="type_addon" name="type_addon">
                                                    @foreach($type_addons as $key => $type_addon)
                                                      <?php
                                                        $selected = '';
                                                        if (!empty($product->meta_product->type_addon)) {
                                                            if ($product->meta_product->type_addon == $key) {
                                                              $selected = 'selected';
                                                            }
                                                        }

                                                      ?>
                                                      <option value="{{$key}}" {{ $selected }}>{{ $type_addon }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                    @endif
                                    @if ( $product->type_product == 'Addon-Hosting')
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="storage" >Storage</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="storage" value="{{ !empty($product->meta_product->storage) ? $product->meta_product->storage : '' }}" class="form-control" id="storage" placeholder="Storage">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <div class="col col-md-2">
                                                <label for="domain" >Số lượng domain</label>
                                            </div>
                                            <div class="col col-md-10">
                                                <input type="text" name="domain" value="{{ !empty($product->meta_product->domain) ? $product->meta_product->domain : '' }}" class="form-control" id="domain" placeholder="Số lượng domain">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="">Mô tả</label>
                                    </div>
                                    <div class="col-md-10">
                                        <textarea name="description" class="form-control" rows="4" cols="40" placeholder="Mô tả">{{ $product->description }}</textarea>
                                    </div>

                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">
                                        <label for="">Khuyến mãi</label>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="icheck-primary d-inline">
                                            <input class="custom-control-input mb-2" type="checkbox" name="promotion" id="promotion" value="1"  @if( !empty( $product->meta_product->promotion ) ) checked @endif>
                                            <label for="promotion"></label>
                                        </div>
                                        <textarea name="girf" id="girf" class="form-control mt-2" rows="4" cols="40" placeholder="Khuyến mãi">{{ !empty($product->meta_product->girf) ? $product->meta_product->girf : old('girf') }}</textarea>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <label for="email" >Email xác nhận</label>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="email_create" id="email" class="form-control">
                                            <option value="" disabled>--- Chọn email ---</option>
                                            @foreach ($emails as $group_email) {
                                                <optgroup label="{{ $group_email->name }}">
                                                    @foreach ($group_email->emails as $email)
                                                        @php
                                                          $selected = '';
                                                          if(!empty($product->meta_product->email_create)) {
                                                              if($email->id == $product->meta_product->email_create) {
                                                                  $selected = 'selected';
                                                              }
                                                          }
                                                        @endphp
                                                        <option value="{{$email->id}}" {{$selected}}>{{$email->name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <label for="email" >Email hoàn thành</label>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="email_id" id="email" class="form-control">
                                            <option value="" disabled>--- Chọn email ---</option>
                                            @foreach ($emails as $group_email) {
                                                <optgroup label="{{ $group_email->name }}">
                                                    @foreach ($group_email->emails as $email)
                                                        @php
                                                          $selected = '';
                                                          if(!empty($product->meta_product->email_id)) {
                                                              if($email->id == $product->meta_product->email_id) {
                                                                  $selected = 'selected';
                                                              }
                                                          }
                                                        @endphp
                                                        <option value="{{$email->id}}" {{$selected}}>{{$email->name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <label for="email_expired" >Email gia hạn</label>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="email_expired" id="email_expired" class="form-control" disabled>
                                            <option value="" disabled>--- Chọn email ---</option>
                                            @foreach ($emails as $group_email) {
                                                <optgroup label="{{ $group_email->name }}">
                                                    @foreach ($group_email->emails as $email)
                                                        @php
                                                          $selected = '';
                                                          if(!empty($product->meta_product->email_expired)) {
                                                              if($email->id == $product->meta_product->email_expired) {
                                                                  $selected = 'selected';
                                                              }
                                                          }
                                                        @endphp
                                                        <option value="{{$email->id}}" {{$selected}}>{{$email->name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <label for="email_expired_finish" >Email hoàn thành gia hạn</label>
                                    </div>
                                    <div class="col-md-10">
                                        <select name="email_expired_finish" id="email_expired_finish" class="form-control">
                                            <option value="" disabled>--- Chọn email ---</option>
                                            @foreach ($emails as $group_email) {
                                                <optgroup label="{{ $group_email->name }}">
                                                    @foreach ($group_email->emails as $email)
                                                        @php
                                                          $selected = '';
                                                          if(!empty($product->meta_product->email_expired_finish)) {
                                                              if($email->id == $product->meta_product->email_expired_finish) {
                                                                  $selected = 'selected';
                                                              }
                                                          }
                                                        @endphp
                                                        <option value="{{$email->id}}" {{$selected}}>{{$email->name}}</option>
                                                    @endforeach
                                                </optgroup>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if ( $product->type_product == 'Server' || $product->type_product == 'Colocation' )
                                    <div class="row form-group">
                                        <div class="col-md-2">
                                            <label for="email_config_finish" class="">Email hoàn thành cài đặt</label>
                                        </div>
                                        <div class="col-md-10">
                                            <select name="email_config_finish" id="email_config_finish" class="form-control">
                                                <option value="" disabled>--- Chọn email ---</option>
                                                @foreach ($emails as $group_email) {
                                                    <optgroup label="{{ $group_email->name }}">
                                                        @foreach ($group_email->emails as $email)
                                                            @php
                                                            $selected = '';
                                                            if(!empty($product->meta_product->email_config_finish)) {
                                                                if($email->id == $product->meta_product->email_config_finish) {
                                                                    $selected = 'selected';
                                                                }
                                                            }
                                                            @endphp
                                                            <option value="{{$email->id}}" {{$selected}}>{{$email->name}}</option>
                                                        @endforeach
                                                    </optgroup>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                @endif
                                <div class="row form-group clearfix">
                                    <div class="col-md-2">
                                        @if ( $product->type_product == 'VPS-US' )
                                            <label for="product_special" >VPS US Cloudzone</label>
                                        @else
                                            <label for="product_special" >Sản phẩm đặc biệt</label>
                                        @endif
                                    </div>
                                    <div class="col-md-10 icheck-primary d-inline">
                                        <input class="custom-control-input" type="checkbox" name="product_special" id="product_special" value="1"  @if( !empty( $product->meta_product->product_special ) ) checked @endif>
                                        <label for="product_special">
                                        </label>
                                    </div>
                                </div>
                                <div class="row form-group clearfix">
                                    <div class="col col-md-2">
                                        <label for="hidden" >Ẩn sản phẩm</label>
                                    </div>
                                    <div class="col col-md-10 icheck-primary d-inline">
                                        <input class="custom-control-input" type="checkbox" name="hidden" id="hidden" value="1"  @if( !empty( $product->hidden ) ) checked @endif>
                                        <label for="hidden">
                                        </label>
                                    </div>
                                </div>
                            </div>
                            {{-- /. chi tiết sản phẩm --}}
                            {{-- Giá --}}
                            <div id="pricing" class="tab-pane fade" role="tabpanel" aria-labelledby="custom-tabs-two-pricing-tab">
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="type_pricing" class="label2">Hình thức thanh toán</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <span class="mr-3">
                                          <label for="free_label">
                                              <input type="radio" id="free_label" class="type_pricing" name="type" value="free" @if(!empty($product->pricing->type) && $product->pricing->type == 'free') checked @endif>
                                              Free
                                          </label>
                                        </span>
                                        <span class="mr-3">
                                          <label for="one_time_label">
                                              <input type="radio" id="one_time_label" class="type_pricing" name="type" value="one_time" @if(!empty($product->pricing->type) && $product->pricing->type == 'one_time') checked @endif>
                                              Một lần
                                          </label>
                                        </span>
                                        <span class="mr-3">
                                          <label for="recurring_label">
                                              <input type="radio" id="recurring_label" class="type_pricing" name="type" value="recurring" @if(!empty($product->pricing->type) && $product->pricing->type == 'recurring') checked @endif>
                                              Định kỳ
                                          </label>
                                        </span>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="monthly" class="label2">1 tháng</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="monthly" value="{{ !empty($product->pricing->monthly) ? $product->pricing->monthly : 0 }}" class="form-control recurring" id="monthly" placeholder="1 tháng" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-md-2">
                                        <label for="monthly" class="label2">2 tháng</label>
                                    </div>
                                    <div class="col-md-10">
                                        <input type="text" name="twomonthly" value="{{ !empty($product->pricing->twomonthly) ? $product->pricing->twomonthly : 0 }}" class="form-control recurring" id="monthly" placeholder="1 tháng" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="quarterly" class="label2">3 tháng</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="quarterly" value="{{ !empty($product->pricing->quarterly) ? $product->pricing->quarterly : 0 }}" class="form-control recurring" id="quarterly" placeholder="3 tháng" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="semi_annually" class="label2">6 tháng</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="semi_annually" value="{{ !empty($product->pricing->semi_annually) ? $product->pricing->semi_annually : 0 }}" class="form-control recurring" id="semi_annually" placeholder="6 tháng" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="annually" class="label2">1 năm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="annually" value="{{ !empty($product->pricing->annually) ? $product->pricing->annually : 0 }}" class="form-control recurring" id="annually" placeholder="1 năm" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="biennially" class="label2">2 năm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="biennially" value="{{ !empty($product->pricing->biennially) ? $product->pricing->biennially : 0 }}" class="form-control recurring" id="biennially" placeholder="2 năm" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="triennially" class="label2">3 năm</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="triennially" value="{{ !empty($product->pricing->triennially) ? $product->pricing->triennially : 0 }}" class="form-control recurring" id="triennially" placeholder="3 năm" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "one_time" )  disabled @endif @endif>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-2">
                                        <label for="one_time_pay" class="label2">Một lần</label>
                                    </div>
                                    <div class="col col-md-10">
                                        <input type="text" name="one_time_pay" value="{{ !empty($product->pricing->one_time_pay) ? $product->pricing->one_time_pay : 0 }}" class="form-control one_time_pay" id="one_time_pay" placeholder="Một lần" @if( !empty($product->pricing->type)) @if($product->pricing->type == 'free' || $product->pricing->type == "recurring" )  disabled @endif @endif>
                                    </div>
                                </div>
                            </div>
                            {{-- ./ Giá --}}
                            {{--  Package --}}
                            <div id="module" class="tab-pane fade" role="tabpanel" aria-labelledby="custom-tabs-two-module-tab">
                                <input type="hidden" name="package_product" class="package" data-type="{{ $product->type_product }}" value="{{ !empty($product->package) ? $product->package : '' }}">
                                <div id="package">

                                </div>
                            </div>
                            {{-- /. Package --}}
                            {{-- Nâng cấp --}}
                            <div id="upgrate" class="tab-pane fade" role="tabpanel" aria-labelledby="custom-tabs-two-module-tab-upgrate">
                                @if ($product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore' )
                                  <select class="form-control" name="upgrate[]" multiple='multiple' id="upgrate_callbacks">
                                      @foreach ($list_product_hosting_default as $product_hosting)
                                          <?php
                                              $disabled = '';
                                              $selected = '';
                                              if ($product->id == $product_hosting->id) {
                                                 $disabled = 'disabled';
                                              }
                                              if (!empty($product->product_upgrates)) {
                                                  foreach ($product->product_upgrates as $upgrate) {
                                                     if ($upgrate->product_upgrate_id == $product_hosting->id) {
                                                       $selected = 'selected';
                                                     }
                                                  }
                                              }
                                          ?>
                                          <option value="{{ $product_hosting->id }}" {{ $selected }} {{ $disabled }}>{{ $product_hosting->name }} - {{ $product_hosting->group_product->name }}</option>
                                      @endforeach
                                  </select>
                                @endif
                            </div>
                            {{-- /. Nâng cấp --}}
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <input type="hidden" value="{{ $product->id }}" name="id">
                        <input type="hidden" name="grouUserId" value="{{ $grouUserId }}">
                        <input type="submit" value="Lưu thay đổi" class="btn btn-primary">
                        <a href="{{ route('admin.product.index') }}" class="btn btn-default">Hủy thay đổi</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="edit_product_private">

@endsection
@section('scripts')
<script src="{{ asset('/libraries/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/lou-multi-select/js/jquery.multi-select.js') }}" defer></script>
<script src="{{ asset('js/edit_product.js') }}"></script>
<script>
    $(document).ready(function() {
        bsCustomFileInput.init();

        $('.type_pricing').on('click', function() {
            // console.log($(this).val());
            if( $(this).val() == 'free' ) {
                $('.one_time_pay').attr('disabled', true);
                $('.recurring').attr('disabled', true);
            } else if ( $(this).val() == 'recurring' ) {
                $('.recurring').attr('disabled', false);
                $('.one_time_pay').attr('disabled', true);
            } else {
                $('.one_time_pay').attr('disabled', false);
                $('.recurring').attr('disabled', true);
            }

        });

        $('#upgrate_callbacks').multiSelect({});
    });
</script>
@endsection
