@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<i class="nav-icon fas fa-cubes"></i> Quản lý thông tin sản phẩm riêng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Sản phẩm</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <button class="btn btn-primary" id="createGroup">Tạo nhóm sản phẩm</button>
                <a href="{{ route('admin.product_private.createPrivate', $id) }}" class="btn btn-success">Tạo sản phẩm</a>
            </div>
            <!-- form search -->
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-hover table-bordered">
                <thead class="primary">
                    <th width="5%"></th>
                    <th>Tên sản phẩm</th>
                    <th>Loại</th>
                    <th>Thông tin</th>
                    <th>Giá</th>
                    <th>Trạng thái</th>
                    <th>Mô tả</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="idGroupUser" value="{{ $id }}">
{{-- Modal --}}
<div class="modal fade" id="modal-product">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tạo nhóm sản phẩm</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
            <form id="form-group-product" action="{{ route('admin.product.createPrivateProduct') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div id="notication">

                    </div>
                  <div class="form-group">
                      <label for="name">Tên nhóm sản phẩm</label>
                      <input type="text" id="name" name="name" class="form-control" placeholder="Tên nhóm sản phẩm">
                  </div>
                  <div class="form-group">
                      <label for="title">Tiều đề hiển thị</label>
                      <input type="text" id="title" name="title" class="form-control" placeholder="Tiêu đề hiển thị">
                  </div>
                  <div class="form-group">
                      <label for="type">Loại</label>
                      <select class="form-control" name="type" id="type">
                          <option value="" selected disabled>Chọn loại sản phẩm</option>
                          <option value="vps">VPS</option>
                          <option value="vps_us">VPS-US</option>
                          <option value="vps_uk">VPS-UK</option>
                          <option value="vps_sing">VPS-Singapore</option>
                          <option value="vps_hl">VPS-Hà Lan</option>
                          <option value="vps_uc">VPS-Úc</option>
                          <option value="vps_ca">VPS-Canada</option>
                          <option value="hosting">Hosting</option>
                          <option value="email_hosting">Email Hosting</option>
                          <option value="server">Server</option>
                          <option value="addon_server">Addon Server</option>
                          <option value="proxy">Proxy</option>
                          <option value="colocation">Colocation</option>
                          <option value="addon_colo">Addon Colocation</option>
                      </select>
                  </div>
                  <div class="form-group">
                      <label for="stt">Số thứ tự</label>
                      <input type="number" id="stt" name="stt" class="form-control" placeholder="Số thứ tự">
                  </div>
                  <div class="form-group">
                      <label for="link">Loại sản phẩm VPS</label>
                      <input type="link" id="link" name="link" class="form-control" placeholder="Loại sản phẩm VPS">
                  </div>
                  <div class="form-group row">
                      <div class="col-md-4">
                          <label>Ẩn nhóm sản phẩm</label>
                      </div>
                      <div class="col-md-8 icheck-primary d-inline">
                          <input type="checkbox" class="custom-control-input" name="hidden" id="hidden" value="1">
                          <label for="hidden"></label>
                      </div>
                  </div>
                  <div class="hidden">
                      <input type="text" id="action" name="action" value="create">
                      <input type="text" id="id-group" name="id" value="">
                  </div>
              </div>
              <div class="modal-footer justify-content-between">
                <input type="hidden" id="group_user_id" name="group_user_id" value="{{ $id }}">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <input type="submit" class="btn btn-primary" id="button-submit" value="Tạo nhóm sản phẩm">
              </div>
            </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <input type="submit" class="btn btn-primary" id="button-product" value="Xóa sản phẩm">
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_product_private">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript" src="{{ asset('js/product_privates.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
    });
</script>
@endsection
