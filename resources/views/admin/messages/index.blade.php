@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <style type="text/css">
      /*---------chat window---------------*/
      .container{
          max-width:900px;
      }
      .chat_list:hover {
        background: #f2f2f2;
      }
      .inbox_people {
        background: #fff;
        float: left;
        overflow: hidden;
        width: 30%;
        border-right: 1px solid #ddd;
      }

      .inbox_msg {
        border: 1px solid #ddd;
        clear: both;
        overflow: hidden;
      }

      .top_spac {
        margin: 20px 0 0;
      }

      .recent_heading {
        float: left;
        width: 40%;
      }

      .srch_bar {
        display: inline-block;
        text-align: right;
        width: 60%;
        padding:
      }

      .headind_srch {
        padding: 10px 29px 10px 20px;
        overflow: hidden;
        border-bottom: 1px solid #c4c4c4;
      }

      .recent_heading h4 {
        color: #0465ac;
          font-size: 16px;
          margin: auto;
          line-height: 29px;
      }

      .srch_bar input {
        outline: none;
        border: 1px solid #cdcdcd;
        border-width: 0 0 1px 0;
        width: 80%;
        padding: 2px 0 4px 6px;
        background: none;
      }

      .srch_bar .input-group-addon button {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        padding: 0;
        color: #707070;
        font-size: 18px;
      }

      .srch_bar .input-group-addon {
        margin: 0 0 0 -27px;
      }

      .chat_ib h5 {
        font-size: 15px;
        color: #464646;
        margin: 0 0 8px 0;
      }

      .chat_ib h5 span {
        font-size: 13px;
        float: right;
      }

      .chat_ib p {
          font-size: .8125rem;
          color: #989898;
          margin: auto;
          display: inline-block;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
      }

      .chat_img {
        float: left;
        width: 11%;
      }

      .chat_img img {
        width: 100%
      }

      .chat_ib {
        float: left;
        padding: 0 0 0 15px;
        width: 88%;
      }

      .chat_people {
        overflow: hidden;
        clear: both;
      }

      .chat_list {
        cursor: pointer;
        border-bottom: 1px solid #ddd;
        margin: 0;
        padding: 18px 16px 10px;
      }

      .inbox_chat {
        height: 550px;
        overflow-y: scroll;
      }

      .active_chat {
        background: #e8f6ff;
      }

      .incoming_msg_img {
        display: inline-block;
        width: 6%;
      }

      .incoming_msg_img img {
        width: 100%;
      }

      .received_msg {
        display: inline-block;
        padding: 0 0 0 10px;
        vertical-align: top;
        width: 92%;
      }

      .received_withd_msg p {
        background: #ebebeb none repeat scroll 0 0;
        border-radius: 0 15px 15px 15px;
        color: #646464;
        font-size: 14px;
        margin: 0;
        padding: 5px 10px 5px 12px;
        width: 100%;
      }

      .time_date {
        color: #747474;
        display: block;
        font-size: 12px;
        margin: 8px 0 0;
      }

      .received_withd_msg {
        width: 57%;
      }

      .mesgs{
        float: left;
        width:70%;
        background: white;
      }

      .sent_msg p {
        background:#0465ac;
        border-radius: 12px 15px 15px 0;
        font-size: 14px;
        margin: 0;
        color: #fff;
        padding: 5px 10px 5px 12px;
        width: 100%;
      }

      .outgoing_msg {
        overflow: hidden;
        margin: 26px 0 26px;
      }

      .sent_msg {
        float: right;
        width: 46%;
      }

      .input_msg_write input {
        background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
        border: medium none;
        color: #4c4c4c;
        font-size: 15px;
        min-height: 48px;
        width: 100%;
        outline:none;
      }

      .type_msg {
        border-top: 1px solid #c4c4c4;
        position: relative;
      }

      .msg_send_btn {
        background: #05728f none repeat scroll 0 0;
        border:none;
        border-radius: 50%;
        color: #fff;
        cursor: pointer;
        font-size: 15px;
        height: 33px;
        position: absolute;
        right: 0;
        top: 11px;
        width: 33px;
      }

      .messaging {
        padding: 0 0 50px 0;
      }

      .msg_history {
        height: 516px;
        overflow-y: auto;
      }
      .widget-user-header {
          padding: 0.6rem 1rem;
          margin-bottom: 10px;
          border-bottom: 1px solid #e9e9e9;
          background-color: #e9e9e9;
          min-height: 80px;
          display: flex;
      }
      .widget-user-image > img {
          float: left;
          height: auto;
          width: 50px;
      }
      .widget-user-info {
          margin-left: 10px;
          /*display: block;*/
      }
      .widget-user-username {
          font-size: 17px;
          font-weight: 600;
      }
      .widget-user-desc {
          margin-top: 0;
          font-size: 14px;
          color: #6a6a6a;
      }
    </style>
@endsection
@section('title')
    <i class="far fa-comment-dots"></i> Quản lý tin nhắn
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Quản lý tin nhắn</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    <div id="notication">

    </div>
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
    <div class="messaging col-md-12">
      <div class="inbox_msg">
        <div class="inbox_people">
          <div class="headind_srch">
            <div class="recent_heading">
              <h4>Gần đây</h4>
            </div>
            <div class="srch_bar">
              <div class="stylish-input-group">
                <input type="text" class="search-bar"  placeholder="Tìm kiếm..." >
              </div>
            </div>
          </div>
          <div class="inbox_chat scroll">
            @if ($list_message->count() > 0)
              @foreach($list_message as $message)
                {{-- active_chat --}}
                <div class="chat_list user_chat_{{ !empty($message->user_id) ? $message->user_id : 0 }}" data-id="{{ !empty($message->user_id) ? $message->user_id : 0 }}"> 
                  <div class="chat_people">
                    <div class="chat_img"> 
                      <img class="img-circle elevation-2" src="{{ !empty($message->user->user_meta->avatar) ? $message->user->user_meta->avatar : url('/images/user2-160x160.jpg') }}" alt="sunil"> 
                    </div>
                    <div class="chat_ib">
                      <h5 class="{{ ($message->status_meta == "pending") ? "font-weight-bold" : "" }} ">
                        {{ !empty($message->user->name) ? $message->user->name : '<b class="text-danger">Đã xóa</b>' }} 
                        <span class="chat_date">{{ $message->diffForHumans }}</span>
                      </h5>
                      @if ( strlen($message->content) > 50 )
                        <p class="chat_people_text {{ ($message->status_meta == "pending") ? "text-primary font-weight-bold" : "" }} ">{{ substr($message->content, 0, 50) }}...</p>
                      @else 
                        <p class="chat_people_text {{ ($message->status_meta == "pending") ? "text-primary font-weight-bold" : "" }} ">{{ $message->content }}</p>
                      @endif
                    </div>
                  </div>
                </div>
              @endforeach
            @endif
          </div>
        </div>
        <div class="mesgs">
          <div class="widget-user-header frames_user"> 
          </div>
          <div class="msg_history">
          </div>
          <div class="type_msg">
            <form class="form_chat">
              <div class="input_msg_write">
                <input type="hidden" id="user" value="0">
                <input type="text" class="write_msg" placeholder="Type a message" />
                <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- Modal --}}
<div class="modal fade" id="modal-product">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tạo tài khoản đại API</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_vld">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-submit" value="Tạo tài khoản API">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <input type="submit" class="btn btn-primary" id="button-product" value="Xóa sản phẩm">
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="user_chat" value="{{ Auth::id() }}" data-name="{{ Auth::user()->name }}">
<input type="hidden" id="page" value="chat">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_message.js') }}?token={{ date('YmdH') }}"></script>
@endsection