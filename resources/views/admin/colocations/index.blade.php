@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Colocation
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Colocation</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
	<div class="col-md-4">
		<a class="btn btn-primary" href="{{ route('admin.colocation.create') }}">Tạo Colocation</a>
	</div>
    <div class="col-md-4">
        <div class="row">
            <select class="form-control" id="status">
              <option value="" disabled selected>Chọn trạng thái của Colocation</option>
              <option value="on">Trạng thái đang bật</option>
              <option value="off">Trạng thái đã tắt</option>
              <option value="progressing">Trạng thái đang tạo</option>
              <option value="rebuild">Trạng thái đang cài lại</option>
              <option value="change_ip">Trạng thái đang đổi IP</option>
              <option value="reset_password">Trạng thái đặt lại mật khẩu</option>
              <option value="expire">Trạng thái hết hạn</option>
              {{-- <option value="admin_off">Trạng thái Admin tắt</option>
              <option value="suspend">Trạng thái đã khóa</option> --}}
              <option value="change_user">Trạng thái đã chuyển khách hàng</option>
              <option value="delete_vps">Trạng thái đã xóa</option>
              <option value="cancel">Trạng thái đã hủy</option>
            </select>
        </div>
    </div>
    <!-- form search -->
    <div class="col-md-4">
        <div class="row">
            <div class="form-group col-md-12">
              <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Tìm kiếm Colocation" id="q">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i></button>
                </div>
              </div>
            </div>
        </div>
    </div>
    <div class="mt-4 pl-4 col-md-12" style="display: inherit;">
        <div class="form-group">
            <span class="mt-1">Số lượng</span> 
            <select id="qtt">
                <option value="10">10</option>
                <option value="30" selected>30</option>
                <option value="50">50</option>
                <option value="100">100</option>
                <option value="200">200</option>
                <option value="500">500</option>
            </select>
        </div>
        <div class="form-group text-primary ml-4">
            <b>Đã chọn: <span class="last-item">0</span> / <span class="total-item">{{ $list_colos->count() }}</span></b>
        </div>
    </div>
    <div class="col-md-12 detail-course-finish">
        @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
        @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
        @endif
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered text-center">
                <thead class="primary">
                    <th>
                        <input type="checkbox"  class="checkbox_all">
                    </th>
                    <th>Tên khách hàng</th>
                    <th>Sản phẩm</th>
                    <th>IP</th>
                    <th>Loại</th>
                    <th>Addon IP</th>
                    <th>Datacenter <br> Vị trí</th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Tổng thời gian thuê</th>
                    <th>Chu kỳ thanh toán</th>
                    <th>Chi phí <br> VNĐ</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @if ($list_colos->count() > 0)
                        @foreach( $list_colos as $colo )
                            @php
                                $total_time = '';
                                $create_date = new Carbon($colo->date_create);
                                $next_due_date = new Carbon($colo->next_due_date);
                                if ( $next_due_date->diffInYears($create_date) ) {
                                    $year = $next_due_date->diffInYears($create_date);
                                    $total_time = $year . ' Năm ';
                                    $create_date = $create_date->addYears($year);
                                    $month = $next_due_date->diffInMonths($create_date);
                                    //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                    if ( $month ) {
                                        $total_time .= $month . ' Tháng';
                                    }
                                } else {
                                    $diff_month = $next_due_date->diffInMonths($create_date);
                                    $total_time = $diff_month . ' Tháng';
                                }
                            @endphp
                            <tr>
                                <td>
                                    <input type="checkbox" value="{{ $colo->id }}" data-ip="{{ $colo->ip }}" class="checkbox">
                                </td>
                                <td><a href="{{ route('admin.user.detail', $colo->user_id) }}">{{ !empty($colo->user->name) ? $colo->user->name : 'Đã xóa' }}</a></td>
                                <td>
                                    @if (!empty($colo->product))
                                        @if ( !empty($colo->product->duplicate) )
                                            <a href="{{ route('admin.product.edit' , $colo->product->id) }}">{{ $colo->product->name }}</a>
                                        @else
                                            @if(!empty($colo->product->group_product->private))
                                                <a href="{{ route('admin.product_private.editPrivate' , [$colo->product->group_product->id, $colo->product->id]) }}">{{ $colo->product->name }}</a>
                                            @else
                                                <a href="{{ route('admin.product.edit' , $colo->product->id) }}">{{ $colo->product->name }}</a>
                                            @endif
                                        @endif    
                                    @endif
                                </td>
                                <td><a href="{{ route('admin.colocation.detail' ,$colo->id ) }}">{{ $colo->ip }}</a></td>
                                <td>{{ $colo->type_colo }}</td>
                                <td>
                                    {{ !empty( $colo->colocation_config->ip ) ? $colo->colocation_config->ip : 0 }}
                                    <button type="button" class="ml-1 btn btn-sm btn-outline-success float-right collapsed tooggle-plus" data-toggle="collapse" data-target="#service{{ $colo->id }}" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                    <div id="service{{ $colo->id }}" class="mt-4 collapse" style="">
                                        @foreach ($colo->colocation_ips as $colocation_ip)
                                            {{ $colocation_ip->ip }} <br>
                                        @endforeach
                                    </div>
                                </td>
                                <td>{{ $colo->location }} <br> {{ $colo->rack }}</td>
                                <td>{{ date( 'd-m-Y' , strtotime($colo->created_at) ) }}</td>
                                @if(!empty($colo->next_due_date))
                                    <td>{{  date( 'd-m-Y' , strtotime($colo->next_due_date) ) }}</td>
                                @else
                                    <td class="text-danger">Chưa thanh toán</td>
                                @endif
                                <td>{{ $total_time }}</td>
                                <td>
                                    {{ $billings[$colo->billing_cycle] }}
                                </td>
                                <td>
                                    @php
                                      $amount = 0;
                                      if ( !empty($colo->amount) ) {
                                        $amount = $colo->amount;
                                      } else {
                                        $product = $colo->product;
                                        $amount = !empty( $product->pricing[$colo->billing_cycle] ) ? $product->pricing[$colo->billing_cycle] : 0;
                                        if ( !empty($colo->colocation_config_ips) ) {
                                          foreach ($colo->colocation_config_ips as $key => $colocation_config_ip) {
                                            $amount += !empty( $colocation_config_ip->product->pricing[$colo->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colo->billing_cycle] : 0;
                                          }
                                        }
                                      }
                                    @endphp
                                    {!!number_format( $amount ,0,",",".")!!}
                                </td>
                                @if ($colo->status == 'Pending')
                                    <td class="text-danger">Chưa thanh toán</td>
                                @elseif ($colo->status_colo == 'on')
                                    <td class="text-success">Đang sử dựng</td>
                                @elseif ($colo->status_colo == 'expire')
                                    <td class="text-danger">Đã hết hạn</td>
                                @elseif ($colo->status_colo == 'off')
                                    <td class="text-danger">Đã tắt</td>
                                @elseif ($colo->status_colo == 'cancel')
                                    <td class="text-danger">Yêu cầu hủy</td>
                                @elseif ($colo->status_colo == 'delete_colo')
                                    <td class="text-danger">Đã xóa</td>
                                @else 
                                    <td class="text-danger">Chưa tạo</td>
                                @endif
                                <td>
                                    <a href="{{ route('admin.colocation.detail' ,$colo->id ) }}" class="btn btn-sm btn-warning" style="font-size: 10px"><i class="fas fa-edit text-white"></i></a>
                                    <button class="btn btn-danger btn-sm btn-delete-colo" data-id="{{$colo->id}}" data-ip="{{$colo->ip}}" ><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="15" class="text-center text-danger">
                            Không có dịch vụ Colocation trong dữ liệu!
                        </td>
                    @endif
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="15" class="text-center">
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-vps" data-type="mark_paid">Tạo VPS</button>
            <button class="btn btn-outline-secondary btn-action-vps" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-vps" value="Xóa đơn hàng">
          <button type="button" class="btn btn-danger" id="button-vps-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('js/admin_colo.js') }}"></script>
@endsection
