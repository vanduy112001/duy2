@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Colocation
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/colocation/">Colocation</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Colocation</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4 class="text-center">Thông tin Colocation {{$detail->ip}}</h4>
            <form action="{{ route('admin.colocation.update') }}" method="post">
                <div class="container">
                    <div class="row">
                        <div class="col col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Tên khách hàng:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="user_id" id="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                    @php
                                                        $selected = '';
                                                        if ($user->id == $detail->user_id) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                                @foreach ($products as $product)
                                                    @php
                                                        $selected = '';
                                                        if ($product->id == $detail->product_id) {
                                                            $selected = 'selected';
                                                        }
                                                        $group_product = $product->group_product;
                                                        if ($group_product->private == 0) {
                                                            $group_user_name = 'Cơ bản';
                                                        } else {
                                                            $group_user_name = $group_product->group_user->name;
                                                        }
                                                    @endphp
                                                    <option value="{{$product->id}}" {{$selected}}>{{$product->name}} - {{ $group_user_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">IP:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" name="ip" class="form-control" value="{{$detail->ip}}">
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Loại:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select class="form-control" name="type_colo">
                                                <option value="1 U" @if($detail->type_colo == '1 U') selected @endif>1 U</option>
                                                <option value="2 U" @if($detail->type_colo == '2 U') selected @endif>2 U</option>
                                                <option value="4 U" @if($detail->type_colo == '4 U') selected @endif>4 U</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="status_colo" id="status_colo" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option value="on" @if($detail->status_colo == 'on') selected @endif>Đang bật</option>
                                                <option value="off" @if($detail->status_colo == 'off') selected @endif>Đã tắt</option>
                                                <option value="cancel" @if($detail->status_colo == 'cancel') selected @endif>Yêu cầu hủy</option>
                                                <option value="delete_colo" @if($detail->status_colo == 'delete_colo') selected @endif>Đã xóa</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Datacenter:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="location" id="location" class="form-control"  style="width: 100%;">
                                            <option value="Đà Nẵng" @if($detail->location == 'Đà Nẵng') selected @endif>Đà Nẵng</option>
                                            <option value="Hồ Chí Minh" @if($detail->location == 'Hồ Chí Minh') selected @endif>Hồ Chí Minh</option>
                                            <option value="Hà Nội" @if($detail->location == 'Hà Nội') selected @endif>Hà Nội</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Vị trí:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" name="rack" class="form-control" value="{{ $detail->rack }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Công xuất nguồn:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" name="power" class="form-control" value="{{ $detail->power }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                </div>
                            </div>
                        </div>
                        <div class="col col-md-6">
                                <div class="order">
                                    <div class="detail_order">
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Băng thông:</label>
                                            </div>
                                            <div class="col col-md-8">
                                                <input type="text" name="bandwidth" class="form-control" value="{{ $detail->bandwidth }}">
                                            </div>
                                        </div>
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Ngày đặt hàng:</label>
                                            </div>
                                            <div class="col col-md-8">
                                                <input type="text" id="datepicker" name="created_at" value="{{date('d-m-Y', strtotime($detail->date_create))}}" class="form-control">
                                            </div>
                                        </div>
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Ngày hết hạn :</label>
                                            </div>
                                            <div class="col col-md-8">
                                                <input type="text" id="datepicker_next_due_date" name="next_due_date" value="{{date('d-m-Y', strtotime($detail->next_due_date))}}" class="form-control">
                                            </div>
                                        </div>
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Ngày thanh toán :</label>
                                            </div>
                                            <div class="col col-md-8">
                                                @if(!empty($detail->paid))
                                                    @if($detail->paid == 'paid')
                                                        <span class="text-success">{{date('m/d/Y', strtotime($detail->detail_order->paid_date))}}</span>
                                                    @elseif($detail->paid == 'cancel')
                                                        <span class="text-secondary">Hủy</span>
                                                    @else
                                                        <span class="text-danger">Chưa thanh toán</span>
                                                    @endif
                                                @else
                                                    <span class="text-danger">Chưa thanh toán</span>
                                                @endif
                                            </div>
                                        </div>
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Thời gian:</label>
                                            </div>
                                            <div class="col col-md-8">
                                                <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                                    <option value="" disabled>Chọn thời gian</option>
                                                    @foreach ($billings as $key => $billing)
                                                        @php
                                                            $selected = '';
                                                            if ($key == $detail->billing_cycle) {
                                                                $selected = 'selected';
                                                            }
                                                        @endphp
                                                        <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        {{-- 1 form group --}}
                                        <div class="group-invoice">
                                            <div class="col col-md-4 text-right">
                                                <label for="">Thành tiền :</label>
                                            </div>
                                            <div class="col col-md-8">
                                                @php
                                                  $amount = 0;
                                                  if ( !empty($detail->amount) ) {
                                                    $amount = $detail->amount;
                                                  } else {
                                                    $product = $detail->product;
                                                    $amount = !empty( $product->pricing[$detail->billing_cycle] ) ? $product->pricing[$detail->billing_cycle] : 0;
                                                    if ( !empty($detail->colocation_config_ips) ) {
                                                      foreach ($detail->colocation_config_ips as $key => $colocation_config_ip) {
                                                        $amount += !empty( $colocation_config_ip->product->pricing[$detail->billing_cycle] ) ? $colocation_config_ip->product->pricing[$detail->billing_cycle] : 0;
                                                      }
                                                    }
                                                  }
                                                @endphp
                                                <b>{!!number_format(($amount),0,",",".")!!}</b> VNĐ<br>
                                                <input type="text" value="" class="form-control" name="amount" placeholder="Chỉnh sửa giá dịch vụ">
                                            </div>
                                        </div>
                                        <div class="form-button mt-4 mr-4">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $detail->id }}">
                                            <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                                            <input type="submit" value="Chỉnh sửa" class="btn btn-primary float-right">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col col-md-12">
                            <div class="order">
                                <h4 class="text-center">Thông tin Addon Colocation (có {{ $detail->colocation_config->ip }} Addon IP)</h4>
                                <div class="row">
                                    <div class="col-md-6 row">
                                        <div class="col col-md-7 text-right">
                                            <h5 class="pt-1">Gói sản phẩm Addon</h5>
                                        </div>
                                        <div class="col col-md-5">
                                            @if ( count($list_product_addon) )
                                                <button type="button" class="btn btn-sm btn-outline-success collapsed tooggle-plus mt-1" id="btnAddProductAddonIp" data-toggle="tooltip" data-placement="top" title="Thêm sản phẩm addon ip">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            @endif
                                        </div>
                                        <div class="col-md-12 mt-2">
                                            <div class="group-invoice row" id="addProductAddon">
                                                @php
                                                    $addon_key = 1;
                                                @endphp
                                                @if ( !empty($detail->colocation_config_ips) )
                                                    @foreach ($detail->colocation_config_ips as $colocation_config_ip)
                                                        <div class="form-group col-md-12 row">
                                                            <div class="col col-md-4 text-right">
                                                                <label for="">SP Addon {{ $addon_key }}</label>
                                                            </div>
                                                            <div class="col col-md-8">
                                                                <select name="product_addon_ip[]" class="form-control select2">
                                                                    <option value="0">None</option>
                                                                    @foreach ($list_product_addon as $product_addon)
                                                                        @php
                                                                            $selected = '';
                                                                            if ( $product_addon->id == $colocation_config_ip->product_id ) {
                                                                                $selected = 'selected';
                                                                            }
                                                                        @endphp
                                                                        <option value="{{ $product_addon->id }}" {{ $selected }}>{{ $product_addon->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <input type="hidden" name="colocation_config_ip[]" value="{{ $colocation_config_ip->id }}">
                                                            </div>
                                                        </div>
                                                        @php
                                                            $addon_key++;
                                                        @endphp
                                                    @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 row">
                                        <div class="col col-md-7 text-right">
                                            <h5 class="pt-1">Addon IP</h5>
                                        </div>
                                        <div class="col col-md-5">
                                            <button type="button" class="btn btn-sm btn-outline-success collapsed tooggle-plus mt-1" id="btnAddIp" data-toggle="tooltip" data-placement="top" title="Thêm IP">
                                                <i class="fas fa-plus"></i>
                                            </button>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="group-invoice" id="addIp">
                                                <div class="col col-md-2 text-right"></div>
                                                <div class="col col-md-10">
                                                    <div class="container-ip row mt-1">
                                                        @if ( !empty($detail->colocation_ips) )
                                                            @foreach ($detail->colocation_ips as $colocation_ip)
                                                                <div class="col-12 pl-0 inpAddIp">
                                                                    <div class="form-group input-group">
                                                                        <input type="text" name="addIp[]" value="{{ $colocation_ip->ip }}" placeholder="IP" class="form-control">
                                                                        <span class="input-group-append">
                                                                            <button type="button" class="btn btn-danger btnDeleteAddIp">
                                                                                <i class="fas fa-trash-alt"></i>
                                                                            </button>
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{-- 1 form group --}}
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" name="" id="addon_key" value="{{ $addon_key }}">
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="detail_colo">
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
        // $('#addIp').fadeOut();
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
		$('#datepicker_next_due_date').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();

        $('#btnAddIp').on('click', function () {
            $('#addIp').fadeIn();
            var html = '';
            html += '<div class="col-12 pl-0 inpAddIp">';
            html += '<div class="form-group input-group">';
            html += '<input type="text" name="addIp[]" value="" placeholder="IP" class="form-control">';
            html += '<span class="input-group-append">';
            html += '<button type="button" class="btn btn-danger btnDeleteAddIp">';
            html += '<i class="fas fa-trash-alt"></i>';
            html += '</button>';
            html += '</span>';
            html += '</div>';
            html += '</div>';
            $('.container-ip').append(html);
        });

        $(document).on('click', '.btnDeleteAddIp', function () {
            $(this).closest('.inpAddIp').remove();
        })

        $('#btnAddProductAddonIp').on('click', function () {
            console.log('da click');
            var user_id = $('#user_id').val();
            var addon_key = $('#addon_key').val();
            $.ajax({
                url: '/admin/colocation/getProductAddonIpColocation',
                data: { user_id: user_id },
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    if (data.error == 0) {
                        var html = '';
                        html += '<div class="form-group col-md-12 row">';
                        html += '<div class="col col-md-4 text-right">';
                        html += '<label for="">SP Addon '+ addon_key +'</label>';
                        html += '</div>';
                        html += '<div class="col col-md-8">';
                        html += '<select name="add_product_addon_ip[]" class="form-control selectadd">';
                        html += '<option value="0">None</option>';
                        $.each(data.addons, function (index, addon) { 
                            html += '<option value="'+ addon.id +'">'+ addon.name +'</option>';
                        });
                        html += '</select>';
                        html += '</div>';
                        html += '</div>';
                        $('#addProductAddon').append(html);
                        addon_key = parseInt(addon_key) + 1;
                        $('#addon_key').val(addon_key);
                    } else {
                        alert('Không tìm thấy sản phẩm addon ip');
                    }
                    $('.selectadd').select2();
                },
                error: function(e) {
                    console.log(e);
                    alert('Truy vấn colocation thất bại');
                }
            });
        });
	});
</script>
@endsection
