@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-donate nav-icon"></i> Quản lý @if($method == 'pay') nạp tiền @elseif($method == 'invoice') thanh toán hóa đơn đặt hàng @elseif ($method == 'expired')  thanh toán hóa đơn gia hạn @else tất cả thanh toán @endif
    của tài khoản {{ $user_select->name }} - {{ $user_select->email }}
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Thanh toán</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <div class="col-md-6">
                  <form class="" action="{{ route('admin.payments.select_user') }}" method="post" id="form_select">
                      @csrf
                      <div class="row">
                          <div class="form-group col-md-12">
                            <div class="input-group">
                              <select class="select2" name="user_id" style="width:80%;">
                                  <option value="" selected>Chọn khách hàng</option>
                                  @foreach($users as $user)
                                      <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
                                  @endforeach
                              </select>
                              <input type="hidden" name="type" class="form-control" value="@if($method == 'pay') pay @elseif($method == 'invoice') order @elseif ($method == 'expired') expired @elseif ($method == 'addon') addon  @elseif ($method == 'change_ip') change_ip @else all  @endif">
                              <div class="input-group-append">
                                <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-search"></i>
                                </button>
                              </div>
                            </div>
                          </div>
                      </div>
                  </form>
                </div>
                <div class="col-md-6"></div>
                <!-- form search -->
                <div class="col-md-4 mt-4 text-primary text-center">
                    <b>Trang thanh toán {{ $payments->firstItem() }} - {{ $payments->lastItem() }} / {{ $payments->total() }}</b>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4 mt-4 link-right">
                    {{ $payments->links()  }}
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-bordered">
                    <thead class="primary">
                        <th>ID</th>
                        <th>Mã giao dịch</th>
                        <th>Tên khách hàng</th>
                        <th>Loại</th>
                        <th>Hình thức</th>
                        <th>Số tiền</th>
                        <th>Ngày GD</th>
                        <th>Ngày thanh toán</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                    @if($payments->count() > 0)
                        @foreach ($payments as $payment)
                            <tr>
                                  <td>{{ $payment->id }}</td>
                                  <td>{{ $payment->ma_gd }}</td>
                                  <td>
                                      @if(!empty($payment->user->name))
                                        <a href="{{ route('admin.user.detail', $payment->user->id) }}">{{ $payment->user->name }}</a>
                                      @endif
                                  </td>
                                  <td>{{ !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] :$payment->method_gd }} {{ !empty($payment->detail_order_id) ? '#' . $payment->detail_order_id : '' }}</td>
                                  <td>{{ $type_payin[$payment->type_gd] }}</td>
                                  <td>{!!number_format($payment->money,0,",",".") . ' VNĐ'!!}</td>
                                  <td>{{ date('H:i:s d-m-Y', strtotime($payment->created_at)) }}</td>
                                  <td>{{ date('d-m-Y', strtotime($payment->date_gd)) }}</td>
                                  <td class="payment-status">
                                      @if($payment->status == 'Active')
                                          <span class="text-success">Đã thanh toán</span>
                                      @elseif ($payment->status == 'Pending')
                                          <span class="text-danger">Chưa thanh toán</span>
                                      @else
                                          <span class="text-danger">Thanh toán lỗi</span>
                                      @endif
                                  </td>
                                  <td class="table-button">
                                      @if($payment->status == 'Pending')
                                          <button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="{{ $payment->id }}" data-name="{{ !empty($payment->user->name) ?$payment->user->name : '' }}" data-magd="{{ $payment->ma_gd }}"><i class="fas fa-check"></i></button>
                                      @endif
                                      <a href="" class="btn btn-danger delete-payment" data-id="{{ $payment->id }}" data-name="{{ !empty($payment->user->name) ?$payment->user->name : '' }}"><i class="fas fa-trash-alt"></i></a>
                                  </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="9" class="text-center text-danger">
                            Không có dữ liệu thanh toán của tài khoản này
                        </td>
                    @endif
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="9" class="text-center link-right">
                            {{ $payments->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="delete-invoice">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Xóa đơn đặt hàng</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="notication-invoice" class="text-center">

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" id="button-cancel" data-dismiss="modal">Hủy</button>
                    <input type="submit" class="btn btn-primary" id="button-invoice" value="Xóa giao dịch">
                    <input type="submit" class="btn btn-primary" id="button-confirm" value="Xóa giao dịch">
                    <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection
@section('scripts')
    <script src="{{ asset('js/payments.js') }}"></script>
    <script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
    	$(document).ready(function() {
    		//Date picker
    		$('#datepicker').datepicker({
    			autoclose: true
    		});
            // select
            $('.select2').select2();
    	});
    </script>
@endsection
