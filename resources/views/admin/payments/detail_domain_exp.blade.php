@extends('layouts.app')
@section('title')
    <i class="fas fa-donate nav-icon"></i> Thông tin thanh toán gia hạn tên miền 
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Thanh toán</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
                <!-- form search -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive col-md-9" style="margin: auto;">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title payment-detail-title">Thông tin thanh toán của người dùng</h3>
            </div>
            <div class="card-body payment-detail-body">
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Họ và tên
                    </div>
                    <div class="col-md-8">
                        <a href="">{{ $payment->user->name }}</a>
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Mã giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ $payment->ma_gd }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Ngày giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ date('d-m-Y', strtotime($payment->date_gd)) }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Số tiền giao dịch
                    </div>
                    <div class="col-md-8">
                        {!!number_format($payment->money,0,",",".") . ' VNĐ'!!}
                    </div>
                </div>
                @php
                  $detail_order = $payment->detail_order;
                  $domain_exps = $detail_order->domain_expireds;
                @endphp

                <div class="row payment-detail">
                    <div class="col-md-4">
                        Gia hạn cho tên miền
                    </div>
                    <div class="col-md-8 text-danger">
                        @foreach ($domain_exps as $domain_exp)
                            @php
                                $domain = $domain_exp->domain;
                            @endphp
                            {{ $domain['domain'] }}
                        @endforeach
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Thời gian hết hạn
                    </div>
                    <div class="col-md-8">
                        @foreach ($domain_exps as $domain_exp)
                            @php
                                $domain = $domain_exp->domain;
                                $time = date('d-m-Y', strtotime($domain['next_due_date']));
                            @endphp
                                {{ $time }}
                        @endforeach
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Hình thức giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ $type_payin[$payment->type_gd] }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Trạng thái
                    </div>
                    <div class="col-md-8">
                        @if($payment->status == 'Active')
                            <span class="text-success">Đã thanh toán</span>
                        @elseif ($payment->status == 'Pending')
                            <span class="text-danger">Chưa thanh toán</span>
                        @else
                            <div class="text-danger">Thanh toán lỗi</div>
                            <div class="text-danger">Lỗi: {{ $momo_status[$content->errorCode] }}</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col col-md-12 text-center">
        <a href="{{ route('admin.home') }}"><i class="fas fa-arrow-left"></i> Trở về trang chủ</a>
    </div>
    <input type="hidden" id="page" value="detail_domain_expired">
@endsection
@section('scripts')
{{--    <script src="{{ asset('js/payments.js') }}"></script>--}}
@endsection
