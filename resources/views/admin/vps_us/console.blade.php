@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/console/wmks-all.css') }}">
@endsection
@section('title')
<div class="text-primary">Console VPS {{ !empty($data_console['ip']) ? $data_console['ip'] : '' }} </div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">VPS</li>
@endsection
@section('content')
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="fillter_log mt-4 mb-4">
              <div id="buttonBar">
                  <div class="buttonC">
                      <button id="cad" class="btn btn-primary">
                          Send Ctrl+Alt+Delete
                      </button>
                  </div>
              </div>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
                @if ($data_console['error'] == 0)
                  <input type="hidden" id="vm_host" value="{{ $data_console['vm_host'] }}">
                  <input type="hidden" id="vm_ticket" value="{{ $data_console['vm_ticket'] }}">
                  <div id="wmksContainer" style="position:absolute;width:100%;height:100%"></div>
                @else
                  <div class="error-connect text-center">
                     <b class="text-danger">Lỗi kết nối ....</b>
                  </div>
                @endif
            </div>
          </div>
      </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('libraries/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libraries/console/wmks.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
      var wmks = WMKS.createWMKS("wmksContainer", {}).register(WMKS.CONST.Events.CONNECTION_STATE_CHANGE, function (event, data) {
          if (data.state == WMKS.CONST.ConnectionState.CONNECTED) {
              console.log("connection state change : connected");
          }
      });

      var vm_host = $('#vm_host').val();
      var vm_ticket = $('#vm_ticket').val();

      var connect_string = 'wss://' + vm_host + ':443/ticket/' + vm_ticket;
      wmks.connect(connect_string);
      $('#cad').on("click", function () {
          wmks.sendCAD();
      });

  })
</script>
@endsection
