@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách VPS
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">VPS</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="row">
                    <form action="{{ route('admin.vps.search_user_form') }}" style="width: 100%;">
                        <div class="form-group col-md-12">
                          <div class="input-group">
                            <input type="text" name="search_user" class="form-control" placeholder="Tìm kiếm khách hàng" id="search_user_form">
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-info"><i class="fas fa-search"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- form search -->
            <div class="col-md-4">
                <!-- <div class="row">
                    <div class="form-group col-md-12">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm VPS" id="search">
                        <div class="input-group-append">
                          <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                </div> -->
            </div>
            <div class="col-md-4 mt-4 text-primary pl-4">
                <b>VPS {{ $list_vps->firstItem() }} - {{ $list_vps->lastItem() }} / {{ $list_vps->total() }}</b>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 mt-4 link-right vps_search_user paginate_top">
                <?php
                    $total = $list_vps->total();
                    $perPage = $list_vps->perPage();
                    $current_page = $list_vps->currentPage();
                ?>
                @if ($total > $perPage)
                  <?php
                      $page = $total/$perPage + 1;
                  ?>
                  <nav>
                    <ul class="pagination">
                        <!-- dấu prev -->
                        @if ($current_page != 1)
                            <li class="prev"><a href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$current_page - 1}}" class="page-link">&laquo</a></li>
                        @else
                            <li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>
                        @endif
                        <!-- so trang -->
                        @for($i = 1; $i < $page; $i++)
                          <?php
                            $active = '';
                            if ($i == $current_page) {
                              $active = 'active';
                            }
                          ?>
                          @if ($active == 'active')
                            <li class="page-item active" aria-current="page"><span class="page-link">{{ $i }}</span></li>
                          @else
                            <li class="page-item"><a class="page-link" href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$i}}">{{ $i }}</a></li>
                          @endif
                        @endfor
                        <!-- dấu next -->
                        @if ( $current_page != $page )
                            <li class="page-item"><a href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$current_page+1}}" class="page-link">&raquo;</a></li>
                        @else
                            <li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
                        @endif
                    </ul>
                  </nav>
                @endif
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered" id="table_vps">
                <thead class="primary">
                    <th>ID</th>
                    <th>Tên khách hàng</th>
                    <th>IP</th>
                    <th>Cấu hình</th>
                    <th>Loại</th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Chi phí</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($list_vps as $vps)
                        <tr>
                            <td>{{ $vps->id }}</td>
                            <td>
                              @if (!empty($vps->user_vps->id))
                                <a href="{{ route('admin.user.detail' , !empty($vps->user_vps->id) ? $vps->user_vps->id : 0 ) }}">{{ !empty($vps->user_vps->name) ? $vps->user_vps->name : 'Đã xóa' }}</a>
                              @else
                                <span class="text-danger">Đã xóa</span>
                              @endif
                            </td>
                            <td>
                                @if (!empty($vps->ip))
                                    <a href="{{ route('admin.vps.detail', $vps->id) }}">{{$vps->ip}}</a>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                              @php
                                $product = $vps->product;
                                if(!empty($product->meta_product)) {
                                  $cpu = $product->meta_product->cpu;
                                  $ram = $product->meta_product->memory;
                                  $disk = $product->meta_product->disk;
                                } else {
                                  $cpu = 1;
                                  $ram = 1;
                                  $disk = 20;
                                }
                                // Addon
                                if (!empty($vps->vps_config)) {
                                    $vps_config = $vps->vps_config;
                                    if (!empty($vps_config)) {

                                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                    }
                                }
                              @endphp
                              {{ $cpu }} - {{ $ram }} - {{ $disk }}
                            </td>
                            <td>
                                @if($vps->type_vps == "vps")
                                  VPS
                                @else
                                  NAT VPS
                                @endif
                            </td>
                            <td>@if (!empty($vps->date_create))
                                    <span>{{ date('H:i:m d-m-Y', strtotime($vps->created_at)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>@if (!empty($vps->next_due_date))
                                    <span>{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                              @if(!empty($vps->price_override))
                                <?php
                                  $sub_total = $vps->price_override;
                                  $order_addon_vps = $vps->order_addon_vps;
                                ?>
                                <b class="text-danger">{!!number_format( $sub_total ,0,",",".")!!} VNĐ</b>
                              @else
                                <?php
                                  $product = $vps->product;
                                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                                  if (!empty($vps->vps_config)) {
                                    $addon_vps = $vps->vps_config;
                                    $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                                    $pricing_addon = 0;
                                    foreach ($add_on_products as $key => $add_on_product) {
                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                        }
                                      }
                                    }
                                    $sub_total += $pricing_addon;
                                  }
                                ?>
                                <b>{!!number_format( $sub_total ,0,",",".")!!} VNĐ</b>
                              @endif
                            </td>
                            <td>
                                @if ($vps->status == 'Pending')
                                    <span class="text-danger">Chưa tạo</span>
                                @elseif ($vps->status_vps == 'progressing')
                                    <span class="text-info">Đang tạo</span>
                                @elseif ($vps->status_vps == 'rebuild')
                                    <span class="text-info">Đang cài lại</span>
                                @elseif ($vps->status_vps == 'change_ip')
                                    <span class="text-info">Đang đổi IP</span>
                                @elseif ($vps->status_vps == 'expire')
                                    <span class="text-danger">Hết hạn</span>
                                @elseif ($vps->status_vps == 'on')
                                    <span class="text-success">Đang bật</span>
                                @elseif ($vps->status_vps == 'off')
                                    <span class="text-danger">Đã tắt</span>
                                @elseif ($vps->status_vps == 'admin_off')
                                    <span class="text-danger">Admin tắt</span>
                                @elseif ($vps->status_vps == 'suspend')
                                    <span class="text-danger">Đã khóa</span>
                                @elseif ($vps->status_vps == 'delete_vps')
                                    <span class="text-danger">Đã xóa</span>
                                @else
                                    <span class="text-secondary">Đã hủy</span>
                                @endif
                            </td>
                            <td class="button-action">
                                @if ($vps->status_vps == 'on')
                                    <a href="{{ route('admin.vps.console', $vps->id) }}" target="_blank" class="btn btn-success"><i class="fas fa-desktop"></i></a>
                                @endif
                                <a href="{{ route('admin.vps.detail', $vps->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
                                <button class="btn btn-danger btn-delete-vps" data-id="{{$vps->id}}" data-ip="{{$vps->ip}}" ><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix vps_search_user">
                  <td colspan="11" class="link-right">
                      <?php
                          $total = $list_vps->total();
                          $perPage = $list_vps->perPage();
                          $current_page = $list_vps->currentPage();
                      ?>
                      @if ($total > $perPage)
                        <?php
                            $page = $total/$perPage + 1;
                        ?>
                        <nav>
                          <ul class="pagination">
                              <!-- dấu prev -->
                              @if ($current_page != 1)
                                  <li class="prev"><a href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$current_page - 1}}" class="page-link">&laquo</a></li>
                              @else
                                  <li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>
                              @endif
                              <!-- so trang -->
                              @for($i = 1; $i < $page; $i++)
                                <?php
                                  $active = '';
                                  if ($i == $current_page) {
                                    $active = 'active';
                                  }
                                ?>
                                @if ($active == 'active')
                                  <li class="page-item active" aria-current="page"><span class="page-link">{{ $i }}</span></li>
                                @else
                                  <li class="page-item"><a class="page-link" href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$i}}">{{ $i }}</a></li>
                                @endif
                              @endfor
                              <!-- dấu next -->
                              @if ( $current_page != $page )
                                  <li class="page-item"><a href="/admin/vps/tim-kiem-vps-theo-user?search_user={{$q}}&page={{$current_page+1}}" class="page-link">&raquo;</a></li>
                              @else
                                  <li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>
                              @endif
                          </ul>
                        </nav>
                      @endif
                  </td>
                </tfoot>
            </table>
        </div>
    </div>
    <input type="hidden" id="type_user_personal" value="">
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-vps" data-type="mark_paid" data-type="create">Tạo VPS</button>
            <button class="btn btn-outline-secondary btn-action-vps" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-vps">Xóa VPS</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="user_search" value="{{ $q }}">
<input type="hidden" id="total" value="{{ $list_vps->total() }}">
<input type="hidden" id="perPage" value="{{ $list_vps->perPage() }}">
<input type="hidden" id="currentPage" value="{{ $list_vps->currentPage() }}">
@endsection
@section('scripts')
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/vps.js') }}"></script>
@endsection
