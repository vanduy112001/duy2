@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Tạo hợp đồng cho {{ $user_detail->name }}
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
  <li class="breadcrumb-item active"><a href="{{ route('admin.user_contract', ['id' => $user_detail->id]) }}">Hợp đồng {{ $user_detail->name }}</a></li>
  <li class="breadcrumb-item active">Tạo hợp đồng</li>
@endsection
@section('content')

<div class="col col-md-12">
    <h4 class="mb-2">Tạo hợp đồng</h4>
    <hr class="text-secondary">
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <form action="{{ route('admin.contract.create_data_contract') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="title">Tiêu đề hợp đồng</label>
                        <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="contract_type">Loại hợp đồng</label>
                        <select class="form-control select2" name="contract_type" id="contract_type" required style="width:100%;">
                            <option value="VPS">VPS</option>
                            <option value="HOST">Hosting</option>
                            <option value="EHOST">Email Hosting</option>
                            <option value="COLO">Colocation</option>
                            <option value="SV">Server</option>
                            <option value="DM">Domain</option>
                            <option value="TKW">Thiết kế web</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label for="contract_number">Số hợp đồng</label>
                    <div class="form-group input-group">
                        <input type="text" id="contract_number" name="contract_number" value="{{ old('contract_number') }}" class="form-control" required>
                        <span class="input-group-append">
                          <button type="button" class="btn btn-info" id="create_new_contract_number">Tạo mới!</button>
                        </span>
                    </div>
                </div>
                <div class="col-12" id="add-domain-website" style="display: none">
                    <div class="form-group">
                        <label for="domain_website">Tên miền website</label>
                        <input type="text" id="domain_website" name="domain_website" value="{{ old('domain_website') }}" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 form-group">
                    <label for="user">Thông tin khách hàng</label>
                    <select class="form-control select2" name="user_id" id="user" required style="width:100%;">
                        <option value="" disabled selected>Chọn khách hàng</option>
                        @foreach( $users as $user )
                          <?php
                              $selected = '';
                              if ( $user->id == $user_detail->id ) {
                                  $selected = 'selected';
                              }
                           ?>
                           <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="company">Công ty</label>
                        <input type="text" id="company" name="company" value="{{ $user_detail->user_meta->company ? $user_detail->user_meta->company : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="abbreviation_name">Tên viết tắt (Cá nhân/Công ty/Tổ chức)</label>
                        <input type="text" id="abbreviation_name" name="abbreviation_name" value="{{ $user_detail->user_meta->abbreviation_name ? $user_detail->user_meta->abbreviation_name : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy">Người đại diện</label>
                        <input type="text" id="deputy" name="deputy" value="{{ $user_detail->name ? $user_detail->name : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy_position">Chức vụ người đại diện</label>
                        <input type="text" id="deputy_position" name="deputy_position" value="{{ old('deputy_position') }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="address">Địa chỉ</label>
                        <input type="text" id="address" name="address" value="{{ $user_detail->user_meta->address ? $user_detail->user_meta->address : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" value="{{ $user_detail->email ? $user_detail->email : '' }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="phone">Số điện thoại</label>
                        <input type="text" id="phone" name="phone" value="{{ $user_detail->phone ? $user_detail->phone : '' }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="mst">Mã số thuế</label>
                        <input type="text" id="mst" name="mst" value="{{ $user_detail->user_meta->mst ? $user_detail->user_meta->mst : '' }}" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 my-3">
                    <label for="vat">Thuế VAT</label>
                    <div class="icheck-primary d-inline ml-3">
                        <input type="checkbox" id="vat" name="vat" checked>
                        <label for="vat"></label>
                    </div>
                    {{-- <label for="contract_website" style="margin-left: 20px">Hợp đồng website</label>
                    <div class="icheck-primary d-inline ml-3">
                        <input type="checkbox" id="contract_website" name="contract_website">
                        <label for="contract_website"></label>
                    </div> --}}
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_create">Ngày bắt đầu</label>
                        <input type="text" id="date_create" name="date_create" value="{{ old('date_create') }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_end">Ngày kết thúc</label>
                        <input type="text" id="date_end" name="date_end" value="{{ old('date_end') }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="form-group">
                        <label for="status">Trạng thái</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="active">Active</option>
                            <option value="pending">Pending</option>
                            <option value="cancel">Cancel</option>
                        </select>
                    </div>
                </div>
                {{-- <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="amount">Thành tiền</label>
                        <input type="text" id="amount" name="amount" value="{{ old('amount') }}" class="form-control" disabled required>
                    </div>
                </div> --}}
            </div>
            <div class="card-footer">
                <a href="{{ route('admin.contract.index') }}" class="btn btn-sm btn-default"><i class="fas fa-angle-double-left"></i> Back</a>
                <input type="submit" class="btn btn-primary btn-sm float-right" value="Tạo hợp đồng">
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="create_contract">
@endsection

@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        $('#date_create').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
        $('#date_end').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
    })
</script>
<script>
    jQuery(document).ready(function($){
        $('#date_create').val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#user').on('change', function(){
            var id = $(this).val();
            $('#qtt').val('');
            $('#amount').val('');
            $.ajax({
                url: "{{ route('admin.contract.add_data_contract_form') }}",
                data: {'id': id},
                dataType: 'json',
                type: 'GET',
                success: function(data) {
                    console.log(data);
                    $('[type="text"]').removeAttr('disabled');
                    data.data_user.name ? $('#deputy').val(data.data_user.name) : $('#deputy').val('');
                    // data.data_user.name ? $('#contract_number').val(create_contract_number(data.data_user.name)+'-'+random_char(5)) : $('#contract_number').val('');
                    data.data_user.email ? $('#email').val(data.data_user.email) : $('#email').val('');
                    data.data_user.user_meta.address ? $('#address').val(data.data_user.user_meta.address) : $('#address').val('');
                    data.data_user.user_meta.phone ? $('#phone').val(data.data_user.user_meta.phone) : $('#phone').val('');
                    data.data_user.user_meta.mst ? $('#mst').val(data.data_user.user_meta.mst) : $('#mst').val('');
                    data.data_user.user_meta.company ? $('#company').val(data.data_user.user_meta.company) : $('#company').val('');
                    data.data_user.user_meta.abbreviation_name ? $('#abbreviation_name').val(data.data_user.user_meta.abbreviation_name) : $('#abbreviation_name').val('');
                },
                error: function(e) {
                    console.error(e);
                }
            })
        });

        function create_contract_number(name) {
            var last_name = name.split(' ').slice(-1).join(' ');
            var new_last_name = last_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
            var upper_case_last_name = new_last_name.toUpperCase();
            return upper_case_last_name;
        }
        function random_char(length) {
            var result           = [];
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
            }
            return result.join('');
        }

        $('#contract_type').change(function(){
            if($(this).val() === 'TKW') {
                $('#add-domain-website').show();
            } else {
                $('#add-domain-website').hide();
            } 
        });

        $('#create_new_contract_number').on('click', function() {
            var current_date = $.datepicker.formatDate('ddmmy', new Date());
            var contract_type = $('#contract_type').val();
            var abbreviation_name = $('#abbreviation_name').val();
            if (!contract_type || !abbreviation_name) {
                alert('Chưa chọn loại hợp đồng hoặc chưa có tên viết tắt của đối tác');
            } else {
                var contract_number = current_date + '/' + contract_type + '/ĐVS-' +  abbreviation_name;
                $.ajax({
                    url: "{{ route('admin.contract.create_new_contract_number') }}",
                    data: { contract_number: contract_number },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#contract_number').val(data);
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            }
        });

    });
</script>
@endsection
