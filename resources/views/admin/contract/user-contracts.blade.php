@extends('layouts.app')
@section('title')
Hợp đồng {{ $user->name }}
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Hợp đồng {{ $user->name }}</li>
@endsection
@section('content')
{{-- <div class="row">
    <div data-v-a16ce4fa="" class="col-md-4">
        <a class="btn btn-primary" href="{{ route('admin.user.create') }}">Tạo user</a>
</div>
</div> --}}

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@elseif(session("fail"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("fail")}}</span>
</div>
@elseif(session("error"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("error")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
    <div class="card-header">
        <div class="card-tools float-left">
            <div class="input-group input-group-sm d-inline p-2">
                <button type="submit" id="check_delete" class="btn btn-sm btn-default check_delete"><i
                        class="fa fa-trash"></i> Xóa mục đã chọn</button>
                <a class="btn btn-sm btn-primary" href="{{ route('admin.user_contract.create', ['id' => $user->id]) }}"><i
                        class="fa fa-edit"></i> Tạo hợp đồng</a>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table id="data-contract-table" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>
                        <div>
                            <input class="check_all" id="check_all" type="checkbox" value="1">
                        </div>
                    </th>
                    <th>Khách hàng</th>
                    <th>Số hợp đồng</th>
                    {{-- <th>Tiêu đề</th> --}}
                    <th>Ngày tạo</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($user_contracts->count() > 0)
                @foreach($user_contracts as $contract)
                <tr data-row-id="{{ $contract->id }}">
                    <td>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="check_id" value="{{ $contract->id }}">
                        </div>
                    </td>
                    {{-- <td>{{ $contract->id }} </td> --}}
                    <td>
                        @if (!empty($contract->user->name))
                            <a href="{{ route('admin.user.detail',['id' => $contract->user_id]) }}" target="_blank">{{$contract->user->name}} - {{$contract->user->email}}</a>
                        @else
                            Đã xóa
                        @endif
                    </td>
                    <td>
                        @if ($contract->status === "pending")
                            <a href="{{ route( 'admin.contract.update_form', ['id' => $contract->id ] ) }}"><span class="btn btn-outline-danger btn-sm mb-1"><i class="fa fa-paste bg-c-blue"></i> Duyệt hợp đồng</span></a>
                        @else
                            <a href="{{ route( 'admin.contract.update_form', ['id' => $contract->id ] ) }}" title="Update">{{ !empty($contract->contract_number) ? $contract->contract_number : '' }}</a>
                        @endif
                        
                    </td>
                    {{-- <td><a href="{{ route( 'admin.contract.update_form', ['id' => $contract->id ] ) }}" title="Update">{{ !empty($contract->title) ? $contract->title : '' }}</a></td> --}}
                    <td>{{ !empty($contract->created_at) ? date('d-m-Y', strtotime($contract->created_at)) : '' }}</td>
                    <td>
                        @if (!empty($contract->status))
                            @if ($contract->status === "pending")
                                <span class="btn btn-danger btn-sm mb-1"><i class="fa fa-times-circle"></i> Pending</span>
                            @elseif($contract->status === "active")
                                <span class="btn btn-success btn-sm mb-1"><i class="fa fa-check-circle"></i> Active</span>
                            @elseif($contract->status === "cancel")
                                <span class="btn btn-warning btn-sm mb-1"><i class="fa fa-exclamation-triangle"></i> Cancel</span>
                            @endif
                        @endif
                    </td>
                    <td>
                        <a class="btn btn-danger btn-sm mb-1 delete-contract" href="javascript:void(0)" data-id="{{ $contract->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a>
                        <a class="btn btn-warning btn-sm mb-1 update-contract" href="{{ route( 'admin.contract.update_form', ['id' => $contract->id ] ) }}" title="Update"><i class="fas fa-edit"></i></a>
                        @if (!empty($contract->status))
                            @if ($contract->status === "active")
                                <a class="btn btn-secondary btn-sm mb-1" href="{{ route( 'admin.contract.print_pdf', [ 'id' => $contract->id ] ) }}" target="_blank" title="Xem bản in"><i class="fas fa-print"></i></a>
                            @endif
                        @endif
                    </td>
                </tr>
                @endforeach
                @else
                <td colspan="6" class="text-danger text-center">Không có dữ liệu</td>
                @endif
            </tbody>
        </table>
    </div>
</div>
<input type="hidden" id="page" value="list_contract">

@endsection

@section('scripts')
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#data-contract-table").DataTable({
            "columnDefs": [
                 { "targets": [ 0, 4], "orderable": false }
            ]
        });
    });
</script>
<script>
    //Click để ẩn và hiện các row
    $("#check_all").click(function(){
        $('input:checkbox').prop('checked', this.checked);
    });

    // Hiện button danger
    $('input:checkbox').change(function () {
        var check = $('.form-check-input:checked');
        if (check.length > 0) {
            $("#check_delete").addClass('btn-danger').removeClass('btn-default');
        } else if(check.length == 0){
            $("#check_delete").addClass('btn-default').removeClass('btn-danger');
        }
    });

    $('.delete-contract').click(function (e) {
        var r = confirm("Bạn muốn xóa hợp đồng này?");
        if (r === true) {
            var obj = $(this);
            var id = obj.attr('data-id');
            $.ajax({
                type: "post",
                url: "{{ route('admin.contract.delete') }}",
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": id
                },
                success: function (data) {
                    console.log(data);
                    alert(data.message);
                    if (data.status) {
                        obj.closest('tr').remove();
                    }
                }, 
                error: function (e) {
                    console.error(e);
                }
            });
        }
    });
    $('.check_delete').on('click', function () {
        var r = confirm("Bạn muốn xóa các mục đã chọn?");
        if (r === true) {
            var array = [];
            var value = $('.form-check-input:checked');
            if (value.length > 0) {
                $(value).each(function () {
                    array.push($(this).val());
                });
            } else {
                alert('Chưa chọn mục cần xóa');
            }
            $.ajax({
                type: "post",
                url: "{{ route('admin.contract.multidelete') }}",
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "id": array
                },
                success: function (data) {
                    alert(data.message);
                    $.each(array, function( index, value ) {
                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                    });
                },
                error: function (e) {
                    console.error(e);
                }
            });
        }       
    });

</script>
@endsection