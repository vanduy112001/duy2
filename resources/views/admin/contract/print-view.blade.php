@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <style>
        .bieu-ngu:after {
        content: "-----oOo------";
        position: absolute;
        left: 0;
        right: 0;
        bottom: -12px;
    }
    </style>
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Xem bản in hợp đồng {{ $contract->contract_number }}
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
  <li class="breadcrumb-item active"><a href="{{ route('admin.contract.index') }}">Danh sách hợp đồng</a></li>
  <li class="breadcrumb-item active">Hợp đồng {{ $contract->contract_number }}</li>
@endsection
@section('content')
@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@elseif(session("fail"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("fail")}}</span>
</div>
@elseif(session("error"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("error")}}</span>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="invoice px-5 py-5 mb-3 rounded-0" style="font-family: 'Font Awesome 5 Free';">
                <div class="p-3 border border-dark">
                    <div class="row">
                        <div class="col-12 text-center bieu-ngu">
                            <h5 class="font-weight-bold">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h5>
                            <h6>Độc lập - Tự do - Hạnh phúc</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <span class="float-right"><b>Số: </b>{{ $contract->contract_number ? $contract->contract_number : '' }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-center">
                            <h3 class="mb-4 font-weight-bold text-uppercase">{{ $contract->title }}</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p>
                                1. Căn cứ vào Bộ Luật dân sự số 91/2015/QH13 ngày 24/11/2015 của Quốc hội nước Cộng hòa xã hội chủ nghĩa Việt Nam; <br>
                                2. Căn cứ Thông tư Số: 09/2008/TT-BTTT; Thông tư số 10/2008/TT-BTTTT và các quy định về quản lý, cung cấp và sử dụng tài nguyên Internet hiện hành của Bộ Thông tin và Truyền Thông; <br>
                                3. Căn cứ vào yêu cầu và khả năng của hai bên;
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="font-weight-bold">
                                Hôm nay, {{ $contract->date_create ? date('\n\g\à\y d \t\h\á\n\g m \n\ă\m Y', strtotime($contract->date_create)) : '.......................' }}, tại văn phòng Công ty TNHH MTV Công nghệ Đại Việt Số, chúng tôi gồm có:
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 invoice-col pl-4">
                            <b>Bên A</b>
                            <address class="pl-4">
                                Đại diện bởi<br>
                                Địa chỉ<br>
                                Điện thoại<br>
                                Email <br>
                                Mã số thuế
                            </address>
                        </div>
                        <div class="col-sm-10 invoice-col">
                            : <b class="text-uppercase">{{ $contract->company ? $contract->company : '' }}</b>
                            <address>
                                : <span class="d-inline-block w-50 text-uppercase">{{ $contract->deputy ? $contract->deputy : '' }}</span><span class="">Chức vụ: {{ $contract->deputy_position ? $contract->deputy_position : '' }}</span> <br>
                                : {{ $contract->address ? $contract->address : '' }} <br>
                                : {{ $contract->phone ? $contract->phone : '' }} <br>
                                : {{ $contract->email ? $contract->email : '' }} <br>
                                : {{ $contract->mst  ? $contract->mst : '' }}
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 invoice-col pl-4">
                            <b>Bên B</b>
                            <address class="pl-4">
                                Đại diện bởi<br>
                                Địa chỉ<br>
                                Điện thoại<br>
                                Email <br>
                                Mã số thuế
                            </address>
                        </div>
                        <div class="col-sm-10 invoice-col">
                            : <b class="text-uppercase">Công ty TNHH MTV Công nghệ Đại Việt Số</b>
                            <address>
                                : <span class="d-inline-block w-50 text-uppercase">Bà Nguyễn Thị Ái Hoa</span><span class="">Chức vụ: GIÁM ĐỐC</span> <br>
                                : 257 Lê Duẩn, phường Tân Chính, quận Thanh Khê, thành phố Đà Nẵng <br>
                                : 08 8888 0043 <br>
                                : <u class="d-inline-block w-50">support@cloudzone.vn</u><span class="">Website: <u>https://cloudzone.vn</u></span> <br> 
                                : 0401765630
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p class="font-weight-bold">
                                <u>Hai bên thỏa thuận ký kết hợp đồng với những điều khoản sau:</u>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 1:</u> NỘI DUNG HỢP ĐỒNG</b>
                            <p class="pl-4"> Bên A sử dụng các gói dịch vụ hosting của bên B với nội dung gói dịch vụ được mô tả tại điều 2 của hợp đồng này</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 2:</u> GIÁ TRỊ HỢP ĐỒNG VÀ PHƯƠNG THỨC THANH TOÁN</b>
                            <p class="pl-4"> 2.1 <span class="pl-4">Giá trị hợp đồng:</span></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr class="text-center">
                                        <th >STT</th>
                                        <th>Dịch vụ</th>
                                        <th>Đơn giá (VNĐ)</th>
                                        <th>Số lượng</th>
                                        <th>Thành tiền (VNĐ)</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @foreach ($data_contract_details as $key => $data_contract_detail)
                                    <tr>
                                        <td id="stt" class="text-center">{{ $key+1 }}</td>
                                        <td>
                                            <b>Dịch vụ {{ $data_contract_detail['product']->name }}</b> <br>
                                            Thời gian sử dụng: {{ $data_contract_detail['contract_detail']->billing_cycle ? $billings[$data_contract_detail['contract_detail']->billing_cycle] : '' }} <br>
                                            Từ ngày {{ !empty($data_contract_detail['contract_detail']->date_create) ? date('d/m/Y', strtotime($data_contract_detail['contract_detail']->date_create)) : '....................' }} đến ngày {{ !empty($data_contract_detail['contract_detail']->date_end) ? date('d/m/Y', strtotime($data_contract_detail['contract_detail']->date_end)) : '.........................' }}
                                        </td>
                                        <td class="text-center">{{ $data_contract_detail['product']->pricing ? number_format($data_contract_detail['product']->pricing[$data_contract_detail['contract_detail']->billing_cycle], 0, ',', '.') : '' }}</td>
                                        <td class="text-center">{{ $data_contract_detail['contract_detail']->qtt ? $data_contract_detail['contract_detail']->qtt : '' }}</td>
                                        <td class="text-center product_price" value="{{ $data_contract_detail['contract_detail']->amount ? $data_contract_detail['contract_detail']->amount : 0 }}">{{ $data_contract_detail['contract_detail']->amount ? number_format($data_contract_detail['contract_detail']->amount, 0, ',', '.') : '' }}</td>
                                        <td>{{ $data_contract_detail['contract_detail']->note ? $data_contract_detail['contract_detail']->note : '' }}</td>
                                    </tr>
                                    @endforeach --}}
                                    @foreach ($contract_details as $key => $contract_detail)
                                        <tr>
                                            <td id="stt" class="text-center">{{ $key+1 }}</td>
                                            <td>
                                                {{-- <b>Dịch vụ {{ $data_contract_detail['product']->name }}</b> <br>
                                                Thời gian sử dụng: {{ $data_contract_detail['contract_detail']->billing_cycle ? $billings[$data_contract_detail['contract_detail']->billing_cycle] : '' }} <br>
                                                Từ ngày {{ !empty($data_contract_detail['contract_detail']->date_create) ? date('d/m/Y', strtotime($data_contract_detail['contract_detail']->date_create)) : '....................' }} đến ngày {{ !empty($data_contract_detail['contract_detail']->date_end) ? date('d/m/Y', strtotime($data_contract_detail['contract_detail']->date_end)) : '.........................' }} --}}
                                                <b>Dịch vụ {{ $contract_detail->product_type ? $contract_detail->product_type : '' }}</b> <br>
                                                @if ($contract_detail->product_type === 'vps')
                                                    @php $vps = UserHelper::get_contract_detail_vps($contract_detail->target_id) @endphp
                                                    IP: {{ $vps->ip ? $vps->ip : '' }} - Type: {{ $vps->product->name ? $vps->product->name : '' }} <br>
                                                @elseif($contract_detail->product_type === 'server')
                                                    @php $server = UserHelper::get_contract_detail_server($contract_detail->target_id) @endphp
                                                    IP: {{ $server->ip ? $server->ip : '' }} - Type: {{ $server->type ? $server->type : '' }} - Name: {{ $server->server_name ? $server->server_name : '' }} <br>
                                                @elseif($contract_detail->product_type === 'hosting')
                                                    @php $hosting = UserHelper::get_contract_detail_hosting($contract_detail->target_id) @endphp
                                                    Hosting: {{ $hosting->domain ? $hosting->domain : '' }} <br>
                                                @elseif($contract_detail->product_type === 'domain')
                                                    @php $domain = UserHelper::get_contract_detail_domain($contract_detail->target_id) @endphp
                                                    Tên miền: {{ $domain->domain ? $domain->domain : '' }} <br>
                                                @endif
                                                Thời gian sử dụng: {{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }} <br>
                                                Từ ngày {{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '....................' }} đến ngày {{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '.........................' }}
                                            </td>
                                            <td class="text-center">{{ $contract_detail->amount ? number_format($contract_detail->amount, 0, ',', '.') : '' }}</td>
                                            <td class="text-center">{{ $contract_detail->qtt ? $contract_detail->qtt : '' }}</td>
                                            <td class="text-center product_price" value="{{ $contract_detail->amount ? $contract_detail->amount : 0 }}">{{ $contract_detail->amount ? number_format($contract_detail->amount, 0, ',', '.') : '' }}</td>
                                            <td>{{ $contract_detail->note ? $contract_detail->note : '' }}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-center" colspan="4">TỔNG CỘNG</td>
                                        <td id="price" class="text-center"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="4">THUẾ VAT (10%)</td>
                                        <td id="vat" class="text-center"></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center font-weight-bold" colspan="4">TỔNG CHI PHÍ (ĐÃ CÓ VAT)</td>
                                        <td id="sum" class="text-center"></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-12">
                            <div class="pl-4"> 2.2 <span class="pl-4">Phương thức thanh toán: Sau khi hai bên ký kết hợp đồng, bên A thanh toán ngay cho bên B các khoản phí theo Điều 2.1, thanh toán thông qua chuyển khoản (theo Điều 2.3) hoặc tiền mặt.</span></div>
                        </div>
                        <div class="col-12">
                            <div class="pl-4"> 
                                2.3 <span class="pl-4">Tài khoản thanh toán</span> <br>
                                <div class="pl-4">
                                    <p class="pl-4">
                                        Thông tin tài khoản thanh toán của bên B: <br>
                                        <i>Công ty TNHH MTV Công nghệ Đại Việt Số</i> <br>
                                        <i>Số tài khoản: 5651.00000.57234</i> <br>
                                        <i>Ngân hàng: Ngân hàng Đầu tư và Phát triển - Chi nhánh Sông Hàn, Đà Nẵng</i>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 3:</u> TRÁCH NHIỆM CỦA BÊN A</b>
                            <p class="pl-4"> 
                                3.1 <span class="pl-4">Chủ động thanh toán chi phí nêu trên cho Bên B đầy đủ.</span> <br>
                                3.2 <span class="pl-4">Tự chủ động sử dụng các tài nguyên hosting đã được cung cấp theo yêu cầu công việc/hợp đồng của bên A. <br>
                                3.3 <span class="pl-4">Chịu hoàn toàn trách nhiệm với các dịch vụ và nội dung được thiết lập, đăng tải trên hosting của bên A.
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 4:</u> TRÁCH NHIỆM CỦA BÊN B</b>
                            <p class="pl-4"> 
                                4.1 <span class="pl-4">Bảo đảm dịch vụ cung cấp cho bên A hoạt động ổn định</span> <br>
                                4.2 <span class="pl-4">Khi thực hiện tiến hành các thao tác bảo trì, thay đổi hệ thống ảnh hưởng đến dịch vụ phải thông báo cho bên A trước ít nhất 02 ngày, trừ các trường hợp bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. </span> <br>
                                4.3 <span class="pl-4">Đảm bảo dữ liệu của bên A không bị mất mát, trừ các trường hợp lỗi chủ quan đến từ bên B (xóa nhầm dữ liệu, lỗ hổng phần mềm dẫn đến bị xâm nhập, thay đổi dữ liệu, ...) hoặc do các sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5.</span> <br>
                                4.4 <span class="pl-4">Trong trường hợp có sự thay đổi trong hoạt động của bên B làm ảnh hưởng đến các dịch vụ đang sử dụng của bên A, bên B có trách nhiệm đảm bảo các dịch vụ bên A hoạt động ổn định thông qua việc hỗ trợ di chuyển nhà cung cấp dịch vụ, duy trì hệ thống hoạt động cho đến khi chấm dứt hợp đồng giữa 2 bên mà không phát sinh thêm các chi phí khác.</span> <br>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 5:</u> BẤT KHẢ KHÁNG</b>
                            <p class="pl-4"> 
                                5.1 <span class="pl-4">Các sự kiện như thiên tai, dịch họa, lũ lụt, bão, hỏa hoạn, động đất, cháy nổ hoặc các hiểm họa thiên tai khác hoặc sự can thiệp của cơ quan chức năng gọi là bất khả kháng.</span> <br>
                                5.2 <span class="pl-4">Bên bị ảnh hưởng bởi sự kiện bất khả kháng có nghĩa vụ thông báo cho bên còn lại. <br>
                                5.3 <span class="pl-4">Khi sự kiện bất khả kháng chấm dứt, và có khả năng tiếp tục dịch vụ thì các bên sẽ tiếp tục thực hiện hợp đồng. <br>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b><u>ĐIỀU 6:</u> GIA HẠN VÀ THANH LÝ HỢP ĐỒNG</b>
                            <div class="pl-4"> 
                                6.1 <span class="pl-4">Hợp đồng mặc nhiên được gia hạn hằng năm nếu một trong 2 bên không có các yêu cầu liên quan đến hủy dịch vụ khi đến hạn. </span> <br>
                                6.2 <span class="pl-4">Hợp đồng sẽ được thanh lý khi một trong các trường hợp sau <br>
                                    <div class="pl-4">
                                        <div class="pl-4">+ Cả hai bên hoàn tất mọi nghĩa vụ và quyền lợi liên quan tại Hợp đồng này.</div> 
                                        <div class="pl-4">+ Bên A gửi văn bản yêu cầu Bên B chấm dứt việc cung cấp dịch vụ, hai bên cùng trao đổi để hoàn tất các nghĩa vụ liên quan trong hợp đồng trước khi chấm dứt. </div> 
                                        <div class="pl-4">+ Theo yêu cầu của các cơ quan chức năng. </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <p> Hợp đồng được lưu thành 02 bản, mỗi bên giữ 01 bản, có giá trị như nhau.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 text-center">
                            <b>ĐẠI DIỆN BÊN A</b> <br>

                        </div>
                        <div class="col-sm-6 text-center">
                            <b>ĐẠI DIỆN BÊN B</b> <br>
                            <div class="mt-5 pt-5"><b>NGUYỄN THỊ ÁI HOA</b></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="print_contract">

@endsection

@section('scripts')
<script>
    jQuery(document).ready(function($){
        var price = 0;
        $('.product_price').each(function( index ) {
            price += Number($(this).attr('value'));
        });
        $('#price').text(addCommas(price));
        var product_price = $('.product_price').attr('value');
        // var price = Number($('#price').attr('value'));
        var vat = (price*10)/100;
        var sum = price+vat;
        $('#vat').text(addCommas(vat));
        $('#sum').text(addCommas(sum));
        
    });
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
</script>
@endsection