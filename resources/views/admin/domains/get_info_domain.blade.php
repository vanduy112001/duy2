@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
<i class="fas fa-file-invoice nav-icon "></i> Quản lý tên miền
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item "><a href="/admin/domain/">List domain</a></li>
<li class="breadcrumb-item active">Thông tin Domain</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                <div class="bg-success">
                    <p class="text-light">{{session("success")}}</p>
                </div>
                @elseif(session("fails"))
                <div class="bg-danger">
                    <p class="text-light">{{session("fails")}}</p>
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<form action="{{ route('admin.domain.create_domain_with_PA') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper dt-bootstrap">
                <div class="row container mb-3">
                    <div class="col">
                        <h4>Thông tin tên miền {{$domain_name}}</h4>
                    </div>
                </div>
                <div class="row mb-2 mr-2">
                    <div class="col">
                        <a href="{{ route('admin.domain.index') }}" class="btn btn-default btn-sm mb-2 ml-2"><i
                                class="fas fa-list"></i> Danh sách tên miền</a>
                        <input type="submit" class="btn btn-info btn-sm float-right" value="Cập nhập">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                  <option value="{{$user->id}}">{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Chọn sản phẩm:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" class="form-control select2">
                                                <option value="" disabled>Chọn sản phẩm</option>
                                                @foreach ($domain_products as $domain_product)
                                                  <option value="{{$domain_product->id}}">{{$domain_product->type}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control"
                                                style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                <option value="Active" selected>Active</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Cancel" >Cancel</option>
                                            </select>
                                        </div>
                                    </div> -->
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Domain:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="domain" value="{{$domain_name}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="password_domain" value="" placeholder="Password Domain">
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng sử dụng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="customer_domain">
                                                <option value="canhan">Cá nhân</option>
                                                <option value="congty">Công ty</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="date_create" value="{{date('d-m-Y')}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            <input type="next_due_date" class="form-control" name="next_due_date" value="{{date('d-m-Y')}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col-md-8" id="paid_date">
                                            <input type="text" class="form-control" name="date_paid" value="{{date('d-m-Y')}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                          <select class="form-control" name="billing_cycle">
                                              <option value="annually">1 Năm</option>
                                              <option value="biennially">2 Năm</option>
                                              <option value="triennially">3 Năm</option>
                                          </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Giá ghi đè :</label>
                                        </div>
                                        <div class="col-md-8" id="price_override">
                                            <input type="text" class="form-control" name="price_override" value="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="order">
                                {{-- 1 form group --}}
                                <h5 class="font-weight-bold text-info text-center">Thông tin khách hàng (owner)</h5>
                                <div class="form-group">
                                    <label for="owner-name" class="required">Tên (cá nhân hoặc công ty)</label>
                                    <input type="text" class="form-control" name="owner-name" value="{{ !empty($domain->{'owner_name'}) ? $domain->{'owner_name'} : '' }}">
                                </div>
                                <div class="form-group input_ownerid_number">
                                    <label for="ownerid-number" class="required">Chứng minh thư</label>
                                    <input type="text" name="ownerid-number" id="ownerid-number" class="form-control" value="{{ !empty($domain->{'owner_inttraname'}) ? $domain->{'owner_inttraname'} : '' }}" required>
                                </div>
                                <div class="form-group input_owner_taxcode" style="disabled:none;">
                                    <label for="owner-taxcode" class="required">Mã số thuế </label>
                                    <input type="text" name="owner-taxcode" class="form-control" id="owner-taxcode" value="{{ !empty($domain->{'owner_mst'}) ? $domain->{'owner_mst'} : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="owner-address" class="required">Địa chỉ</label>
                                    <input type="text" name="owner-address" class="form-control" id="owner-address" value="{{ !empty($domain->{'owner_address'}) ? $domain->{'owner_address'} : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="owner-email" class="required">Email </label>
                                    <input type="email" name="owner-email" class="form-control" id="owner-email" value="{{ !empty($domain->{'owner_email'}) ? $domain->{'owner_email'} : '' }}">
                                </div>
                                <div class="form-group">
                                    <label for="owner-phone" class="required">Phone </label>
                                    <input type="text" name="owner-phone" id="owner-phone" value="{{ !empty($domain->{'owner_phone'}) ? $domain->{'owner_phone'} : '' }}" class="form-control">
                                </div>
                                {{-- 1 form group --}}
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin chủ thể (ui)</h5>
                                <div class="form-group">
                                    <label for="">Tên chủ thể(cá nhân hoặc công ty):</label>
                                    <input type="text" name="ui_name" value="{{ !empty($domain->{'tech_name'}) ? $domain->{'tech_name'} : '' }}" class="form-control">
                                </div>
                                <div class="form-group input_ui_taxcode">
                                </div>

                                <div class="form-group">
                                    <label for="">Chứng minh nhân dân:</label>
                                    <input type="text" name="uiid_number" value="{{ !empty($domain->{'owner_inttraname'}) ? $domain->{'owner_inttraname'} : '' }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Số điện thoại:</label>
                                    <input type="text" name="ui_phone" value="{{ !empty($domain->{'tech_phone'}) ? $domain->{'tech_phone'} : '' }}"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="text" name="ui_email" value="{{ !empty($domain->{'tech_email'}) ? $domain->{'tech_email'} : '' }}"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Giới tính:</label>
                                    <select class="form-control" name="ui_gender">
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ">Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Ngày sinh:</label>
                                    <input type="text" name="ui_birthdate" value="{{ !empty($domain->{'tech_birthdate'}) ? $domain->{'tech_birthdate'} : '' }}" class="form-control">
                                    <div class="luu-y">
                                        Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Địa chỉ:</label>
                                    <input type="text" name="ui_address" value="{{ !empty($domain->{'tech_address'}) ? $domain->{'tech_address'} : '' }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Tỉnh/Thành phố:</label>
                                    <select class="form-control" name="ui_province">
                                        @foreach($provinces as $province)
                                        <?php
                                            $selected = '';
                                            if ( !empty($domain->{'tech_city'}) ) {
                                              if ($province == $domain->{'tech_city'}) {
                                                $selected = 'selected';
                                              }
                                            }

                                          ?>
                                        <option value="{{ $province }}" {{ $selected }}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row form-group mb-4">
                                    <div class="col-md-6 text-center mt-4">
                                        <p>
                                            <b>Ảnh CMND mặt trước</b>
                                        </p>
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input" name="cmnd_before"
                                                id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước">
                                            <label class="custom-file-label text-left overflow-hidden"
                                                for="ownerid-scan-before">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước" required=""> --}}
                                    </div>
                                    <div class="col-md-6 text-center mt-4">
                                        <p>
                                            <b>Ảnh CMND mặt sau</b>
                                        </p>
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input" name="cmnd_after"
                                                id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau">
                                            <label class="custom-file-label text-left overflow-hidden"
                                                for="ownerid-scan-before">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau" required=""> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin người quản lý (admin)</h5>
                                <div class="form-group">
                                    <label for="admin-name" class="required">Tên</label>
                                    <div class="admin-name">
                                        <input type="text" name="admin-name" id="admin-name" class="form-control" value="{{ !empty($domain->{'admin_name'}) ? $domain->{'admin_name'} : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="adminid-number" class="required">Chứng minh thư</label>
                                    <div class="adminid-number">
                                        <input type="text" name="adminid-number" id="adminid-number" class="form-control" value="{{ !empty($domain->{'admin_idpp'}) ? $domain->{'admin_idpp'} : '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-gender" class="required">Giới tính</label>
                                    <select class="form-control @error('admin-gender') is-invalid @enderror" name="admin-gender" required>
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ" >Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-birthdate" class="required">Ngày sinh</label>
                                    @if ( !empty($domain->{'admin_birthdate'}) )
                                      <input type="text" name="admin-birthdate" id="admin-birthdate" value="{{ date('d-m-Y',  strtotime($domain->{'admin_birthdate'})) }}" class="form-control">
                                    @else
                                      <input type="text" name="admin-birthdate" id="admin-birthdate" value="16/06/1992" class="form-control">
                                    @endif
                                    <div class="luu-y">
                                        Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-address" class="required">Địa chỉ</label>
                                    <div class="admin-address">
                                        <input type="text" name="admin-address" id="admin-address" value="{{ !empty($domain->{'admin_address'}) ? $domain->{'admin_address'} : '' }}"  class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-province" class="required">Tỉnh/Thành phố</label>
                                    <select class="form-control @error('admin-province') is-invalid @enderror"
                                        name="admin-province" required>
                                        <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                        @foreach($provinces as $province)
                                        <?php
                                          $selected = '';
                                          if ( !empty($domain->{'admin_city'}) ) {
                                            if ( $domain->{'admin_city'} == $province) {
                                                $selected = 'selected';
                                            }
                                          }
                                        ?>
                                        <option value="{{$province}}" {{$selected}}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-email" class="required">Email </label>
                                    <div class="admin-email">
                                        <input type="email" name="admin-email" id="admin-email" value="{{ !empty($domain->{'admin_email'}) ? $domain->{'admin_email'} : '' }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-phone" class="required">Phone</label>
                                    <div class="admin-phone">
                                        <input type="text" name="admin-phone" id="admin-phone" value="{{ !empty($domain->{'admin_phone'}) ? $domain->{'admin_phone'} : '' }}" class="form-control"  required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Cập nhật" class="btn btn-info btn-sm float-right mt-2 mr-2">
                </div>
            </div>
        </div>
    </div>
</form>

<input type="hidden" id="page" value="get_info_domain">
@endsection
@section('scripts')
<!-- <script src="{{ asset('js/vps.js') }}"></script> -->
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
		//Date picker
		$('#datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();
    });
    //Chọn file ảnh
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }


</script>
@endsection
