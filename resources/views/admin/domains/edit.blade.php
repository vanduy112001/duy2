@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.domain-products.index') }}"><i class="fa fa-users"
            aria-hidden="true"></i> List domains</a></li>
<li class="breadcrumb-item active"><i class="fa fa-globe" aria-hidden="true"></i> Edit domain</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Sửa domain</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
    <form role="form" method="POST" action="{{ route('admin.domain-products.update') }}">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="type">Loại</label>
                    <input type="text" name="type" class="form-control" id="type" placeholder="Loại tên miền"
                        value="{{ old('type') ? old('type') : $domainproduct->type }}">
                </div>
                <div class="form-group">
                    <label>Vùng</label>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="global_domain" name="local_type" value="Quốc tế" @if ($domainproduct->local_type == 'Quốc tế') checked @endif>
                        <label for="global_domain" class="custom-control-label">Quốc tế</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="vietnam_domain" name="local_type" value="Việt Nam" @if ($domainproduct->local_type == 'Việt Nam') checked @endif>
                        <label for="vietnam_domain" class="custom-control-label">Việt Nam</label>
                    </div>
                </div>
                <hr>
                <h5 class="text-success" style="font-weight:bold; text-align: center;">Giá đăng ký mới</h5>
                <div class="form-group">
                    <label for="annually">Giá 1 năm</label>
                    <input type="annually" name="annually" class="form-control" id="annually" placeholder="Giá 1 năm"
                        value="{{ old('annually') ? old('annually') : $domainproduct->annually }}">
                </div>
                <div class="form-group">
                    <label for="biennially">Giá 2 năm</label>
                    <input type="biennially" name="biennially" class="form-control" id="biennially" placeholder="Giá 2 năm"
                        value="{{ old('biennially') ? old('biennially') : $domainproduct->biennially }}">
                </div>
                <div class="form-group">
                    <label for="triennially">Giá 3 năm</label>
                    <input type="triennially" name="triennially" class="form-control" id="triennially" placeholder="Giá 3 năm"
                        value="{{ old('triennially') ? old('triennially') : $domainproduct->triennially }}">
                </div>
                <hr>
                <h5 class="text-danger" style="font-weight:bold; text-align: center;">Giá gia hạn</h5>
                <div class="form-group">
                    <label for="annually_exp">Giá 1 năm</label>
                    <input type="annually_exp" name="annually_exp" class="form-control" id="annually_exp" placeholder="Giá 1 năm"
                        value="{{ old('annually_exp') ? old('annually_exp') : $domainproduct->annually_exp }}">
                </div>
                <div class="form-group">
                    <label for="biennially_exp">Giá 2 năm</label>
                    <input type="biennially_exp" name="biennially_exp" class="form-control" id="biennially_exp" placeholder="Giá 2 năm"
                        value="{{ old('biennially_exp') ? old('biennially_exp') : $domainproduct->biennially_exp }}">
                </div>
                <div class="form-group">
                    <label for="triennially_exp">Giá 3 năm</label>
                    <input type="triennially_exp" name="triennially_exp" class="form-control" id="triennially_exp" placeholder="Giá 3 năm"
                        value="{{ old('triennially_exp') ? old('triennially_exp') : $domainproduct->triennially_exp }}">
                </div>
                <div class="form-group row mt-2">
                    <div class="col-md-2">
                        <label for="">Khuyến mãi</label>
                    </div>
                    <div class="col-md-10">
                        <div class="icheck-primary d-inline">
                            <input class="custom-control-input mb-2" type="checkbox" name="promotion" id="promotion" value="1"  @if( !empty( $domainproduct->promotion ) ) checked @endif>
                            <label for="promotion"></label>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.domain-products.index') }} ">Hủy</a>
                <input type="hidden" name="id" value="{{ $domainproduct->id }}">
                <button type="submit" class="btn btn-info float-right">Cập nhập</button>
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="edit_domain">
@endsection
@section('scripts')

@endsection
