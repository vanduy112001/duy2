@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
<i class="fa fa-globe" aria-hidden="true"></i> Danh sách domain
<div class="pl-4 mt-4" id="form_get_info_domain">
  <button type="button" name="button" class="btn btn-success" id="get_info_domain">Thêm Domain từ PA</button>
</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-globe" aria-hidden="true"></i> List domains</li>
@endsection
@section('content')
<div class="row">

</div>

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@elseif(session("error"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("error")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
    <!-- /.card-header -->
    <div class="card-header p-0">
        <div class="card-header">
            <div class="card-tools float-left">
                <div class="input-group input-group-sm">
                    <button type="submit" id="check_delete" class="btn btn-sm btn-default check_delete m-1"><i
                        class="fa fa-trash" aria-hidden="true"></i> Xóa mục đã chọn</button>
                    <button id="check-money-still" class="btn btn-sm btn-info m-1" data-toggle="modal" data-target="#info-check-money-still" title="Thông tin domain"><i class="fas fa-dollar-sign"></i> Số dư</button>
                    <button class="btn btn-secondary btn-sm m-1 create-domain" data-toggle="modal" data-target="#create-domain" title="Tạo tên miền"><i class="fas fa-edit"></i>Tạo tên miền</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table id="data-domain-table" class="table table-hover">
            <thead>
                <tr>
                    <th>
                        <input class="check_all" id="check_all" type="checkbox" value="1">
                    </th>
                    <th>ID</th>
                    <th>Domain</th>
                    <th>Người dùng</th>
                    <th>Vùng</th>
                    <th>Ngày thanh toán</th>
                    <th>Thời hạn</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($domains->count() > 0)
                    @foreach($domains as $domain)

                        {{-- kiểm tra hết hạn hoặc gần hết hạn --}}
                        @php
                            $isExpire = false;
                            $expired = false;
                            if(!empty($domain->next_due_date)){
                                $next_due_date = strtotime(date('Y-m-d', strtotime($domain->next_due_date)));
                                $date = date('Y-m-d');
                                $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
                                if($next_due_date <= $date) {
                                $isExpire = true;
                                }
                                $date_now = strtotime(date('Y-m-d'));
                                if ($next_due_date < $date_now) {
                                    $expired = true;
                                }
                            }
                        @endphp

                        <tr data-row-id="{{ $domain->id }}">
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="check_id" value="{{ $domain->id }}">
                                </div>
                            </td>
                            <td>{{ $domain->id }}</td>
                            <td><a href="{{ route('admin.domain.detail', $domain->id) }}">{{ $domain->domain }}</a></td>
                            <td>
                              {{ !empty($domain->user->name) ? $domain->user->name : 'Đã xóa' }}
                            </td>
                            <td>{{ $domain->domain_product->local_type }}</td>
                            <td>{{ !empty($domain->detail_order->paid_date) ? $domain->detail_order->paid_date : '' }}</td>
                            <td>{{ !empty($domain->detail_order->due_date) ? $domain->next_due_date : '' }} </td>
                            <td>
                                @if ( $domain->status == 'Pending')
                                    <span class="text-danger">{{ $domain->status }}</span>
                                @elseif($domain->status == 'Active')
                                    @if (strtotime(date("Y-m-d")) < strtotime($domain->next_due_date))
                                        <span class="text-success">{{ $domain->status }}</span>
                                    @else
                                        <span class="text-danger">Đã hết hạn</span>
                                    @endif
                                @else
                                    <span class="text-primary">Đang chờ tạo</span> @endif
                            </td>
                            <td>
                                @if ($expired)
                                    <button type="button" class="btn btn-warning expired-domain" data-toggle="modal" data-id="{{ $domain->id }}" data-target="#domain-{{ $domain->id }}" data-toggle="tooltip" title="Gia hạn">
                                        <i class="fas fa-plus-circle"></i>
                                    </button>
                                @endif
                                @if ($domain->status == 'Active')
                                    <button name="info-domain" id="{{ $domain->id }}" class="btn btn-info info-domain" data-toggle="modal" data-target="#info-domain" title="Thông tin domain"><i class="fa fa-info-circle"></i></button>
                                @endif
                                <button name="delete-domain" id="{{ $domain->id }}" class="btn btn-danger delete-domain" data-toggle="modal" data-target="#delete-domain" title="Xóa domain"><i class="fas fa-trash-alt"></i></button>

                            </td>
                        </tr>
                    @endforeach
                @else
                    <td colspan="6" class="text-center text-danger">
                        Chưa có domain
                    </td>
                @endif
            </tbody>
        </table>
    </div>
</div>

{{-- Modal xóa domain --}}
<div class="modal fade" id="delete-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Xóa tên miền</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="loading" style="text-align: center;"></div>
          Bạn muốn xóa tên miền (id: <span class="text-danger font-weight-bold id-confirm"></span>) này không?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary delete-cancel" data-dismiss="modal">Hủy</button>
            <a class="btn btn-success delete-success" href="{{ route('admin.domain.index') }}">Hoàn thành</a>
          <button type="button" class="btn btn-primary delete-confirm">Xác nhận</button>
        </div>
      </div>
    </div>
  </div>

{{-- Modal thông tin domain --}}
<div class="modal fade" id="info-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title show-domain" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body loading" style="margin:auto">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary info-domain-cancel" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>

{{-- Modal lấy số dư đại lý --}}
<div class="modal fade" id="info-check-money-still" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Kiểm tra số dư của Cloudzone trên PA Viet Nam</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body show-info-check-money-still">
            <div class="loading" style="text-align: center;">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary info-domain-cancel" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>

{{-- Modal tạo domain --}}
<div class="modal fade" id="create-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Tạo tên miền mới</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('admin.domain.create_domain_form') }}" method="get">
            <div class="modal-body">
                <div class="form-group">
                    <label for="domain-input">Nhập tên miền cần tạo</label>
                    <input name="domain" type="text" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="domain-time">Chọn thời gian</label>
                    <select name="billing_cycle" class="form-control" id="domain-time">
                        <option value="" disabled>Chọn thời gian</option>
                        <option value="annually">1 Năm</option>
                        <option value="biennially">2 Năm</option>
                        <option value="triennially">3 Năm</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary info-domain-cancel" data-dismiss="modal">Đóng</button>
                <input type="submit" class="btn btn-success" value="Xác nhận">
            </div>
        </form>
      </div>
    </div>
  </div>
    {{-- Modal gia hạn --}}
    <div class="modal fade modal-exp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Gia hạn tên miền</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">
                    <form action="{{ route('admin.domain.extend_domain_expired') }}" method="post">
                        <div class="form-group">
                                <label for="billing">Chọn thời gian gia hạn</label>
                                <div class="mt-3 mb-3">
                                    <select name="billing" class="form-control select2 time" id="billing">
                                    </select>
                                </div>

                                <div class="card">
                                    <div class="card-header text-center">
                                        <h3 class="card-title m-0">Bảng giá gia hạn</h3>
                                    </div>
                                    <div class="loading"></div>
                                    <div class="card-body p-0 show-price">
                                        <table class="table table-sm">
                                            <thead><tr class="add-domain"></tr></thead>
                                            <tbody class="add-billing"></tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                @csrf
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
              <input type="hidden" name="domain" value="">
              <input type="submit" class="btn btn-block btn-primary" style="width:auto" value="Xác nhận">
            </div>
        </form>
          </div>
        </div>
      </div>
<input type="hidden" id="page" value="list_domain">
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        $('#check-money-still').on('click', function(){
            var show = '';
            $.ajax({
                url: "{{ route('admin.domain.check_account_still') }}",
                type: 'get',
                dataType: 'json' ,
                beforeSend: function() {
                    $('.loading').html('<div class="spinner-border spinner-border-sm text-info" ></div>');
                },
                success: function(data) {
                    if(data.status == true) {
                        show += '<div class="row">';
                        show += '<div class="col-md-6 col-sm-6 col-12">';
                        show += '<div class="info-box">';
                        show += '<span class="info-box-icon bg-info"><i class="far fa-user"></i></span>';
                        show += '<div class="info-box-content">';
                        show += '<span class="info-box-text">User</span>';
                        show += '<span class="info-box-number">'+data.user_name+'</span>';
                        show += '</div>';
                        show += '</div>';
                        show += '</div>';
                        show += '<div class="col-md-6 col-sm-6 col-12">';
                        show += '<div class="info-box">';
                        show += '<span class="info-box-icon bg-success"><i class="fas fa-money-check-alt"></i></span>';
                        show += '<div class="info-box-content">';
                        show += '<span class="info-box-text">Số dư</span>';
                        show += '<span class="info-box-number">'+data.money+'</span>';
                        show += '</div>';
                        show += '</div>';
                        show += '</div>';
                        show += '</div>';
                        $('.show-info-check-money-still').html(show);
                    } else {
                        $('.show-info-check-money-still').html('<span class="text-danger">'+data.message+'</span>');
                    }
                },
                error: function(e){
                    $('.show-info-check-money-still').html('<span class="text-danger">Truy vấn lỗi</span>');
                }
            });
        });
        $('.info-domain').on('click', function(){
            var id = $(this).attr('id');
            var show = '';
            var title = '';
            $.ajax({
                url: '/admin/domain/info',
                data: {'id': id},
                dataType: 'json' ,
                type: 'get',
                beforeSend: function() {
                    $('.loading').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data){
                    title = 'Thông tin quản lý tên miền <a href="http://'+ data.domain +'" target="_blank">'+ data.domain +'</a></span><br>';
                    show += '- Tên miền đăng ký: <a href="http://'+ data.domain +'" target="_blank">'+ data.domain +'</a></span><br>';
                    show += '- Ngày hết hạn: '+data.next_due_date;
                    show += '<br>';
                    show += '- Qúy khách có thể login với thông tin bên dưới để cấu hình Tên miền:<br>';
                    show += '<div style="margin-top: 10px; padding: 10px; border: solid 1px; border-radius: 5px;">';
                    show += '<a href="http://dotvn.cloudzone.vn" target="_blank">http://dotvn.cloudzone.vn</a><br>';
                    show += '+ Tên miền: '+data.domain+'<br>';
                    show += '+ Mật khẩu: <span class="show-pass">'+data.password_domain+'</span><button class="btn btn-info badge badge-pill ml-1 show-form" data-toggle="collapse" data-target="#form-change-pass">Đổi mật khẩu</button><br>';
                    show += '<div class="show-form-change-pass-domain collapse" id="form-change-pass">';
                    show += '<form id="form-change-pass-domain" class="form-inline input-group-sm mt-2" method="POST" action="{{ route('admin.domain.change_pass_domain') }}">';
                    show += '@csrf';
                    show += '<div class="form-group input-group-sm">';
                    show += '<input type="text" name="password_domain" class="form-control password-domain" placeholder="Nhập mật khẩu mới" autocomplete="off" required>';
                    show += '<input type="hidden" name="domain-id" value="'+data.id+'" class="form-control password-domain">';
                    show += '</div>';
                    show += '<input type="submit" class="btn btn-primary btn-sm ml-2" value="Xác nhận">';
                    show += '</form>';
                    show += '<div class="status">';
                    show += '</div>';
                    show += '</div>';
                    show += '</div>';
                    $('.show-domain').html(title);
                    $('.loading').html(show);
                },
                error: function(e) {
                    $('.modal-body').html('Truy vấn lỗi');
                },
            });
        });
        $('#info-domain').delegate('#form-change-pass-domain', 'submit', function(e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            var html = '';
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: form.serialize(),
                beforeSend: function() {
                    $('.status').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data) {
                    if(data.status == true) {
                        $('.status').html('<span class="text-success">Đổi mật khẩu thành công</span>');
                        $('.show-pass').text(data.password);
                    } else {
                        console.log(data);
                        if(data.message.password_domain == null) {
                            $('.status').html('<span class="text-danger">'+data.message+'</span>');
                        } else {
                            $('.status').html('<span class="text-danger">'+data.message.password_domain+'</span>');
                        }
                    }
                },
                error: function(e) {
                    $('.status').html('<span class="text-danger">Truy vấn lỗi</span>');
                }
            });
        });
        //Click để ẩn và hiện các row
         $("#check_all").click(function(){
            $('input:checkbox').prop('checked', this.checked);
        });
        // Hiện button danger
        $('input:checkbox').change(function () {
            var check = $('.form-check-input:checked');
            if (check.length > 0) {
                $("#check_delete").addClass('btn-danger').removeClass('btn-default');
            } else if(check.length == 0){
                $("#check_delete").addClass('btn-default').removeClass('btn-danger');
            }
        });
        $('.delete-domain').on('click', function(){
            var id = $(this).attr('id');
            $('.id-confirm').text(id);
            $('.delete-success').hide();
            $('.delete-confirm').attr('data-id', id);
        });
        $('.delete-confirm').on('click', function(){
            var id = $('.delete-confirm').attr('data-id');
            var show = '';
            $.ajax({
                url: '/admin/domain/delete',
                data: {id: id},
                dataType: 'json' ,
                type: 'get',
                beforeSend: function() {
                    $('.loading').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data){
                    console.log(data);
                    show += '<span>'+ data.message + id +'</span>';
                    $('.loading').hide();
                    $('.modal-body').html(show);
                    $('.delete-success').show();
                    $('.delete-confirm').hide();
                    $('.delete-cancel').hide();
                    $('table tr').filter("[data-row-id='" + id + "']").remove();
                },
                error: function(e) {
                    $('.modal-body').html('Truy vấn lỗi');
                },
            });
        });
        //Xóa nhiều domain
        $('.check_delete').on('click', function () {
            var r = confirm("Bạn muốn xóa các mục đã chọn?");
            if (r == true) {
                var array = [];
                var value = $('.form-check-input:checked');
                if (value.length > 0) {
                    $(value).each(function () {
                        array.push($(this).val());
                    })
                } else {
                    alert('Chưa chọn mục cần xóa');
                }
                console.log(array);
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.domain.multidelete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": array
                    },
                    success: function (data) {
                        alert(data.message);
                        $.each(data.id, function( index, value ) {
				            $('table tr').filter("[data-row-id='" + value + "']").remove();
			            });
                    },
                    error: function (e) {
                        alert(data.message);
                    }
                });
            }
        });

        //show modal bảng giá và xác nhận gia hạn
        $('.expired-domain').on('click', function(){
            var get_id = $(this).attr('data-id');
            var class_domain_id = 'domain-'+get_id;
            $('.modal-exp').attr('id', class_domain_id);
            $.ajax({
                type: 'get',
                url: '/domain/domain-product-expired-price',
                data: {id: get_id},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.loading').html(html);
                    $('.show-price').hide();
                },
                success: function(data) {
                    console.log(data);
                    var html = '';
                    var html_billing = '';
                    var html_time = '<option value="" disabled>Chọn thời gian</option>';
                    html += '<th>domain</th>';
                    html += '<th class="text-danger">'+data.domain.domain+'</th>';
                    $.each(data.billings, function(index, value){
                        console.log(data);
                        if(data.domain_product[index] != null) {
                            html_billing += '<tr>';
                            html_billing += '<td>'+ value +' Năm</td>';
                            html_billing += '<td>'+ addCommas(data.domain_product[index]) +' VNĐ</td>';
                            html_billing += '</tr>';
                            html_time +='<option value="'+index+'" data-year="'+value+'" data-domain="'+data.domain.domain+'">'+ value +' Năm</option>';
                        }
                    });
                    $('input[name="domain"]').val(data.domain.domain);
                    $('.add-billing').html(html_billing);
                    $('.add-domain').html(html);
                    $('.time').html(html_time);
                    $('.show-price').show();
                    $('.loading').html('');
                },
                error: function(e) {
                    $('.loading').css('opacity', '1');
                    $('.loading').html('Không hiển thị được giá sản phẩm');
                },

            });
        });
    });
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
</script>
<!-- DataTables -->
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#data-domain-table").DataTable({
            "columnDefs": [
                 { "targets": [0, 4, 5, 6, 7, 8], "orderable": false }
            ]
        });

        $('#get_info_domain').on('click', function () {
            var html = "";
            html += "<form action='/admin/lay-thong-tin-domain' method='get'>";
            html += '<div class="form-group col-md-12">';
            html += '<div class="input-group">';
            html += '<input type="text" name="domain" class="form-control" placeholder="Nhập domain">';
            html += '<div class="input-group-append">';
            html += '<button type="submit" class="btn btn-info"><i class="fas fa-search"></i></button>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</form>';
            $('#form_get_info_domain').html(html);
        })

    });

</script>
@endSection
