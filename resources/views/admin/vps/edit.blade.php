@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý VPS
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/vps/">VPS</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa VPS</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <div class="col-12 text-center mt-2 mb-4">
              <h4 class="text-info">Thông tin VPS {{$detail->ip}}</h4>
            </div>
            <div class="container">
                <form action="{{ route('admin.vps.update_sevices') }}" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Tên khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                    @php
                                                        $selected = '';
                                                        if ($user->id == $detail->user_id) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                                @foreach ($products as $product)
                                                    @php
                                                        $selected = '';
                                                        if ($product->id == $detail->product_id) {
                                                                $selected = 'selected';
                                                        }
                                                        $group_product = $product->group_product;
                                                        if ($group_product->private == 0) {
                                                           $group_user_name = 'Cơ bản';
                                                        } else {
                                                           $group_user_name = $group_product->group_user->name;
                                                        }
                                                    @endphp
                                                    <option value="{{$product->id}}" {{$selected}}>{{$product->name}} - {{ $group_user_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">IP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="ip" value="{{$detail->ip}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">User:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_username" value="{{$detail->user}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_password" value="{{$detail->password}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">VM ID:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="vm_id" value="{{$detail->vm_id}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                @if ($detail->status == 'Active')
                                                    <option value="Active" selected>Đã tạo</option>
                                                    <option value="Pending">Chưa tạo</option>
                                                    <option value="Cancel">Hủy</option>
                                                @elseif ($detail->status == 'Pending')
                                                    <option value="Active" >Đã tạo</option>
                                                    <option value="Pending" selected>Chưa tạo</option>
                                                    <option value="Cancel">Hủy</option>
                                                @else
                                                    <option value="Active" >Đã tạo</option>
                                                    <option value="Pending" >Chưa tạo</option>
                                                    <option value="Cancel" selected>Hủy</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="status_vps">Trạng thái VPS:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status_vps" id="status_vps" class="form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái VPS</option>
                                                @foreach ($status_vps as $key => $status)
                                                  <?php
                                                      $selected = '';
                                                      if ($detail->status_vps == $key) {
                                                         $selected  = 'selected';
                                                      }
                                                  ?>
                                                  <option value="{{ $key }}" {{ $selected }}>{{ $status }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Cấu hình:</label>
                                        </div>
                                        <div class="col-md-8">
                                            @php
                                                $error = 0;
                                                if ( !empty($detail->product) ) {
                                                    $product = $detail->product;
                                                    $cpu = $product->meta_product->cpu;
                                                    $ram = $product->meta_product->memory;
                                                    $disk = $product->meta_product->disk;
                                                    // Addon

                                                    if (!empty($detail->vps_config)) {
                                                        $detail_config = $detail->vps_config;
                                                        if (!empty($detail_config)) {

                                                            $cpu += (int) !empty($detail_config->cpu) ? $detail_config->cpu : 0;
                                                            $ram += (int) !empty($detail_config->ram) ? $detail_config->ram : 0;
                                                            $disk += (int) !empty($detail_config->disk) ? $detail_config->disk : 0;
                                                        }
                                                    }
                                                } else {
                                                    $error = 1;
                                                }
                                            @endphp
                                            @if ( !$error )
                                                {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} DISK <br>
                                            @else
                                                <span class="text-danger">Sản phẩm đã xóa</span>
                                            @endif
                                        </div>
                                    </div>
                                    @if (!empty($detail->vps_config))
                                        <input type="hidden" name="addon_vps" value="1">
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon CPU:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_cpu" class="form-control" value="{{ !empty($detail_config->cpu) ? $detail_config->cpu : 0 }}">
                                            </div>
                                        </div>
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon RAM:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_ram" class="form-control" value="{{ !empty($detail_config->ram) ? $detail_config->ram : 0 }}">
                                            </div>
                                        </div>
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon DISK:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_disk" class="form-control" value="{{ !empty($detail_config->disk) ? $detail_config->disk : 0 }}">
                                            </div>
                                        </div>
                                    @else
                                        <input type="hidden" name="addon_vps" value="1">
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon CPU:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_cpu" class="form-control" value="0">
                                            </div>
                                        </div>
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon RAM:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_ram" class="form-control" value="0">
                                            </div>
                                        </div>
                                        <div class="group-invoice">
                                            <div class="col-md-4 text-right">
                                                <label for="">Addon DISK:</label>
                                            </div>
                                            <div class="col-md-8">
                                                <input type="number" name="addon_disk" class="form-control" value="0">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-success">{{date('d-m-Y', strtotime($detail->created_at))}}</p>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày tạo :</label>
                                        </div>
                                        <div class="col-md-8" id="date_create" >
                                            {{-- <input type="text" id="datepicker" name="created_at" placeholder="" value="{{date('d-m-Y', strtotime($detail_order->created_at))}}" class="form-control"> --}}
                                            @if (!empty($detail->date_create))
                                                {{date('d-m-Y', strtotime($detail->date_create))}}
                                                <span class="ml-4">
                                                   <a href="#" class="btn btn-outline-default date_create" data-date="{{date('d-m-Y', strtotime($detail->date_create))}}"><i class="fas fa-edit"></i></a>
                                                </span>
                                            @else
                                                <p class="text-danger">Chưa tạo</p>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            @if (!empty($detail->next_due_date))
                                                {{date('d-m-Y', strtotime($detail->next_due_date))}}
                                                <span class="ml-4">
                                                   <a href="#" class="btn btn-outline-default next_due_date" data-date="{{date('d-m-Y', strtotime($detail->next_due_date))}}"><i class="fas fa-edit"></i></a>
                                                </span>
                                            @else
                                                <p class="text-danger">Chưa tạo</p>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col-md-8" id="paid_date">
                                            @if(!empty($detail->paid))
                                                @if($detail->paid == 'paid')
                                                    <span class="text-success">{{date('d-m-Y', strtotime($detail->detail_order->paid_date))}}</span>
                                                    <!-- <span class="ml-4">
                                                       <a href="#" class="btn btn-outline-default paid_date" data-date="{{date('d-m-Y', strtotime($detail->detail_order->paid_date))}}"><i class="fas fa-edit"></i></a>
                                                    </span> -->
                                                @elseif($detail->paid == 'cancel')
                                                    <span class="text-secondary">Hủy</span>
                                                @else
                                                    <span class="text-danger">Chưa thanh toán</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa thanh toán</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                            {{ $billings[$detail->billing_cycle] }}
                                            <span class="ml-4">
                                               <a href="#" class="btn btn-outline-default billing_cycle" data-name="{{$detail->billing_cycle}}"><i class="fas fa-edit"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col-md-4 text-right" style="padding-top: 5px;">
                                          <label for="">Giá áp tay :</label>
                                      </div>
                                      <div class="col-md-8 ml-2 icheck-primary d-inline">
                                          <input type="checkbox" class="custom-control-input" name="price_override" id="price_override" @if(!empty($detail->price_override)) checked @endif  value="1">
                                          <label for="price_override">
                                          </label>
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col-md-4 text-right" style="padding-top: 5px;">
                                          <label for="">GH theo chu kỳ :</label>
                                      </div>
                                      <div class="col-md-8 ml-2 icheck-primary d-inline">
                                          <input type="checkbox" class="custom-control-input" name="expire_billing_cycle" id="expire_billing_cycle" @if(!empty($detail->expire_billing_cycle)) checked @endif  value="1">
                                          <label for="expire_billing_cycle">
                                          </label>
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8" id="sub_total">
                                            @if(!empty($detail->price_override))
                                              <span class="mt-1">
                                                <b>{!!number_format( $detail->price_override ,0,",",".")!!} VNĐ</b>
                                              </span>
                                              <input type="hidden" id="sub_total_hidden" data-price="{{$detail->price_override}}" name="sub_total_hidden" value="{{$detail->price_override}}">
                                            @else
                                              <span class="mt-1">
                                                <?php
                                                    $product = $detail->product;
                                                    $sub_total = !empty( $product->pricing[$detail->billing_cycle] ) ? $product->pricing[$detail->billing_cycle] : 0;
                                                    if (!empty($detail->vps_config)) {
                                                      $addon_vps = $detail->vps_config;
                                                      $add_on_products = UserHelper::get_addon_product_private($detail->user_id);
                                                      $pricing_addon = 0;
                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$detail->billing_cycle];
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$detail->billing_cycle];
                                                          }
                                                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$detail->billing_cycle] / 10;
                                                          }
                                                        }
                                                      }
                                                      $sub_total += $pricing_addon;
                                                    }
                                                ?>
                                                <b>{!!number_format(($sub_total),0,",",".")!!} VNĐ</b>
                                              </span>
                                              <input type="hidden" id="sub_total_hidden"  data-price="{{$detail->sub_total}}" name="sub_total_hidden" value="{{$detail->detail_order->sub_total}}">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-button mt-4 mr-4">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $detail->id }}">
                                        <input type="hidden" name="type" value="vps">
                                        <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                                        <input type="submit" value="Cập nhật VPS" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <a href="" class="btn btn-outline-success btn-action-edit-vps @if($detail->status == 'Active') disabled @endif" data-type="create" data-id="{{ $detail->id }}">Tạo VPS</a>
                            <a href="" class="btn btn-outline-secondary btn-action-edit-vps @if($detail->status == 'Cancel') disabled @endif" data-type="cancel" data-id="{{ $detail->id }}">Hủy VPS</a>
                            <a href="" class="btn btn-outline-danger btn-action-edit-vps" data-type="delete" data-id="{{ $detail->id }}">Xóa VPS</a>
                            <a href="" class="btn btn-success btn-action-edit-vps @if($detail->paid == 'paid') disabled @endif" data-type="paid" data-id="{{ $detail->id }}">Đã thanh toán</a>
                            <a href="" class="btn btn-danger btn-action-edit-vps @if($detail->paid == 'unpaind' || empty($detail->paid)) disabled @endif" data-type="unpaid" data-id="{{ $detail->id }}">Chưa thanh toán</a>
                            <a href="" class="btn btn-outline-warning btn-action-edit-vps @if($detail->status_vps == 'on') disabled @endif @if($detail->status == 'Pending') disabled @endif" data-type="on" data-id="{{ $detail->id }}">Bật VPS</a>
                            <a href="" class="btn btn-outline-danger btn-action-edit-vps @if($detail->status_vps == 'off') disabled @endif @if($detail->status == 'Pending') disabled @endif" data-type="off" data-id="{{ $detail->id }}">Tắt VPS</a>
                        </div> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="card card-outline card-success">
    <div class="card-header">
      <h3 class="card-title">Thanh toán</h3>
      <!-- /.card-tools -->
    </div>
    <div class="card-body table-responsive">
      <table class="table table-bordered">
          <thead class="primary">
              <th>Loại</th>
              <th>Trạng thái</th>
              <th>Thời gian</th>
          </thead>
          <tbody>
            @foreach ( $log_payments as $log_payment )
                <tr>
                  <td>{{ $log_payment->action }}</td>
                  <td>{!! $log_payment->description !!}</td>
                  <td>{{ date('H:i:s d-m-Y', strtotime($log_payment->created_at) ) }}</td>
                </tr>
            @endforeach
          </tbody>
      </table>
    </div>
</div>
<div class="card card-outline card-danger">
    <div class="card-header">
      <h3 class="card-title">Hoạt động</h3>
      <!-- /.card-tools -->
    </div>
    <div class="card-body">
      <table class="table table-bordered">
          <thead class="primary">
              <th>Loại</th>
              <th>Trạng thái</th>
              <th>Thời gian</th>
          </thead>
          <tbody>
            @foreach ( $log_activities as $log_activity )
                <tr>
                  <td>{{ $log_activity->action }}</td>
                  <td>{!! !empty($log_activity->description) ? $log_activity->description : $log_activity->description_user !!}</td>
                  <td>{{ date('H:i:s d-m-Y', strtotime($log_activity->created_at) ) }}</td>
                </tr>
            @endforeach
          </tbody>
      </table>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-vps">Xóa VPS</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="edit_vps">
@endsection
@section('scripts')
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/vps.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
        $('#datepicker2').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();

	});
</script>
@endsection
