@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-envelope-square"></i> Chỉnh sửa Email Marketing
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Email Marketing</li>
@endsection
@section('content')
<div class="col col-md-12">
    <h4 class="mb-2">Chỉnh sửa Email Marketing</h4>
    <hr class="text-secondary">
</div>
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success mv-4">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger mv-4">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger mv-4">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <form class="col-12 row m-0 p-0" action="{{route('email.email_marketing.update')}}" method="post">
            @csrf
            <div class="col-12 col-md-8 col-lg-9">
                <div class="form-group">
                    <label for="nameEmail">Tiêu đề</label>
                    <input type="text" id="nameEmail" name="name" value="{{ $email->name }}" class="form-control">
                </div>
                <div class="form-group">
                    <label for="subject-email">Subject</label>
                    <input type="text" id="subject-email" name="subject" class="form-control" value="{{ $email->meta_email->subject }}">
                </div>
                <div class="form-group">
                    <label for="content-email">Nội dung</label>
                    <textarea name="content" id="content-email" cols="30" rows="10" class="form-control">{{ $email->meta_email->content }}</textarea>
                </div>
            </div>
            <div class="col-12 col-md-4 col-lg-3">
                <div class="row">
                    <div class="col-12">
                        <label for="group_user">Nhóm đại lý</label>
                        <div class="form-group">
                            <select id="group_user" class="select2" name="group_user_id[]" multiple="" style="width: 100%;">
                                <option value="99" @if( !$email->meta_email->type_group ) selected @endif>Tất cả</option>
                                @if( !$email->meta_email->type_group )
                                    <option value="0">Cơ bản</option>
                                    @foreach( $list_group_user as $group_user )
                                        <option value="{{ $group_user->id }}">{{ $group_user->name }}</option>
                                    @endforeach
                                @else
                                    @php
                                        $selected = '';
                                        foreach( $email->email_group_users as $key => $email_group_user ) {
                                            if ($email_group_user->group_user_id == 0) {
                                                $selected = 'selected';
                                            }
                                        }
                                    @endphp
                                    <option value="0" {{ $selected }}>Cơ bản</option>
                                    @foreach( $list_group_user as $group_user )
                                        @php
                                            $selected = '';
                                            foreach( $email->email_group_users as $key => $email_group_user ) {
                                                if ($email_group_user->group_user_id == $group_user->id) {
                                                    $selected = 'selected';
                                                }
                                            }
                                        @endphp
                                        <option value="{{ $group_user->id }}" {{ $selected }}>{{ $group_user->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <label for="date">Ngày</label>
                        <div class="form-group">
                            <input type="text" class="form-control" name="date" id="datepicker" value="{{ $email->meta_email->date }}">
                        </div>
                    </div>
                    <div class="col-12">
                        <label for="time">Thời gian</label>
                        <div class="form-group">
                            <div class="input-group date" id="timepicker" data-target-input="nearest">
                              <input type="text" class="form-control datetimepicker-input" name="time" data-target="#timepicker" value="{{ $email->meta_email->time }}" />
                              <div class="input-group-append" data-target="#timepicker" data-toggle="datetimepicker">
                                  <div class="input-group-text"><i class="far fa-clock"></i></div>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-12">
                                        <label for="status" >Trạng thái</label>
                                        <div class="form-group">
                                            <select id="status" class="form-control" name="status">
                                                <option value="0" @if(!$email->status) selected @endif>Tắt</option>
                                                <option value="1" @if($email->status) selected @endif>Gửi</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="row">
                                    <div class="col-12 text-right">
                                        <input type="hidden" name="_method" value="put">
                                        <input type="hidden" name="id" value="{{ $email->id }}">
                                        <input type="submit" class="btn btn-primary" value="Chỉnh sửa">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="tips-email">
        <div class="row">
            <div class="col col-md-6">
                <b>Client</b>
                <br>
                <div class="row">
                    <div class="col col-md-6">
                        User ID
                    </div>
                    <div class="col col-md-6">
                        {$user_id}
                    </div>
                    <div class="col col-md-6">
                        User Name
                    </div>
                    <div class="col col-md-6">
                        {$user_name}
                    </div>
                    <div class="col col-md-6">
                        Email Address
                    </div>
                    <div class="col col-md-6">
                        {$user_email}
                    </div>
                    <div class="col col-md-6">
                        Địa chỉ
                    </div>
                    <div class="col col-md-6">
                        {$user_addpress}
                    </div>
                    <div class="col col-md-6">
                        Số điện thoại
                    </div>
                    <div class="col col-md-6">
                        {$user_phone}
                    </div>
                    <div class="col col-md-6">
                        Url website
                    </div>
                    <div class="col col-md-6">
                        {$url}
                    </div>
                   <!--  <div class="col col-md-6">
                        Url control panel
                    </div>
                    <div class="col col-md-6">
                        {$url_control_panel}
                    </div>
                    <div class="col col-md-6">
                        Url kích hoạt order
                    </div>
                    <div class="col col-md-6">
                        portal.cloudzone.vn/order/verify/{$token}
                    </div> -->
                </div>
            </div>
            <div class="col col-md-6">
                <b>Product / Services</b>
                <br>
                <!-- <div class="row">
                    <div class="col col-md-6">
                        Order ID
                    </div>
                    <div class="col col-md-6">
                        {$order_id}
                    </div>
                    <div class="col col-md-6">
                        Tên sản phẩm
                    </div>
                    <div class="col col-md-6">
                        {$product_name}
                    </div>
                    <div class="col col-md-6">
                        Domain
                    </div>
                    <div class="col col-md-6">
                        {$domain}
                    </div>
                    <div class="col col-md-6">
                        Ip
                    </div>
                    <div class="col col-md-6">
                        {$ip}
                    </div>
                    <div class="col col-md-6">
                        Port
                    </div>
                    <div class="col col-md-6">
                        {$port}
                    </div>
                    <div class="col col-md-6">
                        Thời gian thuê
                    </div>
                    <div class="col col-md-6">
                        {$billing_cycle}
                    </div>
                    <div class="col col-md-6">
                        Ngày kết thúc
                    </div>
                    <div class="col col-md-6">
                        {$next_due_date}
                    </div>
                    <div class="col col-md-6">
                        Ngày đặt hàng
                    </div>
                    <div class="col col-md-6">
                        {$order_created_at}
                    </div>
                    <div class="col col-md-6">
                        Username (Services)
                    </div>
                    <div class="col col-md-6">
                        {$services_username}
                    </div>
                    <div class="col col-md-6">
                        Password (Services)
                    </div>
                    <div class="col col-md-6">
                        {$services_password}
                    </div>
                    <div class="col col-md-6">
                        Giá
                    </div>
                    <div class="col col-md-6">
                        {$sub_total}
                    </div>
                    <div class="col col-md-6">
                        Thành tiền
                    </div>
                    <div class="col col-md-6">
                        {$total}
                    </div>
                    <div class="col col-md-6">
                        Số lượng
                    </div>
                    <div class="col col-md-6">
                        {$qtt}
                    </div>
                    <div class="col col-md-6">
                        Hệ điều hành
                    </div>
                    <div class="col col-md-6">
                        {$os}
                    </div>
                    <div class="col col-md-6">
                        Token
                    </div>
                    <div class="col col-md-6">
                        {$token}
                    </div>
                    <div class="col col-md-6">
                        List IP VPS yêu cầu thay đổi
                    </div>
                    <div class="col col-md-6">
                        {$list_ip_vps}
                    </div>
                    <div class="col col-md-6">
                        List IP VPS đã thay đổi
                    </div>
                    <div class="col col-md-6">
                        {$list_ip_changed_vps}
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/moment/moment.min.js') }}"></script>
<script src="{{ asset('libraries/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content');
        $('.select2').select2();
        $('#datepicker').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy',
        });
        //Timepicker
        $('#timepicker').datetimepicker({
          format: 'HH:mm'
        })
    })
</script>
@endsection
