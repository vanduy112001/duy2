@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-chart-line"></i> Quản lý Email Marketing
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Email Marketing</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    <div id="notication">

    </div>
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="m-4">
            <a href="{{ route('email.email_marketing.create') }}" class="btn btn-primary">Thêm Email</a>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover text-center">
                  <thead>
                    <th width="25%">Tiêu đề</th>
                    <th width="20%">Mục tiêu</th>
                    <th width="15%">Thời gian <br>(Setting)</th>
                    <th width="15%">Bắt đầu gởi</th>
                    <th width="10%">Trạng thái</th>
                    <th width="20%">Hành động</th>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot></tfoot>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product" class="text-center">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <button class="btn btn-primary" id="button-product">Xác nhận</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_state">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_email_marketing.js') }}"></script>
@endsection