@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Quản lý danh sách báo giá
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Danh sách báo giá</li>
    <li class="breadcrumb-item active">Xem trước báo giá</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
                <div class="bg-success">
                    <p class="text-light">{{session("success")}}</p>
                </div>
            @elseif(session("fails"))
                <div class="bg-danger">
                    <p class="text-light">{{session("fails")}}</p>
                </div>
            @endif
        </div>
        <div id="invoice-page">
            <!-- Main content -->
            <div class="invoice quotation-body p-3 mb-3">
                <!-- title row -->
                <div class="row invoice-header quotation-header">
                    <div class="col col-md-8 invoice-status">
                      <address class="company">
                          Công ty TNHH MTV Công nghệ Đại Việt Số<br>
                          Địa chỉ: 257 Lê Duẩn, Đà Nẵng<br>
                          Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng <br>
                          SĐT: 088 888 0043 <span class="ml-3 mr-3">-</span> Website: <a href="https://cloudzone.vn">https://cloudzone.vn</a>
                      </address>
                    </div>
                    <div class="col col-md-4 logo text-right">
                      <img src="{{ url('images/LogoCloud.png') }}" alt="cloudzone.vn" width="160">
                    </div>
                    <hr class="quotation-header-hr-bottom">
                <!-- /.col -->
                </div>
                <!-- info row -->
                <div class="row quotation-info mt-4">
                  <div class="col-md-12 text-center">
                    <p class="quotation-info-title">
                      BÁO GIÁ DỊCH VỤ
                      @if ( !empty($quotation->quotation_details[0]->type_product) )
                        @if ( $quotation->quotation_details[0]->type_product == 'vps' || $quotation->quotation_details[0]->type_product == 'vps_us' )
                          THUÊ MÁY CHỦ ẢO CLOUD VPS
                        @elseif ( $quotation->quotation_details[0]->type_product == 'server' )
                          THUÊ MÁY CHỦ VẬT LÝ
                        @elseif ( $quotation->quotation_details[0]->type_product == 'colocation' )
                          THUÊ COLOCATION
                        @elseif ( $quotation->quotation_details[0]->type_product == 'design_webiste' )
                          THIẾT KẾ WEBSITE
                        @elseif ( $quotation->quotation_details[0]->type_product == 'domain' )
                          THUÊ TÊN MIỀN
                        @else
                          THUÊ HOSTING
                        @endif
                      @endif
                    </p>
                  </div>
                  <div class="col-md-12 quotation-info-user">
                      <div class="quotation-info-user-info">
                        <strong>Kính gởi: </strong> Quý khách hàng
                        @if (!empty($quotation->name_replace))
                          {{ $quotation->name_replace }}
                        @else
                          {{ !empty($quotation->user->name) ? $quotation->user->name : '<strong class="text-danger">Đã xóa</strong>' }}
                        @endif
                        <br>
                        <p class="mt-1">Chúng tôi, trân trọng gởi đến quý khách hàng báo giá dịch vụ
                          @if ( $quotation->quotation_details[0]->type_product == 'vps' )
                            Cloud VPS
                          @elseif ( $quotation->quotation_details[0]->type_product == 'vps_us' )
                            Cloud VPS US
                          @elseif ( $quotation->quotation_details[0]->type_product == 'server' )
                            máy chủ vật lý
                          @elseif ( $quotation->quotation_details[0]->type_product == 'colocation' )
                            Colocation
                          @elseif ( $quotation->quotation_details[0]->type_product == 'design_webiste' )
                            thiết kế website
                          @elseif ( $quotation->quotation_details[0]->type_product == 'domain' )
                            Domain
                          @else
                            Hosting
                          @endif
                          như sau:
                        </p>
                      </div>
                      <div class="table-responsive table-quotation" style="width: 80%;">
                        <table class="table table-bordered">
                            <thead class="bg-table-cyal text-center">
                              <th width="5%">STT</th>
                              <th width="47%">Nội dung sản phẩm</th>
                              <th width="12%">Đơn giá/
                                {{ !empty($billings[$quotation->quotation_details[0]->price_billing_cycle]) ? $billings[$quotation->quotation_details[0]->price_billing_cycle] : 'Tháng' }}
                                 (VNĐ)
                              </th>
                              <th width="12%">Số lượng</th>
                              <th width="12%">Thời gian</th>
                              <th width="12%">Thành tiền (VNĐ)</th>
                            </thead>
                            <tbody>
                              @php
                                  $total = 0;
                              @endphp
                              @foreach ($quotation->quotation_details as $key => $quotation_detail)
                              @php
                       //     dd($quotation_detail->product);
                              @endphp
                                <tr>
                                  <td class="text-center">{{ $key + 1 }}</td>
                                  <td  class="description">
                                    <b>
                                      @if ( $quotation_detail->type_product == 'vps' )
                                        Dịch vụ máy chủ ảo Cloud VPS
                                      @elseif ( $quotation_detail->type_product == 'vps_us' )
                                        Dịch vụ máy chủ ảo Cloud VPS US
                                      @elseif ( $quotation_detail->type_product == 'server' )
                                        THUÊ MÁY CHỦ VẬT LÝ
                                      @elseif ( $quotation_detail->type_product == 'colocation' )
                                        Dịch vụ Colocation
                                      @elseif ( $quotation_detail->type_product == 'design_webiste' )
                                        Thiết kế website {{ $quotation_detail->domain }}
                                      @elseif ( $quotation_detail->type_product == 'domain' )
                                        Domain {{ $quotation_detail->domain }}
                                      @else
                                        Hosting {{ $quotation_detail->domain }}
                                      @endif
                                    </b> <br>
                                    @if( $quotation_detail->type_product == 'vps' || $quotation_detail->type_product == 'vps_us'  )T
                                      @php
                                      
                                          $product = $quotation_detail->product;
                                         
                                          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                          if ( $quotation_detail->addon_cpu ) {
                                            $cpu += $quotation_detail->addon_cpu;
                                          }
                                          if ( $quotation_detail->addon_ram ) {
                                            $ram += $quotation_detail->addon_ram;
                                          }
                                          if ( $quotation_detail->addon_disk ) {
                                            $disk += $quotation_detail->addon_disk;
                                          }
                                        
                                      @endphp
                                      Cấu hình chi tiết: {{ $cpu }} core - {{ $ram }} ram - {{ $disk }} disk <br>
                                      @elseif ($quotation_detail->type_product == 'colocation')
                                     @php      
                                     $product = $quotation_detail->product;   
                                     $id = $quotation_detail->product->id;
                                     $ip = $product->meta_product->ip; 
                                     $os =   $product->meta_product->os; 
                                     $bandwidth = $product->meta_product->bandwidth; 
                                      $datacenter = $quotation_detail->datacenter;
                                     @endphp             
                                      Cấu hình chi tiết: <br> - AddonIP:  {{$ip}} <br> - Loại u: {{$os}} <br> - Băng thông: {{$bandwidth}}  <br>
                                      Địa chỉ: {{$datacenter}} <br>
                                    @elseif( $quotation_detail->type_product == 'server' )
                                      <b>Cấu hình chi tiết:</b> <br>
                                      $addonIp = 0;
                                      if ( !empty($quotation_detail->quotation_server->addon_ip) ) {
                                        $product_addon_ip = $quotation_detail->quotation_server->product_addon_ip;
                                        $addonIp = !empty($product_addon_ip->meta_product->ip) ? $product_addon_ip->meta_product->ip : 0;
                                      }
                                      @php
                                          $product = $quotation_detail->product;
                                          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                          $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                          $addonRam = 0;
                                          $addonIp = 0;
                                          $text_config = '';
                                          if ( !empty($quotation_detail->quotation_server->addon_ram) ) {
                                            $product_addon_ram = $quotation_detail->quotation_server->product_addon_ram;
                                            $addonRam = !empty($product_addon_ram->meta_product->memory) ? $product_addon_ram->meta_product->memory : 0;
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_ip) ) {
                                            $product_addon_ip = $quotation_detail->quotation_server->product_addon_ip;
                                            $addonIp = !empty($product_addon_ip->meta_product->ip) ? $product_addon_ip->meta_product->ip : 0;
                                          }
                                          $data_config = [];
                                          if ( !empty($quotation_detail->quotation_server->addon_disk2) ) {
                                            $data_config[] = [
                                              'id' => $quotation_detail->quotation_server->addon_disk2,
                                              'name' => $quotation_detail->quotation_server->product_disk2->name,
                                              'qtt' => 1
                                            ];
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk3) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk3) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk3,
                                                'name' => $quotation_detail->quotation_server->product_disk3->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk4) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk4) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk4,
                                                'name' => $quotation_detail->quotation_server->product_disk4->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk5) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk5) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk5,
                                                'name' => $quotation_detail->quotation_server->product_disk5->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk6) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk6) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk6,
                                                'name' => $quotation_detail->quotation_server->product_disk6->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk7) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk7) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk7,
                                                'name' => $quotation_detail->quotation_server->product_disk7->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( !empty($quotation_detail->quotation_server->addon_disk8) ) {
                                            $check = true;
                                            foreach ($data_config as $key => $data) {
                                              if ($data['id'] == $quotation_detail->quotation_server->addon_disk8) {
                                                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                                                $check = false;
                                              }
                                            }
                                            if ( $check ) {
                                              $data_config[] = [
                                                'id' => $quotation_detail->quotation_server->addon_disk8,
                                                'name' => $quotation_detail->quotation_server->product_disk8->name,
                                                'qtt' => 1
                                              ];
                                            }
                                          }
                                          if ( count($data_config) ) {
                                            $text_config .= '- Addon Disk: ';
                                            foreach ($data_config as $key => $data) {
                                              $text_config .= ( $data['qtt'] . ' &#215; ' . $data['name']  );
                                              if ( count( $data_config ) - 1 > $key ) {
                                                $text_config .= ' - ';
                                              }
                                            }
                                            $text_config .= '';
                                          }
                                      @endphp
                                      {{-- in ra --}}
                                      &nbsp;&nbsp;- CPU: {{ $cpu }} ({{ $cores }}) <br>
                                      &nbsp;&nbsp;- Memory: {{ $ram }} <br>
                                      &nbsp;&nbsp;- Disk: {{ $disk }} <br>
                                      @if ($addonRam)
                                        &nbsp;&nbsp;- Addon RAM: {{ $addonRam }} GB RAM <br>
                                      @endif
                                      &nbsp;&nbsp;{!! $text_config !!}<br>
                                      @if ($addonIp)
                                        &nbsp;&nbsp;- Addon IP: {{ $addonIp }} IP Public <br>
                                      @endif
                                      Đã bao gồm chỗ đặt tại Datacenter
                                      <br>
                                 
                                    @endif
                                    Thời gian thuê:
                                    {{ !empty($billings[$quotation_detail->billing_cycle]) ? $billings[$quotation_detail->billing_cycle] : '1 tháng' }}
                                    (từ ngày {{ date('d/m/Y', strtotime($quotation_detail->start_date)) }}
                                    đến ngày {{ date('d/m/Y', strtotime($quotation_detail->end_date)) }})
                                    <br>
                                    @if ( !empty($quotation_detail->os) )
                                      Hệ điều hành: {{ $quotation_detail->os }}
                                    @endif
                                  </td>
                                  <td>
                                    @if ( $quotation_detail->type_product != 'domain' && $quotation_detail->type_product != 'design_webiste' )
                                      {{ number_format( $quotation_detail->amount ,0,",",".") }}
                                    @endif
                                  </td>
                                  @php
                                      $billing_meta = !empty($billingDashBoard[$quotation_detail->billing_cycle]) ? $billingDashBoard[$quotation_detail->billing_cycle] : 1;
                                      $price_billing_cycle = !empty($billingDashBoard[$quotation_detail->price_billing_cycle]) ? $billingDashBoard[$quotation_detail->price_billing_cycle] : 1;
                                      $total += $quotation_detail->amount * $quotation_detail->qtt * $billing_meta / $price_billing_cycle;
                                  @endphp
                                  <td  class="text-center">{{ $quotation_detail->qtt }}</td>
                                  <td  class="text-center">
                                    @if ( $quotation_detail->type_product != 'design_webiste' )
                                      {{ !empty($billings[$quotation_detail->billing_cycle]) ? $billings[$quotation_detail->billing_cycle] : '1 Tháng' }}
                                    @endif
                                  </td>
                                  @if( $quotation_detail->type_product != 'design_webiste' )
                                    <td class="text-center">{{ number_format( $quotation_detail->amount * $quotation_detail->qtt * $billing_meta / $price_billing_cycle,0,",",".") }}</td>
                                  @else
                                    <td class="text-center">{{ number_format( $quotation_detail->amount * $quotation_detail->qtt / $price_billing_cycle,0,",",".") }}</td>
                                  @endif
                                </tr>
                              @endforeach
                              @if ( !empty($quotation->vat) )
                                <tr>
                                  <td></td>
                                  <td>THUẾ VAT (10%)</td>
                                  <td colspan="3"></td>
                                  <td class="text-center">{{ number_format( $total/10 ,0,",",".") }}</td>
                                </tr>
                              @endif
                              <tr>
                                <td></td>
                                <td><strong>TỔNG CHI PHÍ</strong></td>
                                <td colspan="3"></td>
                                <td class="text-center"><strong>{{ number_format( $quotation->total ,0,",",".") }}</strong></td>
                              </tr>
                            </tbody>
                        </table>
                        <span>(Bằng chữ: {{ $quotation->text_total }})</span>
                      </div>
                      <div class="mt-2">
                        <strong>Ghi chú:
                        <span class="ml-1"></span>	Chu kỳ thanh toán {{ $billings[$quotation->payment_cycle] }}/lần, vào đầu mỗi chu kỳ.</strong>
                      </div>
                      <div class="row mt-4">
                        <div class="col-md-4"></div>
                        <div class="col-md-6 text-center">
                          <i>
                            Đà Nẵng,
                            ngày {{ date( 'd', strtotime($quotation->created_at) ) }}
                            tháng {{ date( 'm', strtotime($quotation->created_at) ) }}
                            năm {{ date( 'Y', strtotime($quotation->created_at) ) }}
                          </i>
                          <br>
                          <p class="mt-2"><strong>GIÁM ĐỐC</strong></p>
                          <div class="mt-4 mb-4 pt-4 pb-4"></div>
                          <p class="mt-4"><strong> NGUYỄN THỊ ÁI HOA </strong></p>
                        </div>
                      </div>
                  </div>
                </div>
                <!-- /.row -->
            </div>
            <div class="text-center mt-4">
              <a href="{{ route('admin.quotation.edit', $quotation->id) }}" class="btn btn-warning text-light">Chỉnh sửa báo giá</a>
              <a href="{{ route('admin.quotation.create_pdf', $quotation->id) }}" class="btn btn-success text-light">Tạo PDF</a>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="preview_quotation">
@endsection
@section('scripts')
@endsection
