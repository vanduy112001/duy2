@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <style>
        #create-user .modal-dialog {
            max-width: 65% !important;
        }
        .card-tools {
            float: right;
            transform: translateY(-18px);
        }
    </style>
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Tạo báo giá mới
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="/admin">Home</a></li>
  <li class="breadcrumb-item active">Danh sách báo giá</li>
  <li class="breadcrumb-item active">Tạo báo giá mới</li>
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Chỉnh sửa báo giá
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="/admin">Home</a></li>
  <li class="breadcrumb-item active">Danh sách báo giá</li>
  <li class="breadcrumb-item active">Chỉnh sửa báo giá</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title pl-4">Chỉnh sửa báo giá</h3>
    </div>
    <div class="box-body table-responsive">
        <form action="{{route('admin.quotation.update')}}" method="post" class="p-4">
            @csrf
            <div class="form-group">
                <label for="title">Tiêu đề</label>
                <input type="text" id="title" name="title" value="{{ $quotation->title }}" class="form-control" required>
                <input type="hidden" name="quotation_id" value="{{ $quotation->user_id }}">
            </div>
            <div class="form-group">
                <label for="user">Khách hàng</label>
                <select class="form-control select2" name="user_id" id="user" required style="width:100%;">
                    <option value="" disabled selected>Chọn khách hàng</option>
                    @foreach( $users as $user )
                      <?php
                          $selected = '';
                          if ( $user->id == $quotation->user_id ) {
                              $selected = 'selected';
                          }
                       ?>
                       <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }} </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                @if (!empty($quotation->name_replace))
                    <div id="name_replace">
                        <label for="ip_name_replace">Tên thay thế (Họ và tên hoặc tên công ty)</label>
                        <input type="text" id="ip_name_replace" name="name_replace" value="{{ $quotation->name_replace }}" class="form-control" placeholder="Tên thay thế (Họ và tên hoặc tên công ty)">
                    </div>
                @else 
                    <button type="button" id="btn_name_replace" class="btn btn-primary btn-sm p-2 mt-3 mb-3"><i class="far fa-plus-square"></i> Đổi tên người báo giá</button>
                    <div id="name_replace">
                    </div>
                @endif
            </div>
            <div class="choose-product mt-4">
                <div class="form-group">
                    <label for="type_product">Loại sản phẩm</label>
                    <select name="type_product[0]" id="type_product" class="form-control">
                       
                    </select>
                </div>
                <div class="form-group">
                    <label for="product">Sản phẩm</label>
                    <div class="product">
                      <input type="text" class="form-control" name="" value="" disabled>
                    </div>
                </div>
                <div class="info_quotation">
    
                </div>
            </div>
            <button type="button" id="btn_add_product" class="btn btn-success btn-sm p-2 mt-3 mb-3"><i class="far fa-plus-square"></i> Thêm sản phẩm</button>
            <div id="add_product">

            </div>
            {{-- <div class="form-group">
                <label for="amount">Thành tiền</label>
                <input type="text" id="total" name="total" value="{{ !empty($quotation->total) ? $quotation->total : 0 }}" class="form-control" disabled>
            </div> --}}
            <div class="form-group">
                <label for="payment_cycle">Chu kỳ thanh toán / lần</label>
                <select class="form-control" name="payment_cycle" id="payment_cycle" required>
                    @foreach( $billings as $key => $billing )
                      <?php
                          $selected = '';
                          if ( $key == $quotation->payment_cycle ) {
                              $selected = 'selected';
                          }
                       ?>
                       <option value="{{ $key }}" {{ $selected }}>{{ $billing }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <div class="col-1">
                    <label for="vat" class="">Thuế VAT</label>
                </div>
                <div class="col-11 clearfix">
                    <div class="icheck-primary d-inline">
                        <input type="checkbox" id="vat" name="vat" @if( !empty( $quotation->vat ) ) checked @endif>
                        <label for="vat"></label>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <input type="hidden" name="id" id="id" value="{{ $quotation->id }}">
                <input type="submit" class="btn btn-primary" value="Sửa báo giá">
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="edit_qoutation">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('js/editquotation.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        
        $('#btn_name_replace').on('click', function () {
            let html = '<label for="ip_name_replace">Tên thay thế (Họ và tên hoặc tên công ty)</label>';
            html += '<input type="text" id="ip_name_replace" name="name_replace" value="" class="form-control" placeholder="Tên thay thế (Họ và tên hoặc tên công ty)">';
            $(this).fadeOut();
            $('#name_replace').html(html);
        });
    })
</script>
@endsection
