@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <style>
        #create-user .modal-dialog {
            max-width: 65% !important;
        }
        .card-tools {
            float: right;
            transform: translateY(-18px);
        }
    </style>
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Tạo báo giá mới
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="/admin">Home</a></li>
  <li class="breadcrumb-item active">Danh sách báo giá</li>
  <li class="breadcrumb-item active">Tạo báo giá mới</li>
@endsection
@section('content')
{{-- box-primary --}}
<div class="box box-success box-solid" style="width: 80%; margin: auto;">
    <div class="box-header with-border">
        <h3 class="box-title pl-4">Tạo báo giá mới</h3>
    </div>
    <div class="box-body table-responsive form-pading">
        <form action="{{route('admin.quotation.store')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="title">Tiêu đề</label>
                <input type="text" id="title" name="title" value="{{ old('title') }}" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="user">Khách hàng</label>
                <div class="row">
                    <div class="col-11">
                        <select class="form-control select2" name="user_id" id="user" required style="width:100%;">
                            <option value="" disabled selected>Chọn khách hàng</option>
                            @foreach( $users as $user )
                              <?php
                                  $selected = '';
                                  if ( $user->id == $userId ) {
                                      $selected = 'selected';
                                  }
                               ?>
                               <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-1">
                        <button type="button" id="add_user" class="btn btn-outline-info" data-toggle="tooltip" 
                        data-placement="top" title="Thêm khách hàng"><i class="fas fa-user-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <button type="button" id="btn_name_replace" class="btn btn-primary btn-sm p-2 mt-3 mb-3"><i class="far fa-plus-square"></i> Đổi tên người báo giá</button>
                <div id="name_replace">
                </div>
            </div>
            <div class="choose-product mt-4">
                <div class="form-group">
                    <label for="type_product">Loại sản phẩm</label>
                    <select name="type_product[0]" id="type_product" class="form-control">
                        <option value="" disabled selected>Chọn loại sản phẩm</option>
                        <option value="vps">VPS</option>
                        <option value="vps_us">VPS US</option>
                        <option value="server">Server vật lý</option>
                        <option value="colocation">Colocation</option>
                        <option value="design_webiste">Thiết kế webiste</option>
                        <option value="domain">Domain</option>
                        <option value="hosting">Hosting</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="product">Sản phẩm</label>
                    <div class="product">
                      <input type="text" class="form-control" name="" value="" disabled>
                    </div>
                </div>
                <div class="info_quotation">
    
                </div>  
            </div>
            <button type="button" id="btn_add_product" class="btn btn-success btn-sm p-2 mt-3 mb-3"><i class="far fa-plus-square"></i> Thêm sản phẩm</button>
            <div id="add_product">

            </div>
            {{-- <div class="form-group">
                <label for="amount">Thành tiền</label>
                <input type="text" id="total" name="total" value="{{ !empty(old('total')) ? old('total') : 0 }}" class="form-control" disabled>
            </div> --}}
            <div class="form-group">
                <label for="payment_cycle">Chu kỳ thanh toán / lần</label>
                <select class="form-control" name="payment_cycle" id="payment_cycle" required>
                    @foreach( $billings as $key => $billing )
                      <?php
                          $selected = '';
                          if ( $key == old('payment_cycle') ) {
                              $selected = 'selected';
                          }
                       ?>
                       <option value="{{ $key }}" {{ $selected }}>{{ $billing }}</option>
                    @endforeach
                </select>
            </div>
            <div class="row form-group">
                <div class="col-1">
                    <label for="vat" class="">Thuế VAT</label>
                </div>
                <div class="col-11 clearfix">
                    <div class="icheck-primary d-inline">
                        <input type="checkbox" id="vat" name="vat" @if( !empty( old('vat') ) ) checked @endif>
                        <label for="vat"></label>
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                <input type="submit" class="btn btn-primary" value="Tạo báo giá">
            </div>
        </form>
    </div>
</div>
{{-- Modal xoa product --}}
<div class="modal fade" id="create-user">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Thêm khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="container_notification">

            </div>
            <div id="container_user">
                <form role="form" id="form_create_user">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="name">Họ và Tên</label>
                            </div>
                            <div class="col-10">
                                <input type="text" name="name" class="form-control" id="name" placeholder="Họ và Tên" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="gender">Giới tính</label>
                            </div>
                            <div class="col-10">
                                <select class="form-control" name="gender">
                                    <option value="nam" selected="">Nam</option>
                                    <option value="nữ">Nữ</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="address">Địa chỉ</label>
                            </div>
                            <div class="col-10">
                                <input type="text" name="address" class="form-control" id="address" placeholder="Địa chỉ" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="email">Email</label>
                            </div>
                            <div class="col-10">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="phone">Phone</label>
                            </div>
                            <div class="col-10">
                                <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="company">Công ty / Tổ chức</label>
                            </div>
                            <div class="col-10">
                                <input type="text" name="company" class="form-control" id="company" placeholder="Công ty / Tổ chức" value="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label>Nhóm đại lý</label>
                            </div>
                            <div class="col-10">
                                <select class="form-control" name="group_user_id">
                                    <option value="0">Cơ bản</option>
                                    @foreach($group_users as $group_user)
                                      @php
                                          $selected = '';
                                          if( $group_user->id == old('group_user_id') ) {
                                              $selected = 'selected';
                                          }
                                      @endphp
                                      <option value="{{ $group_user->id }}">{{ $group_user->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="enterprise">Doanh nghiệp</label>
                            </div>
                            <div class="col-10 clearfix">
                                <div class="icheck-primary d-inline">
                                    <input type="checkbox" class="custom-control-input" name="enterprise" id="enterprise" value="1">
                                    <label for="enterprise"></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row" id="mst">
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="password">Mật khẩu</label>
                            </div>
                            <div class="col-10">
                                <div class=" input-group">
                                    <input type="password" name="password" class="form-control" id="password" placeholder="Password" value="">
                                    <div class="input-group-append show_password">
                                        <span class="input-group-text"><i class="fas fa-eye"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="password_confirmation">Nhập lại mật khẩu</label>
                            </div>
                            <div class="col-10">
                                <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                                    placeholder="Nhập lại mật khẩu" value="">
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="btn_submit">Xác nhận</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal" id="btn_finish">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="create_quotation">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('js/create_quotation.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();

        $('.date_create').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
        });
    })
</script>
@endsection
