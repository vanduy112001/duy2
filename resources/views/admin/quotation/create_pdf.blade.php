<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="application/pdf">

    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <title>Cloudzone Portal Manager</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudzone">

    <link rel="stylesheet" href="{{ asset('libraries/test_pdf/style.css') }}" media="all" />

    <style type="text/css">
      html, body
        {
          /* font-family: DejaVu Sans !important; */
          /* font-size: 12pt !important; */
          /* font-family: DejaVu Sans ,sans-serif !important;  */
          font-family: "dejavu sans", serif;
        }
      .text-danger {
        color: red;
      }
    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{ url('images/logo-bg-login.png') }}" alt="cloudzone.vn" width="120" height="55.5">
      </div>
      <div id="company">
        <span style="color: #3f3e3e;">Công ty TNHH MTV Công nghệ Đại Việt Số</span><br>
        <span style="color: #3f3e3e;">Địa chỉ: 257 Lê Duẩn, Đà Nẵng</span><br>
        <span style="color: #3f3e3e;">Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng</span> <br>
        <span style="color: #3f3e3e;">SĐT: 088 888 0043</span> <span class="ml-3 mr-3">-</span>
        <span style="color: #3f3e3e;">Website: <a href="https://cloudzone.vn">https://cloudzone.vn</a></span>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <p class="quotation-info-title">
            BÁO GIÁ DỊCH VỤ
            @if ( !empty($quotation->quotation_details[0]->type_product) )
              @if ( $quotation->quotation_details[0]->type_product == 'vps' || $quotation->quotation_details[0]->type_product == 'vps_us' )
                THUÊ MÁY CHỦ ẢO CLOUD VPS
              @elseif ( $quotation->quotation_details[0]->type_product == 'server' )
                THUÊ MÁY CHỦ VẬT LÝ
              @elseif ( $quotation->quotation_details[0]->type_product == 'colocation' )
                THUÊ COLOCATION
              @elseif ( $quotation->quotation_details[0]->type_product == 'design_webiste' )
                THIẾT KẾ WEBSITE
              @elseif ( $quotation->quotation_details[0]->type_product == 'domain' )
                THUÊ TÊN MIỀN
              @else
                THUÊ HOSTING
              @endif
            @endif
          </p>
        </div>
        <div id="invoice">
          <div class="quotation-info-user-info">
            <strong>Kính gởi: </strong> Quý khách hàng
            @if (!empty($quotation->name_replace))
              {{ $quotation->name_replace }}
            @else
              {{ !empty($quotation->user->name) ? $quotation->user->name : '<strong class="text-danger">Đã xóa</strong>' }}
            @endif
            <br>
            Chúng tôi, trân trọng gởi đến quý khách hàng báo giá dịch vụ
              @if ( $quotation->quotation_details[0]->type_product == 'vps' )
                Cloud VPS
              @elseif ( $quotation->quotation_details[0]->type_product == 'vps_us' )
                Cloud VPS US
              @elseif ( $quotation->quotation_details[0]->type_product == 'server' )
                máy chủ vật lý
              @elseif ( $quotation->quotation_details[0]->type_product == 'colocation' )
                Colocation
              @elseif ( $quotation->quotation_details[0]->type_product == 'design_webiste' )
                thiết kế website
              @elseif ( $quotation->quotation_details[0]->type_product == 'domain' )
                Domain
              @else
                Hosting
              @endif
            như sau:
          </div>
        </div>
      </div>
      <table class="table table-bordered">
        <thead class="bg-table-cyal">
          <tr>
            <th width="5%">STT</th>
            <th width="40%">Nội dung sản phẩm</th>
            <th width="12%">Đơn giá/
              {{ !empty($billings[$quotation->quotation_details[0]->price_billing_cycle]) ? $billings[$quotation->quotation_details[0]->price_billing_cycle] : 'Tháng' }}
               (VNĐ)
             </th>
            <th width="12%">Số lượng</th>
            <th width="12%">Thời gian</th>
            <th width="19%">Thành tiền (VNĐ)</th>
          </tr>
        </thead>
        <tbody>
          @php
              $total = 0;
          @endphp
          @foreach ($quotation->quotation_details as $key => $quotation_detail)
            <tr>
              <td class="text-center">{{ $key + 1 }}</td>
              <td  class="description">
                <b>
                  @if ( $quotation_detail->type_product == 'vps' )
                    Dịch vụ máy chủ ảo Cloud VPS
                  @elseif ( $quotation_detail->type_product == 'vps_us' )
                    Dịch vụ máy chủ ảo Cloud VPS US
                  @elseif ( $quotation_detail->type_product == 'server' )
                    Dịch vụ máy chủ vật lý
                  @elseif ( $quotation_detail->type_product == 'colocation' )
                    Dịch vụ Colocation
                  @elseif ( $quotation_detail->type_product == 'design_webiste' )
                    Thiết kế website {{ $quotation_detail->domain }}
                  @elseif ( $quotation_detail->type_product == 'domain' )
                    Domain {{ $quotation_detail->domain }}
                  @else
                    Hosting {{ $quotation_detail->domain }}
                  @endif
                </b> <br>
                @if( $quotation_detail->type_product == 'vps' || $quotation_detail->type_product == 'vps_us' )
                  @php
                      $product = $quotation_detail->product;
                      $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                      $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                      $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                      if ( $quotation_detail->addon_cpu ) {
                        $cpu += $quotation_detail->addon_cpu;
                      }
                      if ( $quotation_detail->addon_ram ) {
                        $ram += $quotation_detail->addon_ram;
                      }
                      if ( $quotation_detail->addon_disk ) {
                        $disk += $quotation_detail->addon_disk;
                      }
                  @endphp
                  Cấu hình chi tiết: {{ $cpu }} core - {{ $ram }} ram - {{ $disk }} disk <br>
                  @elseif ($quotation_detail->type_product == 'colocation')
                  @php      
                  $product = $quotation_detail->product;   
                  $id = !empty($quotation_detail->product->id) ? $quotation_detail->product->id : 0;
                  $ip = !empty($product->meta_product->ip) ? $product->meta_product->ip : 0; 
                  $os = !empty($product->meta_product->os) ? $product->meta_product->os : 0 ; 
                  $bandwidth = !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : 0; 
                  $datacenter = !empty($quotation_detail->datacenter) $quotation_detail->datacenter : 0;
                  @endphp             
                   Cấu hình chi tiết: <br> - AddonIP:  {{$ip}} <br> - Loại u: {{$os}} <br> - Băng thông: {{$bandwidth}}  <br>
                   Địa chỉ: {{$datacenter}} <br>
                @elseif( $quotation_detail->type_product == 'server')
                  <b>Cấu hình chi tiết:</b> <br>
                  @php
                      $product = $quotation_detail->product;
                      $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                      $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                      $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                      $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                      $addonRam = 0;
                      $addonIp = 0;
                      $text_config = '';
                      if ( !empty($quotation_detail->quotation_server->addon_ram) ) {
                        $product_addon_ram = $quotation_detail->quotation_server->product_addon_ram;
                        $addonRam = !empty($product_addon_ram->meta_product->memory) ? $product_addon_ram->meta_product->memory : 0;
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_ip) ) {
                        $product_addon_ip = $quotation_detail->quotation_server->product_addon_ip;
                        $addonIp = !empty($product_addon_ip->meta_product->ip) ? $product_addon_ip->meta_product->ip : 0;
                      }
                      $data_config = [];
                      if ( !empty($quotation_detail->quotation_server->addon_disk2) ) {
                        $data_config[] = [
                          'id' => $quotation_detail->quotation_server->addon_disk2,
                          'name' => $quotation_detail->quotation_server->product_disk2->name,
                          'qtt' => 1
                        ];
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk3) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk3) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk3,
                            'name' => $quotation_detail->quotation_server->product_disk3->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk4) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk4) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk4,
                            'name' => $quotation_detail->quotation_server->product_disk4->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk5) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk5) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk5,
                            'name' => $quotation_detail->quotation_server->product_disk5->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk6) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk6) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk6,
                            'name' => $quotation_detail->quotation_server->product_disk6->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk7) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk7) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk7,
                            'name' => $quotation_detail->quotation_server->product_disk7->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( !empty($quotation_detail->quotation_server->addon_disk8) ) {
                        $check = true;
                        foreach ($data_config as $key => $data) {
                          if ($data['id'] == $quotation_detail->quotation_server->addon_disk8) {
                            $data_config[$key]['qtt'] = $data['qtt'] + 1;
                            $check = false;
                          }
                        }
                        if ( $check ) {
                          $data_config[] = [
                            'id' => $quotation_detail->quotation_server->addon_disk8,
                            'name' => $quotation_detail->quotation_server->product_disk8->name,
                            'qtt' => 1
                          ];
                        }
                      }
                      if ( count($data_config) ) {
                        $text_config .= '- Addon Disk: ';
                        foreach ($data_config as $key => $data) {
                          $text_config .= ( $data['qtt'] . ' &#215; ' . $data['name']  );
                          if ( count( $data_config ) - 1 > $key ) {
                            $text_config .= ' - ';
                          }
                        }
                        $text_config .= '';
                      }
                  @endphp
                  {{-- in ra --}}
                  &nbsp;&nbsp;- CPU: {{ $cpu }} ({{ $cores }}) <br>
                  &nbsp;&nbsp;- Memory: {{ $ram }} <br>
                  &nbsp;&nbsp;- Disk: {{ $disk }} <br>
                  @if ($addonRam)
                    &nbsp;&nbsp;- Addon RAM: {{ $addonRam }} GB RAM <br>
                  @endif
                  &nbsp;&nbsp;{!! $text_config !!}<br>
                  @if ($addonIp)
                    &nbsp;&nbsp;- Addon IP: {{ $addonIp }} IP Public <br>
                  @endif
                  Đã bao gồm chỗ đặt tại Datacenter
                  <br>
                @endif
                Thời gian thuê:
                {{ !empty($billings[$quotation_detail->billing_cycle]) ? $billings[$quotation_detail->billing_cycle] : '1 tháng' }}
                (từ ngày {{ date('d/m/Y', strtotime($quotation_detail->start_date)) }}
                đến ngày {{ date('d/m/Y', strtotime($quotation_detail->end_date)) }})
                <br>
                Hệ điều hành: {{ $quotation_detail->os }}
              </td>
              <td>
                @if ( $quotation_detail->type_product != 'domain' && $quotation_detail->type_product != 'design_webiste' )
                  {{ number_format( $quotation_detail->amount ,0,",",".") }}
                @endif
              </td>
              @php
                  $billing_meta = !empty($billingDashBoard[$quotation_detail->billing_cycle]) ? $billingDashBoard[$quotation_detail->billing_cycle] : 1;
                  $price_billing_cycle = !empty($billingDashBoard[$quotation_detail->price_billing_cycle]) ? $billingDashBoard[$quotation_detail->price_billing_cycle] : 1;
                  $total += $quotation_detail->amount * $quotation_detail->qtt * $billing_meta / $price_billing_cycle;
              @endphp
              <td  class="text-center">{{ $quotation_detail->qtt }}</td>
              <td  class="text-center">
                @if ( $quotation_detail->type_product != 'design_webiste' )
                  {{ !empty($billings[$quotation_detail->billing_cycle]) ? $billings[$quotation_detail->billing_cycle] : 1 }}
                @endif
              </td>
              @if( $quotation_detail->type_product != 'design_webiste' )
                <td class="text-center">{{ number_format( $quotation_detail->amount * $quotation_detail->qtt * $billing_meta  / $price_billing_cycle,0,",",".") }}</td>
              @else
                <td class="text-center">{{ number_format( $quotation_detail->amount * $quotation_detail->qtt  / $price_billing_cycle,0,",",".") }}</td>
              @endif
            </tr>
          @endforeach
          @if ( !empty($quotation->vat) )
            <tr>
              <td></td>
              <td>THUẾ VAT (10%)</td>
              <td colspan="3"></td>
              <td class="text-center">{{ number_format( $total/10 ,0,",",".") }}</td>
            </tr>
          @endif
          <tr>
            <td></td>
            <td><strong>TỔNG CHI PHÍ</strong></td>
            <td colspan="3"></td>
            <td class="text-center"><strong>{{ number_format( $quotation->total ,0,",",".") }}</strong></td>
          </tr>
        </tfoot>
      </table>
      <div id="text-amount">
        <i>(Bằng chữ: {{ $quotation->text_total }})</i>
      </div>
      <div id="notices">
        <div class="notice">
          Ghi chú: Chu kỳ thanh toán {{ $billings[$quotation->payment_cycle] }}/lần vào đầu mỗi kỳ.
        </div>
      </div>
      <div id="signature">
        <i class="location-date">
          Đà Nẵng,
          ngày {{ date( 'd', strtotime($quotation->created_at) ) }}
          tháng {{ date( 'm', strtotime($quotation->created_at) ) }}
          năm {{ date( 'Y', strtotime($quotation->created_at) ) }}
        </i> <br>
        <span class="position"><strong>GIÁM ĐỐC</strong></span> <br>
        <br> <br> <br> <br> <br> <br> <br> <br>
        <span class="position-name"><strong> NGUYỄN THỊ ÁI HOA </strong></span>
      </div>
    </main>
    {{-- <footer>
      Bảng báo giá dịch vụ của Cloudzone.
    </footer> --}}
  </body>
</html>
