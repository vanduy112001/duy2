<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="application/pdf">

    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <title>Cloudzone Portal Manager</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudzone">

    <link rel="stylesheet" href="{{ asset('libraries/test_pdf/style.css') }}" media="all" />

    <style type="text/css">
      html, body
        {
          /* font-family: DejaVu Sans !important; */
          /* font-size: 12pt !important; */
          /* font-family: DejaVu Sans ,sans-serif !important;  */
          font-family: "dejavu sans", serif;
        }
    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{ url('images/logo-bg-login.png') }}" alt="cloudzone.vn" width="120" height="55.5">
      </div>
      <div id="company">
        <span style="color: #3f3e3e;">Công ty TNHH MTV Công nghệ Đại Việt Số</span><br>
        <span style="color: #3f3e3e;">Địa chỉ: 257 Lê Duẩn, Đà Nẵng</span><br>
        <span style="color: #3f3e3e;">Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng</span> <br>
        <span style="color: #3f3e3e;">SĐT: 0236 4455 789</span> <span class="ml-3 mr-3">-</span> 
        <span style="color: #3f3e3e;">Website: <a href="https://cloudzone.vn">https://cloudzone.vn</a></span>
      </div>
    </header>
    <main>
      <div id="details" class="clearfix">
        <div id="client">
          <p class="quotation-info-title">
            BÁO GIÁ DỊCH VỤ THUÊ MÁY CHỦ ẢO CLOUD VPS
          </p>
        </div>
        <div id="invoice">
          <div class="quotation-info-user-info">
            <strong>Kính gởi: Quý khách hàng</strong> Lê Tấn Hiển <br>
            Chúng tôi, trân trọng gởi đến quý khách hàng báo giá dịch vụ Cloud VPS như sau:
          </div>
        </div>
      </div>
      <table class="table table-bordered">
        <thead class="bg-table-cyal">
          <tr>
            <th width="5%">STT</th>
            <th width="40%">Nội dung sản phẩm</th>
            <th width="12%">Đơn giá/tháng (VNĐ)</th>
            <th width="12%">Số lượng</th>
            <th width="12%">Thời gian (tháng)</th>
            <th width="19%">Thành tiền</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>
              <b>Dịch vụ máy chủ ảo VPS US</b> <br>
              Cấu hình chi tiết: 1 CORE - 1 RAM - 12 DISK <br>
              Thời gian thuê: 1 năm (từ ngày 01/01/2021 đến ngày 01/01/2022)
            </td>
            <td>120.000</td>
            <td>1</td>
            <td>1</td>
            <td>120.000</td>
          </tr>
          <tr>
            <td>2</td>
            <td>
              <b>Domain:</b> abc.com <br>
              Thời gian thuê: 1 năm (từ ngày 01/01/2021 đến ngày 01/01/2022)
            </td>
            <td>120.000</td>
            <td>1</td>
            <td>1</td>
            <td>120.000</td>
          </tr>
          <tr>
            <td></td>
            <td>THUẾ VAT (10%)</td>
            <td colspan="3"></td>
            <td>24.000</td>
          </tr>
          <tr>
            <td></td>
            <td><b>TỔNG CHI PHÍ</b></td>
            <td colspan="3"></td>
            <td>240.000</td>
          </tr>
        </tfoot>
      </table>
      <div id="text-amount">
        <i>(Bằng chữ: Hai trăm bốn mươi ngàn đồng y)</i>
      </div>
      <div id="notices">
        <div class="notice">
          Ghi chú: Chu kỳ thanh toán 03 tháng/lần vào đầu mỗi kỳ.
        </div>
      </div>
      <div id="signature">
        <i class="location-date"> 
          Đà Nẵng,
          ngày 01
          tháng 01
          năm 2020
        </i> <br>
        <span class="position"><strong>GIÁM ĐỐC</strong></span> <br>
        <img class="position-img" src="{{ asset('images/chuky.png') }}" alt="chữ ký giám đốc"> <br>
        <span class="position-name"><strong> NGUYỄN THỊ ÁI HOA </strong></span>
      </div>
    </main>
    {{-- <footer>
      Bảng báo giá dịch vụ của Cloudzone.
    </footer> --}}
  </body>
</html>