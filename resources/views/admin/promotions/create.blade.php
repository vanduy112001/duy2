@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/lou-multi-select/css/multi-select.css') }}" />
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/vps/">Server</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Server</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Tạo mã khuyến mãi</h3>
      <!-- /.card-tools -->
    </div>
    <form action="{{ route('admin.promotion.store') }}" method="post">
        @csrf
        <div class="card-body">
              <div class="form-group">
                  <label for="name">Tiêu đề</label>
                  <input type="text" name="name" class="form-control" placeholder="Tiêu đề" value="{{ old('name') }}">
              </div>
              <div class="form-group ">
                  <label for="code">Mã khuyến mãi</label>
                  <div class="row">
                    <div class="col-9">
                      <input type="text" name="code" id="code" class="form-control" placeholder="Mã khuyến mãi" value="{{ old('code') }}">
                    </div>
                    <div class="col-3">
                      <button type="button" class="btn btn-success btn_auto_promotion">Tạo mã khuyến mãi</button>
                    </div>
                  </div>
              </div>
              <div class="form-group ">
                  <label for="type">Loại mã khuyến mãi</label>
                  <select class="form-control" name="type">
                      @foreach ( $type_promotion as $key => $type )
                        <?php
                            $selected = '';
                            if ($key == old('type')) {
                              $selected = 'selected';
                            }
                        ?>
                        <option value="{{ $key }}" {{ $selected }}>{{ $type }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="value">Giá trị</label>
                  <input type="text" name="value" class="form-control" placeholder="Giá trị" value="{{ old('value') }}">
              </div>
              <div class="form-group">
                  <label for="product">Sản phẩm</label>
                  <select class="select2" name="product[]" multiple="multiple" data-placeholder="Chọn sản phẩm" style="width: 100%;">
                      @foreach ( $group_products as $group_product )
                        <optgroup label="{{ $group_product->name }}">
                            @foreach ( $group_product->products as $product )
                              <option value="{{ $product->id }}">{{ $product->name }}</option>
                            @endforeach
                        </optgroup>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="billing">Thời gian</label>
                  <select class="form-control" name="billing[]" multiple="multiple" size="10" data-placeholder="Chọn thời gian" style="width: 100%;height: 100%;">
                    @foreach ( $billings as $key => $billing )
                      <option value="{{ $key }}">{{ $billing }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="start_date">Ngày bắt đầu</label>
                  <input type="date" name="start_date" class="form-control" placeholder="Ngày bắt đầu" value="{{ old('start_date') }}">
              </div>
              <div class="form-group">
                  <label for="end_date">Ngày kết thúc</label>
                  <input type="date" name="end_date" class="form-control" placeholder="Ngày kết thúc" value="{{ old('end_date') }}">
              </div>
              <div class="form-group">
                  <label for="max_uses">Số lượng sử dụng tối đa</label>
                  <input type="text" name="max_uses" class="form-control" placeholder="Số lượng sử dụng tối đa" value="{{ old('max_uses') }}">
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="life_time">Lifetime Promotion</label>
                 </div>
                 <div class="col-md-10">
                    <input type="checkbox" name="life_time" value="1">
                    <span>Giá khuyến mãi được áp dụng ngay cả khi nâng cấp trong tương lai, tạo mới , gia hạn, v.v;</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="life_time">Apply Once</label>
                 </div>
                 <div class="col-md-10">
                    <input type="checkbox" name="life_time" value="1">
                    <span>Chỉ áp dụng mã khuyến mãi cho mỗi khách hàng một lần</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="upgrade">Nâng cấp</label>
                 </div>
                 <div class="col-md-10">
                    <input type="checkbox" name="upgrade" value="1">
                    <span>Áp dụng mã khuyến mãi cho cả đơn hàng nâng cấp</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="type_order">Loại đơn hàng</label>
                 </div>
                 <div class="col-md-10">
                    <select class="form-control" name="type_order">
                      <option value="order">Order</option>
                      <option value="expire">Gia hạn</option>
                    </select>
                 </div>
              </div>
        </div>
        <div class="card-footer">
          <div class="text-right">
            <input type="submit" value="Tạo mã khuyến mãi" class="btn btn-primary">
          </div>
        </div>
    </form>
</div>
<input type="hidden" id="page" value="create_promotion">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/lou-multi-select/js/jquery.multi-select.js') }}" defer></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.select2').select2();
    $('.btn_auto_promotion').on('click', function () {
        var length = 12;
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
          retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#code').val(retVal);
    })
  })
</script>
@endsection
