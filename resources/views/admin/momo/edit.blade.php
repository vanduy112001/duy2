@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
Chỉnh sửa lại giao dịch MoMo và xác nhận lại thanh toán
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.momo.index') }}"><i class="fa fa-credit-card" aria-hidden="true"></i> MoMo</a></li>
<li class="breadcrumb-item active"></i> Chỉnh sửa</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Chỉnh sửa thông tin chuyển khoản MoMo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.momo.update') }}" id="form_select">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="ma_gd">Mã giao dịch MoMo</label>
                    <input type="text" name="id_momo" class="form-control" value="{{ $momo->id_momo }}" disabled="">
                </div>
                <div class="form-group">
                    <label for="number_phone">Số điện thoại</label>
                    <input type="text" name="number_phone" class="form-control"  value="{{ $momo->number_phone }}" disabled="">
                </div>
                <div class="form-group">
                    <label for="amount">Số tiền</label>
                    <input type="text" id="amount" class="form-control" name="amount" value="{{ $momo->amount }}">
                </div>
                <div class="form-group">
                    <label for="content">Nội dung</label>
                    <textarea name="content" class="form-control">{{ $momo->content }}</textarea>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <input type="hidden" name="number_phone" class="form-control"  value="{{ $momo->number_phone }}">
                <input type="hidden" name="id_momo" class="form-control" value="{{ $momo->id_momo }}">
                <a class="btn btn-default" href="{{ route('admin.momo.index') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Xác nhận</button>
            </div>
        </form>
    </div>
</div>
    <input type="hidden" id="page" value="edit_momo">
@endsection
@section('scripts')
    <script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });
            // select
            $('.select2').select2();
      });
    </script>
@endsection
