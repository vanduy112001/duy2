@extends('layouts.app')
@section('title')
<i class="nav-icon fas fa-cubes"></i> Quản lý thông tin đại lý
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Đại lý</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <button class="btn btn-primary" id="createGroup">Tạo đại lý</button>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-hover table-bordered">
                <thead class="primary">
                    <th>Id</th>
                    <th>Tên đại lý</th>
                    <th>Số lượng user</th>
                    <th>Số lượng nhóm SP</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>

{{-- Modal --}}
<div class="modal fade" id="modal-create">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tạo nhóm sản phẩm</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication">
            </div>
            <form id="form-group-product">
                @csrf
                <div class="form-group">
                    <label for="name">Tên đại lý</label>
                    <input type="text" id="name" name="name" class="form-control" placeholder="Tên đại lý">
                </div>
                <div class="hidden">
                    <input type="text" id="action" name="action" value="create">
                    <input type="text" id="id-group" name="id" value="">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" name="button" class="btn btn-primary" id="button-create" title="Tạo nhóm sản phẩm">Tạo nhóm sản phẩm</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="modal-delete">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-delete">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="button" name="button" class="btn btn-primary" id="button-delete">Xác nhận</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" id="button-finish">Hoàn thành</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_group_user">
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/group_user.js') }}"></script>
@endsection
