@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary" id="update_sevice_hosting">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <div class="text-center mt-4 mb-4">
                <h3>Thêm Hosting ở DAPortal vào Portal</h3>
            </div>
            <div class="container">
                <form action="{{ route('admin.hosting.setHostingWithPortal') }}" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Tên khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select id="select_user" name="user_id" class="form-control select2">
                                                <option value="" disabled selected>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                    @php
                                                        $selected = '';
                                                        if ($user->id == old('user_id')) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$user->id}}" {{ $selected }}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8" id="product">
                                            <select disabled name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Domain:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" class="form-control" name="domain" value="{{ $hosting['domain'] }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Username:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_username" value="{{ $hosting['username'] }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_password" value="" disabled>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                <option value="Active" selected>Đã tạo</option>
                                                <option value="Pending">Chưa tạo</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Package :</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-danger">{{ $hosting['package'] }} <br></p>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày tạo :</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-danger">Chưa tạo</p>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                            <select class="select2" name="billing_cycle" style="width:100%;">
                                                <option value="" disabled selected>Chọn thời gian thuê</option>
                                                @foreach($billings as $key => $billing)
                                                  @php
                                                    $selected = '';
                                                    if($key == old('billing_cycle')) {
                                                        $selected = 'selected';
                                                    }
                                                  @endphp
                                                  <option value="{{ $key }}" {{ $selected }}>{{ $billing }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice form-group">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            <input class="form-control" id="datepicker" name="next_due_date" placeholder="Nhập ngày hết hạn" value="{{ old('next_due_date') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Giá áp tay :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="price_override" id="price_override" value="1">
                                            <label for="price_override">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col-md-4 text-right" style="padding-top: 5px;">
                                          <label for="">GH theo chu kỳ :</label>
                                      </div>
                                      <div class="col-md-8 ml-2 icheck-primary d-inline">
                                          <input type="checkbox" class="custom-control-input" name="expire_billing_cycle" id="expire_billing_cycle" @if(!empty($detail->expire_billing_cycle)) checked @endif  value="1">
                                          <label for="expire_billing_cycle">
                                          </label>
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text"  name="total" id="total" value="{{ old('total') }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Trừ tiền tài khoản :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="sub_value" id="sub_value" value="1">
                                            <label for="sub_value">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="form-button mt-4">
                                        @csrf
                                        <input type="hidden" name="location" value="vn">
                                        <input type="hidden" name="server_hosting_id" value="{{ $id }}">
                                        <input type="submit" value="Cập nhật Hosting" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa nhiều hosting --}}
<div class="modal fade" id="delete-multiple-hosting">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-hostings" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit-multi-hosting">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="add_hosting_for_portal">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
    $('#datepicker').datepicker({
			autoclose: true
		});
        // select
    $('.select2').select2();

    $('#price_override').on('click', function () {
        if ( $(this).is(':checked') ) {
          $('#total').attr('disabled', false);
        } else {
          $('#total').attr('disabled', true);
        }
    })

    $('#select_user').on('change', function () {
        var user_id = $(this).val();
        // console.log('da doi');
        $.ajax({
            type: "get",
            url: "/admin/orders/list_product",
            data: {action: 'Hosting', user_id: user_id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#product').html(html);
            },
            success: function (data) {
                var html = '';
                // 1 form-group
                html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
                html += '<option disabled selected>Chọn sản phẩm</option>';
                if (data.product != null) {
                    $.each(data.product, function (index, product) {
                        html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    });
                } else {
                    html += '<option disabled class="text-danger">Không có sản phẩm VPS</option>';
                }
                html += '</select>';
                $('#product').html(html);
                $('.select2').select2();
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                $('#product').html(html);
            }
        });
    })

	});
</script>
@endsection
