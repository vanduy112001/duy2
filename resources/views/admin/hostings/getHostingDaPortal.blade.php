@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Hosting Việt Nam
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered" id="table_da">
                <thead class="primary">
                    <th>Domain</th>
                    <th>Người tạo</th>
                    <th>Package</th>
                    <th>Suspend</th>
                    <th>User</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                </tbody>
                <tfoot class="card-footer clearfix">
                </tfoot>
            </table>
        </div>
    </div>
</div>
{{-- Modal xoa nhiều hosting --}}
<input type="hidden" name="id" id="server_hosting_id" value="{{ $id }}">
<div class="modal fade" id="delete-multiple-hosting">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-hostings" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit-multi-hosting">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="get_hosting_for_daportal">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}" defer></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}" defer></script>
<script src="{{ asset('js/admin_hosting_da.js') }}"></script>
@endsection
