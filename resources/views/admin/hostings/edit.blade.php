@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý sản phẩm / dịch vụ Hosting
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/hostings">Quản lý Hosting</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary" id="update_sevice_hosting">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <div class="text-center mt-4 mb-4">
                <h3>Thông tin Hosting {{$detail->domain}} @if($detail->location == 'vn') - Việt Nam @elseif($detail->location == 'si') - Singapore @endif</h3>
            </div>
            <div class="container">
                <form action="{{ route('admin.hosting.update_sevices') }}" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Tên khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                    @php
                                                        $selected = '';
                                                        if ($user->id == $detail->user_id) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                                @foreach ($products as $product)
                                                    @php
                                                        $selected = '';
                                                        if ($product->id == $detail->product_id) {
                                                                $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$product->id}}" {{$selected}}>{{$product->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Domain:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" class="form-control" name="domain" value="{{$detail->domain}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Username:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_username" value="{{$detail->user}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_password" value="{{ !empty($detail->password) ? $detail->password : '' }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                @if ($detail->status == 'Active')
                                                    <option value="Active" selected>Đã tạo</option>
                                                    <option value="Pending">Chưa tạo</option>
                                                    <option value="Cancel">Hủy</option>
                                                @elseif ($detail->status == 'Pending')
                                                    <option value="Active" >Đã tạo</option>
                                                    <option value="Pending" selected>Chưa tạo</option>
                                                    <option value="Cancel">Hủy</option>
                                                @else
                                                    <option value="Active" >Đã tạo</option>
                                                    <option value="Pending" >Chưa tạo</option>
                                                    <option value="Cancel" selected>Hủy</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-success">{{date('d-m-Y', strtotime($detail->created_at))}}</p>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày tạo :</label>
                                        </div>
                                        <div class="col-md-8" id="date_create">
                                            {{-- <input type="text" id="datepicker" name="created_at" value="{{date('d-m-Y', strtotime($detail_order->created_at))}}" class="form-control"> --}}
                                            @if (!empty($detail->date_create))
                                                {{date('d-m-Y', strtotime($detail->date_create))}}
                                                <span class="ml-4">
                                                   <a href="#" class="btn btn-outline-default date_create" data-date="{{date('d-m-Y', strtotime($detail->date_create))}}"><i class="fas fa-edit"></i></a>
                                                </span>
                                            @else
                                                <p class="text-danger">Chưa tạo</p>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            @if (!empty($detail->next_due_date))
                                                {{date('d-m-Y', strtotime($detail->next_due_date))}}
                                                <span class="ml-4">
                                                   <a href="#" class="btn btn-outline-default next_due_date" data-date="{{date('d-m-Y', strtotime($detail->next_due_date))}}"><i class="fas fa-edit"></i></a>
                                                </span>
                                            @else
                                                <p class="text-danger">Chưa tạo</p>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col-md-8" id="paid_date">
                                            @if(!empty($detail->paid))
                                                @if($detail->paid == 'paid')
                                                    <span class="text-success">{{date('d-m-Y', strtotime($detail->date_create))}}</span>
                                                    <span class="ml-4">
                                                       <a href="#" class="btn btn-outline-default paid_date" data-date="{{date('d-m-Y', strtotime($detail->date_create))}}"><i class="fas fa-edit"></i></a>
                                                    </span>
                                                @elseif($detail->paid == 'cancel')
                                                    <span class="text-secondary">Hủy</span>
                                                @else
                                                    <span class="text-danger">Chưa thanh toán</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa thanh toán</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                            {{ $billings[$detail->billing_cycle] }}
                                            <span class="ml-4">
                                               <a href="#" class="btn btn-outline-default billing_cycle" data-name="{{$detail->billing_cycle}}"><i class="fas fa-edit"></i></a>
                                            </span>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Giá áp tay :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="price_override" id="price_override" value="1" @if(!empty($detail->price_override)) checked @endif>
                                            <label for="price_override">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col-md-4 text-right" style="padding-top: 5px;">
                                          <label for="">GH theo chu kỳ :</label>
                                      </div>
                                      <div class="col-md-8 ml-2 icheck-primary d-inline">
                                          <input type="checkbox" class="custom-control-input" name="expire_billing_cycle" id="expire_billing_cycle" @if(!empty($detail->expire_billing_cycle)) checked @endif  value="1">
                                          <label for="expire_billing_cycle">
                                          </label>
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8" id="sub_total">
                                            @if(!empty($detail->price_override))
                                              <input type="hidden" name="sub_total" value="{{ $detail->price_override }}">
                                              <span>
                                                <b>{!!number_format( $detail->price_override ,0,",",".")!!} VNĐ</b>
                                              </span>
                                              <span class="pl-2 text-danger">
                                                - giá ghi đè
                                              </span>
                                              <span class="ml-4">
                                                 <a href="#" class="btn btn-outline-default sub_total" data-price="{{$detail->price_override}}"><i class="fas fa-edit"></i></a>
                                              </span>
                                            @else
                                              <input type="hidden" name="sub_total" value="{{ $detail->detail_order->sub_total }}">
                                              <span>
                                                <b>{!!number_format(($detail->detail_order->sub_total),0,",",".")!!} VNĐ</b>
                                              </span>
                                              <span class="ml-4">
                                                 <a href="#" class="btn btn-outline-default sub_total" data-price="{{$detail->detail_order->sub_total}}"><i class="fas fa-edit"></i></a>
                                              </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-button mt-4">
                                        @csrf
                                        <input type="hidden" id="hosting_id" name="id" value="{{ $detail->id }}">
                                        <input type="hidden" name="type" value="hosting">
                                        <input type="hidden" name="services_location" value="@if($detail->location == 'vn') vn @elseif($detail->location == 'si') si @endif">
                                        <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                                        <input type="submit" value="Cập nhật Hosting" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-4 mb-4">
                            <a href="" class="btn btn-outline-success @if($detail->status == 'Active') disabled @endif button-action-hosting" data-type="create">Tạo Hosting</a>
                            <a href="" class="btn btn-outline-warning button-action-hosting @if($detail->status_hosting == 'off') disabled @endif" data-type="suspend">Suspend Hosting</a>
                            <a href="" class="btn btn-outline-info button-action-hosting @if($detail->status_hosting == 'on') disabled @endif" data-type="unsuspend">Unsuspend Hosting</a>
                            <a href="" class="btn btn-danger button-action-hosting" data-type="delete">Xóa Hosting</a>
                            <a href="" class="btn btn-success @if($detail->paid == 'paid') disabled @endif button-action-hosting">Đã thanh toán</a>
                            <a href="" class="btn btn-danger @if($detail->paid == 'unpaind' || empty($detail->paid)) disabled @endif button-action-hosting">Chưa thanh toán</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xác nhận">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="edit_hosting">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}" defer></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/admin_edit_hosting.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true
		});
        // select
        $('.select2').select2();
	});
</script>
@endsection
