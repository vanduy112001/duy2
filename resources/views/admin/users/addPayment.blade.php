@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
Tạo nạp tiền vào tài khoản
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i> List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Tạo nạp tiền vào tài khoản</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Nhập thông tin người dùng</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.user.addPayment') }}" id="form_select">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="user_id">Tên khách hàng</label>
                    <select class="form-control select2" name="user_id" style="width:100%;" id="user_id">
                        <option value="" selected disabled>Chọn khách hàng</option>
                        @foreach($users as $user)
                            <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="amount">Số tiền</label>
                    <input type="text" id="amount" class="form-control" name="amount" value="{{ old('amount') }}">
                </div>
                <div class="form-group">
                    <label for="method_gd">Hình thức thanh toán</label>
                    <select class="form-control" name="method_pay" id="method_pay">
                        <option value="" selected disabled>Chọn hình thức thanh toán</option>
                        <option value="pay_in_office">Trực tiếp tại văn phòng</option>
                        <option value="bank_transfer_vietcombank">Chuyển khoản ngân hàng Vietcombank</option>
                        <option value="bank_transfer_techcombank">Chuyển khoản ngân hàng Techcombank</option>
                        <option value="momo">Thanh toán qua MOMO</option>
                    </select>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.user.list') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Tạo</button>
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="add_payment_user">
@endsection
@section('scripts')
    <script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });
            // select
            $('.select2').select2();
      });
    </script>
@endsection
