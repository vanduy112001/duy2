@extends('layouts.app')
@section('title')
Danh sách người dùng
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> List users</li>
@endsection
@section('content')
<div class="row">
	<div data-v-a16ce4fa="" class="col-md-12">
		<a class="btn btn-primary" href="{{ route('admin.user.create') }}">Tạo user</a>
<!-- 		<a class="btn btn-success" href="{{ route('admin.user.list_user_personal') }}">KH cá nhân</a>
		<a class="btn btn-danger" href="{{ route('admin.user.list_user_enterprise') }}">KH doanh nghiệp</a> -->
	</div>
</div>

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<span class="text-light">{{session("success")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
	<div class="card-header">
		<div class="card-tools float-left">
			<div class="input-group input-group-sm" style="width: 180px;">
				<button type="submit" id="check_delete" class="btn btn-sm btn-default check_delete"><i
					class="fa fa-trash" aria-hidden="true"></i> Xóa
				mục đã chọn</button>
			</div>
		</div>
		{{-- <div class="card-tools">
			<form method="get">
				<div class="input-group input-group-sm" style="width: 200px;">
					<input type="search" name="keyword" class="form-control float-right"
					placeholder="Nhập tên hoặc email"
					value="{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}">
					<div class="input-group-append">
						<button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
					</div>
				</div>
			</form>
		</div> --}}
	</div>
	<!-- /.card-header -->
	<div class="card-body table-responsive">
		<table id="data-user-table" class="table table-sm table-bordered table-hover rounded">
			<thead>
				<tr>
					<!-- <th>
						<div>
							<input class="check_all" id="check_all" type="checkbox" value="1">
						</div>
					</th> -->
					<th>ID</th>
					<th>Tên</th>
					<th>Email</th>
					<th>Loại</th>
					<th>Phone</th>
					<th>Đại lý</th>
					<th>SDTK</th>
					<th>Tổng nạp</th>
					<th>Role</th>
					<th>Nguồn</th>
					<th>Ngày tạo</th>
					<th>Action</th>
				</tr>
			</thead>

		</table>
	</div>
	<!-- /.card-body -->
	{{-- <div class="card-footer clearfix">
		<ul class="pagination pagination-sm m-0 float-right">
			<div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
				{{ $users->links() }}
			</div>
		</ul>
	</div> --}}
	<!-- /.card-footer -->
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
		  <button type="button" class="btn btn-primary" id="button-vps">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-vps-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection

@section('scripts')
<!-- DataTables -->
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
	jQuery(document).ready(function($) {

		$(document).on('click', '.btn-change-social', function () {
			$(this).closest('tr').removeClass('action');
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			$('#delete-vps').modal("show");
			$('.modal-title').text('Đổi nguồn tạo tài khoản');
			$('#notication-invoice').html('<span class="">Đổi nguồn tạo của tài khoản <b class="text-danger">'+ name +'</b> qua nguồn tạo Email</span>');
			$('#button-vps').fadeIn();
			$('#button-vps').attr('data-id', id);
			$('#button-vps-finish').fadeOut();
			$(this).closest('tr').addClass('action');
		})

		$('#button-vps').on('click', function () {
			var id = $('#button-vps').attr('data-id');
			$.ajax({
				url: "/admin/users/change_social/" + id,
				dataType: "json",
				beforeSend: function(){
					var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
					$('#notication-invoice').html(html);
				},
				success: function (response) {
					if ( !response.error ) {
						// social-user
						$('.action .social-user').text('Email');
						$('#notication-invoice').html('<span class="text-success">Đổi nguồn tạo tài khoản thành công.</span>');
						$(this).closest('tr').removeClass('action');
						$('#button-vps').fadeOut();
						$('#button-vps-finish').fadeIn();
					} else {
						$('#notication-invoice').html('<span class="text-danger">Đổi nguồn tạo tài khoản thất bại.</span>');
						$(this).closest('tr').removeClass('action');
					}
				},
				error: function (e) {
					console.log(e);
					$('#notication-invoice').html('<span class="text-danger">Lỗi truy vấn.</span>');
					$(this).closest('tr').removeClass('action');
				}
			});		
		})

		$("#data-user-table").DataTable({
			processing: true,
			serverSide: true,
			"pageLength": 25, //Mặc định hiển thị 25 dòng / trang
			responsive:true,
			"columnDefs": [
                 { "targets": [ 3, 4, 5, 8, 9 , 11 ], "orderable": false }
            ],
            order:[[0,'desc']], //Mặc định sắp xếp bảng bảng theo ID giảm dần, (0 là cột ID)
			ajax: {
				url: '{!! route('admin.user.list') !!}',
				dataType: 'json',
				type: 'GET',
			},

			//Tạo các cột để datatable render các giá trị mà ajax trả về vào table
			columns: [
				{ data: "id", name: "id" },
				{ data: "name", name: "name" },
				{ data: "email", name: "email" },
				{ data: "user_type", name: "user_type" },
				{ data: "phone", name: "phone" },
				{ data: "group_user", name: "group_user" },
				{ data: "credit_value", name: "credit_value", 
					render:function ( data, type, row, meta ) {
               			if (data == null){
                   			return 0;
               			} else {
                   			return $.fn.dataTable.render.number('.', ',', 0).display(data);
               			}
           			}
           		},
				{ data: "credit_total", name: "credit_total",
					render:function ( data, type, row, meta ) {
               			if (data == null){
                   			return 0;
               			} else {
                   			return $.fn.dataTable.render.number('.', ',', 0).display(data);
               			}
           			}
           		},
				{ data: "role", name: "role" },
				{ data: "source_reg_acc", name: "source_reg_acc" },
				{ data: "created_at", name: "created_at"},
				{ data: "action", name: "action" }
			],

			//Tạo tùy chọn nhóm người dùng ở cột thứ 4, 6 render trong thẻ <thead>
			initComplete: function () {
	            this.api().columns([3]).every( function () {
	                var column = this;
                    var user_type_group = '';
                    user_type_group += '<select class="rounded py-1">';
                    user_type_group += '<option value="">Khách hàng</option>';
                    user_type_group += '<option value="enterprise">Doanh nghiệp</option>';
                    user_type_group += '<option value="individual">Cá nhân</option>';
                    user_type_group += '</select>';
	                var select = $(user_type_group)
	                    .appendTo( $(column.header()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	 
	                        column
	                            .search( val ? val : '', true, false )
	                            .draw();
	                    } );
	 
	                // column.data().unique().sort().each( function ( d, j ) {
	                //     select.append( '<option value="'+d+'">'+d+'</option>' )
	                // } );
	            });
	            this.api().columns([5]).every( function () {
	                var column = this;
                    var select_group_user = '';
                    select_group_user += '<select class="rounded py-1" name="group_user">';
                    select_group_user += '<option value="">Chọn đại lý</option>';
                    select_group_user += '<option value="normal">Cơ bản</option>';
                    select_group_user += '<option value="2">Nhóm đại lý cấp 1</option>';
                    select_group_user += '<option value="1">Nhóm đại lý cấp 2</option>';
                    select_group_user += '<option value="3">Nhóm đại lý cấp 3</option>';
                    select_group_user += '<option value="4">Nhóm Cộng tác viên</option>';
                    select_group_user += '<option value="5">Testing</option>';
                    select_group_user += '</select>';
	                var select = $(select_group_user)
	                    .appendTo( $(column.header()).empty() )
	                    .on( 'change', function () {
	                        var val = $.fn.dataTable.util.escapeRegex(
	                            $(this).val()
	                        );
	                        column
	                            .search( val ? val : '', true, false )
	                            .draw();
	                    } );
	            } );
	        },

			//Thêm thuộc tính data-id vào các row <tr>
			"createdRow": function( row, data, dataIndex ) {
				$(row).attr('data-id', data.id);
			},

			//Việt hóa
			language: {
		        processing: '<span class="spinner-border text-primary spinner-border-sm fa-3x fa-fw"></span> Đang tải dữ liệu ...',
		        search: "Tìm kiếm: ",
		        searchPlaceholder: "id, tên, email, sđt",
		        lengthMenu: "Chọn số lượng / trang _MENU_ ",
		        info: "Từ _START_ đến _END_ (Tổng cộng: _TOTAL_)",
		        infoEmpty: "Không có dữ liệu",
		        loadingRecords: "",
		        emptyTable: "Không có dữ liệu",
		        paginate: {
		            first: "Đầu",
		            previous: "Trước",
		            next: "Sau",
		            last: "Cuối"
		        }
		    },
		});

		$(document).on('click', '.delete-user', function (e) {
			var r = confirm("Bạn muốn xóa người dùng này?");
			if (r == true) {
				var obj = $(this);
				var id = obj.attr('data-id');

				$.ajax({
					type: "post",
					url: "{{ route('admin.user.delete') }}",
					dataType: 'json',
					data: {
						"_token": "{{ csrf_token() }}",
						"id": id
					},
					success: function (data) {
						alert(data.message);
						if (data.status) {
							obj.closest('tr').remove();
						}
					}, error: function () {
						alert(data.message);
					}
				});
			}
		});

		$(document).on( 'click', '#data-user-table tbody tr', function () {
			$(this).toggleClass('bg-info selected');
			var select = $('#data-user-table tbody tr.selected');
			if (select.length > 0) {
				$("#check_delete").addClass('btn-danger').removeClass('btn-default');
			} else {
				$("#check_delete").addClass('btn-default').removeClass('btn-danger');
			}
		});

	    //Gửi ajax lên serverver để xóa các row được chọn
	    $('.check_delete').on('click', function () {
	    	var r = confirm("Bạn muốn xóa các mục đã chọn?");
	    	if (r == true) {
	    		var select = $('#data-user-table .selected');
	    		var array = [];
	    		if (select.length > 0) {
	    			select.each(function()
	    			{
	    				array.push($(this).attr("data-id"));
	    			});
	    			if ( array != '') {
	    				$.ajax({
			    			type: "post",
			    			url: "{{ route('admin.user.multidelete') }}",
			    			dataType: 'json',
			    			data: {
			    				"_token": "{{ csrf_token() }}",
			    				"id": array
			    			},
			    			success: function (data) {
			    				alert(data.message);
			    				location.reload();
			    			},
			    			error: function () {
			    				alert(data.message);
			    			}
			    		});
	    			} else {
	    				alert('Không có dữ liệu để xóa');
	    			}
	    		} else {
	    			alert('Chưa chọn mục cần xóa');
	    		}
	    	}
	    });

	});
	// $(function () {
        // $("#data-user-table").DataTable({
        //     "columnDefs": [
        //          { "targets": [ 0, 3, 4, 5, 9 , 12 ], "orderable": false }
        //     ]
        // });

        // 0, 3, 4, 6, 7, 8, 11
    //   $('#data-user-table').DataTable({
    //     "paging": true,
    //     "lengthChange": false,
    //     "searching": false,
    //     "ordering": true,
    //     "info": true,
    //     "autoWidth": false,
    //   });
// });

</script>
@endSection