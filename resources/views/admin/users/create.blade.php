@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
Tạo người dùng
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i> List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Create users</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Nhập thông tin người dùng</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.user.store') }}" autocomplete="off">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Tên</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Tên"
                        value="{{ old('name') }}">
                </div>
                <div class="form-group">
                    <label for="gender">Giới tính</label>
                    <select class="form-control" name="gender">
                        <option value="nam" selected="">Nam</option>
                        <option value="nữ">Nữ</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="address">Địa chỉ</label>
                    <input type="text" name="address" class="form-control" id="address" placeholder="Địa chỉ" value="{{ old('address') }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                        value="{{ old('email') }}">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone" value="{{ old('phone') }}">
                </div>
                <div class="form-group">
                    <label for="company">Công ty / Tổ chức</label>
                    <input type="text" name="company" class="form-control" id="company" placeholder="Công ty / Tổ chức" value="{{ old('company') }}">
                </div>
                <div class="form-group">
                    <label for="abbreviation_name">Tên viết tắt</label>
                    <input type="text" name="abbreviation_name" class="form-control" id="abbreviation_name" placeholder="Tên viết tắt (cá nhân, công ty, tổ chức)" value="{{ old('abbreviation_name') }}">
                </div>
                <div class="form-group">
                    <label>Nhóm đại lý</label>
                    <select class="form-control" name="group_user_id">
                        <option value="0">Cơ bản</option>
                        @foreach($group_users as $group_user)
                          @php
                              $selected = '';
                              if( $group_user->id == old('group_user_id') ) {
                                  $selected = 'selected';
                              }
                          @endphp
                          <option value="{{ $group_user->id }}" {{$selected}}>{{ $group_user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <label for="enterprise">Doanh nghiệp</label>
                  <div class="ml-4 icheck-primary d-inline">
                      <input type="checkbox" class="custom-control-input" name="enterprise" id="enterprise" @if( old('enterprise') ) checked @endif  value="1">
                      <label for="enterprise">
                      </label>
                  </div>
                </div>
                <div class="form-group" id="mst">
                   @if( !empty( old('enterprise') ) )
                      <label for="imst">Mã số thuế</label>
                      <input type="text" name="mst" value="{{ old('mst') }}" class="form-control" placeholder="Mã số thuế">
                   @endif
                </div>
                <div class="form-group">
                    <label for="password">Mật khẩu</label>
                    <div class=" input-group">
                      <input type="password" name="password" class="form-control" id="password" placeholder="Password"
                          value="{{ old('password') }}">
                      <div class="input-group-append show_password">
                        <span class="input-group-text">
                          <i class="fas fa-eye"></i>
                        </span>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password_confirmation">Nhập lại mật khẩu</label>
                    <input type="password" name="password_confirmation" class="form-control" id="password_confirmation"
                        placeholder="Nhập lại mật khẩu" value="{{ old('password_confirmation') }}">
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.user.list') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Tạo</button>
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="create_user">
@endsection
@section('scripts')
<script type="text/javascript">
  $(document).ready(function () {
    generatePassword();
    function generatePassword() {
      var length = 12;
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      retVal = "";
      for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
      }
      $('#password_confirmation').val(retVal);
      $('#password').val(retVal);
    }
  })

  $(document).on('click', '.show_password', function () {
      $('#password').attr('type', 'text');
      $('.show_password i').removeClass('fa-eye');
      $('.show_password i').addClass('fa-eye-slash');
      $(this).addClass('hidden_password');
      $(this).removeClass('show_password');
  })

  $(document).on('click', '.hidden_password', function () {
      $('#password').attr('type', 'password');
      $('.hidden_password i').addClass('fa-eye');
      $('.hidden_password i').removeClass('fa-eye-slash');
      $(this).removeClass('hidden_password');
      $(this).addClass('show_password');
  })

  $('#enterprise').on('click', function () {
      if ( $(this).is(':checked') ) {
        var html = '';
        html += '<label for="imst">Mã số thuế</label>';
        html += '<input type="text" name="mst" value="" class="form-control" placeholder="Mã số thuế">';
        $('#mst').html(html);
      } else {
        $('#mst').html('');
      }
  })

</script>
@endsection
