@extends('layouts.app')
@section('style')
<link href="{{ asset('/libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Danh sách báo giá {{ $user->name }}
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Danh sách báo giá</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-6">
                <a href="{{ route('admin.quotation.create_by_user', $id) }}" class="btn btn-primary">Tạo đơn báo giá</a>
            </div>
            <!-- form search -->
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("error"))
                    <div class="bg-danger">
                        <p class="text-light">{!! session("error") !!}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="table_order" class="table table-hover table-bordered">
                <thead class="primary">
                    <th>Tiêu đề</th>
                    <th>Ngày tạo</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($list_quotation as $quotation)
                        <tr>
                            <td>{{ $quotation->title }}</td>
                            <td>{{ date( 'd-m-Y H:i:s', strtotime($quotation->created_at) ) }}</td>
                            <td>
                                @if ($quotation->link)
                                    <a href="{{ $quotation->link }}" class="btn-sm btn btn-info" download data-toggle="tooltip" title="Tải báo giá"><i class="fas fa-download"></i></a>
                                @endif
                                <a href="/admin/users/xem-truoc-bao-gia/{{ $quotation->id }}" class="btn-sm btn btn-success" data-toggle="tooltip" title="Xem trước báo giá"><i class="fas fa-chart-bar"></i></a>
                                <a href="/admin/users/chinh-sua-bao-gia/{{ $quotation->id }}" class="btn-sm btn btn-warning text-white" data-toggle="tooltip" title="Chỉnh sửa báo giá"><i class="fas fa-edit"></i></a>
                                <button class="btn btn-danger delete-order" data-id="{{ $quotation->id }}"><i class="fas fa-trash-alt" data-toggle="tooltip" title="Xóa báo giá"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="7" class="text-center">
                        {{ $list_quotation->links() }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="user_id" value="{{ $id }}">
<input type="hidden" id="page" value="history_order_user">
{{-- Modal xoa product --}}
<div class="modal fade" id="delete-product">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa báo giá</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-product" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="delete_quotation">Xóa báo giá</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal" id="finish_quotation">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
   
            $(document).on('click', '.delete-order' , function () {
                console.log("da den");
                var quotation_id = $(this).attr('data-id');
                $('tr').removeClass('remove-quotation');
                $('#delete_quotation').fadeIn();
                $('#delete_quotation').attr('data-id', quotation_id);
                $('#finish_quotation').fadeOut();
                $('#delete-product').modal('show');
                $('#notication-product').html('<span class="">Bạn có muốn xóa báo giá <b class="text-danger">(ID: '+ quotation_id +')</b> này không?</span>');
                $(this).closest('tr').addClass('remove-quotation');
            })

            $('#delete_quotation').on('click', function () {
                var quotation_id = $(this).attr('data-id');
                $.ajax({
                url: '/admin/users/quotation/delete_quotation',
                dataType: 'json',
                data: { id: quotation_id},
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#notication-product').html(html);
                    $('#delete_quotation').attr('disabled', true);
                },
                success: function (data) {
                    // console.log(data);
                    if (data != '') {
                        $('#notication-product').html('<span class="text-danger">Xóa báo giá thành công</span>');
                        $('#delete_quotation').attr('disabled', false);
                        $('.remove-quotation').fadeOut(1500);
                        $('#delete_quotation').fadeOut();
                        $('#finish_quotation').fadeIn();
                    } else {
                        $('#notication-product').html('<span class="text-danger">Xóa báo giá thất bại</span>');
                        $('#delete_quotation').attr('disabled', false);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('#notication-product').html('<span class="text-danger">Truy vấn báo giá lỗi!</span>');
                    $('#delete_quotation').attr('disabled', false);
                }
                })
            })
        });
    </script>
@endsection