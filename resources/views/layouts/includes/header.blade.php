@php
    use Carbon\Carbon;
    $ticket = UserHelper::admin_get_qtt_ticket_pending();
    $payment = UserHelper::admin_get_qtt_payment_confirm();
    //ticket
    $last_time = '';
    if ($ticket['last_time']) {
      Carbon::setLocale('vi');
      $now = Carbon::now();
      $last_time = $ticket['last_time'];
      $last_time = $last_time->diffForHumans($now);
    }
    // payment
    $last_time_payment = '';
    if ($payment['last_time']) {
      Carbon::setLocale('vi');
      $now = Carbon::now();
      $last_time_payment = $payment['last_time'];
      $last_time_payment = $last_time_payment->diffForHumans($now);
    }
@endphp
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="{{ route('admin.home') }}" class="nav-link">Home</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>

        <!-- Right navbar links fa-comments -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments"></i>
              <span class="badge badge-warning navbar-badge">{{ $ticket['qtt'] }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">{{ $ticket['qtt'] }} Yêu cầu hổ trợ mới</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> {{ $ticket['qtt'] }} Yêu cầu hổ trợ
                <span class="float-right text-muted text-sm">{{ $last_time }}</span>
              </a>
            </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-bell"></i>
              <span class="badge badge-danger navbar-badge">{{ $payment['qtt'] }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-item dropdown-header">{{ $payment['qtt'] }} thanh toán</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-donate  mr-2"></i> {{ $payment['qtt'] }} thanh toán
                <span class="float-right text-muted text-sm">{{ $last_time_payment }}</span>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link" href="{{ route('admin.logout') }}" data-toggle="tooltip" data-placement="bottom" title="Đăng xuất">
              <i class="fas fa-sign-out-alt"></i>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.navbar -->
