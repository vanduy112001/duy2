@php
$currentRouteName = \Illuminate\Support\Facades\Route::currentRouteName();
$group_products = GroupProduct::get_group_product_client();
@endphp
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('client.index')}}">
        <img src="{{ url('images/logo.png') }}" alt="Logo Cloudzone" width="160">
    </a>
    <hr class="sidebar-divider my-0">
    {{-- <!-- Nav Item - Dashboard -->
    <li class="nav-item {{ $currentRouteName == 'index' ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('index') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li> --}}

    <!-- Divider -->
    <hr class="sidebar-divider">
    {{-- <div class="sidebar-heading">
        Dịch vụ
    </div> --}}
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item active">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-services"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-store" style="opacity:1;color: rgba(255,255,255,.8);"></i>
            <span>Đăng ký dịch vụ</span>
        </a>
        <div id="collapse-services" class="menu-collapse collapse show" aria-labelledby="headingTwo"
            data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @foreach ($group_products as $group_product)
                    @if ($group_product->private == '0' && $group_product->id != 39 && $group_product->id != 29)
                        <a class="collapse-item" href="{{ route('client.product', [ $group_product->id, Str::slug($group_product->name)] ) }}">{{ $group_product->name }}</a>
                    @endif
                @endforeach
                <a class="collapse-item {{ $currentRouteName == 'client.domain.search' ? 'active' : '' }}" href="{{ route('client.domain.search') }}">Tìm tên miền</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    {{-- <hr class="sidebar-divider d-none d-md-block"> --}}
    <li class="nav-item active">
        <a class="nav-link collapsed" href="http://support.cloudzone.vn/" target="_blank">
            <i class="fas fa-question" style="opacity:1;color: rgba(255,255,255,.8);"></i>
            <span>Câu hỏi thường gặp</span>
        </a>
    </li>
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>