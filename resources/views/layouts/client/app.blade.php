<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <title>{{ 'Trang quản lý dịch vụ - Cloudzone Portal' }}</title>

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('css/template.css')}}">
    {{---------------------------------- Template AdminLTE --------------------------}}

    <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link href="{{ asset('css/user.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
    <link href="{{ asset('css/usercss.css') }}" rel="stylesheet">
    @yield('style')
        <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHDFXLJ');</script>
    <!-- End Google Tag Manager -->
</head>
<body id="page-top">
    <div id="wrapper">
      @include('layouts/client/includes/siderbar')
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
              @include('layouts/client/includes/header')
                <!-- End of Topbar -->
                <!-- Begin Page Content -->
                <div class="container-fluid">
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <div class="col col-md-6">
                            <h1 class="h3 mb-0 title-page">@yield('title')</h1>
                        </div>
                        <div class="col col-md-6">
                            <ol class="breadcrumb float-sm-right">
                                @yield('breadcrumb')
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="content-row">
                    @yield('content')
                </div>
                <!-- End of Main Content -->
                <!-- Footer -->
                @include('layouts/client/includes/footer')
                <!-- End of Footer -->
            </div>
            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('libraries/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('libraries/js/template.js')}}"></script>
        <!-- pusher -->
        <script src="https://js.pusher.com/5.1/pusher.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/user.min.js') }}"></script>
        <script src="{{ asset('js/all_page.js') }}"></script>
        <script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
        <script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
        <script src="{{ asset('/libraries/bootstrap/js/bootstrap-show-password.min.js') }}"></script>
        @yield('scripts')
    </div>
</body>
</html>
