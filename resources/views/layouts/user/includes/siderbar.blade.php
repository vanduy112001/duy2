@php
    $currentRouteName = \Illuminate\Support\Facades\Route::currentRouteName();
@endphp
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('index')}}">
        <img src="{{ url('images/logo.png') }}" alt="Logo Cloudzone" width="160">
    </a>
    <hr class="sidebar-divider my-0">

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Dịch vụ
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    @php
        $group_products = GroupProduct::get_group_product_private(Auth::user()->id);
    @endphp
    <li class="nav-item {{ $currentRouteName == 'user.product' || $currentRouteName == 'user.domain.search' ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-services" aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-store" style="opacity:1;color: rgba(255,255,255,.8);"></i>
            <span>Đăng ký dịch vụ</span>
        </a>
        <div id="collapse-services" class="menu-collapse collapse {{ $currentRouteName == 'user.product' || $currentRouteName == 'user.domain.search' ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @foreach ($group_products as $group_product)
                    <a class="collapse-item {{($currentRouteName == route('user.product', $group_product->id )) ? 'active' : '' }}" href="{{ route('user.product', $group_product->id ) }}">{{ $group_product->name }}</a>
                @endforeach
                <a class="collapse-item {{ $currentRouteName == 'user.domain.search' ? 'active' : '' }}"  href="{{ route('user.domain.search') }}">Đăng ký tên miền</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Pages Collapse Menu -->
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ ( $currentRouteName == 'service.index' || $currentRouteName == 'service.on' || $currentRouteName == 'service.servive_nearly' || $currentRouteName == 'user.domain.index' || $currentRouteName == 'service.peding' || $currentRouteName == 'user.order.list') ? 'active' : '' }}">
        <a style="padding-top: 5px" class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-myservices" aria-expanded="true" aria-controls="collapse3">
            <i class="fas fa-chart-pie" style="color: rgba(255,255,255,.8);"></i>
            <span>Dịch vụ của tôi</span>
        </a>
        <div id="collapse-myservices" class="menu-collapse collapse {{ ( $currentRouteName == 'service.index' || $currentRouteName == 'service.on' || $currentRouteName == 'service.servive_nearly' || $currentRouteName == 'user.domain.index' || $currentRouteName == 'service.peding' || $currentRouteName == 'user.order.list') ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ $currentRouteName == 'service.on' ? 'active' : '' }}" href="{{ route('service.on') }}">Đang sử dụng</a>
                <a class="collapse-item {{ $currentRouteName == 'user.domain.index' ? 'active' : '' }}"  href="{{ route('user.domain.index') }}">Dịch vụ tên miền</a>
                <a class="collapse-item {{ $currentRouteName == 'service.servive_nearly' ? 'active' : '' }}" href="{{ route('service.servive_nearly') }}">Đã hết hạn/hủy</a>
                <a class="collapse-item {{ $currentRouteName == 'service.index' ? 'active' : '' }}"  href="{{ route('service.index') }}">Tất cả dịch vụ</a>
                <a class="collapse-item {{ $currentRouteName == 'user.order.list' ? 'active' : '' }}"  href="{{ route('user.order.list') }}">Đơn đặt hàng</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Thanh toán
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ ( $currentRouteName == 'order.list_invoices' || $currentRouteName == 'order.list_invoices.paid' || $currentRouteName == 'order.list_invoices.unpaid' ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-payments" aria-expanded="true" aria-controls="collapse3">
            <i class="fas fa-file-invoice" style="color: rgba(255,255,255,.8);"></i>
            <span>Quản lý hóa đơn</span>
        </a>
        <div id="collapse-payments" class="menu-collapse collapse {{ ( $currentRouteName == 'order.list_invoices' || $currentRouteName == 'order.list_invoices.paid' || $currentRouteName == 'order.list_invoices.unpaid' ) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ $currentRouteName == 'order.list_invoices' ? 'active' : '' }}" href="{{ route('order.list_invoices') }}">Tất cả hóa đơn</a>
                <a class="collapse-item {{ $currentRouteName == 'order.list_invoices.paid' ? 'active' : '' }}" href="{{ route('order.list_invoices.paid') }}">Đã thanh toán</a>
                <a class="collapse-item {{ $currentRouteName == 'order.list_invoices.unpaid' ? 'active' : '' }}" href="{{ route('order.list_invoices.unpaid') }}">Chưa thanh toán</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Giao dịch
    </div>
    <li class="nav-item {{ $currentRouteName == 'user.pay_in_online' ? 'active' : '' }}">
        <a class="nav-link" href="{{ route('user.pay_in_online') }}">
        <i class="fas fa-donate"></i>
        <span>Nạp tiền vào tài khoản</span></a>
    </li>
    <li class="nav-item {{ $currentRouteName == 'user.history_payment' ? 'active' : '' }}">
        <a style="padding-top: 5px" class="nav-link" href="{{ route('user.history_payment') }}">
            <i class="fa fa-history" aria-hidden="true"></i>
        <span>Lịch sử giao dịch</span></a>
    </li>
    {{-- <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item" {{ $currentRouteName == 'user.pay_in_online' ? 'active' : '' }}>
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-payment" aria-expanded="true" aria-controls="collapse-payment">
            <i class="fas fa-donate" style="color: rgba(255,255,255,.8);"></i>
            <span>Nạp tiền vào tài khoản</span>
        </a>
        <div id="collapse-payment" class="menu-collapse collapse {{ $currentRouteName == 'user.pay_in_online' ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ $currentRouteName == 'user.pay_in_online' ? 'active' : '' }}" href="{{ route('user.pay_in_online') }}">Nạp tiền vào tài khoản</a>
            </div>
        </div>
    </li>

    <li class="nav-item" {{ $currentRouteName == 'user.history_payment' ? 'active' : '' }}>
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-history" aria-expanded="true" aria-controls="collapse-payment">
            <i class="fas fa-donate" style="color: rgba(255,255,255,.8);"></i>
            <span>Lịch sử giao dịch</span>
        </a>
        <div id="collapse-history" class="menu-collapse collapse {{ $currentRouteName == 'user.history_payment' ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ $currentRouteName == 'user.history_payment' ? 'active' : '' }}" href="{{ route('user.history_payment') }}">Lịch sử giao dịch</a>
            </div>
        </div>
    </li> --}}



    <!-- Divider -->
    <!-- <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Tài khoản đại lý
    </div> -->
    <!-- Nav Item - Pages Collapse Menu -->
    <!-- <li class="nav-item {{ ( $currentRouteName == 'user.customer.index' || $currentRouteName == 'user.customer.create' ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse-customer" aria-expanded="true" aria-controls="collapse-customer">
            <i class="fas fa-users" style="color: rgba(255,255,255,.8);"></i>
            <span>Quản lý khách hàng</span>
        </a>
        <div id="collapse-customer" class="menu-collapse collapse {{ ( $currentRouteName == 'user.customer.index' || $currentRouteName == 'user.customer.create' ) ? 'show' : '' }}" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item {{ $currentRouteName == 'user.customer.index' ? 'active' : '' }}" href="{{ route('user.customer.index') }}">Hồ sơ khách hàng</a>
                <a class="collapse-item {{ $currentRouteName == 'user.customer.create' ? 'active' : '' }}" href="{{ route('user.customer.create') }}">Tạo khách hàng mới</a>
            </div>
        </div>
    </li> -->
    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Thông báo
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ ( $currentRouteName == 'user.notification.index' || $currentRouteName == 'user.notification.detail' ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('user.notification.index') }}">
            <i class="fas fa-bell" style="color: rgba(255,255,255,.8);"></i>
            <span>Danh sách thông báo</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Liên hệ
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ ( $currentRouteName == 'user.ticket.getList' || $currentRouteName == 'user.ticket.detail' ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('user.ticket.getList') }}">
            <i class="fas fa-comments" style="color: rgba(255,255,255,.8);"></i>
            <span>Danh sách Ticket</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">
    <!-- Divider -->
    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        Nhật ký hoạt động
    </div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item {{ ( $currentRouteName == 'user.activity_log' ) ? 'active' : '' }}">
        <a class="nav-link collapsed" href="{{ route('user.activity_log') }}">
            <i class="fas fa-chart-line" style="color: rgba(255,255,255,.8);"></i>
            <span>Danh sách hoạt động</span>
        </a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>
