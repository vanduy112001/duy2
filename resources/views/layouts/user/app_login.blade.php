<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">

    <title>{{ 'Cloudzone Customer Portal' }}</title>

    <!-- Scripts -->

    <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link href="{{ asset('css/user.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
    @yield('style')

    <!-- Google Tag Manager -->
    <script>
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHDFXLJ');
    </script>
    <!-- End Google Tag Manager -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-SSN2BLFKBW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-SSN2BLFKBW');
  </script>
</head>
<style>
    canvas {
        display: block;
        vertical-align: bottom;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    #particles-js {
        width: 100%;
        height: 100%;
        background-color: #4c71dd;
        background-image: url('');
        background-size: cover;
        background-position: 50% 50%;
        background-repeat: no-repeat;
    }

    #overlay {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #0a0e1bab;
        cursor: pointer;
    }

    .contact-icon .btn-icon {
        border-radius: 50%;
        width: 30px;
        line-height: 0;
        height: 30px;
        padding: 0;
        text-align: center;
    }

    img.gioi-thieu {
        position: absolute;
        top: 10px;
        left: 20px;
        width: 30%;
        opacity: .08;
        min-width: 200px;
        max-height: 100px;
    }
</style>

<body class="bg-gradient-primary" style="background-image: url({{ url('images/login-bg.jpeg') }});">
    <div id="overlay"></div>
    <div id="container">
        <!-- Content Wrapper -->
        <!-- Outer Row -->
        <div class="row justify-content-center"
            style="margin:auto; align-items:center; display:-webkit-flex; min-height:100vh;padding: 15px 0;">
            <div class="col-xl-4 col-lg-5 col-md-6 col-sm-10 col-12" style="z-index: 1">
                <div class="card o-hidden border-0 shadow-lg" style="max-width: 400px; margin: auto;">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-12">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- particles.js container -->
            <div id="particles-js"></div>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="{{ asset('libraries/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <!-- Core plugin JavaScript-->
        <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
        <script src="{{ asset('js/user.min.js') }}"></script>
        @yield('scripts')
    </div>
</body>
<!-- scripts -->
<script src="{{ asset('libraries/particles/particles.js')}}"></script>
<script src="{{ asset('libraries/particles/app.js')}}"></script>

</html>