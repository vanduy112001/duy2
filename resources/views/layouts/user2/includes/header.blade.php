@php
    use Illuminate\Support\Facades\Auth;
    $user = UserHelper::get_user(Auth::user()->id);
    $unread_notifications_number = GroupProduct::get_unread_notifications_number();
    $notifications = GroupProduct::get_notifications();
    $qttVpsPros = GroupProduct::qttVpsPros();
@endphp
<link rel="stylesheet" type="text/css" href="{{ asset('css/user2/pages.css') }}">
<nav class="navbar header-navbar pcoded-header">
  <div class="navbar-wrapper">
    <div class="navbar-logo" logo-theme="theme1">
      <a href="{{ route('index') }}">
        <img src="{{ url('images/logo.png') }}" alt="Logo Cloudzone" width="140">
      </a>
      <a class="mobile-menu" id="mobile-collapse" href="#!">
        <i class="feather icon-menu icon-toggle-right"></i>
      </a>
      <a class="mobile-options waves-effect waves-light">
        <i class="feather icon-more-horizontal"></i>
      </a>
    </div>
    <div class="navbar-container container-fluid">
      <ul class="nav-left">
        <li class="p-0">
            <a class="p-0" href="{{ route('user.ticket.getList') }}">
                <div class="navbar-custom-link">
                    <div class="icon-custom-link">
                        <i class="fas fa-life-ring"></i>
                    </div>
                    <div class="custom-link">
                        Hỗ trợ
                    </div>
                </div>
            </a>
        </li>
        <li class="p-0">
            <a class="p-0" href="{{ route('user.history_payment') }}">
                <div class="navbar-custom-link">
                    <div class="icon-custom-link">
                        <i class="fas fa-exchange-alt"></i>
                    </div>
                    <div class="custom-link">
                        Giao dịch
                    </div>
                </div>
            </a>
        </li>
        @php
            $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
            //$group_products_vps_us = GroupProduct::get_group_product_vps_us_private(Auth::user()->id);
            $list_group_products_vps_us = GroupProduct::list_group_products_vps_us(Auth::user()->id);
        @endphp
        <li class="p-0" style="font-size: 16px">
            <div class="navbar-custom-link create_vps">
                <div class="icon-custom-link">
                    <i class="fa fa-server" aria-hidden="true"></i>
                </div>
                <div class="custom-link">
                    Tạo VPS
                </div>
                <div class="create-vps-menu">
                  @foreach ( $group_products_vps as $group )
                      @if ($group->link == 'vps')
                          <a class="create-vps-menu-item" href="{{ route('user.product.vps', $group->id) }}">Tạo VPS VN</a>
                      @endif
                  @endforeach
                  @foreach ($list_group_products_vps_us as $group_product_vps_us)
                      @if($group_product_vps_us->type == 'vps_us' || $group_product_vps_us->type == 'vps_uk' 
                      || $group_product_vps_us->type == 'vps_sing' || $group_product_vps_us->type == 'vps_hl'
                      || $group_product_vps_us->type == 'vps_ca')
                      <a class="create-vps-menu-item" href="{{ route('user.product.vps_us', $group_product_vps_us->id ) }}">Tạo {{ $group_product_vps_us->title }}</a>
                      @endif
                  @endforeach
                </div>
            </div>
        </li>
      </ul>
      <ul class="nav-right">
        @if ( !empty(Session::has('admin_id')) )
          <li class="header-notification px-0 show-point">
            <a class="nav-link" href="{{ route('admin.login_at_admin') }}" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Đăng nhập lại admin">
              <i class="fas fa-sign-out-alt"></i>
            </a>
          </li>
        @endif

        <li class="header-notification notification-desktop px-0">
            <a href="{{ route('user.notification.index') }}" class="m-0">
                <div class="dropdown-primary dropdown navbar-custom-link">
                    <div class="icon-custom-link">
                        <i class="fas fa-bell"></i>
                    </div>
                    <div class="custom-link dropdown-toggle" data-toggle="dropdown" style="white-space: normal;">
                        <span class="mytooltip tooltip-effect-9" href="#"> 
                            Thông báo: 
                            <b class="text-danger">{{ $unread_notifications_number }}</b>
                        </span>
                    </div>
                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                        <li>
                            <h6>Thông báo</h6>
                            @if ($unread_notifications_number > 0)
                                <label class="label label-danger">Mới</label>
                            @endif
                        </li>
                        @if ( $notifications->count() > 0 )
                            @foreach ($notifications as $notification)
                                <li style="border-top: solid 1px #d8d8d8; padding: 5px; @if (empty($notification->read_at)) background: #eeeeeeb8; @endif" >
                                    <a href="{{ route('user.notification.detail', $notification->notification->id) }}">
                                        <div class="media">
                                        <div class="mr-3">
                                            <button class="btn waves-effect waves-light btn-info btn-icon">
                                            <i class="fas fa-file-alt text-white mr-0"></i>
                                            </button>
                                        </div>
                                        <div class="media-body">
                                            <h5 class="notification-user">{{ $notification->notification->name }}</h5>
                                            <span class="notification-time">{{ $notification->notification->created_at->format('H:i d/m/Y') }}</span>
                                        </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                            <li style="border-top: solid 1px #d8d8d8">
                            <a class="text-center" style="font-size: 13px;font-weight: bold;color: #42a5f5;" href="{{ route('user.notification.index') }}">Xem tất cả</a>
                            </li>
                        @else
                            <li style="border-top: solid 1px #d8d8d8">
                                <a class="text-center" style="font-size: 13px;font-weight: bold;color: #42a5f5;" href="{{ route('user.notification.index') }}">Không có thông báo</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </a>
        </li>


        <li class="header-notification px-0 show-point">
            <div class="navbar-custom-link vps-on-processing">
                <div class="icon-custom-link">
                    <i class="fa fa-server" aria-hidden="true"></i>
                </div>
                <div class="custom-link">
                    <span class="mytooltip tooltip-effect-9" href="#"> 
                        Vps chờ tạo: 
                        <b class="text-danger {{ !empty($qttVpsPros) ? 'qttVpsPros' : '' }}" id="vpsPros" data-pros="{{ !empty($qttVpsPros) ? $qttVpsPros : '0' }}">{{ !empty($qttVpsPros) ? $qttVpsPros : '0' }}</b>
                    </span>
                </div>
            </div>
        </li>
        {{-- Cloudzone point --}}
        <li class="header-notification px-0 show-point">
          <a class="mytooltip tooltip-effect-9" href="#"><span class="tooltip-content3">"Point sẽ được tích lũy khi đăng ký sử dụng các dịch vụ của Cloudzone. Khi tích lũy đủ, bạn có thể dùng điểm này để đặt hàng"</span>
            Point: <b class="text-danger">{{ !empty($user->user_meta->point) ? $user->user_meta->point : '0' }}</b>
          </a>
        </li>
        {{-- Số dư tài khoản --}}
        <li class="header-notification px-2" style="line-height: 2.7;">
          <div class="show-credit">
            Số dư:
            <b class="text-danger">
              @if(!empty($user->credit->value))
                {!!number_format($user->credit->value,0,",",".") . ' ₫'!!}
              @else
                0 ₫
              @endif
            </b>
          </div>
          <div class="text-center" style="position: absolute; top: 22px;">
            <a class="btn waves-effect waves-light btn-warning btn-skew" href="{{ route('user.pay_in_online') }}">Nạp tiền</a>
          </div>
        </li>

        <li class="header-notification notification-mobile px-0 @if(!$unread_notifications_number) ml-3 @endif">
          <div class="dropdown">
            <div class="dropdown-toggle" data-toggle="dropdown">
              <i class="feather icon-bell"></i>
              <span class="badge bg-c-red" @if ($unread_notifications_number === 0) style="display: none" @endif>
                @if ($unread_notifications_number > 5)
                  5+
                @else
                  {{ $unread_notifications_number }}
                @endif
              </span>
            </div>
            <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
              <li>
                <h6>Thông báo</h6>
                @if ($unread_notifications_number > 0)
                  <label class="label label-danger">Mới</label>
                @endif
              </li>
              @if ( $notifications->count() > 0 )
                @foreach ($notifications as $notification)

                  <li style="border-top: solid 1px #d8d8d8; padding: 5px; @if (empty($notification->read_at)) background: #eeeeeeb8; @endif" >
                    <a href="{{ route('user.notification.detail', $notification->notification->id) }}">
                    <div class="media">
                      <div class="mr-3">
                        <button class="btn waves-effect waves-light btn-info btn-icon">
                          <i class="fas fa-file-alt text-white mr-0"></i>
                        </button>
                      </div>
                      <div class="media-body">
                        <h5 class="notification-user">{{ $notification->notification->name }}</h5>
                        <span class="notification-time">{{ $notification->notification->created_at->format('H:i d/m/Y') }}</span>
                      </div>
                    </div>
                  </a>
                  </li>

                @endforeach
                <li style="border-top: solid 1px #d8d8d8">
                  <a class="text-center" style="font-size: 13px;font-weight: bold;color: #42a5f5;" href="{{ route('user.notification.index') }}">Xem tất cả</a>
                </li>
              @else
                <li style="border-top: solid 1px #d8d8d8">
                    <a class="text-center" style="font-size: 13px;font-weight: bold;color: #42a5f5;" href="{{ route('user.notification.index') }}">Không có thông báo</a>
                </li>
                  {{-- <span class="dropdown-item d-flex align-items-center">Không có thông báo</span> --}}
              @endif
            </ul>
          </div>
        </li>

        <li class="user-profile header-notification pl-2 pr-1">
          <div class="dropdown">
            <div class="dropdown-toggle" data-toggle="dropdown">
              <img class="img-radius" style="height: 40px; object-fit: cover;" src="@php
                if(!empty($user->user_meta->avatar)) {
                  if (!empty(Storage::disk('public')->exists($user->user_meta->avatar))) {
                    echo Storage::url($user->user_meta->avatar);
                  } else {
                    echo $user->user_meta->avatar;
                  }
                } else {
                  echo asset('images/avatar5.png');
                }
              @endphp">
              <div class="d-none profile-name">
                <span>{{ $user->name }}</span>
                <i class="feather icon-chevron-down"></i>
              </div>
            </div>
            <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">

              <li>
                <a href="{{ route('profile') }}">
                  <i class="feather icon-user"></i>
                  Thông tin cá nhân
                </a>
              </li>
              <li>
                <a href="{{ route('change_pass_form') }}">
                  <i class="feather icon-lock"></i>
                  Đổi mật khẩu
                </a>
              </li>
              <li>
                <a href="{{ route('logout') }}">
                  <i class="feather icon-log-out"></i>
                  Đăng xuất
                </a>
              </li>
            </ul>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
<style>

</style>
<script src="{{ asset('libraries/user2/jquery.min.js')}}"></script>
<script type="text/javascript">
    $('a.mobile-options.waves-effect.waves-light').on('click', function(){
        $('.nav-left').toggle(500);
    });
    $('.notification-desktop').hover(
        function(){
            $('.notification-desktop .navbar-custom-link').addClass('show');
            $('.notification-desktop ul.show-notification').addClass('show');
        },
        function(){
            $('.notification-desktop .navbar-custom-link').removeClass('show');
            $('.notification-desktop ul.show-notification').removeClass('show');
        },
    );
</script>
