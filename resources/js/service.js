$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    // khởi tạo modal Toast
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });

    var $vpsCheckbox = $('.vps_checkbox');
    var lastChecked = null;
    var $vpsCheckboxNearly = $('.vps_nearly_checkbox');

    $(document).on('click', '.vps_checkbox_all', function () {
        if ($(this).is(':checked')) {
            $('.vps_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').addClass('action-vps');
        } else {
            $('.vps_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').removeClass('action-vps');
        }
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    })

    $(document).on('click', '.vps_checkbox_nearly_all', function () {
        if ($(this).is(':checked')) {
            $('.vps_nearly_checkbox').prop('checked', this.checked);
            $('.list_vps_nearly tbody tr').addClass('action-vps');
        } else {
            $('.vps_nearly_checkbox').prop('checked', this.checked);
            $('.list_vps_nearly tbody tr').removeClass('action-vps');
        }
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    })

    $('.hosting_checkbox_all').on('click', function () {
        if ($(this).is(':checked')) {
            $('.hosting_checkbox').prop('checked', this.checked);
            $('.list_hosting tbody tr').addClass('action-services');
        } else {
            $('.hosting_checkbox').prop('checked', this.checked);
            $('.list_hosting tbody tr').removeClass('action-services');
        }
    })

    $('.vp_classic').on('change', function () {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                    $vpsCheckbox = $('.vps_checkbox');
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_search').on("keyup", function () {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic').val();
        $.ajax({
            url: '/services/search_vps',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_on_multi').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_on_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#on_search_multi').html(html);
    });

    $(document).on("click", '#btn_on_search_multi', function () {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if (text.match(r2)) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if (text.match(r)) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof (ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_on',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_screent(data);
                    } else {
                        $('.list_vps tfoot').html('');
                        $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type').val();
            var sl = $('.vp_classic').val();
            $.ajax({
                url: '/services/search_vps',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_screent(data);
                    } else {
                        $('.list_vps tfoot').html('');
                        $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    $('.sort_next_due_date').on('click', function () {
        var vps_q = $('#vps_search').val();
        var vps_sort = $(this).attr('data-sort');
        var sl = $('.vp_classic').val();
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date i').removeClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date i').addClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps tfoot').html('');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    function list_vps_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index, vps) {
            // console.log(vps);
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_checkbox"></td>';
                // ip
                html += '<td class="ip_vps text-left" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                if (vps.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (vps.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                // password
                if (vps.type_vps == "vps") {
                    html += '<td class="type_vps" data-type="vps">' + vps.password + '</td>';
                } else {
                    html += '<td class="type_vps" data-type="nat">' + vps.password + '</td>';
                }
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                // if (vps.type_vps == 'vps') {
                //   html += '<td class="type_vps" data-type="vps">VPS</td>';
                // } else {
                //   html += '<td class="type_vps" data-type="nat">NAT-VPS</td>';
                // }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
                html += '<td class="page-service-action text-left">';
                if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                    if (vps.expired == true) {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else if (vps.status_vps == 'expire') {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        }
                        if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        }
                        html += '<button class="btn mr-1 btn-default button-action-vps" data-toggle="tooltip" data-placement="top" title="Rebuild VPS" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                        if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                            html += '<a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success btn-console" target="_blank" data-toggle="tooltip" data-placement="top" title="console"><i class="fas fa-desktop"></i></a>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="15">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps tfoot').html(html_page);
        $vpsCheckbox = $('.vps_checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.list_vps .pagination_vps_on a', function (event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic').val();
        $.ajax({
            url: '/services/search_vps',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                //  console.log(data);
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });
    // vps còn hạn
    $('.vp_classic_use').on('change', function () {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_use_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                    $vpsCheckbox = $('.vps_checkbox');
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_search_use').on("keyup", function () {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic_use').val();
        $.ajax({
            url: '/services/search_vps_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_use').on('click', function () {
        var vps_q = $('#vps_search').val();
        var vps_sort = $(this).attr('data-sort');
        var sl = $('.vp_classic_use').val();
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_use i').removeClass('fa-sort-down');
            $('.sort_next_due_date_use i').removeClass('fa-sort');
            $('.sort_next_due_date_use i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_use i').addClass('fa-sort-down');
            $('.sort_next_due_date_use i').removeClass('fa-sort');
            $('.sort_next_due_date_use i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                } else {
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps tfoot').html('');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_use_multi').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi_use" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_use_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#on_search_multi').html(html);
    });

    $(document).on("click", '#btn_use_search_multi', function () {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if (text.match(r2)) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if (text.match(r)) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof (ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_use',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_screent(data);
                    } else {
                        $('.list_vps tfoot').html('');
                        $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type').val();
            var sl = $('.vp_classic_use').val();
            $.ajax({
                url: '/services/search_vps_use',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_use_screent(data);
                    } else {
                        $('.list_vps tfoot').html('');
                        $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_vps_use_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_checkbox"></td>';
                // ip
                html += '<td class="ip_vps text-left" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                if (vps.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (vps.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                // password
                html += '<td>' + vps.password + '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';

                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
                html += '<td class="page-service-action  text-left">';
                if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                    if (vps.expired == true) {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        }
                        if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        }
                        html += '<button class="btn mr-1 btn-default button-action-vps off" data-toggle="tooltip" data-placement="top" title="Rebuild Vps" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                        // console
                        if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                            html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success btn-console" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                        } else {
                            html += '<td></td>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="15">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps tfoot').html(html_page);
        $vpsCheckbox = $('.vps_checkbox');
    }
    // ajax phân trang vps
    $(document).on('click', '.list_vps .pagination_vps_use a', function (event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic_use').val();
        $.ajax({
            url: '/services/search_vps_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                //  console.log(data);
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });
    // vps hết hạn
    // chọn sl
    $('.vp_nearly_classic').on('change', function () {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_nearly_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })
    // ajax phân trang vps nearly
    $(document).on('click', '.pagination_vps_nearly a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_nearly_search').val();
        var sl = $('.vp_nearly_classic').val();
        var vps_sort = $('.sort_type').val();
        $.ajax({
            url: '/services/search_vps_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    $('#vps_nearly_search').on("keyup", function () {
        var vps_q = $(this).val();
        var sl = $('.vp_nearly_classic').val();
        var vps_sort = $('.sort_type').val();
        $.ajax({
            url: '/services/search_vps_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_nearly').on('click', function () {
        var vps_q = $('#vps_nearly_search').val();
        var vps_sort = $(this).attr('data-sort');
        var sl = $('.vp_nearly_classic').val();
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort-down');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort');
            $('.sort_next_due_date_nearly i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_nearly i').addClass('fa-sort-down');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps tfoot').html('');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="15">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_nearly_multi').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_nearly_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#nearly_search_multi').html(html);
    });

    $(document).on("click", '#btn_nearly_search_multi', function () {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if (text.match(r2)) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if (text.match(r)) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof (ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_nearly',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_nearly_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var sl = $('.vp_nearly_classic').val();
            var vps_sort = $('.sort_type').val();
            $.ajax({
                url: '/services/search_vps_nearly',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function (data) {
                    if (data.data.length > 0) {
                        list_vps_nearly_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_vps_nearly_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_nearly_checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                //password
                html += '<td>' + vps.password + '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                // description
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                // status
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
                html += '<td class="page-service-action">';
                if (vps.status_vps != 'delete_vps') {
                    if (vps.status_vps != 'cancel') {
                        if (vps.status_vps != 'suspend') {
                            html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                            html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps_nearly tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps_nearly tfoot').html(html_page);
        $vpsCheckboxNearly = $('.vps_nearly_checkbox');
    }

    // chọn sl
    $('.vp_cancel_classic').on('change', function () {
        var sl = $(this).val();
        var vps_q = $('#vps_cancel_search').val();
        $.ajax({
            url: '/services/search_vps_cancel',
            dataType: 'json',
            data: { q: vps_q, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="11">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })
    // ajax phân trang vps nearly
    $(document).on('click', '.pagination_vps_cancel a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_cancel_search').val();
        var sl = $('.vp_cancel_classic').val();
        $.ajax({
            url: '/services/search_vps_cancel',
            dataType: 'json',
            data: { q: vps_q, sl: sl, page: page },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="11">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="12">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    $('#vps_cancel_search').on("keyup", function () {
        var vps_q = $(this).val();
        var sl = $('.vps_cancel_search').val();
        $.ajax({
            url: '/services/search_vps_cancel',
            dataType: 'json',
            data: { q: vps_q, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="11">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_cancel_multi').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_cancel_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#cancel_search_multi').html(html);
    });

    $(document).on("click", '#btn_cancel_search_multi', function () {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if (text.match(r2)) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if (text.match(r)) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof (ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_cancel',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_cancel_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var sl = $('.vps_cancel_search').val();
            $.ajax({
                url: '/services/search_vps_cancel',
                dataType: 'json',
                data: { q: vps_q, sl: sl },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="11"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function (data) {
                    if (data.data.length > 0) {
                        list_vps_cancel_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="11">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_vps_cancel_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            // checkbox
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_nearly_checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps_nearly tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps_nearly tfoot').html(html_page);
        $vpsCheckbox = $('.vps_checkbox');
    }

    // Lưu các checkbox vps
    $(document).on('click', '.vps_nearly_checkbox', function (e) {
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log(lastChecked, this);
            var start = $vpsCheckboxNearly.index(this);
            var end = $vpsCheckboxNearly.index(lastChecked);
            $vpsCheckboxNearly.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($vpsCheckboxNearly, function (index, value) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action-vps');
                } else {
                    $(this).closest('tr').removeClass('action-vps');
                }
            })
        }
        lastChecked = this;
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    });

    $('.list_action_nearly').on('change', function () {
        var action = $('.list_action_nearly').val();
        if (action == "") {
            alert('Vui lòng chọn hành động.213123');
        } else if (action == 'change_user') {
            var checked = $('.vps_nearly_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function (index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
                // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
            } else {
                alert('Vui lòng chọn một VPS3.');
            }
        } else if (action == 'export_vps') {
            var checked = $('.vps_nearly_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function (index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/xuat-file-excel?list_vps=' + list_vps;
            } else {
                alert('Vui lòng chọn một VPS3.');
            }
        } else {
            var checked = $('.vps_nearly_checkbox:checked');
            $('#button-terminated-nearly').fadeIn();
            $('#button-rebuil-nearly').fadeOut();
            $('#button-multi-finish').fadeOut();
            $('#button-multi-finish').attr('data-type', 'action');
            $('#button-multi-finish').text('Hoàn thành');
            if (checked.length > 0) {
                $('#modal-services-nearly').modal('show');
                switch (action) {
                    case 'delete':
                        $('.modal-title').text('Hủy dịch vụ VPS');
                        $('#notication-invoice-nearly').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ VPS này không?</span>');
                        $('#button-terminated-nearly').attr('data-type', 'vps');
                        $('#button-terminated-nearly').attr('data-action', action);
                        break;
                    case 'expired':
                        $('.modal-title').text('Yêu cầu gia hạn dịch vụ VPS');
                        var list_vps = [];
                        $.each(checked, function (index, value) {
                            list_vps.push($(this).val());
                        })
                        $.ajax({
                            url: '/service/request_expired_vps',
                            data: { list_vps: list_vps },
                            dataType: 'json',
                            beforeSend: function () {
                                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                                $('#button-terminated-nearly').attr('disabled', true);
                                $('#notication-invoice-nearly').html(html);
                            },
                            success: function (data) {
                                var html = '';
                                if (data.error == 9998) {
                                    $('#notication-invoice-nearly').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                    $('#button-terminated-nearly').attr('disabled', false);
                                } else {
                                    if (data.price_override != '') {
                                        if (data.price_override.error == 0) {
                                            html += '<div class="form-group">';
                                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                            html += '<div class="mt-3 mb-3">';
                                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                            html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                            html += '</select>';
                                            html += '</div>';
                                            html += '</div>';
                                            $('#notication-invoice-nearly').html(html);
                                            $('#button-terminated-nearly').attr('data-action', action);
                                            $('#button-terminated-nearly').attr('data-type', 'vps');
                                            $('#button-terminated-nearly').attr('disabled', false);
                                            $('.select_expired').select2();
                                        } else {
                                            $('#notication-invoice-nearly').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                            $('#button-terminated-nearly').attr('disabled', false);
                                        }
                                    } else {
                                        html += '<div class="form-group">';
                                        html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                        html += '<div class="mt-3 mb-3">';
                                        html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                        html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                        html += '</select>';
                                        html += '</div>';
                                        html += '</div>';
                                        $('#notication-invoice-nearly').html(html);
                                        $('#button-terminated-nearly').attr('data-action', action);
                                        $('#button-terminated-nearly').attr('data-type', 'vps');
                                        $('#button-terminated-nearly').attr('disabled', false);
                                        $('.select_expired').select2();
                                    }
                                }
                            },
                            error: function (e) {
                                console.log(e);
                                $('#notication-invoice-nearly').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                                $('#button-terminated-nearly').attr('disabled', false);
                            },
                        })
                        break;
                }
            } else {
                alert('Vui lòng chọn một VPS3.');
            }
        }
    });

    $('#button-multi-finish').on('click', function () {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr('data-link');
            window.open(link, '_blank').focus();
        }
    })

    $('#button-terminated-nearly').on('click', function () {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        var checked = $('.vps_nearly_checkbox:checked');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function (index, value) {
            id_services.push($(this).val());
        })
        // console.log(id_services);
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated-nearly').attr('disabled', true);
                $('#notication-invoice-nearly').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 7) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đã hết hạn. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 8) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang đổi IP. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        } else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        $('#modal-services-nearly').modal('hide');
                        $('#button-terminated-nearly').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        if (action == 'delete') {
                            html = '<span class="text-danger">Đã hủy</span>';
                            $('#vps .action-vps .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'hủy dịch vụ',
                                body: "Thao tác hủy các dịch vụ vps thành công.",
                            })
                            $('#modal-services-nearly').modal('hide');
                            $('#button-terminated-nearly').attr('disabled', false);
                            $('#vps .action-vps .off').attr('disabled', false);
                            $('#vps .action-vps .on').attr('disabled', true);
                        } else if (action == 'expired') {
                            // dd(data);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'gia hạn',
                                body: "Thao tác yêu cầu gia hạn các dịch vụ VPS thành công.",
                            })
                            $('#vps .action-vps .expired').fadeOut();
                            $('#button-terminated-nearly').attr('disabled', false);
                            html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                            $('#notication-invoice-nearly').html(html);
                            $('#button-terminated-nearly').fadeOut();
                            $('#button-multi-finish').fadeIn();
                            $('#button-multi-finish').attr('data-type', 'expired');
                            $('#button-multi-finish').attr('data-link', '/order/check-invoices/' + data);
                            $('#button-multi-finish').text('Thanh toán');
                        }
                    }
                    $('tr').removeClass('action-vps');
                    $('.vps_nearly_checkbox').removeAttr('checked');
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
                $('.list_action_nearly').val('');
            },
            error: function (e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service').fadeOut();
                $('#button-finish').fadeIn();
            }
        })
    });

    $('.vps_all_classic').on('change', function () {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_all_vps_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#all_vps_search').on("keyup", function () {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vps_all_classic').val();
        $.ajax({
            url: '/services/all_vps_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function (data) {
                // console.log(data.data);
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_all_vps').on('click', function () {
        var vps_q = $('#all_vps_search').val();
        var sl = $('.vps_all_classic').val();
        var vps_sort = $(this).attr('data-sort');
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort-down');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort');
            $('.sort_next_due_date_all_vps i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_all_vps i').addClass('fa-sort-down');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/all_vps_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_all_multi').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_all_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#all_search_multi').html(html);
    });

    $(document).on("click", '#btn_all_search_multi', function () {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if (text.match(r2)) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if (text.match(r)) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof (ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_all',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_all_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_all_vps_screent(data);
                    } else {
                        $('.list_all_vps tfoot').html('');
                        $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type').val();
            var sl = $('.vps_all_classic').val();
            $.ajax({
                url: '/services/all_vps_search',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function () {
                    var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_all_vps tbody').html(html);
                },
                success: function (data) {
                    // console.log(data.data);
                    if (data.data.length > 0) {
                        list_all_vps_screent(data);
                    } else {
                        $('.list_all_vps tfoot').html('');
                        $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_all_vps_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                // ip
                html += '<td class="ip_vps" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_all_vps tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="11" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_all_vps tfoot').html(html_page);
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_all a', function (event) {
        event.preventDefault();
        // console.log('da click');
        var page = $(this).attr('data-page');
        var vps_q = $('#all_vps_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vps_all_classic').val();
        // console.log(vps_q, vps_sort);
        $.ajax({
            url: '/services/all_vps_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function () {
                var html = '<td class="text-center"  colspan="8"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });
    /**
     * VPS
     */
    // Lưu các checkbox vps
    $(document).on('click', '.vps_checkbox', function (e) {
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log('da click 2');
            var start = $vpsCheckbox.index(this);
            var end = $vpsCheckbox.index(lastChecked);
            $vpsCheckbox.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($vpsCheckbox, function (index, value) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action-vps');
                } else {
                    $(this).closest('tr').removeClass('action-vps');
                }
            })
        }
        lastChecked = this;
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    });
    // Click vào các button action của Vps
    function listaction (action){
        console.log(action);
        var action = action;
        if (action == "") {
            alert('Vui lòng chọn hành động12312312.');
        } else if (action == 'change_user') {
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function (index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
                // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
            } else {
                alert('Vui lòng chọn một VPS1');
            }
        } else if (action == 'export_vps') {
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function (index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps/xuat-file-excel?list_vps=' + list_vps;
            } else {
                alert('Vui lòng chọn một VPS.1');
            }
        } else {
            var checked = $('.vps_checkbox:checked');
            $('#button-terminated').fadeIn();
            $('#button-rebuil').fadeOut();
            $('#button-multi-finish').fadeOut();
            $('#button-multi-finish').attr('data-type', 'action');
            $('#button-multi-finish').text('Hoàn thành');
            if (checked.length > 0) {
                $('#modal-services').modal('show');
                switch (action) {
                    case 'on':
                        $('.modal-title').text('Bật VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn bật các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off':
                        $('.modal-title').text('Tắt VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn tắt các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'delete':
                        $('.modal-title').text('Hủy dịch vụ VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'auto_refurn':
                        $('.modal-title').text('Cài đặt tự động gia hạn VPS');
                        var html_auto = '<span>Bạn có muốn cài đặt tự động gia hạn các dịch vụ VPS này không?</span><br><br>';
                        html_auto += '<span class="luu-y"><b>* Lưu ý: </b>Tự động gia hạn VPS sẽ tự động gia hạn khi VPS hết hạn và sẽ tự động trừ tiền trong số dư tài khoản của quý khách. ';
                        html_auto += 'Trong khi số dư tài khoản của quý khách không đủ để gia hạn VPS thì hệ thống sẽ thông báo cho quý khách qua thông báo và Email.</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off_auto_refurn':
                        $('.modal-title').text('Tắt tự động gia hạn VPS');
                        var html_auto = '<span class="text-danger">Bạn có muốn tắt tự động gia hạn các dịch vụ VPS này không?</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'restart':
                        $('.modal-title').text('Khởi động lại VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn khởi động lại các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'expired':
                        $('.modal-title').text('Yêu cầu gia hạn dịch vụ VPS');
                        var list_vps = [];
                        $.each(checked, function (index, value) {
                            list_vps.push($(this).val());
                        })
                        // console.log(list_vps);
                        $.ajax({
                            url: '/service/request_expired_vps',
                            data: { list_vps: list_vps },
                            dataType: 'json',
                            beforeSend: function () {
                                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                                $('#button-terminated').attr('disabled', true);
                                $('#notication-invoice').html(html);
                            },
                            success: function (data) {
                                var html = '';
                                if (data.error == 9998) {
                                    $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (data.error == 1) {
                                    $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                                    $('#button-terminated').attr('disabled', false);
                                } else {
                                    if (data.price_override != '') {
                                        if (data.price_override.error == 0) {
                                            html += '<div class="form-group">';
                                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                            html += '<div class="mt-3 mb-3">';
                                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                            html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                            html += '</select>';
                                            html += '</div>';
                                            html += '</div>';
                                            $('#notication-invoice').html(html);
                                            $('#button-terminated').attr('data-action', action);
                                            $('#button-terminated').attr('data-type', 'vps');
                                            $('#button-terminated').attr('disabled', false);
                                            $('.select_expired').select2();
                                        } else {
                                            $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                            $('#button-terminated').attr('disabled', false);
                                        }
                                    } else {
                                        html += '<div class="form-group">';
                                        html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                        html += '<div class="mt-3 mb-3">';
                                        html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                        html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                        html += '</select>';
                                        html += '</div>';
                                        html += '</div>';
                                        $('#notication-invoice').html(html);
                                        $('#button-terminated').attr('data-action', action);
                                        $('#button-terminated').attr('data-type', 'vps');
                                        $('#button-terminated').attr('disabled', false);
                                        $('.select_expired').select2();
                                    }
                                }
                            },
                            error: function (e) {
                                console.log(e);
                                $('#notication-invoice').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                                $('#button-terminated').attr('disabled', false);
                            },
                        })
                        break;
                    case 'rebuild':
                        $('.modal-title').text('Cài đặt lại VPS');
                        $('#button-rebuil').attr('disabled', false);
                        var type_vps = $('.action-vps .type_vps');
                        var check = [];
                        $.each(type_vps, function (index, type) {
                            check.push($(this).attr('data-type'));
                        })
                        var check_type = check[0];
                        var term = true;
                        for (var i = 1; i < check.length; i++) {
                            if (check_type != check[i]) {
                                term = false;
                            }
                        }
                        if (term == true) {
                            if (check_type == ' vps ' || check_type == 'vps') {
                                var html = '';
                                html += '<div class="text-notication">';
                                html += '</div>';
                                html += '<div class="text-left ml-4">';
                                html += 'Danh sách VPS yêu cầu cài đặt lại: <br>';
                                $.each(checked, function (index, value) {
                                    html += $(this).attr('data-ip') + '<br>';
                                })
                                html += '</div>';
                                html += '<div class="text-center text-danger mt-2 form-rebuild">';
                                html += 'Bạn có muốn cài đặt lại tất cả VPS này không';
                                html += '</div>';
                                html += '<div class="text-luuy">';
                                html += '</div>';
                                // html += '<div class="text-danger text-left mt-2">';
                                // html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại';
                                // html += '</div>';
                                $('#notication-invoice').html(html);
                                $('#button-rebuil').attr('data-action', 'form');
                                $('#button-rebuil').attr('data-type', 'vps');
                                $('#button-rebuil').fadeIn();
                                $('#button-terminated').fadeOut();
                            } else {
                                var html = '';
                                html += '<div class="text-notication">';
                                html += '</div>';
                                html += '<div class="text-left ml-4">';
                                html += 'Danh sách VPS yêu cầu cài đặt lại: <br>';
                                $.each(checked, function (index, value) {
                                    html += $(this).attr('data-ip') + '<br>';
                                })
                                html += '</div>';
                                html += '<div class="text-center text-danger mt-2 form-rebuild">';
                                html += 'Bạn có muốn cài đặt lại tất cả VPS này không';
                                html += '</div>';
                                html += '<div class="text-luuy">';
                                html += '</div>';
                                // html += '<div class="text-danger text-left mt-2">';
                                // html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại';
                                // html += '</div>';
                                $('#notication-invoice').html(html);
                                $('#button-rebuil').attr('data-action', 'form-nat');
                                $('#button-rebuil').attr('data-type', 'vps');
                                $('#button-rebuil').fadeIn();
                                $('#button-terminated').fadeOut();
                            }
                        } else {
                            var html = '';
                            $('#notication-invoice').html('<span class="text-danger">Loại VPS không trùng nhau.Quý khách vui lòng kiểm tra lại loại VPS của các VPS được chọn.</span>');
                            $('#button-terminated').fadeOut();
                        }

                        break;
                    // case 'change_ip':
                    //     $('.modal-title').text('Thay đổi IP VPS');
                    //     var html = '';
                    //     var type_vps = $('.action-vps .type_vps');
                    //     var check = [];
                    //     $.each(type_vps, function (index, type) {
                    //         check.push($(this).attr('data-type'));
                    //     })
                    //     var check_type = check[0];
                    //     var term = true;
                    //     for (var i = 1; i < check.length; i++) {
                    //       if (check_type != check[i]) {
                    //         term = false;
                    //       }
                    //     }
                    //     if (term == true) {
                    //         html += '<div class="text-notication">';
                    //         html += '</div>';
                    //         html += '<div class="text-left ml-4">';
                    //         html += 'Danh sách VPS yêu cầu đổi IP: <br>';
                    //         $.each(checked, function(index, value) {
                    //             html += $(this).attr('data-ip') + '<br>';
                    //         })
                    //         html += '</div>';
                    //         html += '<div class="text-center text-danger mt-4 form-rebuild">';
                    //         html += 'Bạn có muốn đổi IP cho tất cả VPS này không';
                    //         html += '</div>';
                    //         html += '<div class="text-danger text-left mt-2 mb-2">';
                    //         html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần đổi IP.';
                    //         html += '</div>';
                    //         $('#notication-invoice').html(html);
                    //         $('#button-terminated').attr('data-action', action);
                    //         $('#button-terminated').attr('data-type', 'vps');
                    //     }
                    //     else {
                    //         var html = '';
                    //         $('#notication-invoice').html('<span class="text-danger">NAT VPS không thể đổi IP. Quý khách vui lòng kiểm tra lại loại VPS của các VPS được chọn.</span>');
                    //         $('#button-terminated').fadeOut();
                    //     }
                    //   break;
                }
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        }
    }
    $('#vps .list_action').on('change', function () {
        console.log(1)
           
           listaction($(".list_action").val());
           
 
      
    });
    $('#vps .list_action_bot').on('change', function () {
        console.log(2)
      
   listaction( $(".list_action_bot").val());


});

    $('.btn_change_user').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function (index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
            // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
        } else {
            alert('Vui lòng chọn một VPS2.');
        }
    });

    $('.btn_nearly_change_user').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_nearly_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function (index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
            // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
        } else {
            alert('Vui lòng chọn một VPS2.');
        }
    });

    $('.btn_export_vps').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function (index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps/xuat-file-excel?list_vps=' + list_vps;
        } else {
            alert('Vui lòng chọn một VPS2.');
        }
    });

    $('.btn_nearly_export_vps').on('click', function (event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_nearly_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function (index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps/xuat-file-excel?list_vps=' + list_vps;
        } else {
            alert('Vui lòng chọn một VPS2.');
        }
    });

    $('#button-rebuil').on('click', function () {
        var action = $(this).attr('data-action');
        if (action == 'form') {
            var html = '';
            var checked = $('.vps_checkbox:checked');
            var id_services = [];
            $.each(checked, function (index, value) {
                id_services.push($(this).val());
            })
            $.ajax({
                url: '/service/check-os',
                dataType: 'json',
                data: { list_id: id_services },
                beforeSend: function () {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-rebuil').attr('disabled', true);
                    $('#form-rebuild').html(html);
                },
                success: function (data) {
                    //called when successful
                    // console.log(data);
                    var html = '';
                    if (!data.error) {
                        html += '<div class="form-group mt-4">';
                        html += '<label for="os_select">Chọn hệ điều hành</label>';
                        html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
                        if (data.type) {
                            $.each(data.os, function (index, value) {
                                html += '<option value="' + value.id + '">' + value.os + '</option>';
                            })
                        } else {
                            // html += '<option value="1">Windows 7</option>';
                            html += '<option value="1">Windows 10 64bit</option>';
                            html += '<option value="2">Windows Server 2012 R2</option>';
                            html += '<option value="3">Windows Server 2016</option>';
                            html += '<option value="4">Linux CentOS 7 64bit</option>';
                            html += '<option value="15">Windows Server 2019</option>';
                            html += '<option value="31">Linux Ubuntu-20.04</option>';
                        }
                        html += '</select>';
                        html += '</div>';
                        $('.form-rebuild').html(html);
                        $('.select2').select2();
                        $('#button-rebuil').attr('data-action', 'confirm');
                        $('#button-rebuil').attr('disabled', false);
                    } else {
                        html += 'VPS ' + data.ip + ' bị giới hạn hệ điều hành khác với các VPS khác. Quý khách vui lòng bỏ chọn VPS này và thực hiện tiếp quy trình cài lại hệ điều hành.';
                        $('.form-rebuild').html(html);
                    }
                },
                error: function (e) {
                    //called when there is an error
                    console.log(e);
                    $('#form-rebuild').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            });
        } else if (action == 'form-nat') {
            var html = '';
            html += '<div class="form-group mt-4">';
            html += '<label for="os_select">Chọn hệ điều hành</label>'
            html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
            html += '<option value="2">Windows Server 2012 R2</option>';
            html += '<option value="3">Windows Server 2016</option>';
            html += '</select>';
            html += '</div>';
            $('.form-rebuild').html(html);
            $('.select2').select2();
            $('#button-rebuil').attr('data-action', 'confirm');
        } else if (action == 'confirm') {
            var os = $('#os_select option:selected').text();
            var os_id = $('#os_select option:selected').val();
            var security = 0;
            var security_text = 'Không';
            if ($('#security').is(':checked')) {
                security = 1;
                security_text = 'Có';
            }
            var html = '';
            html += '<div class="text-left text-bold mauden mb-2">';
            html += 'Bạn sẽ cài đặt lại các VPS ở trên với thông tin như sau: <br>';
            html += 'Hệ điều hành: ' + os;
            // html += '<br> Security: ' + security_text;
            html += '<input type="hidden" name="os" id="os" value="' + os_id + '">';
            html += '<input type="hidden" name="security" id="security" value="' + security + '">';
            html += '</div>';
            html += '<div class="text-danger text-left mt-2 mb-2">';
            html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại.';
            html += '</div>';
            $('.form-rebuild').html(html);
            $('#button-rebuil').attr('data-action', 'submit');
        } else if (action == 'submit') {
            var token = $('meta[name="csrf-token"]').attr('content');
            var id_services = [];
            var checked = $('.vps_checkbox:checked');
            var os = $('#os').val();
            var security = $('#security').val();
            $.each(checked, function (index, value) {
                id_services.push($(this).val());
            })
            $.ajax({
                url: '/services/rebuild_vps',
                type: 'post',
                data: { '_token': token, id: id_services, os: os, security: security },
                dataType: 'json',
                beforeSend: function () {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-rebuil').attr('disabled', true);
                    $('#notication-invoice').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.success != '') {
                        $(document).Toasts('create', {
                            class: 'bg-info',
                            title: 'VPS',
                            subtitle: 'cài đặt lại vps',
                            body: "Thao tác cài đặt lại các dịch vụ vps thành công.",
                        })
                        $('#button-rebuil').attr('disabled', false);
                        $('#modal-services').modal('hide');
                        $('.action-vps .vps-status span').addClass('vps-progressing');
                        $('.action-vps .vps-status span').text('Đang cài lại ...');
                        $('.action-vps .vps-status span').removeClass('text-success');
                        $('.action-vps .vps-status span').removeClass('text-danger');
                        $(".action-vps .page-service-action").html('');
                        $(".vps-console").html('');
                        $('tr').removeClass('action-vps');
                    } else {
                        $('#notication-invoice').html('<span class="text-danger">' + data.error + '</span>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('#button-rebuil').attr('disabled', false);
                    $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            })
        }
    })
    /**
     * Hosting
     */
    // Lưu các checkbox hosting
    $(document).on('click', '.hosting_checkbox', function () {
        if ($(this).is(':checked')) {
            // console.log('h1');
            $(this).closest('tr').addClass('action-services');
        } else {
            // console.log('h2');
            $(this).closest('tr').removeClass('action-services');
        }
    });

    // Click vào các button action của hosting
    $('#hosting .btn-action-hosting').on('click', function () {
        var action = $(this).attr('data-type');
        var checked = $('.hosting_checkbox:checked');
        $('#button-terminated').fadeIn();
        $('#button-rebuil').fadeOut();
        if (checked.length > 0) {
            $('#modal-services').modal('show');
            switch (action) {
                case 'on':
                    $('.modal-title').text('Yêu cầu bật dịch vụ Hosting');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn bật các hosting này không?</span>');
                    $('#button-terminated').attr('data-action', action);
                    $('#button-terminated').attr('data-type', 'hosting');
                    break;
                case 'off':
                    $('.modal-title').text('Yêu cầu tắt dịch vụ Hosting');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn tắt các hosting này không?</span>');
                    $('#button-terminated').attr('data-action', action);
                    $('#button-terminated').attr('data-type', 'hosting');
                    break;
                case 'delete':
                    $('.modal-title').text('Yêu cầu hủy dịch vụ Hosting');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ hosting này không?</span>');
                    $('#button-terminated').attr('data-action', action);
                    $('#button-terminated').attr('data-type', 'hosting');
                    break;
                case 'expired':
                    $('.modal-title').text('Yêu cầu gia hạn dịch vụ Hosting');
                    var list_hosting = [];
                    $.each(checked, function (index, value) {
                        list_hosting.push($(this).val());
                    })
                    $.ajax({
                        url: '/service/request_expired_hosting',
                        data: { list_hosting: list_hosting },
                        dataType: 'json',
                        beforeSend: function () {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button-terminated').attr('disabled', true);
                            $('#notication-invoice').html(html);
                        },
                        success: function (data) {
                            var html = '';
                            if (data.error == 9998) {
                                $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button-terminated').attr('disabled', false);
                            } else {
                                if (data.price_override != '') {
                                    if (data.price_override.error == 0) {
                                        html += '<div class="form-group">';
                                        html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                        html += '<div class="mt-3 mb-3">';
                                        html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                        html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                        html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                        html += '</select>';
                                        html += '</div>';
                                        html += '</div>';
                                        $('#notication-invoice').html(html);
                                        $('#button-terminated').attr('data-action', action);
                                        $('#button-terminated').attr('data-type', 'hosting');
                                        $('#button-terminated').attr('disabled', false);
                                        $('.select_expired').select2();
                                    } else {
                                        $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                        $('#button-terminated').attr('disabled', false);
                                    }
                                } else {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    if (data.total['monthly'] != '0 VNĐ') {
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                    }
                                    if (data.total['twomonthly'] != '0 VNĐ') {
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                    }
                                    if (data.total['quarterly'] != '0 VNĐ') {
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                    }
                                    if (data.total['semi_annually'] != '0 VNĐ') {
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                    }
                                    if (data.total['annually'] != '0 VNĐ') {
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                    }
                                    if (data.total['biennially'] != '0 VNĐ') {
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                    }
                                    if (data.total['triennially'] != '0 VNĐ') {
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                    }
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-invoice').html(html);
                                    $('#button-terminated').attr('data-action', action);
                                    $('#button-terminated').attr('data-type', 'hosting');
                                    $('#button-terminated').attr('disabled', false);
                                    $('.select_expired').select2();
                                }
                            }
                        },
                        error: function (e) {
                            console.log(e);
                            $('#notication-invoice').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button-terminated').attr('disabled', false);
                        },
                    })
                    break;
            }
        } else {
            alert('Chọn ít nhất một Hosting');
        }
    });
    // bấm vào thanh toán để chuyển link
    $('#button-multi-finish').on('click', function () {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr('data-link');
            window.open(link, '_blank').focus();
        }
    })
    // modal action của hosting
    $('#button-terminated').on('click', function () {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        if (type == 'hosting') {
            var checked = $('.hosting_checkbox:checked');
        } else if (type == 'vps') {
            var checked = $('.vps_checkbox:checked');
        }
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function (index, value) {
            id_services.push($(this).val());
        })
        // console.log(id_services);
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {

                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 7) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đã hết hạn. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 8) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang đổi IP. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        } else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        $('#modal-services').modal('hide');
                        $('#button-terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        switch (type) {
                            case 'hosting':
                                if (action == 'on') {
                                    html = '<span class="text-success">Đã bật</span>';
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'Hosting',
                                        subtitle: 'bật hosting',
                                        body: "Thao tác mở các dịch vụ hostings thành công.",
                                    })
                                    $('#hosting .action-services .hosting-status').html(html);
                                    $('.action-services .suspend').attr('disabled', false);
                                    $('.action-services .unsuspend').attr('disabled', true);
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (action == 'off') {
                                    html = '<span class="text-danger">Đã tắt</span>';
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'Hosting',
                                        subtitle: 'tắt hosting',
                                        body: "Thao tác tắt các dịch vụ hostings thành công.",
                                    })
                                    $('#hosting .action-services .hosting-status').html(html);
                                    $('.action-services .suspend').attr('disabled', true);
                                    $('.action-services .unsuspend').attr('disabled', false);
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (action == 'delete') {
                                    html = '<span class="text-danger">Đã hủy</span>';
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'Hosting',
                                        subtitle: 'hủy dịch vụ hosting',
                                        body: "Thao tác hủy các dịch vụ hostings thành công.",
                                    })
                                    $('.action-services .terminated').fadeOut();
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#hosting .action-services .hosting-status').html(html);
                                    $('.action-services .suspend').attr('disabled', true);
                                    $('.action-services .unsuspend').attr('disabled', true);
                                } else if (action == 'expired') {
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'Hosting',
                                        subtitle: 'gia hạn hosting',
                                        body: "Thao tác gia hạn các dịch vụ hosting thành công.",
                                    })
                                    $('.action-services .expired').fadeOut();
                                    $('#button-terminated').attr('disabled', false);
                                    html += '<span class="text-center">Yêu cầu gia hạn hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                    $('#notication-invoice').html(html);
                                    $('#button-terminated').fadeOut();
                                }
                                break;
                            case 'vps':
                                if (action == 'on') {
                                    html = '<span class="text-success">Đang bật</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'bật vps',
                                        body: "Thao tác bật các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    // console.log('da den on');
                                    $('.action-vps .off').attr('disabled', true);
                                    $('.action-vps .on').attr('disabled', false);
                                    $('#button-multi-finish').fadeIn();
                                } else if (action == 'off') {
                                    html = '<span class="text-danger">Đã tắt</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'tắt vps',
                                        body: "Thao tác tắt các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    // console.log('da den off');
                                    $('.action-vps .off').attr('disabled', false);
                                    $('.action-vps .on').attr('disabled', true);
                                    $('#button-multi-finish').fadeIn();
                                } else if (action == 'delete') {
                                    html = '<span class="text-danger">Đã hủy</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'hủy dịch vụ',
                                        body: "Thao tác hủy các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps').html('');
                                    $('#button-multi-finish').fadeIn();
                                } else if (action == 'auto_refurn') {
                                    // html = '<span class="text-danger">Đã hủy</span>';
                                    // $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'tự động gia hạn',
                                        body: "Thao tác cài đặt tự động gia hạn các dịch vụ VPS thành công.",
                                    })
                                    $('#vps .action-vps .auto_refurn span').removeClass('text-danger');
                                    $('#vps .action-vps .auto_refurn span').addClass('text-success');
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#button-multi-finish').fadeIn();
                                } else if (action == 'off_auto_refurn') {
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'tắt tự động gia hạn',
                                        body: "Thao tác tắt tự động gia hạn các dịch vụ VPS thành công.",
                                    })
                                    $('#vps .action-vps .auto_refurn span').removeClass('text-success');
                                    $('#vps .action-vps .auto_refurn span').addClass('text-danger');
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#button-multi-finish').fadeIn();
                                } else if (action == 'restart') {
                                    html = '<span class="text-danger">Đang khởi động lại</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'khởi động lại',
                                        body: "Thao tác khởi động lại các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps .off').attr('disabled', false);
                                    $('#vps .action-vps .on').attr('disabled', true);
                                    $('#button-multi-finish').fadeIn();
                                    setTimeout(function () {
                                        html = '<span class="text-success">Đã bật</span>';
                                        $('#vps .action-vps .vps-status').html(html);
                                    }, 40000);

                                } else if (action == 'expired') {
                                    // dd(data);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'gia hạn',
                                        body: "Thao tác yêu cầu gia hạn các dịch vụ VPS thành công.",
                                    })
                                    $('#vps .action-vps .expired').fadeOut();
                                    $('#button-terminated').attr('disabled', false);
                                    html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                    $('#notication-invoice').html(html);
                                    $('#button-terminated').fadeOut();
                                    $('#button-multi-finish').fadeIn();
                                    $('#button-multi-finish').text('Thanh toán');
                                    $('#button-multi-finish').attr('data-type', 'expired');
                                    $('#button-multi-finish').attr('data-link', '/order/check-invoices/' + data);
                                } else if (action == 'change_ip') {
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'thay đổi IP',
                                        body: "Thao tác yêu cầu thay đổi IP các dịch vụ VPS thành công.",
                                    })
                                    $('#button-terminated').attr('disabled', false);
                                    html += '<span class="text-center">Yêu cầu thay đổi IP VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình thay đổi IP.</span>';
                                    $('#notication-invoice').html(html);
                                    $('#button-terminated').fadeOut();
                                    $('#button-multi-finish').fadeIn();
                                }
                                break;
                        }
                    }
                    $('tr').removeClass('action-services');
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
                $('.list_action').val('');
            },
            error: function (e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service').fadeOut();
                $('#button-finish').fadeIn();
            }
        })
    });
    // auto gia hạn
    $(document).on('click', '.btnAutoRefurn', function () {
        var type = $(this).attr('data-type');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        if (this.checked) {
            var action = 'auto_refurn';
            var typeCheked = 'checked';
        } else {
            var action = 'off_auto_refurn';
            var typeCheked = 'unchecked';
        }
        id_services.push($(this).attr('data-id'));
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            success: function (data) {
                if (data) {
                    if (data.error) {
                        if (typeCheked == 'checked') {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Bật tự động gia hạn thất bại",
                            })
                        } else {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Tắt tự động gia hạn thất bại",
                            })
                        }
                    } else {
                        if (typeCheked == 'checked') {
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Bật tự động gia hạn thành công",
                            })
                        } else {
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Tắt tự động gia hạn thành công",
                            })
                        }
                    }
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
            },
            error: function (e) {
                console.log(e);
                toastr.warning('Thao tác với dịch vụ VPS thất bại.');
            }
        })
    })
    // Load status từ đang tạo thành on
    loadStatusVPS();

    function loadStatusVPS() {
        setInterval(function () {
            var list_vps_progressing = $('.vps-progressing');
            var ids = [];
            $.each(list_vps_progressing, function (index, value) {
                ids.push($(this).attr('data-id'));
            });
            if (ids.length > 0) {
                // console.log(ids);
                $.ajax({
                    url: '/service/loadStatusVPS',
                    dataType: 'json',
                    data: { ids: ids },
                    success: function (data) {
                        // console.log(data);
                        $.each(data, function (index, vps) {
                            if (vps.status_vps != 'progressing' && vps.status_vps != 'rebuild' && vps.status_vps != 'change_ip') {
                                $(document).Toasts('create', {
                                    class: 'bg-success',
                                    title: 'Action VPS',
                                    subtitle: 'tạo vps',
                                    body: "Tạo VPS " + vps.ip + " thành công ",
                                });
                                $.each(list_vps_progressing, function (index, value) {
                                    // console.log($(this));
                                    if ($(this).attr('data-id') == vps.id) {
                                        $(this).removeClass('vps-progressing');
                                        $(this).text('Đã tạo xong');
                                        $(this).addClass('text-success');
                                    }
                                });
                            }
                        });
                    },
                    error: function (e) {
                        console.log(e);
                    },
                });
            }
        }, 5000);
    }

    $('.button-action-hosting').on('click', function () {
        $('tr').removeClass('action-row');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var domain = $(this).attr('data-domain');
        $('#modal-service').modal('show');
        $('#button-service').fadeIn();

        switch (action) {
            case 'expired':
                $('#modal-service .modal-title').text('Yêu cầu gia hạn dịch vụ Hosting ' + domain);
                $.ajax({
                    url: '/service/request_expired_hosting',
                    data: { list_hosting: id },
                    dataType: 'json',
                    beforeSend: function () {
                        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                        $('#button-service').attr('disabled', true);
                        $('#notication-service').html(html);
                    },
                    success: function (data) {
                        var html = '';
                        if (data.price_override != '') {
                            if (data.expire_billing_cycle == 1) {
                                html += '<div class="form-group">';
                                html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                html += '<div class="mt-3 mb-3">';
                                html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                html += '</select>';
                                html += '</div>';
                                html += '</div>';
                                $('#notication-service').html(html);
                                $('#button-service').attr('data-action', action);
                                $('#button-service').attr('data-type', 'hosting');
                                $('#button-service').attr('disabled', false);
                                $('.select_expired').select2();
                            } else {
                                $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                $('#button-service').attr('disabled', false);
                            }
                        } else {
                            html += '<div class="form-group">';
                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                            html += '<div class="mt-3 mb-3">';
                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                            if (data.total['monthly'] != '0 VNĐ') {
                                html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                            }
                            if (data.total['twomonthly'] != '0 VNĐ') {
                                html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                            }
                            if (data.total['quarterly'] != '0 VNĐ') {
                                html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                            }
                            if (data.total['semi_annually'] != '0 VNĐ') {
                                html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                            }
                            if (data.total['annually'] != '0 VNĐ') {
                                html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                            }
                            if (data.total['biennially'] != '0 VNĐ') {
                                html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                            }
                            if (data.total['triennially'] != '0 VNĐ') {
                                html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                            }
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            $('#notication-service').html(html);
                            $('#button-service').attr('data-action', action);
                            $('#button-service').attr('data-type', 'hosting');
                            $('#button-service').attr('disabled', false);
                            $('.select_expired').select2();
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                        $('#button-service').attr('disabled', false);
                    },
                })
                break;
            case 'suspend':
                $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ Hosting');
                $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ Hosting <b class="text-danger">(domain: ' + domain + ')</b> này không ?</span>');
                break;
            case 'unsuspend':
                $('#modal-service .modal-title').text('Yêu cầu mở lại dịch vụ Hosting');
                $('#notication-service').html('<span>Bạn có muốn mở lại dịch vụ Hosting <b class="text-danger">(domain: ' + domain + ')</b> này không ?</span>');
                break;
            case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Hosting');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Hosting <b class="text-danger">(domain: ' + domain + ')</b> này không ?</span>');
                break;
        }

        $('#button-service').attr('data-id', id);
        $('#button-service').attr('data-action', action);
        $('#button-service').attr('data-domain', domain);
        $('#button-service').attr('data-type', 'hosting');
        $(this).closest('tr').addClass('action-row');
        $('#button-service').fadeIn();
        $('#button-finish').fadeOut();
    });

    $('#button-finish').on('click', function () {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr("data-link");
            window.open(link, '_blank').focus();
        } else {
            $('#modal-service').modal('hide');
        }
    })

    $('#button-service').on('click', function () {
        var action_rebuild = $(this).attr('data-rebuild');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        if (action_rebuild == 1) {
            var id_services = [];
            id_services.push(id);
            var html = '';
            $.ajax({
                url: '/service/check-os',
                dataType: 'json',
                data: { list_id: id_services },
                beforeSend: function () {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-rebuil').attr('disabled', true);
                    $('#form-rebuild').html(html);
                },
                success: function (data) {
                    //called when successful
                    // console.log(data);
                    var html = '';
                    if (!data.error) {
                        html += '<div class="form-group mt-4">';
                        html += '<label for="os_select">Chọn hệ điều hành</label>';
                        html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
                        if (data.type) {
                            $.each(data.os, function (index, value) {
                                html += '<option value="' + value.id + '">' + value.os + '</option>';
                            })
                        } else {
                            // html += '<option value="1">Windows 7</option>';
                            html += '<option value="1">Windows 10 64bit</option>';
                            html += '<option value="2">Windows Server 2012 R2</option>';
                            html += '<option value="3">Windows Server 2016</option>';
                            html += '<option value="4">Linux CentOS 7 64bit</option>';
                            html += '<option value="15">Windows Server 2019</option>';
                            html += '<option value="31">Linux Ubuntu-20.04</option>';
                        }
                        html += '</select>';
                        html += '</div>';
                        $('#notication-service').html(html);
                        $('.select2').select2();
                        $('#button-service').attr('data-rebuild', '2');
                    } else {
                        html += 'VPS ' + data.ip + ' bị giới hạn hệ điều hành khác với các VPS khác. Quý khách vui lòng bỏ chọn VPS này và thực hiện tiếp quy trình cài lại hệ điều hành.';
                        $('.form-rebuild').html(html);
                    }
                },
                error: function (e) {
                    //called when there is an error
                    console.log(e);
                    $('#form-rebuild').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            });
        } else if (action_rebuild == 2) {
            var os = $('#os_select option:selected').text();
            var os_id = $('#os_select option:selected').val();
            var html = '';
            html += '<div class="text-left text-bold mauden mb-2">';
            html += 'Bạn sẽ cài đặt lại các VPS ở trên với thông tin như sau: <br>';
            html += 'Hệ điều hành: ' + os;
            // html += '<br> Security: ' + security_text;
            html += '<input type="hidden" name="os" id="os" value="' + os_id + '">';
            html += '</div>';
            html += '<div class="text-danger text-left mt-2 mb-2">';
            html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại.';
            html += '</div>';
            $('#notication-service').html(html);
            $('#button-service').attr('data-rebuild', '3');
        } else if (action_rebuild == 3) {
            var token = $('meta[name="csrf-token"]').attr('content');
            var id_services = [];
            id_services.push(id);
            var os = $('#os').val();
            $.ajax({
                url: '/services/rebuild_vps',
                type: 'post',
                data: { '_token': token, id: id_services, os: os, security: 0 },
                dataType: 'json',
                beforeSend: function () {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    if (data.success != '') {
                        $(document).Toasts('create', {
                            class: 'bg-info',
                            title: 'VPS',
                            subtitle: 'cài đặt lại vps',
                            body: "Thao tác cài đặt lại các dịch vụ vps thành công.",
                        })
                        $('#button-service').attr('disabled', false);
                        $('#modal-service').modal('hide');
                        $('.action-row-vps .vps-status span').addClass('vps-progressing');
                        $('.action-row-vps .vps-status span').text('Đang cài lại ...');
                        $('.action-row-vps .vps-status span').removeClass('text-success');
                        $('.action-row-vps .vps-status span').removeClass('text-danger');
                        $(".action-row-vps .page-service-action").html('');
                        $(".vps-console").html('');
                        $('tr').removeClass('action-row-vps');
                    } else {
                        $('#notication-service').html('<span class="text-danger">' + data.error + '</span>');
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('#button-service').attr('disabled', false);
                    $('#notication-service').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            })
        } else {
            var billing_cycle = '';
            var type = $(this).attr('data-type');
            var token = $('meta[name="csrf-token"]').attr('content');
            if (type == 'hosting') {
                var domain = $(this).attr('data-domain');
            } else if (type == 'vps') {
                var ip = $(this).attr('data-ip');
            }
            if (action == 'expired') {
                billing_cycle = $('#select_billing_cycle').val();
            }
            $.ajax({
                url: '/services/action',
                type: 'post',
                dataType: 'json',
                data: { '_token': token, id: id, type: type, action: action, billing_cycle: billing_cycle },
                beforeSend: function () {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function (data) {
                    // console.log(data);
                    var html = '';
                    if (data) {
                        if (data.error) {
                            if (data.error == 1) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 2) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 3) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 4) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS do Admin off. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 5) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 400) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                                })
                            } else if (data.error == 2223) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 2224) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 9998) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'Không thành công',
                                    body: "Yêu cầu thất bại. Dịch vụ được chọn không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.",
                                })
                            }
                            $('#modal-service').modal('hide');
                            $('#button-service').attr('disabled', false);
                        } else {
                            switch (type) {
                                case 'hosting':
                                    if (action == 'expired') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'Hosting',
                                            subtitle: 'gia hạn',
                                            body: "Yêu cầu gia hạn hosting thành công",
                                        });
                                        $('.action-row .expired').fadeOut();
                                        $('#button-service').attr('disabled', false);
                                        html += '<span class="text-center">Yêu cầu gia hạn hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                        $('#notication-service').html(html);
                                        $('#button-service').fadeOut();
                                        $('#button-finish').fadeIn();
                                        $('tr').removeClass('action-row');
                                    } else if (action == 'suspend') {
                                        $(document).Toasts('create', {
                                            class: 'bg-warning',
                                            title: 'Hosting',
                                            subtitle: 'suspend',
                                            body: "Tắt hosting thành công",
                                        });
                                        $('.action-row .suspend').attr('disabled', true);
                                        $('.action-row .unsuspend').attr('disabled', false);
                                        $('.action-row .hosting-status').html('<span class="text-danger">Đã tắt</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row');
                                    } else if (action == 'unsuspend') {
                                        $(document).Toasts('create', {
                                            class: 'bg-info',
                                            title: 'Hosting',
                                            subtitle: 'unsuspend',
                                            body: "Mở lại hosting thành công",
                                        });
                                        $('.action-row .suspend').attr('disabled', false);
                                        $('.action-row .unsuspend').attr('disabled', true);
                                        $('.action-row .hosting-status').html('<span class="text-success">Đã bật</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row');
                                    } else if (action == 'terminated') {
                                        $(document).Toasts('create', {
                                            class: 'bg-danger',
                                            title: 'Hosting',
                                            subtitle: 'terminated',
                                            body: "Hủy dịch vụ hosting thành công",
                                        });
                                        $('.action-row .terminated').fadeOut();
                                        $('.action-row .hosting-status').html('<span class="text-danger">Đã hủy</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row');
                                    }
                                    break;
                                case 'vps':
                                    if (action == 'expired') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS',
                                            subtitle: 'gia hạn',
                                            body: "Yêu cầu gia hạn vps thành công",
                                        });
                                        $('.action-row-vps .expired').fadeOut();
                                        $('#button-service').attr('disabled', false);
                                        html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                        $('#notication-service').html(html);
                                        $('#button-service').fadeOut();
                                        $('#button-finish').fadeIn();
                                        $('#button-finish').attr('data-type', 'expired');
                                        $('#button-finish').html('Thanh toán');
                                        $('#button-finish').attr('data-link', '/order/check-invoices/' + data);
                                        $('tr').removeClass('action-row-vps');
                                    } else if (action == 'off') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS',
                                            subtitle: 'off',
                                            body: "Yêu cầu tắt VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', false);
                                        $('.action-row-vps .on').attr('disabled', true);
                                        $('.action-row-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    } else if (action == 'on') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS',
                                            subtitle: 'on',
                                            body: "Yêu cầu bật VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', true);
                                        $('.action-row-vps .on').attr('disabled', false);
                                        $('.action-row-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    } else if (action == 'restart') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS',
                                            subtitle: 'on',
                                            body: "Yêu cầu khởi động lại VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', false);
                                        $('.action-row-vps .on').attr('disabled', true);
                                        $('.action-row-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                        setTimeout(function () {
                                            html = '<span class="text-danger">Đã bật</span>';
                                            $('.action-row-vps .vps-status').html(html);
                                        }, 40000);
                                    } else if (action == 'terminated') {
                                        $(document).Toasts('create', {
                                            class: 'bg-danger',
                                            title: 'VPS',
                                            subtitle: 'terminated',
                                            body: "Hủy dịch vụ VPS thành công",
                                        });
                                        $('.action-row-vps .terminated').fadeOut();
                                        $('.action-row-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    }
                                    break;
                            }
                            // $('#modal-service').modal('hide');
                            // $('#button-service').attr('disabled', false);
                        }
                    } else {
                        $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                        $('#button-service').attr('disabled', false);
                    }
                },
                error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                    $('#button-service').attr('disabled', false);
                }
            });
        }
    });

    $(document).on('click', '.button-action-vps', function () {
        $('tr').removeClass('action-row-vps');
        var action = $(this).attr('data-action');
        if (action == 'change_user') {
            var id = $(this).attr('data-id');
            var list_vps = [];
            list_vps.push(id);
            window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
        } else {
            $('#button-service').attr('data-rebuild', '0');
            var id = $(this).attr('data-id');
            var ip = $(this).attr('data-ip');
            $('#button-service').fadeIn();
            $('#modal-service').modal('show');
            switch (action) {
                case 'expired':
                    $('#modal-service .modal-title').text('Yêu cầu gia hạn VPS');
                    $('#notication-service').html('<span>Bạn có muốn gia hạn vps <b class="text-danger">(ip: ' + ip + ')</b> này không </span>');
                    $.ajax({
                        url: '/service/request_expired_vps',
                        data: { list_vps: id },
                        dataType: 'json',
                        beforeSend: function () {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button-service').attr('disabled', true);
                            $('#notication-service').html(html);
                        },
                        success: function (data) {
                            var html = '';
                            if (data.error == 9998) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else if (data.error == 1) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else {
                                // console.log(data);
                                if (data.expire_billing_cycle == true) {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                } else {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                    html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                    html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                    html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                    html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                    html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                    html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                }
                            }
                        },
                        error: function (e) {
                            console.log(e);
                            $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button-terminated').attr('disabled', false);
                        },
                    });
                    break;
                case 'off':
                    $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'on':
                    $('#modal-service .modal-title').text('Yêu cầu bật lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn bật lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'restart':
                    $('#modal-service .modal-title').text('Yêu cầu khởi động lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn khởi động lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'terminated':
                    $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'rebuild':
                    $('#modal-service .modal-title').text('Yêu cầu cài đặt lại VPS');
                    $('#notication-service').html('<span>Bạn có muốn cài đặt lại VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    $('#button-service').attr('data-rebuild', '1');
                    break;
            }

            $('#button-service').attr('data-id', id);
            $('#button-service').attr('data-action', action);
            $('#button-service').attr('data-ip', ip);
            $('#button-service').attr('data-type', 'vps');
            $(this).closest('tr').addClass('action-row-vps');
            $('#button-service').fadeIn();
            $('#button-finish').fadeOut();
            $('#button-finish').text('Hoàn thành');
            $('#button-finish').attr('data-type', 'action');
        }
    });

    // updateCusomter
    $(document).on('click', '.button_edit_customer', function functionName(event) {
        event.preventDefault();
        $(this).closest('td').addClass('choose-update-customer');
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/services/loadCusomter',
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.choose-update-customer').html(html);
            },
            success: function (data) {
                if (data != '') {
                    var token = $('meta[name="csrf-token"]').attr('content');
                    var html = '';
                    html += '<form id="form_update_customer">';
                    html += '<input type="hidden" name="_token" value="' + token + '">';
                    html += '<input type="hidden" name="vps_id" value="' + id + '">';
                    html += '<div class="form-group">';
                    html += '<select class="form-control select2 customer_select" name="makh" style="width=100%">';
                    html += '<option value="">Cho tôi</option>';
                    $.each(data, function (index, customer) {
                        if (customer.type_customer == 0) {
                            html += '<option value="' + customer.ma_customer + '">' + customer.customer_name + '</option>';
                        } else {
                            html += '<option value="' + customer.ma_customer + '">' + customer.customer_tc_name + '</option>';
                        }
                    })
                    html += '</select>';
                    html += '</div>';
                    html += '<div class="text-right">';
                    html += '<button type="button" name="button" class="btn mr-1 btn-primary button_submit_update_customer">Thay đổi</button>';
                    html += '</div>';
                    html += '</form>';
                    $('.choose-update-customer').html(html);
                    $('.select2').select2();
                } else {
                    $('.choose-update-customer').html('<span class="text-center">Không có dữ liệu khách hàng.</span>');
                }
                $('td').removeClass('choose-update-customer');
            },
            error: function (e) {
                console.log(e);
                $('.choose-update-customer').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                $('td').removeClass('choose-update-customer');
            }
        });
    })

    $(document).on('click', '.button_submit_update_customer', function () {
        var form = $(this).closest('#form_update_customer').serialize();
        var name = $(".customer_select option:selected").text();
        var makh = $(".customer_select option:selected").val();
        $(this).closest('td').addClass('choose-update-customer');
        $.ajax({
            url: '/services/updateCusomter',
            data: form,
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.choose-update-customer').html(html);
            },
            success: function (data) {
                if (data != '') {
                    var html = '';
                    html += makh + ' - ' + name;
                    html += '<span>';
                    html += '<a href="#" class="text-secondary ml-2 button_edit_customer" data-id="' + data.id + '" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a>';
                    html += '</span>';
                    $('.choose-update-customer').html(html);
                } else {
                    $('.choose-update-customer').html('<span class="text-center">Cập nhật không thành công.</span>');
                }
                $('td').removeClass('choose-update-customer');
            },
            error: function (e) {
                console.log(e);
                $('.choose-update-customer').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                $('td').removeClass('choose-update-customer');
            }
        });
    });
    // ./updateCusomter

    // update description
    $(document).on('click', '.button_edit_description', function functionName(event) {
        event.preventDefault();
        $('td').removeClass('choose-update-description');
        $(this).closest('td').addClass('choose-update-description');
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        var text = $('.choose-update-description .text-description').text();
        // console.log(text);
        var html = '';
        html += '<form id="form_update_description">';
        html += '<input type="hidden" name="_token" value="' + token + '">';
        html += '<input type="hidden" name="vps_id" value="' + id + '">';
        html += '<div class="form-group">';
        html += '<textarea rows="5" cols="12" maxlength="40" class="form-control description_area" name="description">' + text.trim() + '</textarea>'
        html += '</div>';
        html += '<div class="text-right mt-2">';
        html += '<button type="button" name="button" class="btn mr-1 btn-primary button_submit_update_description">Thay đổi</button>';
        html += '</div>';
        html += '</form>';
        $('.choose-update-description').html(html);
    });

    $(document).on('click', '.button_submit_update_description', function () {
        var form = $(this).closest('#form_update_description').serialize();
        var description = $(".description_area").val();
        $.ajax({
            url: '/services/updateDescription',
            data: form,
            type: 'post',
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.choose-update-description').html(html);
            },
            success: function (data) {
                if (data != '') {
                    var html = '';
                    html += '<span class="text-description">';
                    html += description;
                    html += '</span>';
                    html += '<span>';
                    html += '<a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + data.id + '"><i class="fas fa-edit"></i></a>';
                    html += '</span>';
                    $('.choose-update-description').html(html);
                } else {
                    $('.choose-update-description').html('<span class="text-center">Cập nhật ghi chú không thành công.</span>');
                }
                $('td').removeClass('choose-update-description');
            },
            error: function (e) {
                console.log(e);
                $('.choose-update-description').html('<span class="text-center">Lỗi truy xuất VPS.</span>');
                $('td').removeClass('choose-update-description');
            }
        });
    });

    $(document).on('click', '.button-action-server', function () {
        $('tr').removeClass('action-row-server');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $('#button_server_service').fadeIn();
        $('#button-finish').fadeOut();
        $('#modal-service').modal('show');
        $('#button-finish').text('Hoàn thành');
        switch (action) {
            case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Server');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Server <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                break;
            case 'expired':
                $('#modal-service .modal-title').text('Yêu cầu gia hạn Server');
                $('#notication-service').html('<span>Bạn có muốn gia hạn Server <b class="text-danger">(ip: ' + ip + ')</b> này không </span>');
                $.ajax({
                    url: '/service/request_expired_server',
                    data: { id: id },
                    dataType: 'json',
                    beforeSend: function () {
                        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                        $('#button_server_service').attr('disabled', true);
                        $('#notication-service').html(html);
                    },
                    success: function (data) {
                        var html = '';
                        if (data.error == 1) {
                            $('#button_server_service').html('<span class="text-center text-danger">Dịch vụ thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.</span>');
                            $('#button-service').attr('disabled', false);
                        } else {
                            // console.log(data);
                            html += '<div class="form-group">';
                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                            html += '<div class="mt-3 mb-3">';
                            html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                            if (data.total['monthly'] != '0 VNĐ') {
                                html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                            }
                            if (data.total['twomonthly'] != '0 VNĐ') {
                                html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                            }
                            if (data.total['quarterly'] != '0 VNĐ') {
                                html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                            }
                            if (data.total['semi_annually'] != '0 VNĐ') {
                                html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                            }
                            if (data.total['annually'] != '0 VNĐ') {
                                html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                            }
                            if (data.total['biennially'] != '0 VNĐ') {
                                html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                            }
                            if (data.total['triennially'] != '0 VNĐ') {
                                html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                            }
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            $('#notication-service').html(html);
                            $('#button_server_service').attr('disabled', false);
                            $('.select2').select2();
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                        $('#button_server_service').attr('disabled', false);
                    },
                });
                break;

        }
        $('#button_server_service').attr('data-id', id);
        $('#button_server_service').attr('data-action', action);
        $('#button_server_service').attr('data-ip', ip);
        $(this).closest('tr').addClass('action-row-server');
    })

    $('#button_server_service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var ip = $(this).attr('data-ip');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/server/action-server',
            type: 'post',
            dataType: 'json',
            data: { '_token': token, id: id, action: action, billing_cycle: billing_cycle },
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_server_service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error == 1) {
                        $('#notication-service').html('<span class="text-danger">Dịch vụ Server này không thuộc quyền quản lý của bạn</span>');
                        $('#button_server_service').attr('disabled', false);
                    } else if (data.error == 4) {
                        $('#notication-service').html('<span class="text-danger">Hủy dịch vụ Server thất bại!</span>');
                        $('#button_server_service').attr('disabled', false);
                    } else if (data.error == 3) {
                        $('#notication-service').html('<span class="text-danger">Gia hạn dịch vụ Server thất bại!</span>');
                        $('#button_server_service').attr('disabled', false);
                    } else {
                        if (data.type == 4) {
                            $('.action-row-server .terminated').fadeOut();
                            $('.action-row-server .server-status').html('<span class="text-danger">Đã hủy</span>');
                            $('#button_server_service').attr('disabled', false);
                            $('tr').removeClass('action-row-server');
                            $('#notication-service').html('<span class="text-danger">Yêu cầu hủy Server thành công</span>');
                            $('#button_server_service').fadeOut();
                            $('#button-finish').fadeIn();
                        } else if (data.type == 3) {
                            var html = '<span class="text-center">Yêu cầu gia hạn Server thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                            $('#button_server_service').attr('disabled', false);
                            $('tr').removeClass('action-row-server');
                            $('#notication-service').html(html);
                            $('#button_server_service').fadeOut();
                            $('#button-finish').fadeIn();
                            $('#button-finish').text('Thanh toán');
                            $('#button-finish').attr('data-type', 'expired');
                            $('#button-finish').attr('data-link', '/order/check-invoices/' + data.invoice_id);
                        }
                    }
                    // $('#modal-service').modal('hide');
                    // $('#button-service').attr('disabled', false);
                } else {
                    $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                    $('#button_server_service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button-service').attr('disabled', false);
            }
        });
    });

    $('#button-finish').on('click', function () {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr("data-link");
            window.open(link, '_blank').focus();
            // window.location.href = link;
        } else {
            $('#modal-service').modal('hide');
        }
    })

    $('.list_action_server_use').on('change', function () {
        // var action = $(this).attr('data-type');
        var action = $(this).val();
        var checked = $('.server_checkbox:checked');
        $('#button_server_terminated').fadeIn();
        $('#button_server_rebuil').fadeOut();
        $('#button_server_rebuil').text('Hoàn thành');
        if (checked.length > 0) {
            $('#modal-services').modal('show');
            switch (action) {
                case 'on':
                    $('.modal-title').text('Yêu cầu bật dịch vụ Hosting');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn bật các hosting này không?</span>');
                    $('#button-terminated').attr('data-action', action);
                    $('#button-terminated').attr('data-type', 'hosting');
                    break;
                case 'off':
                    $('.modal-title').text('Yêu cầu tắt dịch vụ Hosting');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn tắt các hosting này không?</span>');
                    $('#button-terminated').attr('data-action', action);
                    $('#button-terminated').attr('data-type', 'hosting');
                    break;
                case 'delete':
                    $('.modal-title').text('Yêu cầu hủy dịch vụ Server');
                    $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ Server này không?</span>');
                    $('#button_server_terminated').attr('data-action', action);
                    break;
                case 'expired':
                    $('.modal-title').text('Yêu cầu gia hạn dịch vụ Server');
                    var list_id = [];
                    $.each(checked, function (index, value) {
                        list_id.push($(this).val());
                    })
                    $.ajax({
                        url: '/service/request_expired_list_server',
                        data: { list_id: list_id },
                        dataType: 'json',
                        beforeSend: function () {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button_server_terminated').attr('disabled', true);
                            $('#notication-invoice').html(html);
                        },
                        success: function (data) {
                            var html = '';
                            if (data.error == 1) {
                                $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button_server_terminated').attr('disabled', false);
                                $('#button_server_terminated').fadeOut();
                            } else {
                                html += '<div class="form-group">';
                                html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                html += '<div class="mt-3 mb-3">';
                                html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                if (data.total['monthly'] != '0 VNĐ') {
                                    html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                }
                                if (data.total['twomonthly'] != '0 VNĐ') {
                                    html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                }
                                if (data.total['quarterly'] != '0 VNĐ') {
                                    html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                }
                                if (data.total['semi_annually'] != '0 VNĐ') {
                                    html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                }
                                if (data.total['annually'] != '0 VNĐ') {
                                    html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                }
                                if (data.total['biennially'] != '0 VNĐ') {
                                    html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                }
                                if (data.total['triennially'] != '0 VNĐ') {
                                    html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                }
                                html += '</select>';
                                html += '</div>';
                                html += '</div>';
                                $('#notication-invoice').html(html);
                                $('#button_server_terminated').attr('data-action', action);
                                $('#button_server_terminated').attr('disabled', false);
                                $('.select_expired').select2();
                            }
                        },
                        error: function (e) {
                            console.log(e);
                            $('#notication-invoice').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button_server_terminated').attr('disabled', false);
                        },
                    })
                    break;
            }
        } else {
            alert('Chọn ít nhất một Server');
        }
    })
    // modal action của hosting
    $('#button_server_terminated').on('click', function () {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        var checked = $('.server_checkbox:checked');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function (index, value) {
            id_services.push($(this).val());
        })
        // console.log(action);
        $.ajax({
            url: '/services/action_server_services',
            type: 'post',
            data: { '_token': token, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_server_terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');
                        if (data.error == 1) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                            $('#button_server_terminated').attr('disabled', false);
                            $('#button_server_terminated').fadeOut();
                            $('#button_server_rebuil').fadeIn();
                        } else if (data.error == 4) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Hủy dịch vụ Server thất bại.</span>');
                            $('#button_server_terminated').attr('disabled', false);
                            $('#button_server_terminated').fadeOut();
                            $('#button_server_rebuil').fadeIn();
                        } else if (data.error == 3) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Yêu cầu gia hạn dịch vụ Server thất bại.</span>');
                            $('#button_server_terminated').attr('disabled', false);
                            $('#button_server_terminated').fadeOut();
                            $('#button_server_rebuil').fadeIn();
                        }
                        $('#button_server_terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        if (data.type == 4) {
                            html = '<span class="text-danger">Đã hủy</span>';
                            $('.action').fadeOut();
                            $('#button_server_terminated').attr('disabled', false);
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu hủy Server thành công</span>');
                            $('#button_server_terminated').fadeOut();
                            $('#button_server_rebuil').fadeIn();
                        } else if (data.type == 3) {
                            // $('.action .terminated').fadeOut();
                            $('#button_server_terminated').attr('disabled', false);
                            $('#notication-invoice').html('Yêu cầu gia hạn Server thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.');
                            $('#button_server_terminated').fadeOut();
                            $('#button_server_rebuil').fadeIn();
                            $('#button_server_rebuil').text('Thanh toán');
                            $('#button_server_rebuil').attr('data-type', 'expired');
                            $('#button_server_rebuil').attr('data-link', '/order/check-invoices/' + data.invoice_id);
                        }
                    }
                    $('.list_action_server_use').val('');
                    $('tr').removeClass('action');
                } else {
                    $('#notication-invoice').html('<span class="text-danger">Thao tác với dịch vụ Server thất bại.</span>');
                    $('#button_server_terminated').attr('disabled', false);
                    $('#button_server_terminated').fadeOut();
                    $('#button_server_rebuil').fadeIn();
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
            },
            error: function (e) {
                console.log(e);
                $('#button_server_terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button_server_terminated').fadeOut();
                $('#button_server_rebuil').fadeIn();
            }
        })
    });


    // bấm vào thanh toán để chuyển link
    $('#button_server_rebuil').on('click', function () {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr('data-link');
            window.location.href = link;
        }
    })

    var $serverCheckbox = $('.server_checkbox');
    var lastChecked = null;

    $(document).on('click', '.checkbox_server_all', function () {
        if ($(this).is(':checked')) {
            $('.server_checkbox').prop('checked', this.checked);
            $('tbody tr').addClass('action');
        } else {
            $('.server_checkbox').prop('checked', this.checked);
            $('tbody tr').removeClass('action');
        }
        var checked = $('.server_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    })

    // Lưu các checkbox server
    $(document).on('click', '.server_checkbox', function (e) {
        var checked = $('.server_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action');
        } else {
            $(this).closest('tr').removeClass('action');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log('da click 2');
            var start = $serverCheckbox.index(this);
            var end = $serverCheckbox.index(lastChecked);
            $serverCheckbox.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($serverCheckbox, function () {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action');
                } else {
                    $(this).closest('tr').removeClass('action');
                }
            })
        }
        lastChecked = this;
        var checked = $('.server_checkbox:checked');
        $('.qtt-checkbox').text(checked.length);
    });

    // Click vào các button action của Vps

    $('.server_classic').on('change', function () {
        list_server_use();
    })

    $('.sort_server').on('click', function () {
        var vps_sort = $('.sort_server').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_server').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_server i').removeClass('fa-sort-down');
            $('.sort_server i').removeClass('fa-sort');
            $('.sort_server i').addClass('fa-sort-up');
        } else {
            $('.sort_server').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_server i').addClass('fa-sort-down');
            $('.sort_server i').removeClass('fa-sort');
            $('.sort_server i').removeClass('fa-sort-up');
        }
        list_server_use();
    })

    $('#btn_server_use_search_multi').on('click', function () {
        list_server_use();
    })

    function list_server_use() {
        var qtt = $('.server_classic').val();
        var vps_sort = $('.sort_server').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'use', multi_q: multi_q, sort: vps_sort },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_use(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server đang sử dụng trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    $(document).on('click', '.pagination_server_use a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('.server_classic').val();
        var vps_sort = $('.sort_server').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'use', multi_q: multi_q, sort: vps_sort, page: page },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_use(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server đang sử dụng trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function screent_server_use(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="server_checkbox"></td>';
                // ip
                console.log(server.id);
                if (server.id) {
                    html += '<td><a href="/service/server/detail/' + server.id + '?type=server">' + server.ip + '</a> <br>';
                    html += '<a href="/service/server/detail/' + server.id + '?type=server">' + server.ip2 + '</a> <br>';
                    if (server.expired == true) {
                        html += ' - <span class="text-danger">Hết hạn</span>';
                    } else if (server.isExpire == true) {
                        html += ' - <span class="text-danger">Gần hết hạn</span>';
                    }
                    html += '</td>';
                } else {
                    html += '<td class="text-danger">Đang cài đặt</td>';
                }
                // cau hinh
                html += '<td>' + server.text_server_config + '</td>';
                // location
                html += '<td>' + server.location + '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (server.isExpire == true || server.expired == true) {
                    html += '<span class="text-danger">' + server.next_due_date + ' - ' + server.text_day + '<span>';
                } else {
                    html += '<span>' + server.next_due_date + ' - ' + server.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + server.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + server.amount + '</b></td>';
                // ghi chu
                html += '<td>';
                if (server.description != null) {
                    html += '<span class="text-description">';
                    html += server.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + server.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // trang thai
                html += '<td class="server-status">' + server.text_status_server + '</td>';
                // hành động
                html += '<td class="page-service-action">';
                if (server.status_server != 'suspend') {
                    html += '<button type="button" class="btn btn-sm mr-1 btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fas fa-plus-circle"></i></button>';
                    html += '<button class="btn btn-sm mr-1 btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                }
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Server này không tồn tại hoặc không thuộc quyền quản lý của quý khách.</td>';
                html += '</tr>';
            }
        })
        $('.list_server tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('.list_server tfoot').html(html_page);
        $serverCheckbox = $('.server_checkbox');
    }

    $('.server_on_classic').on('change', function () {
        list_server_on();
    })

    $('.sort_server_on').on('click', function () {
        var vps_sort = $('.sort_server_on').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_server_on').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_server_on i').removeClass('fa-sort-down');
            $('.sort_server_on i').removeClass('fa-sort');
            $('.sort_server_on i').addClass('fa-sort-up');
        } else {
            $('.sort_server_on').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_server_on i').addClass('fa-sort-down');
            $('.sort_server_on i').removeClass('fa-sort');
            $('.sort_server_on i').removeClass('fa-sort-up');
        }
        list_server_on();
    })

    $('#btn_server_on_search_multi').on('click', function () {
        list_server_on();
    })

    function list_server_on() {
        var qtt = $('.server_on_classic').val();
        var vps_sort = $('.sort_server_on').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'on', multi_q: multi_q, sort: vps_sort },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_on(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    function screent_server_on(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="server_checkbox"></td>';
                // ip
                if (server.id) {
                    html += '<td><a href="/service/server/detail/' + server.id + '?type=server">' + server.ip + '</a> <br>';
                    html += '<a href="/service/server/detail/' + server.id + '?type=server">' + server.ip2 + '</a> <br>';
                    if (server.expired == true) {
                        html += ' - <span class="text-danger">Hết hạn</span>';
                    } else if (server.isExpire == true) {
                        html += ' - <span class="text-danger">Gần hết hạn</span>';
                    }
                    html += '</td>';
                } else {
                    html += '<td class="text-danger">Đang cài đặt</td>';
                }
                // cau hinh
                html += '<td>' + server.text_server_config + '</td>';
                // location
                html += '<td>' + server.location + '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (server.isExpire == true || server.expired == true) {
                    html += '<span class="text-danger">' + server.next_due_date + ' - ' + server.text_day + '<span>';
                } else {
                    html += '<span>' + server.next_due_date + ' - ' + server.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + server.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + server.amount + '</b></td>';
                // ghi chu
                html += '<td>';
                if (server.description != null) {
                    html += '<span class="text-description">';
                    html += server.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + server.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // trang thai
                html += '<td class="server-status">' + server.text_status_server + '</td>';
                // hành động
                html += '<td class="page-service-action">';
                if (server.status_server != 'suspend') {
                    html += '<button type="button" class="btn btn-sm mr-1 btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fas fa-plus-circle"></i></button>';
                    if (server.status_server != 'cancel') {
                        html += '<button class="btn btn-sm mr-1 btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                }
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Server này không tồn tại hoặc không thuộc quyền quản lý của quý khách.</td>';
                html += '</tr>';
            }
        })
        $('.list_server tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('.list_server tfoot').html(html_page);
    }

    $(document).on('click', '.pagination_server_on a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('.server_on_classic').val();
        var vps_sort = $('.sort_server_on').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'on', multi_q: multi_q, sort: vps_sort, page: page },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_on(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('.server_nearly_classic').on('change', function () {
        list_server_nearly();
    })

    $('.sort_server_nearly').on('click', function () {
        var vps_sort = $('.sort_server_nearly').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_server_nearly').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_server_nearly i').removeClass('fa-sort-down');
            $('.sort_server_nearly i').removeClass('fa-sort');
            $('.sort_server_nearly i').addClass('fa-sort-up');
        } else {
            $('.sort_server_nearly').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_server_nearly i').addClass('fa-sort-down');
            $('.sort_server_nearly i').removeClass('fa-sort');
            $('.sort_server_nearly i').removeClass('fa-sort-up');
        }
        list_server_nearly();
    })

    $('#btn_server_nearly_search_multi').on('click', function () {
        list_server_nearly();
    })

    $(document).on('click', '.pagination_server_nearly a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('.server_nearly_classic').val();
        var vps_sort = $('.sort_server_nearly').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'nearly', multi_q: multi_q, sort: vps_sort, page: page },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_nearly(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function list_server_nearly() {
        var qtt = $('.server_nearly_classic').val();
        var vps_sort = $('.sort_server_nearly').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'nearly', multi_q: multi_q, sort: vps_sort },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_nearly(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    function screent_server_nearly(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="server_checkbox"></td>';
                // ip
                if (server.id) {
                    html += '<td><a href="/service/server/detail/' + server.id + '?type=server">' + server.ip + '</a> <br>';
                    html += '<a href="/service/server/detail/' + server.id + '?type=server">' + server.ip2 + '</a> <br>';
                    if (server.expired == true) {
                        html += ' - <span class="text-danger">Hết hạn</span>';
                    } else if (server.isExpire == true) {
                        html += ' - <span class="text-danger">Gần hết hạn</span>';
                    }
                    html += '</td>';
                } else {
                    html += '<td class="text-danger">Đang cài đặt</td>';
                }
                // cau hinh
                html += '<td>' + server.text_server_config + '</td>';
                // location
                html += '<td>' + server.location + '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (server.isExpire == true || server.expired == true) {
                    html += '<span class="text-danger">' + server.next_due_date + ' - ' + server.text_day + '<span>';
                } else {
                    html += '<span>' + server.next_due_date + ' - ' + server.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + server.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + server.amount + '</b></td>';
                // ghi chu
                html += '<td>';
                if (server.description != null) {
                    html += '<span class="text-description">';
                    html += server.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + server.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // trang thai
                html += '<td class="server-status">' + server.text_status_server + '</td>';
                // hành động
                html += '<td class="page-service-action">';
                if (server.status_server != 'suspend') {
                    html += '<button type="button" class="btn btn-sm mr-1 btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fas fa-plus-circle"></i></button>';
                    html += '<button class="btn btn-sm mr-1 btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                }
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Server này không tồn tại hoặc không thuộc quyền quản lý của quý khách.</td>';
                html += '</tr>';
            }
        })
        $('tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

    $('.server_cancel_classic').on('change', function () {
        list_server_cancel();
    })

    $('.sort_server_cancel').on('click', function () {
        var vps_sort = $('.sort_server_cancel').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_server_cancel').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_server_cancel i').removeClass('fa-sort-down');
            $('.sort_server_cancel i').removeClass('fa-sort');
            $('.sort_server_cancel i').addClass('fa-sort-up');
        } else {
            $('.sort_server_cancel').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_server_cancel i').addClass('fa-sort-down');
            $('.sort_server_cancel i').removeClass('fa-sort');
            $('.sort_server_cancel i').removeClass('fa-sort-up');
        }
        list_server_cancel();
    })

    $('#btn_server_cancel_search_multi').on('click', function () {
        list_server_cancel();
    })

    function list_server_cancel() {
        var qtt = $('.server_cancel_classic').val();
        var vps_sort = $('.sort_server_cancel').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'cancel', multi_q: multi_q, sort: vps_sort },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_cancel(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    function screent_server_cancel(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                // html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="server_checkbox"></td>';
                // ip
                if (server.id) {
                    html += '<td><a href="/service/server/detail/' + server.id + '?type=server">' + server.ip + '</a> <br>';
                    html += '<a href="/service/server/detail/' + server.id + '?type=server">' + server.ip2 + '</a> <br>';
                    html += '</td>';
                } else {
                    html += '<td class="text-danger">Đang cài đặt</td>';
                }
                // cau hinh
                html += '<td>' + server.text_server_config + '</td>';
                // location
                html += '<td>' + server.location + '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span>' + server.next_due_date + '<span>';
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + server.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + server.amount + '</b></td>';
                // ghi chu
                html += '<td>';
                if (server.description != null) {
                    html += '<span class="text-description">';
                    html += server.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + server.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // trang thai
                html += '<td class="server-status">' + server.text_status_server + '</td>';
                // hành động
                // html += '<td class="page-service-action">';
                // if (server.status_server != 'suspend') {
                //     html += '<button type="button" class="btn mr-1 btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fas fa-plus-circle"></i></button>';
                //     if (server.status_server != 'expire') {
                //         html += '<button class="btn mr-1 btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                //     }
                // }
                // html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Server này không tồn tại hoặc không thuộc quyền quản lý của quý khách.</td>';
                html += '</tr>';
            }
        })
        $('tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

    $(document).on('click', '.pagination_server_cancel a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_server_cancel').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'cancel', multi_q: multi_q, sort: vps_sort, page: page },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_cancel(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('.server_all_classic').on('change', function () {
        list_server_all();
    })

    $('.sort_server_all').on('click', function () {
        var vps_sort = $('.sort_server_all').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_server_all').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_server_all i').removeClass('fa-sort-down');
            $('.sort_server_all i').removeClass('fa-sort');
            $('.sort_server_all i').addClass('fa-sort-up');
        } else {
            $('.sort_server_all').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_server_all i').addClass('fa-sort-down');
            $('.sort_server_all i').removeClass('fa-sort');
            $('.sort_server_all i').removeClass('fa-sort-up');
        }
        list_server_all();
    })

    $('#btn_server_all_search_multi').on('click', function () {
        list_server_all();
    })

    function list_server_all() {
        var qtt = $('.server_all_classic').val();
        let text = $("#form_search_multi").val();
        var vps_sort = $('.sort_server_all').attr('data-sort');
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'all', multi_q: multi_q, sort: vps_sort },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_all(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    $(document).on('click', '.pagination_server_all a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('.server_all_classic').val();
        var vps_sort = $('.sort_server_all').attr('data-sort');
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if (text != "") {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        action = $('#service_action').val('select');
        $.ajax({
            url: '/services/server/select_server',
            data: { qtt: qtt, action: 'all', multi_q: multi_q, sort: vps_sort, page: page },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_server_all(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Quý khách không có dịch vụ Server còn hạn trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function screent_server_all(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                // html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="server_checkbox"></td>';
                // ip
                if (server.id) {
                    html += '<td><a href="/service/server/detail/' + server.id + '?type=server">' + server.ip + '</a> <br>';
                    html += '<a href="/service/server/detail/' + server.id + '?type=server">' + server.ip2 + '</a> <br>';
                    html += '</td>';
                } else {
                    html += '<td class="text-danger">Đang cài đặt</td>';
                }
                // cau hinh
                html += '<td>' + server.text_server_config + '</td>';
                // location
                html += '<td>' + server.location + '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span>' + server.next_due_date + '<span>';
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + server.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + server.amount + '</b></td>';
                // ghi chu
                html += '<td>';
                if (server.description != null) {
                    html += '<span class="text-description">';
                    html += server.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + server.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // trang thai
                html += '<td class="server-status">' + server.text_status_server + '</td>';
                // hành động
                // html += '<td class="page-service-action">';
                // if (server.status_server != 'suspend') {
                //     html += '<button type="button" class="btn mr-1 btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fas fa-plus-circle"></i></button>';
                //     if (server.status_server != 'expire') {
                //         html += '<button class="btn mr-1 btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + server.id + '" data-ip="' + server.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                //     }
                // }
                // html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Server này không tồn tại hoặc không thuộc quyền quản lý của quý khách.</td>';
                html += '</tr>';
            }
        })
        $('tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }
    /**
     * Email Hosting
     */
    $(document).on('click', '.button-action-email_hosting', function () {
        $('tr').removeClass('action-row-email_hosting');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var domain = $(this).attr('data-ip');
        $('#button_email_hosting_service').val(id);
        $('#button_email_hosting_service').fadeIn();
        $('#button-finish').fadeOut();
        $('#modal-service').modal('show');
        switch (action) {
            case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Email Hosting');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Email Hosting <b class="text-danger">(domain: ' + domain + ')</b> này không ?</span>');
                break;
            case 'expired':
                $('#modal-service .modal-title').text('Yêu cầu gia hạn Email Hosting');
                $('#notication-service').html('<span>Bạn có muốn gia hạn Email Hosting <b class="text-danger">(domain: ' + domain + ')</b> này không </span>');
                $.ajax({
                    url: '/service/request_expired_email_hosting',
                    data: { id: id },
                    dataType: 'json',
                    beforeSend: function () {
                        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                        $('#button_email_hosting_service').attr('disabled', true);
                        $('#notication-service').html(html);
                    },
                    success: function (data) {
                        // console.log(data);
                        var html = '';
                        if (data.error == 1) {
                            $('#notication-service').html('<span class="text-center text-danger">Dịch vụ thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.</span>');
                            $('#button_email_hosting_service').attr('disabled', false);
                        } else {
                            // console.log(data);
                            html += '<div class="form-group">';
                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                            html += '<div class="mt-3 mb-3">';
                            html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                            if (data.total['monthly'] != "0 VNĐ") {
                                html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                            }
                            if (data.total['twomonthly'] != "0 VNĐ") {
                                html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                            }
                            if (data.total['quarterly'] != "0 VNĐ") {
                                html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                            }
                            if (data.total['semi_annually'] != "0 VNĐ") {
                                html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                            }
                            if (data.total['annually'] != "0 VNĐ") {
                                html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                            }
                            if (data.total['biennially'] != "0 VNĐ") {
                                html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                            }
                            if (data.total['triennially'] != "0 VNĐ") {
                                html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                            }
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            $('#notication-service').html(html);
                            $('#button_email_hosting_service').attr('disabled', false);
                            $('.select2').select2();
                        }
                    },
                    error: function (e) {
                        console.log(e);
                        $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                        $('#button_email_hosting_service').attr('disabled', false);
                    },
                });
                break;

        }
        $('#button_email_hosting_service').attr('data-id', id);
        $('#button_email_hosting_service').attr('data-action', action);
        $('#button_email_hosting_service').attr('data-ip', domain);
        $(this).closest('tr').addClass('action-row-email_hosting');
    })

    $('#button_email_hosting_service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var ip = $(this).attr('data-ip');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/email_hosting/action_email_hosting',
            type: 'post',
            dataType: 'json',
            data: { '_token': token, id: id, action: action, billing_cycle: billing_cycle },
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_email_hosting_service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error == 1) {
                        $('#notication-service').html('<span class="text-danger">Dịch vụ Email Hosting này không thuộc quyền quản lý của bạn</span>');
                        $('#button_email_hosting_service').attr('disabled', false);
                    } else if (data.error == 4) {
                        $('#notication-service').html('<span class="text-danger">Hủy dịch vụ Email Hosting thất bại!</span>');
                        $('#button_email_hosting_service').attr('disabled', false);
                    } else if (data.error == 3) {
                        $('#notication-service').html('<span class="text-danger">Gia hạn dịch vụ Email Hosting thất bại!</span>');
                        $('#button_email_hosting_service').attr('disabled', false);
                    } else {
                        if (data.type == 4) {
                            $('.action-row-email_hosting .terminated').fadeOut();
                            $('.action-row-email_hosting .email_hosting_status').html('<span class="text-danger">Đã hủy</span>');
                            $('#button_server_service').attr('disabled', false);
                            $('tr').removeClass('action-row-email_hosting');
                            $('#notication-service').html('<span class="text-danger">Yêu cầu hủy Email Hosting thành công</span>');
                            $('#button_email_hosting_service').fadeOut();
                            $('#button-finish').fadeIn();
                        } else if (data.type == 3) {
                            var html = '<span class="text-center">Yêu cầu gia hạn Email Hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                            $('#button_server_service').attr('disabled', false);
                            $('tr').removeClass('action-row-colocation');
                            $('#notication-service').html(html);
                            $('#button_email_hosting_service').fadeOut();
                            $('#button-finish').fadeIn();
                        }
                    }
                    // $('#modal-service').modal('hide');
                    // $('#button-service').attr('disabled', false);
                } else {
                    $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                    $('#button_email_hosting_service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button_email_hosting_service').attr('disabled', false);
            }
        });
    });

    $(document).on('click', '.email_hosting_checkbox', function () {
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action_email_hosting');
        } else {
            $(this).closest('tr').removeClass('action_email_hosting');
        }
    });

    email_hosting_checkbox();
    var $emailHostingCheckbox = $('.email_hosting_checkbox');
    $.each($emailHostingCheckbox, function (index, value) {
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action_email_hosting');
        } else {
            $(this).closest('tr').removeClass('action_email_hosting');
        }
    })

    function email_hosting_checkbox() {
        // var top = $('html, body').offset().top;
        var $vpsCheckbox = $('.email_hosting_checkbox');
        var lastChecked = null;
        $vpsCheckbox.click(function (e) {
            if (!lastChecked) {
                lastChecked = this;
                return;
            }
            if (e.shiftKey) {
                var start = $vpsCheckbox.index(this);
                var end = $vpsCheckbox.index(lastChecked);
                $vpsCheckbox.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
                $.each($vpsCheckbox, function (index, value) {
                    if ($(this).is(':checked')) {
                        $(this).closest('tr').addClass('action_email_hosting');
                    } else {
                        $(this).closest('tr').removeClass('action_email_hosting');
                    }
                })
            }
            lastChecked = this;

        })

    }

    $('.btn-action-email_hosting').on('click', function () {
        var action = $(this).attr('data-type');
        var checked = $('.email_hosting_checkbox:checked');
        $('#button_email_hosting_terminated').fadeIn();
        $('#button_email_hosting_rebuil').fadeOut();
        if (checked.length > 0) {
            $('#modal-services').modal('show');
            switch (action) {
                case 'expired':
                    $('.modal-title').text('Yêu cầu gia hạn dịch vụ Email Hosting');
                    var list_id = [];
                    $.each(checked, function (index, value) {
                        list_id.push($(this).val());
                    })
                    $.ajax({
                        url: '/services/email_hosting/request_expired_list_email_hosting',
                        data: { list_id: list_id },
                        dataType: 'json',
                        beforeSend: function () {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button_email_hosting_terminated').attr('disabled', true);
                            $('#notication-invoice').html(html);
                        },
                        success: function (data) {
                            // console.log(data.price_override.expire_billing_cycle);
                            var html = '';
                            if (data.error == 1) {
                                $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button_email_hosting_terminated').attr('disabled', false);
                            } else {
                                if (data.price_override.expire_billing_cycle == 1) {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-invoice').html(html);
                                } else {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    if (data.total['monthly'] != '0 VNĐ') {
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                    }
                                    if (data.total['twomonthly'] != '0 VNĐ') {
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                    }
                                    if (data.total['quarterly'] != '0 VNĐ') {
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                    }
                                    if (data.total['semi_annually'] != '0 VNĐ') {
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                    }
                                    if (data.total['annually'] != '0 VNĐ') {
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                    }
                                    if (data.total['biennially'] != '0 VNĐ') {
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                    }
                                    if (data.total['triennially'] != '0 VNĐ') {
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                    }
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-invoice').html(html);
                                }
                                $('#button_email_hosting_terminated').attr('data-action', action);
                                $('#button_email_hosting_terminated').attr('disabled', false);
                                $('.select_expired').select2();
                            }
                        },
                        error: function (e) {
                            console.log(e);
                            $('#notication-invoice').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button_email_hosting_terminated').attr('disabled', false);
                        },
                    })
                    break;
                case 'delete':
                    $('.modal-title').text('Hủy dịch vụ Email Hosting');

                    var html = '';
                    html += '<div class="text-notication">';
                    html += '</div>';
                    html += '<div class="text-left ml-4">';
                    html += 'Danh sách Email Hosting yêu cầu hủy dịch vụ: <br>';
                    $.each(checked, function (index, value) {
                        html += $(this).attr('data-ip') + '<br>';
                    })
                    html += '</div>';
                    html += '<div class="text-center text-danger mt-2 form-rebuild">';
                    html += 'Bạn có muốn cài đặt lại tất cả Email Hosting này không';
                    html += '</div>';
                    html += '<div class="text-luuy">';
                    html += '</div>';

                    $('#notication-invoice').html(html);
                    $('#button_email_hosting_terminated').attr('data-action', action);
                    break;
            }
        } else {
            alert('Chọn ít nhất một Email Hosting');
        }
    })
    // modal action của email hosting
    $('#button_email_hosting_terminated').on('click', function () {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        var checked = $('.email_hosting_checkbox:checked');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function (index, value) {
            id_services.push($(this).val());
        })
        // console.log(id_services, billing_cycle);
        $.ajax({
            url: '/services/email_hosting/action_email_hosting_services',
            type: 'post',
            data: { '_token': token, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_email_hosting_terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');
                        if (data.error == 1) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                            $('#button_email_hosting_terminated').attr('disabled', false);
                            $('#button_email_hosting_terminated').fadeOut();
                            $('#button_email_hosting_rebuil').fadeIn();
                        } else if (data.error == 4) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Hủy dịch vụ Email Hosting thất bại.</span>');
                            $('#button_email_hosting_terminated').attr('disabled', false);
                            $('#button_email_hosting_terminated').fadeOut();
                            $('#button_email_hosting_rebuil').fadeIn();
                        } else if (data.error == 3) {
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu thất bại. Yêu cầu gia hạn dịch vụ Email Hosting thất bại.</span>');
                            $('#button_email_hosting_terminated').attr('disabled', false);
                            $('#button_email_hosting_terminated').fadeOut();
                            $('#button_email_hosting_rebuil').fadeIn();
                        }
                        $('#button_email_hosting_terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        if (data.type == 4) {
                            html = '<span class="text-danger">Đã hủy</span>';
                            $('.action_email_hosting .email_hosting_status').html(html);
                            $('.action_email_hosting .terminated').fadeOut();
                            $('.action_email_hosting .email_hosting_status').html('<span class="text-danger">Đã hủy</span>');
                            $('#button_email_hosting_terminated').attr('disabled', false);
                            $('#notication-invoice').html('<span class="text-danger">Yêu cầu hủy Email Hosting thành công</span>');
                            $('#button_email_hosting_terminated').fadeOut();
                            $('#button_email_hosting_rebuil').fadeIn();
                        } else if (data.type == 3) {
                            $('.action_email_hosting .terminated').fadeOut();
                            $('#button_email_hosting_terminated').attr('disabled', false);
                            $('#notication-invoice').html('Yêu cầu gia hạn Email Hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.');
                            $('#button_email_hosting_terminated').fadeOut();
                            $('#button_email_hosting_rebuil').fadeIn();
                        }
                    }
                    $('tr').removeClass('action_email_hosting');
                } else {
                    $('#notication-invoice').html('<span class="text-danger">Thao tác với dịch vụ Email Hosting thất bại.</span>');
                    $('#button_email_hosting_terminated').attr('disabled', false);
                    $('#button_email_hosting_terminated').fadeOut();
                    $('#button_email_hosting_rebuil').fadeIn();
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
            },
            error: function (e) {
                console.log(e);
                $('#button_email_hosting_terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button_email_hosting_terminated').fadeOut();
                $('#button_email_hosting_rebuil').fadeIn();
            }
        })
    });

    $('.email_hosting_classic').on('change', function () {
        var qtt = $(this).val();
        $('#service_action').val('select');
        $.ajax({
            url: '/services/email_hosting/select_email_hosting',
            data: { qtt: qtt, action: 'on' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_on(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('#search_email_hosting').on('keyup', function () {
        var q = $(this).val();
        var qtt = $('.email_hosting_classic').val();
        $('#service_action').val('search');
        $.ajax({
            url: '/services/email_hosting/search_email_hosting',
            data: { q: q, qtt: qtt, action: 'on' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_on(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function screent_email_hosting_on(data) {
        var html = '';
        $.each(data.data, function (index, email_hosting) {
            html += '<tr>';
            // checkbox
            html += '<td><input type="checkbox" value="' + email_hosting.id + '" data-ip="' + email_hosting.domain + '" class="email_hosting_checkbox"></td>';
            // ten san pham
            html += '<td>' + email_hosting.text_product + '</td>';
            // ip
            html += '<td><a href="/service/email_hosting/detail/' + email_hosting.id + '?type=email_hosting">' + email_hosting.domain + '</a>';
            if (email_hosting.expired == true) {
                html += ' - <span class="text-danger">Hết hạn</span>';
            } else if (email_hosting.isExpire == true) {
                html += ' - <span class="text-danger">Gần hết hạn</span>';
            }
            html += '</td>';
            // ngay tao
            html += '<td><span>' + email_hosting.date_create + '</span></td>';
            // ngay ket thuc
            html += '<td class="next_due_date">';
            if (email_hosting.isExpire == true || email_hosting.expired == true) {
                html += '<span class="text-danger">' + email_hosting.next_due_date + '<span>';
            } else {
                html += '<span>' + email_hosting.next_due_date + '<span>';
            }
            html += '</td>';
            // thoi gian thue
            html += '<td>' + email_hosting.text_billing_cycle + '</td>';
            // giá
            html += '<td><b>' + email_hosting.amount + '</b></td>';
            html += '<td class="email_hosting_status">' + email_hosting.text_status_server + '</td>';
            html += '<td class="page-service-action">';
            if (email_hosting.status_hosting != 'suspend') {
                if (email_hosting.expired || email_hosting.isExpire) {
                    html += '<button type="button" class="btn mr-1 btn-warning button-action-email_hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Email Hosting" data-action="expired" data-id="' + email_hosting.id + '" data-ip="' + email_hosting.domain + '"><i class="fas fa-plus-circle"></i></button>';
                }
                if (email_hosting.status_hosting != 'expire') {
                    if (email_hosting.status_hosting != 'cancel') {
                        html += '<button class="btn mr-1 btn-outline-danger button-action-email_hosting terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Email Hosting" data-action="terminated" data-id="' + email_hosting.id + '" data-ip="' + email_hosting.domain + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                }
            }
            html += '</td>';
            html += '</tr>';
        })
        $('.list_email_hosting tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_email_hosting tfoot').html(html_page);
    }

    $(document).on('click', '.pagination_email_hosting_on a', function (event) {
        event.preventDefault();
        var type = $('#service_action').val();
        var page = $(this).attr('data-page');
        if (type == 'select') {
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                qtt: qtt,
                page: page,
                action: 'on'
            };
            var link = '/services/email_hosting/select_email_hosting';
        } else {
            var q = $('#search_email_hosting').val();
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                q: q,
                qtt: qtt,
                page: page,
                action: 'on'
            };
            var link = '/services/email_hosting/search_email_hosting';
        }

        $.ajax({
            url: link,
            data: data_type,
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_on(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('.email_hosting_nearly').on('change', function () {
        var qtt = $(this).val();
        $('#service_action').val('select');
        $.ajax({
            url: '/services/email_hosting/select_email_hosting',
            data: { qtt: qtt, action: 'nearly' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_nearly(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('#search_email_hosting_nearly').on('keyup', function () {
        var q = $(this).val();
        var qtt = $('.email_hosting_classic').val();
        $('#service_action').val('search');
        $.ajax({
            url: '/services/email_hosting/search_email_hosting',
            data: { q: q, qtt: qtt, action: 'nearly' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_nearly(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $(document).on('click', '.pagination_email_hosting_nearly a', function (event) {
        event.preventDefault();
        var type = $('#service_action').val();
        var page = $(this).attr('data-page');
        if (type == 'select') {
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                qtt: qtt,
                page: page,
                action: 'nearly'
            };
            var link = '/services/email_hosting/select_email_hosting';
        } else {
            var q = $('#search_email_hosting').val();
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                q: q,
                qtt: qtt,
                page: page,
                action: 'nearly'
            };
            var link = '/services/email_hosting/search_email_hosting';
        }

        $.ajax({
            url: link,
            data: data_type,
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_nearly(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function screent_email_hosting_nearly(data) {
        var html = '';
        $.each(data.data, function (index, email_hosting) {
            html += '<tr>';
            // ten san pham
            html += '<td>' + email_hosting.text_product + '</td>';
            // ip
            html += '<td><a href="/service/email_hosting/detail/' + email_hosting.id + '?type=email_hosting">' + email_hosting.domain + '</a>';
            if (email_hosting.expired == true) {
                html += ' - <span class="text-danger">Hết hạn</span>';
            } else if (email_hosting.isExpire == true) {
                html += ' - <span class="text-danger">Gần hết hạn</span>';
            }
            html += '</td>';
            // ngay tao
            html += '<td><span>' + email_hosting.date_create + '</span></td>';
            // ngay ket thuc
            html += '<td class="next_due_date">';
            if (email_hosting.isExpire == true || email_hosting.expired == true) {
                html += '<span class="text-danger">' + email_hosting.next_due_date + '<span>';
            } else {
                html += '<span>' + email_hosting.next_due_date + '<span>';
            }
            html += '</td>';
            // thoi gian thue
            html += '<td>' + email_hosting.text_billing_cycle + '</td>';
            // giá
            html += '<td><b>' + email_hosting.amount + '</b></td>';
            html += '<td class="email_hosting_status">' + email_hosting.text_status_server + '</td>';
            html += '<td class="page-service-action">';
            if (email_hosting.status_hosting != 'suspend') {
                if (email_hosting.expired || email_hosting.isExpire) {
                    html += '<button type="button" class="btn mr-1 btn-warning button-action-email_hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Email Hosting" data-action="expired" data-id="' + email_hosting.id + '" data-ip="' + email_hosting.domain + '"><i class="fas fa-plus-circle"></i></button>';
                }
                if (email_hosting.status_hosting != 'expire') {
                    if (email_hosting.status_hosting != 'cancel') {
                        html += '<button class="btn mr-1 btn-outline-danger button-action-email_hosting terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Email Hosting" data-action="terminated" data-id="' + email_hosting.id + '" data-ip="' + email_hosting.domain + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                }
            }
            html += '</td>';
            html += '</tr>';
        })
        $('.list_email_hosting tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_email_hosting tfoot').html(html_page);
    }

    $('.email_hosting_all').on('change', function () {
        var qtt = $(this).val();
        $('#service_action').val('select');
        $.ajax({
            url: '/services/email_hosting/select_email_hosting',
            data: { qtt: qtt, action: 'all' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_all(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $('#search_email_hosting_all').on('keyup', function () {
        var q = $(this).val();
        var qtt = $('.email_hosting_classic').val();
        $('#service_action').val('search');
        $.ajax({
            url: '/services/email_hosting/search_email_hosting',
            data: { q: q, qtt: qtt, action: 'all' },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_all(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $(document).on('click', '.pagination_email_hosting_all a', function (event) {
        event.preventDefault();
        var type = $('#service_action').val();
        var page = $(this).attr('data-page');
        if (type == 'select') {
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                qtt: qtt,
                page: page,
                action: 'all'
            };
            var link = '/services/email_hosting/select_email_hosting';
        } else {
            var q = $('#search_email_hosting').val();
            var qtt = $('.email_hosting_classic').val();
            var data_type = {
                q: q,
                qtt: qtt,
                page: page,
                action: 'all'
            };
            var link = '/services/email_hosting/search_email_hosting';
        }

        $.ajax({
            url: link,
            data: data_type,
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    var html = '';
                    screent_email_hosting_nearly(data);
                } else {
                    $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="9" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    function screent_email_hosting_all(data) {
        var html = '';
        $.each(data.data, function (index, email_hosting) {
            html += '<tr>';
            // ten san pham
            html += '<td>' + email_hosting.text_product + '</td>';
            // ip
            html += '<td><a href="/service/email_hosting/detail/' + email_hosting.id + '?type=email_hosting">' + email_hosting.domain + '</a>';
            if (email_hosting.expired == true) {
                html += ' - <span class="text-danger">Hết hạn</span>';
            } else if (email_hosting.isExpire == true) {
                html += ' - <span class="text-danger">Gần hết hạn</span>';
            }
            html += '</td>';
            // ngay tao
            html += '<td><span>' + email_hosting.date_create + '</span></td>';
            // ngay ket thuc
            html += '<td class="next_due_date">';
            if (email_hosting.isExpire == true || email_hosting.expired == true) {
                html += '<span class="text-danger">' + email_hosting.next_due_date + '<span>';
            } else {
                html += '<span>' + email_hosting.next_due_date + '<span>';
            }
            html += '</td>';
            // thoi gian thue
            html += '<td>' + email_hosting.text_billing_cycle + '</td>';
            // giá
            html += '<td><b>' + email_hosting.amount + '</b></td>';
            html += '<td class="email_hosting_status">' + email_hosting.text_status_server + '</td>';
            html += '</tr>';
        })
        $('.list_email_hosting tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_email_hosting_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_email_hosting tfoot').html(html_page);
    }
    // ./update description
    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
