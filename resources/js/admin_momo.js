$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();

	$('.btn_filter').on('click' , function(event) {
		event.preventDefault();
		/* Act on the event */
		var html = '';
		html += '<div class="row">';
		html += '<div class="col-4">';
		html += '<input type="text" name="" class="form-control" id="number_phone" placeholder="Tìm kiếm số điện thoại">';
		html += '</div>';
		html += '<div class="col-4">';
		html += '<input type="text" name="" class="form-control" id="content" placeholder="Tìm kiếm nội dung">';
		html += '</div>';
		html += '<div class="col-4">';
		html += '<input type="text" name="" class="form-control" id="ma_gd" placeholder="Tìm kiếm mã giao dịch của MoMo">';
		
		html += '</div>';
		$('.fillter_momo').html(html);
	});

	$(document).on('keyup', '#number_phone', function(event) {
		event.preventDefault();
		/* Act on the event */
		var q = $(this).val();
		$('#type').val('number_phone');
		$.ajax({
			url: '/admin/momo/search_momo',
			dataType: 'json',
			data: {type: 'number_phone', q: q},
			beforeSend: function(){
              var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          	},
			success: function (data) {
				// console.log(data);
				if (data.data != '') {
					screen_momo(data);
				} else {
					$('tbody').html('<td class="text-center text-danger" colspan="6">Dữ liệu cần tìm không có!</td>');
				}
			},
			error: function (e) {
				console.log(e);
				$('tbody').html('<td class="text-center text-danger" colspan="6">Truy vấn dữ liệu giao dịch MoMo lỗi!</td>');
			}
		})
		
	});	

	$(document).on('keyup', '#content', function(event) {
		event.preventDefault();
		/* Act on the event */
		var q = $(this).val();
		$('#type').val('content');
		$.ajax({
			url: '/admin/momo/search_momo',
			dataType: 'json',
			data: {type: 'content', q: q},
			beforeSend: function(){
              var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          	},
			success: function (data) {
				// console.log(data);
				if (data.data != '') {
					screen_momo(data);
				} else {
					$('tbody').html('<td class="text-center text-danger" colspan="6">Dữ liệu cần tìm không có!</td>');
				}
			},
			error: function (e) {
				console.log(e);
				$('tbody').html('<td class="text-center text-danger" colspan="6">Truy vấn dữ liệu giao dịch MoMo lỗi!</td>');
			}
		})
		
	});	

	$(document).on('keyup', '#ma_gd', function(event) {
		event.preventDefault();
		/* Act on the event */
		var q = $(this).val();
		$('#type').val('id_momo');
		$.ajax({
			url: '/admin/momo/search_momo',
			dataType: 'json',
			data: {type: 'id_momo', q: q},
			beforeSend: function(){
              var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          	},
			success: function (data) {
				// console.log(data);
				if (data.data != '') {
					screen_momo(data);
				} else {
					$('tbody').html('<td class="text-center text-danger" colspan="6">Dữ liệu cần tìm không có!</td>');
				}
			},
			error: function (e) {
				console.log(e);
				$('tbody').html('<td class="text-center text-danger" colspan="6">Truy vấn dữ liệu giao dịch MoMo lỗi!</td>');
			}
		})
		
	});		

	function screen_momo(data) {
		var html = '';
		$.each(data.data , function (index, momo) {
			html += '<tr>';
			html += '<td>' + momo.date + '</td>';
			html += '<td>' + momo.number_phone + '</td>';
			html += '<td>' + momo.amount + '</td>';
			html += '<td>' + momo.content + '</td>';
			html += '<td>' + momo.id_momo + '</td>';
			html += '<td><a href="/admin/chinh-sua-giao-dich-momo/' + momo.id_momo + '" class="btn btn-warning text-white" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa lại giao dịch MoMo và xác nhận lại thanh toán"><i class="fas fa-edit"></i></a></td>';
			html += '</tr>';
		})
		$('tbody').html(html);
		$('[data-toggle="tooltip"]').tooltip();
		// phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if ( total / per_page > 11 ) {
              var page = parseInt(total/per_page + 1);
              html_page += '<td colspan="10" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_momo">';
              if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              if (current_page < 7) {
                for (var i = 1; i < 9; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              else if (current_page >= 7 || current_page <= page - 7) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = current_page - 3; i <= current_page +3; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
              }
              else if (current_page >= page - 6) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 6; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
              }

              if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '<nav>';
              html_page += '</td>';
            } else {
              var page = total/per_page + 1;
              html_page += '<td colspan="10" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_momo">';
              if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                      active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }

              }
              if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '<nav>';
              html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
	}

	$(document).on('click', '.pagination_momo li a', function (event) {
          event.preventDefault();
          // console.log('da click');
          var type = $('#type').val();
          var page = $(this).attr('data-page');
          if (type == 'number_phone') {
          	var q = $('#number_phone').val();
          }
          else if (type == 'content') {
          	var q = $('#content').val();
          }
          else if (type == 'id_momo') {
          	var q = $('#ma_gd').val();
          }
          $.ajax({
			url: '/admin/momo/search_momo',
			dataType: 'json',
			data: {type: type, q: q, page: page},
			beforeSend: function(){
              var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          	},
			success: function (data) {
				console.log(data);
				if (data.data != '') {
					screen_momo(data);
				} else {
					$('tbody').html('<td class="text-center text-danger" colspan="6">Dữ liệu cần tìm không có!</td>');
				}
			},
			error: function (e) {
				console.log(e);
				$('tbody').html('<td class="text-center text-danger" colspan="6">Truy vấn dữ liệu giao dịch MoMo lỗi!</td>');
			}
		})
    })

});