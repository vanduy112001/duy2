$(document).ready(function () {
    load_server();

    function load_server() {
        $.ajax({
            url: '/dich-vu/server/list_server_nearly_and_expire',
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data.data);
                if ( data.data.length > 0 ) {
                  list_server_nearly_and_expire_screent(data);
                  $("#data-server-table").DataTable({
                      "columnDefs": [
                           { "targets": [  1, 3 , 4, 6, 7, 8, 9 ], "orderable": false }
                      ],
                      "pageLength": 30,
                      "lengthMenu": [[ 25, 50, 100, -1], [25, 50, 100, "All"]],
                      "iDisplayLength": 25
                  });
                } else {
                  $('tfoot').html('');
                  $('tbody').html('<td class="text-center text-danger"  colspan="10">Không có dịch vụ Server nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tfoot').html('');
                $('tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ Server!</td>');
            }
        });
    }

    function list_server_nearly_and_expire_screent(data) {
       // console.log(data);
       var html = '';
       $.each(data.data, function (index , vps) {
          html += '<tr>';
          // checkbox
          // ip
          html += '<td class="ip_vps" data-ip="'+ vps.ip +'"><a href="/service/detail/'+ vps.id +'?type=vps">'+ vps.ip +'</a>';
          if (vps.expired == true) {
            html += ' - <span class="text-danger">Hết hạn</span>';
          } else if (vps.isExpire == true) {
            html += ' - <span class="text-danger">Gần hết hạn</span>';
          }
          html += '</td>';
          // cau hinh
          html += '<td>'+ vps.text_vps_config +'</td>';
          // loai
          // khach hang
          // ngay tao
          html += '<td><span>' + vps.date_create + '</span></td>';
          // ngay ket thuc
          html += '<td class="next_due_date">';
          if (vps.isExpire == true || vps.expired == true) {
            html += '<span class="text-danger">' + vps.next_due_date + '<span>';
          } else {
            html += '<span>' + vps.next_due_date + '<span>';
          }
          html += '</td>';
          // thoi gian thue
          html += '<td>' + vps.text_billing_cycle +  '</td>';
          html += '<td>' + vps.amount + ' VNĐ</td>';
          html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
          html += '<td class="page-service-action">';
          if (vps.status_vps != 'delete_server' ) {
            if (vps.status_vps != 'cancel') {
              if (vps.status_vps != 'suspend') {
                if (vps.isExpire == true || vps.expired == true) {
                  html += '<button type="button" class="btn btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                } else {
                  if (vps.status_vps != 'admin_off') {
                    if (vps.status_vps != 'cancel') {
                      html += '<button class="btn btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                  }
                }
              }
            }
          }
          html += '</td>';
          html += '</tr>';
       })
       $('tbody').html(html);
       $('[data-toggle="tooltip"]').tooltip();
    }

    $(document).on('click','.button-action-server', function () {
        $('tr').removeClass('action-row-server');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $('#button_server_service').fadeIn();
        $('#button-finish').fadeOut();
        $('#modal-service').modal('show');
        switch (action) {
          case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Server');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Server <b class="text-danger">(ip: '+ ip +')</b> này không ?</span>');
            break;
          case 'expired':
              $('#modal-service .modal-title').text('Yêu cầu gia hạn Server');
              $('#notication-service').html('<span>Bạn có muốn gia hạn Server <b class="text-danger">(ip: '+ ip +')</b> này không </span>');
              $.ajax({
                  url: '/service/request_expired_server',
                  data: {id: id},
                  dataType: 'json',
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('#button_server_service').attr('disabled', true);
                      $('#notication-service').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      if (data.error == 1) {
                          $('#button_server_service').html('<span class="text-center text-danger">Dịch vụ thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.</span>');
                          $('#button-service').attr('disabled', false);
                      } else {
                          // console.log(data);
                          html += '<div class="form-group">';
                          html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                          html += '<div class="mt-3 mb-3">';
                          html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                          html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                          html += '<option value="monthly">1 Tháng / '+ data.total['monthly'] +'</option>';
                          html += '<option value="twomonthly">2 Tháng / '+ data.total['twomonthly'] +'</option>';
                          html += '<option value="quarterly">3 Tháng / '+ data.total['quarterly'] +'</option>';
                          html += '<option value="semi_annually">6 Tháng / '+ data.total['semi_annually'] +'</option>';
                          html += '<option value="annually">1 Năm / '+ data.total['annually'] +'</option>';
                          html += '<option value="biennially">2 Năm / '+ data.total['biennially'] +'</option>';
                          html += '<option value="triennially">3 Năm / '+ data.total['triennially'] +'</option>';
                          html += '</select>';
                          html += '</div>';
                          html += '</div>';
                          $('#notication-service').html(html);
                          $('#button_server_service').attr('disabled', false);
                          $('.select2').select2();
                      }
                  },
                  error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                    $('#button-terminated').attr('disabled', false);
                  },
                });
            break;

        }
        $('#button_server_service').attr('data-id', id);
        $('#button_server_service').attr('data-action', action);
        $('#button_server_service').attr('data-ip', ip);
        $(this).closest('tr').addClass('action-row-server');
    })

    $('#button_server_service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var ip = $(this).attr('data-ip');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/server/action-server',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, action: action, billing_cycle: billing_cycle},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_server_service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                console.log(data);
                var html = '';
                if (data) {
                  if (data.error == 1) {
                    $('#notication-service').html('<span class="text-danger">Dịch vụ Server này không thuộc quyền quản lý của bạn</span>');
                    $('#button_server_service').attr('disabled', false);
                  }
                  else if (data.error == 4) {
                    $('#notication-service').html('<span class="text-danger">Hủy dịch vụ Server thất bại!</span>');
                    $('#button_server_service').attr('disabled', false);
                  }
                  else if (data.error == 3) {
                    $('#notication-service').html('<span class="text-danger">Gia hạn dịch vụ Server thất bại!</span>');
                    $('#button_server_service').attr('disabled', false);
                  }
                  else {
                    if (data.type == 4) {
                        $('.action-row-server .terminated').fadeOut();
                        $('.action-row-server .server-status').html('<span class="text-danger">Đã hủy</span>');
                        $('#button_server_service').attr('disabled', false);
                        $('tr').removeClass('action-row-server');
                        $('#notication-service').html('<span class="text-danger">Yêu cầu hủy Server thành công</span>');
                        $('#button_server_service').fadeOut();
                        $('#button-finish').fadeIn();
                    }
                    else if ( data.type == 3 ) {
                        var html = '<span class="text-center">Yêu cầu gia hạn Server thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data.invoice_id+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                        $('#button_server_service').attr('disabled', false);
                        $('tr').removeClass('action-row-server');
                        $('#notication-service').html(html);
                        $('#button_server_service').fadeOut();
                        $('#button-finish').fadeIn();
                    }
                  }
                  // $('#modal-service').modal('hide');
                  // $('#button-service').attr('disabled', false);
                } else {
                  $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                  $('#button_server_service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button-service').attr('disabled', false);
            }
        });
    });

})
