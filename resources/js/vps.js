$(document).ready(function () {
    $(document).on('click', '.btn-delete-vps' ,function () {
        $('tr').removeClass('remove-vps');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $(this).closest('tr').addClass('remove-vps');
        $('#delete-order').modal('show');
        $('.modal-title').text('Xóa VPS');
        $('#notication-order').html('Bạn có muốn xóa VPS <b class="text-danger"> (ID:'+ id +' - IP:' + ip + ' )</b> này không?');
        $('#button-multi-vps').attr('disabled', false);
        $('#button-order').val('Xác nhận');
        $('#button-order').attr('data-id', id);
        $('#button-order').attr('data-action', "delete");
        $('#button-order').fadeIn();
        $('#button-finish').fadeOut();

    });

    $(document).on('click', '.button-action-vps', function() {
        $('tr').removeClass('remove-vps');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        // console.log(action);
        if ( action == 'change_user' ) {
          window.location.href = '/admin/vps/change_user?id=' + id;
        } else {
          $(this).closest('tr').addClass('remove-vps');
          $('#delete-order').modal('show');
          $('#button-order').val('Xác nhận');
          $('#button-order').fadeIn();
          $('#button-finish').fadeOut();
          $('#button-order').attr('data-id', id);
          $('#button-order').attr('data-action', action);

          if ( action == 'off' ) {
              $('.modal-title').text('Tắt VPS');
              $('#notication-order').html('Bạn có muốn tắt VPS <b class="text-danger"> (ID:'+ id +' - IP:' + ip + ' )</b> này không?');
          }
          else if ( action == 'on' ) {
              $('.modal-title').text('Bật VPS');
              $('#notication-order').html('Bạn có muốn bật VPS <b class="text-danger"> (ID:'+ id +' - IP:' + ip + ' )</b> này không?');
          }
          else if ( action == 'restart' ) {
              $('.modal-title').text('Khởi động lại VPS');
              $('#notication-order').html('Bạn có muốn khởi động lại VPS <b class="text-danger"> (ID:'+ id +' - IP:' + ip + ' )</b> này không?');
          }
        }
    });

    $('#button-order').on('click', function () {
        id = $(this).attr('data-id');
        action = $(this).attr('data-action');
        $.ajax({
            type: "get",
            url: "/admin/vps/delete_ajax_vps",
            data: {id:  id, action: action},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-order').attr('disabled', true);
                $('#notication-order').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( action == 'delete' ) {
                    if (data) {
                        var html = '<p class="text-danger">Xóa VPS thành công</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        $('.remove-vps').fadeOut(1500);
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                    } else {
                        var html = '<p class="text-danger">Xóa VPS thất bại</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                    }
                }
                else if ( action == 'off' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', false);
                        $('.remove-vps .on').attr('disabled', true);
                        $('.remove-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                        $('#notication-order').html('<span class="text-success">Tắt VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Tắt VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                else if ( action == 'on' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', true);
                        $('.remove-vps .on').attr('disabled', false);
                        $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                        $('#notication-order').html('<span class="text-success">Bật VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Bật VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                else if ( action == 'restart' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', false);
                        $('.remove-vps .on').attr('disabled', true);
                        $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                        $('#notication-order').html('<span class="text-success">Khởi động lại VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Khởi động lại VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                
            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn VPS bị lỗi!</p>';
                $('#nnotication-order').html(html);
                $('#button-order').attr('disabled', false);
                $('.remove-vps').removeClass('remove-vps');
            }
        });
    });

    $(document).on('click', '.btn-multi-action-vps', function() {
        var action = $(this).attr('data-type');
        var checked = $('.vps_checkbox:checked');
        // console.log(action);
        if(checked.length > 0) {
            if (action == 'change_user') {
              var list_id = [];
              $.each(checked, function (index, value) {
                list_id.push($(this).val());
              });
              window.location.href = '/admin/vps/change_user?id=' + list_id;
            }
            else {
              $('#delete-multi-vps').modal('show');
              $('#button-multi-vps').val('Xác nhận');
              $('#button-multi-vps').fadeIn();
              $('#button-multi-vps-finish').fadeOut();
              $('#button-multi-vps').attr('data-type', action);

              if ( action == 'off' ) {
                  $('.modal-title').text('Tắt VPS');
                  $('#notication-multi').html('Bạn có muốn tắt các VPS này không?');
                  $('#button-multi-vps').attr('disabled', false);
              }
              else if ( action == 'on' ) {
                  $('.modal-title').text('Bật VPS');
                  $('#notication-multi').html('Bạn có muốn bật các VPS này không?');
                  $('#button-multi-vps').attr('disabled', false);
              }
              else if ( action == 'restart' ) {
                  $('.modal-title').text('Khởi động lại VPS');
                  $('#notication-multi').html('Bạn có muốn khởi động lại các VPS này không?');
                  $('#button-multi-vps').attr('disabled', false);
              }
              else if ( action == 'change_ip' ) {
          // console.log("da den");
                  $('.modal-title').text('Đổi IP VPS');
                  $('#notication-multi').html('Bạn có muốn đổi IP cho các VPS này không?');
                  $('#button-multi-vps').attr('disabled', false);
              }
            }
        } else {
            alert('Vui lòng chọn 1 hoặc nhiều VPS để tiếp tục.');
        }
        
    });

    // button action
    $('#button-multi-vps').on('click', function () {
        var action = $(this).attr('data-type');
        var checked = $('.vps_checkbox:checked');
        var vps_id = [];
        $.each(checked, function (index, value) {
            vps_id.push($(this).val());
        });
        $.ajax({
            type: "get",
            url: "/admin/vps/action",
            data: {vps_id: vps_id, action: action},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-multi-vps').attr('disabled', true);
                $('#notication-multi').html(html);
            },
            success: function (data) {
                var html = '';
                if ( action == 'off' ) {
                    if (data) {
                        $('.action-vps .off').attr('disabled', false);
                        $('.action-vps .on').attr('disabled', true);
                        $('.action-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                        html = '<span class="text-success">Tắt VPS thành công</span>';
                        $('#notication-multi').html(html);
                    } else {
                        html = '<span class="text-danger">Tắt VPS thất bại</span>';
                        $('#notication-multi').html(html);
                        $('#button-multi-vps').attr('disabled', false);
                    }
                }
                else if ( action == 'on' ) {
                    if (data) {
                        $('.action-vps .off').attr('disabled', true);
                        $('.action-vps .on').attr('disabled', false);
                        $('.action-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                        html = '<span class="text-success">Bật VPS thành công</span>';
                        $('#notication-multi').html(html);
                    } else {
                        html = '<span class="text-danger">Bật VPS thất bại</span>';
                        $('#notication-multi').html(html);
                        $('#button-multi-vps').attr('disabled', false);
                    }
                }
                else if ( action == 'restart' ) {
                    if (data) {
                        $('.vps_checkbox .off').attr('disabled', false);
                        $('.vps_checkbox .on').attr('disabled', true);
                        $('.vps_checkbox .vps-status').html('<span class="text-success">Đang bật</span>');
                        html = '<span class="text-success">Khởi động lại VPS thành công</span>';
                        $('#notication-multi').html(html);
                    } else {
                        html = '<span class="text-danger">Khởi động lại VPS thất bại</span>';
                        $('#notication-multi').html(html);
                        $('#button-multi-vps').attr('disabled', false);
                    }
                }
                else if ( action == 'change_ip' ) {
                    if (data) {
                        $('.vps_checkbox .off').attr('disabled', true);
                        $('.vps_checkbox .on').attr('disabled', true);
                        $('.vps_checkbox .vps-status').html('<span class="text-info">Đang đổi IP</span>');
                        html = '<span class="text-success">Đổi IP cho VPS thành công</span>';
                        $('#notication-multi').html(html);
                    } else {
                        html = '<span class="text-danger">Đổi IP cho VPS thất bại</span>';
                        $('#notication-multi').html(html);
                        $('#button-multi-vps').attr('disabled', false);
                    }
                    setTimeout(() => {
                        location.reload();
                    }, 4000);
                }
                $('#button-multi-vps').fadeOut();
                $('#button-multi-vps-finish').fadeIn();
                $('tr').removeClass('action-vps');
            },
            error: function (e) {
                console.log(e);
                $('#notication-multi').html('<p class="text-center text-danger">Truy vấn dữ liệu VPS lỗi</p>');
                $('#button-multi-vps').attr('disabled', false);
            }
        });
    });

    $('#btn_on_multi').on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      var html = '';
      html += '<div class="input-group">';
      html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
      html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_on_search_multi">';
      html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
      html += '</div>';
      html += '</div>';
      $('#on_search_multi').html(html);
    });

    $(document).on("click", '#btn_on_search_multi' , function () {
         let text = $("#form_search_multi").val();
         let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
         let ips, ip = '';
         while (text.match(r) != null) {
            ip = text.match(r)[0];
            ips += ip + ',';
            text = text.replace(ip, '');
        }
        ips = ips.slice(0, -1);
        ips = ips.replace('undefined', '');

        var vps_q = ips;
        $.ajax({
            url: '/admin/vps/search_multi_vps_vn',
            dataType: 'json',
            data: { q: vps_q },
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    var html = '';
                    $.each(data.data, function (index , vps) {
                        html += '<tr>';
                        html += '<td><input type="checkbox"  value="'+ vps.id +'" data-ip="'+ vps.ip +'" class="vps_checkbox"></td>';
                        if ( vps.user_vps != null ) {
                          html += '<td><a href="/admin/users/detail/'+ vps.user_id +'">'+ vps.user_vps.name +'</a></td>';
                        } else {
                          html += '<td><span class="text-danger">Đã xóa</span></td>';
                        }
                        // IP VPS
                        if (vps.ip) {
                            html += '<td><a href="/admin/vps/detail/' + vps.id + '">'+ vps.ip +'</a></td>';
                        } else {
                            html += '<td class="text-danger">Chưa tạo</td>';
                        }
                        // cấu hình
                        if (vps.config != '') {
                            html += '<td>' + vps.config + '</td>';
                        } else {
                            html += '<td class="text-danger">Chưa tạo</td>';
                        }
                        // Loại vps
                        // if (vps.type_vps == "vps") {
                        //   html += '<td>VPS</td>';
                        // } else {
                        //   html += '<td>NAT VPS</td>';
                        // }
                        // Ngày tạo
                        if (vps.created_at) {
                            var date = new Date(vps.created_at);
                            // console.log(date);
                            var day = date.getDate();
                            if (day < 10) {
                              day = '0' + day;
                            }
                            var month = date.getMonth() + 1;
                            if (month < 10) {
                              month = '0' + month;
                            }
                            html += '<td>';
                            html += date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + day + '-' + month + '-' + date.getFullYear();
                            html += '</td>';
                        } else {
                            html += '<td class="text-danger">Chưa tạo</td>';
                        }
                        // Ngày kết thúc
                        if (vps.next_due_date) {
                            var date2 = new Date(vps.next_due_date);
                            var day2 = date2.getDate();
                            if (day2 < 10) {
                              day2 = '0' + day2;
                            }
                            var month2 = date2.getMonth() + 1;
                            if (month2 < 10) {
                              month2 = '0' + month2;
                            }
                            html += '<td>';
                            html += day2 + '-' + month2 + '-' + date2.getFullYear();
                            html += '</td>';
                        } else {
                            html += '<td class="text-danger">Chưa tạo</td>';
                        }
                        // chu kỳ thanh toán
                        html += '<td>'+ vps.total_time +'</td>';
                        // Tổng thời gian thuê
                        html += '<td>'+ vps.text_billing_cycle +'</td>';
                        // thành tiềnote
                        if (vps.price_override != 0 && vps.price_override != null) {
                          html += '<td><b class="text-danger">' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
                        } else {
                          html += '<td><b>' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
                        }

                        // Trạng thái

                        if(vps.status == 'Pending') {
                            html += '<td class="text-danger vps-status">Chưa tạo</td>';
                        }
                        else if(vps.status_vps == 'progressing') {
                            html += '<td class="text-info vps-status">Đang tạo</td>';
                        }
                        else if(vps.status_vps == 'off') {
                            html += '<td class="text-danger vps-status">Đã tắt</td>';
                        }
                        else if(vps.status_vps == 'on') {
                            html += '<td class="text-success vps-status">Đang bật</td>';
                        }
                        else if(vps.status_vps == 'admin_off') {
                            html += '<td class="text-danger vps-status">Admin tắt</td>';
                        }
                        else if(vps.status_vps == 'cancel') {
                            html += '<td class="text-danger vps-status">Đã hủy</td>';
                        }
                        else if(vps.status_vps == 'delete_vps') {
                            html += '<td class="text-danger vps-status">Đã xóa</td>';
                        }
                        else if(vps.status_vps == 'change_user') {
                            html += '<td class="text-danger vps-status">Đã chuyển</td>';
                        }
                        else if(vps.status_vps == 'rebuild') {
                            html += '<td class="text-info vps-status">Đang cài lại</td>';
                        }
                        else if(vps.status_vps == 'change_ip') {
                            html += '<td class="text-info vps-status">Đang đổi IP</td>';
                        }
                        else if(vps.status_vps == 'reset_password') {
                            html += '<td class="text-info vps-status">Đang đặt lại mật khẩu</td>';
                        }
                        else if(vps.status_vps == 'expire') {
                            html += '<td class="text-danger vps-status">Đã hết hạn</td>';
                        }
                        else {
                            html += '<td class="text-secondary vps-status">Chưa tạo</td>';
                        }
                        html += '<td class="button-action">';
                        if(vps.status_vps == 'on') {
                            html += '<a href="/vps/console/'+ vps.id +'" target="_blank" class="btn btn-success"><i class="fas fa-desktop"></i></a>';
                        }
                        if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user' && vps.ip ) {
                            if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                              html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                              html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                            } else {
                              html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                              html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                            }
                            if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                                // console.log("da den");
                              html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                            } else {
                              html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                            }
                        }
                        html += '</td>';
                        // hành động
                        html += '<td class="button-action">';
                        if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user') {
                          html += '<button class="btn btn-info button-action-vps" data-action="change_user" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-id="' + vps.id + '" data-ip="' + vps.ip + '" ><i class="fas fa-exchange-alt"></i></button>';
                        }
                        html += '<a href="/admin/vps/detail/'+ vps.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>'
                        html += '<button class="btn btn-danger btn-delete-vps" data-id="'+ vps.id +'"  data-action="delete" data-ip="'+ vps.ip +'" ><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';

                        html += '</tr>';
                    })
                    $('tbody').html(html);
                    $('tfoot').html("");
                    $('.paginate_top').html("");
                } else {
                  $('.list_vps tfoot').html('');
                  $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    }) 

    $(document).on('click', 'a.date_create', function (event) {
        event.preventDefault();
        var date = $(this).attr('data-date');
        var html = '';
        html += '<input type="text" placeholder="Ngày tạo" value="'+date+'" name="date_create" class="form-control">';
        $('#date_create').html(html);
    })

    $(document).on('click','.vps_checkbox', function () {
        if($(this).is(':checked')) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        var checked = $('.vps_checkbox:checked');
        if(checked.length > 0) {
          $('.qtt_vps').text('Số lượng: ' + checked.length);
          $('.button-header').css('opacity', 0.8);
        } else {
          $('.button-header').css('opacity', 0);
        }
    });

    $(document).on('click','.vps_checkbox_all', function () {
        if($(this).is(':checked')) {
            $('tr').addClass('action-vps');
            $('.vps_checkbox').prop('checked', this.checked);
        } else {
            $('.vps_checkbox').prop('checked', this.checked);
            $('tr').removeClass('action-vps');
        }
        var checked = $('.vps_checkbox:checked');
        if(checked.length > 0) {
          $('.qtt_vps').text('Số lượng: ' + checked.length);
          $('.button-header').css('opacity', 0.8);
        } else {
          $('.button-header').css('opacity', 0);
        }
    });

    vps_checkbox();

    function vps_checkbox() {
      // var top = $('html, body').offset().top;
      var $vpsCheckbox = $('.vps_checkbox');
      var lastChecked = null;
      $vpsCheckbox.click(function (e) {
          if (!lastChecked) {
              lastChecked = this;
              return;
          }
          if ( e.shiftKey ) {
              var start = $vpsCheckbox.index(this);
              var end = $vpsCheckbox.index(lastChecked);
              $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
              $.each( $vpsCheckbox , function (index , value) {
                  if( $(this).is(':checked') ) {
                      $(this).closest('tr').addClass('action-vps');
                  } else {
                      $(this).closest('tr').removeClass('action-vps');
                  }
              })
          }
          lastChecked = this;
      })

      var checked = $('.vps_checkbox:checked');
      if(checked.length > 0) {
        $('.qtt_vps').text('Số lượng: ' + checked.length);
        $('.button-header').css('opacity', 0.8);
      } else {
        $('.button-header').css('opacity', 0);
      }

    }

    $('.btn-action-vps').on('click', function() {
        var action = $(this).attr('data-type');
        var checked = $('.vps_checkbox:checked');
        if(checked.length > 0) {
            $('#delete-vps').modal('show');
            $('#button-vps').fadeIn();
            $('#button-vps-finish').fadeOut();
            if (action == 'delete') {
                var vps_ip = [];
                var html = '';
                $.each(checked, function (index, value) {
                    if ( $(this).attr('data-ip') != '' ) {
                      vps_ip.push($(this).attr('data-ip'));
                    } else {
                      vps_ip.push($(this).val());
                    }
                });
                html += '<p class="text-danger">Bạn có muốn xóa tất cả VPS được chọn không?</p>';
                html += '<div class="text-left ml-4">';
                html += 'Danh sách VPS: <br>';
                html += '<div class="ml-4">';
                $.each(vps_ip, function (index2, ip) {
                  html += ip + '</br>';
                })
                html += '</div>';
                html += '</div>';
                $('.modal-title').text('Xóa VPS');
                $('#notication-invoice').html(html);
                $('#button-vps').text('Xóa VPS');
                $('#button-vps').attr('data-type', 'delete');
            }
            else if (action == 'create') {
                $('.modal-title').text('Tạo VPS');
                $('#notication-invoice').html('<p class="text-danger">Bạn có muốn tạo tất cả VPS được chọn không?</p>');
                $('#button-vps').text('Tạo VPS');
                $('#button-vps').attr('data-type', 'create');
            }
            else if (action == 'on') {
                $('.modal-title').text('Bật VPS');
                $('#notication-invoice').html('<p class="text-danger">Bạn có muốn bật tất cả VPS được chọn không?</p>');
                $('#button-vps').text('Bật VPS');
                $('#button-vps').attr('data-type', 'on');
            }
            else if (action == 'off') {
                $('.modal-title').text('Tắt VPS');
                $('#notication-invoice').html('<p class="text-danger">Bạn có muốn tắt tất cả VPS được chọn không?</p>');
                $('#button-vps').text('Tắt VPS');
                $('#button-vps').attr('data-type', 'off');
            } else {
                $('.modal-title').text('Hủy VPS');
                $('#notication-invoice').html('<p class="text-danger">Bạn có muốn hủy tất cả VPS được chọn không?</p>');
                $('#button-vps').text('Hủy VPS');
                $('#button-vps').attr('data-type', 'cancel');
            }
        } else {
            alert('Chọn ít nhất một VPS');
        }
    });
    // button action
    $('#button-vps').on('click', function () {
        var action = $(this).attr('data-type');
        var checked = $('.vps_checkbox:checked');
        var vps_id = [];
        $.each(checked, function (index, value) {
            vps_id.push($(this).val());
        });
        $.ajax({
            type: "get",
            url: "/admin/vps/action",
            data: {vps_id: vps_id, action: action},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-vps').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                var html = '';
                if (data.create != '') {
                    html = '<p class="text-success">'+data.create+'</p>';
                } else if (data.delete != ''){
                    html = '<p class="text-danger">'+data.delete+'</p>';
                    $('.action-vps').fadeOut();
                    $('tr').removeClass('action-vps');
                    if(checked.length > 0) {
                      $('.qtt_vps').text('Số lượng: ' + checked.length);
                      $('.button-header').css('opacity', 0.8);
                    } else {
                      $('.button-header').css('opacity', 0);
                    }
                }
                else if (data.on != '') {
                    html = '<p class="text-success">'+data.on+'</p>';
                }
                else if (data.off != '') {
                    html = '<p class="text-danger">'+data.off+'</p>';
                } else {
                    html = '<p class="text-success">'+data.cancel+'</p>';
                }
                $('#button-vps').attr('disabled', false);
                $('#notication-invoice').html(html);
                $('#button-vps').fadeOut();
                $('#button-vps-finish').fadeIn();
                // setTimeout(() => {
                //     location.reload();
                // }, 1000);
            },
            error: function (e) {
                console.log(e);
                $('#notication-invoice').html('<p class="text-center text-danger">Truy vấn dữ liệu VPS lỗi</p>');
                $('#button-vps').attr('disabled', false);
            }
        });
    });
    // button action trong file edit
    $('.btn-action-edit-vps').on('click', function (event) {
        event.preventDefault();
        var action = $(this).attr("data-type");
        var id = $(this).attr('data-id');
        var html = '';
        if (action == 'delete') {
            html += '<p class="text-danger">Bạn có muốn xóa VPS này không</p>';
            $('.modal-title').text('Xóa VPS');
            $('#button-vps').text('Xóa VPS');
            $('#button-vps').attr('data-type', 'delete');
        }
        else if (action == 'on') {
            html += '<p class="text-success">Bạn có muốn bật VPS này không</p>';
            $('.modal-title').text('Bật VPS');
            $('#button-vps').text('Bật VPS');
            $('#button-vps').attr('data-type', 'on');
        }
        else if (action == 'create') {
            html += '<p class="text-success">Bạn có muốn tạo VPS này không</p>';
            $('.modal-title').text('Tạo VPS');
            $('#button-vps').text('Tạo VPS');
            $('#button-vps').attr('data-type', 'create');
        }
        else if (action == 'paid') {
            html += '<p class="text-success">Bạn có muốn gán đã thanh toán cho VPS này không</p>';
            $('.modal-title').text('Thanh toán VPS');
            $('#button-vps').text('Đã thanh toán');
            $('#button-vps').attr('data-type', 'paid');
        }
        else if (action == 'unpaid') {
            html += '<p class="text-danger">Bạn có muốn gán chưa thanh toán cho VPS này không</p>';
            $('.modal-title').text('Thanh toán VPS');
            $('#button-vps').text('Chưa thanh toán');
            $('#button-vps').attr('data-type', 'unpaid');
        }
        else if (action == 'off') {
            html += '<p class="text-danger">Bạn có muốn tắt VPS này không</p>';
            $('.modal-title').text('Tắt VPS');
            $('#button-vps').text('Tắt VPS');
            $('#button-vps').attr('data-type', 'off');
        } else {
            html += '<p class="text-danger">Bạn có muốn hủy VPS này không</p>';
            $('.modal-title').text('Hủy VPS');
            $('#button-vps').text('Hủy VPS');
            $('#button-vps').attr('data-type', 'cancel');
        }
        $('#delete-vps').modal('show');
        $('#notication-invoice').html(html);
        $('#button-vps').attr('data-id', id);

    });

    $(".next_due_date").on('click', function(event) {
        event.preventDefault();
        var date = $(this).attr('data-date');
        var html = '';
        html += '<input type="text" value="'+date+'" name="next_due_date" class="form-control">';
        $('#next_due_date').html(html);
    });

    $(".paid_date").on('click', function(event) {
        event.preventDefault();
        var date = $(this).attr('data-date');
        var html = '';
        html += '<input type="text" value="'+date+'" name="paid_date" class="form-control">';
        $('#paid_date').html(html);
    });

    $("#price_override").on('click', function() {
        if ($(this).is(':checked')) {
          var price = $('#sub_total_hidden').val();
          var html = '';
          html += '<input type="text" value="'+price+'" data-price="'+ price +'" name="sub_total_hidden" id="sub_total_hidden" class="form-control">';
        }
        else {
          var price = $('#sub_total_hidden').val();
          // console.log(price);
          var html = '<span class="mt-1">';
          html += '<b>'+ addCommas(price) +' VNĐ</b>';
          html += '</span>';
          html += '<input type="hidden" name="sub_total_hidden" id="sub_total_hidden" name="sub_total_hidden" value="'+ price +'">';
        }
        $('#sub_total').html(html);
    });

    $('.billing_cycle').on('click', function(event) {
        event.preventDefault();
        var billing_cycle = $(this).attr('data-name');
        $.ajax({
            url: '/admin/vps/loadBillings',
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#billing_cycle').html(html);
            },
            success: function (data) {
                var html = '';
                if (data != '' || data != null) {
                    html += '<select class="select2 form-control select_billing_cycle" style="width:100%" name="billing_cycle">';
                    html += '<option disabled>---Chọn thời gian thuê---</option>';
                    $.each(data , function(index, billing) {
                        if (billing_cycle == index) {
                            html += '<option value="'+index+'" selected>'+billing+'</option>';
                        } else {
                              html += '<option value="'+index+'">'+billing+'</option>';
                        }
                    });
                    html += '</select>';
                } else {
                    html += '<p class="text-danger">Không có thời gian thuê cho truy vấn này!</p>';
                }
                $('#billing_cycle').html(html);
                $('.select2').select2();
            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn thời gian thuê thất bại!</p>';
                $('#billing_cycle').html(html);
            }
        });
    });

    $(document).on('change' , '.select_billing_cycle' ,function() {
        var price = $('#sub_total_hidden').val();
        var html = '';
        html += '<input type="text" name="sub_total_hidden" value="'+price+'" class="form-control is-invalid">';
        $('#sub_total').html(html);
    });

    $('#select_status_vps').on('change', function () {
        var status = $(this).val();
        var q = $('#search').val();
        var sort = $('.sort_type').val();
        var qtt = $('#qtt').val();
        // console.log(qtt);
        $('#type_user_personal').val('select_status_vps');
        $.ajax({
            url: '/admin/vps/select_status_vps',
            data: { status: status, q: q, sort: sort, qtt: qtt },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data.data != '') {
                    screen_vps(data);
                } else {
                    html = '<td  colspan="14" class="text-center text-danger">Không có VPS trong dữ liệu</td>';
                    $('tbody').html(html);
                }
            },
            error: function (e) {
                console.log(e);
                var html = '<td  colspan="14" class="text-center text-danger">Truy vấn VPS lỗi!</td>';
                $('tbody').html(html);
            }
        });
    });

    $('#search').on('keyup', function () {
        var q = $(this).val();
        var status = $('#select_status_vps').val();
        var sort = $('.sort_type').val();
        var qtt = $('#qtt').val();
        $('#type_user_personal').val('search_vps');
        $.ajax({
            url: '/admin/vps/search',
            data: { q: q, status: status, sort: sort, qtt: qtt },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data.data != '') {
                    screen_vps(data);
                } else {
                    html = '<td  colspan="14" class="text-center text-danger">Không có VPS trong dữ liệu</td>';
                    $('tbody').html(html);
                }
            },
            error: function (e) {
                console.log(e);
                var html = '<td  colspan="14" class="text-center text-danger">Truy vấn VPS lỗi!</td>';
                $('tbody').html(html);
            }
        });
    });

    $('#qtt').on('change', function() {
        var q = $('#search').val();
        var status = $('#select_status_vps').val();
        var sort = $('.sort_type').val();
        var qtt = $('#qtt').val();
        $('#type_user_personal').val('search_vps');
        $.ajax({
            url: '/admin/vps/search',
            data: { q: q, status: status, sort: sort, qtt: qtt },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data.data != '') {
                    screen_vps(data);
                } else {
                    html = '<td  colspan="14" class="text-center text-danger">Không có VPS trong dữ liệu</td>';
                    $('tbody').html(html);
                }
            },
            error: function (e) {
                console.log(e);
                var html = '<td  colspan="14" class="text-center text-danger">Truy vấn VPS lỗi!</td>';
                $('tbody').html(html);
            }
        });
    });

    $(document).on('click', '#vps_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
         url: '/admin/vps/list_vps_of_personal',
         dataType: 'json',
         beforeSend: function(){
             var html = '<td colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
              // console.log(data);
             if (data.data.length > 0) {
                 screen_list_vps(data);
             } else {
                 var html = '<td colspan="14" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
                 $('tbody').html(html);
                 $('tfoot').html('');
             }
         },
         error: function (e) {
             console.log(e);
             var html = '<td colspan="14" class="text-center">Truy vấn thanh toán lỗi!</td>';
             $('tbody').html(html);
         }
      })
    })

    $(document).on('click', '#vps_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
         url: '/admin/vps/list_vps_of_enterprise',
         dataType: 'json',
         beforeSend: function(){
             var html = '<td colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
              // console.log(data);
             if (data.data.length > 0) {
                 screen_list_vps(data);
             } else {
                 var html = '<td colspan="14" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
                 $('tbody').html(html);
                 $('tfoot').html('');
             }
         },
         error: function (e) {
             console.log(e);
             var html = '<td colspan="14" class="text-center">Truy vấn thanh toán lỗi!</td>';
             $('tbody').html(html);
         }
      })
    })

    function screen_list_vps(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index , vps) {
            html += '<tr>';
            html += '<td><input type="checkbox"  value="'+ vps.id +'" data-ip="'+ vps.ip +'" class="vps_checkbox"></td>';
            if ( vps.user_vps != null ) {
              html += '<td><a href="/admin/users/detail/'+ vps.user_id +'">'+ vps.user_vps.name +'</a></td>';
            } else {
              html += '<td><span class="text-danger">Đã xóa</span></td>';
            }
            // IP VPS
            if (vps.ip) {
                html += '<td><a href="/admin/vps/detail/' + vps.id + '">'+ vps.ip +'</a></td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // cấu hình
            if (vps.config != '') {
                html += '<td>' + vps.config + '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Loại vps
            // if (vps.type_vps == "vps") {
            //   html += '<td>VPS</td>';
            // } else {
            //   html += '<td>NAT VPS</td>';
            // }
            // Ngày tạo
            if (vps.created_at) {
                var date = new Date(vps.created_at);
                // console.log(date);
                var day = date.getDate();
                if (day < 10) {
                  day = '0' + day;
                }
                var month = date.getMonth() + 1;
                if (month < 10) {
                  month = '0' + month;
                }
                html += '<td>';
                html += date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + day + '-' + month + '-' + date.getFullYear();
                html += '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Ngày kết thúc
            if (vps.next_due_date) {
                var date2 = new Date(vps.next_due_date);
                var day2 = date2.getDate();
                if (day2 < 10) {
                  day2 = '0' + day2;
                }
                var month2 = date2.getMonth() + 1;
                if (month2 < 10) {
                  month2 = '0' + month2;
                }
                html += '<td>';
                html += day2 + '-' + month2 + '-' + date2.getFullYear();
                html += '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Tổng thời gian thuê
            html += '<td>'+ vps.total_time +'</td>';
            // chu kỳ thanh toán
            html += '<td>'+ vps.text_billing_cycle +'</td>';
            // thành tiềnote
            if (vps.price_override != 0 && vps.price_override != null) {
              html += '<td><b class="text-danger">' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
            } else {
              html += '<td><b>' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
            }

            // Trạng thái

            if(vps.status == 'Pending') {
                html += '<td class="text-danger vps-status">Chưa tạo</td>';
            }
            else if(vps.status_vps == 'progressing') {
                html += '<td class="text-info vps-status">Đang tạo</td>';
            }
            else if(vps.status_vps == 'off') {
                html += '<td class="text-danger vps-status">Đã tắt</td>';
            }
            else if(vps.status_vps == 'on') {
                html += '<td class="text-success vps-status">Đang bật</td>';
            }
            else if(vps.status_vps == 'admin_off') {
                html += '<td class="text-danger vps-status">Admin tắt</td>';
            }
            else if(vps.status_vps == 'cancel') {
                html += '<td class="text-danger vps-status">Đã hủy</td>';
            }
            else if(vps.status_vps == 'delete_vps') {
                html += '<td class="text-danger vps-status">Đã xóa</td>';
            }
            else if(vps.status_vps == 'change_user') {
                html += '<td class="text-danger vps-status">Đã chuyển</td>';
            }
            else if(vps.status_vps == 'rebuild') {
                html += '<td class="text-info vps-status">Đang cài lại</td>';
            }
            else if(vps.status_vps == 'change_ip') {
                html += '<td class="text-info vps-status">Đang đổi IP</td>';
            }
            else if(vps.status_vps == 'reset_password') {
                html += '<td class="text-info vps-status">Đang đặt lại mật khẩu</td>';
            }
            else if(vps.status_vps == 'expire') {
                html += '<td class="text-danger vps-status">Đã hết hạn</td>';
            }
            else {
                html += '<td class="text-secondary vps-status">Chưa tạo</td>';
            }
            html += '<td class="button-action">';
            if(vps.status_vps == 'on') {
                html += '<a href="/vps/console/'+ vps.id +'" target="_blank" class="btn btn-success"><i class="fas fa-desktop"></i></a>';
            }
            if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user' && vps.ip ) {
                if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                  html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                  html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                } else {
                  html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                  html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                }
                if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                    // console.log("da den");
                  html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                } else {
                  html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                }
            }
            html += '</td>';
            // hành động
            html += '<td class="button-action">';
            if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user' ) {
              html += '<button class="btn btn-info button-action-vps" data-toggle="tooltip" data-action="change_user" data-placement="top" title="Chuyển khách hàng" data-id="' + vps.id + '" data-ip="' + vps.ip + '" ><i class="fas fa-exchange-alt"></i></button>';
            }
            html += '<a href="/admin/vps/detail/'+ vps.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>'
            html += '<button class="btn btn-danger btn-delete-vps" data-id="'+ vps.id +'"  data-action="delete" data-ip="'+ vps.ip +'" ><i class="fas fa-trash-alt"></i></button>';
            html += '</td>';

            html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="14" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="14" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_list_vps">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_list_vps">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screen_vps(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function (index , vps) {
            html += '<tr>';
            html += '<td><input type="checkbox"  value="'+ vps.id +'" data-ip="'+ vps.ip +'" class="vps_checkbox"></td>';
            if ( vps.user_vps != null ) {
              html += '<td><a href="/admin/users/detail/'+ vps.user_id +'">'+ vps.user_vps.name +'</a></td>';
            } else {
              html += '<td><span class="text-danger">Đã xóa</span></td>';
            }
            // IP VPS
            if (vps.ip) {
                html += '<td><a href="/admin/vps/detail/' + vps.id + '">'+ vps.ip +'</a></td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // cấu hình
            if (vps.config != '') {
                html += '<td>' + vps.config + '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Loại vps
            // if (vps.type_vps == "vps") {
            //   html += '<td>VPS</td>';
            // } else {
            //   html += '<td>NAT VPS</td>';
            // }
            // Ngày tạo
            if (vps.created_at) {
                var date = new Date(vps.created_at);
                // console.log(date);
                var day = date.getDate();
                if (day < 10) {
                  day = '0' + day;
                }
                var month = date.getMonth() + 1;
                if (month < 10) {
                  month = '0' + month;
                }
                html += '<td>';
                html += date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds() + ' ' + day + '-' + month + '-' + date.getFullYear();
                html += '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Ngày kết thúc
            if (vps.next_due_date) {
                var date2 = new Date(vps.next_due_date);
                var day2 = date2.getDate();
                if (day2 < 10) {
                  day2 = '0' + day2;
                }
                var month2 = date2.getMonth() + 1;
                if (month2 < 10) {
                  month2 = '0' + month2;
                }
                html += '<td>';
                html += day2 + '-' + month2 + '-' + date2.getFullYear();
                html += '</td>';
            } else {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            // Tổng thời gian thuê
            html += '<td>'+ vps.total_time +'</td>';
            // chu kỳ thanh toán
            html += '<td>'+ vps.text_billing_cycle +'</td>';
            // thành tiềnote
            if (vps.price_override != 0 && vps.price_override != null) {
              html += '<td><b class="text-danger">' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
            } else {
              html += '<td><b>' + addCommas(vps.total_vps) + ' VNĐ</b></td>';
            }

            // Trạng thái

            if(vps.status == 'Pending') {
                html += '<td class="text-danger vps-status">Chưa tạo</td>';
            }
            else if(vps.status_vps == 'progressing') {
                html += '<td class="text-info vps-status">Đang tạo</td>';
            }
            else if(vps.status_vps == 'off') {
                html += '<td class="text-danger vps-status">Đã tắt</td>';
            }
            else if(vps.status_vps == 'on') {
                html += '<td class="text-success vps-status">Đang bật</td>';
            }
            else if(vps.status_vps == 'admin_off') {
                html += '<td class="text-danger vps-status">Admin tắt</td>';
            }
            else if(vps.status_vps == 'cancel') {
                html += '<td class="text-danger vps-status">Đã hủy</td>';
            }
            else if(vps.status_vps == 'delete_vps') {
                html += '<td class="text-danger vps-status">Đã xóa</td>';
            }
            else if(vps.status_vps == 'change_user') {
                html += '<td class="text-danger vps-status">Đã chuyển</td>';
            }
            else if(vps.status_vps == 'rebuild') {
                html += '<td class="text-info vps-status">Đang cài lại</td>';
            }
            else if(vps.status_vps == 'change_ip') {
                html += '<td class="text-info vps-status">Đang đổi IP</td>';
            }
            else if(vps.status_vps == 'reset_password') {
                html += '<td class="text-info vps-status">Đang đặt lại mật khẩu</td>';
            }
            else if(vps.status_vps == 'expire') {
                html += '<td class="text-danger vps-status">Đã hết hạn</td>';
            }
            else {
                html += '<td class="text-secondary vps-status">Chưa tạo</td>';
            }
            html += '<td class="button-action">';
            if(vps.status_vps == 'on') {
                html += '<a href="/vps/console/'+ vps.id +'" target="_blank" class="btn btn-success"><i class="fas fa-desktop"></i></a>';
            }
            if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user' && vps.ip ) {
                if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                  html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                  html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                } else {
                  html += '<button class="btn mr-1 btn-sm btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                  html += '<button class="btn mr-1 btn-sm btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                }
                if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                    // console.log("da den");
                  html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                } else {
                  html += '<button class="btn mr-1 btn-sm btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                }
            }
            html += '</td>';
            // hành động
            html += '<td class="button-action">';
            if (vps.status_vps != 'delete_vps' && vps.status_vps != 'cancel' && vps.status_vps != 'change_user' ) {
              html += '<button class="btn btn-info button-action-vps" data-toggle="tooltip" data-action="change_user" data-placement="top" title="Chuyển khách hàng" data-id="' + vps.id + '" data-ip="' + vps.ip + '" ><i class="fas fa-exchange-alt"></i></button>';
            }
            html += '<a href="/admin/vps/detail/'+ vps.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>'
            html += '<button class="btn btn-danger btn-delete-vps" data-id="'+ vps.id +'"  data-action="delete" data-ip="'+ vps.ip +'" ><i class="fas fa-trash-alt"></i></button>';
            html += '</td>';

            html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        $('.first-item').text(data.firstItem);
        $('.last-item').text(data.lastItem);
        $('.total-item').text(data.total);
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        $('#qttVpsSearch').html(
            "<span class='text-primary'>Tổng cộng: " + total + "</span>"
        );
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="14" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="14" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps">';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_list_vps">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_list_vps">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);

    }

    $(document).on('click', '.pagination_list_vps a', function (event) {
        event.preventDefault();
        var type_user = $('#type_user_personal').val();
        var page = $(this).attr('data-page');
        var sort = $('.sort_type').val();
        var qtt = $('#qtt').val();
        if ( type_user == 'select_status_vps' ) {
          var q = $('#search').val();
          var status = $('#select_status_vps').val();
          $.ajax({
              url: '/admin/vps/select_status_vps',
              data: { status: status, q: q, sort:sort, page: page, qtt: qtt },
              dataType: 'json',
              beforeSend: function(){
                  var html = '<td  colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                  $('tbody').html(html);
              },
              success: function (data) {
                  // console.log(data);
                  var html = '';
                  if (data != '') {
                      screen_vps(data);
                  } else {
                      html = '<td  colspan="14" class="text-center text-danger">Không có VPS trong dữ liệu</td>';
                      $('tbody').html(html);
                  }
              },
              error: function (e) {
                  console.log(e);
                  var html = '<td  colspan="14" class="text-center text-danger">Truy vấn VPS lỗi!</td>';
                  $('tbody').html(html);
              }
          });
        }
        else if ( type_user == 'search_vps' ) {
          var q = $('#search').val();
          var status = $('#select_status_vps').val();
          $('#type_user_personal').val('search_vps');
          $.ajax({
              url: '/admin/vps/search',
              data: { q: q, status: status, sort:sort, qtt: qtt, page: page },
              dataType: 'json',
              beforeSend: function(){
                  var html = '<td  colspan="14" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                  $('tbody').html(html);
              },
              success: function (data) {
                  // console.log(data);
                  var html = '';
                  if (data.data != '') {
                      screen_vps(data);
                  } else {
                      html = '<td  colspan="14" class="text-center text-danger">Không có VPS trong dữ liệu</td>';
                      $('tbody').html(html);
                  }
              },
              error: function (e) {
                  console.log(e);
                  var html = '<td  colspan="14" class="text-center text-danger">Truy vấn VPS lỗi!</td>';
                  $('tbody').html(html);
              }
          });
        }
        else {
          if ( type_user ==  'personal' ) {
              var link = '/admin/vps/list_vps_of_personal';
          } else {
              var link = '/admin/vps/list_vps_of_enterprise';
          }
          // console.log(link);
          $.ajax({
              url: link,
              data: {page: page},
              dataType: 'json',
              beforeSend: function(){
                  var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                  $('tbody').html(html);
              },
              success: function (data) {
                  if (data.data != '') {
                      screen_list_vps(data);
                  } else {
                      var html = '';
                      html += '<td colspan="15" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                      $('tbody').html(html);
                      $('tfoot').html('');
                      $('.paginate_top').html('');
                  }
              },
              error: function (e) {
                console.log(e);
                var html = '';
                html += '<td colspan="15" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
          });
        }
    })

    $('.sort_next_due_date').on('click', function () {
      var vps_q = $('#search').val();
      var status = $('#select_status_vps').val();
      $('#type_user_personal').val('search_vps');
      var vps_sort = $(this).attr('data-sort');
      var qtt = $('#qtt').val();
      if (vps_sort == 'ASC') {
        $(this).attr('data-sort', 'DESC');
        $('.sort_type').val('ASC');
        $('.sort_next_due_date i').removeClass('fa-sort-down');
        $('.sort_next_due_date i').removeClass('fa-sort');
        $('.sort_next_due_date i').addClass('fa-sort-up');
      } else {
        $(this).attr('data-sort', 'ASC');
        $('.sort_type').val('DESC');
        $('.sort_next_due_date i').addClass('fa-sort-down');
        $('.sort_next_due_date i').removeClass('fa-sort');
        $('.sort_next_due_date i').removeClass('fa-sort-up');
      }
      $.ajax({
        url: '/admin/vps/search',
        dataType: 'json',
        data: { q: vps_q, sort: vps_sort, status: status, qtt: qtt },
        beforeSend: function(){
          var html = '<td class="text-center"  colspan="13"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          if ( data.data.length > 0 ) {
            screen_vps(data);
          } else {
            $('tbody').html('<td class="text-center text-danger"  colspan="13">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          $('tfoot').html('');
          $('tbody').html('<td class="text-center text-danger"  colspan="13">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
        }
      })
    })

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
