$(document).ready(function() {
  // load siderba top
  load_siderbar_top();
  function load_siderbar_top() {
    $.ajax({
        url: '/admin/load_siderbar_top',
        dataType: 'json',
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('.siderbar_top').html(html);
        },
        success: function (data) {
            // console.log(data);
            //Đơn hàng mới
            var html_total_order_pending = '';
            html_total_order_pending += '<h3>'+ data.total_order_pending +'</h3>';
            html_total_order_pending += '<p>Đơn hàng mới</p>';
            $('#total_order_pending').html(html_total_order_pending);
            //Đơn hàng đang chờ tọa
            var html_total_order_confirm = '';
            html_total_order_confirm += '<h3>'+ data.total_order_confirm +'</h3>';
            html_total_order_confirm += '<p>Đơn hàng đang chờ tạo</p>';
            $('#total_order_confirm').html(html_total_order_confirm);
            //VPS gần hết hạn
            var html_total_vps_before_termination = '';
            html_total_vps_before_termination += '<h3>'+ data.total_vps_before_termination +'</h3>';
            html_total_vps_before_termination += '<p>VPS gần hết hạn</p>';
            $('#total_vps_before_terminatio').html(html_total_vps_before_termination);
            //VPS US gần hết hạn
            var html_total_vps_us_before_termination = '';
            html_total_vps_us_before_termination += '<h3>'+ data.total_vps_us_before_termination +'</h3>';
            html_total_vps_us_before_termination += '<p>VPS US gần hết hạn</p>';
            $('#total_vps_us_before_termination').html(html_total_vps_us_before_termination);
            //Server gần hết hạn
            var html_total_server_before_termination = '';
            html_total_server_before_termination += '<h3>'+ data.total_server_before_termination +'</h3>';
            html_total_server_before_termination += '<p>Server gần hết hạn</p>';
            $('#total_server_before_termination').html(html_total_server_before_termination);
            //Server gần hết hạn
            var html_total_colo_before_termination = '';
            html_total_colo_before_termination += '<h3>'+ data.total_colo_before_termination +'</h3>';
            html_total_colo_before_termination += '<p>Colocation gần hết hạn</p>';
            $('#total_colo_before_termination').html(html_total_colo_before_termination);
            //Hosting gần hết hạn
            var html_total_hosting_before_termination = '';
            html_total_hosting_before_termination += '<h3>'+ data.total_hosting_before_termination +'</h3>';
            html_total_hosting_before_termination += '<p>Hosting gần hết hạn</p>';
            $('#total_hosting_before_termination').html(html_total_hosting_before_termination);
            //Hosting gần hết hạn
            var html_total_email_hosting_termination = '';
            html_total_email_hosting_termination += '<h3>'+ data.total_email_hosting_termination +'</h3>';
            html_total_email_hosting_termination += '<p>Email Hosting gần hết hạn</p>';
            $('#total_email_hosting_termination').html(html_total_email_hosting_termination);

        },
        error: function(e) {
            console.log(e);
            var html = '<div class="text-center text-danger">Truy vấn thất bại.</div>';
            $('.siderbar_top').html(html);
        }
    })
  }
  // Thông tin dịch vụ
  load_all_services();
  function load_all_services() {
    $.ajax({
        url: '/admin/load_all_services',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td  colspan="4" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('#list_services_on tbody').html(html);
        },
        success: function (data) {
            // console.log(data);
            //Đơn hàng mới
            var html = '';
            $.each(data, function (index, item) {
              html += '<tr>';
              html += '<td>'+ index +'</td>';
              html += '<td>'+ item.total +'</td>';
              html += '<td>'+ item.used +'</td>';
              html += '<td>'+ item.termination +'</td>';
              html += '</tr>';
            })
            $('#list_services_on tbody').html(html);
        },
        error: function(e) {
            console.log(e);
            var html = '<div class="text-center text-danger">Truy vấn thất bại.</div>';
            $('#list_services_on tbody').html(html);
        }
    })
  }
  // Thông tin tổng thu
  load_tong_thu();
  function load_tong_thu() {
    $.ajax({
        url: '/admin/load_tong_thu',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td  colspan="4" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('#tong_thu tbody').html(html);
        },
        success: function (data) {
            // console.log(data);
            //Đơn hàng mới
            var html = '';
            $.each(data, function (index, item) {
              html += '<tr>';
              html += '<td>'+ index +'</td>';
              html += '<td>'+ addCommas(item) +' VNĐ</td>';
              var t = (item / data.Total) * 100;
              t = Math.round(t, -2);
              html += '<td>'+ t +' %</td>';
              html += '</tr>';
            })
            $('#tong_thu tbody').html(html);
        },
        error: function(e) {
            console.log(e);
            var html = '<div class="text-center text-danger">Truy vấn thất bại.</div>';
            $('#tong_thu tbody').html(html);
        }
    })
  }

  chart_home();
  function chart_home() {
      $.ajax({
          url: '/admin/chart-dashboard',
          dataType: "json",
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('#pieChart').html(html);
          },
          success: function(result) {
            // console.log(result);
            var donutData        = {
              labels: [
                  'VPS',
                  'VPS-US',
                  'Hosting',
                  'Server',
                  'Colocation',
                  'Email Hosting',
              ],
              datasets: [
                {
                  data: [ result['vps'], result['vps_us'], result['hosting'], result['server'] , result['colocation'] , result['email_hosting'] ],
                  backgroundColor : ['#f56954', 'blue' , '#00a65a', '#f39c12', '#f7008b', '#f70000'],
                }
              ]
            }
            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
            var pieData        = donutData;
            var pieOptions     = {
              maintainAspectRatio : false,
              responsive : true,
            }
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var pieChart = new Chart(pieChartCanvas, {
              type: 'pie',
              data: pieData,
              options: pieOptions
            })
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('#pieChart').html(html);
          }
      });
  }
  // Thông tin tổng thu
  chart_2_home();
  function chart_2_home() {
      $.ajax({
          url: '/admin/chart-total-price',
          dataType: "json",
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('#pieChartTotal').html(html);
          },
          success: function(result) {
            // console.log(result);
            var donutData = {
              labels: [
                  'VPS',
                  'VPS-US',
                  'Hosting',
                  'Server',
                  'Colocation',
                  'Email Hosting',
              ],
              datasets: [
                {
                  data: [ result['vps'], result['vps_us'], result['hosting'], result['server'] , result['colocation'] , result['email_hosting'] ],
                  backgroundColor : ['#f56954', 'blue' , '#00a65a', '#f39c12', '#f7008b', '#f70000'],
                }
              ]
            };
            //-------------
            //- PIE CHART -
            //-------------
            // Get context with jQuery - using jQuery's .get() method.
            var pieChartCanvas = $('#pieChartTotal').get(0).getContext('2d')
            var pieData        = donutData;
            var pieOptions     = {
              maintainAspectRatio : false,
              responsive : true,
            };
            //Create pie or douhnut chart
            // You can switch between pie and douhnut using the method below.
            var pieChart = new Chart(pieChartCanvas, {
              type: 'pie',
              data: pieData,
              options: pieOptions
            });

          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('#pieChartTotal').html(html);
          }
      });
  }
  // đồ thị user theo ngày trong tháng
  $('#this_week').attr('disabled', true);
  pieChartUser('sort_select', '', '', '');
  function pieChartUser(type , sort1 = '' , sort2 = '' , sort3 = '') {
      $('.print_chart_user').html('<canvas id="pieChartUser" style="min-height: 400px; height: 400px; max-height: 400px; max-width: 100%;"></canvas>');
      $.ajax({
          url: '/admin/chart-user',
          data: { type: type, sort1: sort1, sort2: sort2 , sort3: sort3 },
          dataType: 'json',
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.errorPieChartUser').html(html);
          },
          success: function (result) {
              // console.log(result);
              var areaChartData = {
                labels  : result['time'],
                datasets: [
                  {
                    label               : 'User ' + result['percent'],
                    backgroundColor     : 'rgba(60,141,188,0.9)',
                    borderColor         : 'rgba(60,141,188,0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : result['data']
                  },
                ]
              }
              var areaChartOptions = {
                  maintainAspectRatio : false,
                  responsive : true,
                  legend: {
                    display: false
                  },
                  scales: {
                    xAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }],
                    yAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }]
                  }
              }
              var barChartCanvas = $('#pieChartUser').get(0).getContext('2d');
              var barChartData = jQuery.extend(true, {}, areaChartData);
              var temp0 = areaChartData.datasets[0];
              barChartData.datasets[0] = temp0;

              var barChartOptions = {
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
              };

              var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
              });
              $('.errorPieChartUser').html('');
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('.errorPieChartUser').html(html);
          }
      })
  }

  $('#last_week').on('click', function () {
      $('button').attr('disabled', false);
      pieChartUser('last_week', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#last_month').on('click', function () {
      $('button').attr('disabled', false);
      pieChartUser('last_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_week').on('click', function () {
      $('button').attr('disabled', false);
      pieChartUser('sort_select', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_month').on('click', function () {
      $('button').attr('disabled', false);
      pieChartUser('this_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#sort').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-5">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="sort_select">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-5 in_select_sort">';
      html += '</div>';
      html += '<div class="form-group col-md-2 submit_sort">';
      html += '</div>';
      $('#set_sort').html(html);
  })

  $(document).on('change', '#sort_select' ,function () {
      var sort = $(this).val();
      if (sort == 'day') {
            var html = '<label>Trong</label>';
            html += '<select class="form-control" id="in_sort_select">';
            html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
            html += '<option value="week">Tuần</option>';
            html += '<option value="month">Tháng</option>';
            html += '<option value="year">Năm</option>';
            html += '<select>';
            $('.in_select_sort').html(html);

      }
      else if (sort == 'week') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="month">Tháng</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort').html(html);
      }
      else if (sort == 'month') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort').html(html);
      }

      var html2 = '';
      html2 += '<label>Tạo biểu đồ theo</label>';
      html2 += '<button class="btn btn-block  btn-primary btn_submit" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      $('.submit_sort').html(html2);
  })

  $(document).on('change', '#in_sort_select', function () {
      $('.btn_submit').attr('disabled', false);
  })

  $(document).on('click', '.btn_submit', function () {
      var sort = $('#sort_select').val();
      var sort2 = $('#in_sort_select').val();
      pieChartUser('sort', sort, sort2, '');
  })

  $('#custom').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-4">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="custom_select">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort">';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort2">';
      html += '</div>';
      html += '<div class="form-group col-md-12 mt-4 text-center">';
      html += '<button class="btn  btn-primary" id="btn_submit_custom" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      html += '</div>';

      $('#set_sort').html(html);
  })

  $(document).on('change', '#custom_select', function () {
      var html = '<label>Từ ngày</label>';
      html += '<input class="data_date1 form-control" id="date1">';
      $('.in_select_sort').html(html);
      var html = '<label>Đến ngày ngày</label>';
      html += '<input class="data_date2 form-control" id="date2">';
      $('.in_select_sort2').html(html);
      $('#btn_submit_custom').attr('disabled', false);
      $('#date1').datepicker({
          autoclose: true
      });
      $('#date2').datepicker({
          autoclose: true
      });
  })

  $(document).on('click', '#btn_submit_custom', function () {
     var sort1 = $('#custom_select').val();
     var sort2 = $('#date1').val();
     var sort3 = $('#date2').val();
     pieChartUser('custom', sort1, sort2, sort3);
  })

  // Đồ thị VPS
  // đồ thị user theo ngày trong tháng
  $('#this_week_vps').attr('disabled', true);
  pieChartVps('sort_select', '', '', '');
  function pieChartVps(type , sort1 = '' , sort2 = '' , sort3 = '') {
      $('.print_chart_vps').html('<canvas id="pieChartVps" style="min-height: 400px; height: 400px; max-height: 400px; max-width: 100%;"></canvas>');
      $.ajax({
          url: '/admin/chart-vps',
          data: { type: type, sort1: sort1, sort2: sort2 , sort3: sort3 },
          dataType: 'json',
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.errorPieChartVps').html(html);
          },
          success: function (result) {
              // console.log(result);
              if ( result != false ) {
                var areaChartData = {
                  labels  : result['time'],
                  datasets: [
                    {
                      label               : 'VPS ' + result['percent'],
                      backgroundColor     : 'rgba(255, 102, 0, 0.9)',
                      borderColor         : 'rgba(255, 102, 0, 0.8)',
                      pointRadius          : false,
                      pointColor          : '#3b8bba',
                      pointStrokeColor    : 'rgba(255, 102, 0, 1)',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: 'rgba(255, 102, 0, 1)',
                      data                : result['data']
                    },
                    {
                      label               : 'Gia Hạn' + result['percent_expired'] ,
                      backgroundColor     : 'rgba(0, 170, 0, 1)',
                      borderColor         : 'rgba(0, 170, 0, 1)',
                      pointRadius         : false,
                      pointColor          : 'rgba(0, 170, 0, 1)',
                      pointStrokeColor    : '#c1c7d1',
                      pointHighlightFill  : '#fff',
                      pointHighlightStroke: 'rgba(0, 170, 0, 1)',
                      data                : result['data_expired']
                    },
                  ]
                }
                var areaChartOptions = {
                    maintainAspectRatio : false,
                    responsive : true,
                    legend: {
                      display: false
                    },
                    scales: {
                      xAxes: [{
                        gridLines : {
                          display : false,
                        }
                      }],
                      yAxes: [{
                        gridLines : {
                          display : false,
                        }
                      }]
                    }
                }
                var barChartCanvas = $('#pieChartVps').get(0).getContext('2d');
                var barChartData = jQuery.extend(true, {}, areaChartData);
                var temp0 = areaChartData.datasets[1]
                var temp1 = areaChartData.datasets[0]
                barChartData.datasets[0] = temp1
                barChartData.datasets[1] = temp0

                var barChartOptions = {
                  responsive              : true,
                  maintainAspectRatio     : false,
                  datasetFill             : false
                };

                var barChart = new Chart(barChartCanvas, {
                  type: 'bar',
                  data: barChartData,
                  options: barChartOptions
                });
                $('.errorPieChartVps').html('');
              } else {
                console.log('Lỗi');
              }
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('.errorPieChartVps').html(html);
          }
      })
  }

  $('#last_week_vps').on('click', function () {
      $('button').attr('disabled', false);
      pieChartVps('last_week', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#last_month_vps').on('click', function () {
      $('button').attr('disabled', false);
      pieChartVps('last_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_week_vps').on('click', function () {
      $('button').attr('disabled', false);
      pieChartVps('sort_select', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_month_vps').on('click', function () {
      $('button').attr('disabled', false);
      pieChartVps('this_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#sort_vps').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-5">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="sort_select_vps">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-5 in_select_sort_vps">';
      html += '</div>';
      html += '<div class="form-group col-md-2 submit_sort_vps">';
      html += '</div>';
      $('#set_sort_vps').html(html);
  })

  $(document).on('change', '#sort_select_vps' ,function () {
      var sort = $(this).val();
      if (sort == 'day') {
            var html = '<label>Trong</label>';
            html += '<select class="form-control" id="in_sort_select_vps">';
            html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
            html += '<option value="week">Tuần</option>';
            html += '<option value="month">Tháng</option>';
            html += '<option value="year">Năm</option>';
            html += '<select>';
            $('.in_select_sort_vps').html(html);

      }
      else if (sort == 'week') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_vps">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="month">Tháng</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_vps').html(html);
      }
      else if (sort == 'month') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_vps">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_vps').html(html);
      }

      var html2 = '';
      html2 += '<label>Tạo biểu đồ theo</label>';
      html2 += '<button class="btn btn-block  btn-primary btn_submit_vps" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      $('.submit_sort_vps').html(html2);
  })

  $(document).on('change', '#in_sort_select_vps', function () {
      $('.btn_submit_vps').attr('disabled', false);
  })

  $(document).on('click', '.btn_submit_vps', function () {
      var sort = $('#sort_select_vps').val();
      var sort2 = $('#in_sort_select_vps').val();
      // console.log(sort, sort2);
      pieChartVps('sort', sort, sort2, '');
  })

  $('#custom_vps').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-4">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="custom_select_vps">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort_vps">';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort2_vps">';
      html += '</div>';
      html += '<div class="form-group col-md-12 mt-4 text-center">';
      html += '<button class="btn  btn-primary" id="btn_submit_custom_vps" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      html += '</div>';

      $('#set_sort_vps').html(html);
  })

  $(document).on('change', '#custom_select_vps', function () {
      var html = '<label>Từ ngày</label>';
      html += '<input class="data_date1 form-control" id="date1">';
      $('.in_select_sort_vps').html(html);
      var html = '<label>Đến ngày ngày</label>';
      html += '<input class="data_date2 form-control" id="date2">';
      $('.in_select_sort2_vps').html(html);
      $('#btn_submit_custom_vps').attr('disabled', false);
      $('#date1').datepicker({
          autoclose: true
      });
      $('#date2').datepicker({
          autoclose: true
      });
  })

  $(document).on('click', '#btn_submit_custom_vps', function () {
     var sort1 = $('#custom_select_vps').val();
     var sort2 = $('#date1').val();
     var sort3 = $('#date2').val();
     pieChartVps('custom', sort1, sort2, sort3);
  })
  // Đồ thị HostingController
  // đồ thị user theo ngày trong tháng
  $('#this_week_hosting').attr('disabled', true);
  pieChartHosting('sort_select', '', '', '');
  function pieChartHosting(type , sort1 = '' , sort2 = '' , sort3 = '') {
      $('.print_chart_hosting').html('<canvas id="pieChartHosting" style="min-height: 400px; height: 400px; max-height: 400px; max-width: 100%;"></canvas>');
      $.ajax({
          url: '/admin/chart-hosting',
          data: { type: type, sort1: sort1, sort2: sort2 , sort3: sort3 },
          dataType: 'json',
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.errorPieChartHosting').html(html);
          },
          success: function (result) {
              // console.log(result);
              var areaChartData = {
                labels  : result['time'],
                datasets: [
                  {
                    label               : 'Hosting ' + result['percent'],
                    backgroundColor     : 'rgba(255, 0, 153, 0.9)',
                    borderColor         : 'rgba(255, 0, 153, 0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(255, 0, 153, 1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(255, 0, 153, 1)',
                    data                : result['data']
                  },
                  {
                    label               : 'Gia Hạn' + result['percent_expired'] ,
                    backgroundColor     : 'rgba(102, 255, 255, 1)',
                    borderColor         : 'rgba(102, 255, 255, 1)',
                    pointRadius         : false,
                    pointColor          : 'rgba(102, 255, 255, 1)',
                    pointStrokeColor    : '#c1c7d1',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(102, 255, 255, 1)',
                    data                : result['data_expired']
                  },
                ]
              }
              var areaChartOptions = {
                  maintainAspectRatio : false,
                  responsive : true,
                  legend: {
                    display: false
                  },
                  scales: {
                    xAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }],
                    yAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }]
                  }
              }
              var barChartCanvas = $('#pieChartHosting').get(0).getContext('2d');
              var barChartData = jQuery.extend(true, {}, areaChartData);
              var temp0 = areaChartData.datasets[1]
              var temp1 = areaChartData.datasets[0]
              barChartData.datasets[0] = temp1
              barChartData.datasets[1] = temp0

              var barChartOptions = {
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
              };

              var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
              });
              $('.errorPieChartHosting').html('');
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('.errorPieChartHosting').html(html);
          }
      })
  }

  $('#last_week_hosting').on('click', function () {
      $('button').attr('disabled', false);
      pieChartHosting('last_week', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#last_month_hosting').on('click', function () {
      $('button').attr('disabled', false);
      pieChartHosting('last_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_week_hosting').on('click', function () {
      $('button').attr('disabled', false);
      pieChartHosting('sort_select', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_month_hosting').on('click', function () {
      $('button').attr('disabled', false);
      pieChartHosting('this_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#sort_hosting').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-5">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="sort_select_hosting">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-5 in_select_sort_hosting">';
      html += '</div>';
      html += '<div class="form-group col-md-2 submit_sort_hosting">';
      html += '</div>';
      $('#set_sort_hosting').html(html);
  })

  $(document).on('change', '#sort_select_hosting' ,function () {
      var sort = $(this).val();
      if (sort == 'day') {
            var html = '<label>Trong</label>';
            html += '<select class="form-control" id="in_sort_select_hosting">';
            html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
            html += '<option value="week">Tuần</option>';
            html += '<option value="month">Tháng</option>';
            html += '<option value="year">Năm</option>';
            html += '<select>';
            $('.in_select_sort_hosting').html(html);

      }
      else if (sort == 'week') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_hosting">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="month">Tháng</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_hosting').html(html);
      }
      else if (sort == 'month') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_hosting">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_hosting').html(html);
      }

      var html2 = '';
      html2 += '<label>Tạo biểu đồ theo</label>';
      html2 += '<button class="btn btn-block  btn-primary btn_submit_hosting" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      $('.submit_sort_hosting').html(html2);
  })

  $(document).on('change', '#in_sort_select_hosting', function () {
      $('.btn_submit_hosting').attr('disabled', false);
  })

  $(document).on('click', '.btn_submit_hosting', function () {
      var sort = $('#sort_select_hosting').val();
      var sort2 = $('#in_sort_select_hosting').val();
      // console.log(sort, sort2);
      pieChartHosting('sort', sort, sort2, '');
  })

  $('#custom_hosting').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-4">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="custom_select_hosting">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort_hosting">';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort2_hosting">';
      html += '</div>';
      html += '<div class="form-group col-md-12 mt-4 text-center">';
      html += '<button class="btn  btn-primary" id="btn_submit_custom_hosting" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      html += '</div>';

      $('#set_sort_hosting').html(html);
  })

  $(document).on('change', '#custom_select_hosting', function () {
      var html = '<label>Từ ngày</label>';
      html += '<input class="data_date1 form-control" id="date1">';
      $('.in_select_sort_hosting').html(html);
      var html = '<label>Đến ngày ngày</label>';
      html += '<input class="data_date2 form-control" id="date2">';
      $('.in_select_sort2_hosting').html(html);
      $('#btn_submit_custom_hosting').attr('disabled', false);
      $('#date1').datepicker({
          autoclose: true
      });
      $('#date2').datepicker({
          autoclose: true
      });
  })

  $(document).on('click', '#btn_submit_custom_hosting', function () {
     var sort1 = $('#custom_select_hosting').val();
     var sort2 = $('#date1').val();
     var sort3 = $('#date2').val();
     pieChartHosting('custom', sort1, sort2, sort3);
  })

  // Đồ thị PaymentController
  // đồ thị payment theo ngày trong tháng
  $('#this_week_payment').attr('disabled', true);
  pieChartPayment('sort_select', '', '', '');
  function pieChartPayment(type , sort1 = '' , sort2 = '' , sort3 = '') {
      $('.print_chart_payment').html('<canvas id="pieChartPayment" style="min-height: 400px; height: 400px; max-height: 400px; max-width: 100%;"></canvas>');
      $.ajax({
          url: '/admin/chart-payment',
          data: { type: type, sort1: sort1, sort2: sort2 , sort3: sort3 },
          dataType: 'json',
          beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.errorPieChartPayment').html(html);
          },
          success: function (result) {
              // console.log(result);
              var areaChartData = {
                labels  : result['time'],
                datasets: [
                  {
                    label               : 'Nạp tiền ' + result['percent'],
                    backgroundColor     : 'rgba(255, 51, 51, 0.9)',
                    borderColor         : 'rgba(255, 51, 51, 0.8)',
                    pointRadius          : false,
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(255, 51, 51, 1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(255, 51, 51, 1)',
                    data                : result['data']
                  },
                ]
              }
              var areaChartOptions = {
                  maintainAspectRatio : false,
                  responsive : true,
                  legend: {
                    display: false
                  },
                  scales: {
                    xAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }],
                    yAxes: [{
                      gridLines : {
                        display : false,
                      }
                    }]
                  }
              }
              var barChartCanvas = $('#pieChartPayment').get(0).getContext('2d');
              var barChartData = jQuery.extend(true, {}, areaChartData);
              var temp1 = areaChartData.datasets[0]
              barChartData.datasets[0] = temp1

              var barChartOptions = {
                responsive              : true,
                maintainAspectRatio     : false,
                datasetFill             : false
              };

              var barChart = new Chart(barChartCanvas, {
                type: 'bar',
                data: barChartData,
                options: barChartOptions
              });
              $('.errorPieChartPayment').html('');
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center text-danger">Truy vấn bản đồ thất bại.</div>';
              $('.errorPieChartPayment').html(html);
          }
      })
  }

  $('#last_week_payment').on('click', function () {
      $('button').attr('disabled', false);
      pieChartPayment('last_week', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#last_month_payment').on('click', function () {
      $('button').attr('disabled', false);
      pieChartPayment('last_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_week_payment').on('click', function () {
      $('button').attr('disabled', false);
      pieChartPayment('sort_select', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#this_month_payment').on('click', function () {
      $('button').attr('disabled', false);
      pieChartPayment('this_month', '', '', '');
      $(this).attr('disabled', true);
  });

  $('#sort_payment').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-5">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="sort_select_payment">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-5 in_select_sort_payment">';
      html += '</div>';
      html += '<div class="form-group col-md-2 submit_sort_payment">';
      html += '</div>';
      $('#set_sort_payment').html(html);
  })

  $(document).on('change', '#sort_select_payment' ,function () {
      var sort = $(this).val();
      if (sort == 'day') {
            var html = '<label>Trong</label>';
            html += '<select class="form-control" id="in_sort_select_payment">';
            html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
            html += '<option value="week">Tuần</option>';
            html += '<option value="month">Tháng</option>';
            html += '<option value="year">Năm</option>';
            html += '<select>';
            $('.in_select_sort_payment').html(html);

      }
      else if (sort == 'week') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_payment">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="month">Tháng</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_payment').html(html);
      }
      else if (sort == 'month') {
          var html = '<label>Trong</label>';
          html += '<select class="form-control" id="in_sort_select_payment">';
          html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
          html += '<option value="year">Năm</option>';
          html += '<select>';
          $('.in_select_sort_payment').html(html);
      }

      var html2 = '';
      html2 += '<label>Tạo biểu đồ theo</label>';
      html2 += '<button class="btn btn-block  btn-primary btn_submit_payment" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      $('.submit_sort_payment').html(html2);
  })

  $(document).on('change', '#in_sort_select_payment', function () {
      $('.btn_submit_payment').attr('disabled', false);
  })

  $(document).on('click', '.btn_submit_payment', function () {
      var sort = $('#sort_select_payment').val();
      var sort2 = $('#in_sort_select_payment').val();
      // console.log(sort, sort2);
      pieChartPayment('sort', sort, sort2, '');
  })

  $('#custom_payment').on('click', function () {
      $('button').attr('disabled', false);
      $(this).attr('disabled', true);
      var html = '<div class="form-group col-md-4">';
      html += '<label>Sắp xếp theo</label>';
      html += '<select class="form-control" id="custom_select_payment">';
      html += '<option disabled selected>--- Chọn sắp xếp ---</option>';
      html += '<option value="day">Ngày</option>';
      html += '<option value="week">Tuần</option>';
      html += '<option value="month">Tháng</option>';
      html += '<option value="year" disabled>Năm</option>';
      html += '<select>';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort_payment">';
      html += '</div>';
      html += '<div class="form-group col-md-4 in_select_sort2_payment">';
      html += '</div>';
      html += '<div class="form-group col-md-12 mt-4 text-center">';
      html += '<button class="btn  btn-primary" id="btn_submit_custom_payment" disabled><i class="fas fa-sync-alt"></i> Xác nhận</button>';
      html += '</div>';

      $('#set_sort_payment').html(html);
  })

  $(document).on('change', '#custom_select_payment', function () {
      var html = '<label>Từ ngày</label>';
      html += '<input class="data_date1 form-control" id="date1">';
      $('.in_select_sort_payment').html(html);
      var html = '<label>Đến ngày ngày</label>';
      html += '<input class="data_date2 form-control" id="date2">';
      $('.in_select_sort2_payment').html(html);
      $('#btn_submit_custom_payment').attr('disabled', false);
      $('#date1').datepicker({
          autoclose: true
      });
      $('#date2').datepicker({
          autoclose: true
      });
  })

  $(document).on('click', '#btn_submit_custom_payment', function () {
     var sort1 = $('#custom_select_payment').val();
     var sort2 = $('#date1').val();
     var sort3 = $('#date2').val();
     pieChartPayment('custom', sort1, sort2, sort3);
  })

  // ./update description
  function addCommas(nStr)
  {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
  }

});
