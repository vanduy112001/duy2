$(document).ready(function () {
    load_vps();
    var $vpsCheckbox = $('.vps_checkbox');
    var lastChecked = null;

    function load_vps() {
        $.ajax({
            url: '/dich-vu/vps/list_vps_nearly_and_expire',
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data.data);
                if ( data.data.length > 0 ) {
                  list_vps_nearly_and_expire_screent(data);
                  $("#data-vps-table").DataTable({
                      "columnDefs": [
                           { "targets": [  2, 4 , 5, 7, 8, 9, 10 ], "orderable": false }
                      ],
                      "pageLength": 30,
                      "lengthMenu": [[ 25, 50, 100, -1], [25, 50, 100, "All"]],
                      "iDisplayLength": 25
                  });
                } else {
                  $('tfoot').html('');
                  $('tbody').html('<td class="text-center text-danger"  colspan="10">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tfoot').html('');
                $('tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    }

    function list_vps_nearly_and_expire_screent(data) {
       // console.log(data);
       var html = '';
       $.each(data.data, function (index , vps) {
          html += '<tr>';
          // checkbox
          html += '<td><input type="checkbox" value="'+ vps.id +'" data-ip="'+ vps.ip +'" class="vps_checkbox"></td>';
          // ip
          html += '<td class="ip_vps" data-ip="'+ vps.ip +'"><a href="/service/detail/'+ vps.id +'?type=vps">'+ vps.ip +'</a>';
          html += '</td>';
          // cau hinh
          html += '<td>'+ vps.text_vps_config +'</td>';
          // loai
          if (vps.type_vps == 'vps') {
            html += '<td class="type_vps" data-type="vps">VPS</td>';
          } else {
            html += '<td class="type_vps" data-type="nat">NAT-VPS</td>';
          }
          // khach hang
          // html += '<td>' + vps.link_customer;
          // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
          // html += '</td>';
          // ngay tao
          html += '<td><span>' + vps.date_create + '</span></td>';
          // ngay ket thuc
          html += '<td class="next_due_date">';
          if (vps.isExpire == true || vps.expired == true) {
            html += '<span class="text-danger">' + vps.next_due_date + '<span>';
            html += ' - <span class="text-danger">' + vps.text_next_due_date + '</span>';
          } else {
            html += '<span>' + vps.next_due_date + '<span>';
          }
          html += '</td>';
          // thoi gian thue
          html += '<td>' + vps.text_billing_cycle +  '</td>';
          // giá
          html += '<td>' + addCommas(vps.price_vps) + ' VNĐ</td>';
          html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
          html += '<td class="page-service-action">';
          if (vps.status_vps != 'delete_vps' ) {
            if (vps.status_vps != 'cancel') {
              if (vps.status_vps != 'suspend') {
                if (vps.isExpire == true || vps.expired == true) {
                  html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                } else {
                  if (vps.status_vps != 'admin_off') {
                    if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                      html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                      html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                    } else {
                      html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                      html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                    }
                    if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                      html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                    } else {
                      html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                    }
                    if (vps.status_vps != 'cancel') {
                      html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                  }
                }
              }
            }
          }
          html += '</td>';
          html += '</tr>';
       })
       $('tbody').html(html);
       $('[data-toggle="tooltip"]').tooltip();
       // phân trang cho vps
       $vpsCheckbox = $('.vps_checkbox');
    }

    $(document).on('click', '.vps_checkbox_all', function () {
        if ( $(this).is(':checked') ) {
          $('.vps_checkbox').prop('checked', this.checked);
          $('.list_list_vps_nearly_and_expire tbody tr').addClass('action-vps');
        } else {
          $('.vps_checkbox').prop('checked', this.checked);
          $('.list_list_vps_nearly_and_expire tbody tr').removeClass('action-vps');
        }
    })

    // Lưu các checkbox vps
    $(document).on('click', '.vps_checkbox' , function(e) {
        if( $(this).is(':checked') ) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if ( e.shiftKey ) {
            // console.log('da click 2');
            var start = $vpsCheckbox.index(this);
            var end = $vpsCheckbox.index(lastChecked);
            $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
            $.each( $vpsCheckbox , function (index , value) {
                if( $(this).is(':checked') ) {
                    $(this).closest('tr').addClass('action-vps');
                } else {
                    $(this).closest('tr').removeClass('action-vps');
                }
            })
        }
        lastChecked = this;
    });

    $(document).on('click','.button-action-vps', function () {
        $('tr').removeClass('action-row-vps');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $('#button-service').fadeIn();
        $('#modal-service').modal('show');
        switch (action) {
          case 'expired':
              $('#modal-service .modal-title').text('Yêu cầu gia hạn VPS');
              $('#notication-service').html('<span>Bạn có muốn gia hạn vps <b class="text-danger">(ip: '+ ip +')</b> này không </span>');
              $.ajax({
                  url: '/service/request_expired_vps',
                  data: {list_vps: id},
                  dataType: 'json',
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('#button-service').attr('disabled', true);
                      $('#notication-service').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      if (data.error == 9998) {
                          $('#notication-service').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                          $('#button-service').attr('disabled', false);
                      } else {
                          // console.log(data);
                          if (data.expire_billing_cycle == true) {
                              html += '<div class="form-group">';
                              html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                              html += '<div class="mt-3 mb-3">';
                              html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                              html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                              html += '<option value="'+ data.price_override.billing_cycle +'">'+ data.price_override.text_billing_cycle +' / '+ data.price_override.total +'</option>';
                              html += '</select>';
                              html += '</div>';
                              html += '</div>';
                              $('#notication-service').html(html);
                              $('#button-service').attr('disabled', false);
                              $('.select2').select2();
                          } else {
                              html += '<div class="form-group">';
                              html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                              html += '<div class="mt-3 mb-3">';
                              html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                              html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                              if ( data.total['monthly'] !=  '0 VNĐ' ) {
                                html += '<option value="monthly">1 Tháng / '+ data.total['monthly'] +'</option>';
                              }
                              if ( data.total['twomonthly'] !=  '0 VNĐ' ) {
                                html += '<option value="twomonthly">2 Tháng / '+ data.total['twomonthly'] +'</option>';
                              }
                              if ( data.total['quarterly'] !=  '0 VNĐ' ) {
                                html += '<option value="quarterly">3 Tháng / '+ data.total['quarterly'] +'</option>';
                              }
                              if ( data.total['semi_annually'] !=  '0 VNĐ') {
                                html += '<option value="semi_annually">6 Tháng / '+ data.total['semi_annually'] +'</option>';
                              }
                              if ( data.total['annually'] !=  '0 VNĐ' ) {
                                html += '<option value="annually">1 Năm / '+ data.total['annually'] +'</option>';
                              }
                              if ( data.total['biennially'] !=  '0 VNĐ' ) {
                                html += '<option value="biennially">2 Năm / '+ data.total['biennially'] +'</option>';
                              }
                              if ( data.total['triennially'] !=  '0 VNĐ' ) {
                                html += '<option value="triennially">3 Năm / '+ data.total['triennially'] +'</option>';
                              }
                              html += '</select>';
                              html += '</div>';
                              html += '</div>';
                              $('#notication-service').html(html);
                              $('#button-service').attr('disabled', false);
                              $('.select2').select2();
                          }
                      }
                  },
                  error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                    $('#button-terminated').attr('disabled', false);
                  },
                });
            break;
          case 'off':
                $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ VPS');
                $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ VPS <b class="text-danger">(ip: '+ ip +')</b> này không ?</span>');
            break;
          case 'on':
                $('#modal-service .modal-title').text('Yêu cầu bật lại dịch vụ VPS');
                $('#notication-service').html('<span>Bạn có muốn bật lại dịch vụ VPS <b class="text-danger">(ip: '+ ip +')</b> này không ?</span>');
            break;
          case 'restart':
                $('#modal-service .modal-title').text('Yêu cầu khởi động lại dịch vụ VPS');
                $('#notication-service').html('<span>Bạn có muốn khởi động lại dịch vụ VPS <b class="text-danger">(ip: '+ ip +')</b> này không ?</span>');
            break;
          case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ VPS');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ VPS <b class="text-danger">(ip: '+ ip +')</b> này không ?</span>');
            break;
        }

        $('#button-service').attr('data-id', id);
        $('#button-service').attr('data-action', action);
        $('#button-service').attr('data-ip', ip);
        $('#button-service').attr('data-type', 'vps');
        $(this).closest('tr').addClass('action-row-vps');
        $('#button-service').fadeIn();
        $('#button-finish').fadeOut();
    });

    $('#button-service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var type = $(this).attr('data-type');
        var token = $('meta[name="csrf-token"]').attr('content');
        if (type == 'hosting') {
          var domain = $(this).attr('data-domain');
        } else if (type == 'vps') {
          var ip = $(this).attr('data-ip');
        }
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/action',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, type: type, action: action, billing_cycle: billing_cycle},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS do Admin off. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                         else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'mở lại hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'mở lại hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Dịch vụ được chọn không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.",
                            })
                        }
                        $('#modal-service').modal('hide');
                        $('#button-service').attr('disabled', false);
                    } else {
                        switch (type) {
                          case 'hosting':
                              if (action == 'expired') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'Hosting',
                                      subtitle: 'gia hạn',
                                      body: "Yêu cầu gia hạn hosting thành công",
                                  });
                                  $('.action-row .expired').fadeOut();
                                  $('#button-service').attr('disabled', false);
                                  html += '<span class="text-center">Yêu cầu gia hạn hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                  $('#notication-service').html(html);
                                  $('#button-service').fadeOut();
                                  $('#button-finish').fadeIn();
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'suspend') {
                                  $(document).Toasts('create', {
                                      class: 'bg-warning',
                                      title: 'Hosting',
                                      subtitle: 'suspend',
                                      body: "Tắt hosting thành công",
                                  });
                                  $('.action-row .suspend').attr('disabled', true);
                                  $('.action-row .unsuspend').attr('disabled', false);
                                  $('.action-row .hosting-status').html('<span class="text-danger">Đã tắt</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'unsuspend') {
                                  $(document).Toasts('create', {
                                      class: 'bg-info',
                                      title: 'Hosting',
                                      subtitle: 'unsuspend',
                                      body: "Mở lại hosting thành công",
                                  });
                                  $('.action-row .suspend').attr('disabled', false);
                                  $('.action-row .unsuspend').attr('disabled', true);
                                  $('.action-row .hosting-status').html('<span class="text-success">Đã bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'terminated') {
                                  $(document).Toasts('create', {
                                      class: 'bg-danger',
                                      title: 'Hosting',
                                      subtitle: 'terminated',
                                      body: "Hủy dịch vụ hosting thành công",
                                  });
                                  $('.action-row .terminated').fadeOut();
                                  $('.action-row .hosting-status').html('<span class="text-danger">Đã hủy</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                            break;
                          case 'vps':
                              if (action == 'expired') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'gia hạn',
                                      body: "Yêu cầu gia hạn vps thành công",
                                  });
                                  $('.action-row-vps .expired').fadeOut();
                                  $('#button-service').attr('disabled', false);
                                  html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                  $('#notication-service').html(html);
                                  $('#button-service').fadeOut();
                                  $('#button-finish').fadeIn();
                                  $('tr').removeClass('action-row-vps');
                                  $('#button-finish').attr('data-type', 'expired');
                                  $('#button-finish').html('Thanh toán');
                                  $('#button-finish').attr('data-link', '/order/check-invoices/'+data);
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'off') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'off',
                                      body: "Yêu cầu tắt VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', true);
                                  $('.action-row-vps .on').attr('disabled', false);
                                  $('.action-row-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'on') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'on',
                                      body: "Yêu cầu bật VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', false);
                                  $('.action-row-vps .on').attr('disabled', true);
                                  $('.action-row-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'restart') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'on',
                                      body: "Yêu cầu khởi động lại VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', false);
                                  $('.action-row-vps .on').attr('disabled', true);
                                  $('.action-row-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                                  setTimeout(function(){
                                      html = '<span class="text-danger">Đã bật</span>';
                                      $('.action-row-vps .vps-status').html(html);
                                  }, 40000);
                              }
                              else if (action == 'terminated') {
                                  $(document).Toasts('create', {
                                      class: 'bg-danger',
                                      title: 'VPS',
                                      subtitle: 'terminated',
                                      body: "Hủy dịch vụ VPS thành công",
                                  });
                                  $('.action-row-vps .terminated').fadeOut();
                                  $('.action-row-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                            break;
                        }
                        // $('#modal-service').modal('hide');
                        // $('#button-service').attr('disabled', false);
                    }
                } else {
                  $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                  $('#button-service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button-service').attr('disabled', false);
            }
        });
    });

    $('.btn-action-vps').on('click', function () {
        var action = $(this).attr('data-type');
        var checked = $('.vps_checkbox:checked');
        $('#button-terminated').fadeIn();
        $('#button-rebuil').fadeOut();
        $('#button-multi-finish').fadeOut();
        if (checked.length > 0) {
          $('#modal-services').modal('show');
          switch (action) {
            case 'delete':
              $('.modal-title').text('Hủy dịch vụ VPS');
              $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ VPS này không?</span>');
              $('#button-terminated').attr('data-type', 'vps');
              $('#button-terminated').attr('data-action', action);
              break;
            case 'expired':
              $('.modal-title').text('Yêu cầu gia hạn dịch vụ VPS');
              var list_vps = [];
              $.each(checked, function(index, value) {
                  list_vps.push($(this).val());
              })
              $.ajax({
                  url: '/service/request_expired_vps',
                  data: {list_vps: list_vps},
                  dataType: 'json',
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('#button-terminated').attr('disabled', true);
                      $('#notication-invoice').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      if (data.error == 9998) {
                          $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                          $('#button-terminated').attr('disabled', false);
                      }
                      else {
                          if (data.price_override != '') {
                              if (data.price_override.error == 0) {
                                html += '<div class="form-group">';
                                html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                html += '<div class="mt-3 mb-3">';
                                html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                html += '<option value="'+ data.price_override.billing_cycle +'">'+ data.price_override.text_billing_cycle +' / '+ data.price_override.total +'</option>';
                                html += '</select>';
                                html += '</div>';
                                html += '</div>';
                                $('#notication-invoice').html(html);
                                $('#button-terminated').attr('data-action', action);
                                $('#button-terminated').attr('data-type', 'vps');
                                $('#button-terminated').attr('disabled', false);
                                $('.select_expired').select2();
                              } else {
                                $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                $('#button-terminated').attr('disabled', false);
                              }
                          } else {
                              html += '<div class="form-group">';
                              html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                              html += '<div class="mt-3 mb-3">';
                              html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                              html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                              if ( data.total['monthly'] !=  '0 VNĐ' ) {
                                html += '<option value="monthly">1 Tháng / '+ data.total['monthly'] +'</option>';
                              }
                              if ( data.total['twomonthly'] !=  '0 VNĐ' ) {
                                html += '<option value="twomonthly">2 Tháng / '+ data.total['twomonthly'] +'</option>';
                              }
                              if ( data.total['quarterly'] !=  '0 VNĐ' ) {
                                html += '<option value="quarterly">3 Tháng / '+ data.total['quarterly'] +'</option>';
                              }
                              if ( data.total['semi_annually'] !=  '0 VNĐ') {
                                html += '<option value="semi_annually">6 Tháng / '+ data.total['semi_annually'] +'</option>';
                              }
                              if ( data.total['annually'] !=  '0 VNĐ' ) {
                                html += '<option value="annually">1 Năm / '+ data.total['annually'] +'</option>';
                              }
                              if ( data.total['biennially'] !=  '0 VNĐ' ) {
                                html += '<option value="biennially">2 Năm / '+ data.total['biennially'] +'</option>';
                              }
                              if ( data.total['triennially'] !=  '0 VNĐ' ) {
                                html += '<option value="triennially">3 Năm / '+ data.total['triennially'] +'</option>';
                              }
                              html += '</select>';
                              html += '</div>';
                              html += '</div>';
                              $('#notication-invoice').html(html);
                              $('#button-terminated').attr('data-action', action);
                              $('#button-terminated').attr('data-type', 'vps');
                              $('#button-terminated').attr('disabled', false);
                              $('.select_expired').select2();
                          }
                      }
                  },
                  error: function (e) {
                    console.log(e);
                    $('#notication-invoice').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                    $('#button-terminated').attr('disabled', false);
                  },
              })
              break;
          }
        } else {
            alert('Vui lòng chọn một VPS.');
        }
    });

    // modal action của hosting
    $('#button-terminated').on('click', function () {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        if (type == 'hosting') {
            var checked = $('.hosting_checkbox:checked');
        } else if (type == 'vps') {
            var checked = $('.vps_checkbox:checked');
        }
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function(index, value) {
            id_services.push($(this).val());
        })
        // console.log(id_services);
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: {'_token': token, type: type, id: id_services,action: action, billing_cycle: billing_cycle},
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');

                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        }
                        else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        $('#modal-services').modal('hide');
                        $('#button-terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        if (action == 'expired') {
                          $(document).Toasts('create', {
                              class: 'bg-success',
                              title: 'VPS',
                              subtitle: 'gia hạn',
                              body: "Thao tác yêu cầu gia hạn các dịch vụ VPS thành công.",
                          })
                          $('#vps .action-vps .expired').fadeOut();
                          $('#button-terminated').attr('disabled', false);
                          html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                          $('#notication-invoice').html(html);
                          $('#button-terminated').fadeOut();
                          $('#button-multi-finish').fadeIn();
                          $('#button-multi-finish').text('Thanh toán');
                          $('#button-multi-finish').attr('data-type', 'expired');
                          $('#button-multi-finish').attr('data-link', '/order/check-invoices/'+data);
                        }
                        else if ( action == 'delete' ) {
                          html = '<span class="text-danger">Đã hủy</span>';
                          $('#vps .action-vps .vps-status').html(html);
                          $(document).Toasts('create', {
                            class: 'bg-success',
                            title: 'VPS',
                            subtitle: 'hủy dịch vụ',
                            body: "Thao tác hủy các dịch vụ vps thành công.",
                          })
                          $('#modal-services').modal('hide');
                          $('#button-terminated').attr('disabled', false);
                          $('#vps .action-vps .off').attr('disabled', false);
                          $('#vps .action-vps .on').attr('disabled', true);
                          $('#button-multi-finish').fadeIn();
                        }
                    }
                    $('tr').removeClass('action-services');
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
            },
            error: function (e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service').fadeOut();
                $('#button-finish').fadeIn();
            }
        })
    });

    $('#button-multi-finish').on('click', function () {
      if ( $(this).attr('data-type') == 'expired' ) {
        let link = $(this).attr('data-link');
        window.location.href = link;
      }
    })

    $('#button-finish').on('click', function () {
      if ( $(this).attr('data-type') == 'expired' ) {
        let link = $(this).attr('data-link');
        window.location.href = link;
      }
    })

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

})
