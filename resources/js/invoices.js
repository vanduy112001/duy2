$(document).ready(function () {
    $('.invoice_checkbox').on('click', function() {
        if($(this).is(':checked')) {
            $(this).closest('tr').addClass('remove-invoices');
        } else {
            $(this).closest('tr').removeClass('remove-invoices');
        }
    });

    $('.btn-action-invoice').on('click', function () {
        var value = $('.invoice_checkbox:checked');
        var type = $(this).attr('data-type');
        if (value.length > 0) {
            var checkbox_value = [];
            $(value).each(function(){
                checkbox_value.push($(this).val());
            });
            // JSON.stringify(checkbox_value)
            localStorage.setItem("value", checkbox_value);
            localStorage.setItem('type', type);
            if(type == 'mark_paid') {
                $('#delete-order').modal('show');
                $('.modal-title').text('Gán thanh toán cho hóa đơn');
                $('#notication-order').html('<b class="text-danger">Bạn có muốn gán thanh toán cho tất cả các hóa đơn được chọn không?</b>');
                $('#button-order').val('Thanh toán');

            } else if(type == 'unpaid') {
                $('#delete-order').modal('show');
                $('.modal-title').text('Gán chưa thanh toán cho hóa đơn');
                $('#notication-order').html('<b class="text-danger">Bạn có muốn gán chưa thanh toán cho tất cả các hóa đơn được chọn không?</b>');
                $('#button-order').val('Chưa thanh toán');
            } else if(type == 'cancel') {
                $('#delete-order').modal('show');
                $('.modal-title').text('Gán hủy cho hóa đơn');
                $('#notication-order').html('<b class="text-danger">Bạn có muốn gán chưa hủy cho tất cả các hóa đơn được chọn không?</b>');
                $('#button-order').val('Hủy hóa đơn');
            } else {
                $('#delete-order').modal('show');
                $('.modal-title').text('Xóa đơn hàng và hóa đơn');
                $('#notication-order').html('<b class="text-danger">Bạn có muốn xóa tất cả các hóa đơn được chọn không?</b>');
                $('#button-order').val('Xóa đơn hàng');
            }
        } else {
            alert('Chọn ít nhất một hóa đơn');
        }
    });

    $('#button-order').on('click', function () {
        var value = localStorage.getItem('value');
        var type = localStorage.getItem('type');
        var href = "/admin/invoices/invoices-paid?type="+type+'&value='+value;
        localStorage.removeItem("type");
        localStorage.removeItem("value");
        window.location.href = href;
    });

    $(document).on('click', '.btn-delete-invoices' , function () {

        var id = $(this).attr('data-id');
        $(this).closest('tr').addClass('remove-row');
        $('#delete-invoice').modal('show');
        $('.modal-title').text('Xóa hóa đơn');
        $('#notication-invoice').html('Bạn có muốn xóa hóa đơn <b class="text-danger">(ID: '+ id +' )</b> được chọn không?');
        $('#button-invoice').val('Xóa hóa đơn');
        $('#button-invoice').attr('data-id', id);
    });

    $('#button-invoice').on('click', function () {

        var id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/invoices/delete",
            data: {id: id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-invoice').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                if (data) {
                    var html = '<p class="text-danger">Xóa hóa đơn thành công</p>';
                    $('#notication-invoice').html(html);
                    $('#button-invoice').attr('disabled', false);
                    $('.remove-row').fadeOut(1500);
                } else {
                    var html = '<p class="text-danger">Xóa hóa đơn thất bại</p>';
                    $('#notication-invoice').html(html);
                    $('#button-invoice').attr('disabled', false);
                    $('.remove-row').removeClass('remove-row');
                }
            },
            error: function (e) {
                var html = '<p class="text-danger">Xóa hóa đơn bị lỗi!</p>';
                $('#notication-invoice').html(html);
                $('#button-invoice').attr('disabled', false);
                $('.remove-row').removeClass('remove-row');
            }
        });
    });

    $(document).on('click', '#type_invoice span' ,function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn loại hóa đơn</option>';
        html += '<option value="VPS">Tạo VPS</option>';
        html += '<option value="NAT-VPS">Tạo NAT VPS</option>';
        html += '<option value="Server">Tạo Server</option>';
        html += '<option value="Colocation">Tạo Colocation</option>';
        html += '<option value="Hosting">Tạo Hosting</option>';
        html += '<option value="Email Hosting">Tạo Email Hosting</option>';
        html += '<option value="Hosting-Singapore">Tạo Hosting Singapore</option>';
        html += '<option value="Domain">Tạo Domain</option>';
        html += '<option value="addon_vps">Nâng cấp VPS</option>';
        html += '<option value="addon_server">Nâng cấp Server</option>';
        html += '<option value="upgrade_hosting">Nâng cấp Hosting</option>';
        html += '<option value="change_ip">Đổi IP VPS</option>';
        html += '<option value="expired_vps">Gia hạn VPS</option>';
        html += '<option value="expired_server">Gia hạn Server</option>';
        html += '<option value="expired_colocation">Gia hạn Colocation</option>';
        html += '<option value="expired_hosting">Gia hạn Hosting</option>';
        html += '<option value="expired_email_hosting">Gia hạn Email Hosting</option>';
        html += '<option value="Domain_exp">Gia hạn Domain</option>';
        html += '</select>';
        $('#type_invoice').html(html);
    });

    $(document).on('change', '#type_invoice select', function () {
        // console.log( 'da click', $('#type_invoice select :selected').val(), $('#type_invoice select :selected').text() );
        var type = $('#type_invoice select :selected').val();
        var text_type = $('#type_invoice select :selected').text();
        var user_id = $('#input_filter_user').val();
        var status = $('#input_filter_status').val();
        $.ajax({
          url: '/admin/invoices/filter_type',
          data: {type: type, user_id: user_id, status: status},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.length > 0) {
                  var html = '';
                  $.each(data, function(index , invoice) {
                      html += '<tr>';
                      html += '<td><a href="/admin/invoices/edit/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
                      html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                      html += '<td>'+ text_type +'</td>';
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td>'+ invoice.due_date +'</td>';
                      html += '<td>';
                      if (invoice.paid_date) {
                        html += invoice.paid_date;
                      } else {
                        html += '<span class="text-danger">Chưa thanh toán<span>';
                      }
                      html += '</td>';
                      html += '<td>'+ invoice.sub_total +'</td>';
                      html += '<td>'+ invoice.quantity +'</td>';
                      html += '<td>';
                      if (invoice.status == 'paid') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'unpaid') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      html += '<td class="button-action">';
                      html += '<a href="/admin/invoices/edit/' + invoice.id + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id +'"><i class="fas fa-edit"></i></a>';
                      html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Loại ' + text_type;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_type" value="' + type + '">';
                  html_th_type += '</span>';
                  $('#type_invoice').html(html_th_type);
              } else {
                  var html = '<td colspan="10" class="text-center">Không có loại hóa đơn này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Loại ' + text_type;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_type" value="' + type + '">';
                  html_th_type += '</span>';
                  $('#type_invoice').html(html_th_type);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn hóa đơn lỗi!</td>';
              $('tbody').html(html);
          }
        })
    });

    $(document).on('click', '#user_invoice span' ,function () {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name +'</option>';
              })
              html += '</select>';
              $('#user_invoice').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_invoice span').html(html);
          }
        })
    });

    $(document).on('change', '#user_invoice select', function () {
        // console.log( 'da click', $('#type_invoice select :selected').val(), $('#type_invoice select :selected').text() );
        var user_id = $('#user_invoice select :selected').val();
        var user_name = $('#user_invoice select :selected').text();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        $.ajax({
          url: '/admin/invoices/filter_user',
          data: {type: type, user_id: user_id, status: status},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.length > 0) {
                  var html = '';
                  $.each(data, function(index , invoice) {
                      html += '<tr>';
                      html += '<td><a href="/admin/invoices/edit/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
                      html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                      html += '<td>'+ invoice.type +'</td>';
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td>'+ invoice.due_date +'</td>';
                      html += '<td>';
                      if (invoice.paid_date) {
                        html += invoice.paid_date;
                      } else {
                        html += '<span class="text-danger">Chưa thanh toán<span>';
                      }
                      html += '</td>';
                      html += '<td>'+ invoice.sub_total +'</td>';
                      html += '<td>'+ invoice.quantity +'</td>';
                      html += '<td>';
                      if (invoice.status == 'paid') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'unpaid') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      html += '<td class="button-action">';
                      html += '<a href="/admin/invoices/edit/' + invoice.id + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id +'"><i class="fas fa-edit"></i></a>';
                      html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Khách hàng ' + user_name;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_user" value="' + user_id + '">';
                  html_th_type += '</span>';
                  $('#user_invoice').html(html_th_type);
              } else {
                  var html = '<td colspan="10" class="text-center">Không có hóa đơn của khách hàng này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Khách hàng ' + user_name;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_user" value="' + user_id + '">';
                  html_th_type += '</span>';
                  $('#user_invoice').html(html_th_type);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn hóa đơn lỗi!</td>';
              $('tbody').html(html);
          }
        })
    });


    $(document).on('click', '#status_invoice span' ,function () {
      var html = '';
      html += '<select style="width: 100%;">';
      html += '<option value="" selected disabled>Chọn trạng thái</option>';
      html += '<option value="paid">Đã thanh toán</option>';
      html += '<option value="unpaid">Chưa thanh toán</option>';
      html += '<option value="cancel">Hủy</option>';
      html += '</select>';
      $('#status_invoice').html(html);
    });

    $(document).on('change', '#status_invoice select', function () {
        // console.log( 'da click', $('#type_invoice select :selected').val(), $('#type_invoice select :selected').text() );
        var status = $('#status_invoice select :selected').val();
        var status_text = $('#status_invoice select :selected').text();
        var type = $('#input_filter_type').val();
        var user_id = $('#input_filter_user').val();
        $.ajax({
          url: '/admin/invoices/filter_status',
          data: {type: type, user_id: user_id, status: status},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.length > 0) {
                  var html = '';
                  $.each(data, function(index , invoice) {
                      html += '<tr>';
                      html += '<td><a href="/admin/invoices/edit/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
                      html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                      html += '<td>'+ invoice.type +'</td>';
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td>'+ invoice.due_date +'</td>';
                      html += '<td>';
                      if (invoice.paid_date) {
                        html += invoice.paid_date;
                      } else {
                        html += '<span class="text-danger">Chưa thanh toán<span>';
                      }
                      html += '</td>';
                      html += '<td>'+ invoice.sub_total +'</td>';
                      html += '<td>'+ invoice.quantity +'</td>';
                      html += '<td>';
                      if (invoice.status == 'paid') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'unpaid') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      html += '<td class="button-action">';
                      html += '<a href="/admin/invoices/edit/' + invoice.id + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id +'"><i class="fas fa-edit"></i></a>';
                      html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Trạng thái ' + status_text;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_status" value="' + status + '">';
                  html_th_type += '</span>';
                  $('#status_invoice').html(html_th_type);
              } else {
                  var html = '<td colspan="10" class="text-center">Không có trạng thái hóa đơn này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Trạng thái ' + status_text;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_status" value="' + status + '">';
                  html_th_type += '</span>';
                  $('#status_invoice').html(html_th_type);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn hóa đơn lỗi!</td>';
              $('tbody').html(html);
          }
        })
    });

    $(document).on('click', '.detail_invoice', function (event) {
        event.preventDefault();
        $('#detail_invoice').modal('show');
        var id = $(this).attr('data-id');
        $.ajax({
          url: '/admin/invoices/detail_invoice?id=' + id,
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication-detail').html(html);
          },
          success: function (data) {
              console.log(data);
              var html = '';
              // Thông tin paid
              html += '<div class="detail_order text-center">';
              html += '<div class="paid">';
              if (data.status == 'paid') {
                html += '<span class="text-success">Đã thanh toán</span>';
              } else if (data.status == 'unpaid') {
                html += '<span class="text-danger">Chưa thanh toán</span>';
              } else {
                html += '<span class="text-default">Hủy</span>';
              }
              html += '</div>';
              html += '<div class="date_paid">';
              if (data.paid_date != '') {
                html += '<b class="text-success">' + data.paid_date + '</b>';
              } else {
                html += '<b class="text-danger">Chưa thanh toán</b>';
              }
              html += '</div>';
              html += '<div class="date_due">';
              html += 'Ngày đặt hàng: <b>' + data.date_create + '</b>';
              html += '</div>';
              html += '<div class="date_due">';
              if (data.status == 'unpaid') {
                html += 'Ngày hết hạn: <b>' + data.due_date + '</b>';
              }
              html += '</div>';
              html += '</div>';
              // Thông tin đơn đặt hàng
              html += '<div class="detail_order">';
              html += '<div>';
              html += '<h4>Thông tin dịch vụ / sản phẩm</h4>';
              html += '</div>';
              html += '<div class="m-3">';
              html += '<table class="table table-hover table-bordered">';
              if (data.type_order == 'expired') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                    html += '<tr>';
                    html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                    html += '<td>';
                    if (data.type == 'VPS') {
                      html += 'Gia hạn VPS - ';
                      html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    } else if (data.type == 'hosting') {
                      html += 'Gia hạn Hosting - ';
                      html += '<a href="/admin/hostings/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    } else if (data.type == 'Domain') {
                      html += 'Gia hạn Domain - ';
                      html += '<a href="/admin/domain/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    html += '</td>';
                    html += '<td>';
                    html += service.config;
                    html += '</td>';
                    html += '<td>';
                    html += data.date_create;
                    html += '</td>';
                    html += '<td>';
                    html += service.billing_cycle;
                    html += '</td>';
                    html += '<td>';
                    html += service.amount;
                    html += '</td>';
                    html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'addon_vps') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>IP</th><th>CPU</th><th>RAM</th><th>DISK</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                  html += '<tr>';
                  html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                  html += '<td>';
                  html += 'Addon VPS - ';
                  html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                  html += '</td>';
                  html += '<td>' + service.cpu + '</td>';
                  html += '<td>' + service.ram + '</td>';
                  html += '<td>' + service.disk + '</td>';
                  html += '<td>';
                  html += data.date_create;
                  html += '</td>';
                  html += '<td>';
                  html += service.time;
                  html += '</td>';
                  html += '<td>';
                  html += service.amount;
                  html += '</td>';
                  html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'change_ip') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>IP</th><th>Ngày đặt hàng</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                  html += '<tr>';
                  html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                  html += '<td>';
                  html += 'Đổi IP VPS - ';
                  html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                  html += '</td>';
                  html += '<td>';
                  html += data.date_create;
                  html += '</td>';
                  html += '<td>';
                  html += service.amount;
                  html += '</td>';
                  html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'upgrade_hosting') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Hosting</th><th>Nâng cấp gói Hosting</th><th>Ngày đặt hàng</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                html += '<tr>';
                html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                html += '<td>';
                html += 'Nâng cấp Hosting - ';
                html += '<a href="/admin/vps/detail/'+ data.services.sevice_id +'">'+ data.services.sevice_name +'</a>';
                html += '</td>';
                html += '<td>' + data.services.upgrade + '</td>';
                html += '<td>';
                html += data.date_create;
                html += '</td>';
                html += '<td>';
                html += data.services.amount;
                html += '</td>';
                html += '</tr>';
                html += '</tbody>';
              }
              else {
                // console.log(data);
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                    html += '<tr>';
                    html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                    html += '<td>';
                    if (data.type == 'VPS' || data.type == 'NAT-VPS') {
                      html += 'Tạo '+ data.type +' - ';
                      html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    } else if (data.type == 'Hosting' || data.type == 'Hosting-Singapore') {
                      html += 'Tạo '+ data.type +' - ';
                      html += '<a href="/admin/hostings/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    } else if (data.type == 'Domain') {
                      html += 'Tạo Domain - ';
                      html += '<a href="/admin/domain/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    html += '</td>';
                    html += '<td>';
                    html += service.config;
                    html += '</td>';
                    html += '<td>';
                    html += data.date_create;
                    html += '</td>';
                    html += '<td>';
                    html += service.billing_cycle;
                    html += '</td>';
                    html += '<td>';
                    html += service.amount;
                    html += '</td>';
                    html += '</tr>';
                });
                html += '</tbody>';
              }
              html += '</table>';
              html += '</div>';
              html += '</div>';
              $('#notication-detail').html(html);
          },
          error: function (e) {
            console.log(e);
            $('#notication-detail').html('<div  class="text-center"><span>Lỗi truy vấn Detail Order!</span></div>');
          },
        });
    });

});
