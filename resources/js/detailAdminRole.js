$(document).ready(function() {

	loadUsers();
    $('.selectPage').select2();

    function loadUsers() {
        var id = $('#id-group').val();
        $.ajax({
            url: '/admin/admin-roles/loadUsers',
            dataType: 'json',
            data: {id: id},
            beforeSend: function(){
              var html = '<tr><td class="text-center" colspan="3"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>';
              $('#users tbody').html(html);
            },
            success: function (data) {
              // console.log(data);
                var html = '';
                if (data != '') {
                    $.each(data, function(index, user) {
                        html += '<tr>';
                        html += '<td>' + user.name + '</td>';
                        html += '<td>' + user.email + '</td>';
                        html += '<td>';
                        html += '<button type="button" name="button" class="btn btn-danger delete_user" data-id="'+ user.id +'" data-name="'+ user.name +'" data-toggle="tooltip" data-placement="top" title="Xóa tài khoản khỏi nhóm quản trị"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('#users tbody').html(html);
                } else {
                    $('#users tbody').html('<td class="text-center text-danger" colspan="3">Không có tài khoản trong nhóm quản trị này.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#users tbody').html('<td class="text-center text-danger" colspan="3">Truy vấn nhóm quản trị lỗi.</span>');
            }
        })
    }

    // Thêm user vào group user
    $('#add_user').on('click', function () {
        $('#modal').modal('show');
        $('#modal .modal-title').text('Thêm tài khoản vào nhóm quản trị');
        $('#type').val('user');
        $('#change_group').html('');
        $('#buttonSubmit').fadeIn();
        $("#buttonFinish").fadeOut();
        var group_id = $('#id-group').val();
        $.ajax({
            url: '/admin/admin-roles/loadUserAdmin',
            dataType: 'json',
            data: {id: group_id},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication_modal').html(html);
            },
            success: function (data) {
                console.log(data);
                var html = '';
                if (data != '') {
                    html += '<label>Chọn khách hàng cho nhóm quản trị</label>';
                    html += '<select class="form-control select2" name="user_id[]" data-placeholder="Chọn khách hàng" multiple style="width:100%;">';
                    $.each(data, function (index, user) {
                        html += '<option value="' + user.id + '">';
                        html += user.name + ' - ' + user.email;
                        html += '</option>';
                    });
                    html += '</select>';
                    $('#notication_modal').html('');
                    $('#change_group').html(html);
                    $('.select2').select2();
                } else {
                    $('#notication_modal').html('<span class="text-center text-danger">Không có tài khoản nào phù hợp với yêu cầu.</span>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication_modal').html('<span class="text-center text-danger">Truy vấn nhóm quản trị lỗi.</span>');
            }
        })
    })

    $('#buttonSubmit').on('click', function() {
        var form = $('#form-group-product').serialize();
        var type = $('#type').val();
        $.ajax({
            url: '/admin/admin-roles/update_user_with_group',
            type: 'post',
            dataType: 'json',
            data: form,
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication_modal').html(html);
              $('#change_group').html('');
              $('#buttonSubmit').attr('disabled', true);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data != '') {
                    $('#notication_modal').html('');
                    $('#buttonSubmit').attr('disabled', false);
                    // $('modal').modal('hide');
                    $('#notication_modal').html('<span class="text-success text-center">Thêm khách hàng vào nhóm quản trị thành công.</span> ');
                    if (type == 'user') {
                        loadUsers();
                        $('#buttonSubmit').fadeOut();
                        $("#buttonFinish").fadeIn();
                    } else {
                        loadGroupProduct();
                        $('#buttonSubmit').fadeOut();
                        $("#buttonFinish").fadeIn();
                    }
                } else {
                    $('#notication_modal').html('<span class="text-danger text-center">Thêm khách hàng vào nhóm quản trị không thành công.</span> ');
                    $('#buttonSubmit').attr('disabled', false);
                }
            },
            error: function (e) {
              console.log(e);
              $('#notication_modal').html('<span class="text-danger text-center">Truy vấn nhóm quản trị lỗi.</span> ');
              $('#buttonSubmit').attr('disabled', false);
            }
        });
    })

    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#modalAll').modal('show');
        $('#modalAll .modal-title').text('Xóa tài khoản trong nhóm quản trị');
        $('#noticationModalAll').html('<span>Bạn có muốn xóa tài khoản <b class="text-danger">' + name + '</b> ra khỏi nhóm quản trị này không?');
        $('#buttonSubmitAll').attr('data-id', id);
        $('#buttonSubmitAll').attr('data-type', 'user');
        $('#buttonSubmitAll').fadeIn();
        $('#buttonFinishAll').fadeOut();
    });

    $('#buttonSubmitAll').on('click', function () {
        var id  = $(this).attr('data-id');
        var type  = $(this).attr('data-type');
        var role_id = $('#id-group').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/admin-roles/delete_user_with_group',
            type: 'post',
            dataType: 'json',
            data: {'_token': token,role_id: role_id, id: id, type: type},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#noticationModalAll').html(html);
              $('#buttonSubmitAll').attr('disabled', true);
            },
            success: function (data) {
                if (data) {
                    $('#noticationModalAll').html('<span class="text-success text-center">Xóa tài khoản trong nhóm quản trị thành công.</span> ');
                    $('#buttonSubmitAll').attr('disabled', false);
                    $('#buttonSubmitAll').fadeOut();
                    $('#buttonFinishAll').fadeIn();
                    if (type == 'user') {
                        loadUsers();
                    }
                    else {
                        loadGroupProduct();
                    }
                } else {
                    $('#noticationModalAll').html('<span class="text-danger text-center">Xóa tài khoản trong nhóm quản trị thất bại.</span> ');
                    $('#buttonSubmitAll').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#noticationModalAll').html('<span class="text-danger text-center">Truy vấn nhóm quản trị lỗi.</span> ');
                $('#buttonSubmitAll').attr('disabled', false);
            }
        });
    })

    // Nhóm sản phẩm
    loadGroupProduct();
    function loadGroupProduct() {
        var id = $('#id-group').val();
        var role = $('#selectPage :selected').val();
        $.ajax({
            url: '/admin/admin-roles/load_admin_roles',
            dataType: 'json',
            data: {id: id, role: role},
            beforeSend: function(){
              var html = '<tr><td class="text-center" colspan="3"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>';
              $('#group_products tbody').html(html);
            },
            success: function (data) {
              // console.log(data);
                var html = '';
                if (data.data != '') {
                	screent(data);
                } else {
                    $('#group_products tbody').html('<td class="text-center text-danger" colspan="3">Không có nhóm sản phẩm trong nhóm quản trị này.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#group_products tbody').html('<td class="text-center text-danger" colspan="3">Truy vấn nhóm quản trị lỗi.</span>');
            }
        })
    }

    function screent(data) {
    	var html = '';
    	$.each(data.data, function(index, group_product) {
                        html += '<tr>';
                        html += '<td>' + group_product.page + '</td>';
                        html += '<td>';
                        html += '<button type="button" name="button" class="btn btn-danger delete_group_product" data-id="'+ group_product.id +'" data-name="'+ group_product.page +'" data-toggle="tooltip" data-placement="top" title="Xóa trang cho phép khỏi nhóm quản trị"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    });
        $('#group_products tbody').html(html);
                    // phân trang cho user
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if ( total / per_page > 11 ) {
              var page = parseInt(total/per_page + 1);
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination">';
              if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              if (current_page < 7) {
                for (var i = 1; i < 9; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              else if (current_page >= 7 || current_page <= page - 7) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = current_page - 3; i <= current_page +3; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
              }
              else if (current_page >= page - 6) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 6; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
              }
   
              if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            } else {
              var page = total/per_page + 1;
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination">';
              if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                      active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
   
              }
              if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }
    // Thêm nhóm sản phẩm vào group user
    $('#add_group_product').on('click', function () {
        $('#modal').modal('show');
        $('#modal .modal-title').text('Thêm đặc quyền vào nhóm quản trị');
        $('#type').val('group_product');
        $('#change_group').html('');
        var group_id = $('#id-group').val();
        $('#buttonSubmit').fadeIn();
        $("#buttonFinish").fadeOut();
        $.ajax({
            url: '/admin/admin-roles/load_roles',
            dataType: 'json',
            data: {id: group_id},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication_modal').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data != '') {
                    html += '<label>Chọn đặc quyền vào nhóm quản trị</label>';
                    html += '<select class="form-control select2" name="page[]" data-placeholder="Chọn đặc quyền" multiple style="width:100%;">';
                    $.each(data, function (index, group_product) {
                        html += '<option value="' + index + '">';
                        html += group_product;
                        html += '</option>';
                    });
                    html += '</select>';
                    $('#notication_modal').html('');
                    $('#change_group').html(html);
                    $('.select2').select2();
                } else {
                    $('#notication_modal').html('<span class="text-center text-danger">Không có đặc quyền nào phù hợp với yêu cầu.</span>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication_modal').html('<span class="text-center text-danger">Truy vấn nhóm quản trị lỗi.</span>');
            }
        })
    })

    $(document).on('click', '.delete_group_product', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#modalAll').modal('show');
        $('#modalAll .modal-title').text('Xóa đặc quyền trong nhóm quản trị');
        $('#noticationModalAll').html('<span>Bạn có muốn xóa đặc quyền vào Trang <b class="text-danger">' + name + '</b> ra khỏi nhóm quản trị này không?');
        $('#buttonSubmitAll').attr('data-id', id);
        $('#buttonSubmitAll').attr('data-type', 'group_product');
        $('#buttonSubmitAll').fadeIn();
        $('#buttonFinishAll').fadeOut();
    });

    $('#selectPage').on('change', function(event) {
    	loadGroupProduct();
    });

});