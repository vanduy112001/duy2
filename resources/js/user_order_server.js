$(document).ready(function () {

    var billingConfig = $('#billing').val();

    loadAmount();

    $('#qtt').on('keyup', function() {
        if ( isNumber($(this).val()) ) {
            if ( $(this).val() > 0 ) {
                loadAmount();
            } else {
                alert('Số lượng phải lớn hơn 0');
            }
        } else {
            alert('Số lượng phải là ký tự số');
        }
    })

    $('#billing').on('change', function () {
        loadAmount();
    })

    function isNumber(n) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
    }

    function loadAmount() {
        var qtt = $('#qtt').val();
        var billing = $('#billing').val();
        var disk2 = $('#disk2').val();
        var disk3 = $('#disk3').val();
        var disk4 = $('#disk4').val();
        var disk5 = $('#disk5').val();
        var disk6 = $('#disk6').val();
        var disk7 = $('#disk7').val();
        var disk8 = $('#disk8').val();
        var addon_ram = $('#addon_ram').val();
        var addon_ip = $('#addon_ip').val();
        var data = {
            qtt: qtt,
            billing: billing,
            product_id: $('#product_id').val(),
            disk2: disk2,
            disk3: disk3,
            disk4: disk4,
            disk5: disk5,
            disk6: disk6,
            disk7: disk7,
            disk8: disk8,
            addon_ram: addon_ram,
            addon_ip: addon_ip,
        };
        // console.log(data);
        $.ajax({
            url: "/order/order_server",
            data: data,
            dataType: "json",
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.before-send').css('opacity', '0.5');
                $('.before-send').html(html);
            },
            success: function (response) {
                if ( response.error ) {
                    alert(response.message);
                } else {
                    // console.log(response, billing);
                    $('.quantity-printf').text(qtt);
                    $('.item_pricing_cell').text( addCommas(response.amount) + ' ₫' );
                    $('.total_pricing').text( addCommas(response.total) + ' ₫' );
                    // disk 2
                    var html_disk_2 = '';
                    if ( disk2 != 0 ) {
                        html_disk_2 += '<td>DISK 2: '+ response.disk2.name +'</td>';
                        html_disk_2 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_2 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk2.pricing) +' ₫</td>';
                        $('.detail-disk-2').html(html_disk_2);
                        $('#item-disk2 .item-text .item-title').text(response.disk2.name);
                    } else {
                        $('.detail-disk-2').html('');
                        $('#item-disk2 .item-text .item-title').text('None');
                    }
                    $('#item-disk2 .item-amount .amount').text( addCommas(response.disk2.pricing) + ' ₫' );
                    // disk 3
                    var html_disk_3 = '';
                    if ( disk3 != 0 ) {
                        console.log(response.disk3.name);
                        html_disk_3 += '<td>DISK 3: '+ response.disk3.name +'</td>';
                        html_disk_3 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_3 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk3.pricing) +' ₫</td>';
                        $('.detail-disk-3').html(html_disk_3);
                        $('#item-disk3 .item-text .item-title').text(response.disk3.name);
                    } else {
                        $('.detail-disk-3').html('');
                        $('#item-disk3 .item-text .item-title').text('None');
                    }
                    $('#item-disk3 .item-amount .amount').text( addCommas(response.disk3.pricing) + ' ₫' );
                    // disk 4
                    var html_disk_4 = '';
                    if ( disk4 != 0 ) {
                        html_disk_4 += '<td>DISK 4: '+ response.disk4.name +'</td>';
                        html_disk_4 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_4 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk4.pricing) +' ₫</td>';
                        $('.detail-disk-4').html(html_disk_4);
                        $('#item-disk4 .item-text .item-title').text(response.disk4.name);
                    } else {
                        $('.detail-disk-4').html('');
                        $('#item-disk4 .item-text .item-title').text('None');
                    }
                    $('#item-disk4 .item-amount .amount').text( addCommas(response.disk4.pricing) + ' ₫' );
                    // disk 5
                    var html_disk_5 = '';
                    if ( disk5 != 0 ) {
                        html_disk_5 += '<td>DISK 5: '+ response.disk5.name +'</td>';
                        html_disk_5 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_5 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk5.pricing) +' ₫</td>';
                        $('.detail-disk-5').html(html_disk_5);
                        $('#item-disk5 .item-text .item-title').text(response.disk5.name);
                    } else {
                        $('.detail-disk-5').html('');
                        $('#item-disk5 .item-text .item-title').text('None');
                    }
                    $('#item-disk5 .item-amount .amount').text( addCommas(response.disk5.pricing) + ' ₫' );
                    // disk 6
                    var html_disk_6 = '';
                    if ( disk6 != 0 ) {
                        html_disk_6 += '<td>DISK 6: '+ response.disk6.name +'</td>';
                        html_disk_6 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_6 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk6.pricing) +' ₫</td>';
                        $('.detail-disk-6').html(html_disk_6);
                        $('#item-disk6 .item-text .item-title').text(response.disk6.name);
                    } else {
                        $('.detail-disk-6').html('');
                        $('#item-disk6 .item-text .item-title').text('None');
                    }
                    $('#item-disk6 .item-amount .amount').text( addCommas(response.disk6.pricing) + ' ₫' );
                    // disk 5
                    var html_disk_7 = '';
                    if ( disk7 != 0 ) {
                        html_disk_7 += '<td>DISK 7: '+ response.disk7.name +'</td>';
                        html_disk_7 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_7 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk7.pricing) +' ₫</td>';
                        $('.detail-disk-7').html(html_disk_7);
                        $('#item-disk7 .item-text .item-title').text(response.disk7.name);
                    } else {
                        $('.detail-disk-7').html('');
                        $('#item-disk7 .item-text .item-title').text('None');
                    }
                    $('#item-disk7 .item-amount .amount').text( addCommas(response.disk7.pricing) + ' ₫' );
                    // disk 8
                    var html_disk_8 = '';
                    if ( disk8 != 0 ) {
                        html_disk_8 += '<td>DISK 8: '+ response.disk8.name +'</td>';
                        html_disk_8 += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_disk_8 += '<td class="item_pricing_cell text-right">'+ addCommas(response.disk8.pricing) +' ₫</td>';
                        $('.detail-disk-8').html(html_disk_8);
                        $('#item-disk8 .item-text .item-title').text(response.disk8.name);
                    } else {
                        $('.detail-disk-8').html('');
                        $('#item-disk8 .item-text .item-title').text('None');
                    }
                    $('#item-disk8 .item-amount .amount').text( addCommas(response.disk8.pricing) + ' ₫' );
                    // addon ram
                    var html_addon_ram = '';
                    // console.log(addon_ram);
                    if ( addon_ram != 0 ) {
                        html_addon_ram += '<td>Addon RAM: '+ response.addon_ram.name +'</td>';
                        html_addon_ram += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_addon_ram += '<td class="item_pricing_cell text-right">'+ addCommas(response.addon_ram.pricing) +' ₫</td>';
                        $('.detail-addon-ram').html(html_addon_ram);
                        $('#addon_ram_container .item-text .item-title').text(response.addon_ram.name);
                        
                        $('#btn_addon_ram').attr('data-type', 'minus');
                        $('#btn_addon_ram').html('<i class="fas fa-minus"></i>');
                        $('#addon_ram_container').removeClass('addon-ram-hidden');

                    } else {
                        $('.detail-addon-ram').html('');
                        $('#addon_ram_container .item-text .item-title').text('None');
                    }
                    $('#addon_ram_container .item-amount .amount').text( addCommas(response.addon_ram.pricing) + ' ₫' );
                    // addon ip
                    var html_addon_ip = '';
                    if ( addon_ip != 0 ) {
                        html_addon_ip += '<td>Addon IP: '+ response.addon_ip.name +'</td>';
                        html_addon_ip += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_addon_ip += '<td class="item_pricing_cell text-right">'+ addCommas(response.addon_ip.pricing) +' ₫</td>';
                        $('.detail-addon-ip').html(html_addon_ip);
                        $('#addon_ip_container .item-text .item-title').text(response.addon_ip.name);
                        
                        $('#btn_addon_ip').attr('data-type', 'minus');
                        $('#btn_addon_ip').html('<i class="fas fa-minus"></i>');
                        $('#addon_ip_container').removeClass('addon-ip-hidden');
                    } else {
                        $('.detail-addon-ip').html('');
                        $('#addon_ip_container .item-text .item-title').text('None');
                    }
                    $('#addon_ip_container .item-amount .amount').text( addCommas(response.addon_ip.pricing) + ' ₫' );

                    loadAmountDisk(response.list_add_on_disk_server, billing);
                    loadAmountRam(response.list_add_on_disk_ram, billing);
                    loadAmountIp(response.list_add_on_ip_server, billing);
                }
            },
            error: function(e) {
                console.log(e);
                alert('Truy vấn lỗi');
            }
        });
    }

    function loadAmountDisk(list_add_on_disk_server, billing) {
        // disk 2
        var html2 = '';
        var disk2 = $('#disk2').val();
        html2 += '<div class="list-select-item '+ (disk2 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk2">';
        html2 += '<div class="list-select-item-text">None</div>';
        html2 += '<div class="list-select-item-amount">0 ₫</div>';
        html2 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk2 == value.id ) {
                selected = 'selected';
            }
            html2 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk2">';
            html2 += '<div class="list-select-item-text">' + value.name + '</div>';
            html2 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html2 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html2 += '0 ₫';
            }
            html2 += '</div>';
            html2 += '</div>';
        })
        $('#item-disk2 .list-select').html(html2);
        // disk 3
        var html3 = '';
        var disk3 = $('#disk3').val();
        html3 += '<div class="list-select-item '+ (disk3 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk3">';
        html3 += '<div class="list-select-item-text">None</div>';
        html3 += '<div class="list-select-item-amount">0 ₫</div>';
        html3 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk3 == value.id ) {
                selected = 'selected';
            }
            html3 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk3">';
            html3 += '<div class="list-select-item-text">' + value.name + '</div>';
            html3 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html3 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html3 += '0 ₫';
            }
            html3 += '</div>';
            html3 += '</div>';
        })
        $('#item-disk3 .list-select').html(html3);
        // disk 4
        var html4 = '';
        var disk4 = $('#disk4').val();
        html4 += '<div class="list-select-item '+ (disk4 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk4">';
        html4 += '<div class="list-select-item-text">None</div>';
        html4 += '<div class="list-select-item-amount">0 ₫</div>';
        html4 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk4 == value.id ) {
                selected = 'selected';
            }
            html4 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk4">';
            html4 += '<div class="list-select-item-text">' + value.name + '</div>';
            html4 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html4 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html4 += '0 ₫';
            }
            html4 += '</div>';
            html4 += '</div>';
        })
        $('#item-disk4 .list-select').html(html4);
        // disk 5
        var html5 = '';
        var disk5 = $('#disk5').val();
        html5 += '<div class="list-select-item '+ (disk5 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk5">';
        html5 += '<div class="list-select-item-text">None</div>';
        html5 += '<div class="list-select-item-amount">0 ₫</div>';
        html5 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk5 == value.id ) {
                selected = 'selected';
            }
            html5 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk5">';
            html5 += '<div class="list-select-item-text">' + value.name + '</div>';
            html5 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html5 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html5 += '0 ₫';
            }
            html5 += '</div>';
            html5 += '</div>';
        })
        $('#item-disk5 .list-select').html(html5);
        // disk 6
        var html6 = '';
        var disk6 = $('#disk6').val();
        html6 += '<div class="list-select-item '+ (disk6 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk6">';
        html6 += '<div class="list-select-item-text">None</div>';
        html6 += '<div class="list-select-item-amount">0 ₫</div>';
        html6 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk6 == value.id ) {
                selected = 'selected';
            }
            html6 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk6">';
            html6 += '<div class="list-select-item-text">' + value.name + '</div>';
            html6 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html6 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html6 += '0 ₫';
            }
            html6 += '</div>';
            html6 += '</div>';
        })
        $('#item-disk6 .list-select').html(html6);
        // disk 7
        var html7 = '';
        var disk7 = $('#disk7').val();
        html7 += '<div class="list-select-item '+ (disk7 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk7">';
        html7 += '<div class="list-select-item-text">None</div>';
        html7 += '<div class="list-select-item-amount">0 ₫</div>';
        html7 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk7 == value.id ) {
                selected = 'selected';
            }
            html7 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk7">';
            html7 += '<div class="list-select-item-text">' + value.name + '</div>';
            html7 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html7 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html7 += '0 ₫';
            }
            html7 += '</div>';
            html7 += '</div>';
        })
        $('#item-disk7 .list-select').html(html7);
        // disk 8
        var html8 = '';
        var disk8 = $('#disk8').val();
        html8 += '<div class="list-select-item '+ (disk8 == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="disk8">';
        html8 += '<div class="list-select-item-text">None</div>';
        html8 += '<div class="list-select-item-amount">0 ₫</div>';
        html8 += '</div>';
        $.each( list_add_on_disk_server , function (index , value) {
            var selected = '';
            if ( disk8 == value.id ) {
                selected = 'selected';
            }
            html8 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="disk8">';
            html8 += '<div class="list-select-item-text">' + value.name + '</div>';
            html8 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html8 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html8 += '0 ₫';
            }
            html8 += '</div>';
            html8 += '</div>';
        })
        $('#item-disk8 .list-select').html(html8);
    }

    function loadAmountRam(list_add_on_disk_ram, billing) {
        var html2 = '';
        var addon_ram = $('#addon_ram').val();
        html2 += '<div class="list-select-item '+ (addon_ram == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="addon_ram">';
        html2 += '<div class="list-select-item-text">None</div>';
        html2 += '<div class="list-select-item-amount">0 ₫</div>';
        html2 += '</div>';
        $.each( list_add_on_disk_ram , function (index , value) {
            var selected = '';
            if ( addon_ram == value.id ) {
                selected = 'selected';
            }
            html2 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="addon_ram">';
            html2 += '<div class="list-select-item-text">' + value.name + '</div>';
            html2 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html2 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html2 += '0 ₫';
            }
            html2 += '</div>';
            html2 += '</div>';
        })
        $('#addon_ram_container .list-select').html(html2);
    }

    function loadAmountIp(list_add_on_ip_server, billing) {
        var html2 = '';
        var addon_ip = $('#addon_ip').val();
        html2 += '<div class="list-select-item '+ (addon_ip == 0 ? 'selected' : '') +'" data-id="0" data-title="None" data-type="addon_ip">';
        html2 += '<div class="list-select-item-text">None</div>';
        html2 += '<div class="list-select-item-amount">0 ₫</div>';
        html2 += '</div>';
        $.each( list_add_on_ip_server , function (index , value) {
            var selected = '';
            if ( addon_ip == value.id ) {
                selected = 'selected';
            }
            html2 += '<div class="list-select-item '+ selected +'" data-id="' + value.id + '" data-title="' + value.name + '"  data-type="addon_ip">';
            html2 += '<div class="list-select-item-text">' + value.name + '</div>';
            html2 += '<div class="list-select-item-amount">';
            if ( value.pricing[billing] ) {
                html2 += addCommas(value.pricing[billing]) + ' ₫';
            } else {
                html2 += '0 ₫';
            }
            html2 += '</div>';
            html2 += '</div>';
        })
        $('#addon_ip_container .list-select').html(html2);
    }

    $('.select-item').on('click', function () {
        // $('.select-item.active').removeClass('active');
        // $(this).addClass('active');
        if ( $(this).attr('data-type') == 'off' ) {
            $('.select-item.active').removeClass('active');
            $(this).addClass('active');
            $(this).attr('data-type', 'on');
        }
        else {
            $('.select-item.active').removeClass('active');
            $(this).attr('data-type', 'off');
        }
    })

    $(document).on('click', '.select-item .list-select-item', function () {
        var type = $(this).attr('data-type');
        // console.log(type);
        if ( type == 'os' ) {
            $('#item-os .list-select-item.selected').removeClass('selected');
            $(this).addClass('selected');
            var idOs = $(this).attr('data-id');
            var titleOs = $(this).attr('data-title');
            var price = $(this).attr('data-pricing');
            $('#item-os .item-title').text(titleOs);
            $('#os').val(titleOs);
            var html = '';
            html += '<td>OS: '+ titleOs +'</td>';
            html += '<td></td>';
            html += '<td></td>';
            $('.detail-os').html(html);
        }
        else if ( type == 'datacenter' )
        {
            $('#item-datacenter .list-select-item.selected').removeClass('selected');
            $(this).addClass('selected');
            var title = $(this).attr('data-title');
            var price = $(this).attr('data-pricing');
            $('#item-datacenter .item-title').text(title);
            $('#datacenter').val(title);
            var html = '';
            html += '<td>Datacenter: '+ title +'</td>';
            html += '<td></td>';
            html += '<td></td>';
            $('.detail-datacenter').html(html);
        }
        else if ( type == 'server_management' ) {
            $('#item-server-management .list-select-item.selected').removeClass('selected');
            $(this).addClass('selected');
            var title = $(this).attr('data-title');
            var price = $(this).attr('data-pricing');
            $('#item-server-management .item-title').text(title);
            $('#server_management').val(title);
        }
        else if ( type == 'raid' ) {
            $('#item-raid .list-select-item.selected').removeClass('selected');
            $(this).addClass('selected');
            var title = $(this).attr('data-title');
            var price = $(this).attr('data-pricing');
            $('#item-raid .item-title').text(title);
            $('#raid').val(title);
            var html = '';
            html += '<td>Raid: '+ title +'</td>';
            html += '<td></td>';
            html += '<td></td>';
            $('.detail-raid').html(html);
        }
        else if ( type == 'disk2' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-2').html('');
                $('#item-disk2 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk2 .item-title').text(title);
                $('#item-disk2 .item-amount .amount').text( '0 ₫' );
                $('#disk2').val(0);
                loadAmount();
            } else {
                $('#item-disk2 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk2 .item-title').text(title);
                $('#disk2').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk3' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-3').html('');
                $('#item-disk3 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk3 .item-title').text(title);
                $('#item-disk3 .item-amount .amount').text( '0 ₫' );
                $('#disk3').val(0);
                loadAmount();
            } else {
                $('#item-disk3 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk3 .item-title').text(title);
                $('#disk3').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk4' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-4').html('');
                $('#item-disk4 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk4 .item-title').text(title);
                $('#item-disk4 .item-amount .amount').text( '0 ₫' );
                $('#disk4').val(0);
                loadAmount();
            } else {
                $('#item-disk4 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk4 .item-title').text(title);
                $('#disk4').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk5' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-5').html('');
                $('#item-disk5 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk5 .item-title').text(title);
                $('#item-disk5 .item-amount .amount').text( '0 ₫' );
                $('#disk5').val(0);
                loadAmount();
            } else {
                $('#item-disk5 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk5 .item-title').text(title);
                $('#disk5').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk6' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-6').html('');
                $('#item-disk6 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk6 .item-title').text(title);
                $('#item-disk6 .item-amount .amount').text( '0 ₫' );
                $('#disk6').val(0);
                loadAmount();
            } else {
                $('#item-disk6 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk6 .item-title').text(title);
                $('#disk6').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk7' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-7').html('');
                $('#item-disk7 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk7 .item-title').text(title);
                $('#item-disk7 .item-amount .amount').text( '0 ₫' );
                $('#disk7').val(0);
                loadAmount();
            } else {
                $('#item-disk7 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk7 .item-title').text(title);
                $('#disk7').val(id);
                loadAmount();
            }
        }
        else if ( type == 'disk8' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-disk-8').html('');
                $('#item-disk8 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk8 .item-title').text(title);
                $('#item-disk8 .item-amount .amount').text( '0 ₫' );
                $('#disk8').val(0);
                loadAmount();
            } else {
                $('#item-disk8 .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#item-disk8 .item-title').text(title);
                $('#disk8').val(id);
                loadAmount();
            }
        }
        else if ( type == 'addon_ram' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-addon-ram').html('');
                $('#addon_ram_container .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#addon_ram_container .item-title').text(title);
                $('#addon_ram_container .item-amount .amount').text( '0 ₫' );
                $('#addon_ram').val(0);
                loadAmount();
            } else {
                $('#addon_ram_container .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#addon_ram_container .item-title').text(title);
                $('#addon_ram').val(id);
                loadAmount();
            }
        }
        else if ( type == 'addon_ip' ) {
            var id = $(this).attr('data-id');
            if ( id == 0 ) {
                $('.detail-addon-ip').html('');
                $('#addon_ip_container .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#addon_ip_container .item-title').text(title);
                $('#addon_ip_container .item-amount .amount').text( '0 ₫' );
                $('#addon_ip').val(0);
                loadAmount();
            } else {
                $('#addon_ip_container .list-select-item.selected').removeClass('selected');
                $(this).addClass('selected');
                var title = $(this).attr('data-title');
                $('#addon_ip_container .item-title').text(title);
                $('#addon_ip').val(id);
                loadAmount();
            }
        }
        $('.select-item.active').removeClass('active');
    });

    $('#btn_addon_ram').on('click', function () {
        type = $(this).attr('data-type');
        if ( type == 'plus' ) {
            $(this).attr('data-type', 'minus');
            $(this).html('<i class="fas fa-minus"></i>');
            $('#addon_ram_container').removeClass('addon-ram-hidden');
        } else {
            $(this).attr('data-type', 'plus');
            $(this).html('<i class="fas fa-plus"></i>');
            $('#addon_ram_container').addClass('addon-ram-hidden');
        }
    });

    $('#btn_addon_ip').on('click', function () {
        type = $(this).attr('data-type');
        if ( type == 'plus' ) {
            $(this).attr('data-type', 'minus');
            $(this).html('<i class="fas fa-minus"></i>');
            $('#addon_ip_container').removeClass('addon-ip-hidden');
        } else {
            $(this).attr('data-type', 'plus');
            $(this).html('<i class="fas fa-plus"></i>');
            $('#addon_ip_container').addClass('addon-ip-hidden');
        }
    });

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
