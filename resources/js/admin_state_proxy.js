$(document).ready(function() {

	const Toast = Swal.mixin({
		toast: true,
		position: 'top-end',
		showConfirmButton: false,
		timer: 2000
	});
	
	loadState();

	function loadState() {
		$.ajax({
	       type: "get",
	       url: "/admin/products/list_state_proxy",
	       dataType: "json",
	       beforeSend: function(){
	           var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
	           $('tbody').html(html);
	       },
	       success: function (data) {
	           // 1 form-group
	           if (data.length) {
	               screent_state(data);
	               $('[data-toggle="tooltip"]').tooltip();
	           } else {
	               var html = '<td class="text-center text-danger" colspan="6">Không Bang nào trong dữ liệu!</td>';
	               $('tbody').html(html);
	           }
	       },
	       error: function(e) {
	           console.log(e);
	           var html = '<td class="text-center text-danger" colspan="6">Truy vấn sản phẩm lỗi!</td>';
	           $('tbody').html(html);
	       }
	    })
	}

	function screent_state(data) {
	     var html = '';
	     $.each(data , function (index, value) {
	       	html += '<tr>';
	       	html += '<td>'+ value.name +'</td>';
	       	html += '<td>' + value.id_state + '</td>';
	       	html += '<td>';
	       	if ( value.hidden ) {
				html += '<div class="form-group" style="padding-left: 10px;position: relative;">';
				html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
				html += '<input type="checkbox" class="custom-control-input btnHiddenState" id="customSwitch'+ value.id +'" data-id="'+ value.id +'" data-type="vps">';
				html += '<label class="custom-control-label" for="customSwitch'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Tắt"></label>';
			} else {
				html += '<div class="form-group" style="padding-left: 10px;position: relative;">';
				html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
				html += '<input type="checkbox" class="custom-control-input btnHiddenState" id="customSwitch'+ value.id +'" data-id="'+ value.id +'" data-type="vps" checked>';
				html += '<label class="custom-control-label" for="customSwitch'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Bật"></label>';
			}
       		html += '</div>';
           	html += '</div>';
	       	html += '</td>';
	       	html += '<td>';
	       	html += '<button type="button" name="button" class="btn btn-warning edit_agency mr-1" data-id="'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa Bang"><i class="far fa-edit"></i></button>';
	       	html += '<button type="button" name="button" class="btn btn-danger delete_agency" data-name="'+ value.name +'" data-id="'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Xóa Bang"><i class="far fa-trash-alt"></i></button>';
	       	html += '</td>';
	       	html += '<tr>';
	     });
	     $('tbody').html(html);
	}

	$(document).on('click', '.create_state', function () {
		$('#modal-product').modal('show');
		$('.modal-title').text('Tạo Bang');
		$('#notication_vld').html("");
		$('#form-group-product')[0].reset();
		$('#action').val('create');
	})

	$(document).on('click', '.edit_agency', function() {
		var id = $(this).attr('data-id');
	    // console.log(id, $(this));
	    $('#form-group-product')[0].reset();
	    $.ajax({
	    	url: "/admin/products/detail_state_proxy",
	    	data: {id: id},
	    	dataType: "json",
	    	success: function (data) {
	        // console.log(data);
		        $('#modal-product').modal('show');
				$('.modal-title').text('Sửa Bang');
		        $('#notication_vld').html("");

		        $('#id').val(data.id);
		        $('#name').val(data.name);
		        $('#id_state').val(data.id_state);
		        $('#action').val('update');
		        if ( data.hidden ) {
		        	$('#hidden').attr("checked" , true);
		        } else {
		        	$('#hidden').attr("checked" , false);
		        }
		    },
		    error: function (e) {
		    	console.log(e);
		    }
		});
	});

	$('#button-submit').on('click', function(event) {
		event.preventDefault();
		var form_data = $('#form-group-product').serialize();
		let action = $('#action').val();
    	// console.log(form_data);
    	$.ajax({
    		type: "post",
    		url: "/admin/products/action_state_proxy",
    		data: form_data,
    		dataType: "json",
    		beforeSend: function(){
    			var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
    			$('#notication_vld').html(html);
    			$('#form-group-product').css('opacity', 0.5);
    			$('#button-submit').attr('disabled', true);
    			$('#form-group-product').attr('disabled', true);
    		},
    		success: function (response) {
          		// console.log(response);
          		if (response) {
					if ( action == 'create' ) {
						var html = '<p class="text-success">Tạo Bang cho Proxy thành công</p>';
					} else {
						var html = '<p class="text-success">Sửa Bang cho Proxy thành công</p>';
					}
          			$('#notication_vld').html(html);
          			$('#form-group-product')[0].reset();
					$('.modal-title').text('Tạo Bang');
          			$('#form-group-product').css('opacity', 1);
          			$('#button-submit').attr('disabled', false);
          			$('#form-group-product').attr('disabled', false);
          		} else {
					if ( action == 'create' ) {
						var html = '<p class="text-danger">Tạo Bang cho Proxy thất bại</p>';
					} else {
						var html = '<p class="text-danger">Sửa Bang cho Proxy thất bại</p>';
					}
          			$('#notication_vld').html(html);
          			$('#form-group-product').css('opacity', 1);
          			$('#button-submit').attr('disabled', false);
          			$('#form-group-product').attr('disabled', false);
          		}
          		loadState();
          	},
          	error: function(e) {
          		console.log(e);
          		var html = '<p class="text-danger">Truy vấn sản phẩm thất bại</p>';
          		$('#notication_vld').html(html);
          		$('#form-group-product').css('opacity', 1);
          		$('#button-submit').attr('disabled', false);
          		$('#form-group-product').attr('disabled', false);
          	}
         });
    });

    // delete product
    $(document).on('click', '.delete_agency', function() {
    	$('#delete-product').modal('show');
    	$('tr').removeClass('choose_agency');
    	var id = $(this).attr('data-id');
    	var name = $(this).attr('data-name');
    	var html = '<span class="text-center">Bạn có muốn xóa Bang (<b class="text-danger">'+ name +'</b>) này không?</span>';
    	$('#button-product').attr('data-id', id);
    	$('#button-product').attr('data-action', 'delete');
    	$('#button-product').val('Xóa Bang');
    	$(this).closest('tr').addClass('choose_agency');
    	$('#notication-product').html(html);
    });


  // click vao button-product
  $('#button-product').on('click', function () {
    var id = $(this).attr('data-id');
    var token = $('meta[name="csrf-token"]').attr('content');
    // //console.log(id, action);
    $.ajax({
    	type: "post",
    	url: "/admin/products/action_state_proxy",
        data: { '_token': token, id: id, action: 'delete' },
    	dataType: "json",
    	beforeSend: function(){
    		var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
    		$('#notication-product').html(html);
    		$('#button-product').attr('disabled', true);
    		$('#delete-product').css('opacity', 0.5);
    	},
    	success: function (data) {
	        // console.log(data);
	        var html = '<div class="text-center text-success">Xóa Bang thành công.</div>';
	        $('#notication-product').html(html);
	    	$('#button-product').attr('disabled', false);
	    	$('#delete-product').css('opacity', 1);
	        loadState();
	    },
	    error: function(e) {
	    	console.log(e);
	    	var html = '<div class="text-center text-danger">Truy vấn sản phẩm thất bại.</div>';
	    	$('#notication-product').html(html);
	    	$('#button-product').attr('disabled', false);
	    	$('#delete-product').css('opacity', 1);
	    }
    });
  });

  	$(document).on('click', '.btnHiddenState', function () {
		var id = $(this).attr('data-id');
		var token = $('meta[name="csrf-token"]').attr('content');
		if (this.checked){
			var action = 'on';
			var typeCheked = 'checked';
		}
		else{
			var action = 'off';
			var typeCheked = 'unchecked';
		}
		$.ajax({
			type: "post",
			url: "/admin/products/action_state",
			data: { '_token': token, id: id, action: action },
			dataType: "json",
			success: function (data) {
				//console.log(data);
				if (data.error) {
					if ( typeCheked == 'checked' ) {
						$(document).Toasts('create', {
							class: 'bg-danger',
							title: 'Bang VPS US',
							body: "Bật thất bại",
						})
					}
					else {
						$(document).Toasts('create', {
							class: 'bg-danger',
							title: 'Bang VPS US',
							body: "Tắt thất bại",
						})
					}
				} else {
					if ( typeCheked == 'checked' ) {
						$(document).Toasts('create', {
							class: 'bg-success',
							title: 'Bang VPS US',
							body: "Bật thành công",
						})
					}
					else {
						$(document).Toasts('create', {
							class: 'bg-success',
							title: 'Bang VPS US',
							body: "Tắt thành công",
						})
					}
				}
			},
			error: function(e) {
				console.log(e);
				$(document).Toasts('create', {
					class: 'bg-danger',
					title: 'Bang VPS US',
					body: "Truy vấn lỗi",
				})
			}
		});
	});


});