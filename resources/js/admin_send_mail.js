$(document).ready(function () {

    loadSendMail();

    function loadSendMail() {
        $.ajax({
            url: "/admin/send_mail/load_mail",
            dataType: "json",
            beforeSend: function(){
                var html = '<td  colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    screentMail(data);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $('tbody').html('<td colspan="6" class="text-center text-danger">Không có mail trong cơ sở dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="6" class="text-center text-danger">Truy vấn quản trị lỗi.</td>');
            }
        });
    }

    function screentMail(data) {
        $('.total_send').text(data.total_send_mail);
        $('.total_mail').text(data.total_mail);
        var html = '';
        $.each(data.data , function (index, mail) {
            html += '<tr>';
            html += '<td><a href="/admin/users/detail/'+ mail.user_id +'">' + mail.user_name + '</a></td>';
            html += '<td><a href="/admin/users/detail/'+ mail.user_id +'">' + mail.user_mail + '</a></td>';
            html += '<td>' + mail.type_send_mail + '</td>';
            html += '<td>' + mail.date_create_mail + '</td>';
            if ( mail.status ) {
                html += '<td><span class="text-success">Đã gửi (' + mail.send_time + ')</span></td>';
            } else {
                html += '<td><span class="text-danger">Chưa gửi</span></td>';
            }
            html += '<td>';
            html += '<button type="button" class="btn btn-warning text-light edit_email mr-2" data-toggle="tooltip" data-id="'+ mail.id +'" data-placement="top" title="Chi tiết mail"><i class="far fa-edit"></i></button>';
            // html += '<button type="button" class="btn btn-danger delete_email" data-id="'+ mail.id +'" data-toggle="tooltip" data-placement="top" title="Xóa mail"><i class="far fa-trash-alt"></i></button>';
            html += '</td>';
            html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho user
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if ( total / per_page > 11 ) {
              var page = parseInt(total/per_page + 1);
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_all">';
              if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              if (current_page < 7) {
                for (var i = 1; i < 9; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              else if (current_page >= 7 && current_page <= page - 7) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = current_page - 3; i <= current_page +3; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
              }
              else if (current_page >= page - 6) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 6; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
              }

              if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            } else {
              var page = total/per_page + 1;
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_all">';
              if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                      active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }

              }
              if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

    $(document).on('click', '.pagination_all a', function () {
        var page = $(this).attr('data-page');
        $.ajax({
            url: "/admin/send_mail/load_mail",
            data: {page: page},
            dataType: "json",
            beforeSend: function(){
                var html = '<td  colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    screentMail(data);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $('tbody').html('<td colspan="6" class="text-center text-danger">Không có mail trong cơ sở dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="6" class="text-center text-danger">Truy vấn quản trị lỗi.</td>');
            }
        });
    })

    $(document).on('click', '.edit_email' , function () {
        $('#detail_invoice').modal('show');
        $('#btn_delete_quotation').fadeOut('hide');
        $('#finish_quotation').fadeOut('hide');
        let id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/send_mail/detail_mail",
            data: {id : id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#notication_mail').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.error == 0 ) {
                    // console.log('da den');
                    let html = '';
                    html += '<div>';
                    html += '<strong>Loại Email: </strong>' + data.type_send_mail + '<br>';
                    html += '<strong>Tiêu đề: </strong><span class="email-title">';
                    html += data.subject ? data.subject : data.content.subject;
                    html += '</span><br>';
                    html += '<strong>Ngày tạo: </strong>' + data.date_create_mail + '<br>';
                    if ( data.status ) {
                      html += '<strong>Trạng thái: </strong><span class="text-success">Đã gửi ' + data.send_time + '</span><br>';
                    } else {
                      html += '<strong>Trạng thái: </strong><span class="text-danger">Chưa gửi</span><br>';
                    }
                    html += '<strong>Nội dung: </strong><br>';
                    html += '<div class="content-mail">'
                    html += loadContent(data);
                    html += '</div>';
                    html += '</div>';
                    $('#notication_mail').html(html);
                } else {
                    $('#notication_mail').html('<div class="text-center text-danger">Không có mail trong cơ sở dữ liệu.</div>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication_mail').html('<div class="text-center text-danger">Truy vấn quản trị lỗi.</div>');
            }
        });
    });

    function loadContent(data) {
        let html = '';
        if ( data.type == 'mail_cron_nearly' ) {
            // console.log(data.content.subject, data.content);
            html += '<br>';
            if ( data.type_service == 'vps' || data.type_service == 'vps_us' ) {
                html += 'Xin chào '+ data.content.name +',<br>';
                html += 'Cloudzone kính gửi thông tin các VPS hết hạn của quý khách: <br>';
                if ( data.content.expire_minus_3 ) {
                    html += '- Danh sách VPS hết hạn 3 ngày: <br>';
                    $.each(data.content.expire_minus_3.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_3.amount + ' <br>';
                }
                if ( data.content.expire_minus_2 ) {
                    html += '- Danh sách VPS hết hạn 2 ngày: <br>';
                    $.each(data.content.expire_minus_2.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_2.amount + ' <br>';
                }
                if ( data.content.expire_minus_1 ) {
                    html += '- Danh sách VPS hết hạn 1 ngày: <br>';
                    $.each(data.content.expire_minus_1.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_1.amount + ' <br>';
                }
                if ( data.content.expire_0 ) {
                    html += '- Danh sách VPS còn hạn 0 ngày: <br>';
                    $.each(data.content.expire_0.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_0.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_0.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_0.amount + ' <br>';
                }
                if ( data.content.expire_1 ) {
                    html += '- Danh sách VPS còn hạn 1 ngày: <br>';
                    $.each(data.content.expire_1.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_1.amount + ' <br>';
                }
                if ( data.content.expire_2 ) {
                    html += '- Danh sách VPS còn hạn 2 ngày: <br>';
                    $.each(data.content.expire_2.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_2.amount + ' <br>';
                }
                if ( data.content.expire_3 ) {
                    html += '- Danh sách VPS còn hạn 3 ngày: <br>';
                    $.each(data.content.expire_3.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_3.amount + ' <br>';
                }
                if ( data.content.expire_4 ) {
                    html += '- Danh sách VPS còn hạn 4 ngày: <br>';
                    $.each(data.content.expire_4.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_4.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_4.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_4.amount + ' <br>';
                }
                if ( data.content.expire_5 ) {
                    html += '- Danh sách VPS còn hạn 5 ngày: <br>';
                    $.each(data.content.expire_5.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_5.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_5.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_5.amount + ' <br>';
                }
                html += 'Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>';
            }
            else if ( data.type_service == 'proxy' ) {
                html += 'Xin chào '+ data.content.name +',<br>';
                html += 'Cloudzone kính gửi thông tin các Proxy hết hạn của quý khách: <br>';
                if ( data.content.expire_minus_3 ) {
                    html += '- Danh sách Proxy hết hạn 3 ngày: <br>';
                    $.each(data.content.expire_minus_3.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_3.amount + ' <br>';
                }
                if ( data.content.expire_minus_2 ) {
                    html += '- Danh sách Proxy hết hạn 2 ngày: <br>';
                    $.each(data.content.expire_minus_2.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_2.amount + ' <br>';
                }
                if ( data.content.expire_minus_1 ) {
                    html += '- Danh sách Proxy hết hạn 1 ngày: <br>';
                    $.each(data.content.expire_minus_1.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_1.amount + ' <br>';
                }
                if ( data.content.expire_0 ) {
                    html += '- Danh sách Proxy còn hạn 0 ngày: <br>';
                    $.each(data.content.expire_0.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_0.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_0.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_0.amount + ' <br>';
                }
                if ( data.content.expire_1 ) {
                    html += '- Danh sách Proxy còn hạn 1 ngày: <br>';
                    $.each(data.content.expire_1.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_1.amount + ' <br>';
                }
                if ( data.content.expire_2 ) {
                    html += '- Danh sách Proxy còn hạn 2 ngày: <br>';
                    $.each(data.content.expire_2.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_2.amount + ' <br>';
                }
                if ( data.content.expire_3 ) {
                    html += '- Danh sách Proxy còn hạn 3 ngày: <br>';
                    $.each(data.content.expire_3.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_3.amount + ' <br>';
                }
                if ( data.content.expire_4 ) {
                    html += '- Danh sách Proxy còn hạn 4 ngày: <br>';
                    $.each(data.content.expire_4.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_4.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_4.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_4.amount + ' <br>';
                }
                if ( data.content.expire_5 ) {
                    html += '- Danh sách Proxy còn hạn 5 ngày: <br>';
                    $.each(data.content.expire_5.list_ip, function (index, ip) {
                         html += '&nbsp;&nbsp;&nbsp;&nbsp; + IP: '+ ip +' <br>';
                    });
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_5.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_5.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_5.amount + ' <br>';
                }
                html += 'Lưu ý: Proxy sau khi hết hạn sẽ bị off. Dữ liệu Proxy của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>';
            }
            else if ( data.type_service == 'server' ) {
                html += 'Xin chào '+ data.content.name +',<br>';
                html += 'Cloudzone kính gửi thông tin các Server hết hạn của quý khách: <br>';
                if ( data.content.expire_minus_3 ) {
                    html += '- Danh sách Server hết hạn 3 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_3.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_3.amount + ' <br>';
                }
                if ( data.content.expire_minus_2 ) {
                    html += '- Danh sách Server hết hạn 2 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_2.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_2.amount + ' <br>';
                }
                if ( data.content.expire_minus_1 ) {
                    html += '- Danh sách Server hết hạn 1 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_1.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_1.amount + ' <br>';
                }
                if ( data.content.expire_0 ) {
                    html += '- Danh sách Server còn hạn 0 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_0.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_0.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_0.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_0.amount + ' <br>';
                }
                if ( data.content.expire_1 ) {
                    html += '- Danh sách Server còn hạn 1 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_1.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_1.amount + ' <br>';
                }
                if ( data.content.expire_2 ) {
                    html += '- Danh sách Server còn hạn 2 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_2.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_2.amount + ' <br>';
                }
                if ( data.content.expire_3 ) {
                    html += '- Danh sách Server còn hạn 3 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_3.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_3.amount + ' <br>';
                }
                if ( data.content.expire_4 ) {
                    html += '- Danh sách Server còn hạn 4 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_4.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_4.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_4.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_4.amount + ' <br>';
                }
                if ( data.content.expire_5 ) {
                    html += '- Danh sách Server còn hạn 5 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_5.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_5.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_5.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_5.amount + ' <br>';
                }
                html += 'Lưu ý: Server sau khi hết hạn sẽ bị tắt. Dữ liệu Server của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>';
            }
            else if ( data.type_service == 'colocation' ) {
                html += 'Xin chào '+ data.content.name +',<br>';
                html += 'Cloudzone kính gửi thông tin các Colocation hết hạn của quý khách: <br>';
                if ( data.content.expire_minus_3 ) {
                    html += '- Danh sách Colocation hết hạn 3 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_3.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_3.amount + ' <br>';
                }
                if ( data.content.expire_minus_2 ) {
                    html += '- Danh sách Colocation hết hạn 2 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_2.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_2.amount + ' <br>';
                }
                if ( data.content.expire_minus_1 ) {
                    html += '- Danh sách Colocation hết hạn 1 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_minus_1.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_minus_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_minus_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_minus_1.amount + ' <br>';
                }
                if ( data.content.expire_0 ) {
                    html += '- Danh sách Colocation còn hạn 0 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_0.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_0.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_0.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_0.amount + ' <br>';
                }
                if ( data.content.expire_1 ) {
                    html += '- Danh sách Colocation còn hạn 1 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_1.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_1.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_1.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_1.amount + ' <br>';
                }
                if ( data.content.expire_2 ) {
                    html += '- Danh sách Colocation còn hạn 2 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_2.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_2.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_2.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_2.amount + ' <br>';
                }
                if ( data.content.expire_3 ) {
                    html += '- Danh sách Colocation còn hạn 3 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_3.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_3.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_3.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_3.amount + ' <br>';
                }
                if ( data.content.expire_4 ) {
                    html += '- Danh sách Colocation còn hạn 4 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_4.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_4.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_4.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_4.amount + ' <br>';
                }
                if ( data.content.expire_5 ) {
                    html += '- Danh sách Colocation còn hạn 5 ngày: <br>';
                    html += '<div style="margin: 0.6em 3.33em 0.6em 1em;">';
                    $.each(data.content.expire_5.list_ip, function (index, ip) {
                         html += '+ IP: '+ ip +' <br>';
                    });
                    html += '</div>';
                    html += '&nbsp;&nbsp;- Số lượng: ' + data.content.expire_5.quantity + '<br>';
                    html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.expire_5.next_due_date + ' <br>';
                    html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.expire_5.amount + ' <br>';
                }
                html += 'Lưu ý: Colocation sau khi hết hạn sẽ bị tắt. Dữ liệu Colocation của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 3-7 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>';
            }
            else if ( data.type_service == 'hosting' ) {
                html += 'Xin chào '+ data.content.name +',<br>';
                html += 'Cloudzone kính gửi thông tin các Hosting hết hạn của quý khách: <br>';
                html += '- Danh sách Hosting sắp hết hạn: <br>';
                $.each(data.content.list_domain, function (index, domain) {
                    html += '&nbsp;&nbsp; + Domain: '+ domain.domain +' <br>';
                    html += '&nbsp;&nbsp; + Thời gian thuê: '+ billing[domain.billing_cycle] +' <br>';
                });
                html += '&nbsp;&nbsp;- Số lượng: ' + data.content.quantity + '<br>';
                html += '&nbsp;&nbsp;- Ngày hết hạn: ' + data.content.next_due_date + ' <br>';
                html += '&nbsp;&nbsp;- Thành tiền: ' + data.content.amount + ' <br>';
                html += 'Lưu ý: Hosting sau khi hết hạn sẽ bị Suspend. Dữ liệu Hosting của quý khách sẽ được lưu trữ trong vài ngày sau đó, và <b style="color:red;">bị xóa trong vòng 10-15 ngày</b> tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>';
            }
        }
        else if ( data.type == 'mail_order_vps' || data.type == 'mail_order_server' || data.type == 'mail_order_vps_us' || data.type == 'mail_order_hosting' ||
          data.type == 'mail_order_expire_vps' || data.type == 'mail_order_expire_vps_us' || data.type == 'mail_order_expire_hosting' ||
          data.type == 'mail_finish_order_vps' || data.type == 'mail_finish_order_hosting' || data.type == 'mail_finish_order_email_hosting' ||
          data.type == 'mail_finish_order_expire_vps' || data.type == 'mail_finish_order_expire_hosting' || data.type == 'mail_cron_auto_refurn_finish'
          || data.type == 'mail_finish_order_server' || data.type == 'mail_finish_order_expire_server' || data.type == 'mail_finish_config_server'
          || data.type == 'mail_finish_order_colo' || data.type == 'mail_order_colo' || data.type == 'mail_finish_payment_colo'
          || data.type == 'mail_finish_order_expire_proxy' || data.type == 'mail_finish_order_proxy' || data.type == 'mail_finish_payment_colo'
        )  {
          html = data.content;
        }
        else if ( data.type == 'test_mail_template_marketing' || data.type == 'mail_template_marketing' ) {
          html = data.content.content;
        }
        else if ( data.type == 'admin_send_mail' || data.type == 'mail_order_change_ip' ) {
          html = data.content.content;
        }
        else if ( data.type == 'mail_order_finish_change_ip' ) {
            // html = '<p>Xin chào <b>' + data.content.name + '<b>,</p>';
            // html += '<p>Cloudzone xin thông báo đã hoàn thành yêu cầu thay đổi IP cho VPS US</p>';
            // html += '<b>Thông tin VPS:</b><br />';
            // html += '- IP cũ: '+ data.content.iped +' <br>';
            // html += '- IP mới: '+ data.content.ip +' <br>';
            // html += '- Tài khoản: '+ data.content.user +' <br>';
            // html += '- Mật khẩu: '+ data.content.password +' <br>';
            // html += '- Bang: '+ data.content.state +' ';
            // html += '<p>';
            // html += '</p>';
            // html += '<p><b>Cloudzone xin trân trọng cảm ơn Quý khách đã sử dụng dịch vụ!</b></p>';
            html = data.content.content;
        }
        else if ( data.type == 'order_upgrade_hosting' ) {
          html = 'Xin chào ' + data.content.name + ',<br>';
          html += 'Cảm ơn quý khách đã đặt hàng. Thông tin đặt hàng của quý khách: <br><br>';
          html += '<b>Thông tin nâng cấp Hosting</b> <br>';
          html += 'Hosting: ' + data.content.domain +  ' <br>';
          html += 'Gói Hosting: ' + data.content.product_name +  ' <br>';
          html += 'Thành tiền: ' + addCommas(data.content.amount) +  ' VNĐ <br>';
          html += '<b>Cloudzone xin cảm ơn quý khách!</b>';
        }
        else if ( data.type == 'finish_order_upgarde_hosting' ) {
          html = 'Xin chào ' + data.content.name + ',<br>';
          html += 'Cảm ơn quý khách đã đặt hàng thành công. Thông tin đặt hàng của quý khách: <br><br>';
          html += '<b>Thông tin nâng cấp Hosting</b> <br>';
          html += 'Hosting: ' + data.content.domain +  ' <br>';
          html += 'Dịch vụ Hosting: ' + data.content.product_name +  ' <br>';
          html += '<b>Cloudzone đã hoàn tất nâng cấp dịch vụ Hosting cho quý khách!</b>';
        }
        else if ( data.type == 'order_addon_vps' ) {
          html = 'Xin chào ' + data.content.name + ',<br>';
          html += 'Cảm ơn quý khách đã đặt hàng. Thông tin đặt hàng của quý khách: <br><br>';
          html += '<b>Thông tin cấu hình thêm VPS</b> <br>';
          if ( data.content.cpu ) {
            html += 'Tên sản phẩm: ' + data.content.product_cpu.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_cpu.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.cpu + ' cores<br>';
          }
          if ( data.content.ram ) {
            html += 'Tên sản phẩm: ' + data.content.product_ram.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_ram.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.ram + ' cores<br>';
          }
          if ( data.content.disk ) {
            html += 'Tên sản phẩm: ' + data.content.product_disk.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_disk.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.disk + ' cores<br>';
          }
          html += 'Thời gian: ' + data.content.time + '<br>';
          html += 'Thành tiền: ' + addCommas(data.content.amount) +  ' VNĐ <br>';
          html += '<b>Cloudzone xin cảm ơn quý khách!</b>';
        }
        else if ( data.type == 'finish_order_addon_vps' ) {
          html = 'Xin chào ' + data.content.name + ',<br>';
          html += 'Cảm ơn quý khách đã đặt hàng thành công. Thông tin đặt hàng của quý khách: <br><br>';
          html += '<b>Thông tin cấu hình thêm VPS</b> <br>';
          if ( data.content.cpu ) {
            html += 'Tên sản phẩm: ' + data.content.product_cpu.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_cpu.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.cpu + ' cores<br>';
          }
          if ( data.content.ram ) {
            html += 'Tên sản phẩm: ' + data.content.product_ram.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_ram.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.ram + ' cores<br>';
          }
          if ( data.content.disk ) {
            html += 'Tên sản phẩm: ' + data.content.product_disk.name + ' <br>';
            html += 'Giá: ' + addCommas(data.content.product_disk.pricing[data.content.billing_cycle]) + ' <br>';
            html += 'Số lượng: ' + data.content.disk + ' cores<br>';
          }
          html += 'Thời gian: ' + data.content.time + '<br>';
          html += 'Thành tiền: ' + addCommas(data.content.amount) +  ' VNĐ <br>';
          html += '<b>Cloudzone đã hoàn tất nâng cấp cấu hình VPS cho quý khách!</b>';
        }
        else if ( data.type == 'mail_create_user' ) {
          html = 'Kính chào ' + data.content.name + ',<br>';
          html += 'Chúc mừng Quý khách hàng đã đăng ký thành công tài khoản sử dụng Portal của Cloudzone<br>';
          html += 'Dưới đây là thông tin đăng nhập tài khoản khách hàng của Quý khách: <br>';
          html += 'Dưới đây là thông tin đăng nhập tài khoản khách hàng của Quý khách: <br>';
          html += '- <b>Tài khoản:</b> ' + data.content.email + '<br>';
          html += '- <b>Mật khẩu:</b> ' + data.content.password +  ' <br>';
          html += '- <b>Liên kết đăng nhập:</b> <a href="https://portal.cloudzone.vn/">Portal Cloudzone</a> <br> <br>';
          html += `Với tài khoản này, Quý khách hoàn toàn có thể chủ động:<br>
                  <ul>
                      <li>
                          Đặt mua các dịch vụ: Cloud VPS Việt Nam, VPS US/UK/…., đăng ký hosting email, tên miền
                      </li>
                      <li>
                          Quản lý dịch vụ đang dùng: tự động gia hạn dịch vụ và thanh toán trực tuyến các hóa đơn
                      </li>
                  </ul>
                  <br>
                  Tham khảo bảng giá các gói dịch vụ của chúng tôi: <br>
                  <ul>
                      <li>
                          Dịch vụ Cloud Hosting:
                          <a href="https://cloudzone.vn/cloud-hosting/">https://cloudzone.vn/cloud-hosting/</a>
                      </li>
                      <li>
                          Dịch vụ Cloud VPS:
                          <a href="https://cloudzone.vn/cloud-server/">https://cloudzone.vn/cloud-server/</a>
                      </li>
                  </ul>
                  <br>
                  Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ. <br>
                  Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br>
                  Trân trọng! <br>`;
        }
        else if ( data.type == 'admin_create_payment' ) {
          html = 'Xin chào ' + data.content.name + ',<br>';
          html += 'Cảm ơn bạn đã sử dụng dịch vụ của Cloudzone.vn, dưới đây là thông tin nạp tiền vào tài khoản của quý khách: <br><br>';
          html += '<b>Chi tiết nạp tiền:</b> <br>';
          html += 'Số tiền: ' + addCommas(data.content.amount) + ' VNĐ<br>';
          html += 'Thời gian: '+ data.send_time +' <br>';
          html += 'Mã giao dịch: '+ data.content.ma_gd +'<br>';
          html += '<b>Cloudzone xin cảm ơn quý khách!!</b>';
        }
        else if ( data.type == 'mail_forgot_password' ) {
            html = 'Xin chào ' + data.content.name + ',<br>';
            html += 'Chào mừng bạn đến với Cloudzone – Nhà cung cấp dịch vụ: Domain, Hosting, Cloud VPS, Server <br>';
            html += 'Yêu cầu đặt lại mật khẩu của bạn thành công. Vui lòng nhấp vào liên kết bên dưới để để bắt đầu đặt lại mật khẩu.';
            html += `<div class="mail-button" style="padding-bottom: 25px;">
                        <a href="/reset_password_forgot_form/${data.content.token}" style="color: blue;" target="_blank">Đặt lại mật khẩu</a>
                    </div>`;
        }
        else if ( data.type == 'request_payment'  ) {
          // html = 'Xin chào ' + data.content.name + ',<br>';
          // html += 'Hệ thống portal vừa ghi nhận yêu cầu nạp tiền vào tài khoản của bạn. <br>';
          // html += 'Có 2 hình thức để nạp tiền:<br> <br>';
          // html += '-  Chuyển khoản vào tài khoản Vietcombank: <br>';
          // html += '&nbsp;&nbsp;Tên tài khoản: Nguyễn Thị Ái Hoa <br>';
          // html += '&nbsp;&nbsp;STK: 0041000830880 <br>';
          // html += '&nbsp;&nbsp;Nội dung chuyển khoản: '+ data.content.ma_gd +' (vui lòng điền chính xác) <br><br>';
          // html += '-  Thanh toán trực tiếp tại văn phòng: khách hàng vui lòng đến văn phòng Cloudzone tại địa chỉ 67 Nguyễn Thị Định, TP Đà Nẵng để giao dịch. Sau khi hoàn thành, chúng tôi sẽ thực hiện điều chỉnh tăng số dư của khách hàng.<br><br>';
          // html += '* Quý khách có thể tham khảo bài hướng dẫn chi tiết nạp tiền vào tài khoản portal ';
          // html += '<a href="https://support.cloudzone.vn/knowledge-base/huong-dan-su-dung-cloudzone-portal/">tại đây</a><br>';
          // html += '<b>Cloudzone xin cảm ơn quý khách!!</b>';
          html = data.content.content;
        }
        else if ( data.type == 'finish_payment' ) {
          // html = 'Xin chào ' + data.content.name + ',<br>';
          // html += 'Hệ thống portal vừa ghi nhận bạn đã nạp số tiền: '+ addCommas(data.content.amount) +' VNĐ vào tài khoản thành công! <br><br>';
          // html += '<b>Chi tiết giao dịch:</b> <br>';
          // html += '&nbsp;&nbsp;Số tiền: '+ addCommas(data.content.amount) +' VNĐ <br>';
          // html += '&nbsp;&nbsp;Thời gian: '+ data.send_time +' <br>';
          // html += '&nbsp;&nbsp;Số dư tài khoản: '+ addCommas(data.content.credit) +' VNĐ<br>';
          // html += 'Cloudzone xin chân thành cảm ơn quý khách!';
          html = data.content.content;
        }
        else if ( data.type == 'mail_finish_rebuild_vps' || data.type == 'mail_rebuild_error_vps' ) {
          html += 'Thông tin VPS đã cài đặt lại <br>';
          html += '- ip: '+ data.content.ip +' <br>';
          html += '- OS:';
          if ( data.content.os == 1 ) {
            html += 'Windows 7 <br>';
            html += '- Security: Có <br>';
          }
          else if ( data.content.os == 2 ) {
            html += 'Windows Server 2012 R2<br>';
            html += '- Security: Có <br>';
          }
          else if ( data.content.os == 3 ) {
            html += 'Windows Server 2016<br>';
            html += '- Security: Có <br>';
          }
          else if ( data.content.os == 4 ) {
            html += 'CentOs 7 64bit<br>';
            html += '- Security: Có <br>';
          }
          else if ( data.content.os == 14 ) {
            html += 'CentOs 7 64bit<br>';
            html += '- Security: Không <br>';
          }
          else if ( data.content.os == 13 ) {
            html += 'Windows Server 2016<br>';
            html += '- Security: Không <br>';
          }
          else if ( data.content.os == 12 ) {
            html += 'Windows Server 2012 R2<br>';
            html += '- Security: Không <br>';
          }
          else {
            html += 'Windows Server 2012 R2<br>';
          }
        }
        else if ( data.type == 'mail_create_error_vps' || data.type == 'mail_create_error_vps_us' ) {
          html += '<b>Thông tin';
          if ( data.type == 'mail_create_error_vps' ) {
            html += ' VPS ';
          } else {
            html += ' VPS US ';
          }
          html += '</b> <br>';
          html += '- Customer ID: ' + data.content.customer + ' <br>';
          html += '- Khách hàng: ' + data.content.user_name + ' <br>';
          html += '- CPU: ' + data.content.cpu + ' <br>';
          html += '- RAM: ' + data.content.ram + ' <br>';
          html += '- DISK: ' + data.content.disk + ' <br>';
          html += '- Thời gian: ' + data.send_time + ' <br>';
        }
        else if ( data.type == 'mail_expire_error_vps' || data.type == 'mail_expire_error_vps_us' ) {
          html += '<b>Thông tin khách hàng</b> <br><br>';
          html += data.content.error_vps;
          html += '<br>- Khách hàng: '+ data.content.user_name +' <br>'
        }
        else if ( data.type == 'mail_upgrade_error_vps' ) {
          html += '<b>Thông tin khách hàng</b> <br>';
          html += '<br>- vm_id: '+ data.content.name +' <br>'
          html += '<br>- extra_cpu: '+ data.content.extra_cpu +' <br>'
          html += '<br>- extra_ram: '+ data.content.extra_ram +' <br>'
          html += '<br>- extra_disk: '+ data.content.extra_disk +' <br>'
          html += '<br>- total: '+ data.content.package +' <br>'
        }
        else if ( data.type == 'mail_verify_user' ) {
          html += '<p>Kính chào <strong>'+ data.content.name +'</strong>,</p>';
          html += '<p>Quý khách hàng vừa đăng ký tạo tài khoản sử dụng Web Portal của Cloudzone – đơn vị cung cấp dịch vụ Cloud VPS, Hosting chuyên nghiệp.</p>'
          html += '<p>Để sử dụng tài khoản này, vui lòng <a href="https://portal.cloudzone.vn/check-verify/'+ data.content.token +'" style="color: blue;" target="_blank">Kích hoạt tài khoản</a> để xác thực tài khoản của Quý khách. </p>'
          html += '<div class="mail-button" style="padding-bottom: 25px;">'
          html += 'Hoặc copy đường link bên dưới và dán vào trình duyệt để kích hoạt tài khoản. <br>'
          html += 'https://portal.cloudzone.vn/check-verify/'+ data.content.token;
          html += '</div>';
          html += `Với tài khoản này, Quý khách có thể:<br>
                  <ul>
                      <li>
                          Đặt mua các dịch vụ: Cloud VPS Việt Nam, VPS US/UK, đăng ký hosting website, tên miền,....
                      </li>
                      <li>
                          Quản lý dịch vụ đang dùng: on/off/rebuild/console các dịch vụ máy chủ ảo, theo dõi và gia hạn dịch vụ,....
                      </li>
                  </ul>
                  <br>
                  Tham khảo bảng giá các gói dịch vụ của chúng tôi: <br>
                  <ul>
                      <li>
                          Dịch vụ Cloud Hosting:
                          <a href="https://cloudzone.vn/cloud-hosting/">https://cloudzone.vn/cloud-hosting/</a>
                      </li>
                      <li>
                          Dịch vụ Cloud VPS:
                          <a href="https://cloudzone.vn/cloud-server/">https://cloudzone.vn/cloud-server/</a>
                      </li>
                  </ul>
                  <br>
                  Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ. <br>
                  Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br>
                  Trân trọng! <br>`;
        }
        else if ( data.type == 'mail_tutorial_user' ) {
          html += '<p>Kính chào <strong>'+ data.content.name +'</strong>,</p>';
          html += `<p>Chúc mừng Quý khách đã kích hoạt thành công tài khoản Web Portal của Cloudzone.</p>
                  <p>Để thuận tiện trong việc sử dụng Portal, chúng tôi kính gửi Quý khách hướng dẫn sử dụng portal cơ bản,
                    các bước để bắt đầu đăng ký và sử dụng dịch vụ Cloud VPS, Hosting, … tại Cloudzone.</p>
                  <p>Quý khách vui lòng xem bài
                  <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-su-dung-cloudzone-portal/" style="color: #15c;" target="_blank">
                    hướng dẫn sử dụng.</a> </p>
                  <p>Hoặc copy đường link bên dưới và dán vào trình duyệt để xem bài hướng dẫn:<br>
                  https://support.cloudzone.vn/knowledge-base/huong-dan-su-dung-cloudzone-portal/ </p>
                  <p>Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ.</p>
                  <p>Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE!</p>
                  <p>Trân trọng!</p>`;
        }
        else if ( data.type == 'mail_marketing_user' ) {
          html += 'Kính chào '+ data.content.name +',<br>';
          html += `Cảm ơn Quý khách đang sử dụng tài khoản Portal của Cloudzone. Để thuận tiện trong việc sử dụng Portal,
                  chúng tôi kính gửi Quý khách hướng dẫn sử dụng Portal cơ bản, các bước để bắt đầu đăng ký và sử dụng dịch vụ
                  Cloud VPS, Hosting,… tại Cloudzone.  <br>
                  Link bài hướng dẫn:
                  <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-su-dung-cloudzone-portal/" style="color: blue;" target="_blank">hướng dẫn.</a> <br>
                  Hoặc copy đường link bên dưới và dán vào trình duyệt. <br>
                  https://support.cloudzone.vn/knowledge-base/huong-dan-su-dung-cloudzone-portal/ <br>
                  Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ. <br>
                  Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại CLOUDZONE! <br>
                  Trân trọng! <br>`;
        }
        else if ( data.type == 'mail_marketing_tiny' ) {
          html += 'Kính chào '+ data.content.name +',<br>';
          html += `<p>Cloudzone xin giới thiệu <b style="color: #5271ff;">gói VPS Tiny</b> với cấu hình:</p>
                  <div style="line-height: 24px;">
                    <span style="color: #5271ff;font-weight: bold;">CPU: 1 core, Memory: 0.5 GB RAM, Disk SSD: 20 GB</span> <br>
                    <span style="color: #5271ff;font-weight: bold;">IP: 1 IP public</span> <br>
                    <span style="color: #5271ff;font-weight: bold;">Băng thông: 100 Mb/ 10 Mb</span> <br>
                    <span style="color: #5271ff;font-weight: bold;">Hệ điều hành: Linux</span>
                    <p>Chỉ với <b style="color: #5271ff;">70.000 / tháng</b>, Quý khách có thể sử dụng Vps Tiny với mục đích chạy Proxy,
                      website với lưu lượng truy cập nhỏ, nghiên cứu, học tập, làm quen về hệ điều hành Linux, ...</p>
                    <div style="text-align: center !important;">
                        <a href="https://portal.cloudzone.vn/client/products/1/cloud-vps" target="_blank">
                          <button type="button" name="button" class="btn" style="cursor: pointer;background: #5271ff; color: #fff;width: 35%;height: 55px;font-size: 1.4em;">Đăng ký ngay</button>
                        </a>
                    </div>
                    <p>Ngoài ra, khi mua với số lượng từ 5 vps, Quý khách sẽ được nhận mức giá ưu đãi khi đăng ký trở thành
                      đại lý của Cloudzone:</p>
                    <p style="font-style: italic;font-weight: bold;">Tham khảo bảng giá đại lý:</p>
                    <img src="https://portal.cloudzone.vn/images/amountMarketingTiny.JPG" class="img" alt="bảng giá đại lý" style="width: 800px" >
                  </div>
                  <p>Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ.</p>
                  <p>Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại
                    <span style="color: #5271ff;font-weight: bold;">CLOUDZONE!</span>
                  </p>
                  <p>Trân trọng!</p>`;
        }
        else if ( data.type == 'mail_marketing_vps_uk' ) {
          html += 'Kính chào '+ data.content.name +',<br>';
          html += `<p  style="text-align: justify;">Bạn biết tin gì chưa, Cloudzone vừa bổ sung Server riêng tại UK với hạ tầng mạnh mẽ
                    để đáp ứng cho việc khách hàng truy cập từ quốc tế nhanh chóng
                    và ổn định với chi phí rất hợp lý
                  </p>
                  <p style="color: red;font-weight: 700;font-style: italic;">Chỉ với 100.000/ tháng, sở hữu ngay gói VPS UK: 1 Core - 1 Gb Ram - 20 Gb SSD.</p>
                  <p><a href="https://portal.cloudzone.vn/public/client/products/58/vps-uk" target="_blank">Đăng ký ngay</a></p>
                  <p>Ngoài ra, khi mua với số lượng lớn, hoặc trở thành đại lý của Cloudzone, Quý khách sẽ nhận được mức giá ưu đãi cực khủng.</p>
                  <p>Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ.</p>
                  <p>Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại
                    <span style="color: #5271ff;font-weight: bold;">CLOUDZONE!</span>
                  </p>
                  <p>Trân trọng!</p>`;
        }
        else if ( data.type == 'mail_update_feature' ) {
          html += 'Kính chào '+ data.content.name +',<br>';
          html += `<p  style="text-align: justify;">
                          Cảm ơn Quý khách đã sử dụng tài khoản Web Portal của Cloudzone - đơn vị cung cấp dịch vụ Cloud VPS, Hosting chuyên nghiệp.
                        </p>
                        <p>Để nâng cao chất lượng dịch vụ, Cloudzone vừa cập nhật một loạt chức năng mới trên Web Portal.</p>
                        <ul>
                          <li>
                            Chức năng đổi IP cho VPS US: <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-doi-ip-cho-vps-us/" target="_blank">https://support.cloudzone.vn/knowledge-base/huong-dan-doi-ip-cho-vps-us/</a>
                          </li>
                          <!-- style="display: block;" -->
                          <li>
                            Chức năng cài lại hệ điều hành cho VPS US: <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-cai-lai-he-dieu-hanh-vps-us/" target="_blank">https://support.cloudzone. vn/knowledge-base/huong-dan-cai-lai-he-dieu-hanh-vps-us/</a>
                          </li>
                          <li>
                            Reset password cho VPS US
                          </li>
                        </ul>
                        <p style="text-align: justify;">Ngoài ra, quý khách có thể trải nghiệm nhiều tính năng khác đã có trên Portal như:</p>
                        <!--   class="btn_primary btn_tutorial" -->
                        <ul>
                          <li>
                            Tìm kiếm, gia hạn nhiều VPS cùng lúc
                          </li>
                          <li>
                            Chức năng Console VPS
                          </li>
                        </ul>
                        <p style="display: block;">Toàn bộ các hướng dẫn liên quan đến Portal tham khảo tại đây: <a href="https://support.cloudzone.vn/article-categories/huong-dan-su-dung-cloudzone-portal/" target="_blank">https://support.cloudzone.vn/article-categories/huong-dan-su-dung-cloudzone-portal/</a></p>
                        <p>Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ.</p>
                        <p>Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại
                          <span style="color: #5271ff;font-weight: bold;">CLOUDZONE!</span>
                        </p>
                        <p>Trân trọng!</p>`;
        }
        else if ( data.type == 'mail_cron_auto_refurn_error' ) {
          html += 'Xin chào, '+ data.content.name +',<br>';
          html += 'Cloudzone kính gửi thông báo tự động gia hạn VPS thất bại do hết số dư tài khoản. Quý khách vui lòng nạp thêm tiền để gia hạn dịch vụ. <br><br>';
          html += '&nbsp;&nbsp;- Thông tin VPS: <br>';
          html += '&nbsp;&nbsp;- IP: '+ data.content.ip +' <br>';
          html += '&nbsp;&nbsp;- Ngày hết hạn: '+ data.content.next_due_date +' <br>';
          html += '&nbsp;&nbsp;- Thành tiền:  '+ addCommas(data.content.total) +' VNĐ<br>';
          html += '&nbsp;&nbsp;- Số dư tài khoản: '+ addCommas(data.content.credit) +' VNĐ<br>';
          html += '<br>';
          html += `Lưu ý: VPS sau khi hết hạn sẽ bị off. Dữ liệu VPS của quý khách sẽ được lưu trữ trong vài ngày sau đó,
          và <b style="color:red;">bị xóa trong vòng 3-5 ngày</b> tính từ ngày hết hạn.
          Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! <br>`;
          html += '<br><b>Cloudzone xin cảm ơn quý khách!</b>';
        }
        else if ( data.type == 'admin_mail_cron_auto_refurn_error' ) {
          html += '&nbsp;&nbsp;- Khách hàng: '+ data.content.name +' <br>';
          html += '&nbsp;&nbsp;- IP: '+ data.content.ip +' <br>';
        }
        return html;
    }

    $('.btn_filter').on('click', function () {
      var html = '';
      html += '<div class="row">';
      // model
      html += '<div class="col-md-3">';
      html += '<label>Chọn số lượng</label>';
      html += '<select style="width: 100%;" id="qtt" class="form-control">';
      html += '<option value="30" selected>30</option>';
      html += '<option value="50">50</option>';
      html += '<option value="100">100</option>';
      html += '<option value="200">200</option>';
      html += '<option value="300">300</option>';
      html += '<option value="500">500</option>';
      html += '</select>';
      html += '</div>';
      // user
      html += '<div class="col-md-3" id="select_product_form">';
      html += '<label>Chọn khách hàng</label>';
      html += '<div class="search-user">';
      html += '<input type="text" id="searchUser" class="form-control" placeholder="Tìm kiếm khách hàng">';
      html += '<div class="list-search-user">';
      html += '</div>';
      html += '</div>';
      html += '</div>';
      // action
      html += '<div class="col-md-3">';
      html += '<label>Chọn hành động</label>';
      html += '<select style="width: 100%;" id="action" class="form-control select2">';
      html += '<option value="" selected disabled>Chọn hành động</option>';
      html += '<option value="mail_cron_nearly">Nhắc gia hạn</option>';
      html += '<option value="mail_order_vps">Order VPS</option>';
      html += '<option value="mail_order_server">Order Server</option>';
      html += '<option value="mail_order_vps_us">Order VPS US</option>';
      html += '<option value="mail_order_hosting">Order Hosting</option>';
      html += '<option value="order_addon_vps">Nâng cấp VPS</option>';
      html += '<option value="order_upgrade_hosting">Nâng cấp Hosting</option>';
      html += '<option value="mail_order_expire_vps">Gia hạn VPS</option>';
      html += '<option value="mail_order_expire_vps_us">Gia hạn VPS US</option>';
      html += '<option value="mail_order_expire_hosting">Gia hạn Hosting</option>';
      html += '<option value="mail_finish_order_vps">Hoàn thành Order VPS</option>';
      html += '<option value="mail_finish_order_proxy">Hoàn thành Order Proxy</option>';
      html += '<option value="mail_finish_config_server">Hoàn thành cài đặt Server</option>';
      html += '<option value="mail_finish_order_server">Hoàn thành Order Server</option>';
      html += '<option value="mail_finish_order_colo">Hoàn thành Order Colocation</option>';
      html += '<option value="mail_finish_order_hosting">Hoàn thành Order Hosting</option>';
      html += '<option value="mail_finish_order_expire_vps">Hoàn thành gia hạn VPS</option>';
      html += '<option value="mail_finish_order_expire_server">Hoàn thành gia hạn Server</option>';
      html += '<option value="mail_finish_order_expire_hosting">Hoàn thành gia hạn Hosting</option>';
      html += '<option value="mail_finish_order_expire_proxy">Hoàn thành gia hạn Proxy</option>';
      html += '<option value="mail_finish_payment_colo">Hoàn thành gia hạn Colocation</option>';
      html += '<option value="finish_order_addon_vps">Hoàn thành Order nâng cấp VPS</option>';
      html += '<option value="finish_order_upgarde_hosting">Hoàn thành Order nâng cấp Hosting</option>';
      html += '<option value="mail_cron_auto_refurn_finish">Tự động gia hạn thành công</option>';
      html += '<option value="mail_cron_auto_refurn_error">Lỗi tự động gia hạn VPS (hết SDTK)</option>';
      html += '<option value="admin_mail_cron_auto_refurn_error">Lỗi tự động gia hạn VPS</option>';
      html += '<option value="mail_create_user">Mail admin tạo khách hàng</option>';
      html += '<option value="admin_send_mail">Admin gửi mail</option>';
      html += '<option value="admin_create_payment">Admin tạo nạp tiền</option>';
      html += '<option value="request_payment">Yêu cầu nạp tiền</option>';
      html += '<option value="finish_payment">Hoàn thành nạp tiền</option>';
      html += '<option value="mail_finish_rebuild_vps">Hoàn thành cài đặt lại VPS</option>';
      html += '<option value="mail_create_error_vps">Lỗi dashboard tạo VPS</option>';
      html += '<option value="mail_expire_error_vps">Lỗi dashboard gia hạn VPS</option>';
      html += '<option value="mail_create_error_vps_us">Lỗi dashboard tạo VPS US</option>';
      html += '<option value="mail_expire_error_vps_us">Lỗi dashboard gia hạn VPS US</option>';
      html += '<option value="mail_upgrade_error_vps">Lỗi dashboard addon VPS</option>';
      html += '<option value="mail_rebuild_error_vps">Lỗi dashboard rebuild VPS</option>';
      html += '<option value="mail_verify_user">Xác thực tài khoản</option>';
      html += '<option value="mail_tutorial_user">Hướng dẫn sử dụng portal (Tạo User)</option>';
      html += '<option value="mail_marketing_user">Hướng dẫn sử dụng portal (1 tháng / 1 lần)</option>';
      html += '<option value="mail_forgot_password">Yêu cầu đổi mật khẩu</option>';
      html += '<option value="mail_payment_change_ip">Thanh toán đơn hàng đổi IP</option>';
      html += '<option value="mail_order_finish_change_ip">Hoàn thành đổi IP</option>';
      html += '<option value="mail_update_feature">Cập nhật nhiều chức năng mới</option>';
      html += '<option value="test_mail_template_marketing">Test email marketing</option>';
      html += '<option value="mail_template_marketing">Email Marketing Template</option>';
      html += '</select>';
      html += '</div>';
      // service
      html += '<div class="col-md-3">';
      html += '<label>Chọn ngày</label>';
      html += '<input type="date" id="date" class="form-control" placeholder="Tìm kiếm theo ngày">';
      html += '</div>';

      html += '</div>';

      $('.fillter_log').html(html);
      $('.select2').select2();
    })

    $(document).on('keyup', '#searchUser',function () {
      $.ajax({
        url: '/admin/users/searchUser?q=' + $(this).val(),
        dataType: 'json',
        success: function (data) {
            // console.log(data);
            $('.list-search-user').fadeIn();
            var html = '';
            html += '<ul>';
            $.each(data, function (index, user) {
              html += '<li class="choose-user" data-id="'+ user.id +'" data-text="'+ user.name + '">'+ user.name + ' - ' + user.email +'</li>';
            })
            html += '</ul>';

            $('.list-search-user').html(html);
        },
        error: function (e) {
          console.log(e);
          var html = '';
          html += 'Lỗi truy vấn';
          $('.search-user').html(html);
        }
      })
    })

    $(document).on("change", '#qtt', function () {
        loadFilterSendMail();
    });

    $(document).on("click", '.choose-user', function () {
      $('.list-search-user').fadeOut();
      $('#user').val( $(this).attr('data-id') );
      $('#searchUser').val( $(this).attr('data-text') );
      loadFilterSendMail();
    });

    $(document).on("change", '#action', function () {
        loadFilterSendMail();
    });

    $(document).on("change", '#date', function () {
        loadFilterSendMail();
    });

    $(document).on("keyup", '#date', function () {
        loadFilterSendMail();
    });

    function loadFilterSendMail() {
        let qtt = $('#qtt').val();
        let user = $('#user').val();
        let action = $('#action').val();
        let date = $('#date').val();
        // console.log(qtt, user);
        $.ajax({
            url: "/admin/send_mail/loadFilterSendMail",
            data: { qtt: qtt, user: user, action: action, date: date },
            dataType: "json",
            beforeSend: function(){
                var html = '<td  colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    screentFilterSendMail(data);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $('tbody').html('<td colspan="6" class="text-center text-danger">Không có mail trong cơ sở dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="6" class="text-center text-danger">Truy vấn quản trị lỗi.</td>');
            }
        });
    }

    function screentFilterSendMail(data) {
        $('.total_send').text(data.total_send_mail);
        $('.total_mail').text(data.total_mail);
        var html = '';
        $.each(data.data , function (index, mail) {
            html += '<tr>';
            html += '<td><a href="/admin/users/detail/'+ mail.user_id +'">' + mail.user_name + '</a></td>';
            html += '<td><a href="/admin/users/detail/'+ mail.user_id +'">' + mail.user_mail + '</a></td>';
            html += '<td>' + mail.type_send_mail + '</td>';
            html += '<td>' + mail.date_create_mail + '</td>';
            if ( mail.status ) {
                html += '<td><span class="text-success">Đã gửi (' + mail.send_time + ')</span></td>';
            } else {
                html += '<td><span class="text-danger">Chưa gửi</span></td>';
            }
            html += '<td>';
            html += '<button type="button" class="btn btn-warning text-light edit_email mr-2" data-toggle="tooltip" data-id="'+ mail.id +'" data-placement="top" title="Chi tiết mail"><i class="far fa-edit"></i></button>';
            // html += '<button type="button" class="btn btn-danger delete_email" data-id="'+ mail.id +'" data-toggle="tooltip" data-placement="top" title="Xóa mail"><i class="far fa-trash-alt"></i></button>';
            html += '</td>';
            html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho user
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if ( total / per_page > 11 ) {
              var page = parseInt(total/per_page + 1);
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_filter">';
              if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              if (current_page < 7) {
                for (var i = 1; i < 9; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              else if (current_page >= 7 || current_page <= page - 7) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = current_page - 3; i <= current_page +3; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
              }
              else if (current_page >= page - 6) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 6; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
              }

              if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            } else {
              var page = total/per_page + 1;
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination pagination_filter">';
              if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                      active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }

              }
              if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

    $(document).on('click', '.pagination_filter a', function () {
        let qtt = $('#qtt').val();
        let user = $('#user').val();
        let action = $('#action').val();
        let date = $('#date').val();
        var page = $(this).attr('data-page');
        $.ajax({
            url: "/admin/send_mail/loadFilterSendMail",
            data: {qtt: qtt, user: user, action: action, date: date, page: page},
            dataType: "json",
            beforeSend: function(){
                var html = '<td  colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    screentFilterSendMail(data);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $('tbody').html('<td colspan="6" class="text-center text-danger">Không có mail trong cơ sở dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="6" class="text-center text-danger">Truy vấn quản trị lỗi.</td>');
            }
        });
    })

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
