$(document).ready(function () {
    let dem = 0;

    let billing_cycle = {
        'monthly' : '1 Tháng',
        'twomonthly' : '2 Tháng',
        'quarterly' : '3 Tháng',
        'semi_annually' : '6 Tháng',
        'annually' : '1 Năm',
        'biennially' : '2 Năm',
        'triennially' : '3 Năm',
        'one_time_pay' : 'Vĩnh viễn',
    };

    loadQuotation();

    function loadQuotation() {
        let id = $('#id').val();
        $.ajax({
            url: "/admin/users/get_detail_quotation",
            data: {id: id},
            dataType: "json",
            success: function (data) { 
              console.log(data);   
                let html_item_group_product = loadTypeProduct(data.item.detail.type_product);
                $('#type_product').html(html_item_group_product);
                if (data.item.detail.type_product == 'vps' || data.item.detail.type_product == 'vps_us' 
                  || data.item.detail.type_product == 'hosting' || data.item.detail.type_product == 'server' 
                  || data.item.detail.type_product == 'colocation') {
                    let html_item_product = loadProduct(data.item.group_products, data.item.detail.product_id);
                    $('.product').html(html_item_product);
                    $('#product').select2();
                }
                let type_product = data.item.detail.type_product;
                let html_item_detail_product = '';
                if ( type_product == 'vps' || type_product == 'vps_us' ) {
                
                    html_item_detail_product = loadProductVps(data.item.detail);
                    $('.info_quotation').html(html_item_detail_product);
                    $('#qtt_ip').attr('disabled', false);
                    $('#addon_cpu').attr('disabled', false);
                    $('#addon_ram').attr('disabled', false);
                    $('#addon_disk').attr('disabled', false);
                    $('#qtt').attr('disabled', false);
                    $('#os').attr('disabled', false);
                    $('#amount').attr('disabled', false);

                    $.each(data.item.group_products.addon , function (index, product_addon) {
                      if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                        $('#label_addon_cpu').text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                        $('#addon_cpu').attr('data-id', product_addon.id);
                      }
                      if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                        $('#label_addon_ram').text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                        $('#addon_ram').attr('data-id', product_addon.id);
                      }
                      if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                        $('#label_addon_disk').text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                        $('#addon_disk').attr('data-id', product_addon.id);
                      }
                    })
                }else if (type_product == 'colocation'){
                  html_item_detail_product = LoadProductIP(data.item.detail, data.item.product, data.item.group_products,data.item.datacenter);
                  $('.info_quotation').html(html_item_detail_product);
                  $('#qtt_ip').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);
                }
                else if ( type_product == 'server' ) {
                  html_item_detail_product = loadProductServer(data.item.detail, data.item.product, data.item.group_products);
                  $('.info_quotation').html(html_item_detail_product);
                  $('#qtt_ip').attr('disabled', false);
                  $('#addon_cpu').attr('disabled', false);
                  $('#addon_ram_server').attr('disabled', false);
                  $('#addon_disk').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#os').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  if ( data.item.product.second ) {
                    $('#addon_disk2').attr('disabled', true);
                  } else {
                    $('#addon_disk2').attr('disabled', false);
                  }
                  $('#addon_disk3').attr('disabled', false);
                  $('#addon_disk4').attr('disabled', false);
                  $('#addon_disk5').attr('disabled', false);
                  $('#addon_disk6').attr('disabled', false);
                  $('#addon_disk7').attr('disabled', false);
                  $('#addon_disk8').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);
                  var html = '';
                  html += '<label id="" for="">Cấu hình sản phẩm</label>';
                  html += '<div>';
                  html += data.item.product.config;
                  html += '</div>';
                  $('#config_server').html(html);
                }
                else if ( type_product == 'design_webiste' || type_product == 'domain' || type_product == 'hosting' ) {
                    html_item_detail_product = loadProductDomain(data.item.detail);
                    $('.info_quotation').html(html_item_detail_product);
                    $('#qtt').attr('disabled', false);
                    $('#domain').attr('disabled', false);
                    $('#amount').attr('disabled', false);
                }
                // sản phẩm thêm trong báo giá
                if ( data.item_add.length ) {
                    addProduct(data.item_add);
                }
                $('#total').attr('disabled', false);
                $('.reservation').datepicker({
                    autoclose: true,
                    format: 'dd-mm-yyyy'
                });
            },
            error: function (e) {
                console.log(e);
            }
        });
    }

    function loadTypeProduct(type_product) {
        var html = '<option value="" disabled>Chọn loại sản phẩm</option>';
        if (type_product == 'vps') {
            html += '<option value="vps" selected>VPS</option>';
        }
        else {
            html += '<option value="vps">VPS</option>';
        }
        if (type_product == 'vps_us') {
            html += '<option value="vps_us" selected>VPS US</option>';
        }
        else {
            html += '<option value="vps_us">VPS US</option>';
        }
        if (type_product == 'server') {
            html += '<option value="server" selected>Server vật lý</option>';
        }
        else {
            html += '<option value="server">Server vật lý</option>';
        }
        if (type_product == 'colocation') {
            html += '<option value="colocation" selected>Colocation</option>';
        }
        else {
            html += '<option value="colocation">Colocation</option>';
        }
      
        if (type_product == 'design_webiste') {
            html += '<option value="design_webiste" selected>Thiết kế webiste</option>';
        }
        else {
            html += '<option value="design_webiste">Thiết kế webiste</option>';
        }
        if (type_product == 'domain') {
            html += '<option value="domain" selected>Domain</option>';
        }
        else {
            html += '<option value="domain">Domain</option>';
        }
        if (type_product == 'hosting') {
            html += '<option value="hosting" selected>Hosting</option>';
        }
        else {
            html += '<option value="hosting">Hosting</option>';
        }
        return html;
    }

    function loadProduct(group_products, product_id) {
        let html = '';
        html += '<select class="form-control select2" name="product_id['+ dem +']" id="product" required style="width:100%;">';
        html += '<option value="" disabled selected>Chọn sản phẩm</option>';
        $.each(group_products.content , function (index, group_product) {
            html += '<optgroup label="' + group_product.name + '">';
            $.each(group_product.products , function ( index2, product ) {
                let selected = '';
                if (product_id == product.id) {
                    selected = 'selected';
                }
                html += '<option value="'+ product.id +'" '+ selected +' >'+ product.name +'</option>';
            })
            html += '</optgroup>';
        })
        html += '</select>';
        return html;
    }

    function loadProductServer(detail, product, group_products) { console.log(detail);
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group" id="config_server">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                   </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                   </label>`;
        }
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.price_billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}" checked> ${value}
                  </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
        }
      });
      html += '</div>';
      // addon ram
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram" for="addon_ram_server">Addon RAM</label>';
      html += '<select id="addon_ram_server" name="addon_ram['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_ram.length) {
        $.each(group_products.addon_ram , function (indexInArray, addon_ram) { 
          if (addon_ram.id == product.productAddonRam) {
            html += '<option value="'+ addon_ram.id +'" selected>'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 2
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk2" for="addon_disk2">Addon Disk 2</label>';
      html += '<select id="addon_disk2" name="addon_disk2['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk2) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 3
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk3" for="addon_disk3">Addon Disk 3</label>';
      html += '<select id="addon_disk3" name="addon_disk3['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk3) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 4
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk4" for="addon_disk4">Addon Disk 4</label>';
      html += '<select id="addon_disk4" name="addon_disk4['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk4) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 5
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk5" for="addon_disk5">Addon Disk 5</label>';
      html += '<select id="addon_disk5" name="addon_disk5['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk5) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 6
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk6" for="addon_disk6">Addon Disk 6</label>';
      html += '<select id="addon_disk6" name="addon_disk6['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk6) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 7
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk7" for="addon_disk7">Addon Disk 7</label>';
      html += '<select id="addon_disk7" name="addon_disk7['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk7) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 8
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk8" for="addon_disk8">Addon Disk 8</label>';
      html += '<select id="addon_disk8" name="addon_disk8['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk8) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip" name="addon_ip['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_ip.length) {
        $.each(group_products.addon_ip , function (indexInArray, addon_ip) { 
          if (addon_ip.id == product.productAddonIp) {
            html += '<option value="'+ addon_ip.id +'" selected>'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      // ẩn 
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="0" data-id="'+ dem +'" ';
      html += (detail.os == 0) ? 'checked' : '';
      html += '> Ẩn</label>';
      // win 10
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows 10 64bit" data-id="'+ dem +'" ';
      html += (detail.os == "Windows 10 64bit") ? 'checked' : '';
      html += '> Windows 10 64bit</label>';
      // win sv 2012
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2012" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2012") ? 'checked' : '';
      html += '> Windows Server 2012</label>';
      // win sv 2016
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2016" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2016") ? 'checked' : '';
      html += '> Windows Server 2016</label>';
      // Linux CentOS 7
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux CentOS 7" data-id="'+ dem +'" ';
      html += (detail.os == "Linux CentOS 7") ? 'checked' : '';
      html += '> Linux CentOS 7</label>';
      // Windows Server 2019
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2019" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2019") ? 'checked' : '';
      html += '> Windows Server 2019</label>';
      // Linux Ubuntu-20.04
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux Ubuntu-20.04" data-id="'+ dem +'" ';
      html += (detail.os == "Linux Ubuntu-20.04") ? 'checked' : '';
      html += '> Linux Ubuntu-20.04</label>';
      // VMware ESXi 6.7
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="VMware ESXi 6.7" data-id="'+ dem +'" ';
      html += (detail.os == "VMware ESXi 6.7") ? 'checked' : '';
      html += '> VMware ESXi 6.7</label>';
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="'+ detail.qtt +'" class="form-control" required disabled>';
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount">'+ addCommas(detail.amount) +' VNĐ</span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
      html += '</div>';
      // tu ngay
      var date = new Date(detail.start_date);
      var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
      var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
      var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      var date2 = new Date(detail.end_date);
      var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
      var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
      var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      
      return html;
    }

    function loadProductVps(detail) {
      
        let html = '<div class="info_product_vps">';
        html += '<div class="form-group">';
        html += '<label id="label_addon_cpu" for="addon_cpu">Addon CPU</label>';
        html += '<input type="number" id="addon_cpu" name="addon_cpu['+ dem +']" value="'+ detail.addon_cpu +'" class="form-control" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_ram" for="addon_ram">Addon RAM</label>';
        html += '<input type="number" id="addon_ram" name="addon_ram['+ dem +']" value="'+ detail.addon_ram +'" class="form-control" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk" for="addon_disk">Addon DISK</label>';
        html += '<input type="number" id="addon_disk" name="addon_disk['+ dem +']" value="'+ detail.addon_disk +'" class="form-control" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt_ip">Số lượng IP</label>';
        html += '<input type="number" id="qtt_ip" name="qtt_ip['+ dem +']" value="'+ detail.qtt_ip +'" class="form-control" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="os" class="mr-4">Hệ điều hành</label>';
        html += '<label class="radio-inline ml-4 mr-4">';
        if ( detail.os == 'Windows' ) {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows" data-id="'+ dem +'" checked> Windows';
        } else {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows" data-id="'+ dem +'"> Windows';
        }
        html += '</label>';
        html += '<label class="radio-inline ml-4 mr-4">';
        if ( detail.os == 'Linux' ) {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux" data-id="'+ dem +'" checked> Linux';
        } else {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux" data-id="'+ dem +'"> Linux';
        }
        html += '</label>';
        if ( detail.os != 'Linux' && detail.os != 'Windows' ) {
            html += '<label class="radio-inline ml-4 mr-4">';
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="other" data-id="'+ dem +'" checked> Khác';
            html += '</label>';
            html += '<div class="os'+ dem +'">';
            html += '<input type="text" name="os_replace['+ dem +']" required value="'+ detail.os +'" class="form-control"  placeholder="Hệ điều hành">';
            html += '</div>';
        } else {
            html += '<label class="radio-inline ml-4 mr-4">';
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="other" data-id="'+ dem +'"> Khác';
            html += '</label>';
            html += '<div class="os'+ dem +'">';
            html += '</div>';
        }
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="'+ detail.qtt +'" class="form-control" required disabled>';
        html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                     </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                     </label>`;
          }
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.price_billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}" checked> ${value}
                    </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
          }
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount">'+ addCommas(detail.amount) +' VNĐ</span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
        html += '<label for="checkboxAmount"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
        html += '</div>';
        // tu ngay
        var date = new Date(detail.start_date);
        var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
        var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        var date2 = new Date(detail.end_date);
        var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
        var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
        var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '</div>';
        return html;
    }
    function LoadProductIP(detail, product, group_products,datacenter){
      
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group" id="config_server">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                   </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                   </label>`;
        }
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.price_billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}" checked> ${value}
                  </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
        }
      });
      html += '</div>';
   
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip" name="addon_ip['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon.length) {
        $.each(group_products.addon , function (indexInArray, addon) { 
          if (addon.id == product.productAddonIp) {
            html += '<option value="'+ addon.id +'" selected>'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
   
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="'+ detail.qtt +'" class="form-control" required disabled>';
      html += '</div>';
             // datacenter
             html += '<div class="form-group">';
             html += '<label id="label_datacenter" for="datacenter">Datacenter</label>';
             html += '<select id="datacenter" name="datacenter['+ dem +']" class="form-control" >';
             html += '<option value="0" selected>None</option>'
                $.each(datacenter , function (indexInArray, datacenter) { 
                  if (datacenter.datacenter == detail.datacenter) {
                    html += '<option value="'+ datacenter.datacenter +'" selected>'+ datacenter.datacenter +' </option>';
                  } else {
                    html += '<option value="'+ datacenter.datacenter +'" >'+ datacenter.datacenter +' </option>';
                  }
                });
             html += '</select>';
             html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount">'+ addCommas(detail.amount) +' VNĐ</span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
      html += '</div>';
      // tu ngay
      var date = new Date(detail.start_date);
      var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
      var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
      var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      var date2 = new Date(detail.end_date);
      var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
      var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
      var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      
      return html;
    }

    function loadProductDomain(detail) {
        let html = '<div class="info_product_domain">';
        html += '<div class="form-group">';
        html += '<label for="domain">Domain</label>';
        html += '<input type="text" id="domain" name="domain['+ dem +']" value="'+ detail.domain +'" class="form-control" required disabled>';
        html += '</div>';
        // html += '<div class="form-group">';
        // html += '<label for="qtt">Số lượng</label>';
        html += '<input type="hidden" id="qtt" name="qtt['+ dem +']" value="'+ detail.qtt +'" class="form-control" required disabled>';
        // html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                     </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                     </label>`;
          }
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.price_billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}" checked> ${value}
                    </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
          }
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount">'+ addCommas(detail.amount) +' VNĐ</span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
        html += '<label for="checkboxAmount"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
        html += '</div>';
        // tu ngay
        var date = new Date(detail.start_date);
        var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
        var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        var date2 = new Date(detail.end_date);
        var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
        var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
        var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '</div>';
        return html;
    }

    function addProduct(item_add) {
        $.each(item_add, function (index, item) {
            dem++;
            loadAddProduct();
            let html_item_group_product = loadTypeProduct(item.detail.type_product);
            $('#type_product'+ dem).html(html_item_group_product);  
            if (item.detail.type_product == 'vps' || item.detail.type_product == 'vps_us' 
              || item.detail.type_product == 'hosting' || item.detail.type_product == 'server' 
              || item.detail.type_product == 'colocation' ) {
                let html_item_product = loadAddDetailProduct(item.group_products, item.detail.product_id);
                $('.product'+dem).html(html_item_product);
                $('#product'+ dem).select2();
            }
            let type_product = item.detail.type_product;
            let html_item_detail_product = '';
            if ( type_product == 'vps' || type_product == 'vps_us') {
                html_item_detail_product = loadAddProductVps(item.detail);
                $('.info_quotation'+dem).html(html_item_detail_product);
                $('#qtt_ip'+dem).attr('disabled', false);
                $('#addon_cpu'+dem).attr('disabled', false);
                $('#addon_ram'+dem).attr('disabled', false);
                $('#addon_disk'+dem).attr('disabled', false);
                $('#qtt'+dem).attr('disabled', false);
                $('#os'+dem).attr('disabled', false);
                $('#amount'+dem).attr('disabled', false);
                $.each(item.group_products.addon , function (index, product_addon) {
                  if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                    $('#label_addon_cpu'+ dem).text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_cpu'+ dem).attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                    $('#label_addon_ram'+ dem).text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_ram'+ dem).attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                    $('#label_addon_disk'+ dem).text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_disk'+ dem).attr('data-id', product_addon.id);
                  }
                })
            }  else if (type_product == 'colocation'){   
              html_item_detail_product = loadAddProductIP(item.detail, item.product, item.group_products,item.datacenter);
              $('.info_quotation'+dem).html(html_item_detail_product);
              $('#qtt_ip'+dem).attr('disabled', false);
              $('#qtt'+dem).attr('disabled', false);
              $('#amount'+dem).attr('disabled', false);
              $('#addon_ip'+dem).attr('disabled', false);
            }
            else if ( type_product == 'design_webiste' || type_product == 'domain' || type_product == 'hosting' 
            ) {
                html_item_detail_product = loadAddProductDomain(item.detail);
                $('.info_quotation'+dem).html(html_item_detail_product);
                $('#qtt'+dem).attr('disabled', false);
                $('#domain'+dem).attr('disabled', false);
                $('#amount'+dem).attr('disabled', false);
            }
            else if ( type_product == 'server' ) {
              html_item_detail_product = loadAddProductServer(item.detail, item.product, item.group_products);
              $('.info_quotation'+dem).html(html_item_detail_product);
              $('#qtt_ip'+dem).attr('disabled', false);
              $('#addon_cpu'+dem).attr('disabled', false);
              $('#addon_ram_server'+dem).attr('disabled', false);
              $('#addon_disk'+dem).attr('disabled', false);
              $('#qtt'+dem).attr('disabled', false);
              $('#os'+dem).attr('disabled', false);
              $('#amount'+dem).attr('disabled', false);
              if ( item.product.second ) {
                $('#addon_disk2'+dem).attr('disabled', true);
              } else {
                $('#addon_disk2'+dem).attr('disabled', false);
              }
              $('#addon_disk3'+dem).attr('disabled', false);
              $('#addon_disk4'+dem).attr('disabled', false);
              $('#addon_disk5'+dem).attr('disabled', false);
              $('#addon_disk6'+dem).attr('disabled', false);
              $('#addon_disk7'+dem).attr('disabled', false);
              $('#addon_disk8'+dem).attr('disabled', false);
              $('#addon_ip'+dem).attr('disabled', false);
              var html = '';
              html += '<label id="" for="">Cấu hình sản phẩm</label>';
              html += '<div>';
              html += item.product.config;
              html += '</div>';
              $('#config_server'+dem).html(html);
            }
        });
    }

    function loadAddDetailProduct(group_products, product_id) {
      // console.log(group_products, product_id);
      let html = '';
      html += '<select class="form-control select2" name="product_id['+ dem +']" id="product'+ dem +'"  required style="width:100%;">';
      html += '<option value="" disabled selected>Chọn sản phẩm</option>';
      $.each(group_products.content , function (index, group_product) {
          html += '<optgroup label="' + group_product.name + '">';
          $.each(group_product.products , function ( index2, product ) {
              let selected = '';
              if (product_id == product.id) {
                  selected = 'selected';
              }
              html += '<option value="'+ product.id +'" '+ selected +' >'+ product.name +'</option>';
          })
          html += '</optgroup>';
      })
      html += '</select>';
      return html;
    }

    function loadAddProduct() {
        let html = '<div class="choose-product mt-4" id="add_product_'+ dem +'">';
        html += '<div class="card-tools">';
        html += '<button type="button" class="btn btn-tool" data-id="'+ dem +'" data-toggle="tooltip" data-placement="top" title="Xóa sản phẩm"><i class="fas fa-times"></i></button>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="type_product">Loại sản phẩm</label>';
        html += '<select name="type_product['+ dem +']" id="type_product'+ dem +'" data-id="'+ dem +'" class="form-control choose_type_product">';
        html += '</select>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="product">Sản phẩm</label>';
        html += '<div class="product'+ dem +'">';
        html += '<input type="text" class="form-control" name="" value="" disabled>';
        html += '</div>';
        html += '</div>';
        html += '<div class="info_quotation'+ dem +'">';
        html += '</div>';
        html += '</div>';
        $("#add_product").append(html);
        $('[data-toggle="tooltip"]').tooltip();
    }

    function loadAddProductServer(detail, product, group_products) {
      // console.log(detail);
      let html = '<div class="info_product_vps'+dem+'">';
      html += '<div class="form-group" id="config_server'+ dem +'">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                   </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                     <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                   </label>`;
        }
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
        if ( index == detail.price_billing_cycle ) {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}" checked> ${value}
                  </label>`;
        } else {
          html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
        }
      });
      html += '</div>';
      // addon ram
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram'+ dem +'" for="addon_ram_server">Addon RAM</label>';
      html += '<select id="addon_ram_server'+ dem +'" name="addon_ram['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ram_server_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_ram.length) {
        $.each(group_products.addon_ram , function (indexInArray, addon_ram) { 
          if (addon_ram.id == product.productAddonRam) {
            html += '<option value="'+ addon_ram.id +'" selected>'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 2
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk2'+ dem +'" for="addon_disk2">Addon Disk 2</label>';
      html += '<select id="addon_disk2'+ dem +'" name="addon_disk2['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk2_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk2) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 3
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk3'+ dem +'" for="addon_disk3">Addon Disk 3</label>';
      html += '<select id="addon_disk3'+ dem +'" name="addon_disk3['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk3_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk3) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 4
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk4'+ dem +'" for="addon_disk4">Addon Disk 4</label>';
      html += '<select id="addon_disk4'+ dem +'" name="addon_disk4['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk4_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk4) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 5
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk5'+ dem +'" for="addon_disk5">Addon Disk 5</label>';
      html += '<select id="addon_disk5'+ dem +'" name="addon_disk5['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk5_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk5) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 6
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk6'+ dem +'" for="addon_disk6">Addon Disk 6</label>';
      html += '<select id="addon_disk6'+ dem +'" name="addon_disk6['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk6_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk6) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 7
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk7'+ dem +'" for="addon_disk7">Addon Disk 7</label>';
      html += '<select id="addon_disk7'+ dem +'" name="addon_disk7['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk7_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk7) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon disk 8
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk8'+ dem +'" for="addon_disk8">Addon Disk 8</label>';
      html += '<select id="addon_disk8'+ dem +'" name="addon_disk8['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk8_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_disk.length) {
        $.each(group_products.addon_disk , function (indexInArray, addon_disk) { 
          if (addon_disk.id == product.productDisk8) {
            html += '<option value="'+ addon_disk.id +'" selected>'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
      html += '<option value="0" selected>None</option>';
      if (group_products.addon_ip.length) {
        $.each(group_products.addon_ip , function (indexInArray, addon_ip) { 
          if (addon_ip.id == product.productAddonIp) {
            html += '<option value="'+ addon_ip.id +'" selected>'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
          } else {
            html += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
          }
        });
      }
      html += '</select>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      // ẩn 
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="0" data-id="'+ dem +'" ';
      html += (detail.os == 0) ? 'checked' : '';
      html += '> Ẩn</label>';
      // win 10
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows 10 64bit" data-id="'+ dem +'" ';
      html += (detail.os == "Windows 10 64bit") ? 'checked' : '';
      html += '> Windows 10 64bit</label>';
      // win sv 2012
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2012" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2012") ? 'checked' : '';
      html += '> Windows Server 2012</label>';
      // win sv 2016
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2016" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2016") ? 'checked' : '';
      html += '> Windows Server 2016</label>';
      // Linux CentOS 7
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux CentOS 7" data-id="'+ dem +'" ';
      html += (detail.os == "Linux CentOS 7") ? 'checked' : '';
      html += '> Linux CentOS 7</label>';
      // Windows Server 2019
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows Server 2019" data-id="'+ dem +'" ';
      html += (detail.os == "Windows Server 2019") ? 'checked' : '';
      html += '> Windows Server 2019</label>';
      // Linux Ubuntu-20.04
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux Ubuntu-20.04" data-id="'+ dem +'" ';
      html += (detail.os == "Linux Ubuntu-20.04") ? 'checked' : '';
      html += '> Linux Ubuntu-20.04</label>';
      // VMware ESXi 6.7
      html += '<label class="radio-inline ml-4 mr-4">';
      html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="VMware ESXi 6.7" data-id="'+ dem +'" ';
      html += (detail.os == "VMware ESXi 6.7") ? 'checked' : '';
      html += '> VMware ESXi 6.7</label>';
      html += '</div>';
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt'+dem+'" name="qtt['+ dem +']" data-id="'+ dem +'" value="'+ detail.qtt +'" class="form-control qtt_add" required disabled>';
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'">'+ addCommas(detail.amount) +' VNĐ</span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
      html += '<label for="checkboxAmount'+ dem +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
      html += '</div>';
      // tu ngay
      var date = new Date(detail.start_date);
      var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
      var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
      var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      var date2 = new Date(detail.end_date);
      var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
      var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
      var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      return html;
    }

    function loadAddProductVps(detail) {
        let html = '<div class="info_product_vps'+dem+'">';
        html += '<div class="form-group">';
        html += '<label id="label_addon_cpu'+ dem +'" for="addon_cpu'+ dem +'">Addon CPU</label>';
        html += '<input type="number" id="addon_cpu'+ dem +'" name="addon_cpu['+ dem +']" value="'+ detail.addon_cpu +'" data-id-product="'+ dem +'" class="form-control addon_add" required>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_ram'+ dem +'" for="addon_ram'+ dem +'">Addon RAM</label>';
        html += '<input type="number" id="addon_ram'+ dem +'" name="addon_ram['+ dem +']" value="'+ detail.addon_ram +'" data-id-product="'+ dem +'" class="form-control addon_add" required>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk'+dem+'" for="addon_disk'+dem+'">Addon DISK</label>';
        html += '<input type="number" id="addon_disk'+dem+'" name="addon_disk['+ dem +']" value="'+ detail.addon_disk +'" data-id-product="'+ dem +'" class="form-control addon_add" required>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt_ip">Số lượng IP</label>';
        html += '<input type="number" id="qtt_ip'+dem+'" name="qtt_ip['+ dem +']" value="'+ detail.qtt_ip +'" class="form-control addon_add" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="os" class="mr-4">Hệ điều hành</label>';
        html += '<label class="radio-inline ml-4 mr-4">';
        if ( detail.os == 'Windows' ) {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows" data-id="'+ dem +'" checked> Windows';
        } else {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Windows" data-id="'+ dem +'"> Windows';
        }
        html += '</label>';
        html += '<label class="radio-inline ml-4 mr-4">';
        if ( detail.os == 'Linux' ) {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux" data-id="'+ dem +'" checked> Linux';
        } else {
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="Linux" data-id="'+ dem +'"> Linux';
        }
        html += '</label>';
        if ( detail.os != 'Linux' && detail.os != 'Windows' ) {
            html += '<label class="radio-inline ml-4 mr-4">';
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="other" data-id="'+ dem +'" checked> Khác';
            html += '</label>';
            html += '<div class="os'+ dem +'">';
            html += '<input type="text" name="os_replace['+ dem +']" required value="'+ detail.os +'" class="form-control"  placeholder="Hệ điều hành">';
            html += '</div>';
        } else {
            html += '<label class="radio-inline ml-4 mr-4">';
            html += '<input type="radio" name="os['+ dem +']" class="chooseOs" value="other" data-id="'+ dem +'"> Khác';
            html += '</label>';
            html += '<div class="os'+ dem +'">';
            html += '</div>';
        }
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt'+dem+'" name="qtt['+ dem +']" data-id="'+ dem +'" value="'+ detail.qtt +'" class="form-control qtt_add" required disabled>';
        html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                     </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                     </label>`;
          }
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.price_billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}" checked> ${value}
                    </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
          }
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'">'+ addCommas(detail.amount) +' VNĐ</span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
        html += '<label for="checkboxAmount'+ dem +'"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
        html += '</div>';
        // tu ngay
        var date = new Date(detail.start_date);
        var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
        var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        var date2 = new Date(detail.end_date);
        var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
        var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
        var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '</div>';
        return html;
    }
    function loadAddProductIP(detail, product, group_products,datacenter){
    
       let html = '<div class="info_product_vps'+dem+'">';
       html += '<div class="form-group" id="config_server'+ dem +'">';
       html += '<label id="" for="">Cấu hình</label>';
       html += '</div>';
       // billing_cycle
       html += '<div class="form-group">';
       html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
       $.each(billing_cycle, function (index, value) {
         if ( index == detail.billing_cycle ) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                    </label>`;
         } else {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
         }
       });
       html += '</div>';
       // price / billing_cycle
       html += '<div class="form-group">';
       html += '<label class="mr-4">Chi phí theo</label>';
       $.each(billing_cycle, function (index, value) {
         if ( index == detail.price_billing_cycle ) {
           html += `<label class="radio-inline mr-4">
                     <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}" checked> ${value}
                   </label>`;
         } else {
           html += `<label class="radio-inline mr-4">
                     <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}"> ${value}
                   </label>`;
         }
       });
       html += '</div>';
       // addon ip
       html += '<div class="form-group">';
       html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
       html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
       html += '<option value="0" selected>None</option>';
       if (group_products.addon.length) {
         $.each(group_products.addon , function (indexInArray, addon) { 
           if (addon.id == product.productAddonIp) {
             html += '<option value="'+ addon.id +'" selected>'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
           } else {
             html += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
           }
         });
       }
       html += '</select>';
       html += '</div>'; 
       // qtt
       html += '<div class="form-group">';
       html += '<label for="qtt">Số lượng</label>';
       html += '<input type="number" id="qtt'+dem+'" name="qtt['+ dem +']" data-id="'+ dem +'" value="'+ detail.qtt +'" class="form-control qtt_add" required disabled>';
       html += '</div>';
              // datacenter
           // datacenter
           html += '<div class="form-group">';
           html += '<label id="label_datacenter" for="datacenter">Datacenter</label>';
           html += '<select id="datacenter'+ dem +'" name="datacenter['+ dem +']" class="form-control" >';
           html += '<option value="0" selected>None</option>'
              $.each(datacenter , function (indexInArray, datacenter) { 
                if (datacenter.datacenter == detail.datacenter) {
                  html += '<option value="'+ datacenter.datacenter +'" selected>'+ datacenter.datacenter +' </option>';
                } else {
                  html += '<option value="'+ datacenter.datacenter +'" >'+ datacenter.datacenter +' </option>';
                }
              });
             
          
           html += '</select>';
           html += '</div>';
       // price
       html += '<div class="form-group">';
       html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'">'+ addCommas(detail.amount) +' VNĐ</span></b>';
       html += '</div>';
       html += '<div class="form-group row">';
       html += '<div class="col-md-1">';
       html += '<label for="amount">Giá áp tay</label>';
       html += '</div>';
       html += '<div class="col-md-10 icheck-primary d-inline">';
       html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
       html += '<label for="checkboxAmount'+ dem +'"></label>';
       html += '</div>';
       html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
       html += '</div>';
       // tu ngay
       var date = new Date(detail.start_date);
       var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
       var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
       var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
       html += '<div class="form-group">';
       html += '<label for="start_date">Từ ngày</label>';
       html += `<div class="input-group">
                   <div class="input-group-prepend">
                     <span class="input-group-text">
                       <i class="far fa-calendar-alt"></i>
                     </span>
                   </div>
                   <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
               </div>`;
       html += '</div>';
       var date2 = new Date(detail.end_date);
       var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
       var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
       var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
       html += '<div class="form-group">';
       html += '<label for="end_date">Đến ngày</label>';
       html += `<div class="input-group">
                   <div class="input-group-prepend">
                     <span class="input-group-text">
                       <i class="far fa-calendar-alt"></i>
                     </span>
                   </div>
                   <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
               </div>`;
       html += '</div>';
       html += '</div>';
       return html;
    }


    function loadAddProductDomain(detail) {
        let html = '<div class="info_product_domain'+dem+'">';
        html += '<div class="form-group">';
        html += '<label for="domain">Domain</label>';
        html += '<input type="text" id="domain'+dem+'" name="domain['+ dem +']" value="'+ detail.domain +'" class="form-control" required disabled>';
        html += '</div>';
        // html += '<div class="form-group">';
        // html += '<label for="qtt">Số lượng</label>';
        html += '<input type="hidden" id="qtt'+dem+'" name="qtt['+ dem +']" value="'+ detail.qtt +'" class="form-control" required disabled>';
        html += '</div>';
        // billing_cycle
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}" checked> ${value}
                     </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                       <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                     </label>`;
          }
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          if ( index == detail.price_billing_cycle ) {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}" checked> ${value}
                    </label>`;
          } else {
            html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_add billing_cycle_add${dem}' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
          }
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'">'+ addCommas(detail.amount) +' VNĐ</span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
        html += '<label for="checkboxAmount'+ dem +'"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="'+ detail.amount +'" class="form-control">';
        html += '</div>';
        // tu ngay
        var date = new Date(detail.start_date);
        var day_start_date = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
        var month_start_date =  ( parseInt(date.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date.getMonth()) + 1 ) : ( parseInt(date.getMonth()) + 1 );
        var start_date = day_start_date + '-' + month_start_date + '-' + date.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="start_date[${dem}]" value="${start_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        var date2 = new Date(detail.end_date);
        var day_end_date = date2.getDate() < 10 ? '0' + date2.getDate() : date2.getDate();
        var month_end_date =  ( parseInt(date2.getMonth()) + 1 ) < 10 ? '0' + ( parseInt(date2.getMonth()) + 1 ) : ( parseInt(date2.getMonth()) + 1 );
        var end_date = day_end_date + '-' + month_end_date + '-' + date2.getFullYear();
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text">
                        <i class="far fa-calendar-alt"></i>
                      </span>
                    </div>
                    <input type="text" name="end_date[${dem}]" value="${end_date}" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '</div>';
        return html;
    }

    $(document).on('click', ".btn-tool", function () {
        let id = $(this).attr('data-id');
        $('#add_product_'+ id +'').remove();
    })

    // thay đổi loại sản phẩm của sản phẩm chính
    $('#user').on('change', function () {
        let user_id = $(this).val();
        let type_product = $('#type_product').val();
        if ( type_product != '' ) {
          changeLoadProduct(user_id, type_product);
        }
    })

    $('#type_product').on('change', function () {
        let user_id = $('#user').val();
        let type_product = $(this).val();
        if ( user_id != '' ) {
            changeLoadProduct(user_id, type_product);
        }
    })

    function changeLoadProduct(user_id, type_product) {
      if (type_product == 'vps' || type_product == 'vps_us' || type_product == 'hosting' 
     ) {
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
              var html = '';
              if (data.error == 0) {
                if (data.content != '') {
                  html += '<select class="form-control select2" name="product_id['+ dem +']" id="product" required style="width:100%;">';
                  html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                  $.each(data.content , function (index, group_product) {
                    html += '<optgroup label="' + group_product.name + '">';
                    $.each(group_product.products , function ( index2, product ) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    })
                    html += '</optgroup>';
                  })
                  html += '</select>';
                }
                $('.product').html(html);
                $('#product').select2();

                if ( type_product == 'vps' || type_product == 'vps_us' || type_product == 'server'  ||
                type_product == 'colocation'
                ) {
                  changeLoadProductVps(0);
                }
                else if ( type_product == 'design_webiste' || type_product == 'domain' || type_product == 'hosting'
                ) {
                  changeLoadProductDomain(0);
                }
                $('#total').attr('disabled', false);

                $.each(data.addon , function (index, product_addon) {
                  if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                    $('#label_addon_cpu').text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_cpu').attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                    $('#label_addon_ram').text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_ram').attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                    $('#label_addon_disk').text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_disk').attr('data-id', product_addon.id);
                  }
                })
              }
          },
          error: function (e) {
            console.log(e);
            $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      if ( type_product == 'server' || type_product == 'colocation' ) {
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
              // console.log(data);
              var html = '';
              if (data.error == 0) {
                if (data.content != '') {
                  html += '<select class="form-control select2" name="product_id['+ dem +']" id="product" required style="width:100%;">';
                  html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                  $.each(data.content , function (index, group_product) {
                    html += '<optgroup label="' + group_product.name + '">';
                    $.each(group_product.products , function ( index2, product ) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    })
                    html += '</optgroup>';
                  })
                  $('#total').attr('disabled', false);
                  if ( type_product == 'server' ) {
                    changeLoadProductServer(0);
                    if ( data.addon_disk.length > 0 ) {
                      var html_addon_disk = '';
                      html_addon_disk += '<option value="0" selected>None</option>';
                      $.each(data.addon_disk , function (index, addon_disk) {
                        html_addon_disk += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_disk2').html(html_addon_disk);
                      $('#addon_disk3').html(html_addon_disk);
                      $('#addon_disk4').html(html_addon_disk);
                      $('#addon_disk5').html(html_addon_disk);
                      $('#addon_disk6').html(html_addon_disk);
                      $('#addon_disk7').html(html_addon_disk);
                      $('#addon_disk8').html(html_addon_disk);
                    }
                    if ( data.addon_ram.length ) {
                      var html_addon_ram = '';
                      html_addon_ram += '<option value="0" selected>None</option>';
                      $.each(data.addon_ram , function (index, addon_ram) {
                        html_addon_ram += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ram_server').html(html_addon_ram);
                    }
                    if ( data.addon_ip.length ) {
                      var html_addon_ip = '';
                      html_addon_ip += '<option value="0" selected>None</option>';
                      $.each(data.addon_ip , function (index, addon_ip) {
                        html_addon_ip += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ip').html(html_addon_ip);
                    }
                  }else if(type_product == 'colocation'){
                    loadProductIP(0) 
                    if ( data.addon.length > 0 ) {
                      var html_addon= '';
                      html_addon += '<option value="0" selected>None</option>';
                      $.each(data.addon , function (index, addon) {
                        html_addon += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
                      })   
                      $('#addon_ip').html(html_addon);     
                    }           
                  }
                }
                $('.product').html(html);
                $('#product').select2();
              }
          },
          error: function (e) {
            console.log(e);
            $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      else if ( type_product == 'design_webiste' || type_product == 'domain' ) {
        changeLoadProductDomain(0);
        if ( type_product == 'design_webiste' || type_product == 'domain' ) {
          $('#qtt').attr('disabled', false);
          $('#domain').attr('disabled', false);
          $('#amount').attr('disabled', false);
        }
      }
      $('#total').attr('disabled', false);
      $('#total').val(0);
    }

    function changeLoadProductVps(dem) {
        let html = '<div class="info_product_vps">';
        // add on
        html += '<div class="form-group">';
        html += '<label id="label_addon_cpu" for="addon_cpu">Addon CPU</label>';
        html += '<input type="number" id="addon_cpu" name="addon_cpu['+ dem +']" value="0" class="form-control" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_ram" for="addon_ram">Addon RAM</label>';
        html += '<input type="number" id="addon_ram" name="addon_ram['+ dem +']" value="0" class="form-control" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk" for="addon_disk">Addon DISK</label>';
        html += '<input type="number" id="addon_disk" name="addon_disk['+ dem +']" value="0" class="form-control" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt_ip">Số lượng IP</label>';
        html += '<input type="number" id="qtt_ip" name="qtt_ip['+ dem +']" value="1" class="form-control" required disabled>';
        html += '</div>';
        // os
        html += '<div class="form-group">';
        html += '<label for="os" class="mr-4">Hệ điều hành</label>';
        html += `<label class="radio-inline ml-4 mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows" data-id="${dem}" checked> Windows
                  </label>
                  <label class="radio-inline mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Linux" data-id="${dem}"> Linux
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="os[${dem}]" value="other" class='chooseOs' data-id="${dem}"> Khác
                  </label>
                <div class="os${dem}">
                </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
        html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
        html += '<label for="checkboxAmount"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
        html += '</div>';
        // tu ngay
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '</div>';
        $('.info_quotation').html(html);
        $('.reservation').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }

    function changeLoadProductDomain(dem) {
        let html = '<div class="info_product_domain">';
        html += '<div class="form-group">';
        html += '<label for="domain">Domain</label>';
        html += '<input type="text" id="domain" name="domain['+ dem +']" value="" class="form-control" required disabled>';
        html += '</div>';
        // html += '<div class="form-group">';
        // html += '<label for="qtt">Số lượng</label>';
        html += '<input type="hidden" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
        // html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label for="amount">Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
        html += '<label for="checkboxAmount"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
        html += '</div>';
        // tu ngay
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '</div>';
        $('.info_quotation').html(html);
        $('.reservation').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }

    function changeLoadProductServer(dem) {
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group" id="config_server">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // addon ram
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram" for="addon_ram_server">Addon RAM</label>';
      html += '<select id="addon_ram_server" name="addon_ram['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 2
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk2" for="addon_disk2">Addon Disk 2</label>';
      html += '<select id="addon_disk2" name="addon_disk2['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 3
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk3" for="addon_disk3">Addon Disk 3</label>';
      html += '<select id="addon_disk3" name="addon_disk3['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 4
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk4" for="addon_disk4">Addon Disk 4</label>';
      html += '<select id="addon_disk4" name="addon_disk4['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 5
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk5" for="addon_disk5">Addon Disk 5</label>';
      html += '<select id="addon_disk5" name="addon_disk5['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 6
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk6" for="addon_disk6">Addon Disk 6</label>';
      html += '<select id="addon_disk6" name="addon_disk6['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 7
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk7" for="addon_disk7">Addon Disk 7</label>';
      html += '<select id="addon_disk7" name="addon_disk7['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 8
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk8" for="addon_disk8">Addon Disk 8</label>';
      html += '<select id="addon_disk8" name="addon_disk8['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip" name="addon_ip['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      html += `
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="0" data-id="${dem}" checked> Ẩn
        </label>
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows 10 64bit" data-id="${dem}"> Windows 10 64bit
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows Server 2012" data-id="${dem}"> Windows Server 2012
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2016" class='chooseOs' data-id="${dem}"> Windows Server 2016
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux CentOS 7" class='chooseOs' data-id="${dem}"> Linux CentOS 7
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2019" class='chooseOs' data-id="${dem}"> Windows Server 2019
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux Ubuntu-20.04" class='chooseOs' data-id="${dem}"> Linux Ubuntu-20.04
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="VMware ESXi 6.7" class='chooseOs' data-id="${dem}"> VMware ESXi 6.7
        </label>
        <div class="os${dem}">
        </div>`;
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    $(document).on("change", ".choose_type_product", function () {
        let user_id = $('#user').val();
        let type_product = $(this).val();
        let id = $(this).attr('data-id');
        if ( user_id != '' ) {
          changeLoadAddProduct(user_id, type_product, id);
        }
    });

    function changeLoadAddProduct(user_id, type_product, id) {
        if (type_product == 'vps' || type_product == 'vps_us' || type_product == 'hosting'  
       ) {
          $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_group_product",
            data: {id: user_id, type_product: type_product},
            dataType: "json",
            success: function (data) {
                var html = '';
                if (data.error == 0) {
                  if (data.content != '') {
                    html += '<select class="form-control select2 choose_add_product" name="product_id['+ id +']" id="product'+ id +'" data-id="'+ id +'" required style="width:100%;">';
                    html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                    $.each(data.content , function (index, group_product) {
                      html += '<optgroup label="' + group_product.name + '">';
                      $.each(group_product.products , function ( index2, product ) {
                        html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                      })
                      html += '</optgroup>';
                    })
                    html += '</select>';
                  }
                  $('.product'+id).html(html);
                  $('#product'+id).select2();
                  if ( type_product == 'vps' || type_product == 'vps_us'  ) {
                    changeLoadAddProductVps(id);
                  }
                  else if ( type_product == 'hosting'  ) {
                    changeLoadAddProductDomain(id);
                  }
  
                  $.each(data.addon , function (index, product_addon) {
                    if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                      $('#label_addon_cpu'+ id).text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_cpu'+ id).attr('data-id', product_addon.id);
                    }
                    if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                      $('#label_addon_ram'+ id).text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_ram'+ id).attr('data-id', product_addon.id);
                    }
                    if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                      $('#label_addon_disk'+ id).text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_disk'+ id).attr('data-id', product_addon.id);
                    }
                  })
                }
            },
            error: function (e) {
              console.log(e);
              $('.product'+id).html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
          });
        }
        else if ( type_product == 'server' || type_product == 'colocation' ) {
          $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_group_product",
            data: {id: user_id, type_product: type_product},
            dataType: "json",
            success: function (data) {
              console.log(data);
                var html = '';
                if (data.error == 0) {
                  if (data.content != '') {
                    html += '<select class="form-control select2 choose_add_product" name="product_id['+ id +']" id="product'+ id +'" data-id="'+ id +'" required style="width:100%;">';
                    html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                    $.each(data.content , function (index, group_product) {
                      html += '<optgroup label="' + group_product.name + '">';
                      $.each(group_product.products , function ( index2, product ) {
                        html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                      })
                      html += '</optgroup>';
                    })
                    $('.product'+id).html(html);
                    $('#product'+id).select2();
                    if ( type_product == 'server' ) {
                      changeLoadAddProductServer(id);
                      if ( data.addon_disk.length > 0 ) {
                        var html_addon_disk = '';
                        html_addon_disk += '<option value="0" selected>None</option>';
                        $.each(data.addon_disk , function (index, addon_disk) {
                          html_addon_disk += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
                        })
                        $('#addon_disk2'+ id).html(html_addon_disk);
                        $('#addon_disk3'+ id).html(html_addon_disk);
                        $('#addon_disk4'+ id).html(html_addon_disk);
                        $('#addon_disk5'+ id).html(html_addon_disk);
                        $('#addon_disk6'+ id).html(html_addon_disk);
                        $('#addon_disk7'+ id).html(html_addon_disk);
                        $('#addon_disk8'+ id).html(html_addon_disk);
                      }
                      if ( data.addon_ram.length ) {
                        var html_addon_ram = '';
                        html_addon_ram += '<option value="0" selected>None</option>';
                        $.each(data.addon_ram , function (index, addon_ram) {
                          html_addon_ram += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
                        })
                        $('#addon_ram_server'+ id).html(html_addon_ram);
                      }
                      if ( data.addon_ip.length ) {
                        var html_addon_ip = '';
                        html_addon_ip += '<option value="0" selected>None</option>';
                        $.each(data.addon_ip , function (index, addon_ip) {
                          html_addon_ip += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
                        })
                        $('#addon_ip'+ id).html(html_addon_ip);
                      }
                    }else if(type_product == 'colocation'){
                      changeLoadAddProductColocation(id); 
                      if ( data.addon.length > 0 ) {  
                        var html_addon= '';
                        html_addon += '<option value="0" selected>None</option>';
                        $.each(data.addon , function (index, addon) {
                          html_addon += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
                        })   
                        $('#addon_ip' + id).html(html_addon);     
                      }        
                    }
                  }
                }
            },
            error: function (e) {
              console.log(e);
              $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
          });
        }
        else if ( type_product == 'design_webiste' || type_product == 'domain' ) {
            changeLoadAddProductDomain(id);
          if ( type_product == 'design_webiste' || type_product == 'domain' ) {
            $('#qtt'+ id +'').attr('disabled', false);
            $('#domain'+ id +'').attr('disabled', false);
            $('#amount'+ id +'').attr('disabled', false);
          }
        }
    }

    function changeLoadAddProductVps(id) {
        let html = '<div class="info_product_vps'+id+'">';
        html += '<div class="form-group">';
        html += '<label id="label_addon_cpu'+ id +'" for="addon_cpu'+ id +'">Addon CPU</label>';
        html += '<input type="number" id="addon_cpu'+ id +'" name="addon_cpu['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_ram'+ id +'" for="addon_ram'+ id +'">Addon RAM</label>';
        html += '<input type="number" id="addon_ram'+ id +'" name="addon_ram['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk'+ id +'" for="addon_disk'+ id +'">Addon DISK</label>';
        html += '<input type="number" id="addon_disk'+ id +'" name="addon_disk['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required >';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt_ip'+ id +'">Số lượng IP</label>';
        html += '<input type="number" id="qtt_ip'+ id +'" name="qtt_ip['+ id +']" value="1" class="form-control" required disabled>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="os" class="mr-4">Hệ điều hành</label>';
        html += `<label class="radio-inline ml-4 mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows" data-id="${dem}" checked> Windows
                  </label>
                  <label class="radio-inline mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Linux" data-id="${dem}"> Linux
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="os[${dem}]" value="other" class='chooseOs' data-id="${dem}"> Khác
                  </label>
                <div class="os${dem}">
                </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt'+ id +'" name="qtt['+ id +']" value="1" class="form-control" required disabled>';
        html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${id}]" class='billing_cycle' value="${index}" data-id="${id}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${id}]" class="billing_cycle_add billing_cycle_add${id}" value="${index}" data-id="${id}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ id +'"></span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label>Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ id +'" id="checkboxAmount'+ id +'" value="1">';
        html += '<label for="checkboxAmount'+ id +'"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount'+ id +'" name="amount['+ id +']" value="0" class="form-control">';
        html += '</div>';
        // tu ngay
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${id}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${id}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '</div>';
        $('.info_quotation'+ id +'').html(html);
        $('.reservation').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
      }
   

    function changeLoadAddProductDomain(id) {
        let html = '<div class="info_product_domain">';
        html += '<div class="form-group">';
        html += '<label for="domain">Domain</label>';
        html += '<input type="text" id="domain'+ id +'" name="domain['+ id +']" value="" class="form-control" required disabled>';
        html += '</div>';
        // html += '<div class="form-group">';
        // html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt'+ id +'" name="qtt['+ id +']" value="1" class="form-control" required disabled>';
        // html += '</div>';
        // billing_cycle
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
          html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${id}]" class='billing_cycle' value="${index}" data-id="${id}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
          html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${id}]" class='billing_cycle_add billing_cycle_add${id}' value="${index}" data-id="${id}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ id +'"></span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label>Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ id +'" id="checkboxAmount'+ id +'" value="1">';
        html += '<label for="checkboxAmount'+ id +'"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount'+ id +'" name="amount['+ id +']" value="0" class="form-control">';
        html += '</div>';
        // tu ngay
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${id}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${id}]" class="form-control float-right reservation">
              </div>`;
        html += '</div>';
        html += '</div>';
        $('.info_quotation'+ id +'').html(html);
        $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
        });
      }

    function changeLoadAddProductServer(dem) {
        let html = '<div class="info_product_vps'+dem+'">';
        html += '<div class="form-group" id="config_server'+ dem +'">';
        html += '<label id="" for="">Cấu hình</label>';
        html += '</div>';
        // billing_cycle
        html += '<div class="form-group">';
        html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // price / billing_cycle
        html += '<div class="form-group">';
        html += '<label class="mr-4">Chi phí theo</label>';
        $.each(billing_cycle, function (index, value) {
           html += `<label class="radio-inline mr-4">
                      <input type="radio" name="price_billing_cycle[${dem}]" class="billing_cycle_add billing_cycle_add${dem}" value="${index}" data-id="${dem}"> ${value}
                    </label>`;
        });
        html += '</div>';
        // addon ram
        html += '<div class="form-group">';
        html += '<label id="label_addon_ram'+ dem +'" for="addon_ram_server">Addon RAM</label>';
        html += '<select id="addon_ram_server'+ dem +'" name="addon_ram['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ram_server_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 2
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk2'+ dem +'" for="addon_disk2">Addon Disk 2</label>';
        html += '<select id="addon_disk2'+ dem +'" name="addon_disk2['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk2_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 3
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk3'+ dem +'" for="addon_disk3">Addon Disk 3</label>';
        html += '<select id="addon_disk3'+ dem +'" name="addon_disk3['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk3_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 4
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk4'+ dem +'" for="addon_disk4">Addon Disk 4</label>';
        html += '<select id="addon_disk4'+ dem +'" name="addon_disk4['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk4_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 5
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk5'+ dem +'" for="addon_disk5">Addon Disk 5</label>';
        html += '<select id="addon_disk5'+ dem +'" name="addon_disk5['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk5_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 6
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk6'+ dem +'" for="addon_disk6">Addon Disk 6</label>';
        html += '<select id="addon_disk6'+ dem +'" name="addon_disk6['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk6_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 7
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk7'+ dem +'" for="addon_disk7">Addon Disk 7</label>';
        html += '<select id="addon_disk7'+ dem +'" name="addon_disk7['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk7_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon disk 8
        html += '<div class="form-group">';
        html += '<label id="label_addon_disk8'+ dem +'" for="addon_disk8">Addon Disk 8</label>';
        html += '<select id="addon_disk8'+ dem +'" name="addon_disk8['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk8_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // addon ip
        html += '<div class="form-group">';
        html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
        html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
        html += '<option value="0" selected>None</option>'
        html += '</select>';
        html += '</div>';
        // os
        html += '<div class="form-group">';
        html += '<label for="os" class="mr-4">Hệ điều hành</label>';
        html += `
          <label class="radio-inline ml-4 mr-4">
            <input type="radio" name="os[${dem}]" class='chooseOs' value="0" data-id="${dem}" checked> Ẩn
          </label>
          <label class="radio-inline ml-4 mr-4">
            <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows 10 64bit" data-id="${dem}"> Windows 10 64bit
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows Server 2012" data-id="${dem}"> Windows Server 2012
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" value="Windows Server 2016" class='chooseOs' data-id="${dem}"> Windows Server 2016
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" value="Linux CentOS 7" class='chooseOs' data-id="${dem}"> Linux CentOS 7
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" value="Windows Server 2019" class='chooseOs' data-id="${dem}"> Windows Server 2019
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" value="Linux Ubuntu-20.04" class='chooseOs' data-id="${dem}"> Linux Ubuntu-20.04
          </label>
          <label class="radio-inline mr-4">
            <input type="radio" name="os[${dem}]" value="VMware ESXi 6.7" class='chooseOs' data-id="${dem}"> VMware ESXi 6.7
          </label>
          <div class="os${dem}">
          </div>`;
        html += '</div>';
        // qtt
        html += '<div class="form-group">';
        html += '<label for="qtt">Số lượng</label>';
        html += '<input type="number" id="qtt'+ dem +'" name="qtt['+ dem +']" data-id="'+ dem +'" value="1" class="form-control qtt_add" required disabled>';
        html += '</div>';
        // price
        html += '<div class="form-group">';
        html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'"></span></b>';
        html += '</div>';
        html += '<div class="form-group row">';
        html += '<div class="col-md-1">';
        html += '<label>Giá áp tay</label>';
        html += '</div>';
        html += '<div class="col-md-10 icheck-primary d-inline">';
        html += '<input type="checkbox" class="custom-control-input" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
        html += '<label for="checkboxAmount'+ dem +'"></label>';
        html += '</div>';
        html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="0" class="form-control">';
        html += '</div>';
        // tu ngay
        html += '<div class="form-group">';
        html += '<label for="start_date">Từ ngày</label>';
        html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="end_date">Đến ngày</label>';
        html += `<div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text">
                      <i class="far fa-calendar-alt"></i>
                    </span>
                  </div>
                  <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
                </div>`;
        html += '</div>';
        html += '</div>';
        $('.info_quotation'+ dem +'').html(html);
  
        $('.reservation').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
        });
    }
    function changeLoadAddProductColocation(dem){
      let html = '<div class="info_product_vps'+dem+'">';
      html += '<div class="form-group" id="config_server'+ dem +'">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class="billing_cycle_add billing_cycle_add${dem}" value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
  
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt'+ dem +'" name="qtt['+ dem +']" data-id="'+ dem +'" value="1" class="form-control qtt_add" required disabled>';
      html += '</div>';
      // datacenter
     html += '<div class="form-group">';
     html += '<label id="label_datacenter" for="datacenter">Datacenter</label>';
     html += '<select id="datacenter'+ dem +'" name="datacenter['+ dem +']" class="form-control" disabled>';
     html += '<option value="0" selected>None</option>'
     html += '</select>';
     html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label>Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
      html += '<label for="checkboxAmount'+ dem +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation'+ dem +'').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    $('#btn_add_product').on('click', function () {
        dem++;
        let html = '<div class="choose-product mt-4" id="add_product_'+ dem +'">';
        html += '<div class="card-tools">';
        html += '<button type="button" class="btn btn-tool" data-id="'+ dem +'" data-toggle="tooltip" data-placement="top" title="Xóa sản phẩm"><i class="fas fa-times"></i></button>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="type_product">Loại sản phẩm</label>';
        html += '<select name="type_product['+ dem +']" id="type_product'+ dem +'" data-id="'+ dem +'" class="form-control choose_type_product">';
        html += '<option value="" disabled selected>Chọn loại sản phẩm</option>';
        html += '<option value="vps">VPS</option><option value="vps_us">VPS US</option><option value="server">Server vật lý</option>';
        html += '<option value="colocation">Colocation</option><option value="design_webiste">Thiết kế webiste</option><option value="domain">Domain</option><option value="hosting">Hosting</option>';
        html += '</select>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="product">Sản phẩm</label>';
        html += '<div class="product'+ dem +'">';
        html += '<input type="text" class="form-control" name="" value="" disabled>';
        html += '</div>';
        html += '</div>';
        html += '<div class="info_quotation'+ dem +'">';
        html += '</div>';
        html += '</div>';
        $("#add_product").append(html);
        $('[data-toggle="tooltip"]').tooltip();
        $('#total').val(0);
    });

    $(document).on('click', '#checkboxAmount', function() {
      if($(this).is(':checked')) {
        $('#amount').attr('type', 'text');
      } else {
        $('#amount').attr('type', 'hidden');
      }
    })

    $(document).on('change', '#product', function () {
        get_product();
    })

    $(document).on('change', '.billing_cycle_product', function () {
      get_product();
    })

    $(document).on('keyup', '#addon_cpu', function() {
      get_product();
    })

    $(document).on('keyup', '#addon_ram', function() {
      get_product();
    })

    $(document).on('keyup', '#qtt', function() {
      get_product();
    })

    $(document).on('keyup', '#addon_disk', function() {
      get_product();
    })

    $(document).on('change', '#addon_ram_server', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk2', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk3', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk4', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk5', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk6', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk7', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk8', function() {
      get_product();
    })

    $(document).on('change', '#addon_ip', function() {
      get_product();
    })

    function get_product() {
        var product_id = $('#product').val();
        var billing_cycle = $('.billing_cycle_product:checked');
        if ( billing_cycle.length == 0 ) {
          billing_cycle = 'monthly';
        } else {
          billing_cycle = billing_cycle.val();
        }
        var addon_cpu = $('#addon_cpu').val();
        var id_addon_cpu = $('#addon_cpu').attr('data-id');
        var addon_ram = $('#addon_ram').val();
        var id_addon_ram = $('#addon_ram').attr('data-id');
        var addon_disk = $('#addon_disk').val();
        var id_addon_disk = $('#addon_disk').attr('data-id');
        var qtt = $('#qtt').val();
        var addon_ram_server = $('#addon_ram_server').val();
        var addon_disk2 = $('#addon_disk2').val();
        var addon_disk3 = $('#addon_disk3').val();
        var addon_disk4 = $('#addon_disk4').val();
        var addon_disk5 = $('#addon_disk5').val();
        var addon_disk6 = $('#addon_disk6').val();
        var addon_disk7 = $('#addon_disk7').val();
        var addon_disk8 = $('#addon_disk8').val();
        var addon_ip = $('#addon_ip').val();
        $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_product",
            data: {
              id: product_id, billing_cycle: billing_cycle, addon_cpu: addon_cpu, id_addon_cpu: id_addon_cpu, 
              addon_ram: addon_ram, id_addon_ram: id_addon_ram, addon_disk: addon_disk, id_addon_disk: id_addon_disk,
              addon_disk2: addon_disk2, addon_disk3: addon_disk3, addon_disk4: addon_disk4, addon_disk5: addon_disk5,
              addon_disk6: addon_disk6, addon_disk7: addon_disk7, addon_disk8: addon_disk8, addon_ip: addon_ip,
              addon_ram_server: addon_ram_server
            },
            dataType: "json",
            success: function (data) {
              console.log(data);
                var html = '';
                if (data.type_product == 'VPS' || data.type_product == 'NAT-VPS'
                  || data.type_product == 'VPS-US'   ) {
                    $('#textAmount').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                    $('#amount').val(data.amount * parseInt(qtt));
                    $('#amount').val(data.amount);
                    $('#qtt_ip').attr('disabled', false);
                    $('#addon_cpu').attr('disabled', false);
                    $('#addon_ram').attr('disabled', false);
                    $('#addon_disk').attr('disabled', false);
                    $('#qtt').attr('disabled', false);
                    $('#os').attr('disabled', false);
                    $('#amount').attr('disabled', false);
                }else if(data.type_product == 'Colocation'){
                  $('#textAmount').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount').val(data.amount * parseInt(qtt));
                  $('#qtt_ip').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);
                  $("#datacenter").attr('disabled', false);
                  var html_datacenter= '';
                  html_datacenter += '<option value="0" selected>None</option>';
                  $.each(data.datacenter , function (index, datacenter) {
                    html_datacenter += '<option value="'+ datacenter.datacenter +'">'+ datacenter.datacenter + '</option>';
                  })   
                  $('#datacenter').html(html_datacenter); 
                }
                else if ( data.type_product == 'Hosting' ) {
                  $('#textAmount').text(addCommas(data.amount) + ' VNĐ');
                  $('#qtt').attr('disabled', false);
                  $('#domain').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                }
                else if ( data.type_product == 'Server' ) {
                  // console.log(data.product.second);
                  $('#textAmount').text(addCommas(data.product.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount').val(data.product.amount * parseInt(qtt));
                  $('#qtt_ip').attr('disabled', false);
                  $('#addon_ram_server').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#os').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  if ( data.product.second ) {
                    $('#addon_disk2').attr('disabled', true);
                  } else {
                    $('#addon_disk2').attr('disabled', false);
                  }
                  $('#addon_disk3').attr('disabled', false);
                  $('#addon_disk4').attr('disabled', false);
                  $('#addon_disk5').attr('disabled', false);
                  $('#addon_disk6').attr('disabled', false);
                  $('#addon_disk7').attr('disabled', false);
                  $('#addon_disk8').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);

                  var html = '';
                  html += '<label id="" for="">Cấu hình sản phẩm</label>';
                  html += '<div>';
                  html += data.product.config;
                  html += '</div>';
                  $('#config_server').html(html);
                }
                $('#total').val(0);
            },
            error: function (e) {
              console.log(e);
              $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
        });
    }

    $(document).on('change', '.choose_add_product', function () {
        var id = $(this).attr('data-id');
        get_add_product(id);
    })

    $(document).on('change', '.billing_cycle_add', function () {
        var id = $(this).attr('data-id');
        get_add_product(id);
    })

    $(document).on('keyup', '.addon_add', function () {
      var id = $(this).attr('data-id-product');
      //  console.log(id);
      get_add_product(id);
    })

    $(document).on('click', '.checkboxAmount', function() {
      var id = $(this).attr('data-id');
      // console.log('#amount' + id);
      if($(this).is(':checked')) {
        $('#amount' + id).attr('type', 'text');
      } else {
        $('#amount' + id).attr('type', 'hidden');
      }
    })

    $(document).on('keyup', '.qtt_add', function() {
      var id = $(this).attr('data-id');
      get_add_product(id);
    })

    $(document).on('change', '.addon_ram_server_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk2_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk3_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk4_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk5_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk6_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk7_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk8_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_ip_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    function get_add_product(id) {
      var product_id = $('#product' + id).val();
      var billing_cycle = $('.billing_cycle_add' + id + ':checked');
      if ( billing_cycle.length == 0 ) {
        billing_cycle = 'monthly';
      } else {
        billing_cycle = billing_cycle.val();
      }
      var addon_cpu = $('#addon_cpu' + id).val();
      var id_addon_cpu = $('#addon_cpu' + id).attr('data-id');
      var addon_ram = $('#addon_ram' + id).val();
      var id_addon_ram = $('#addon_ram' + id).attr('data-id');
      var addon_disk = $('#addon_disk' + id).val();
      var id_addon_disk = $('#addon_disk' + id).attr('data-id');
      // console.log(product_id, billing_cycle, id_addon_cpu, id_addon_ram, id_addon_disk);
      var qtt = $('#qtt' + id).val();
      // server
      var addon_ram_server = $('#addon_ram_server' + id).val();
      var addon_disk2 = $('#addon_disk2' + id).val();
      var addon_disk3 = $('#addon_disk3' + id).val();
      var addon_disk4 = $('#addon_disk4' + id).val();
      var addon_disk5 = $('#addon_disk5' + id).val();
      var addon_disk6 = $('#addon_disk6' + id).val();
      var addon_disk7 = $('#addon_disk7' + id).val();
      var addon_disk8 = $('#addon_disk8' + id).val();
      var addon_ip = $('#addon_ip' + id).val();

        $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_product",
            data: {
              id: product_id, billing_cycle: billing_cycle, addon_cpu: addon_cpu, id_addon_cpu: id_addon_cpu, 
              addon_ram: addon_ram, id_addon_ram: id_addon_ram, addon_disk: addon_disk, id_addon_disk: id_addon_disk,
              addon_disk2: addon_disk2, addon_disk3: addon_disk3, addon_disk4: addon_disk4, addon_disk5: addon_disk5,
              addon_disk6: addon_disk6, addon_disk7: addon_disk7, addon_disk8: addon_disk8, addon_ip: addon_ip,
              addon_ram_server: addon_ram_server
            },
            dataType: "json",
            success: function (data) {
              console.log(data);
                var html = '';
                if (data.type_product == 'VPS' || data.type_product == 'NAT-VPS' 
                  || data.type_product == 'VPS-US' ) {
                    $('#amount'+ id +'').val(data.amount * parseInt(qtt));
                    $('#textAmount'+ id +'').text( addCommas(data.amount * parseInt(qtt)) + ' VNĐ' );
                    $('#qtt_ip'+ id +'').attr('disabled', false);
                    $('#addon_cpu'+ id +'').attr('disabled', false);
                    $('#addon_ram'+ id +'').attr('disabled', false);
                    $('#addon_disk'+ id +'').attr('disabled', false);
                    $('#qtt'+ id +'').attr('disabled', false);
                    $('#os'+ id +'').attr('disabled', false);
                    $('#amount'+ id +'').attr('disabled', false);
                }else if(data.type_product == 'Colocation'){
                  $('#textAmount'+ id +'').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount'+ id +'').val(data.amount * parseInt(qtt));
                  $('#qtt_ip'+ id +'').attr('disabled', false);
                  $('#amount'+ id +'').attr('disabled', false);
                  $('#qtt'+ id +'').attr('disabled', false);
                  $('#addon_ip'+ id +'').attr('disabled', false);
                  $("#datacenter"+ id +'').attr('disabled', false);
                  var html_datacenter= '';
                  html_datacenter += '<option value="0" selected>None</option>';
                  $.each(data.datacenter , function (index, datacenter) {
                    html_datacenter += '<option value="'+ datacenter.datacenter +'">'+ datacenter.datacenter + '</option>';
                  })   
                  $('#datacenter'+ id +'').html(html_datacenter); 
                }
                else if ( data.type_product == 'Hosting' ) {
                  $('#textAmount'+ id +'').text(addCommas(data.amount) + ' VNĐ');
                  $('#amount'+ id +'').val(data.amount);
                  $('#qtt'+ id +'').attr('disabled', false);
                  $('#domain'+ id +'').attr('disabled', false);
                  $('#amount'+ id +'').attr('disabled', false);
                }
                 else if( data.type_product == 'Server' ) {
                  // console.log(data.product.second);
                  $('#textAmount'+ id).text(addCommas(data.product.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount'+ id).val(data.product.amount * parseInt(qtt));
                  $('#qtt_ip'+ id).attr('disabled', false);
                  $('#addon_ram_server'+ id).attr('disabled', false);
                  $('#qtt'+ id).attr('disabled', false);
                  $('#os'+ id).attr('disabled', false);
                  $('#amount'+ id).attr('disabled', false);
                  if ( data.product.second ) {
                    $('#addon_disk2'+ id).attr('disabled', true);
                  } else {
                    $('#addon_disk2'+ id).attr('disabled', false);
                  }
                  $('#addon_disk3'+ id).attr('disabled', false);
                  $('#addon_disk4'+ id).attr('disabled', false);
                  $('#addon_disk5'+ id).attr('disabled', false);
                  $('#addon_disk6'+ id).attr('disabled', false);
                  $('#addon_disk7'+ id).attr('disabled', false);
                  $('#addon_disk8'+ id).attr('disabled', false);
                  $('#addon_ip'+ id).attr('disabled', false);

                  var html = '';
                  html += '<label id="" for="">Cấu hình sản phẩm</label>';
                  html += '<div>';
                  html += data.product.config;
                  html += '</div>';
                  $('#config_server'+ id).html(html);
                }
            },
            error: function (e) {
              console.log(e);
              $('.product'+ id +'').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
        });
    }

    $('#add_user').on('click', function () {
      $('#create-user').modal('show');
      $('#btn_submit').fadeIn();
      $('#btn_finish').fadeOut();
      generatePassword();
    })

    $('#btn_submit').on('click', function () {
      var form = $('#form_create_user').serialize();

      $.ajax({
        url: '/admin/users/quotation/create_user',
        type: 'post',
        data: form,
        dataType: 'json',
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('#container_notification').html(html);
          $('#form_create_user').css('opacity', 0.5);
          $('#btn_submit').attr('disabled', true);
          $('#form_create_user').attr('disabled', true);
        },
        success: function (data) {
          // console.log(data);
          if (data.error == 0) {
            var html = '<p class="text-success">Tạo khách hàng mới thành công</p>';
            $('#container_notification').html(html);
            $('#form_create_user')[0].reset();
            $('#form_create_user').css('opacity', 1);
            $('#btn_submit').attr('disabled', false);
            $('#form_create_user').attr('disabled', false);
            let html2 = '';
            html2 += '<option value="" disabled selected>Chọn khách hàng</option>';
            $.each(data.users, function (index, user) {
             html2 += '<option value="'+ user.id +'">' + user.name + ' - ' + user.email + '</option>';
            });
            $('#user').html(html2);
          } else {
            var html = '<p class="text-danger">Tạo khách hàng thất bại</p>';
            $('#container_notification').html(html);
            $('#form_create_user').css('opacity', 1);
            $('#btn_submit').attr('disabled', false);
            $('#form_create_user').attr('disabled', false)
          }
        },
        error: function (e) {
           console.log(e);
           $('#container_notification').html('<span class="text-danger">Truy vấn thất bại.</span>');
        }
      })
    })

    function generatePassword() {
        var length = 12;
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#password_confirmation').val(retVal);
        $('#password').val(retVal);
    }

    $(document).on('click', '.show_password', function () {
        $('#password').attr('type', 'text');
        $('.show_password i').removeClass('fa-eye');
        $('.show_password i').addClass('fa-eye-slash');
        $(this).addClass('hidden_password');
        $(this).removeClass('show_password');
    })

    $(document).on('click', '.hidden_password', function () {
        $('#password').attr('type', 'password');
        $('.hidden_password i').addClass('fa-eye');
        $('.hidden_password i').removeClass('fa-eye-slash');
        $(this).removeClass('hidden_password');
        $(this).addClass('show_password');
    })

    $('#enterprise').on('click', function () {
        if ( $(this).is(':checked') ) {
          var html = '';
          html += '<div class="col-2">';
          html += '<label for="imst">Mã số thuế</label>';
          html += '</div>';
          html += '<div class="col-10">';
          html += '<input type="text" name="mst" value="" class="form-control" placeholder="Mã số thuế">';
          html += '</div>';
          $('#mst').html(html);
        } else {
          $('#mst').html('');
        }
    })

    $(document).on('click', '.chooseOs', function() {
      // console.log($(this).val());
      if ( $(this).val() == 'other' ) {
        var html = '';
        var id = $(this).attr('data-id');
        html += '<input type="text" name="os_replace['+ id +']" required value="" class="form-control"  placeholder="Hệ điều hành">';
        $('.os'+ id).html(html);
      } else {
        var id = $(this).attr('data-id');
        $('.os'+ id).html('');
      }
    });

    function formatDate(format_date) {
      let date = new Date(format_date);
      var day = date.getDate();
      if (day < 10) {
        day = '0' + day;
      }
      var month = date.getMonth() + 1;
      if (month < 10) {
        month = '0' + month;
      }
      return  month + '/' + day + '/' + date.getFullYear();
    }
    
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
