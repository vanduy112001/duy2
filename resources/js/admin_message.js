$(document).ready(function() {

  let sm = 0;
  const msgerChat = $(".msg_history");
  let scroll = 300;
  const token = $('meta[name="csrf-token"]').attr('content');

	$('.form_chat *').attr('disabled' , true);
	$(document).on('click', '.chat_list', function(event) {
		event.preventDefault();
		const token = $('meta[name="csrf-token"]').attr('content');
		/* Act on the event */
		$('.chat_list').removeClass('active_chat');
		$(this).addClass('active_chat');
		let user_id = $(this).attr('data-id');
		$('#user').val(user_id);
    $('.user_chat_' + user_id + ' h5').removeClass('font-weight-bold');
    $('.user_chat_' + user_id + ' p').removeClass('font-weight-bold');
    $('.user_chat_' + user_id + ' p').removeClass('text-primary');
		$.ajax({
            url: '/admin/messages/loadMessageByUser',
            type: 'get',
            dataType: 'json',
            data: {user_id: user_id},
            beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('.msg_history').html(html);
             },
            success: function (data) {
              	// console.log(data);
            	if (data.error == 0) {
                // info
                var html_frame = `
                  <div class="widget-user-image">
                    <img class="img-circle elevation-2" alt="User Avatar" src="${data.avatar}"></img>
                  </div>
                  <div class="widget-user-info">
                    <div class="widget-user-username">${data.user_name}</div>
                    <div class="widget-user-desc">${data.user_email}</div>
                  </div>
                `;
                $('.frames_user').html(html_frame);
                // form chat
            		$('.form_chat *').attr('disabled' , false);
                $('.msg_history').addClass('user_' + user_id);
                $('.msg_history').attr('data-id', user_id);
            		loadChat(data.content, user_id);
                msgerChat.scrollTop( scroll );
            	}
            	else if (data.error == 2) {
            		$('.form_chat *').attr('disabled' , false);
            		$('.msg_history').html("");
            	}
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<div class="mt-4 text-center text-danger">';
              html += 'Truy vấn tin nhắn lỗi!';
              html += '</div>';
              $('.msg_history').html(html);
            }
          })
	});

	function loadChat(data, user_id) {
		let html = '';
    // let checkImage = false;
    const content = data.data.reverse();
    // console.log(content);
		$.each(content, function(index, val) {
			/* iterate through array or object */
			// console.log(val.user_id , user_id);
			if (val.user_id == user_id) {
        html += `
                <div class="incoming_msg">
                  <div class="received_msg">
                    <div class="received_withd_msg">
                      <p>${val.content}</p>
                      <span class="time_date"> ${val.timeSend}    |    ${val.dateSend}</span>
                    </div>
                  </div>
                </div>
            `;
			} else {
        // checkImage = false;
        let status = '';
        if ( val.status == 'Pending' ) {
          status = '<span class="col-1 text-right status-msg-' + val.id + '"><i class="far fa-circle"></i></span>';
        } else {
          status = '<span class="col-1 text-right status-msg-' + val.id + '"><i class="far fa-check-circle"></i></span>';
        }
				html += `
					   <div class="outgoing_msg">
	                    <div class="sent_msg">
	                      <p>${val.content}</p>
                        <div class="row">
                          <span class="col-11 time_date"> ${val.user.name} | ${val.timeSend}    |    ${val.dateSend}</span>
                          ${status}
                        </div>
	                    </div>
	           </div>
				  `;
			}
      scroll += 150;
		});
		$('.msg_history').html(html);
	}

	$('.form_chat').on('submit', function(event) {
          event.preventDefault();
          /* Act on the event */
          const msgText = $(".write_msg").val();
          const msgUserId = $('.msg_history').attr('data-id');
          // console.log(msgText);
          if (!msgText) return;
          appendMessage("right", msgText, sm);
          $(".write_msg").val('');
          
          $.ajax({
            url: '/admin/messages/sendMessage',
            type: 'POST',
            dataType: 'json',
            data: {'_token': token,msgText: msgText, msgUserId: msgUserId},
            success: function (data) {
              if (!data.error) {
                $('.status-msg-'+ sm +' i').removeClass('fa-circle');
                $('.status-msg-'+ sm +' i').addClass('fa-check-circle');
              } 
              else if ( data.error == 1 ) {
                $('.status-msg-'+ sm +' i').removeClass('fa-circle');
                $('.status-msg-'+ sm +' i').addClass('fa-times-circle');
                $('.status-msg-'+ sm).addClass('text-danger');
              }
            },
            error: function (e) {
              console.log(e);
              $('.status-msg-'+ sm +' i').removeClass('fa-circle');
              $('.status-msg-'+ sm +' i').addClass('fa-times-circle');
              $('.status-msg-'+ sm).addClass('text-danger');
            }
          })
          
  });

	function appendMessage(side, text, sm) {
          //   Simple solution for small apps
        const msgHTML = `
	        <div class="outgoing_msg">
		        <div class="sent_msg">
		        	<p>${text}</p>
              <div class="row">
                <span class="col-11 text-right time_date"> ${formatDate(new Date())}</span>
                <span class="col-1 text-right status-msg-${sm}">
                    <i class="far fa-circle"></i>
                </span>
              </div>
		        </div>
	        </div>
	     `;
        scroll += 300;
        // console.log(scroll);
        msgerChat.append(msgHTML);
        msgerChat.scrollTop( scroll );
      }

      function formatDate(date) {
      	const h = "0" + date.getHours();
      	const m = "0" + date.getMinutes();

      	return `${h.slice(-2)}:${m.slice(-2)}`;
      }

});