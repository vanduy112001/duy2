$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    
    $('.btn-delete-colo').on('click', function () {
        $('tr').removeClass('action-colo');
        $(this).closest('tr').addClass('action-colo');
        var id = $(this).attr("data-id");
        var ip = $(this).attr('data-ip');
        $('#delete-vps').modal('show');
        $('.modal-title').text('Xóa dịch vụ Colocation');
        var html = '';
        html += '<span class="">';
        html += 'Bạn có muỗn xóa dịch vụ Colocation <b class="text-danger">' + ip + '</b> này không?';
        html += '</span>';
        $('#notication-invoice').html(html);
        $('#button-vps').fadeIn();
        $('#button-vps-finish').fadeOut();
        $('#button-vps').attr('data-id', id);
    })

    $('#button-vps').on('click', function () {
       var id = $(this).attr('data-id');
       var token = $('meta[name="csrf-token"]').attr('content');
       $.ajax({
          url: '/admin/colocation/delete',
          type: 'post',
          data: {id: id, _token: token},
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#button-vps').attr('disabled', true);
              $('#notication-invoice').html(html);
          },
          success: function (data) {
              console.log(data);
              if (data) {
                  var html = '<p class="text-danger">Xóa Colocation thành công</p>';
                  $('#notication-invoice').html(html);
                  $('#button-vps').attr('disabled', false);
                  $('.action-colo').fadeOut(1500);
                  $('#button-vps').fadeOut();
                  $('#button-vps-finish').fadeIn();
              } else {
                  var html = '<p class="text-danger">Xóa Colocation thất bại</p>';
                  $('#notication-colo').html(html);
                  $('#button-vps').attr('disabled', false);
              }
          },
          error: function (e) {
             console.log(e);
             $('#notication-invoice').html('<span class="text-danger">Truy vấn Colocation thất bại!</span>');
          }
       })
    })

    $('#qtt').on('change', function () {
        loadColocation();
    })

    $('#status').on('change', function () {
        loadColocation();
    })

    $('#q').on('keyup', function () {
        loadColocation();
    })

    function loadColocation() {
        var qtt = $('#qtt').val();
        var status = $('#status').val();
        var q = $('#q').val();
        $.ajax({
            url: '/admin/colocation/getColocation',
            data: { qtt: qtt, status: status, q: q },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data != '') {
                    screent_colocation(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Không có dịch vụ Colocation trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    function screent_colocation(data) {
        var html = '';
        $.each(data.data, function(index, colo) {
            if ( colo ) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + colo.id + '" data-ip="' + colo.ip + '" class="checkbox"></td>';
                // khách hàng
                html += '<td><a href="/admin/users/detail/'+ colo.user_id +'">'+ colo.text_user_name +'</a></td>';
                // sản phẩm
                html += '<td>' + colo.text_product + '</td>';
                // ip
                html += '<td><a href="/admin/colocation/detail/'+ colo.id +'">'+ colo.ip +'</a>';
                // loai colo
                html += '<td>' + colo.type_colo + '</td>';
                // sl ip
                html += '<td>';
                html += colo.count_ip;
                html += '<button type="button" class="ml-1 btn btn-sm btn-outline-success float-right collapsed tooggle-plus" data-toggle="collapse" data-target="#service' + colo.id + '" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">'
                html += '<i class="fas fa-plus"></i></button>';
                html += '<div id="service' + colo.id + '" class="mt-4 collapse" style="">'; 
                html += colo.text_addon_ip; 
                html += '</div>'; 
                html += '</td>'; 
                // user/password
                html += '<td>';
                html += colo.location + '<br>';
                html += colo.rack;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span>' + colo.text_next_due_date + '<span>';
                html += '</td>';
                // tổng thoi gian thue
                html += '<td>' + colo.total_time + '</td>';
                // chu kỳ thanh toán
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // trang thai
                html += '<td class="server-status">' + colo.text_status + '</td>';
                // hành động
                html += '<td class="page-service-action">';
                html += '<a href="/admin/colocation/detail/'+ colo.id +'" data-toggle="tooltip" class="btn btn-sm btn-warning text-light mr-1" data-placement="top" title="Chỉnh sửa" style="font-size: 10px;"><i class="fas fa-edit"></i></a>';
                html += '<button class="btn btn-sm mr-1 btn-outline-danger btn-delete-colo" data-toggle="tooltip" data-placement="top" title="Xóa dịch vụ" data-action="terminated" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Colocation này không tồn tại.</td>';
                html += '</tr>';
            }
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length)
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    $(document).on('click', '.pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('#qtt').val();
        var status = $('#status').val();
        var q = $('#q').val();
        $.ajax({
            url: '/admin/colocation/getColocation',
            data: { qtt: qtt, status: status, q: q, page: page },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data != '') {
                    screent_colocation(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Không có dịch vụ Colocation trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    var $vpsCheckbox = $('.checkbox');
    var lastChecked = null;

    $(document).on('click','.checkbox', function () {
        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action');
        } else {
            $(this).closest('tr').removeClass('action');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        $vpsCheckbox.click(function (e) {
            if (!lastChecked) {
                lastChecked = this;
                return;
            }
            if ( e.shiftKey ) {
                var start = $vpsCheckbox.index(this);
                var end = $vpsCheckbox.index(lastChecked);
                $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
                $.each( $vpsCheckbox , function (index , value) {
                    if( $(this).is(':checked') ) {
                        $(this).closest('tr').addClass('action');
                    } else {
                        $(this).closest('tr').removeClass('action');
                    }
                })
            }
            lastChecked = this;
        })

        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
    });

    $(document).on('click', '.checkbox_all', function() {
        if ($(this).is(':checked')) {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').addClass('action');
        } else {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').removeClass('action');
        }
        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
    })

    

})
