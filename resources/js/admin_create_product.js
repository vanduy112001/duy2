$(document).ready(function() {

    var type_product = $('#type_product:selected').val();
    loadPackage(type_product);

    $('#type_product').on('change', function() {
        // console.log('da click');
        var type_product = $(this).val();
        loadPackage(type_product);
    });

    function loadPackage(type_product) {
        if (type_product == 'Hosting') {
            $.ajax({
                url: '/admin/directAdmin/showPackage',
                data: {location: 'vn'},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#package').html(html);
                },
                success: function (data) {
                    var html = '';
                    if (data != '' || data != null) {
                        html += '<select name="package"  class="form-control">';
                        $.each(data, function(index, value) {
                            html += '<option value="'+value+'">'+value+'</option>';
                        });
                        html += '</select>';
                    } else {
                        html += '<p class="text-danger">Không có Package trên Direct Admin.<p>';
                    }
                    $('#package').html(html);
                },
                error: function (e) {
                    console.log(e);
                    $('#package').html('<p class="text-danger">Truy vấn Direct Admin lỗi!<p>');
                }
            });
        }
        else if (type_product == 'Hosting-Singapore') {
            $.ajax({
                url: '/admin/directAdmin/showPackage',
                data: {location: 'si'},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#package').html(html);
                },
                success: function (data) {
                    var html = '';
                    if (data != '' || data != null) {
                        html += '<select name="package"  class="form-control">';
                        $.each(data, function(index, value) {
                            html += '<option value="'+value+'">'+value+'</option>';
                        });
                        html += '</select>';
                    } else {
                        html += '<p class="text-danger">Không có Package trên Direct Admin.<p>';
                    }
                    $('#package').html(html);
                },
                error: function (e) {
                    console.log(e);
                    $('#package').html('<p class="text-danger">Truy vấn Direct Admin lỗi!<p>');
                }
            });
        }
    }
});
