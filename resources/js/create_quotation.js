$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    let dem = 0;
    let billing_cycle = {
      'monthly' : '1 Tháng',
      'twomonthly' : '2 Tháng',
      'quarterly' : '3 Tháng',
      'semi_annually' : '6 Tháng',
      'annually' : '1 Năm',
      'biennially' : '2 Năm',
      'triennially' : '3 Năm',
    };

    loadQuotation();

    function loadQuotation() {
      var q = $('#quotation_search').val();
      $.ajax({
         url: '/admin/users/list_quotation',
         dataType: 'json',
         data: { q: q},
         beforeSend: function(){
             var html = '<td  colspan="4" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
             // console.log(data);
             if (data.data != '') {
                 screent_quotation(data);
                 $('[data-toggle="tooltip"]').tooltip();
             } else {
                 $('tbody').html('<td colspan="4" class="text-center text-danger">Không có dữ liệu của bảng báo giá!</span>');
             }
         },
         error: function (e) {
            console.log(e);
            $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn thất bại!</span>');
         }
      })
    }

    function screent_quotation(data) {
      var html = '';
      $.each(data.data , function (index, quotation) {
        // console.log(quotation);
        html += '<tr>';
        html += '<td><a href="/admin/users/detail/'+ quotation.user_id +'">'+ quotation.user_name +'</a></td>';
        html += '<td>'+ quotation.title +'</td>';
        html += '<td>'+ quotation.create_date +'</td>';
        html += '<td  class="button-action">';
        if ( quotation.link ) {
          html += '<a href="'+ quotation.link +'" class="btn btn-info" download data-toggle="tooltip" title="Tải báo giá"><i class="fas fa-download"></i></a>';
        }
        html += '<a href="/admin/users/xem-truoc-bao-gia/'+ quotation.id +'" class="btn btn-success" data-toggle="tooltip" title="Xem trước báo giá"><i class="fas fa-chart-bar"></i></a>';
        html += '<a href="/admin/users/chinh-sua-bao-gia/'+ quotation.id +'" class="btn btn-warning text-white" data-toggle="tooltip" title="Chỉnh sửa báo giá"><i class="fas fa-edit"></i></a>';
        html += '<button class="btn btn-danger delete-order" data-id="'+ quotation.id +'"><i class="fas fa-trash-alt" data-toggle="tooltip" title="Xóa báo giá"></i></button>';
        html += '</td>';
        html += '</tr>';
      })
      $('tbody').html(html);
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_quotation">';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '</nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_quotation">';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
      }
      $('tfoot').html(html_page);
    }

    $(document).on('click', '.delete-order' , function () {
        var quotation_id = $(this).attr('data-id');
        $('tr').removeClass('remove-quotation');
        $('#delete_quotation').fadeIn();
        $('#delete_quotation').attr('data-id', quotation_id);
        $('#finish_quotation').fadeOut();
        $('#delete-product').modal('show');
        $('#notication-product').html('<span class="">Bạn có muốn xóa báo giá <b class="text-danger">(ID: '+ quotation_id +')</b> này không?</span>');
        $(this).closest('tr').addClass('remove-quotation');
    })

    $('#delete_quotation').on('click', function () {
        var quotation_id = $(this).attr('data-id');
        $.ajax({
           url: '/admin/users/quotation/delete_quotation',
           dataType: 'json',
           data: { id: quotation_id},
           beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('#notication-product').html(html);
               $('#delete_quotation').attr('disabled', true);
           },
           success: function (data) {
               // console.log(data);
               if (data != '') {
                   $('#notication-product').html('<span class="text-danger">Xóa báo giá thành công</span>');
                   $('#delete_quotation').attr('disabled', false);
                   $('.remove-quotation').fadeOut(1500);
                   $('#delete_quotation').fadeOut();
                   $('#finish_quotation').fadeIn();
               } else {
                   $('#notication-product').html('<span class="text-danger">Xóa báo giá thất bại</span>');
                   $('#delete_quotation').attr('disabled', false);
               }
           },
           error: function (e) {
              console.log(e);
              $('#notication-product').html('<span class="text-danger">Truy vấn báo giá lỗi!</span>');
              $('#delete_quotation').attr('disabled', false);
           }
        })
    })

    // ajax phân trang vps
    $(document).on('click', '.pagination_quotation a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var q = $('#quotation_search').val();
        $.ajax({
           url: '/admin/users/list_quotation',
           dataType: 'json',
           data: { q: q, page:page },
           beforeSend: function(){
               var html = '<td class="text-center"  colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // console.log(data);
               if (data.data != '') {
                   screent_quotation();
                   $('[data-toggle="tooltip"]').tooltip();
               } else {
                   $('tbody').html('<td colspan="4" class="text-center text-danger">Không có dữ liệu của bảng báo giá!</span>');
               }
           },
           error: function (e) {
              console.log(e);
              $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn thất bại!</span>');
           }
        })
    });

    $('#user').on('change', function () {
        let user_id = $(this).val();
        let type_product = $('#type_product').val();
        if ( type_product != '' ) {
          loadProduct(user_id, type_product);
        }
    })

    $('#type_product').on('change', function () {
        let user_id = $('#user').val();
        let type_product = $(this).val();
        if ( user_id != '' ) {
          loadProduct(user_id, type_product);
        }
    })

    function loadProduct(user_id, type_product) {
      if (type_product == 'vps' || type_product == 'vps_us' || type_product == 'hosting' 
    
      ) {
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
            // console.log(data);
            var html = '';
            if (data.error == 0) {
              if (data.content != '') {
                html += '<select class="form-control select2" name="product_id['+ dem +']" id="product" required style="width:100%;">';
                html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                $.each(data.content , function (index, group_product) {
                  html += '<optgroup label="' + group_product.name + '">';
                  $.each(group_product.products , function ( index2, product ) {
                    html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                  })
                  html += '</optgroup>';
                })
                html += '</select>';
                if ( type_product == 'vps' || type_product == 'vps_us' || type_product == 'server' ) {
                  
                $.each(data.addon , function (index, product_addon) {
                  if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                    $('#label_addon_cpu').text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_cpu').attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                    $('#label_addon_ram').text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_ram').attr('data-id', product_addon.id);
                  }
                  if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                    $('#label_addon_disk').text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                    $('#addon_disk').attr('data-id', product_addon.id);
                  }
                })
                  loadProductVps(0);
                }
                else if ( type_product == 'design_webiste' || type_product == 'domain' || type_product == 'hosting' ) {
                  loadProductDomain(0);
                }
                $('#total').attr('disabled', false);

              }
              $('.product').html(html);
              $('#product').select2();
            }
        },

          error: function (e) {
            console.log(e);
            $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      if ( type_product == 'server' || type_product == 'colocation' ) {
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
            console.log(data);
              var html = '';
              if (data.error == 0) {
                if (data.content != '') {
                  html += '<select class="form-control select2" name="product_id['+ dem +']" id="product" required style="width:100%;">';
                  html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                  $.each(data.content , function (index, group_product) {
                    html += '<optgroup label="' + group_product.name + '">';
                    $.each(group_product.products , function ( index2, product ) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    })
                    html += '</optgroup>';
                  })
                  $('#total').attr('disabled', false);
                  if ( type_product == 'server' ) {
                    loadProductServer(0);
                    if ( data.addon_disk.length > 0 ) {
                      var html_addon_disk = '';
                      html_addon_disk += '<option value="0" selected>None</option>';
                      $.each(data.addon_disk , function (index, addon_disk) {
                        html_addon_disk += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_disk2').html(html_addon_disk);
                      $('#addon_disk3').html(html_addon_disk);
                      $('#addon_disk4').html(html_addon_disk);
                      $('#addon_disk5').html(html_addon_disk);
                      $('#addon_disk6').html(html_addon_disk);
                      $('#addon_disk7').html(html_addon_disk);
                      $('#addon_disk8').html(html_addon_disk);
                    }
                    if ( data.addon_ram.length ) {
                      var html_addon_ram = '';
                      html_addon_ram += '<option value="0" selected>None</option>';
                      $.each(data.addon_ram , function (index, addon_ram) {
                        html_addon_ram += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ram_server').html(html_addon_ram);
                    }
                    if ( data.addon_ip.length ) {
                      var html_addon_ip = '';
                      html_addon_ip += '<option value="0" selected>None</option>';
                      $.each(data.addon_ip , function (index, addon_ip) {
                        html_addon_ip += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ip').html(html_addon_ip);
                    }
                  }else if(type_product == 'colocation'){
                    loadProductIP(0) 
                      if ( data.addon.length > 0 ) {
                        var html_addon= '';
                        html_addon += '<option value="0" selected>None</option>';
                        $.each(data.addon , function (index, addon) {
                          html_addon += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
                        })   
                        $('#addon_ip').html(html_addon);     
                      }        
                  }
                }
                $('.product').html(html);
                $('#product').select2();
              }
          },
          error: function (e) {
            console.log(e);
            $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      else if ( type_product == 'design_webiste' || type_product == 'domain') {
        loadProductDomain(0);
        if ( type_product == 'design_webiste' || type_product == 'domain' ) {
          $('#qtt').attr('disabled', false);
          $('#domain').attr('disabled', false);
          $('#amount').attr('disabled', false);
        }
      }
      $('#total').attr('disabled', false);
    }

    function loadProductServer(dem) {
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group" id="config_server">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // addon ram
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram" for="addon_ram_server">Addon RAM</label>';
      html += '<select id="addon_ram_server" name="addon_ram['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 2
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk2" for="addon_disk2">Addon Disk 2</label>';
      html += '<select id="addon_disk2" name="addon_disk2['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 3
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk3" for="addon_disk3">Addon Disk 3</label>';
      html += '<select id="addon_disk3" name="addon_disk3['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 4
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk4" for="addon_disk4">Addon Disk 4</label>';
      html += '<select id="addon_disk4" name="addon_disk4['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 5
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk5" for="addon_disk5">Addon Disk 5</label>';
      html += '<select id="addon_disk5" name="addon_disk5['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 6
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk6" for="addon_disk6">Addon Disk 6</label>';
      html += '<select id="addon_disk6" name="addon_disk6['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 7
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk7" for="addon_disk7">Addon Disk 7</label>';
      html += '<select id="addon_disk7" name="addon_disk7['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 8
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk8" for="addon_disk8">Addon Disk 8</label>';
      html += '<select id="addon_disk8" name="addon_disk8['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip" name="addon_ip['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      html += `
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="0" data-id="${dem}" checked> Ẩn
        </label>
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows 10 64bit" data-id="${dem}"> Windows 10 64bit
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows Server 2012" data-id="${dem}"> Windows Server 2012
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2016" class='chooseOs' data-id="${dem}"> Windows Server 2016
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux CentOS 7" class='chooseOs' data-id="${dem}"> Linux CentOS 7
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2019" class='chooseOs' data-id="${dem}"> Windows Server 2019
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux Ubuntu-20.04" class='chooseOs' data-id="${dem}"> Linux Ubuntu-20.04
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="VMware ESXi 6.7" class='chooseOs' data-id="${dem}"> VMware ESXi 6.7
        </label>
        <div class="os${dem}">
        </div>`;
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    function loadProductVps(dem) {
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group">';
      html += '<label id="label_addon_cpu" for="addon_cpu">Addon CPU</label>';
      html += '<input type="number" id="addon_cpu" name="addon_cpu['+ dem +']" value="0" class="form-control" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram" for="addon_ram">Addon RAM</label>';
      html += '<input type="number" id="addon_ram" name="addon_ram['+ dem +']" value="0" class="form-control" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk" for="addon_disk">Addon DISK</label>';
      html += '<input type="number" id="addon_disk" name="addon_disk['+ dem +']" value="0" class="form-control" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="qtt_ip">Số lượng IP</label>';
      html += '<input type="number" id="qtt_ip" name="qtt_ip['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      html += `<label class="radio-inline ml-4 mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows" data-id="${dem}" checked> Windows
                  </label>
                  <label class="radio-inline mr-4">
                    <input type="radio" name="os[${dem}]" class='chooseOs' value="Linux" data-id="${dem}"> Linux
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="os[${dem}]" value="other" class='chooseOs' data-id="${dem}"> Khác
                  </label>
              <div class="os${dem}">
              </div>`;
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }
    function loadProductIP(dem){
      let html = '<div class="info_product_vps">';
      html += '<div class="form-group">';
      html += '<label for="qtt_ip">Số lượng IP</label>';
      html += '<input type="number" id="qtt_ip" name="qtt_ip['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      // addon
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip" name="addon_ip['+ dem +']" class="form-control" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
 
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
           // datacenter
       html += '<div class="form-group">';
       html += '<label id="label_datacenter" for="datacenter">Datacenter</label>';
       html += '<select id="datacenter" name="datacenter['+ dem +']" class="form-control" disabled>';
       html += '<option value="0" selected>None</option>'
       html += '</select>';
       html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    function loadProductDomain(dem) {
      let html = '<div class="info_product_domain">';
      html += '<div class="form-group domain">';
      html += '<label for="domain">Domain</label>';
      html += '<input type="text" id="domain" name="domain['+ dem +']" value="" class="form-control" required disabled>';
      html += '</div>';
      // html += '<div class="form-group">';
      // html += '<label for="qtt">Số lượng</label>';
      html += '<input type="hidden" id="qtt" name="qtt['+ dem +']" value="1" class="form-control" required disabled>';
      // html += '</div>';
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class='billing_cycle_product' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label for="amount">Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" id="checkboxAmount" value="1">';
      html += '<label for="checkboxAmount"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // từ ngày
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation').html(html);
      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    $(document).on('click', '.chooseOs', function() {
      // console.log($(this).val());
      if ( $(this).val() == 'other' ) {
        var html = '';
        var id = $(this).attr('data-id');
        html += '<input type="text" name="os_replace['+ id +']" required value="" class="form-control"  placeholder="Hệ điều hành">';
        $('.os'+ id).html(html);
      } else {
        var id = $(this).attr('data-id');
        $('.os'+ id).html('');
      }
    });

    $(document).on('click', ".btn-tool", function () {
      let id = $(this).attr('data-id');
      $('#add_product_'+ id +'').remove();
    })

    $(document).on("change", ".choose_type_product", function () {
      let user_id = $('#user').val();
      let type_product = $(this).val();
      let id = $(this).attr('data-id');
      if ( user_id != '' ) {
        loadAddProduct(user_id, type_product, id);
      }
    });

    function loadAddProduct(user_id, type_product, id) {
      if (type_product == 'vps' || type_product == 'vps_us' || type_product == 'hosting' 
      ){
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
              var html = '';
              if (data.error == 0) {
                if (data.content != '') {
                  html += '<select class="form-control select2 choose_add_product" name="product_id['+ id +']" id="product'+ id +'" data-id="'+ id +'" required style="width:100%;">';
                  html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                  $.each(data.content , function (index, group_product) {
                    html += '<optgroup label="' + group_product.name + '">';
                    $.each(group_product.products , function ( index2, product ) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    })
                    html += '</optgroup>';
                  })
                  html += '</select>';
                }
                $('.product'+id).html(html);
                $('#product'+id).select2();
                if ( type_product == 'vps' || type_product == 'vps_us'  ) {
                  loadAddProductVps(id);
                  $.each(data.addon , function (index, product_addon) {
                    if ( product_addon.meta_product.type_addon == 'addon_cpu' ) {
                      $('#label_addon_cpu'+ id).text('Addon CPU (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_cpu'+ id).attr('data-id', product_addon.id);
                    }
                    if ( product_addon.meta_product.type_addon == 'addon_ram' ) {
                      $('#label_addon_ram'+ id).text('Addon RAM (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_ram'+ id).attr('data-id', product_addon.id);
                    }
                    if ( product_addon.meta_product.type_addon == 'addon_disk' ) {
                      $('#label_addon_disk'+ id).text('Addon DISK (' + addCommas( product_addon.pricing.monthly ) + '/Tháng)');
                      $('#addon_disk'+ id).attr('data-id', product_addon.id);
                    }
                  })
                }
                else if ( type_product == 'hosting'  ) {
                  loadAddProductDomain(id);
                }
              }
          },
          error: function (e) {
            console.log(e);
            $('.product'+id).html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      else if ( type_product == 'server' || type_product == 'colocation' ) {
        $.ajax({
          type: "get",
          url: "/admin/users/quotation/get_group_product",
          data: {id: user_id, type_product: type_product},
          dataType: "json",
          success: function (data) {
            console.log(data);
              var html = '';
              if (data.error == 0) {
                if (data.content != '') {
                  html += '<select class="form-control select2 choose_add_product" name="product_id['+ id +']" id="product'+ id +'" data-id="'+ id +'" required style="width:100%;">';
                  html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                  $.each(data.content , function (index, group_product) {
                    html += '<optgroup label="' + group_product.name + '">';
                    $.each(group_product.products , function ( index2, product ) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    })
                    html += '</optgroup>';
                  })
                  $('.product'+id).html(html);
                  $('#product'+id).select2();
                  if ( type_product == 'server') {
                    loadAddProductServer(id);
                    if ( data.addon_disk.length > 0 ) {
                      var html_addon_disk = '';
                      html_addon_disk += '<option value="0" selected>None</option>';
                      $.each(data.addon_disk , function (index, addon_disk) {
                        html_addon_disk += '<option value="'+ addon_disk.id +'">'+ addon_disk.name +' - ' + addCommas(addon_disk.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_disk2'+ id).html(html_addon_disk);
                      $('#addon_disk3'+ id).html(html_addon_disk);
                      $('#addon_disk4'+ id).html(html_addon_disk);
                      $('#addon_disk5'+ id).html(html_addon_disk);
                      $('#addon_disk6'+ id).html(html_addon_disk);
                      $('#addon_disk7'+ id).html(html_addon_disk);
                      $('#addon_disk8'+ id).html(html_addon_disk);
                    }
                    if ( data.addon_ram.length ) {
                      var html_addon_ram = '';
                      html_addon_ram += '<option value="0" selected>None</option>';
                      $.each(data.addon_ram , function (index, addon_ram) {
                        html_addon_ram += '<option value="'+ addon_ram.id +'">'+ addon_ram.name +' - ' + addCommas(addon_ram.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ram_server'+ id).html(html_addon_ram);
                    }
                    if ( data.addon_ip.length ) {
                      var html_addon_ip = '';
                      html_addon_ip += '<option value="0" selected>None</option>';
                      $.each(data.addon_ip , function (index, addon_ip) {
                        html_addon_ip += '<option value="'+ addon_ip.id +'">'+ addon_ip.name +' - ' + addCommas(addon_ip.pricing.monthly) + '/Tháng</option>';
                      })
                      $('#addon_ip'+ id).html(html_addon_ip);
                    }
                  }else if(type_product == 'colocation'){
                    loadAddProductIP(id); 
                      if ( data.addon.length > 0 ) {  
                        var html_addon= '';
                        html_addon += '<option value="0" selected>None</option>';
                        $.each(data.addon , function (index, addon) {
                          html_addon += '<option value="'+ addon.id +'">'+ addon.name +' - ' + addCommas(addon.pricing.monthly) + '/Tháng</option>';
                        })   
                        $('#addon_ip' + id).html(html_addon);     
                      }        
                  }
                }
              }
          },
          error: function (e) {
            console.log(e);
            $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
          }
        });
      }
      else if ( type_product == 'design_webiste' || type_product == 'domain' ) {
        loadAddProductDomain(id);
        if ( type_product == 'design_webiste' || type_product == 'domain' ) {
          $('#qtt'+ id +'').attr('disabled', false);
          $('#domain'+ id +'').attr('disabled', false);
          $('#amount'+ id +'').attr('disabled', false);
        }
      }
    }

    function loadAddProductServer(dem) {
      let html = '<div class="info_product_vps'+dem+'">';
      html += '<div class="form-group" id="config_server'+ dem +'">';
      html += '<label id="" for="">Cấu hình</label>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class="billing_cycle_add billing_cycle_add${dem}" value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // addon ram
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram'+ dem +'" for="addon_ram_server">Addon RAM</label>';
      html += '<select id="addon_ram_server'+ dem +'" name="addon_ram['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ram_server_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 2
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk2'+ dem +'" for="addon_disk2">Addon Disk 2</label>';
      html += '<select id="addon_disk2'+ dem +'" name="addon_disk2['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk2_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 3
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk3'+ dem +'" for="addon_disk3">Addon Disk 3</label>';
      html += '<select id="addon_disk3'+ dem +'" name="addon_disk3['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk3_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 4
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk4'+ dem +'" for="addon_disk4">Addon Disk 4</label>';
      html += '<select id="addon_disk4'+ dem +'" name="addon_disk4['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk4_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 5
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk5'+ dem +'" for="addon_disk5">Addon Disk 5</label>';
      html += '<select id="addon_disk5'+ dem +'" name="addon_disk5['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk5_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 6
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk6'+ dem +'" for="addon_disk6">Addon Disk 6</label>';
      html += '<select id="addon_disk6'+ dem +'" name="addon_disk6['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk6_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 7
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk7'+ dem +'" for="addon_disk7">Addon Disk 7</label>';
      html += '<select id="addon_disk7'+ dem +'" name="addon_disk7['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk7_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon disk 8
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk8'+ dem +'" for="addon_disk8">Addon Disk 8</label>';
      html += '<select id="addon_disk8'+ dem +'" name="addon_disk8['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_disk8_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // addon ip
      html += '<div class="form-group">';
      html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
      html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
      html += '<option value="0" selected>None</option>'
      html += '</select>';
      html += '</div>';
      // os
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      html += `
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="0" data-id="${dem}" checked> Ẩn
        </label>
        <label class="radio-inline ml-4 mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows 10 64bit" data-id="${dem}"> Windows 10 64bit
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" class='chooseOs' value="Windows Server 2012" data-id="${dem}"> Windows Server 2012
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2016" class='chooseOs' data-id="${dem}"> Windows Server 2016
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux CentOS 7" class='chooseOs' data-id="${dem}"> Linux CentOS 7
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Windows Server 2019" class='chooseOs' data-id="${dem}"> Windows Server 2019
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="Linux Ubuntu-20.04" class='chooseOs' data-id="${dem}"> Linux Ubuntu-20.04
        </label>
        <label class="radio-inline mr-4">
          <input type="radio" name="os[${dem}]" value="VMware ESXi 6.7" class='chooseOs' data-id="${dem}"> VMware ESXi 6.7
        </label>
        <div class="os${dem}">
        </div>`;
      html += '</div>';
      // qtt
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt'+ dem +'" name="qtt['+ dem +']" data-id="'+ dem +'" value="1" class="form-control qtt_add" required disabled>';
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label>Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
      html += '<label for="checkboxAmount'+ dem +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation'+ dem +'').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    function loadAddProductVps(id) {
      // console.log("da den" + id);
      let html = '<div class="info_product_vps'+id+'">';
      html += '<div class="form-group">';
      html += '<label id="label_addon_cpu'+ id +'" for="addon_cpu'+ id +'">Addon CPU</label>';
      html += '<input type="number" id="addon_cpu'+ id +'" name="addon_cpu['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label id="label_addon_ram'+ id +'" for="addon_ram'+ id +'">Addon RAM</label>';
      html += '<input type="number" id="addon_ram'+ id +'" name="addon_ram['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label id="label_addon_disk'+ id +'" for="addon_disk'+ id +'">Addon DISK</label>';
      html += '<input type="number" id="addon_disk'+ id +'" name="addon_disk['+ id +']" data-id-product="'+ id +'" value="0" class="form-control addon_add" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="qtt_ip'+ id +'">Số lượng IP</label>';
      html += '<input type="number" id="qtt_ip'+ id +'" name="qtt_ip['+ id +']" value="1" class="form-control" required disabled>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="os" class="mr-4">Hệ điều hành</label>';
      html += `<label class="radio-inline ml-4 mr-4">
                    <input type="radio" name="os[${id}]" class='chooseOs' value="Windows" data-id="${id}" checked> Windows
                  </label>
                  <label class="radio-inline mr-4">
                    <input type="radio" name="os[${id}]" class='chooseOs' value="Linux" data-id="${id}"> Linux
                  </label>
                  <label class="radio-inline">
                    <input type="radio" name="os[${id}]" value="other" class='chooseOs' data-id="${id}"> Khác
                  </label>
              <div class="os${id}">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="qtt">Số lượng</label>';
      html += '<input type="number" id="qtt'+ id +'" name="qtt['+ id +']" data-id="'+ id +'" value="1" class="form-control qtt_add" required disabled>';
      html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${id}]" class='billing_cycle' value="${index}" data-id="${id}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${id}]" class="billing_cycle_add billing_cycle_add${id}" value="${index}" data-id="${id}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ id +'"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label>Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ id +'" id="checkboxAmount'+ id +'" value="1">';
      html += '<label for="checkboxAmount'+ id +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ id +'" name="amount['+ id +']" value="0" class="form-control">';
      html += '</div>';
      // từ ngày đến ngày
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation'+ id +'').html(html);
      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }
    function loadAddProductIP(dem){
      let html = '<div class="info_product_vps'+dem+'">';
      html += '<div class="form-group">';
      html += '<label for="qtt_ip">Số lượng IP</label>';
      html += '<input type="number" id="qtt_ip'+ dem +'" name="qtt_ip['+ dem +']" value="1" class="form-control" required disabled>';
      html += '</div>';
    
            // qtt
     html += '<div class="form-group">';
     html += '<label for="qtt">Số lượng</label>';
     html += '<input type="number" id="qtt'+ dem +'" name="qtt['+ dem +']" data-id="'+ dem +'" value="1" class="form-control qtt_add" required disabled>';
     html += '</div>';
             // addon ip
     html += '<div class="form-group">';
     html += '<label id="label_addon_ip'+ dem +'" for="addon_ip">Addon IP</label>';
     html += '<select id="addon_ip'+ dem +'" name="addon_ip['+ dem +']" data-id-product="'+ dem +'" class="form-control addon_ip_add" disabled>';
     html += '<option value="0" selected>None</option>'
     html += '</select>';
     html += '</div>';
      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${dem}]" class='billing_cycle' value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${dem}]" class="billing_cycle_add billing_cycle_add${dem}" value="${index}" data-id="${dem}"> ${value}
                  </label>`;
      });
      html += '</div>';
              // datacenter
     html += '<div class="form-group">';
     html += '<label id="label_datacenter" for="datacenter">Datacenter</label>';
     html += '<select id="datacenter'+ dem +'" name="datacenter['+ dem +']" class="form-control" disabled>';
     html += '<option value="0" selected>None</option>'
     html += '</select>';
     html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ dem +'"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label>Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input" data-id="'+ dem +'" id="checkboxAmount'+ dem +'" value="1">';
      html += '<label for="checkboxAmount'+ dem +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ dem +'" name="amount['+ dem +']" value="0" class="form-control">';
      html += '</div>';
      // tu ngay
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation'+ dem +'').html(html);

      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    function loadAddProductDomain(id) {
      let html = '<div class="info_product_domain">';
      html += '<div class="form-group">';
      html += '<label for="domain">Domain</label>';
      html += '<input type="text" id="domain'+ id +'" name="domain['+ id +']" value="" class="form-control" required disabled>';
      html += '</div>';
      // html += '<div class="form-group">';
      // html += '<label for="qtt">Số lượng</label>';
      html += '<input type="hidden" id="qtt'+ id +'" name="qtt['+ id +']" value="1" class="form-control" required disabled>';
      // html += '</div>';

      // billing_cycle
      html += '<div class="form-group">';
      html += '<label class=" mr-4" for="billing_cycle">Thời gian thuê</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="billing_cycle[${id}]" class='billing_cycle' value="${index}" data-id="${id}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price / billing_cycle
      html += '<div class="form-group">';
      html += '<label class="mr-4">Chi phí theo</label>';
      $.each(billing_cycle, function (index, value) {
         html += `<label class="radio-inline mr-4">
                    <input type="radio" name="price_billing_cycle[${id}]" class='billing_cycle_add billing_cycle_add${id}' value="${index}" data-id="${id}"> ${value}
                  </label>`;
      });
      html += '</div>';
      // price
      html += '<div class="form-group">';
      html += '<b>Chi phí: <span class="ml-4" id="textAmount'+ id +'"></span></b>';
      html += '</div>';
      html += '<div class="form-group row">';
      html += '<div class="col-md-1">';
      html += '<label>Giá áp tay</label>';
      html += '</div>';
      html += '<div class="col-md-10 icheck-primary d-inline">';
      html += '<input type="checkbox" class="custom-control-input checkboxAmount" data-id="'+ id +'" id="checkboxAmount'+ id +'" value="1">';
      html += '<label for="checkboxAmount'+ id +'"></label>';
      html += '</div>';
      html += '<input type="hidden" id="amount'+ id +'" name="amount['+ id +']" value="0" class="form-control">';
      html += '</div>';
      // từ ngày đến ngày
      html += '<div class="form-group">';
      html += '<label for="start_date">Từ ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="start_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="end_date">Đến ngày</label>';
      html += `<div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="far fa-calendar-alt"></i>
                  </span>
                </div>
                <input type="text" name="end_date[${dem}]" class="form-control float-right reservation">
              </div>`;
      html += '</div>';
      html += '</div>';
      $('.info_quotation'+ id +'').html(html);
      $('.reservation').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
      });
    }

    $(document).on('change', '#product', function () {
      get_product();
    })

    $(document).on('change', '.billing_cycle_product', function () {
      get_product();
    })

    $(document).on('keyup', '#addon_cpu', function() {
      get_product();
    })

    $(document).on('keyup', '#addon_ram', function() {
      get_product();
    })

    $(document).on('keyup', '#qtt', function() {
      get_product();
    })

    $(document).on('keyup', '#addon_disk', function() {
      get_product();
    })
    
    $(document).on('change', '#addon_ram_server', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk2', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk3', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk4', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk5', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk6', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk7', function() {
      get_product();
    })

    $(document).on('change', '#addon_disk8', function() {
      get_product();
    })

    $(document).on('change', '#addon_ip', function() {
      get_product();
    })

    function get_product() {
        var product_id = $('#product').val();
        var billing_cycle = $('.billing_cycle_product:checked');
        if ( billing_cycle.length == 0 ) {
          billing_cycle = 'monthly';
        } else {
          billing_cycle = billing_cycle.val();
        }
        var addon_cpu = $('#addon_cpu').val();
        var id_addon_cpu = $('#addon_cpu').attr('data-id');
        var addon_ram = $('#addon_ram').val();
        var id_addon_ram = $('#addon_ram').attr('data-id');
        var addon_disk = $('#addon_disk').val();
        var id_addon_disk = $('#addon_disk').attr('data-id');

        var addon_ram_server = $('#addon_ram_server').val();
        var addon_disk2 = $('#addon_disk2').val();
        var addon_disk3 = $('#addon_disk3').val();
        var addon_disk4 = $('#addon_disk4').val();
        var addon_disk5 = $('#addon_disk5').val();
        var addon_disk6 = $('#addon_disk6').val();
        var addon_disk7 = $('#addon_disk7').val();
        var addon_disk8 = $('#addon_disk8').val();
        var addon_ip = $('#addon_ip').val();
        

        var qtt = $('#qtt').val();
        $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_product",
            data: {
              id: product_id, billing_cycle: billing_cycle, addon_cpu: addon_cpu, id_addon_cpu: id_addon_cpu, 
              addon_ram: addon_ram, id_addon_ram: id_addon_ram, addon_disk: addon_disk, id_addon_disk: id_addon_disk,
              addon_disk2: addon_disk2, addon_disk3: addon_disk3, addon_disk4: addon_disk4, addon_disk5: addon_disk5,
              addon_disk6: addon_disk6, addon_disk7: addon_disk7, addon_disk8: addon_disk8, addon_ip: addon_ip,
              addon_ram_server: addon_ram_server
            },
            dataType: "json",
            success: function (data) {
              console.log(data.datacenter);
              if(data == null){
                $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
              }
              else if (data.type_product == 'VPS' || data.type_product == 'NAT-VPS'
                  || data.type_product == 'VPS-US' ) {
                    $('#textAmount').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                    $('#amount').val(data.amount * parseInt(qtt));
                    $('#qtt_ip').attr('disabled', false);
                    $('#addon_cpu').attr('disabled', false);
                    $('#addon_ram').attr('disabled', false);
                    $('#addon_disk').attr('disabled', false);
                    $('#qtt').attr('disabled', false);
                    $('#os').attr('disabled', false);
                    $('#amount').attr('disabled', false);
                }else if(data.type_product == "Colocation"){
                  $('#textAmount').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount').val(data.amount * parseInt(qtt));
                  $('#qtt_ip').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);
                  $("#datacenter").attr('disabled', false);
                  var html_datacenter= '';
                  html_datacenter += '<option value="0" selected>None</option>';
                  $.each(data.datacenter , function (index, datacenter) {
                    html_datacenter += '<option value="'+ datacenter.datacenter +'">'+ datacenter.datacenter + '</option>';
                  })   
                  $('#datacenter').html(html_datacenter); 
                }
                else if ( data.type_product == 'Hosting'  ) {
                  $('#textAmount').text(addCommas(data.amount) + ' VNĐ');
                  $('#amount').val(data.amount);
                  $('#qtt').attr('disabled', false);
                  $('#domain').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                }
                else if ( data.type_product == 'Server' ) {
                  // console.log(data.product.second);
                  $('#textAmount').text(addCommas(data.product.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount').val(data.product.amount * parseInt(qtt));
                  $('#qtt_ip').attr('disabled', false);
                  $('#addon_ram_server').attr('disabled', false);
                  $('#qtt').attr('disabled', false);
                  $('#os').attr('disabled', false);
                  $('#amount').attr('disabled', false);
                  if ( data.product.second ) {
                    $('#addon_disk2').attr('disabled', true);
                  } else {
                    $('#addon_disk2').attr('disabled', false);
                  }
                  $('#addon_disk3').attr('disabled', false);
                  $('#addon_disk4').attr('disabled', false);
                  $('#addon_disk5').attr('disabled', false);
                  $('#addon_disk6').attr('disabled', false);
                  $('#addon_disk7').attr('disabled', false);
                  $('#addon_disk8').attr('disabled', false);
                  $('#addon_ip').attr('disabled', false);
                  var html = '';
                  html += '<label id="" for="">Cấu hình sản phẩm</label>';
                  html += '<div>';
                  html += data.product.config;
                  html += '</div>';
                  $('#config_server').html(html);
                }
            },
            error: function (e) {
              console.log(e);
              $('.product').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
        });
    }

    $('#btn_add_product').on('click', function () {
      dem++;
      let html = '<div class="choose-product mt-4" id="add_product_'+ dem +'">';
      html += '<div class="card-tools">';
      html += '<button type="button" class="btn btn-tool" data-id="'+ dem +'" data-toggle="tooltip" data-placement="top" title="Xóa sản phẩm"><i class="fas fa-times"></i></button>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="type_product">Loại sản phẩm</label>';
      html += '<select name="type_product['+ dem +']" id="type_product'+ dem +'" data-id="'+ dem +'" class="form-control choose_type_product">';
      html += '<option value="" disabled selected>Chọn loại sản phẩm</option>';
      html += '<option value="vps">VPS</option><option value="vps_us">VPS US</option><option value="server">Server vật lý</option>';
      html += '<option value="colocation">Colocation</option><option value="design_webiste">Thiết kế webiste</option><option value="domain">Domain</option><option value="hosting">Hosting</option>';
      html += '</select>';
      html += '</div>';
      html += '<div class="form-group">';
      html += '<label for="product">Sản phẩm</label>';
      html += '<div class="product'+ dem +'">';
      html += '<input type="text" class="form-control" name="" value="" disabled>';
      html += '</div>';
      html += '</div>';
      html += '<div class="info_quotation'+ dem +'">';
      html += '</div>';
      html += '</div>';
      $("#add_product").append(html);
      $('[data-toggle="tooltip"]').tooltip();
    });
    
    $(document).on('click', '#checkboxAmount', function() {
      if($(this).is(':checked')) {
        $('#amount').attr('type', 'text');
      } else {
        $('#amount').attr('type', 'hidden');
      }
    })

    $(document).on('change', '.choose_add_product', function () {
       var id = $(this).attr('data-id');
       get_add_product(id);
    })

    $(document).on('change', '.billing_cycle_add', function () {
       var id = $(this).attr('data-id');
       get_add_product(id);
    })

    $(document).on('keyup', '.addon_add', function () {
       var id = $(this).attr('data-id-product');
      //  console.log(id);
       get_add_product(id);
    })
    
    $(document).on('click', '.checkboxAmount', function() {
      var id = $(this).attr('data-id');
      // console.log('#amount' + id);
      if($(this).is(':checked')) {
        $('#amount' + id).attr('type', 'text');
      } else {
        $('#amount' + id).attr('type', 'hidden');
      }
    })

    $(document).on('keyup', '.qtt_add', function() {
      var id = $(this).attr('data-id');
      get_add_product(id);
    })

    $(document).on('change', '.addon_ram_server_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk2_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk3_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk4_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk5_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk6_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk7_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_disk8_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    $(document).on('change', '.addon_ip_add', function() {
      var id = $(this).attr('data-id-product');
      get_add_product(id);
    })

    function get_add_product(id) {
        var product_id = $('#product' + id).val();
        // console.log(product_id, id);
        var billing_cycle = $('.billing_cycle_add' + id + ':checked');
        if ( billing_cycle.length == 0 ) {
          billing_cycle = 'monthly';
        } else {
          billing_cycle = billing_cycle.val();
        }
        var addon_cpu = $('#addon_cpu' + id).val();
        var id_addon_cpu = $('#addon_cpu' + id).attr('data-id');
        var addon_ram = $('#addon_ram' + id).val();
        var id_addon_ram = $('#addon_ram' + id).attr('data-id');
        var addon_disk = $('#addon_disk' + id).val();
        var id_addon_disk = $('#addon_disk' + id).attr('data-id');
        // console.log(product_id, billing_cycle, id_addon_cpu, id_addon_ram, id_addon_disk);
        var qtt = $('#qtt' + id).val();
        // server
        var addon_ram_server = $('#addon_ram_server' + id).val();
        var addon_disk2 = $('#addon_disk2' + id).val();
        var addon_disk3 = $('#addon_disk3' + id).val();
        var addon_disk4 = $('#addon_disk4' + id).val();
        var addon_disk5 = $('#addon_disk5' + id).val();
        var addon_disk6 = $('#addon_disk6' + id).val();
        var addon_disk7 = $('#addon_disk7' + id).val();
        var addon_disk8 = $('#addon_disk8' + id).val();
        var addon_ip = $('#addon_ip' + id).val();

        $.ajax({
            type: "get",
            url: "/admin/users/quotation/get_product",
            data: {
              id: product_id, billing_cycle: billing_cycle, addon_cpu: addon_cpu, id_addon_cpu: id_addon_cpu, 
              addon_ram: addon_ram, id_addon_ram: id_addon_ram, addon_disk: addon_disk, id_addon_disk: id_addon_disk,
              addon_disk2: addon_disk2, addon_disk3: addon_disk3, addon_disk4: addon_disk4, addon_disk5: addon_disk5,
              addon_disk6: addon_disk6, addon_disk7: addon_disk7, addon_disk8: addon_disk8, addon_ip: addon_ip,
              addon_ram_server: addon_ram_server
            },
            dataType: "json",
            success: function (data) {
              console.log(data);
                var html = '';``
                if(data == null){
                  $('.product'+ id +'').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
                }else if (data.type_product == 'VPS' || data.type_product == 'NAT-VPS'
                  || data.type_product == 'VPS-US' ) {
                    $('#amount'+ id +'').val(data.amount * parseInt(qtt));
                    $('#textAmount'+ id +'').text( addCommas(data.amount * parseInt(qtt)) + ' VNĐ' );
                    $('#qtt_ip'+ id +'').attr('disabled', false);
                    $('#addon_cpu'+ id +'').attr('disabled', false);
                    $('#addon_ram'+ id +'').attr('disabled', false);
                    $('#addon_disk'+ id +'').attr('disabled', false);
                    $('#qtt'+ id +'').attr('disabled', false);
                    $('#os'+ id +'').attr('disabled', false);
                    $('#amount'+ id +'').attr('disabled', false);
                }
                else if ( data.type_product == 'Hosting' ) {
                  $('#textAmount'+ id +'').text(addCommas(data.amount) + ' VNĐ');
                  $('#amount'+ id +'').val(data.amount);
                  $('#qtt'+ id +'').attr('disabled', false);
                 $('#domain'+ id +'').attr('disabled', false);
                  $('#amount'+ id +'').attr('disabled', false);
                } else if(data.type_product == 'Colocation'){     
                  $('#textAmount'+ id +'').text(addCommas(data.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount'+ id +'').val(data.amount * parseInt(qtt));
                  $('#qtt_ip'+ id +'').attr('disabled', false);
                  $('#amount'+ id +'').attr('disabled', false);
                  $('#qtt'+ id +'').attr('disabled', false);
                  $('#addon_ip'+ id +'').attr('disabled', false);
                  $("#datacenter"+ id +'').attr('disabled', false);
                  var html_datacenter= '';
                  html_datacenter += '<option value="0" selected>None</option>';
                  $.each(data.datacenter , function (index, datacenter) {
                    html_datacenter += '<option value="'+ datacenter.datacenter +'">'+ datacenter.datacenter + '</option>';
                  })   
                  $('#datacenter'+ id +'').html(html_datacenter); 
                } else if ( data.type_product == 'Server' ) {
                  // console.log(data.product.second);
                  $('#textAmount'+ id).text(addCommas(data.product.amount * parseInt(qtt)) + ' VNĐ');
                  $('#amount'+ id).val(data.product.amount * parseInt(qtt));
                  $('#qtt_ip'+ id).attr('disabled', false);
                  $('#addon_ram_server'+ id).attr('disabled', false);
                  $('#qtt'+ id).attr('disabled', false);
                  $('#os'+ id).attr('disabled', false);
                  $('#amount'+ id).attr('disabled', false);
                  if ( data.product.second ) {
                    $('#addon_disk2'+ id).attr('disabled', true);
                  } else {
                    $('#addon_disk2'+ id).attr('disabled', false);
                  }
                  $('#addon_disk3'+ id).attr('disabled', false);
                  $('#addon_disk4'+ id).attr('disabled', false);
                  $('#addon_disk5'+ id).attr('disabled', false);
                  $('#addon_disk6'+ id).attr('disabled', false);
                  $('#addon_disk7'+ id).attr('disabled', false);
                  $('#addon_disk8'+ id).attr('disabled', false);
                  $('#addon_ip'+ id).attr('disabled', false);

                  var html = '';
                  html += '<label id="" for="">Cấu hình sản phẩm</label>';
                  html += '<div>';
                  html += data.product.config;
                  html += '</div>';
                  $('#config_server'+ id).html(html);
                }
            },
            error: function (e) {
              console.log(e);
              $('.product'+ id +'').html('<span class="text-danger">Lỗi truy vấn sản phẩm</span>')
            }
        });
    }

    $('#add_user').on('click', function () {
      $('#create-user').modal('show');
      $('#btn_submit').fadeIn();
      $('#btn_finish').fadeOut();
      generatePassword();
    })

    $('#btn_submit').on('click', function () {
      var form = $('#form_create_user').serialize();

      $.ajax({
        url: '/admin/users/quotation/create_user',
        type: 'post',
        data: form,
        dataType: 'json',
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('#container_notification').html(html);
          $('#form_create_user').css('opacity', 0.5);
          $('#btn_submit').attr('disabled', true);
          $('#form_create_user').attr('disabled', true);
        },
        success: function (data) {
          // console.log(data);
          if (data.error == 0) {
            var html = '<p class="text-success">Tạo khách hàng mới thành công</p>';
            $('#container_notification').html(html);
            $('#form_create_user')[0].reset();
            $('#form_create_user').css('opacity', 1);
            $('#btn_submit').attr('disabled', false);
            $('#form_create_user').attr('disabled', false);
            let html2 = '';
            html2 += '<option value="" disabled selected>Chọn khách hàng</option>';
            $.each(data.users, function (index, user) {
             html2 += '<option value="'+ user.id +'">' + user.name + ' - ' + user.email + '</option>';
            });
            $('#user').html(html2);
          } else {
            var html = '<p class="text-danger">Tạo khách hàng thất bại</p>';
            $('#container_notification').html(html);
            $('#form_create_user').css('opacity', 1);
            $('#btn_submit').attr('disabled', false);
            $('#form_create_user').attr('disabled', false)
          }
        },
        error: function (e) {
           console.log(e);
           $('#container_notification').html('<span class="text-danger">Truy vấn thất bại.</span>');
        }
      })
    })

    function generatePassword() {
      var length = 12;
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
      retVal = "";
      for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
      }
      $('#password_confirmation').val(retVal);
      $('#password').val(retVal);
    }

    $(document).on('click', '.show_password', function () {
      $('#password').attr('type', 'text');
      $('.show_password i').removeClass('fa-eye');
      $('.show_password i').addClass('fa-eye-slash');
      $(this).addClass('hidden_password');
      $(this).removeClass('show_password');
    })

    $(document).on('click', '.hidden_password', function () {
        $('#password').attr('type', 'password');
        $('.hidden_password i').addClass('fa-eye');
        $('.hidden_password i').removeClass('fa-eye-slash');
        $(this).removeClass('hidden_password');
        $(this).addClass('show_password');
    })

    $('#enterprise').on('click', function () {
        if ( $(this).is(':checked') ) {
          var html = '';
          html += '<div class="col-2">';
          html += '<label for="imst">Mã số thuế</label>';
          html += '</div>';
          html += '<div class="col-10">';
          html += '<input type="text" name="mst" value="" class="form-control" placeholder="Mã số thuế">';
          html += '</div>';
          $('#mst').html(html);
        } else {
          $('#mst').html('');
        }
    })

    $('#btn_name_replace').on('click', function () {
      let html = '<label for="ip_name_replace">Tên thay thế (Họ và tên hoặc tên công ty)</label>';
      html += '<input type="text" id="ip_name_replace" name="name_replace" value="" class="form-control" placeholder="Tên thay thế (Họ và tên hoặc tên công ty)">';
      $(this).fadeOut();
      $('#name_replace').html(html);
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

})
