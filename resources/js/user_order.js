$(document).ready(function() {
    // var pricing = $('#billing').attr('data-pricing');
    // console.log($('.chosse_account:checked').val());
    if ( $('.chosse_account:checked').val() == 'random' ) {
        $('#username').attr('disabled', true);
        $('#password').attr('disabled', true);
    } else {
        // console.log("da den");
        $('#username').attr('disabled', false);
        $('#password').attr('disabled', false);
    }
    var pricing = $('#billing-price').attr('data-pricing');
    var qtt = $('#qtt').val();
    var type_product = $('#type_product').val();
    var addon = $('#get-addon').val();
    if (typeof(pricing) === 'undefined') {
        pricing = $('#one_time_pay').val();
    }
    if (addon !== null) {
        loadAddon(addon);
    } //ẩn các dòng add on trong bảng tóm tắt đơn hàng khi không chọn
    $('tr.addon-item-cpu').hide();
    $('tr.addon-item-ram').hide();
    $('tr.addon-item-disk').hide();
    // console.log(pricing);
    var sub_total = pricing;
    printf_order(sub_total, qtt);

    function printf_order(sub_total, qtt) {
        $('td.item_pricing_cell').text(addCommas(pricing) + " VNĐ");
        $('#sub_total').val(sub_total);
        total = sub_total * qtt;
        if (type_product == 'Hosting' || type_product == 'Hosting-Singapore') {
            var html = '';
            if (qtt > 1) {
                for (let index = 1; index < qtt; index++) {
                    html += '<div class="input-domain">';
                    html += '<input type="text" name="domain[]" placeholder="Domain" class="form-control" id="name_domain_' + index + '">';
                    html += '</div>';
                }
            } else {
                $('.quantity-order').html('');
            }
            $('.domain').append(html);
            $('.sub_total_pricing').html(addCommas(sub_total) + ' VNĐ');
            $('td.item_pricing_cell').text(addCommas(pricing) + " VNĐ");
            $('.total_pricing').html(addCommas(total) + ' VNĐ');
            $('.quantity-printf').html(qtt);
        } else {
            $('.sub_total_pricing').html(addCommas(sub_total) + ' VNĐ');
            $('.total_pricing').html(addCommas(total) + ' VNĐ');
            $('.quantity-printf').html(qtt);
        }
    }

    $('#qtt').on('keyup', function() {
        var qtt = $(this).val();
        var type_product = $('#type_product').val();
        var sub_total = $('#sub_total').val();
        if (type_product == 'Hosting' || type_product == 'Hosting-Singapore') {
            var html = '';
            var html_order_quantity = '';
            if (qtt > 1) {
                for (let index = 0; index < qtt; index++) {
                    html += '<div class="input-domain">';
                    html += '<input type="text" name="domain[]" placeholder="Domain ' + index + '" class="form-control name_domain">';
                    html += '</div>';
                }
                $('.domain').html(html);
            } else {
                $('.quantity-order').html('');
                html += '<div class="input-domain">';
                html += '<input type="text" name="domain[]" placeholder="Domain" class="form-control name_domain">';
                html += '</div>';
                $('.domain').html(html);
            }
            total = sub_total * qtt;
            var addon = $('#addon-hidden').val();
            if (addon) {
                var qtt_addon_database = parseInt($(".addon-database input").val());
                var pricing_database = parseInt($(".addon-database input").attr('data-pricing'));
                var qtt_addon_storage = parseInt($(".addon-storage input").val());
                var pricing_storage = parseInt($(".addon-storage input").attr('data-pricing'));
                total = sub_total * qtt + (qtt_addon_storage * pricing_storage * qtt) + (qtt_addon_database * pricing_database * qtt);
            }
            $('td.item_pricing_cell').text(addCommas(total) + " VNĐ");
            $('.total_pricing').html(addCommas(total) + ' VNĐ');
            $('.quantity-printf').html(qtt);
            $('#total_order').val(total);
            var value_promotion = $('#promotion_order').val();
            var type_promotion = $('#promotion_order').attr('data-promotion');
            if (type_promotion != '') {
                var total = parseInt($('#total_order').val());
                if (type_promotion == 'percentage') {
                    var total = total - (total * value_promotion / 100);
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                } else if (type_promotion == 'fixed_amount') {
                    var total = (total) - value_promotion;
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                } else if (type_promotion == 'free') {
                    $('.total_pricing').html(' 0 VNĐ');
                    // $('tr.detail-promotion').show();
                }
            }
        } else if (type_product == 'VPS') {
            total = sub_total * qtt;
            var addon = $('#addon-hidden').val();
            if (addon) {
                var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
                var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
                var qtt_addon_ram = parseInt($(".addon-ram input").val());
                var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
                var qtt_addon_disk = parseInt($(".addon-disk input").val());
                var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
                // console.log('2- ', qtt_addon_cpu, pricing_cpu, qtt_addon_ram, pricing_ram, qtt_addon_disk, pricing_disk);
                total = sub_total * qtt + (qtt_addon_cpu * pricing_cpu * qtt) + (qtt_addon_ram * pricing_ram * qtt) + (qtt_addon_disk * pricing_disk * qtt) / 10;
            }


            //Reset lại các dòng addon
            loadAddon(addon);
            total_in_table = sub_total * qtt; //Set lại ô tính tổng tiền trong bảng
            $('.quantity-printf').html(qtt);
            $('td.item_pricing_cell').text(addCommas(total_in_table) + " VNĐ");
            $('.total_pricing').html(addCommas(total_in_table) + ' VNĐ');
            $('#addon-vps').attr('disabled', false);
            $('#addon-hosting').attr('disabled', false);
            $('#addon-server').attr('disabled', false);
            $('.addon-item-database').html('');
            $('.addon-item-storage').html('');
            $('.addon-item-disk').html('');
            $('.addon-item-ram').html('');
            $('.addon-item-cpu').html('');
            $('#total_order').val(total);
            var value_promotion = $('#promotion_order').val();
            var type_promotion = $('#promotion_order').attr('data-promotion');
            if (type_promotion != '') {
                var total = parseInt($('#total_order').val());
                if (type_promotion == 'percentage') {
                    var total = total - (total * value_promotion / 100);
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                } else if (type_promotion == 'fixed_amount') {
                    var total = (total) - value_promotion;
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                } else if (type_promotion == 'free') {
                    $('.total_pricing').html(' 0 VNĐ');
                    // $('tr.detail-promotion').show();
                }
            }
        } else {
            loadAddon(addon);
            total = sub_total * qtt;
            $('td.item_pricing_cell').text(addCommas(total) + " VNĐ");
            $('.total_pricing').html(addCommas(total) + ' VNĐ');
            $('.quantity-printf').html(qtt);
            $('.addon-item-database').html('');
            $('.addon-item-storage').html('');
            $('.addon-item-disk').html('');
            $('.addon-item-ram').html('');
            $('.addon-item-cpu').html('');
        }

    });

    $('#billing').on('change', function() {
        var id_product = $('#product_id').val();
        var qtt = $('#qtt').val();
        var billing = $(this).val();
        var addon = null;
        // console.log(addon);
        $.ajax({
            type: "get",
            url: "/order/check_billing",
            data: { id: id_product, addon: addon },
            dataType: "json",
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.before-send').css('opacity', '0.5');
                $('.before-send').html(html);
            },
            success: function(data) {
                var sub_total = 0;
                if (data.pricing.type == 'free') {
                    $('#sub_total').val(sub_total);
                    $('.item_billing').text('Free');
                    $('.item_pricing').text(addCommas(sub_total) + ' VNĐ');
                } else if (data.pricing.type == 'one_time') {
                    sub_total = data.pricing.one_time_pay;
                    $('#sub_total').val(sub_total);
                    $('.item_billing').text('Vĩnh viễn');
                    $('.item_pricing').text(addCommas(sub_total) + ' VNĐ');
                } else {
                    sub_total = data.pricing[billing];
                    $('#sub_total').val(sub_total);
                    $('.item_billing').text(data.billing[billing]);
                    $('.item_pricing').text(addCommas(sub_total) + ' VNĐ');
                }
                $('.itemtitle').html(data.product.name + '-' + data.group_product.name);
                var total = sub_total * qtt;
                $('#total_order').val(total);
                // console.log(sub_total, qtt ,data);
                $('.sub_total_pricing').text(addCommas(sub_total) + ' VNĐ');
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
                $('.before-send').css('opacity', '1');
                $('.before-send').html('');
                var ma_km = $('#promotion').val();
                if (ma_km != '') {
                    $('.detail-promotion').html('');
                    $('.notification_promotion').html('<span class="text-danger">Vui lòng kiểm tra lại mã khuyến mãi.</span>');
                }
                // $('.screen-addon').html('');
                //Reset lại bảng tính tinh
                loadAddon(addon);
                total_in_table = sub_total * qtt; //Set lại ô tính tổng tiền trong bảng
                $('.item_pricing_cell').html(addCommas(total_in_table) + ' VNĐ');
                $('#addon-vps').attr('disabled', false);
                $('#addon-hosting').attr('disabled', false);
                $('#addon-server').attr('disabled', false);
                $('.addon-item-database').html('');
                $('.addon-item-storage').html('');
                $('.addon-item-disk').html('');
                $('.addon-item-ram').html('');
                $('.addon-item-cpu').html('');
            },
            error: function(e) {
                console.log(e);
                $('.before-send').css('opacity', '1');
                $('.before-send').html('Đã có lỗi xảy ra. Vui lòng thông báo cho quản trị viên để tránh đặt hàng thất bại');
            }
        });
    });

    $('#addon-vps').on('click', function() {
        loadAddon('addon-vps');
        $(this).attr('disabled', true);
    });

    $('#addon-hosting').on('click', function() {
        loadAddon('addon-hosting');
        $(this).attr('disabled', true);
    });

    $('#addon-server').on('click', function() {
        loadAddon('addon-server');
        $(this).attr('disabled', true);
    });

    function loadAddon(type_addon) {
        var billing = $('#billing').val();
        var url_string = window.location.href;
        var url = new URL(url_string);
        var addon_cpu = url.searchParams.get('addon_cpu');
        var addon_ram = url.searchParams.get('addon_ram');
        var addon_disk = url.searchParams.get('addon_disk');
        if (!addon_cpu || !$.isNumeric(addon_cpu)) {
            addon_cpu = 0;
        }
        if (!addon_ram || !$.isNumeric(addon_ram)) {
            addon_ram = 0;
        }
        if (!addon_disk || !$.isNumeric(addon_disk)) {
            addon_disk = 0;
        }
        $.ajax({
            url: '/loadAddon',
            data: { type: type_addon },
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.screen-addon').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html = '';
                if (data != '' || data != null) {
                    html += '<div class="form-group addon-product mt-4 mb-4">';
                    html += '<input name="addon" value="1" id="addon-hidden" type="hidden">';
                    html += '<label for="addon">Chọn gói Addon</label>';
                    html += '<div class="row rounded add-on-vps">'
                    $.each(data, function(index, product) {
                        var type = '';
                        var label = '';
                        // console.log(product, product.meta_product.type_addon);
                        if (product.meta_product.type_addon == 'addon_cpu') {
                            type = 'cpu';
                            label = 'CPU';
                            html += '<input name="id-addon-cpu" value="' + product.id + '" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_ram') {
                            type = 'ram';
                            label = 'Ram';
                            html += '<input name="id-addon-ram" value="' + product.id + '" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_disk') {
                            type = 'disk';
                            label = 'Disk';
                            html += '<input name="id-addon-disk" value="' + product.id + '" id="addon-hidden" type="hidden">';
                        }
                        // if (product.meta_product.type_addon != '') {
                        //     type = 'storage';
                        //     label = 'Storage';
                        //       html += '<input name="id-addon-storage" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        // }
                        // if (product.meta_product.database != '') {
                        //     type = 'database';
                        //     label = 'Database';
                        //       html += '<input name="id-addon-database" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        // }
                        if (type == 'disk') {
                            html += '<div class="col-md-4 text-center addon-' + type + '">';
                            html += '<label>' + label + ' (' + addCommas(product.pricing.monthly) + '/tháng)</lable> <br>';
                            html += '<input name="addon_' + type + '" type="number" value="' + addon_disk + '" min="0" max="200" step="10" data-name="' + product.name + '" data-pricing="' + product.pricing[billing] + '" />';
                            html += '</div>';
                        } else {
                            html += '<div class="col-md-4 text-center addon-' + type + '">';
                            html += '<label>' + label + ' (' + addCommas(product.pricing.monthly) + '/tháng)</lable> <br>';
                            if (type == 'cpu') {
                                html += '<input name="addon_' + type + '" type="number" value="' + addon_cpu + '" min="0" max="16" step="1" data-name="' + product.name + '" data-pricing="' + product.pricing[billing] + '" />';
                            } else {
                                html += '<input name="addon_' + type + '" type="number" value="' + addon_ram + '" min="0" max="16" step="1" data-name="' + product.name + '" data-pricing="' + product.pricing[billing] + '" />';
                            }
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '</div>';
                    $('.screen-addon').html(html);
                    $('.select3').select2();
                    $("input[type='number']").inputSpinner();
                    if (addon_cpu > 0) {
                        var qtt_addon = parseInt(addon_cpu);
                        var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
                        var sub_total = parseInt($('#sub_total').val());
                        var qtt_product = parseInt($('#qtt').val());
                        var name_addon = $(".addon-cpu input").attr('data-name');
                        // lay cac addon con lai
                        var qtt_addon_ram = parseInt($(".addon-ram input").val());
                        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
                        var qtt_addon_disk = parseInt($(".addon-disk input").val());
                        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
                        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk / 10);
                        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product) + (qtt_addon_disk * pricing_disk / 10 * qtt_product);
                        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
                        $('.total_pricing').text(addCommas(total) + ' VNĐ');
                        $('#total_order').val(total);

                        var table = '';
                        table += '<td>CPU Addon</td>';
                        table += '<td>' + qtt_addon + ' cores x' + qtt_product + '</td>';
                        table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
                        $('tr.addon-item-cpu').html(table);
                        $('tr.addon-item-cpu').show();
                    }
                    if (addon_ram > 0) {
                        var qtt_addon = parseInt(addon_ram);
                        var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
                        var sub_total = parseInt($('#sub_total').val());
                        var qtt_product = parseInt($('#qtt').val());
                        var name_addon = $('.addon-ram input').attr('data-name');
                        // lay cac addon con load
                        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
                        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
                        var qtt_addon_disk = parseInt($(".addon-disk input").val());
                        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));

                        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk * pricing_disk / 10);
                        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_disk * pricing_disk / 10 * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product);
                        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
                        $('.total_pricing').text(addCommas(total) + ' VNĐ');
                        $('#total_order').val(total);

                        var table = '';
                        table += '<td>RAM Addon</td>';
                        table += '<td>' + qtt_addon + 'Gb x' + qtt_product + ' </td>';
                        table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
                        $('tr.addon-item-ram').html(table);
                        $('tr.addon-item-ram').show();
                    }
                    if (addon_disk > 0) {
                        var qtt_addon = parseInt(addon_disk) / 10;
                        var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
                        var sub_total = parseInt($('#sub_total').val());
                        var qtt_product = parseInt($('#qtt').val());
                        var name_addon = $('.addon-disk input').attr('data-name');
                        // lay cac addon con lai
                        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
                        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
                        var qtt_addon_ram = parseInt($(".addon-ram input").val());
                        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));

                        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
                        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product);

                        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
                        $('.total_pricing').text(addCommas(total) + ' VNĐ');
                        $('#total_order').val(total);

                        var table = '';
                        table += '<td>DISK Addon</td>';
                        table += '<td>' + qtt_addon * 10 + 'Gb x' + qtt_product + '</td>';
                        table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
                        $('tr.addon-item-disk').html(table);
                        $('tr.addon-item-disk').show();
                    }
                } else {
                    $('.screen-addon').html('Không có gói Addon Product cho gói sản phẩm này.');
                }
            },
            error: function(e) {
                console.log(e);
                $('.screen-addon').html('Truy vấn Addon Product thất bại.');
            }
        });
    }

    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('change', '.addon-cpu', function() {
        var qtt_addon = parseInt($(".addon-cpu input[name='addon_cpu']").val());
        var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#qtt').val());
        var name_addon = $(".addon-cpu input").attr('data-name');
        // lay cac addon con lai
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk / 10);
        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product) + (qtt_addon_disk * pricing_disk / 10 * qtt_product);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        $('#total_order').val(total);
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' cores';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-cpu').html(html);
        //thêm dòng hiển thị add on cpu vào bảng tóm tắt đơn hàng
        if (qtt_addon > 0) {
            var table = '';
            table += '<td>CPU Addon</td>';
            table += '<td>' + qtt_addon + ' cores x' + qtt_product + '</td>';
            table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
            $('tr.addon-item-cpu').html(table);
            $('tr.addon-item-cpu').show();
        } else {
            $('tr.addon-item-cpu').hide();
        }
        var value_promotion = $('#promotion_order').val();
        var type_promotion = $('#promotion_order').attr('data-promotion');
        if (type_promotion != '') {
            var total = parseInt($('#total_order').val());
            if (type_promotion == 'percentage') {
                var total = total - (total * value_promotion / 100);
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'fixed_amount') {
                var total = (total) - value_promotion;
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'free') {
                $('.total_pricing').html(' 0 VNĐ');
                // $('tr.detail-promotion').show();
            }
        }
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('change', '.addon-ram', function() {
        var qtt_addon = parseInt($('.addon-ram input').val());
        var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#qtt').val());
        var name_addon = $('.addon-ram input').attr('data-name');
        // lay cac addon con load
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk * pricing_disk / 10);
        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_disk * pricing_disk / 10 * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        $('#total_order').val(total);
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-ram').html(html);
        //thêm dòng hiển thị add on ram vào bảng tóm tắt đơn hàng
        if (qtt_addon > 0) {
            var table = '';
            table += '<td>RAM Addon</td>';
            table += '<td>' + qtt_addon + 'Gb x' + qtt_product + ' </td>';
            table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
            $('tr.addon-item-ram').html(table);
            $('tr.addon-item-ram').show();
        } else {
            $('tr.addon-item-ram').hide();
        }
        var value_promotion = $('#promotion_order').val();
        var type_promotion = $('#promotion_order').attr('data-promotion');
        if (type_promotion != '') {
            var total = parseInt($('#total_order').val());
            if (type_promotion == 'percentage') {
                var total = total - (total * value_promotion / 100);
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'fixed_amount') {
                var total = (total) - value_promotion;
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'free') {
                $('.total_pricing').html(' 0 VNĐ');
                // $('tr.detail-promotion').show();
            }
        }
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('change', '.addon-disk', function() {
        var qtt_addon = parseInt($('.addon-disk input').val()) / 10;
        var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#qtt').val());
        var name_addon = $('.addon-disk input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        $('#total_order').val(total);
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon * 10 + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
        //thêm dòng hiển thị add on disk vào bảng tóm tắt đơn hàng
        if (qtt_addon > 0) {
            var table = '';
            table += '<td>DISK Addon</td>';
            table += '<td>' + qtt_addon * 10 + 'Gb x' + qtt_product + '</td>';
            table += '<td class="item_pricing text-right">' + addCommas(pricing * qtt_addon * qtt_product) + ' VNĐ</td>';
            $('tr.addon-item-disk').html(table);
            $('tr.addon-item-disk').show();
        } else {
            $('tr.addon-item-disk').hide();
        }
        var value_promotion = $('#promotion_order').val();
        var type_promotion = $('#promotion_order').attr('data-promotion');
        if (type_promotion != '') {
            var total = parseInt($('#total_order').val());
            if (type_promotion == 'percentage') {
                var total = total - (total * value_promotion / 100);
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'fixed_amount') {
                var total = (total) - value_promotion;
                $('.total_pricing').html(addCommas(total) + ' VNĐ');
            } else if (type_promotion == 'free') {
                $('.total_pricing').html(' 0 VNĐ');
                // $('tr.detail-promotion').show();
            }
        }
    });

    $(document).on('click', '.addon-database', function() {
        var qtt_addon = parseInt($('.addon-database input').val());
        var pricing = parseInt($('.addon-database input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#qtt').val());
        var name_addon = $('.addon-database input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_storage = parseInt($(".addon-storage input").val());
        var pricing_storage = parseInt($(".addon-storage input").attr('data-pricing'));


        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_storage * pricing_storage);
        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_storage * pricing_storage * qtt_product);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' database';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-storage').html(html);
        // console.log(total);
    });

    $(document).on('click', '.addon-storage', function() {
        var qtt_addon = parseInt($('.addon-storage input').val());
        var pricing = parseInt($('.addon-storage input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#qtt').val());
        var name_addon = $('.addon-storage input').attr('data-name');
        // Lay cac addon con lai
        var qtt_addon_database = parseInt($(".addon-database input").val());
        var pricing_database = parseInt($(".addon-database input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_database * pricing_database);
        total = sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_database * pricing_database * qtt_product);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
    });

    $(document).on('change', '#qtt_addon', function() {
        var qtt_addon = $(this).val();
        var addon_name = $('#billing_addon_name').val();
        var addon_price = parseInt($('#billing_addon_value').val());
        var sub_total = parseInt($('#sub_total').val());
        var qtt = $('#qtt').val();
        // console.log(addon_name, addon_price);
        sub_total = (sub_total - addon_price) + (addon_price * qtt_addon);

        $('.itemtitle_addon').text(addon_name + ' x ' + qtt_addon);
        $('.item_pricing_addon').text(addCommas(qtt_addon * addon_price) + ' VNĐ');
        $('.sub_total_pricing').text(addCommas(sub_total) + ' VNĐ');
        var total = sub_total * qtt;
        $('.total_pricing').html(addCommas(total) + ' VNĐ');
        $('#total_order').val(total);
    });

    $('.check_promotion').on('click', function() {
        var type_order = $(this).attr('data-order');
        var ma_km = $('#promotion').val();
        var product_id = $('#product_id').val();
        var billing = $('#billing').val();
        // console.log(product_id, billing);
        if (ma_km != '') {
            $.ajax({
                url: '/order/kiem-tra-ma-khuyen-mai',
                data: { type_order: type_order, ma_km: ma_km, product_id: product_id, billing: billing },
                dataType: 'json',
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.notification_promotion').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.error == 0) {
                        $('.notification_promotion').html('<span class="text-success">Kiểm tra mã khuyến mãi thành công.</span>');
                        if (data.type == 'percentage') {
                            var sub_total = parseInt($('#total_order').val());
                            var total = sub_total - (sub_total * data.value / 100);
                            $('.total_pricing').html(addCommas(total) + ' VNĐ');
                            var html = '';
                            html += '<td>Mã khuyến mãi</td>';
                            html += '<td>Giảm</td>';
                            html += '<td class="item_pricing text-right">' + data.value + ' %' + '</td>';
                            $('.detail-promotion').html(html);
                            $('#promotion_order').val(data.value);
                            $('#promotion_order').attr('data-promotion', data.type);
                        } else if (data.type == 'fixed_amount') {
                            var sub_total = parseInt($('#total_order').val());
                            var total = (sub_total) - data.value;
                            $('.total_pricing').html(addCommas(total) + ' VNĐ');
                            var html = '';
                            html += '<td>Mã khuyến mãi</td>';
                            html += '<td>Giảm</td>';
                            html += '<td class="item_pricing text-right">' + addCommas(data.value) + ' VNĐ' + '</td>';
                            $('.detail-promotion').html(html);
                            $('#promotion_order').val(data.value);
                            $('#promotion_order').attr('data-promotion', data.type);
                        } else if (data.type == 'free') {
                            $('.total_pricing').html(' 0 VNĐ');
                            var html = '';
                            html += '<td>Mã khuyến mãi</td>';
                            html += '<td></td>';
                            html += '<td class="item_pricing text-right">Miễn phí</td>';
                            $('tr.detail-promotion').html(html);
                            $('#promotion_order').val(data.value);
                            $('#promotion_order').attr('data-promotion', data.type);
                            // $('tr.detail-promotion').show();
                        }
                    } else if (data.error == 1) {
                        $('.notification_promotion').html('<span class="text-danger">Mã khuyến mãi đã hết hạn.</span>');
                        $('tr.detail-promotion').html('');
                    } else if (data.error == 2) {
                        $('tr.detail-promotion').html('');
                        $('.notification_promotion').html('<span class="text-danger">Mã khuyến mãi chỉ sử dung một lần. Bạn đã sử dụng mã khuyến mãi.</span>');
                    } else if (data.error == 3) {
                        $('.notification_promotion').html('<span class="text-danger">Mã khuyến mãi đã hết hạn sữ dụng.</span>');
                        $('tr.detail-promotion').html('');
                    } else {
                        $('tr.detail-promotion').html('');
                        $('.notification_promotion').html('<span class="text-danger">Mã khuyến mãi không có thật.</span>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('tr.detail-promotion').html('');
                    $('.notification_promotion').html('<span class="text-danger">Truy vấn mã khuyến mãi lỗi!</span>');
                }
            })
        } else {
            $('.notification_promotion').html('<span class="text-danger">Vui lòng nhập mã khuyến mãi</span>');
        }
    })

    $('.add_customer').on('click', function() {
        $('#add_customer').modal('show');
        $('#button_add_customer').fadeIn();
        $('#finish_add_customer').fadeOut();
        $('#form_add_customer')[0].reset();
        $('.radio_personal').fadeIn();
        $('.radio_organization').hide();
    })

    $('#radioPrimary1').on('click', function() {
        $('.radio_personal').fadeIn();
        $('.radio_organization').hide();
    });

    $('#radioPrimary2').on('click', function() {
        $('.radio_personal').hide();
        $('.radio_organization').fadeIn();
    });

    $('#button_add_customer').on('click', function() {
        var form = $('#form_add_customer').serialize();
        var validate = validate_personal();
        //   console.log(validate);
        if (!validate) {
            $.ajax({
                url: '/order/them_thong_tin_khach_hang',
                data: form,
                type: 'post',
                dataType: 'json',
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#notication-product').html(html);
                    $('#button_add_customer').attr('disabled', true);
                    $('#form_add_customer').attr('disabled', true);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.error == 0) {
                        var html_customer = '';
                        $('#notication-product').html('<span class="text-success">Tạo thông tin khách hàng thành công.</span>');
                        $('#button_add_customer').attr('disabled', false);
                        $('#button_add_customer').fadeOut();
                        $('#finish_add_customer').fadeIn();
                        $('#form_add_customer').attr('disabled', false);


                        html_customer += '<option value="" disabled>---Chọn khách hàng---</option>';
                        html_customer += '<option value="0">Cho tôi</option>';
                        $.each(data.customer, function(index, customer) {
                            html_customer += '<option value="' + customer.ma_customer + '">';
                            if (customer.customer_name != '') {
                                html_customer += customer.customer_name;
                            } else {
                                html_customer += customer.customer_tc_name;
                            }
                            html_customer += '</option>';
                        })
                        $('#makh').html(html_customer);
                    } else {
                        $('#notication-product').html('<span class="text-danger">Tạo thông tin khách hàng thất bại.</span>');
                        $('#button_add_customer').attr('disabled', false);
                        $('#form_add_customer').attr('disabled', false);
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('#notication-product').html('<span class="text-danger">Truy vấn thông tin khách hàng lỗi!</span>');
                    $('#button_add_customer').attr('disabled', false);
                    $('#form_add_customer').attr('disabled', false);
                }
            })
        }
    })

    function validate_personal() {
        var name = $('#personal_name').val();
        if (name == null || name == '') {
            $('#notication-product').html('<span class="text-danger">Họ và tên người đăng ký không được để trống.</span>');
            return true;
        }
        return false;
    }

    function validate_enterprise() {
        var customer_tc_name = $('#customer_tc_name').val();
        var customer_tc_mst = $('#customer_tc_mst').val();
        var customer_tc_sdt = $('#customer_tc_sdt').val();
        var customer_tc_dctc = $('#customer_tc_dctc').val();
        var customer_tc_city = $('#customer_tc_city').val();

        var customer_dk_name = $('#customer_dk_name').val();
        var customer_dk_gender = $('#customer_dk_gender').val();
        var customer_dk_cv = $('#customer_dk_cv').val();
        var customer_dk_date = $('#customer_dk_date').val();
        var customer_dk_cmnd = $('#customer_dk_cmnd').val();
        var customer_dk_phone = $('#customer_dk_phone').val();

        var customer_dd_name = $('#customer_dd_name').val();
        var customer_dd_cv = $('#customer_dd_cv').val();
        var customer_dd_gender = $('#customer_dd_gender').val();
        var customer_dd_date = $('#customer_dd_date').val();
        var customer_dd_cmnd = $('#customer_dd_cmnd').val();
        var customer_dd_email = $('#customer_dd_email').val();

        var customer_tt_name = $('#customer_tt_name').val();
        var customer_tt_gender = $('#customer_tt_gender').val();
        var customer_tt_date = $('#customer_tt_date').val();
        var customer_tt_cmnd = $('#customer_tt_cmnd').val();
        var customer_tt_email = $('#customer_tt_email').val();

        if (customer_tc_name == null || customer_tc_name == '') {
            $('.error_customer_tc_name').html('<span class="text-danger">Tên tổ chức đăng ký không được để trống.</span>');
            return true;
        } else if (customer_tc_mst == null || customer_tc_mst == '') {
            $('.error_customer_tc_mst').html('<span class="text-danger">Mã số thuế không được để trống.</span>');
            return true;
        } else if (customer_tc_sdt == null || customer_tc_sdt == '') {
            $('.error_customer_tc_sdt').html('<span class="text-danger">Số điện thoại của tổ chức không được để trống.</span>');
            return true;
        } else if (customer_tc_dctc == null || customer_tc_dctc == '') {
            $('.error_customer_tc_dctc').html('<span class="text-danger">Địa chỉ của tổ chức không được để trống.</span>');
            return true;
        } else if (customer_tc_city == null || customer_tc_city == '') {
            $('.error_customer_tc_city').html('<span class="text-danger">Thành phố của tổ chức không được để trống.</span>');
            return true;
        } else if (customer_dk_name == null || customer_dk_name == '') {
            $('.error_customer_dk_name').html('<span class="text-danger">Họ và tên người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dk_gender == null || customer_dk_gender == '') {
            $('.error_customer_dk_gender').html('<span class="text-danger">Giới tính người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dk_cv == null || customer_dk_cv == '') {
            $('.error_customer_dk_cv').html('<span class="text-danger">Chức vụ người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dk_date == null || customer_dk_date == '') {
            $('.error_customer_dk_date').html('<span class="text-danger">Ngày sinh người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dk_cmnd == null || customer_dk_cmnd == '') {
            $('.error_customer_dk_cmnd').html('<span class="text-danger">Số chứng minh nhân dân người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dk_phone == null || customer_dk_phone == '') {
            $('.error_customer_dk_phone').html('<span class="text-danger">Số điện thoại người đăng ký không được để trống.</span>');
            return true;
        } else if (customer_dd_name == null || customer_dd_name == '') {
            $('.error_customer_dd_name').html('<span class="text-danger">Họ tên người đại diện không được để trống.</span>');
            return true;
        } else if (customer_dd_cv == null || customer_dd_cv == '') {
            $('.error_customer_dd_cv').html('<span class="text-danger">Chức vụ người đại diện không được để trống.</span>');
            return true;
        } else if (customer_dd_gender == null || customer_dd_gender == '') {
            $('.error_customer_dd_gender').html('<span class="text-danger">Giới tính người đại diện không được để trống.</span>');
            return true;
        } else if (customer_dd_date == null || customer_dd_date == '') {
            $('.error_customer_dd_date').html('<span class="text-danger">Ngày sinh người đại diện không được để trống.</span>');
            return true;
        } else if (customer_dd_cmnd == null || customer_dd_cmnd == '') {
            $('.error_customer_dd_cmnd').html('<span class="text-danger">Số chứng minh nhân dân người đại diện không được để trống.</span>');
            return true;
        } else if (customer_dd_email == null || customer_dd_email == '') {
            $('.error_customer_dd_email').html('<span class="text-danger">Email người đại diện không được để trống.</span>');
            return true;
        } else if (customer_tt_name == null || customer_tt_name == '') {
            $('.error_customer_tt_name').html('<span class="text-danger">Họ tên người đại diện làm thủ tục không được để trống.</span>');
            return true;
        } else if (customer_tt_gender == null || customer_tt_gender == '') {
            $('.error_customer_tt_gender').html('<span class="text-danger">Giới tính người đại diện làm thủ tục không được để trống.</span>');
            return true;
        } else if (customer_tt_date == null || customer_tt_date == '') {
            $('.error_customer_tt_gender').html('<span class="text-danger">Ngày sinh người đại diện làm thủ tục không được để trống.</span>');
            return true;
        } else if (customer_tt_cmnd == null || customer_tt_cmnd == '') {
            $('.error_customer_tt_cmnd').html('<span class="text-danger">Số chứng minh nhân dân người đại diện làm thủ tục không được để trống.</span>');
            return true;
        } else if (customer_tt_email == null || customer_tt_email == '') {
            $('.error_customer_tt_email').html('<span class="text-danger">Email người đại diện làm thủ tục không được để trống.</span>');
            return true;
        }
        return false
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    // Bang trong vps us
    // let country = $('.chosse_country:checked').val();
    let country = 'US';
    if (country != 'US') {
        $('#choosse_state').val(country);
        $('#select_states').fadeOut();
    } else {
        let states = $('#states :selected').val();
        $('#choosse_state').val(states);
        $('#select_states').fadeIn();
    }

    $('.chosse_country').on('click', function() {
        if ($(this).val() != 'US') {
            $('#choosse_state').val($(this).val());
            $('#select_states').fadeOut();
        } else {
            $('#select_states').fadeIn();
            states = $('#states :selected').val();
            $('#choosse_state').val(states);
        }
    });

    $(document).on('change', '#states', function() {
        states = $('#states :selected').val();
        $('#choosse_state').val(states);
    })

    let key_add_state = 0;

    $('#btn_add_state').on('click', function() {
        let html = '';
        // Quốc gia
        html += '<div class="col-md-6 divstate" id="divstate' + key_add_state + '">';
        html += '<div class="divstate_btn_tool">';
        html += '<button type="button" class="btn btn-tool btn_tool_remove" data-id="' + key_add_state + '"><i class="fas fa-times"></i></button>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<input type="hidden" name="add_state_type_country[' + key_add_state + ']" class="add_state_chosse_country" data-key="' + key_add_state + '"  value="US">';
        // html += '<label for="">Chọn quốc gia</label>';
        // html += '<div class="select-os  mt-2">';
        // html += '<label class="radio-inline">';
        // html += '<input type="radio" name="add_state_type_country[' + key_add_state + ']" class="add_state_chosse_country" data-key="' + key_add_state + '"  value="US" checked> US';
        // html += '</label>';
        // html += '<label class="radio-inline">';
        // html += '<input type="radio" name="add_state_type_country[' + key_add_state + ']" class="add_state_chosse_country" data-key="' + key_add_state + '" value="UK"> UK';
        // html += '</label>';
        // html += '<label class="radio-inline">';
        // html += '<input type="radio" name="add_state_type_country[' + key_add_state + ']" class="add_state_chosse_country" data-key="' + key_add_state + '" value="Netherlands"> Netherlands (Hà Lan)';
        // html += '</label>';
        // html += '</div>';
        html += '</div>';
        html += '<div class="form-group" id="select_states' + key_add_state + '">';
        html += '<label for="states">Bang</label>';
        html += '<div class="select-states">';
        html += '<select name="add_state_name[' + key_add_state + ']" id="state' + key_add_state + '" class="form-control">';
        html += '</select>';
        html += '</div>';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<label for="">Số lượng</label>';
        html += '<input type="text" value="0" name="add_state_quantity[' + key_add_state + ']" id="qtt' + key_add_state + '" class="form-control add_state_quantity" placeholder="Số lượng">';
        html += '<div class="quantity-order mb-4 mt-4">';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $(this).closest('divstate').addClass('col-md-6');
        $('#inp_add_state').val(1);
        $('#add_state').append(html);
        loadState(key_add_state);
        key_add_state++;
    });

    function loadState(key) {
        var product_special = $('#product_special').val();
        $.ajax({
            url: '/order/loadState',
            data: { product_special: product_special },
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                var html = '';
                $.each(data, function(index, state) {
                    if (state.id_state != 13 && state.id_state != 12 && state.id_state != 16) {
                        html += '<option value="' + state.name + '">' + state.name + '</option>';
                    }
                });
                $('#state' + key + '').html(html);
            }
        });
    }

    $(document).on('click', '.add_state_chosse_country', function() {
        let key = $(this).attr('data-key');
        if ($(this).val() != 'US') {
            $('#select_states' + key).fadeOut();
        } else {
            $('#select_states' + key).fadeIn();
        }
    });

    $(document).on('keyup', '.add_state_quantity', function() {
        var qtt = $('#qtt').val();
        var sub_total = $('#sub_total').val();
        var inpQtt = $('.add_state_quantity');
        var qttAdd = 0;
        $.each(inpQtt, function(index, value) {
            qttAdd += parseInt($(this).val());
        })
        qtt = parseInt(qttAdd) + parseInt(qtt);
        total = sub_total * qtt;
        total_in_table = sub_total * qtt;
        $('.quantity-printf').html(qtt);
        $('td.item_pricing_cell').text(addCommas(total_in_table) + " VNĐ");
        $('.total_pricing').html(addCommas(total_in_table) + ' VNĐ');
    });

    $(document).on('click', '.btn_tool_remove', function() {
        let key = $(this).attr('data-id');
        $('#divstate' + key).remove();
    })

    $('.chosse_account').on('change', function() {
        // console.log("da lcic 1", $(this).val());
        if ( $(this).val() == 'random' ) {
            $('#username').attr('disabled', true);
            $('#password').attr('disabled', true);
        } else {
            $('#username').attr('disabled', false);
            $('#password').attr('disabled', false);
        }
    })

});