$(document).ready(function () {
  $('.select2').select2();
  var select_product = $('#select_product').val();
  // list_event(select_product);

   $('#create_event').on('click', function () {
      $('#delete_vps').modal('show');
      $('.modal-title').text('Tạo khuyến mãi');
      $('#button-submit').val('Tạo khuyến mãi');
      $('#notication_invoice').html("");
      $('#form_group_event')[0].reset();
      $('#create').val('create');
   });

   $('#button-submit').on('click', function () {
      var form = $('#form_group_event').serialize();
      var product_id = $('#product').val();
      var billing = $('#billing').val();
      var point = $('#point').val();
      var validate = group_product_validate(product_id, billing, point);

      if (validate == false) {
          $.ajax({
             url: '/admin/event/actions',
             type: 'post',
             data: form,
             dataType: 'json',
             beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('#notication_invoice').html(html);
               $('#form_group_event').css('opacity', 0.5);
               $('#button-submit').attr('disabled', true);
               $('#form_group_event').attr('disabled', true);
             },
             success: function (data) {
               // console.log(data);
               if (data) {
                 var html = '<p class="text-success">'+data+'</p>';
                 $('#notication_invoice').html(html);
                 $('#form_group_event')[0].reset();
                 $('.modal-title').text('Tạo khuyến mãi');
                 $('.modal-title').text('Tạo nhóm sản phẩm');
                 $('#form_group_event').css('opacity', 1);
                 $('#button-submit').attr('disabled', false);
                 $('#form_group_event').attr('disabled', false);
               } else {
                 var html = '<p class="text-danger">Tạo nhóm sản phẩm thất bại</p>';
                 $('#notication_invoice').html(html);
                 $('#form_group_event').css('opacity', 1);
                 $('#button-submit').attr('disabled', false);
                 $('#form_group_event').attr('disabled', false)
               }
               list_event('');
             },
             error: function (e) {
                console.log(e);
                $('#notication_invoice').html('<span class="text-danger">Tạo event  cho sản phẩm thất bại.</span>');
             }
          })
      }

   })

   function list_event(select_product = '') {
       $.ajax({
          url: '/admin/event/list_event',
          data: {select: select_product},
          dataType: 'json',
          beforeSend: function(){
            var html = '<td  colspan="4" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
            if (data.data != '') {
              var html = '';
              $.each(data.data, function (index, event) {
                  // console.log(event);
                  html += '<tr>';
                  html += '<td><a href="/admin/products/edit/'+ event.product_id +'">' + event.name + '</a></td>';
                  html += '<td>' + event.billing_cycle + '</td>';
                  html += '<td>' + event.point + '</td>';
                  html += '<td>';
                  html += '<button type="button" class="btn btn-warning text-light edit_event mr-2" data-toggle="tooltip" data-id="'+ event.id +'" data-placement="top" title="Chỉnh sửa khuyến mãi"><i class="far fa-edit"></i></button>';
                  html += '<button type="button" class="btn btn-danger delete_event"><i class="far fa-trash-alt" data-id="'+ event.id +'" data-toggle="tooltip" data-placement="top" title="Xóa khuyến mãi"></i></button>';
                  html += '</td>';
                  html += '</tr>';
              })
              var total = data.total;
              var per_page = data.per_page;
              var current_page = data.current_page;
              var html_page = '';
              if (total > per_page) {
                var page = total/per_page + 1;
                html_page += '<td colspan="4" class="text-center">';
                html_page += '<ul class="pagination">';
                if (current_page != 1) {
                  html_page += '<li><a href="#" data-page="'+(current_page-1)+'" class="prev">&laquo</a></li>';
                } else {
                  html_page += '<li><a href="#" class="prev disabled">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  html_page += '<li><a href="#" data-page="'+i+'" class="'+active+'">'+i+'</a></li>';
                }
                if (current_page != page.toPrecision(1)) {
                  html_page += '<li><a href="#" data-page="'+current_page+'" class="prev">&raquo;</a></li>';
                } else {
                  html_page += '<li><a href="#" class="prev disabled">&raquo;</a></li>';
                }
              }
              html_page += '</ul>';
              html_page += '</td>';
              $('tfoot').html(html_page);
              $('tbody').html(html);
            } else {
               $('tbody').html('<td colspan="4" class="text-center text-danger">Không có khuyến mãi nào trong cơ sở dữ liệu.</td>');
            }
          },
          error: function (e) {
             console.log(e);
             $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn khuyến mãi thất bại</td>');
          }
       })
   }

   //ajax phân trang
   $(document).on('click', '.pagination a', function(event) {
       event.preventDefault();
       var page = $(this).attr('data-page');
       $.ajax({
           url: '/admin/event/list_event',
           type: 'get',
           dataType: 'json',
           data: {page: page},
           beforeSend: function(){
             var html = '<td  colspan="4" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
           },
           success: function (data) {
             // console.log(data);
             if (data.data != '') {
               var html = '';
               $.each(data.data, function (index, event) {
                   // console.log(event);
                   html += '<tr>';
                   html += '<td><a href="/admin/products/edit/'+ event.product_id +'">' + event.name + '</a></td>';
                   html += '<td>' + event.billing_cycle + '</td>';
                   html += '<td>' + event.point + '</td>';
                   html += '<td>';
                   html += '<button type="button" class="btn btn-warning text-light edit_event mr-2" data-id="'+ event.id +'" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa khuyến mãi"><i class="far fa-edit"></i></button>';
                   html += '<button type="button" class="btn btn-danger delete_event"><i class="far fa-trash-alt" data-id="'+ event.id +'"  data-toggle="tooltip" data-placement="top" title="Xóa khuyến mãi"></i></button>';
                   html += '</td>';
                   html += '</tr>';
               })
               var total = data.total;
               var per_page = data.per_page;
               var current_page = data.current_page;
               var html_page = '';
               if (total > per_page) {
                 var page = total/per_page + 1;
                 html_page += '<td colspan="4" class="text-center">';
                 html_page += '<ul class="pagination">';
                 if (current_page != 1) {
                   html_page += '<li><a href="#" data-page="'+(current_page-1)+'" class="prev">&laquo</a></li>';
                 } else {
                   html_page += '<li><a href="#" class="prev disabled">&laquo</a></li>';
                 }
                 for (var i = 1; i < page; i++) {
                   var active = '';
                   if (i == current_page) {
                     active = 'active';
                   }
                   html_page += '<li><a href="#" data-page="'+i+'" class="'+active+'">'+i+'</a></li>';
                 }
                 if (current_page != page.toPrecision(1)) {
                   html_page += '<li><a href="#" data-page="'+current_page+'" class="prev">&raquo;</a></li>';
                 } else {
                   html_page += '<li><a href="#" class="prev disabled">&raquo;</a></li>';
                 }
               }
               html_page += '</ul>';
               html_page += '</td>';
               $('tfoot').html(html_page);
               $('tbody').html(html);
             } else {
                $('tbody').html('<td colspan="4" class="text-center text-danger">Không có khuyến mãi nào trong cơ sở dữ liệu.</td>');
             }
           },
           error: function (e) {
              console.log(e);
              $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn khuyến mãi thất bại</td>');
           }
       });
   });

   $(document).on('click', '.edit_event', function () {
      var id = $(this).attr('data-id');
      $('#delete_vps').modal('show');
      $('.modal-title').text('Sửa khuyến mãi');
      $('#button-submit').val('Sửa khuyến mãi');
      $('#notication_invoice').html("");
      $('#form_group_event')[0].reset();
      $('#create').val('update');
      $('#event_id').val(id);

      $.ajax({
         url: '/admin/event/detail',
         data: {id:id},
         dataType: 'json',
         beforeSend: function(){
           var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
           $('#notication_invoice').html(html);
           $('#form_group_event').css('opacity', 0.5);
           $('#button-submit').attr('disabled', true);
           $('#form_group_event').attr('disabled', true);
         },
         success: function (data) {
           console.log(data);
           if (data) {
              var html = '';
              var html2 = '';
              $('#point').val(data.event.point);
              $.each(data.billings , function (index1, billing) {
                 var selected = '';
                 if (index1 == data.event.billing_cycle) {
                    selected = 'selected';
                 }
                 html += '<option value="'+ index1 +'" '+ selected +'>' + billing + '</option>';
              })
              $('#billing').html(html);
              $.each(data.products , function (index2, product) {
                 var selected = '';
                 if (product.id == data.event.product_id) {
                    selected = 'selected';
                 }
                 html2 += '<option value="'+ product.id +'" '+ selected +'>' + product.name + ' - ' + product.group_product.name + '</option>';
              })
              $('#product').html(html2);
           }
           $('#notication_invoice').html('');
           $('#form_group_event').css('opacity', 1);
           $('#button-submit').attr('disabled', false);
           $('#form_group_event').attr('disabled', false);
         },
         error: function (e) {
            console.log(e);
            $('#notication_invoice').html('<span class="text-danger">Truy vấn khuyến mãi thất bại.</span>');
         }
      })
   })

   $('#select_product').on('change', function () {
      var select = $(this).val();
      list_event(select);
   })

   // validate group product
   function group_product_validate(product_id, billing, point) {

     if (product_id == null || product_id == '') {
       $('#notication_invoice').html('<span class="text-danger">Sản phẩm không được để trống.</span>');
       return true;
     } else if(billing == null || billing == '') {
       $('#notication_invoice').html('<span class="text-danger">Thời gian không được để trống.</span>');
       return true;
     } else if(point == '') {
       $('#notication_invoice').html('<span class="text-danger">Số điểm không được để trống.</span>');
       return true;
     }
     else {
       return false;
     }
   }

   $(document).on('click', '.delete_event', function () {
      var id = $(this).attr('data-id');
      $('#delete_tutorial').modal('show');
      $('#notication_tutorial').html('<span class="text-danger">Bạn có muốn xóa khuyến mãi cho sản phẩm này không</span>');
      $('#button_delete').fadeIn();
      $('#button_finish').fadeOut();
      $('#button_delete').attr('data-id', id);
   })

   $('#button_delete').on('click' , function () {
      var id = $(this).attr('data-id');
      $.ajax({
         url: '/admin/event/delete',
         data: {id:id},
         dataType: 'json',
         beforeSend: function(){
           var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
           $('#notication_tutorial').html(html);
           $('#button_delete').attr('disabled', true);
         },
         success: function (data) {
           var html = '';
           if (data.error == 0) {
             if ( data.delete ) {
              html = '<span class="text-success">Xóa khuyến mãi thành công.</span>';
              list_event();
             } else {
              html = '<span class="text-danger">Xóa khuyến mãi thất bại.</span>';
             }
           } else {
             html = '<span class="text-danger">Bạn không có quyền xóa khuyến mãi này.</span>';
           }
           $('#notication_tutorial').html(html);
           $('#button_delete').attr('disabled', false);
           $('#button_delete').fadeOut();
           $('#button_finish').fadeIn();
         },
         error: function (e) {
            console.log(e);
            $('#notication_tutorial').html('<span class="text-danger">Truy vấn khuyến mãi thất bại.</span>');
         }
      })
   })

})
