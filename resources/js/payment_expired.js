$(document).ready(function() {
    // Hình thức thanh toán trực tiếp tại văn phòn
    $('#office').on('click', function(event) {
        event.preventDefault();
        $('.list-type-payment').removeClass('active');
        $(this).addClass('active');
        var invoice_id  = $('#invoice_id').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/pay-in-online/payment_expired?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.payment-info').html(html);
            },
            success: function(data) {
              // console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<h3 class="mb-4">Hình thức thanh toán trực tiếp tại văn phòng</h3>';
                    html += '<div class="company-name mb-3">Công ty TNHH MTV Đại Việt Số</div>';
                    html += '<div class="info-company">';
                    html += '<b>Địa chỉ văn phòng:</b> 67 - Nguyễn Thị Định - quận Sơn Trà - Thành phố Đà Nẵng <br>';
                    html += '<b>Điện thoại hỗ trợ:</b> 0236 4455 789 <br>';
                    html += '<b>Số tiền thanh toán: </b>' + addCommas(data.total) + ' VNĐ';
                    html += '<br><b>Mã giao dịch</b>: ' + data.pay.ma_gd;
                    html += '<form action="/order/payment_expired_off" method="post">';
                    html += '<input type="hidden" name="method_gd_invoice" value="pay_in_office">';
                    html += '<input type="hidden" name="invoice_id" value="'+ invoice_id +'">';
                    html += '<input type="hidden" name="_token" value="'+ token +'">';
                    html += '<button type="submit" id="submit_payment"  class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>';
                    html += '</form>';
                    html += '</div>';
                    $('.payment-info').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.payment-info').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.payment-info').html(html);
            }
        });
    });
    //Hình thức thanh toán trực tiếp bằng ATM
    $('#atm').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        $('.list-type-payment').removeClass('active');
        $(this).addClass('active');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/pay-in-online/payment_expired?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.payment-info').html(html);
            },
            success: function(data) {
              // console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<h3 class="mb-4">Hình thức thanh toán trực tiếp bằng ATM</h3>';
                    html += '<div class="info-company">';
                    html += '<div class="row bank">';
                    html += '<div class="col-md-6 active" id="vietcombank">';
                    html += '<div class="list-type-payment-img">';
                    html += '<img src="/images/icon-vietcombank.png" alt="">';
                    html += '</div>';
                    html += '<div class="info-bank">';
                    html += '<b>Bank Name:</b> Vietcombank <br>';
                    html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
                    html += '<b>Account Number:</b> 0041000830880 <br>';
                    html += '</div>';
                    html += '</div>';
                    // html += '<div class="col-md-6 active" id="techcombank">';
                    // html += '<div class="list-type-payment-img">';
                    // html += '<img src="/images/icon-techcombank.png" alt="">';
                    // html += '</div>';
                    // html += '<div class="info-bank">';
                    // html += '<b>Bank Name:</b> Techcombank <br>';
                    // html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
                    // html += '<b>Account Number:</b> 19033827040017 <br>';
                    // html += '</div>';
                    // html += '</div>';
                    html += '</div>';
                    html += '<b>Nội dung</b>: <br>- Tổng số tiền thanh toán: ' + addCommas(data.total) + ' VNĐ<br>- Mã giao dịch: ' + data.pay.ma_gd;
                    html += '<br><form action="/order/payment_expired_off" method="post">';
                    html += '<input type="hidden" id="method_gd_atm" name="method_gd_invoice" value="bank_transfer_vietcombank">';
                    html += '<input type="hidden" name="invoice_id" value="'+ invoice_id +'">';
                    html += '<input type="hidden" name="_token" value="'+ token +'">';
                    html += '<button type="submit" id="submit_payment"  class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>';
                    html += '</form>';
                    html += '</div>';
                    $('.payment-info').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.payment-info').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.payment-info').html(html);
            }
        });
    });
    // Hình thức thanh toán trực tiếp bằng MOMO
    $('#momo').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        $('.list-type-payment').removeClass('active');
        $(this).addClass('active');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/pay-in-online/payment_expired?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.payment-info').html(html);
            },
            success: function(data) {
              //console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<h3 class="mb-4">Hình thức thanh toán trực tiếp bằng MoMo</h3>';
                    html += '<div class="info-company mb-3">';
                    html += '<div class="info-company">';
                    html += '<div class="info-bank">';
                    html +=  '<b>Account Name:</b> Lê Minh Chí <br>';
                    html += '<b>Account Number:</b>  0905 091 805 <br>';
                    html += '</div>';
                    html += '<b>Nội dung</b>: <br>- Tổng số tiền thanh toán: ' + addCommas(data.total) + ' VNĐ<br> - Mã giao dịch: ' + data.pay.ma_gd;
                    html += '</div>';
                    html += '<form action="/order/payment_expired_off" method="post">';
                    html += '<input type="hidden" name="method_gd_invoice" value="momo">';
                    html += '<input type="hidden" name="invoice_id" value="'+ invoice_id +'">';
                    html += '<input type="hidden" name="_token" value="'+ token +'">';
                    html += '<button type="submit" id="submit_payment"  class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>';
                    html += '</form>';
                    html += '</div>';
                    $('.payment-info').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.payment-info').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.payment-info').html(html);
            }
        });
    });
    // Hình thức thanh toán bằng credit
    $('#credit').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $('.list-type-payment').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url: '/pay-in-online/payment_expired?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.payment-info').html(html);
            },
            success: function(data) {
              // console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<h3 class="mb-4">Hình thức thanh toán bằng số dư tài khoản</h3>';
                    html += '<div class="info-company text-center mt-4">';
                    html += '<b>Số dư tài khoản: </b>' + addCommas(data.credit.value) + ' VNĐ<br>';
                    html += '<b>Tổng số tiền thanh toán: </b>' + addCommas(data.total) + ' VNĐ<br>';
                    html += '<b>Loại giao dịch: </b> Gia hạn dịch vụ / sản phẩm <br>' ;
                    html += '<form action="/order/payment_expired_form" method="post">';
                    html += '<input type="hidden" name="invoice_id" value="'+ invoice_id +'">';
                    html += '<input type="hidden" name="_token" value="'+ token +'">';
                    html += '<button type="submit" id="submit_payment"  class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>';
                    html += '</form>';
                    html += '</div>';
                    $('.payment-info').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.payment-info').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.payment-info').html(html);
            }
        });
    });
    // Thanh toán bằng cloudzone point
    $('#cloudzone_point').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $('.list-type-payment').removeClass('active');
        $(this).addClass('active');
        $.ajax({
            url: '/pay-in-online/payment_expired_cloudzone_point?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.payment-info').html(html);
            },
            success: function(data) {
              //console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<h3 class="mb-4">Hình thức thanh toán bằng Cloudzone Point</h3>';
                    html += '<div class="info-company text-center mt-4">';
                    html += '<b>Cloudzone Point: </b>';
                    if(data.point != null)  {
                      html +=  data.point + ' Điểm<br>';
                    } else {
                      html += '0 Điểm<br>';
                    }
                    html += '<b>Tổng số điểm thanh toán: </b>' + (data.total / 1000) + ' Điểm<br>';
                    html += '<b>Loại giao dịch: </b>  Gia hạn dịch vụ / sản phẩm <br>';
                    html += '<form action="/order/payment_expired_cloudzone_point" method="post">';
                    html += '<input type="hidden" name="invoice_id" value="'+ invoice_id +'">';
                    html += '<input type="hidden" name="_token" value="'+ token +'">';
                    html += '<button type="submit" id="submit_payment"  class="btn btn-success mt-4 mb-4"><i class="far fa-credit-card"></i> Thanh toán</button>';
                    html += '</form>';
                    html += '</div>';
                    $('.payment-info').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.payment-info').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.payment-info').html(html);
            }
        });
    });

    $(document).on('click', '#techcombank', function () {
        $(this).addClass('active');
        $('#vietcombank').removeClass('active');
        $('#method_gd_atm').val('bank_transfer_techcombank');
    })

    $(document).on('click', '#vietcombank', function () {
        $(this).addClass('active');
        $('#techcombank').removeClass('active');
        $('#method_gd_atm').val('bank_transfer_vietcombank');
    })

    $(document).on('click', '#submit_payment', function (e) {
        e.preventDefault();
        // console.log('da click');
        $(this).attr('disabled', true);
        $(this).closest( "form" ).submit();
    })

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
});
