$(document).ready(function() {
    // Load nhóm người dùng
    loadGroupUser();

    function loadGroupUser() {
        $.ajax({
            url: '/admin/user/loadGroupUser',
            dataType: 'json',
            beforeSend: function() {
                var html = '<tr><td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>';
                $('tbody').html(html);
            },
            success: function(data) {
                var html = '';
                if (data != '') {
                    $.each(data, function(index, group_user) {
                        html += '<tr>';
                        html += '<td>' + group_user.id + '</td>';
                        html += '<td>' + group_user.name + '</td>';
                        html += '<td>';
                        if (group_user.users.length > 0) {
                            html += group_user.users.length;
                        } else {
                            html += '0';
                        }
                        html += '</td>';
                        html += '<td>';
                        if (group_user.group_products.length > 0) {
                            html += group_user.group_products.length;
                        } else {
                            html += '0';
                        }
                        html += '</td>';
                        html += '<td class="table-button">';
                        html += '<button type="button" name="button" class="btn btn-secondary edit_group mr-1" data-id="' + group_user.id + '" data-name="' + group_user.name + '" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa nhóm đại lý"><i class="fas fa-edit"></i></button>';
                        html += '<a href="/admin/dai-ly/chi-tiet-dai-ly/' + group_user.id + '" class="btn btn-info mr-1" data-toggle="tooltip" data-placement="top" title="Chi tiết nhóm đại lý"><i class="fas fa-info-circle"></i></a>';
                        html += '<button type="button" name="button" class="btn btn-danger delete_group_user" data-id="' + group_user.id + '" data-name="' + group_user.name + '" data-toggle="tooltip" data-placement="top" title="Xóa nhóm đại lý"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    });
                    $('tbody').html(html);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    var html = '<td class="text-danger text-center" colspan="4">Không có nhóm đại lý trong hệ thống.</td>';
                    $('tbody').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<td class="text-danger text-center" colspan="4">Truy vấn nhóm đại lý lỗi!</td>';
                $('tbody').html(html);
            }
        });
    }

    // Tạo group user
    $('#createGroup').on('click', function() {
        $('#modal-create').modal('show');
        $('#modal-create .modal-title').text('Tạo nhóm đại lý');
        $('#action').val('create');
        $('#button-create').text('Tạo nhóm đại lý');
        $('#form-group-product')[0].reset();
        $('#notication').html("");
    });
    // Form submit
    $('#button-create').on('click', function() {
        var form = $('#form-group-product').serialize();
        $.ajax({
            url: '/admin/user/create_group_user',
            type: 'post',
            data: form,
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#notication').html(html);
                $('#form-group-product').css('opacity', 0.5);
                $('#button-create').attr('disabled', true);
                $('#form-group-product').attr('disabled', true);
            },
            success: function(response) {
                // console.log(response);
                if (response) {
                    var html = '<p class="text-success">' + response + '</p>';
                    $('#notication').html(html);
                    $('#form-group-product')[0].reset();
                    $('#button-create').text('Tạo nhóm đại lý');
                    $('#modal-create .modal-title').text('Tạo nhóm đại lý');
                    $('#form-group-product').css('opacity', 1);
                    $('#button-create').attr('disabled', false);
                    $('#form-group-product').attr('disabled', false);
                } else {
                    var html = '<p class="text-danger">Tạo nhóm đại lý thất bại</p>';
                    $('#notication').html(html);
                    $('#form-group-product').css('opacity', 1);
                    $('#button-submit').attr('disabled', false);
                    $('#form-group-product').attr('disabled', false);
                }
                loadGroupUser();
            },
            error: function(e) {
                console.log(e);
                var html = '<p class="text-danger">Tạo / Chỉnh sửa nhóm đại lý lỗi!</p>';
                $('#notication').html(html);
                $('#form-group-product').css('opacity', 1);
                $('#button-create').attr('disabled', false);
                $('#form-group-product').attr('disabled', false);
            }

        });
    });
    // chỉnh sửa group user
    $(document).on('click', '.edit_group', function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#name').val(name);
        $('#id-group').val(id);
        $('#modal-create').modal('show');
        $('#modal-create .modal-title').text('Chỉnh sửa nhóm đại lý');
        $('#action').val('update');
        $('#button-create').text('Chỉnh sửa');
        $('#notication').html("");
    });
    // Xóa nhóm đại lý
    $(document).on('click', '.delete_group_user', function() {
        $('tr').removeClass('choose-group');
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#modal-delete').modal('show');
        $('#modal-delete .modal-title').text('Xóa nhóm đại lý');
        $('#notication-delete').html('<span>Bạn có muốn xóa đại lý <b class="text-danger">' + name + '</b> này không?');
        $('#button-delete').attr('data-id', id);
        $('#button-delete').fadeIn();
        $('#button-finish').fadeOut();
        $(this).closest('tr').addClass('choose-group');
    })

    $('#button-delete').on('click', function() {
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/user/delete_group_user',
            type: 'post',
            dataType: 'json',
            data: { '_token': token, id: id },
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#notication-delete').html(html);
                $('#button-delete').attr('disabled', true);
            },
            success: function(data) {
                var html = '';
                if (data != '') {
                    html += '<span class="text-success">Xóa nhóm đại lý thành công</span>';
                    $('#button-delete').fadeOut();
                    $('#button-finish').fadeIn();
                    $('.choose-group').fadeOut();
                } else {
                    var html = '<span class="text-danger">Xóa nhóm đại lý thất bại</span>';
                }
                $('#button-delete').attr('disabled', false);
                $('#notication-delete').html(html);
            },
            error: function(e) {
                console.log(e);
                var html = '<span class="text-danger">Truy vấn nhóm đại lý lỗi!</span>';
                $('#notication-delete').html(html);
                $('#button-delete').attr('disabled', false);
            }
        });
    })



})