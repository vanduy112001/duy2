$(document).ready(function () {
    $('.delete-item-invoice').on('click', function() {
        $('#delete-order').modal('show');
        var type = $(this).attr('data-type');
        var name = $(this).attr('data-name');
        var parent = $(this).attr('data-parent');
        var id = $(this).attr('data-id');
        var html = '';
        $('.modal-title').text('Xóa '+ type + ' - '+ name +' trong đơn hàng');
        if (type == 'VPS') {
            html += 'Bạn có muốn xóa <b class="text-danger">' + type + ' - '+ name + '</b> này không?';
            $('#button-order').val('Xóa VPS');
        } else if (type == 'Server') {
            html += 'Bạn có muốn xóa <b class="text-danger">' + type + ' - '+ name + '</b> này không?';
            $('#button-order').val('Xóa Server');
        } else {
            $('#button-order').val('Xóa Hosting');
            html += 'Bạn có muốn xóa <b class="text-danger">' + type + ' - '+ name + '</b> này không?';
        }
        $('#notication-order').html(html);
        $('#button-order').attr('data-id', id);
        $('#button-order').attr('data-type', type);
        $('#button-order').attr('data-parent', parent);
    });

    $('#button-order').on('click', function() {
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var parent = $('#button-order').attr('data-parent');
        window.location.href = '/admin/invoices/delete_item?id='+id+'&type='+type+'&parent='+parent;
    });

});
