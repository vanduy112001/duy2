jQuery(document).ready(function() {
    $('.btn-add-contract').on('click', function() {
        var ids = [];
        var selecteds = $('#table_vps input[type="checkbox"]:checked');
        var product_type = $(this).attr('product-type');
        if (selecteds.length > 0) {
            $(selecteds).each(function() {
                ids.push($(this).val());
            })
        } else {
            alert('Chưa chọn thuộc tính để thao tác');
        }
        // var link = '/admin/contracts/add-vps-to-contract' + '&id=' + ids;
        // console.log(link);
        window.location.href = '/admin/contracts/add-product-to-contract?product_type=' + product_type + '&' + $.param({ ids });
    });
});