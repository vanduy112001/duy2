$(document).ready(function() {
    // khởi tạo modal Toast
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });


    $('.button-action-hosting').on('click', function (event) {
        event.preventDefault();
        var type = $(this).attr('data-type');
        $('#delete-order').modal('show');
        $('#button-order').fadeIn();
        $('#button-finish').fadeOut();
        switch (type) {
          case 'create':
              $('.modal-title').text('Tạo Hosting');
              $('#notication-order').html('<span class="text-danger">Bạn có muốn tạo Hosting này không?</span>');
              $('#button-order').attr('data-type', type);
            break;
          case 'suspend':
              $('.modal-title').text('Suspend Hosting');
              $('#notication-order').html('<span class="text-danger">Bạn có muốn Suspend Hosting này không?</span>');
              $('#button-order').attr('data-type', type);
            break;
          case 'unsuspend':
              $('.modal-title').text('Unsuspend Hosting');
              $('#notication-order').html('<span class="text-danger">Bạn có muốn Unsuspend Hosting này không?</span>');
              $('#button-order').attr('data-type', type);
            break;
          case 'delete':
              $('.modal-title').text('Xóa Hosting');
              $('#notication-order').html('<span class="text-danger">Bạn có muốn xóa Hosting này không?</span>');
              $('#button-order').attr('data-type', type);
            break;
        }
    });

    $(document).on('click', 'a.date_create', function (event) {
        event.preventDefault();
        var date = $(this).attr('data-date');
        var html = '';
        html += '<input type="text" placeholder="Ngày tạo" value="'+date+'" name="date_create" class="form-control">';
        $('#date_create').html(html);
    })

    $('#button-order').on('click', function () {
        var id = $('#hosting_id').val();
        var type = $(this).attr('data-type');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/hostings/action_hosting',
            type: 'post',
            dataType: 'json',
            data: { '_token': token, id: id, type: type },
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-order').attr('disabled', true);
                $('#notication-order').html(html);
            },
            success: function (data) {
              // console.log(data);
                var html = '';
                if (data) {
                    // console.log(data);
                    switch (data) {
                      case 'create':
                          $(document).Toasts('create', {
                              class: 'bg-success',
                              title: 'Cloudzone',
                              subtitle: 'hosting',
                              body: "Tạo Hosting thành công",
                          })
                          var button_action = $('a.button-action-hosting');
                          for (var i = 0; i < button_action.length; i++) {
                              var strt = 'a.button-action-hosting:nth-child('+i+')';
                              var type = $(strt).attr('data-type');
                              if (type == 'create') {
                                  $(strt).addClass('disabled');
                              }
                          }
                        break;
                      case 'unsuspend':
                        // console.log('da toi on');
                          $(document).Toasts('create', {
                              class: 'bg-success',
                              title: 'Cloudzone',
                              subtitle: 'hosting',
                              body: "UnSuppen Hosting thành công",
                          })
                          var button_action = $('a.button-action-hosting');
                          for (var i = 0; i < button_action.length; i++) {
                              var strt = 'a.button-action-hosting:nth-child('+i+')';
                              var type = $(strt).attr('data-type');
                              if (type == 'unsuspend') {
                                  $(strt).addClass('disabled');
                              }
                              if (type == 'suspend') {
                                  $(strt).removeClass('disabled');
                              }
                          }
                        break;
                      case 'suspend':
                      // console.log('da toi off');
                          $(document).Toasts('create', {
                              class: 'bg-success',
                              title: 'Cloudzone',
                              subtitle: 'hosting',
                              body: "Suppen Hosting thành công",
                          })
                          // console.log($('a.button-action-hosting'));
                          var button_action = $('a.button-action-hosting');
                          for (var i = 0; i < button_action.length; i++) {
                              var strt = 'a.button-action-hosting:nth-child('+i+')';
                              var type = $(strt).attr('data-type');
                              if (type == 'suspend') {
                                  $(strt).addClass('disabled');
                              }
                              if (type == 'unsuspend') {
                                  $(strt).removeClass('disabled');
                              }
                          }
                        break;
                      case 'delete':
                          html += '<span class="text-danger">Xóa Hosting thành công. Sau 2s sẽ chuyển tiếp đến trang quản lý hosting.</span>';
                          var setTime = setTimeout(function() {
                              window.location.href = '/admin/hostings';
                          }, 2000);
                        break;
                    }
                } else {
                    $(document).Toasts('create', {
                        class: 'bg-danger',
                        title: 'Cloudzone',
                        subtitle: 'hosting',
                        body: "Thao tác với Hosting thất bại",
                    });
                }
                $('#delete-order').modal('hide');
                $('#notication-order').html(html);
                $('#button-order').attr('disabled', false);
            },
            error: function (e) {
                console.log(e);
                $('#notication-order').html('<span class="text-danger">Truy vấn dịch vụ Hosting lỗi!</span>');
                $('#button-order').attr('disabled', false);
            }
        });
    });

    // Click chinh sửa form
    //next_due_date
    $('.next_due_date').on('click', function (event) {
        event.preventDefault();
        // console.log('da den');
        var data = $(this).attr('data-date');
        var html = '';
        html = '<input class="form-control" name="next_due_date" value="'+data+'" >';
        $('#next_due_date').html(html);
    });
    //paid_date
    $('.paid_date').on('click', function () {
        event.preventDefault();
        var data = $(this).attr('data-date');
        var html = '';
        html = '<input class="form-control" name="paid_date" value="'+data+'" >';
        $('#paid_date').html(html);
    });
    //sub_total
    $('.sub_total').on('click', function (event) {
        event.preventDefault();
        var data = $(this).attr('data-price');
        var html = '';
        html = '<input class="form-control" name="sub_total" value="'+data+'" >';
        $('#sub_total').html(html);
    });
    //billing_cycle
    $('.billing_cycle').on('click', function(event) {
        event.preventDefault();
        var billing_cycle = $(this).attr('data-name');
        $.ajax({
            url: '/admin/vps/loadBillings',
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#billing_cycle').html(html);
            },
            success: function (data) {
                var html = '';
                if (data != '' || data != null) {
                    html += '<select class="select2 form-control select_billing_cycle" style="width:100%" name="billing_cycle">';
                    html += '<option disabled>---Chọn thời gian thuê---</option>';
                    $.each(data , function(index, billing) {
                        if (billing_cycle == index) {
                            html += '<option value="'+index+'" selected>'+billing+'</option>';
                        } else {
                              html += '<option value="'+index+'">'+billing+'</option>';
                        }
                    });
                    html += '</select>';
                } else {
                    html += '<p class="text-danger">Không có thời gian thuê cho truy vấn này!</p>';
                }
                $('#billing_cycle').html(html);
                $('.select2').select2();
            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn thời gian thuê thất bại!</p>';
                $('#billing_cycle').html(html);
            }
        });
    });

    $(document).on('change' , '.select_billing_cycle' ,function() {
        var price = $('.sub_total').attr('data-price');
        var html = '';
        html += '<input type="text" value="'+price+'" name="sub_total" class="form-control is-invalid">';
        $('#sub_total').html(html);
    });

})
