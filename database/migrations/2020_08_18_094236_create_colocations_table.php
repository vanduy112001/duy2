<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colocations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detail_order_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->string('type_colo')->nullable();
            $table->string('ip')->nullable();
            $table->string('billing_cycle')->nullable();
            $table->string('status')->nullable();
            $table->string('status_colo')->nullable();
            $table->string('location')->nullable();
            $table->string('amount')->nullable();
            $table->string('bandwidth')->nullable();
            $table->string('power')->nullable();
            $table->string('paid')->nullable();
            $table->bigInteger('quantity')->nullable();
            $table->date('next_due_date')->nullable();
            $table->time('date_create')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colocations');
    }
}
