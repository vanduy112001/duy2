<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('type_customer');
            $table->string('ma_customer');
            $table->string('customer_name')->nullable();
            $table->date('customer_date')->nullable();
            $table->string('customer_gender')->nullable();
            $table->string('customer_cmnd')->nullable();
            $table->string('customer_phone')->nullable();
            $table->string('customer_email')->nullable();
            $table->string('customer_address')->nullable();
            $table->string('customer_city')->nullable();
            $table->string('customer_tc_name')->nullable();
            $table->string('customer_tc_mst')->nullable();
            $table->string('customer_tc_dctc')->nullable();
            $table->string('customer_tc_sdt')->nullable();
            $table->string('customer_tc_city')->nullable();
            $table->string('customer_dk_name')->nullable();
            $table->date('customer_dk_date')->nullable();
            $table->string('customer_dk_gender')->nullable();
            $table->string('customer_dk_phone')->nullable();
            $table->string('customer_dk_cv')->nullable();
            $table->string('customer_dd_name')->nullable();
            $table->string('customer_dd_cv')->nullable();
            $table->string('customer_dd_gender')->nullable();
            $table->string('customer_dd_date')->nullable();
            $table->string('customer_dd_cmnd')->nullable();
            $table->string('customer_dd_email')->nullable();
            $table->string('customer_tt_name')->nullable();
            $table->string('customer_tt_gender')->nullable();
            $table->string('customer_tt_date')->nullable();
            $table->string('customer_tt_cmnd')->nullable();
            $table->string('customer_tt_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
