<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHistoryPay2105 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('history_pays', 'magd_api')) {
            Schema::table('history_pays', function (Blueprint $table) {
                $table->string('magd_api')->nullable();
            });
        }
        if (! Schema::hasColumn('history_pays', 'type_gd_api')) {
            Schema::table('history_pays', function (Blueprint $table) {
                $table->string('type_gd_api')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_pays', function (Blueprint $table) {
            //
        });
    }
}
