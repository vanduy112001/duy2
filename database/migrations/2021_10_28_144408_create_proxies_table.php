<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProxiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proxies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detail_order_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->bigInteger('vm_id')->nullable();
            $table->string('ip')->nullable();
            $table->bigInteger('port')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->string('billing_cycle')->nullable();
            $table->string('description')->nullable();
            $table->date('next_due_date')->nullable();
            $table->string('status')->nullable();
            $table->string('location')->nullable();
            $table->string('state')->nullable();
            $table->bigInteger('price_override')->default(0);
            $table->boolean('auto_refurn')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proxies');
    }
}
