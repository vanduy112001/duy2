<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('local_type');
            $table->string('annually');
            $table->string('biennially');
            $table->string('triennially');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_products');
    }
}
