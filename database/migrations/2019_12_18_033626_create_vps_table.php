<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detail_order_id');
            $table->bigInteger('user_id');
            $table->bigInteger('product_id');
            $table->string('type');
            $table->string('ip')->nullable();
            $table->string('os');
            $table->string('billing_cycle');
            $table->date('next_due_date')->nullable();
            $table->string('status');
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vps');
    }
}
