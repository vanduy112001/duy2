<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVps2809 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('vps', 'location')) {
              Schema::table('vps', function (Blueprint $table) {
                  $table->string('location')->default('cloudzone');
              });
        }
        if ( ! Schema::hasColumn('vps', 'state')) {
              Schema::table('vps', function (Blueprint $table) {
                  $table->string('state')->nullable();
              });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vps', function (Blueprint $table) {
            //
        });
    }
}
