<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailHostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_hostings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detail_order_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('domain')->nullable();
            $table->string('billing_cycle')->nullable();
            $table->date('next_due_date')->nullable();
            $table->string('status')->nullable();
            $table->string('user')->nullable();
            $table->string('password')->nullable();
            $table->date('date_create')->nullable();
            $table->string('paid')->nullable();
            $table->string('status_hosting')->nullable();
            $table->string('price_override')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_hostings');
    }
}
