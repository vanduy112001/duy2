<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderExpired1304 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('order_expireds', 'type')) {
            Schema::table('order_expireds', function (Blueprint $table) {
                $table->string('type')->default('vps');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_expireds', function (Blueprint $table) {
            //
        });
    }
}
