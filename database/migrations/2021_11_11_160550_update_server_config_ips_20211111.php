<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServerConfigIps20211111 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('server_config_ips', 'ips')) {
            Schema::table('server_config_ips', function (Blueprint $table) {
                $table->text('ips')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_config_ips', function (Blueprint $table) {
            //
        });
    }
}
