<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHistoryPay1603 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('history_pays', 'method_gd_invoice')) {
            Schema::table('history_pays', function (Blueprint $table) {
                $table->string('method_gd_invoice')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_pays', function (Blueprint $table) {
            //
        });
    }
}
