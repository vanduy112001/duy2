<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColoCationConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colocation_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('colocation_id')->nullable();
            $table->string('disk')->nullable();
            $table->string('ram')->nullable();
            $table->string('cpu')->nullable();
            $table->integer('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colo_cation_configs');
    }
}
