<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColocation0910 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('colocations', 'date_create')) {
              Schema::table('colocations', function (Blueprint $table) {
                  $table->date('date_create')->nullable()->change();
              });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colocation', function (Blueprint $table) {
            //
        });
    }
}
