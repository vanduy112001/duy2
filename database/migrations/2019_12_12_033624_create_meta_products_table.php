<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMetaProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('product_id');
            $table->bigInteger('email_id');
            $table->string('hidden')->nullable();
            $table->string('cpu')->nullable();
            $table->string('memory')->nullable();
            $table->string('disk')->nullable();
            $table->string('os')->nullable();
            $table->string('ip')->nullable();
            $table->string('bandwidth')->nullable();
            $table->string('storage')->nullable();
            $table->string('domain')->nullable();
            $table->string('sub_domain')->nullable();
            $table->string('alias_domain')->nullable();
            $table->string('database')->nullable();
            $table->string('ftp')->nullable();
            $table->string('panel')->nullable();
            $table->string('name_server')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_products');
    }
}
