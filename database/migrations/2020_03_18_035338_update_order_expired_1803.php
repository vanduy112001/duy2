<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderExpired1803 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('order_expireds', 'billing_cycle')) {
            Schema::table('order_expireds', function (Blueprint $table) {
                $table->string('billing_cycle')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_expireds', function (Blueprint $table) {
            //
        });
    }
}
